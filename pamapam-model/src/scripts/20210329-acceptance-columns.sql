ALTER TABLE social_economy_entity ADD user_conditions_accepted tinyint(1) NULL;
ALTER TABLE social_economy_entity ADD user_conditions_date DATETIME DEFAULT NULL NULL;
ALTER TABLE social_economy_entity ADD newsletters_accepted tinyint(1) NULL;
ALTER TABLE social_economy_entity ADD newsletters_date DATETIME DEFAULT NULL NULL;
