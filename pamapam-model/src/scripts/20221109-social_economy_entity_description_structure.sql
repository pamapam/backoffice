-- ANTES DEL SCRIPT
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Sala d''Armes Montjuïc va ser fundada el 1977 pel Mestre italià Pierluigi Chicca, de manera que som el club d''esgrima més antic de Catalunya. Actualment som prop de 350 persones sòcies i ens sentim molt orgulloses de la nostra Escola Municipal d''Esgrima, per on cada temporada passen prop de 200 nens i nenes, que aprenen els secrets de l''esgrima i es formen com a esportistes i com a persones. Al club practiquem les tres modalitats d''esgrima esportiva: l''espasa, el floret i el sabre. A més, tenim grups d''esgrima escènica i d''esgrima històrica i una secció estable d''esgrima en cadira de rodes, amb les instal·lacions i vestidors adaptats i amb una àrea especialment preparader a la seva pràctica. Som un clua pb esportiu obert a tothom, i per això realitzem una aposta ferma pels cursos d''iniciació per a qualsevol edat, des dels 6 anys fins als 99! Més enllà de la vessant esportiva, el SAM és una institució amb una vocació cultural i social amb la voluntat d''esdevenir una comunitat implicada en la vida del barri i de la ciutat."}, {"la": "es", "text": ""}]}'
where ID = 7871;

update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Molsa és un projecte iniciat per persones per a persones. Creiem profundament que el model de consum basat en una filosofia econòmica purament especulativa no és suficient, ja que persegueix únicament els beneficis financers. A Molsa creiem que un model cooperatiu és beneficiós per als membres de la cooperativa, els treballadors, els consumidors, la societat i l''entorn."}, {"la": "es", "text": ""}]}'
where ID = 7927;

-- Empieza el script autogenerado
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sistemes avançats d´energia solar tèrmica."}, {"la": "es", "text": ""}]}'
where ID = 7671;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparació i lloguer de bicicletes. Monitors temps lliure."}, {"la": "es", "text": ""}]}'
where ID = 7672;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció socio-laboral de persones amb discapacitats."}, {"la": "es", "text": ""}]}'
where ID = 7673;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperació Internacional."}, {"la": "es", "text": ""}]}'
where ID = 7674;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de les Finances Ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7675;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acció social, cultural i ambiental. Economia social."}, {"la": "es", "text": ""}]}'
where ID = 7676;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Micromecenatge de projectes cívics, col.laboratius i oberts."}, {"la": "es", "text": ""}]}'
where ID = 7677;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoure la innovació social."}, {"la": "es", "text": ""}]}'
where ID = 7678;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Investigació, difusió i suport campanyes."}, {"la": "es", "text": ""}]}'
where ID = 7679;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sensibilització per a la solidaritat i la cooperació."}, {"la": "es", "text": ""}]}'
where ID = 7680;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció social i laboral de persones amb malalties mentals"}, {"la": "es", "text": ""}]}'
where ID = 7681;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Centre, Ateneu democràtic i progressista és una entitat fundada l’any 1865 per les classes populars de Caldes de Montbui, en el bressol del moviment ateneístic de caire humanista de finals del segle XIX, amb l’objectiu de formar lliurepensadors/es per transformar una realitat injusta. Actualment, el Centre té el compromís de continuar amb la mateixa tasca mirant al segle XXI.\n\nEn l’actualitat, a l’Ateneu hi participen 10 entitats i seccions: el concurs de teatre Taca’m, la Coral del Centre, Jazz Sessions Club, Club d’Escacs Caldes, la Cobla Thermalenca, Scena Teatre, Calderins pel programari lliure, el Centre Sona, l’associació juvenil la Guspira i la Cooperativa de consum ecològic el Rusc. Totes aquestes entitats es coordinen a través de la Junta, l’òrgan de decisió del Centre.\n\nA més compta amb El cafè cultural del Centre, un espai que aposta per una oferta de productes de proximitat i de qualitat i amb una programació cultural estable per a totes les edats (de dimarts a diumenge a partir de les 15 h).\n\nEls objectius de l’Ateneu passen per enfortir el projecte associatiu i donar cabuda a d’altres iniciatives que puguin sorgir al municipi, així com generar recursos per acabar de rehabilitar l’edifici i així donar resposta a les necessitats de l’entitat."}, {"la": "es", "text": ""}]}'
where ID = 7682;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arquitectura, urbanisme, participació comunitaria i feminista."}, {"la": "es", "text": ""}]}'
where ID = 7683;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concedir préstecs sense interés per crear llocs de treball."}, {"la": "es", "text": ""}]}'
where ID = 7684;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Federació entitats. Gestió cultural i esportiva"}, {"la": "es", "text": ""}]}'
where ID = 7685;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació que promou l’Agroecologia i la Sobirania Alimentària incidint en els àmbits agroalimentari i del desenvolupament local. Les membres d’Arran de terra comptem amb una formació acadèmica interdisciplinar, que integra aportacions de les ciències naturals i les ciències socials, i amb experiència laboral en recerca, formació, educació, assessorament i dinamització d’iniciatives locals de transició agroecològica."}, {"la": "es", "text": ""}]}'
where ID = 7686;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i altres serveis a les empreses."}, {"la": "es", "text": ""}]}'
where ID = 7687;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Compromeses amb les dones, transformant la societat. l''empoderament és l''eix transversal que articula els àmbits d''acció de Surt, alhora que la filosofía que n''impregna les metodologies de treball"}, {"la": "es", "text": ""}]}'
where ID = 7688;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Participació ciutadana en processos de transformació urbana."}, {"la": "es", "text": ""}]}'
where ID = 7689;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny gràfic i Comunicació visual."}, {"la": "es", "text": ""}]}'
where ID = 7690;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TRACTA''M ÉS UNA COOPERATIVA DE FISIOTERAPEUTES\nNeix amb l’objectiu de crear un projecte on la prioritat és l’atenció personalitzada, directa, amb una visió global de la persona i el seu entorn.\nEstem dins de l’economia social i solidària. Entenent la realitat econòmica i social en la que estem vivint, volem donar facilitats a l’hora d’accedir a aquest servei sanitari amb tarifes assequibles per a tothom, sense perjudicar la qualitat del servei, tant a nivell professional com personal.\nEstem associades de forma cooperativa on no hi ha jerarquies entre les persones treballadores, es prenen les decisions de forma assembleària i amb total transparència. Totes les persones usuàries participen en el creixement de la cooperativa, fent-la sostenible i millorant-ne el servei amb els seus suggeriments."}, {"la": "es", "text": ""}]}'
where ID = 7691;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció i dinamització de fires, foment de la cooperació i associacionisme i serveis complementaris"}, {"la": "es", "text": ""}]}'
where ID = 7692;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de serveis financers islàmics. Finances ètiques i participatives."}, {"la": "es", "text": ""}]}'
where ID = 7693;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Restaurant, càtering, cuines col.lectivitats i infusions ecològiques"}, {"la": "es", "text": ""}]}'
where ID = 7694;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball que gestiona un comerç d''alimentació ecològica al barri de Gràcia"}, {"la": "es", "text": ""}]}'
where ID = 7695;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació privada el Maresme pro persones amb discapacitat intel·lectual és una entitat d''iniciativa social sense afany de lucre que promou i impulsa la integració social i la millora de la qualitat de vida de les persones amb discapacitat intel·lectual de la comarca del Maresme i de les seves famílies.\n\nLa Fundació procura donar resposta a les necessitats i demandes d''aquest col·lectiu organitzant una àmplia xarxa de serveis i centres que ofereixen una atenció amb continuïtat."}, {"la": "es", "text": ""}]}'
where ID = 7696;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ramat d''ovelles amb formatgeria pròpia"}, {"la": "es", "text": ""}]}'
where ID = 7697;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que té com a finalitat principal la reinserció sòcio laboral de persones amb disminució derivada de malaltia mental a partir de la gestió de residus i protecció al Medi Ambient. També portem a terme projectes de cooperació al desenvolupament dins l''àmbit sanitari"}, {"la": "es", "text": ""}]}'
where ID = 7698;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament de grups en l''autopromoció del seu cohabitatge"}, {"la": "es", "text": ""}]}'
where ID = 7699;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball per persones amb discapacitat derivat de malaltia mental. Comarca d''Osona."}, {"la": "es", "text": ""}]}'
where ID = 7700;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir persones afectades de FM,SFC, i altres SSC , mitjançant teràpies alternatives i xerrades informatives."}, {"la": "es", "text": ""}]}'
where ID = 7701;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Tac Osona és l''empresa social de Sant Tomàs, una entitat sense ànim de lucre que vetlla per millorar la qualitat de vida de les persones amb discapacitat intel·lectual i les seves famílies a la comarca d’Osona."}, {"la": "es", "text": ""}]}'
where ID = 7702;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treballem per fomentar el millor desenvolupament i salut mental de nens i adolescents de 0 a 18 anys."}, {"la": "es", "text": ""}]}'
where ID = 7703;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació, té per finalitat la de promoure i dur a terme la gestió, representació i administració de tot tipus de centres residencials, pisos protegits i llars residencials dedicades a l''atenció, asistència i acolliment de malalts psíquics i patologies assimilades."}, {"la": "es", "text": ""}]}'
where ID = 7704;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria/Consultoria Energètica especialitzada en eficiència energètica i energies renovables"}, {"la": "es", "text": ""}]}'
where ID = 7705;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Atenció a les persones amb problemes de salut mental i adiccions de la comarca d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7706;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de barri amb la finalitat de millorar les condicions de vida de les veïnes mijançant el foment de de projectes comunitaris i accions culturals"}, {"la": "es", "text": ""}]}'
where ID = 7707;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa sense ànim de lucre i d’iniciativa social. Treballem en projectes estratègics d’incidència política per a la promoció de l''equitat de gènere. Els nostres eixos de treball són: la comunicació amb perspectiva feminista, l''abordatge estratègic de les violències masclistes i la incorporació de la perspectiva de gènere en organitzacions i polítiques públiques. "}, {"la": "es", "text": ""}]}'
where ID = 7708;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“La Llera” és una cooperativa de consum sense ànim de lucre que treballa per a una nova mirada de l''educació cap als infants creant una estructura pedagògica que intenta respectar els seus ritmes, que permet mirar a l''infant amb confiança, on cada un pot trobar-se a ell/ella mateixa i desenvolupar-se segons les necessitats i interessos propis. "}, {"la": "es", "text": ""}]}'
where ID = 7709;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat juvenil del barri de Prosperitat. Gestiona el Casal de Joves de Prospe."}, {"la": "es", "text": ""}]}'
where ID = 7710;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense anim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7711;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“Contribuir, des del seu compromís ètic, mitjançant suports i oportunitats, a què cada persona amb paràlisi cerebral o altres discapacitats similars i la seva família pugui desenvolupar el seu projecte de qualitat de vida, així com a promoure la seva inclusió com a ciutadana de ple dret en una societat justa i solidària. L’Associació Esclat desenvolupa la seva activitat majoritàriament a Catalunya"}, {"la": "es", "text": ""}]}'
where ID = 7712;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense afany de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7713;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Realitzem serveis de formació, consultoria i recerca relacionats amb la participació ciutadana, la gestió comunitària i l''habitatge."}, {"la": "es", "text": ""}]}'
where ID = 7714;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de Desenvolupament Infantil i Atenció Precoç"}, {"la": "es", "text": ""}]}'
where ID = 7715;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Coopertiva enginyeria del món de l''energia"}, {"la": "es", "text": ""}]}'
where ID = 7716;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "serveis especialitzats d’inserció laboral pel col·lectiu de persones diagnosticades d’esclerosi múltiple i altres discapacitats fisiques i/o sensorials"}, {"la": "es", "text": ""}]}'
where ID = 7717;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa ambiental. Desenvolupament rural, estudis de biodiversitat i millora d''espais naturals, comunicació ambiental i ecoturisme"}, {"la": "es", "text": ""}]}'
where ID = 7718;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Situat a Barcelona, és el primer centre de referència de la mobilitat sostenible a l’Estat espanyol i estem especialitzats en el desenvolupament de la bicicleta com a mode de transport a la ciutat. A més de la mobilitat, l’associació de segon grau BiciHub és un projecte que també gira al voltant de l’Economia Social i Solidària (ESS). Apostem per la intercooperació i la gestió comunitària i cooperativa d’equipaments socials. En aquest sentit, volem formar part del patrimoni ciutadà i que els agents locals i el veïnat ens facin seus."}, {"la": "es", "text": ""}]}'
where ID = 7719;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Societat Orgànica+10"}, {"la": "es", "text": ""}]}'
where ID = 7720;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Afavorir la transformació social a partir de la Sobirania Alimentària amb perspectiva feminista"}, {"la": "es", "text": ""}]}'
where ID = 7721;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Sala d''Armes Montjuïc va ser fundada el 1977 pel Mestre italià Pierluigi Chicca, de manera que som el club d''esgrima més antic de Catalunya. Actualment som prop de 350 persones sòcies i ens sentim molt orgulloses de la nostra Escola Municipal d''Esgrima, per on cada temporada passen prop de 200 nens i nenes, que aprenen els secrets de l''esgrima i es formen com a esportistes i com a persones. Al club practiquem les tres modalitats d''esgrima esportiva: l''espasa, el floret i el sabre. A més, tenim grups d''esgrima escènica i d''esgrima històrica i una secció estable d''esgrima en cadira de rodes, amb les instal·lacions i vestidors adaptats i amb una àrea especialment preparader a la seva pràctica. Som un clua pb esportiu obert a tothom, i per això realitzem una aposta ferma pels cursos d''iniciació per a qualsevol edat, des dels 6 anys fins als 99! Més enllà de la vessant esportiva, el SAM és una institució amb una vocació cultural i social amb la voluntat d''esdevenir una comunitat implicada en la vida del barri i de la ciutat."}, {"la": "es", "text": ""}]}'
where ID = 7722;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació cultural sense ànime de lucre especialitzada en el teatre d''intervenció socials, el foment de la cultura i la formació teatral"}, {"la": "es", "text": ""}]}'
where ID = 7723;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7724;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte de roba personalitzada ambientalment sostenible i de producció ètica."}, {"la": "es", "text": ""}]}'
where ID = 7725;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa jurídica multidisciplinària compromesa amb la justícia social"}, {"la": "es", "text": ""}]}'
where ID = 7726;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat representativa del comerç i les empreses del barri de Trinitat Nova"}, {"la": "es", "text": ""}]}'
where ID = 7727;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''Habitatge"}, {"la": "es", "text": ""}]}'
where ID = 7728;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat d''atenció a persones amb paràlisi cerebral"}, {"la": "es", "text": ""}]}'
where ID = 7729;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball dedicat a la producció d''ous ecològics"}, {"la": "es", "text": ""}]}'
where ID = 7730;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "entitat esportiva que promou l''esport per  a persones amb discapacitat intel·lectual."}, {"la": "es", "text": ""}]}'
where ID = 7731;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Osona Ioga és el centre de referència a la comarca on diferents professionals unifiquem processos, eines i experiència en un projecte comú que posem al teu servei. La nostra missió és comprometre’ns amb tu i la teva salut oferint-te un espai de dedicació personal dins la teva vida quotidiana per a millorar el teu benestar i acompanyar-te en el procés d’aconseguir una millor qualitat de vida."}, {"la": "es", "text": ""}]}'
where ID = 7732;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Trabajamos por la inclusión de personas vulnerables. "}, {"la": "es", "text": ""}]}'
where ID = 7733;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Salut Mental Ponent és una entitat sense ànim de lucre i d’iniciativa social de les comarques de Lleida, registrada l’any 1993, creada en el seu origen per familiars i amics de persones amb algun trastorn mental, per tal de proporcionar orientació, informació, sensibilització, atenció social i un suport emocional i psicològic."}, {"la": "es", "text": ""}]}'
where ID = 7734;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa Feminista pel benestar de les persones en l''entorn laboral"}, {"la": "es", "text": ""}]}'
where ID = 7735;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions comunes i participades per a la millora del desenvolupament i la gestió col·lectiva del territori."}, {"la": "es", "text": ""}]}'
where ID = 7736;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7738;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7741;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7742;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació d''iniciativa social."}, {"la": "es", "text": ""}]}'
where ID = 7743;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "promoció de la salut sexual i la prevenció de riscos associa"}, {"la": "es", "text": ""}]}'
where ID = 7744;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d''iniciativa social que vetllem per potenciar l''autonomia i les oportunitats de les persones, tant a nivell individual com col•lectiu, amb la voluntat de contribuir a la transformació d''una societat més justa."}, {"la": "es", "text": ""}]}'
where ID = 7745;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7746;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "disseny"}, {"la": "es", "text": ""}]}'
where ID = 7747;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7748;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7749;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7750;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7751;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7752;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de menjadors escolars i educació del lleure."}, {"la": "es", "text": ""}]}'
where ID = 7753;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat excursionista"}, {"la": "es", "text": ""}]}'
where ID = 7754;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''infraestructures"}, {"la": "es", "text": ""}]}'
where ID = 7755;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Professionals en tècniques de salut per mèdis naturals"}, {"la": "es", "text": ""}]}'
where ID = 7756;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Introduccio a la cultura del treball col·lectius en exclusio"}, {"la": "es", "text": ""}]}'
where ID = 7757;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats educatives dirigides a dones i joves de Dakar."}, {"la": "es", "text": ""}]}'
where ID = 7758;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de mercat social."}, {"la": "es", "text": ""}]}'
where ID = 7759;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola de formació contínua del cooperativisme."}, {"la": "es", "text": ""}]}'
where ID = 7760;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció promoguda per PROMOCIONS i grup persones promotores"}, {"la": "es", "text": ""}]}'
where ID = 7761;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7762;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Energies renovables."}, {"la": "es", "text": ""}]}'
where ID = 7763;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai gastronòmic de l''Ateneu El Centre."}, {"la": "es", "text": ""}]}'
where ID = 7764;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7765;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arts grafiques i Venda de productes ecologics."}, {"la": "es", "text": ""}]}'
where ID = 7766;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma interassociativa d''entitats juvenils de BCN"}, {"la": "es", "text": ""}]}'
where ID = 7767;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de cluns de fitness"}, {"la": "es", "text": ""}]}'
where ID = 7768;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Clúster pel desenvolupament de l''economia social del treball. Està integrat per entitats socials sense ànim de lucre titulars d''un centre especial de treball (CET) o una empresa d''inserció (EI). "}, {"la": "es", "text": ""}]}'
where ID = 7769;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació excursionista."}, {"la": "es", "text": ""}]}'
where ID = 7770;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7771;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria i Consultoria Energètica"}, {"la": "es", "text": ""}]}'
where ID = 7772;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament a les cooperatives"}, {"la": "es", "text": ""}]}'
where ID = 7773;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i desenvolupament en ciències socials."}, {"la": "es", "text": ""}]}'
where ID = 7774;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Importador i distribuidor de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7775;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessorament i recerca per la sostenibilitat."}, {"la": "es", "text": ""}]}'
where ID = 7776;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció laboral mitjançant diverses activitats."}, {"la": "es", "text": ""}]}'
where ID = 7777;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ateneu social i cultural "}, {"la": "es", "text": ""}]}'
where ID = 7778;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluitar contra la pobresa i l''exclusió social."}, {"la": "es", "text": ""}]}'
where ID = 7779;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola d''infantil, primària i secundària."}, {"la": "es", "text": ""}]}'
where ID = 7780;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''edició i creació gràfica."}, {"la": "es", "text": ""}]}'
where ID = 7781;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treball social en inclusió sociolaboral i cohesió veïnal "}, {"la": "es", "text": ""}]}'
where ID = 7782;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir infants i families en risc d''exclusió social"}, {"la": "es", "text": ""}]}'
where ID = 7783;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació sense ànim de lucre amb fins socials"}, {"la": "es", "text": ""}]}'
where ID = 7784;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d´atenció a la persona."}, {"la": "es", "text": ""}]}'
where ID = 7785;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial i  acció cultural sociopolitica i econ. Som de Xes"}, {"la": "es", "text": ""}]}'
where ID = 7786;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers i Acompanyament processos."}, {"la": "es", "text": ""}]}'
where ID = 7787;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de formació per la inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7788;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria Social"}, {"la": "es", "text": ""}]}'
where ID = 7789;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa dedicada a l''assessorament empresarial dins dels àmbits de les vendes i el màrqueting, la gestió orientada en els valors i la formació d''emprenedoria i inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7790;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició publicació La Marea."}, {"la": "es", "text": ""}]}'
where ID = 7791;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Suport a les finances ètiques i a l''emprenedoria social."}, {"la": "es", "text": ""}]}'
where ID = 7792;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Aportem solucions integrals a tot tipus d''indústries mitjançant implantació i certificació de sistemes de Gestió ISO, assessorament i formació perquè aconsegueixin els més alts estàndards de qualitat exigits pels seus clients."}, {"la": "es", "text": ""}]}'
where ID = 7793;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de consum responsable"}, {"la": "es", "text": ""}]}'
where ID = 7794;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de promoció, difusió i distribució de Comerç Just"}, {"la": "es", "text": ""}]}'
where ID = 7795;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONGD"}, {"la": "es", "text": ""}]}'
where ID = 7796;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "MEDIACIÓ ASSEGURANCES PER A ENTITATS SOCIALS"}, {"la": "es", "text": ""}]}'
where ID = 7797;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "centre mèdic"}, {"la": "es", "text": ""}]}'
where ID = 7798;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Tot Raval és una a plataforma de 48 associacions, institucions, persones i empreses vinculades al Raval que es crea el 2002 amb un objectiu comú: la millora de la qualitat de vida en el barri. Es desenvolupa un treball comunitari, partint d’una visió integral del barri, que incideix en els àmbits social, cultural i econòmic i comercial."}, {"la": "es", "text": ""}]}'
where ID = 7799;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions integrals (sectors energètics i industrials)."}, {"la": "es", "text": ""}]}'
where ID = 7800;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció, distribució i venda de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7801;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "impulsar el desenvolupament i l''ús de la custòdia del territori com a eina de conservació."}, {"la": "es", "text": ""}]}'
where ID = 7802;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7803;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "DELTA gent activa es una entitat sense ànim de lucre, ONG/ONL, que treballant per i des de el sector social, es dirigeix cap a les persones grans i a les que sense ser-lo encara, tenen inquietuds a la vora d''aquest sector de la nostra societat i desitgen participar i/o col·laborar de forma ACTIVA per ajudar a la millora de la qualitat de vida del mateix i a la seva plena integració. "}, {"la": "es", "text": ""}]}'
where ID = 7804;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Administració de finques."}, {"la": "es", "text": ""}]}'
where ID = 7805;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que centre la seva activitat a la defensa de la llengua, la cultura, la cohesió social, l''educació i la identitat nacional de Catalunya. \n\n"}, {"la": "es", "text": ""}]}'
where ID = 7806;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "CERESNATURAL és una cooperativa que treballa per la salut a través de la venda de productes ecològics de proximitat i de la realització de teràpies naturals. La nostra motivació és oferir un espai que fomenti àmpliament la sostenibilitat, el comerç just, i la salut. Sent el nostre objectiu final el fet d’aconseguir que cadascú es faci responsable de la seva pròpia salut i la del planeta."}, {"la": "es", "text": ""}]}'
where ID = 7807;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de segon grau de restauració i dinamització social, que ofereix els seus serveis dins del Centre, Ateneu Democràtic i Progressista. Actualment està formada per set persones sòcies."}, {"la": "es", "text": ""}]}'
where ID = 7808;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de productes ecobiològics del barri del Congrés (Barcelona)"}, {"la": "es", "text": ""}]}'
where ID = 7809;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsar i elaborar els materials Escuracassoles."}, {"la": "es", "text": ""}]}'
where ID = 7810;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Ateneu Santcugatenc va ser creat l’any 1956 per un grup de ciutadans santcugatencs afeccionats al col·leccionisme, la numismàtica, la medallística i la filatèlia. L’any 1977 fou registrat com associació sense ànim de lucre amb la finalitat de fomentar també l’expansió artística, científica i/o la divulgació cultural, promovent el seu màxim desenvolupament i facilitant els coneixements que sobre aquestes matèries requereixin els seus associats/des. Amb el pas dels anys va anar incorporant als seus estatuts nous fins socials relacionats amb la cultura, l’associacionisme i el lleure, entre d’altres. \n\nDes de fa més de 15 anys desenvolupa un programa de cursos i tallers amb una bona acceptació per part de la ciutadania. L’entitat té la capacitat d’organitzar un programa trimestral de 70 disciplines diferents que posa a disposició de la població més de 1.400 places de formació i lleure. La xifra d’ocupació del darrer any 2015, només en aquest projecte, ha estat de 3.053 inscripcions. \n\nPel que fa l’àmbit de Cultura, a banda de tota la programació de cursos i tallers, realitza accions per al foment de la lectura, la gastronomia, la cultura popular, exposicions, concurs de pessebres, Festa de Tardor i, esporàdicament, Microespectacles entre d’altres accions. Així com activitats per a nens i nenes com casals d’estiu o vacances de Nadal.\n\nL’Ateneu realitza un seguit d’activitats en el camp de les polítiques socials que l’han fet mereixedor d’un conveni amb aquest àmbit de l’Ajuntament de Sant Cugat, per desenvolupar: l’Oficina del Voluntariat, l’Espai de Lleure (Lleure Social per a persones amb Trastorn Mental Sever), Joves Jubilats (envelliment actiu) i Català per a Tothom (projecte de foment de l’ús social del català i acollida sociocultural de persones nouvingudes). \n\nUn dels altres àmbits on ha tingut intervenció ha estat l’educatiu. Coordinant-se amb el Pla Educatiu d’Entorn dinamitza els Tallers d’Estudi Assistit per alumnes de secundaria de l’institut Arnau Cadell, el col·legi Pureza de Maria i l’Avenç, per lluitar contra el fracàs escolar, així com els grups de conversa i aprenentatge de català, directament a les escoles de primària, amb pares i mares de nens i nenes nouvinguts. \n\nO també activitats de sensibilització ambiental i promoció del consum responsable, ecològic, de proximitat i la reducció de residus, com el Mercat de Pagès, els Mercats de 2a mà o la sensibilització contra l’ús de bosses de plàstic.\n\nA més a més el fet d’haver pogut disposar fins ara de l’edifici de plaça Pep Ventura, 1, al nucli antic de la ciutat, obert a la ciutadania i a les entitats, permet gaudir d’espais de trobada i relació, exposicions, xerrades i conferències, presentacions de llibres, reunions o assajos. En aquests moment diferents entitats o col·lectius usen les instal·lacions de l’Ateneu per a desenvolupar les seves activitats de manera estable, com per exemple: Amics dels Veterans i la Penya Regalèssia, Comissió Festa de Tardor, Institut Trastorn Límit Fundació, Federació d’Associacions de Veïns, Grup de Jocs de Rol, Grup d’estudis de Teosofia, Pipa Club Sant Cugat, Societat Gastronòmica del Vallès. Alguns a més utilitzen la infraestructura i personal de l’Ateneu per a l’organització de les seves activitats, recepció d’usuaris i/o inscripcions.  A aquest cal sumar-hi els que de manera puntual utilitzen també la seu de l’Ateneu.\n"}, {"la": "es", "text": ""}]}'
where ID = 7811;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7812;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció audiovisual i Comunicació integral."}, {"la": "es", "text": ""}]}'
where ID = 7813;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lafede.cat està formada per 117 organitzacions i neix el gener de 2013 com a resultat de la fusió de les antigues Federació Catalana d’ONG per al Desenvolupament-FCONGD, Federació Catalana d’ONG pels Drets Humans-FCONGDH i Federació Catalana d’ONG per la Pau-FCONGPAU (vegeu història de Lafede.cat)Lafede.cat recull l’experiència i la trajectòria associativa de les seves 116 organitzacions federades però, a la vegada, aposta per la integració i l’actualització dels seus discursos, objectius i espais de relació institucional. Després d’un procés participatiu de mesos, a la seva primera Assemblea general, el 19 de juny de 2014."}, {"la": "es", "text": ""}]}'
where ID = 7814;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que va començar amb l''activitat principal d''apadrinar, adoptar i rescatar animals salvatges i de companyia. Arrel de l''experiència adquirida com a professionals del sector, a l''any 2012, FAADA va decidir iniciar un procés de transformació intern per poder incidir a les causes de forma més estratègica des de l''acció en l''àmbit social, legislatiu i educatiu. \nFacilitem informació i assessorament tècnic i jurídic a la societat sobre la protecció els animals i dirigim projectes de sensibilització que plantegin alternatives als sectors empresarials que directa o indirectament causen patiment als animals. Promovem accions formatives que incideixen en el foment de l''empatia i el respecte pels animals en els diferents cicles educatius. "}, {"la": "es", "text": ""}]}'
where ID = 7815;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny per emprenedoria social i iniciatives ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7816;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7817;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7818;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjançant la innovació narrativa, tecnològica i social, a COMPACTO posem la nostra creativitat al servei d’un món més lliure, solidari i participatiu. Des que vam fundar l’empresa el 2010, ens involucrem en projectes ambiciosos que transmetin els valors d’innovació, solidaritat, participació i llibertat, per enfortir l’economia social a través de la cultura i la cultura a través de l’economia social i col·laborativa."}, {"la": "es", "text": ""}]}'
where ID = 7819;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Distribució de productes ecològics, frescos i locals a les botigues i restaurants. Treballem  amb productors, cooperatives i entitats amb dificultats per fer arribar els productes als establiments oferint un servei global dins un àmbit local i creant xarxes de intercooperació local. Cafès La Chapolera és la creació pròpia de cafè ecològic d''origen amb valor social"}, {"la": "es", "text": ""}]}'
where ID = 7820;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La inserció laboral de persones en situació de risc i/o exclusió social, mitjançant la combinació de formació teòric-pràctic, social, personal i laboral de forma  que possibiliti l'' adquisició de hàbits i coneixements suficients per a poder accedir a un lloc de feina normalitzat.\nVenda de productes recicltas de segona mà i subsproductes relacionats amb els següents sectors: mobles, articles textils.."}, {"la": "es", "text": ""}]}'
where ID = 7821;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Portar a terme qualsevol activitats econòmiques de producció de béns, comercialització o de prestació de serveis que tinguin per finalitat primordial la de fer integració sociolaboral de .persones en situació o greu risc d''exclusió social.                                       "}, {"la": "es", "text": ""}]}'
where ID = 7822;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Panorama180 és una iniciativa sense ànim de lucre que es centra en el cinema i l’experimentació audiovisuals realitzats amb paràmetres del segle XXI: digitalització, xarxa, compromís, agitació, canvi, empoderament, comunitat…atentes a les novetats i propostes superadores dels problemes generats per la concepció obsoleta dels drets d’autor en l’era digital."}, {"la": "es", "text": ""}]}'
where ID = 7823;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i informació."}, {"la": "es", "text": ""}]}'
where ID = 7824;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Borda, és una cooperativa d''habitatge en cessió d''us, ubicada al barri de Sants de Barcelona. Un cop acabades les obres (a l''estiu 2018) tindrà 28 habitatges i varis espais comunitàris. Té signat un dret de superficie amb l''Ajuntament per 75 anys. Es construeix en sol públic ( i quedarà public) i té qualificació màxima d''eficiència energètica. L’edifici costarà tres milions d’euros (no arriba a 110.000 per habitatge) i es finançia amb l’aportació inicial dels socis, un préstec de Coop 57, l’emissió de títols participatius per part de la mateixa cooperativa i aportacions o préstecs d’altres entitats. "}, {"la": "es", "text": ""}]}'
where ID = 7825;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de projectes en l´àmbit de la Joventut."}, {"la": "es", "text": ""}]}'
where ID = 7826;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació"}, {"la": "es", "text": ""}]}'
where ID = 7827;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Matriu és un laboratori de democràcia viva, i ofereix formacions, acompanyament, recerca i creació de materials, amb la finalitat d''afavorir que persones i grups esdevinguin agents actius en la construcció de relacions i formes d’organització conscients i respectuoses.\nLa mirada dels feminismes; dóna eines per abordar la complexitat de cada una de les relacions i els grups, en què sovint actuen eixos diversos de desigualtat i opressió. Ens situa davant del repte d’atendre amb cura, a més, les dimensions múltiples de les relacions: emocions, experiències, somnis, posicionaments, coneixements, creences.\nEnfoca l’apoderament de grups i de persones; a través de generar processos de presa de consciència de la diversitat existent i de la distribució de poder que hi opera, com també de transformar els conflictes i establir mecanismes de diàleg i comunicació útils.\nAposta per la cerca de nous imaginaris polítics, noves formes d’organitzar-se, a través de concretar col·lectivament com es pot funcionar perquè les vides que vivim siguin dignes i sostenibles."}, {"la": "es", "text": ""}]}'
where ID = 7828;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ingeniería Social S.A.L. és una empresa d’economia cooperativa especialitzada en la formació, diagnòstic, definició, implementació i verificació de sistemes de gestió en Responsabilitat Social Corporativa. Ubicada a la ciutat de Barcelona, l’empresa compta amb un equip multidisciplinari format per consultores i expertes en matèria de responsabilitat social. \nLa nostra missió és assessorar a les empreses i organitzacions a desenvolupar negocis i organitzacions estables i sostenibles que aportin valor a la societat.\n"}, {"la": "es", "text": ""}]}'
where ID = 7829;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Andròmines te com a missió ser una organització de referència en la lluita i denuncia de l?exclusió social, defensant la dignitat i la igualtat d?oportunitats per totes les persones i la defensa i protecció del medi ambient.\n\nAquest darrer any Andròmines ha augmentat el dissenya i execució de programes de capacitació, formació i inserció laboral que s?adeqüen a les necessitats socials dels col?lectius i a la demanda real del mercat de treball. \n\nles empreses que contracten els serveis de gestió de residus, així com a les persones i organitzacions on arriben les campanyes d''educació ambiental i a les entitats amb les que realitzem treball en xarxa\n"}, {"la": "es", "text": ""}]}'
where ID = 7830;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Elaboració ecològica i autòctona de cerveses, most i maltes."}, {"la": "es", "text": ""}]}'
where ID = 7831;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "gdgfdgfd"}, {"la": "es", "text": ""}]}'
where ID = 7832;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7833;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7834;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7835;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7836;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball associat dedicada a la docència."}, {"la": "es", "text": ""}]}'
where ID = 7837;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA PAGE ORIGINAL, estudi de comunicació visual, es constitueix com empresa l’any 1991.\nEn l’actualitat, els seus socis Sònia Martínez Ruzafa i Josep Martínez Ruzafa (llicenciats en Belles Arts per la Universitat de Barcelona) dirigeixen projectes especialitzats en comunicació social, institucional i empresarial."}, {"la": "es", "text": ""}]}'
where ID = 7838;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7839;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7840;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació Vella Terra és una entitat sense ànim de lucre que neix amb el ferm compromís de promocionar l''autonomia de les persones depenents, i donar continuitat i sentit al seu projecte de vida. Els principals col·lectius atesos son persones grans, persones amb discapacitat, i persones tutelades."}, {"la": "es", "text": ""}]}'
where ID = 7841;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Instituto de Economía Social y Solidaria"}, {"la": "es", "text": ""}]}'
where ID = 7842;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7843;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de Servicios para el Mercado Social de Aragón"}, {"la": "es", "text": ""}]}'
where ID = 7844;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "eee"}, {"la": "es", "text": ""}]}'
where ID = 7845;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7846;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un agència de disseny i desenvolupament web i d’aplicacions al servei de la transformació social.Estem aquí per facilitar a les organitzacions la incorporació de la innovació tecnològica i ajudar-les a superar l''escletxa digital.Posem a l’abast de tothom el potencial que ofereix la tecnologia, adaptant-nos a les necessitats i possibilitats de cada projecte i acompanyant-lo amb un equip creatiu, multidisciplinar i proper que posa l’accent en l’accessibilitat, la pedagogia, el treball col·laboratiu i la perspectiva de gènere.Apostem per les tecnologies lliures com a instrument per assolir la sobirania tecnològica efectiva."}, {"la": "es", "text": ""}]}'
where ID = 7847;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7848;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestiona 19 habitatges dignes per a famílies en situació de vulnerabilitat i/o d''exclusió social. Acolliment i cessió d''ús dels habitatges per un període màxim de 3 anys."}, {"la": "es", "text": ""}]}'
where ID = 7849;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hosteleria mixta."}, {"la": "es", "text": ""}]}'
where ID = 7850;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 7851;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(la)baula: peça en forma d’anella que, enllaçada amb altres similars, forma una cadena.\n\nlabaula és una cooperativa d’arquitectes amb trajectòries professionals diverses que han decidit cooperar i sumar esforços per construir un projecte comú amb una metodologia de treball horitzontal.\n\nEl camp de treball de labaula consisteix en projectes d’edificació, rehabilitació, disseny industrial, urbanisme, espais públics, escenografia i espais efímers. Encarem els treballs amb idèntica dedicació i esforç, i amb independència de les seves dimensions o característiques. Aquests són entesos sempre com un espai de debat on s’incorporen col·laboradors d’altres disciplines per enriquir les perspectives pròpies.\n\nlabaula neix de la nostra il·lusió i inquietud per intentar fomentar la qualitat, el rigor i la responsabilitat en la nostra feina. Esperem poder ser una plataforma des d’on seguir treballant en aquesta direcció."}, {"la": "es", "text": ""}]}'
where ID = 7852;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7853;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fabricació de motlles i matrius per automoció."}, {"la": "es", "text": ""}]}'
where ID = 7854;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Teixits amb tellers manuals."}, {"la": "es", "text": ""}]}'
where ID = 7855;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió veïnal de barri."}, {"la": "es", "text": ""}]}'
where ID = 7856;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocions d´iniciatives socials i económiques."}, {"la": "es", "text": ""}]}'
where ID = 7857;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social."}, {"la": "es", "text": ""}]}'
where ID = 7858;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers."}, {"la": "es", "text": ""}]}'
where ID = 7859;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparacions i reformes."}, {"la": "es", "text": ""}]}'
where ID = 7860;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comercialització de complements."}, {"la": "es", "text": ""}]}'
where ID = 7862;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Menjador social, Cooperativa consum i Secció local coop57."}, {"la": "es", "text": ""}]}'
where ID = 7863;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concepte, disseny, producció i muntatge d´exposicions."}, {"la": "es", "text": ""}]}'
where ID = 7865;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7866;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Roba de llit feta amb justícia a Costa d''Ivori i Burkina"}, {"la": "es", "text": ""}]}'
where ID = 7869;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Auditories Comptes i Subvencions - ROAC 12236."}, {"la": "es", "text": ""}]}'
where ID = 7870;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sistemes avançats d´energia solar tèrmica."}, {"la": "es", "text": ""}]}'
where ID = 7671;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparació i lloguer de bicicletes. Monitors temps lliure."}, {"la": "es", "text": ""}]}'
where ID = 7672;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció socio-laboral de persones amb discapacitats."}, {"la": "es", "text": ""}]}'
where ID = 7673;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperació Internacional."}, {"la": "es", "text": ""}]}'
where ID = 7674;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de les Finances Ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7675;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acció social, cultural i ambiental. Economia social."}, {"la": "es", "text": ""}]}'
where ID = 7676;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Micromecenatge de projectes cívics, col.laboratius i oberts."}, {"la": "es", "text": ""}]}'
where ID = 7677;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoure la innovació social."}, {"la": "es", "text": ""}]}'
where ID = 7678;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Investigació, difusió i suport campanyes."}, {"la": "es", "text": ""}]}'
where ID = 7679;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sensibilització per a la solidaritat i la cooperació."}, {"la": "es", "text": ""}]}'
where ID = 7680;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció social i laboral de persones amb malalties mentals"}, {"la": "es", "text": ""}]}'
where ID = 7681;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Centre, Ateneu democràtic i progressista és una entitat fundada l’any 1865 per les classes populars de Caldes de Montbui, en el bressol del moviment ateneístic de caire humanista de finals del segle XIX, amb l’objectiu de formar lliurepensadors/es per transformar una realitat injusta. Actualment, el Centre té el compromís de continuar amb la mateixa tasca mirant al segle XXI.\n\nEn l’actualitat, a l’Ateneu hi participen 10 entitats i seccions: el concurs de teatre Taca’m, la Coral del Centre, Jazz Sessions Club, Club d’Escacs Caldes, la Cobla Thermalenca, Scena Teatre, Calderins pel programari lliure, el Centre Sona, l’associació juvenil la Guspira i la Cooperativa de consum ecològic el Rusc. Totes aquestes entitats es coordinen a través de la Junta, l’òrgan de decisió del Centre.\n\nA més compta amb El cafè cultural del Centre, un espai que aposta per una oferta de productes de proximitat i de qualitat i amb una programació cultural estable per a totes les edats (de dimarts a diumenge a partir de les 15 h).\n\nEls objectius de l’Ateneu passen per enfortir el projecte associatiu i donar cabuda a d’altres iniciatives que puguin sorgir al municipi, així com generar recursos per acabar de rehabilitar l’edifici i així donar resposta a les necessitats de l’entitat."}, {"la": "es", "text": ""}]}'
where ID = 7682;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arquitectura, urbanisme, participació comunitaria i feminista."}, {"la": "es", "text": ""}]}'
where ID = 7683;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concedir préstecs sense interés per crear llocs de treball."}, {"la": "es", "text": ""}]}'
where ID = 7684;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Federació entitats. Gestió cultural i esportiva"}, {"la": "es", "text": ""}]}'
where ID = 7685;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació que promou l’Agroecologia i la Sobirania Alimentària incidint en els àmbits agroalimentari i del desenvolupament local. Les membres d’Arran de terra comptem amb una formació acadèmica interdisciplinar, que integra aportacions de les ciències naturals i les ciències socials, i amb experiència laboral en recerca, formació, educació, assessorament i dinamització d’iniciatives locals de transició agroecològica."}, {"la": "es", "text": ""}]}'
where ID = 7686;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i altres serveis a les empreses."}, {"la": "es", "text": ""}]}'
where ID = 7687;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Compromeses amb les dones, transformant la societat. l''empoderament és l''eix transversal que articula els àmbits d''acció de Surt, alhora que la filosofía que n''impregna les metodologies de treball"}, {"la": "es", "text": ""}]}'
where ID = 7688;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Participació ciutadana en processos de transformació urbana."}, {"la": "es", "text": ""}]}'
where ID = 7689;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny gràfic i Comunicació visual."}, {"la": "es", "text": ""}]}'
where ID = 7690;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TRACTA''M ÉS UNA COOPERATIVA DE FISIOTERAPEUTES\nNeix amb l’objectiu de crear un projecte on la prioritat és l’atenció personalitzada, directa, amb una visió global de la persona i el seu entorn.\nEstem dins de l’economia social i solidària. Entenent la realitat econòmica i social en la que estem vivint, volem donar facilitats a l’hora d’accedir a aquest servei sanitari amb tarifes assequibles per a tothom, sense perjudicar la qualitat del servei, tant a nivell professional com personal.\nEstem associades de forma cooperativa on no hi ha jerarquies entre les persones treballadores, es prenen les decisions de forma assembleària i amb total transparència. Totes les persones usuàries participen en el creixement de la cooperativa, fent-la sostenible i millorant-ne el servei amb els seus suggeriments."}, {"la": "es", "text": ""}]}'
where ID = 7691;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció i dinamització de fires, foment de la cooperació i associacionisme i serveis complementaris"}, {"la": "es", "text": ""}]}'
where ID = 7692;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de serveis financers islàmics. Finances ètiques i participatives."}, {"la": "es", "text": ""}]}'
where ID = 7693;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Restaurant, càtering, cuines col.lectivitats i infusions ecològiques"}, {"la": "es", "text": ""}]}'
where ID = 7694;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball que gestiona un comerç d''alimentació ecològica al barri de Gràcia"}, {"la": "es", "text": ""}]}'
where ID = 7695;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació privada el Maresme pro persones amb discapacitat intel·lectual és una entitat d''iniciativa social sense afany de lucre que promou i impulsa la integració social i la millora de la qualitat de vida de les persones amb discapacitat intel·lectual de la comarca del Maresme i de les seves famílies.\n\nLa Fundació procura donar resposta a les necessitats i demandes d''aquest col·lectiu organitzant una àmplia xarxa de serveis i centres que ofereixen una atenció amb continuïtat."}, {"la": "es", "text": ""}]}'
where ID = 7696;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ramat d''ovelles amb formatgeria pròpia"}, {"la": "es", "text": ""}]}'
where ID = 7697;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que té com a finalitat principal la reinserció sòcio laboral de persones amb disminució derivada de malaltia mental a partir de la gestió de residus i protecció al Medi Ambient. També portem a terme projectes de cooperació al desenvolupament dins l''àmbit sanitari"}, {"la": "es", "text": ""}]}'
where ID = 7698;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament de grups en l''autopromoció del seu cohabitatge"}, {"la": "es", "text": ""}]}'
where ID = 7699;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball per persones amb discapacitat derivat de malaltia mental. Comarca d''Osona."}, {"la": "es", "text": ""}]}'
where ID = 7700;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir persones afectades de FM,SFC, i altres SSC , mitjançant teràpies alternatives i xerrades informatives."}, {"la": "es", "text": ""}]}'
where ID = 7701;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Tac Osona és l''empresa social de Sant Tomàs, una entitat sense ànim de lucre que vetlla per millorar la qualitat de vida de les persones amb discapacitat intel·lectual i les seves famílies a la comarca d’Osona."}, {"la": "es", "text": ""}]}'
where ID = 7702;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treballem per fomentar el millor desenvolupament i salut mental de nens i adolescents de 0 a 18 anys."}, {"la": "es", "text": ""}]}'
where ID = 7703;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació, té per finalitat la de promoure i dur a terme la gestió, representació i administració de tot tipus de centres residencials, pisos protegits i llars residencials dedicades a l''atenció, asistència i acolliment de malalts psíquics i patologies assimilades."}, {"la": "es", "text": ""}]}'
where ID = 7704;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria/Consultoria Energètica especialitzada en eficiència energètica i energies renovables"}, {"la": "es", "text": ""}]}'
where ID = 7705;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Atenció a les persones amb problemes de salut mental i adiccions de la comarca d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7706;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de barri amb la finalitat de millorar les condicions de vida de les veïnes mijançant el foment de de projectes comunitaris i accions culturals"}, {"la": "es", "text": ""}]}'
where ID = 7707;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa sense ànim de lucre i d’iniciativa social. Treballem en projectes estratègics d’incidència política per a la promoció de l''equitat de gènere. Els nostres eixos de treball són: la comunicació amb perspectiva feminista, l''abordatge estratègic de les violències masclistes i la incorporació de la perspectiva de gènere en organitzacions i polítiques públiques. "}, {"la": "es", "text": ""}]}'
where ID = 7708;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“La Llera” és una cooperativa de consum sense ànim de lucre que treballa per a una nova mirada de l''educació cap als infants creant una estructura pedagògica que intenta respectar els seus ritmes, que permet mirar a l''infant amb confiança, on cada un pot trobar-se a ell/ella mateixa i desenvolupar-se segons les necessitats i interessos propis. "}, {"la": "es", "text": ""}]}'
where ID = 7709;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat juvenil del barri de Prosperitat. Gestiona el Casal de Joves de Prospe."}, {"la": "es", "text": ""}]}'
where ID = 7710;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense anim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7711;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“Contribuir, des del seu compromís ètic, mitjançant suports i oportunitats, a què cada persona amb paràlisi cerebral o altres discapacitats similars i la seva família pugui desenvolupar el seu projecte de qualitat de vida, així com a promoure la seva inclusió com a ciutadana de ple dret en una societat justa i solidària. L’Associació Esclat desenvolupa la seva activitat majoritàriament a Catalunya"}, {"la": "es", "text": ""}]}'
where ID = 7712;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense afany de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7713;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Realitzem serveis de formació, consultoria i recerca relacionats amb la participació ciutadana, la gestió comunitària i l''habitatge."}, {"la": "es", "text": ""}]}'
where ID = 7714;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de Desenvolupament Infantil i Atenció Precoç"}, {"la": "es", "text": ""}]}'
where ID = 7715;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Coopertiva enginyeria del món de l''energia"}, {"la": "es", "text": ""}]}'
where ID = 7716;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "serveis especialitzats d’inserció laboral pel col·lectiu de persones diagnosticades d’esclerosi múltiple i altres discapacitats fisiques i/o sensorials"}, {"la": "es", "text": ""}]}'
where ID = 7717;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa ambiental. Desenvolupament rural, estudis de biodiversitat i millora d''espais naturals, comunicació ambiental i ecoturisme"}, {"la": "es", "text": ""}]}'
where ID = 7718;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Situat a Barcelona, és el primer centre de referència de la mobilitat sostenible a l’Estat espanyol i estem especialitzats en el desenvolupament de la bicicleta com a mode de transport a la ciutat. A més de la mobilitat, l’associació de segon grau BiciHub és un projecte que també gira al voltant de l’Economia Social i Solidària (ESS). Apostem per la intercooperació i la gestió comunitària i cooperativa d’equipaments socials. En aquest sentit, volem formar part del patrimoni ciutadà i que els agents locals i el veïnat ens facin seus."}, {"la": "es", "text": ""}]}'
where ID = 7719;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Societat Orgànica+10"}, {"la": "es", "text": ""}]}'
where ID = 7720;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Afavorir la transformació social a partir de la Sobirania Alimentària amb perspectiva feminista"}, {"la": "es", "text": ""}]}'
where ID = 7721;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Sala d''Armes Montjuïc va ser fundada el 1977 pel Mestre italià Pierluigi Chicca, de manera que som el club d''esgrima més antic de Catalunya. Actualment som prop de 350 persones sòcies i ens sentim molt orgulloses de la nostra Escola Municipal d''Esgrima, per on cada temporada passen prop de 200 nens i nenes, que aprenen els secrets de l''esgrima i es formen com a esportistes i com a persones. Al club practiquem les tres modalitats d''esgrima esportiva: l''espasa, el floret i el sabre. A més, tenim grups d''esgrima escènica i d''esgrima històrica i una secció estable d''esgrima en cadira de rodes, amb les instal·lacions i vestidors adaptats i amb una àrea especialment preparader a la seva pràctica. Som un clua pb esportiu obert a tothom, i per això realitzem una aposta ferma pels cursos d''iniciació per a qualsevol edat, des dels 6 anys fins als 99! Més enllà de la vessant esportiva, el SAM és una institució amb una vocació cultural i social amb la voluntat d''esdevenir una comunitat implicada en la vida del barri i de la ciutat."}, {"la": "es", "text": ""}]}'
where ID = 7722;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació cultural sense ànime de lucre especialitzada en el teatre d''intervenció socials, el foment de la cultura i la formació teatral"}, {"la": "es", "text": ""}]}'
where ID = 7723;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7724;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte de roba personalitzada ambientalment sostenible i de producció ètica."}, {"la": "es", "text": ""}]}'
where ID = 7725;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa jurídica multidisciplinària compromesa amb la justícia social"}, {"la": "es", "text": ""}]}'
where ID = 7726;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat representativa del comerç i les empreses del barri de Trinitat Nova"}, {"la": "es", "text": ""}]}'
where ID = 7727;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''Habitatge"}, {"la": "es", "text": ""}]}'
where ID = 7728;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat d''atenció a persones amb paràlisi cerebral"}, {"la": "es", "text": ""}]}'
where ID = 7729;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball dedicat a la producció d''ous ecològics"}, {"la": "es", "text": ""}]}'
where ID = 7730;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "entitat esportiva que promou l''esport per  a persones amb discapacitat intel·lectual."}, {"la": "es", "text": ""}]}'
where ID = 7731;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Osona Ioga és el centre de referència a la comarca on diferents professionals unifiquem processos, eines i experiència en un projecte comú que posem al teu servei. La nostra missió és comprometre’ns amb tu i la teva salut oferint-te un espai de dedicació personal dins la teva vida quotidiana per a millorar el teu benestar i acompanyar-te en el procés d’aconseguir una millor qualitat de vida."}, {"la": "es", "text": ""}]}'
where ID = 7732;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Trabajamos por la inclusión de personas vulnerables. "}, {"la": "es", "text": ""}]}'
where ID = 7733;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Salut Mental Ponent és una entitat sense ànim de lucre i d’iniciativa social de les comarques de Lleida, registrada l’any 1993, creada en el seu origen per familiars i amics de persones amb algun trastorn mental, per tal de proporcionar orientació, informació, sensibilització, atenció social i un suport emocional i psicològic."}, {"la": "es", "text": ""}]}'
where ID = 7734;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa Feminista pel benestar de les persones en l''entorn laboral"}, {"la": "es", "text": ""}]}'
where ID = 7735;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions comunes i participades per a la millora del desenvolupament i la gestió col·lectiva del territori."}, {"la": "es", "text": ""}]}'
where ID = 7736;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7738;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7741;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7742;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació d''iniciativa social."}, {"la": "es", "text": ""}]}'
where ID = 7743;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "promoció de la salut sexual i la prevenció de riscos associa"}, {"la": "es", "text": ""}]}'
where ID = 7744;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d''iniciativa social que vetllem per potenciar l''autonomia i les oportunitats de les persones, tant a nivell individual com col•lectiu, amb la voluntat de contribuir a la transformació d''una societat més justa."}, {"la": "es", "text": ""}]}'
where ID = 7745;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7746;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "disseny"}, {"la": "es", "text": ""}]}'
where ID = 7747;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7748;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7749;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7750;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7751;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7752;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de menjadors escolars i educació del lleure."}, {"la": "es", "text": ""}]}'
where ID = 7753;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat excursionista"}, {"la": "es", "text": ""}]}'
where ID = 7754;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''infraestructures"}, {"la": "es", "text": ""}]}'
where ID = 7755;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Professionals en tècniques de salut per mèdis naturals"}, {"la": "es", "text": ""}]}'
where ID = 7756;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Introduccio a la cultura del treball col·lectius en exclusio"}, {"la": "es", "text": ""}]}'
where ID = 7757;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats educatives dirigides a dones i joves de Dakar."}, {"la": "es", "text": ""}]}'
where ID = 7758;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de mercat social."}, {"la": "es", "text": ""}]}'
where ID = 7759;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola de formació contínua del cooperativisme."}, {"la": "es", "text": ""}]}'
where ID = 7760;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció promoguda per PROMOCIONS i grup persones promotores"}, {"la": "es", "text": ""}]}'
where ID = 7761;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7762;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Energies renovables."}, {"la": "es", "text": ""}]}'
where ID = 7763;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai gastronòmic de l''Ateneu El Centre."}, {"la": "es", "text": ""}]}'
where ID = 7764;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7765;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arts grafiques i Venda de productes ecologics."}, {"la": "es", "text": ""}]}'
where ID = 7766;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma interassociativa d''entitats juvenils de BCN"}, {"la": "es", "text": ""}]}'
where ID = 7767;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de cluns de fitness"}, {"la": "es", "text": ""}]}'
where ID = 7768;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Clúster pel desenvolupament de l''economia social del treball. Està integrat per entitats socials sense ànim de lucre titulars d''un centre especial de treball (CET) o una empresa d''inserció (EI). "}, {"la": "es", "text": ""}]}'
where ID = 7769;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació excursionista."}, {"la": "es", "text": ""}]}'
where ID = 7770;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7771;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria i Consultoria Energètica"}, {"la": "es", "text": ""}]}'
where ID = 7772;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament a les cooperatives"}, {"la": "es", "text": ""}]}'
where ID = 7773;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i desenvolupament en ciències socials."}, {"la": "es", "text": ""}]}'
where ID = 7774;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Importador i distribuidor de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7775;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessorament i recerca per la sostenibilitat."}, {"la": "es", "text": ""}]}'
where ID = 7776;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció laboral mitjançant diverses activitats."}, {"la": "es", "text": ""}]}'
where ID = 7777;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ateneu social i cultural "}, {"la": "es", "text": ""}]}'
where ID = 7778;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluitar contra la pobresa i l''exclusió social."}, {"la": "es", "text": ""}]}'
where ID = 7779;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola d''infantil, primària i secundària."}, {"la": "es", "text": ""}]}'
where ID = 7780;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''edició i creació gràfica."}, {"la": "es", "text": ""}]}'
where ID = 7781;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treball social en inclusió sociolaboral i cohesió veïnal "}, {"la": "es", "text": ""}]}'
where ID = 7782;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir infants i families en risc d''exclusió social"}, {"la": "es", "text": ""}]}'
where ID = 7783;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació sense ànim de lucre amb fins socials"}, {"la": "es", "text": ""}]}'
where ID = 7784;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d´atenció a la persona."}, {"la": "es", "text": ""}]}'
where ID = 7785;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial i  acció cultural sociopolitica i econ. Som de Xes"}, {"la": "es", "text": ""}]}'
where ID = 7786;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers i Acompanyament processos."}, {"la": "es", "text": ""}]}'
where ID = 7787;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de formació per la inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7788;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria Social"}, {"la": "es", "text": ""}]}'
where ID = 7789;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa dedicada a l''assessorament empresarial dins dels àmbits de les vendes i el màrqueting, la gestió orientada en els valors i la formació d''emprenedoria i inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7790;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició publicació La Marea."}, {"la": "es", "text": ""}]}'
where ID = 7791;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Suport a les finances ètiques i a l''emprenedoria social."}, {"la": "es", "text": ""}]}'
where ID = 7792;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Aportem solucions integrals a tot tipus d''indústries mitjançant implantació i certificació de sistemes de Gestió ISO, assessorament i formació perquè aconsegueixin els més alts estàndards de qualitat exigits pels seus clients."}, {"la": "es", "text": ""}]}'
where ID = 7793;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de consum responsable"}, {"la": "es", "text": ""}]}'
where ID = 7794;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de promoció, difusió i distribució de Comerç Just"}, {"la": "es", "text": ""}]}'
where ID = 7795;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONGD"}, {"la": "es", "text": ""}]}'
where ID = 7796;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "MEDIACIÓ ASSEGURANCES PER A ENTITATS SOCIALS"}, {"la": "es", "text": ""}]}'
where ID = 7797;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "centre mèdic"}, {"la": "es", "text": ""}]}'
where ID = 7798;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Tot Raval és una a plataforma de 48 associacions, institucions, persones i empreses vinculades al Raval que es crea el 2002 amb un objectiu comú: la millora de la qualitat de vida en el barri. Es desenvolupa un treball comunitari, partint d’una visió integral del barri, que incideix en els àmbits social, cultural i econòmic i comercial."}, {"la": "es", "text": ""}]}'
where ID = 7799;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions integrals (sectors energètics i industrials)."}, {"la": "es", "text": ""}]}'
where ID = 7800;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció, distribució i venda de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7801;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "impulsar el desenvolupament i l''ús de la custòdia del territori com a eina de conservació."}, {"la": "es", "text": ""}]}'
where ID = 7802;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7803;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "DELTA gent activa es una entitat sense ànim de lucre, ONG/ONL, que treballant per i des de el sector social, es dirigeix cap a les persones grans i a les que sense ser-lo encara, tenen inquietuds a la vora d''aquest sector de la nostra societat i desitgen participar i/o col·laborar de forma ACTIVA per ajudar a la millora de la qualitat de vida del mateix i a la seva plena integració. "}, {"la": "es", "text": ""}]}'
where ID = 7804;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Administració de finques."}, {"la": "es", "text": ""}]}'
where ID = 7805;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que centre la seva activitat a la defensa de la llengua, la cultura, la cohesió social, l''educació i la identitat nacional de Catalunya. \n\n"}, {"la": "es", "text": ""}]}'
where ID = 7806;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "CERESNATURAL és una cooperativa que treballa per la salut a través de la venda de productes ecològics de proximitat i de la realització de teràpies naturals. La nostra motivació és oferir un espai que fomenti àmpliament la sostenibilitat, el comerç just, i la salut. Sent el nostre objectiu final el fet d’aconseguir que cadascú es faci responsable de la seva pròpia salut i la del planeta."}, {"la": "es", "text": ""}]}'
where ID = 7807;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de segon grau de restauració i dinamització social, que ofereix els seus serveis dins del Centre, Ateneu Democràtic i Progressista. Actualment està formada per set persones sòcies."}, {"la": "es", "text": ""}]}'
where ID = 7808;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de productes ecobiològics del barri del Congrés (Barcelona)"}, {"la": "es", "text": ""}]}'
where ID = 7809;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsar i elaborar els materials Escuracassoles."}, {"la": "es", "text": ""}]}'
where ID = 7810;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Ateneu Santcugatenc va ser creat l’any 1956 per un grup de ciutadans santcugatencs afeccionats al col·leccionisme, la numismàtica, la medallística i la filatèlia. L’any 1977 fou registrat com associació sense ànim de lucre amb la finalitat de fomentar també l’expansió artística, científica i/o la divulgació cultural, promovent el seu màxim desenvolupament i facilitant els coneixements que sobre aquestes matèries requereixin els seus associats/des. Amb el pas dels anys va anar incorporant als seus estatuts nous fins socials relacionats amb la cultura, l’associacionisme i el lleure, entre d’altres. \n\nDes de fa més de 15 anys desenvolupa un programa de cursos i tallers amb una bona acceptació per part de la ciutadania. L’entitat té la capacitat d’organitzar un programa trimestral de 70 disciplines diferents que posa a disposició de la població més de 1.400 places de formació i lleure. La xifra d’ocupació del darrer any 2015, només en aquest projecte, ha estat de 3.053 inscripcions. \n\nPel que fa l’àmbit de Cultura, a banda de tota la programació de cursos i tallers, realitza accions per al foment de la lectura, la gastronomia, la cultura popular, exposicions, concurs de pessebres, Festa de Tardor i, esporàdicament, Microespectacles entre d’altres accions. Així com activitats per a nens i nenes com casals d’estiu o vacances de Nadal.\n\nL’Ateneu realitza un seguit d’activitats en el camp de les polítiques socials que l’han fet mereixedor d’un conveni amb aquest àmbit de l’Ajuntament de Sant Cugat, per desenvolupar: l’Oficina del Voluntariat, l’Espai de Lleure (Lleure Social per a persones amb Trastorn Mental Sever), Joves Jubilats (envelliment actiu) i Català per a Tothom (projecte de foment de l’ús social del català i acollida sociocultural de persones nouvingudes). \n\nUn dels altres àmbits on ha tingut intervenció ha estat l’educatiu. Coordinant-se amb el Pla Educatiu d’Entorn dinamitza els Tallers d’Estudi Assistit per alumnes de secundaria de l’institut Arnau Cadell, el col·legi Pureza de Maria i l’Avenç, per lluitar contra el fracàs escolar, així com els grups de conversa i aprenentatge de català, directament a les escoles de primària, amb pares i mares de nens i nenes nouvinguts. \n\nO també activitats de sensibilització ambiental i promoció del consum responsable, ecològic, de proximitat i la reducció de residus, com el Mercat de Pagès, els Mercats de 2a mà o la sensibilització contra l’ús de bosses de plàstic.\n\nA més a més el fet d’haver pogut disposar fins ara de l’edifici de plaça Pep Ventura, 1, al nucli antic de la ciutat, obert a la ciutadania i a les entitats, permet gaudir d’espais de trobada i relació, exposicions, xerrades i conferències, presentacions de llibres, reunions o assajos. En aquests moment diferents entitats o col·lectius usen les instal·lacions de l’Ateneu per a desenvolupar les seves activitats de manera estable, com per exemple: Amics dels Veterans i la Penya Regalèssia, Comissió Festa de Tardor, Institut Trastorn Límit Fundació, Federació d’Associacions de Veïns, Grup de Jocs de Rol, Grup d’estudis de Teosofia, Pipa Club Sant Cugat, Societat Gastronòmica del Vallès. Alguns a més utilitzen la infraestructura i personal de l’Ateneu per a l’organització de les seves activitats, recepció d’usuaris i/o inscripcions.  A aquest cal sumar-hi els que de manera puntual utilitzen també la seu de l’Ateneu.\n"}, {"la": "es", "text": ""}]}'
where ID = 7811;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7812;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció audiovisual i Comunicació integral."}, {"la": "es", "text": ""}]}'
where ID = 7813;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lafede.cat està formada per 117 organitzacions i neix el gener de 2013 com a resultat de la fusió de les antigues Federació Catalana d’ONG per al Desenvolupament-FCONGD, Federació Catalana d’ONG pels Drets Humans-FCONGDH i Federació Catalana d’ONG per la Pau-FCONGPAU (vegeu història de Lafede.cat)Lafede.cat recull l’experiència i la trajectòria associativa de les seves 116 organitzacions federades però, a la vegada, aposta per la integració i l’actualització dels seus discursos, objectius i espais de relació institucional. Després d’un procés participatiu de mesos, a la seva primera Assemblea general, el 19 de juny de 2014."}, {"la": "es", "text": ""}]}'
where ID = 7814;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que va començar amb l''activitat principal d''apadrinar, adoptar i rescatar animals salvatges i de companyia. Arrel de l''experiència adquirida com a professionals del sector, a l''any 2012, FAADA va decidir iniciar un procés de transformació intern per poder incidir a les causes de forma més estratègica des de l''acció en l''àmbit social, legislatiu i educatiu. \nFacilitem informació i assessorament tècnic i jurídic a la societat sobre la protecció els animals i dirigim projectes de sensibilització que plantegin alternatives als sectors empresarials que directa o indirectament causen patiment als animals. Promovem accions formatives que incideixen en el foment de l''empatia i el respecte pels animals en els diferents cicles educatius. "}, {"la": "es", "text": ""}]}'
where ID = 7815;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny per emprenedoria social i iniciatives ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7816;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7817;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7818;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjançant la innovació narrativa, tecnològica i social, a COMPACTO posem la nostra creativitat al servei d’un món més lliure, solidari i participatiu. Des que vam fundar l’empresa el 2010, ens involucrem en projectes ambiciosos que transmetin els valors d’innovació, solidaritat, participació i llibertat, per enfortir l’economia social a través de la cultura i la cultura a través de l’economia social i col·laborativa."}, {"la": "es", "text": ""}]}'
where ID = 7819;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Distribució de productes ecològics, frescos i locals a les botigues i restaurants. Treballem  amb productors, cooperatives i entitats amb dificultats per fer arribar els productes als establiments oferint un servei global dins un àmbit local i creant xarxes de intercooperació local. Cafès La Chapolera és la creació pròpia de cafè ecològic d''origen amb valor social"}, {"la": "es", "text": ""}]}'
where ID = 7820;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La inserció laboral de persones en situació de risc i/o exclusió social, mitjançant la combinació de formació teòric-pràctic, social, personal i laboral de forma  que possibiliti l'' adquisició de hàbits i coneixements suficients per a poder accedir a un lloc de feina normalitzat.\nVenda de productes recicltas de segona mà i subsproductes relacionats amb els següents sectors: mobles, articles textils.."}, {"la": "es", "text": ""}]}'
where ID = 7821;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Portar a terme qualsevol activitats econòmiques de producció de béns, comercialització o de prestació de serveis que tinguin per finalitat primordial la de fer integració sociolaboral de .persones en situació o greu risc d''exclusió social.                                       "}, {"la": "es", "text": ""}]}'
where ID = 7822;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Panorama180 és una iniciativa sense ànim de lucre que es centra en el cinema i l’experimentació audiovisuals realitzats amb paràmetres del segle XXI: digitalització, xarxa, compromís, agitació, canvi, empoderament, comunitat…atentes a les novetats i propostes superadores dels problemes generats per la concepció obsoleta dels drets d’autor en l’era digital."}, {"la": "es", "text": ""}]}'
where ID = 7823;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i informació."}, {"la": "es", "text": ""}]}'
where ID = 7824;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Borda, és una cooperativa d''habitatge en cessió d''us, ubicada al barri de Sants de Barcelona. Un cop acabades les obres (a l''estiu 2018) tindrà 28 habitatges i varis espais comunitàris. Té signat un dret de superficie amb l''Ajuntament per 75 anys. Es construeix en sol públic ( i quedarà public) i té qualificació màxima d''eficiència energètica. L’edifici costarà tres milions d’euros (no arriba a 110.000 per habitatge) i es finançia amb l’aportació inicial dels socis, un préstec de Coop 57, l’emissió de títols participatius per part de la mateixa cooperativa i aportacions o préstecs d’altres entitats. "}, {"la": "es", "text": ""}]}'
where ID = 7825;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de projectes en l´àmbit de la Joventut."}, {"la": "es", "text": ""}]}'
where ID = 7826;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació"}, {"la": "es", "text": ""}]}'
where ID = 7827;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Matriu és un laboratori de democràcia viva, i ofereix formacions, acompanyament, recerca i creació de materials, amb la finalitat d''afavorir que persones i grups esdevinguin agents actius en la construcció de relacions i formes d’organització conscients i respectuoses.\nLa mirada dels feminismes; dóna eines per abordar la complexitat de cada una de les relacions i els grups, en què sovint actuen eixos diversos de desigualtat i opressió. Ens situa davant del repte d’atendre amb cura, a més, les dimensions múltiples de les relacions: emocions, experiències, somnis, posicionaments, coneixements, creences.\nEnfoca l’apoderament de grups i de persones; a través de generar processos de presa de consciència de la diversitat existent i de la distribució de poder que hi opera, com també de transformar els conflictes i establir mecanismes de diàleg i comunicació útils.\nAposta per la cerca de nous imaginaris polítics, noves formes d’organitzar-se, a través de concretar col·lectivament com es pot funcionar perquè les vides que vivim siguin dignes i sostenibles."}, {"la": "es", "text": ""}]}'
where ID = 7828;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ingeniería Social S.A.L. és una empresa d’economia cooperativa especialitzada en la formació, diagnòstic, definició, implementació i verificació de sistemes de gestió en Responsabilitat Social Corporativa. Ubicada a la ciutat de Barcelona, l’empresa compta amb un equip multidisciplinari format per consultores i expertes en matèria de responsabilitat social. \nLa nostra missió és assessorar a les empreses i organitzacions a desenvolupar negocis i organitzacions estables i sostenibles que aportin valor a la societat.\n"}, {"la": "es", "text": ""}]}'
where ID = 7829;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Andròmines te com a missió ser una organització de referència en la lluita i denuncia de l?exclusió social, defensant la dignitat i la igualtat d?oportunitats per totes les persones i la defensa i protecció del medi ambient.\n\nAquest darrer any Andròmines ha augmentat el dissenya i execució de programes de capacitació, formació i inserció laboral que s?adeqüen a les necessitats socials dels col?lectius i a la demanda real del mercat de treball. \n\nles empreses que contracten els serveis de gestió de residus, així com a les persones i organitzacions on arriben les campanyes d''educació ambiental i a les entitats amb les que realitzem treball en xarxa\n"}, {"la": "es", "text": ""}]}'
where ID = 7830;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Elaboració ecològica i autòctona de cerveses, most i maltes."}, {"la": "es", "text": ""}]}'
where ID = 7831;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "gdgfdgfd"}, {"la": "es", "text": ""}]}'
where ID = 7832;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7833;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7834;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7835;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7836;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball associat dedicada a la docència."}, {"la": "es", "text": ""}]}'
where ID = 7837;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA PAGE ORIGINAL, estudi de comunicació visual, es constitueix com empresa l’any 1991.\nEn l’actualitat, els seus socis Sònia Martínez Ruzafa i Josep Martínez Ruzafa (llicenciats en Belles Arts per la Universitat de Barcelona) dirigeixen projectes especialitzats en comunicació social, institucional i empresarial."}, {"la": "es", "text": ""}]}'
where ID = 7838;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7839;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7840;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació Vella Terra és una entitat sense ànim de lucre que neix amb el ferm compromís de promocionar l''autonomia de les persones depenents, i donar continuitat i sentit al seu projecte de vida. Els principals col·lectius atesos son persones grans, persones amb discapacitat, i persones tutelades."}, {"la": "es", "text": ""}]}'
where ID = 7841;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Instituto de Economía Social y Solidaria"}, {"la": "es", "text": ""}]}'
where ID = 7842;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7843;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de Servicios para el Mercado Social de Aragón"}, {"la": "es", "text": ""}]}'
where ID = 7844;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "eee"}, {"la": "es", "text": ""}]}'
where ID = 7845;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7846;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un agència de disseny i desenvolupament web i d’aplicacions al servei de la transformació social.Estem aquí per facilitar a les organitzacions la incorporació de la innovació tecnològica i ajudar-les a superar l''escletxa digital.Posem a l’abast de tothom el potencial que ofereix la tecnologia, adaptant-nos a les necessitats i possibilitats de cada projecte i acompanyant-lo amb un equip creatiu, multidisciplinar i proper que posa l’accent en l’accessibilitat, la pedagogia, el treball col·laboratiu i la perspectiva de gènere.Apostem per les tecnologies lliures com a instrument per assolir la sobirania tecnològica efectiva."}, {"la": "es", "text": ""}]}'
where ID = 7847;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7848;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestiona 19 habitatges dignes per a famílies en situació de vulnerabilitat i/o d''exclusió social. Acolliment i cessió d''ús dels habitatges per un període màxim de 3 anys."}, {"la": "es", "text": ""}]}'
where ID = 7849;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hosteleria mixta."}, {"la": "es", "text": ""}]}'
where ID = 7850;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 7851;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(la)baula: peça en forma d’anella que, enllaçada amb altres similars, forma una cadena.\n\nlabaula és una cooperativa d’arquitectes amb trajectòries professionals diverses que han decidit cooperar i sumar esforços per construir un projecte comú amb una metodologia de treball horitzontal.\n\nEl camp de treball de labaula consisteix en projectes d’edificació, rehabilitació, disseny industrial, urbanisme, espais públics, escenografia i espais efímers. Encarem els treballs amb idèntica dedicació i esforç, i amb independència de les seves dimensions o característiques. Aquests són entesos sempre com un espai de debat on s’incorporen col·laboradors d’altres disciplines per enriquir les perspectives pròpies.\n\nlabaula neix de la nostra il·lusió i inquietud per intentar fomentar la qualitat, el rigor i la responsabilitat en la nostra feina. Esperem poder ser una plataforma des d’on seguir treballant en aquesta direcció."}, {"la": "es", "text": ""}]}'
where ID = 7852;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7853;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fabricació de motlles i matrius per automoció."}, {"la": "es", "text": ""}]}'
where ID = 7854;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Teixits amb tellers manuals."}, {"la": "es", "text": ""}]}'
where ID = 7855;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió veïnal de barri."}, {"la": "es", "text": ""}]}'
where ID = 7856;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocions d´iniciatives socials i económiques."}, {"la": "es", "text": ""}]}'
where ID = 7857;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social."}, {"la": "es", "text": ""}]}'
where ID = 7858;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers."}, {"la": "es", "text": ""}]}'
where ID = 7859;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparacions i reformes."}, {"la": "es", "text": ""}]}'
where ID = 7860;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comercialització de complements."}, {"la": "es", "text": ""}]}'
where ID = 7862;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Menjador social, Cooperativa consum i Secció local coop57."}, {"la": "es", "text": ""}]}'
where ID = 7863;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concepte, disseny, producció i muntatge d´exposicions."}, {"la": "es", "text": ""}]}'
where ID = 7865;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7866;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Roba de llit feta amb justícia a Costa d''Ivori i Burkina"}, {"la": "es", "text": ""}]}'
where ID = 7869;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Auditories Comptes i Subvencions - ROAC 12236."}, {"la": "es", "text": ""}]}'
where ID = 7870;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sistemes avançats d´energia solar tèrmica."}, {"la": "es", "text": ""}]}'
where ID = 7671;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparació i lloguer de bicicletes. Monitors temps lliure."}, {"la": "es", "text": ""}]}'
where ID = 7672;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció socio-laboral de persones amb discapacitats."}, {"la": "es", "text": ""}]}'
where ID = 7673;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperació Internacional."}, {"la": "es", "text": ""}]}'
where ID = 7674;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de les Finances Ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7675;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acció social, cultural i ambiental. Economia social."}, {"la": "es", "text": ""}]}'
where ID = 7676;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Micromecenatge de projectes cívics, col.laboratius i oberts."}, {"la": "es", "text": ""}]}'
where ID = 7677;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoure la innovació social."}, {"la": "es", "text": ""}]}'
where ID = 7678;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Investigació, difusió i suport campanyes."}, {"la": "es", "text": ""}]}'
where ID = 7679;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sensibilització per a la solidaritat i la cooperació."}, {"la": "es", "text": ""}]}'
where ID = 7680;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció social i laboral de persones amb malalties mentals"}, {"la": "es", "text": ""}]}'
where ID = 7681;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Centre, Ateneu democràtic i progressista és una entitat fundada l’any 1865 per les classes populars de Caldes de Montbui, en el bressol del moviment ateneístic de caire humanista de finals del segle XIX, amb l’objectiu de formar lliurepensadors/es per transformar una realitat injusta. Actualment, el Centre té el compromís de continuar amb la mateixa tasca mirant al segle XXI.\n\nEn l’actualitat, a l’Ateneu hi participen 10 entitats i seccions: el concurs de teatre Taca’m, la Coral del Centre, Jazz Sessions Club, Club d’Escacs Caldes, la Cobla Thermalenca, Scena Teatre, Calderins pel programari lliure, el Centre Sona, l’associació juvenil la Guspira i la Cooperativa de consum ecològic el Rusc. Totes aquestes entitats es coordinen a través de la Junta, l’òrgan de decisió del Centre.\n\nA més compta amb El cafè cultural del Centre, un espai que aposta per una oferta de productes de proximitat i de qualitat i amb una programació cultural estable per a totes les edats (de dimarts a diumenge a partir de les 15 h).\n\nEls objectius de l’Ateneu passen per enfortir el projecte associatiu i donar cabuda a d’altres iniciatives que puguin sorgir al municipi, així com generar recursos per acabar de rehabilitar l’edifici i així donar resposta a les necessitats de l’entitat."}, {"la": "es", "text": ""}]}'
where ID = 7682;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arquitectura, urbanisme, participació comunitaria i feminista."}, {"la": "es", "text": ""}]}'
where ID = 7683;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concedir préstecs sense interés per crear llocs de treball."}, {"la": "es", "text": ""}]}'
where ID = 7684;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Federació entitats. Gestió cultural i esportiva"}, {"la": "es", "text": ""}]}'
where ID = 7685;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació que promou l’Agroecologia i la Sobirania Alimentària incidint en els àmbits agroalimentari i del desenvolupament local. Les membres d’Arran de terra comptem amb una formació acadèmica interdisciplinar, que integra aportacions de les ciències naturals i les ciències socials, i amb experiència laboral en recerca, formació, educació, assessorament i dinamització d’iniciatives locals de transició agroecològica."}, {"la": "es", "text": ""}]}'
where ID = 7686;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i altres serveis a les empreses."}, {"la": "es", "text": ""}]}'
where ID = 7687;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Compromeses amb les dones, transformant la societat. l''empoderament és l''eix transversal que articula els àmbits d''acció de Surt, alhora que la filosofía que n''impregna les metodologies de treball"}, {"la": "es", "text": ""}]}'
where ID = 7688;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Participació ciutadana en processos de transformació urbana."}, {"la": "es", "text": ""}]}'
where ID = 7689;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny gràfic i Comunicació visual."}, {"la": "es", "text": ""}]}'
where ID = 7690;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TRACTA''M ÉS UNA COOPERATIVA DE FISIOTERAPEUTES\nNeix amb l’objectiu de crear un projecte on la prioritat és l’atenció personalitzada, directa, amb una visió global de la persona i el seu entorn.\nEstem dins de l’economia social i solidària. Entenent la realitat econòmica i social en la que estem vivint, volem donar facilitats a l’hora d’accedir a aquest servei sanitari amb tarifes assequibles per a tothom, sense perjudicar la qualitat del servei, tant a nivell professional com personal.\nEstem associades de forma cooperativa on no hi ha jerarquies entre les persones treballadores, es prenen les decisions de forma assembleària i amb total transparència. Totes les persones usuàries participen en el creixement de la cooperativa, fent-la sostenible i millorant-ne el servei amb els seus suggeriments."}, {"la": "es", "text": ""}]}'
where ID = 7691;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció i dinamització de fires, foment de la cooperació i associacionisme i serveis complementaris"}, {"la": "es", "text": ""}]}'
where ID = 7692;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de serveis financers islàmics. Finances ètiques i participatives."}, {"la": "es", "text": ""}]}'
where ID = 7693;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Restaurant, càtering, cuines col.lectivitats i infusions ecològiques"}, {"la": "es", "text": ""}]}'
where ID = 7694;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball que gestiona un comerç d''alimentació ecològica al barri de Gràcia"}, {"la": "es", "text": ""}]}'
where ID = 7695;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació privada el Maresme pro persones amb discapacitat intel·lectual és una entitat d''iniciativa social sense afany de lucre que promou i impulsa la integració social i la millora de la qualitat de vida de les persones amb discapacitat intel·lectual de la comarca del Maresme i de les seves famílies.\n\nLa Fundació procura donar resposta a les necessitats i demandes d''aquest col·lectiu organitzant una àmplia xarxa de serveis i centres que ofereixen una atenció amb continuïtat."}, {"la": "es", "text": ""}]}'
where ID = 7696;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ramat d''ovelles amb formatgeria pròpia"}, {"la": "es", "text": ""}]}'
where ID = 7697;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que té com a finalitat principal la reinserció sòcio laboral de persones amb disminució derivada de malaltia mental a partir de la gestió de residus i protecció al Medi Ambient. També portem a terme projectes de cooperació al desenvolupament dins l''àmbit sanitari"}, {"la": "es", "text": ""}]}'
where ID = 7698;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament de grups en l''autopromoció del seu cohabitatge"}, {"la": "es", "text": ""}]}'
where ID = 7699;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball per persones amb discapacitat derivat de malaltia mental. Comarca d''Osona."}, {"la": "es", "text": ""}]}'
where ID = 7700;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir persones afectades de FM,SFC, i altres SSC , mitjançant teràpies alternatives i xerrades informatives."}, {"la": "es", "text": ""}]}'
where ID = 7701;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Tac Osona és l''empresa social de Sant Tomàs, una entitat sense ànim de lucre que vetlla per millorar la qualitat de vida de les persones amb discapacitat intel·lectual i les seves famílies a la comarca d’Osona."}, {"la": "es", "text": ""}]}'
where ID = 7702;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treballem per fomentar el millor desenvolupament i salut mental de nens i adolescents de 0 a 18 anys."}, {"la": "es", "text": ""}]}'
where ID = 7703;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació, té per finalitat la de promoure i dur a terme la gestió, representació i administració de tot tipus de centres residencials, pisos protegits i llars residencials dedicades a l''atenció, asistència i acolliment de malalts psíquics i patologies assimilades."}, {"la": "es", "text": ""}]}'
where ID = 7704;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria/Consultoria Energètica especialitzada en eficiència energètica i energies renovables"}, {"la": "es", "text": ""}]}'
where ID = 7705;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Atenció a les persones amb problemes de salut mental i adiccions de la comarca d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7706;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de barri amb la finalitat de millorar les condicions de vida de les veïnes mijançant el foment de de projectes comunitaris i accions culturals"}, {"la": "es", "text": ""}]}'
where ID = 7707;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa sense ànim de lucre i d’iniciativa social. Treballem en projectes estratègics d’incidència política per a la promoció de l''equitat de gènere. Els nostres eixos de treball són: la comunicació amb perspectiva feminista, l''abordatge estratègic de les violències masclistes i la incorporació de la perspectiva de gènere en organitzacions i polítiques públiques. "}, {"la": "es", "text": ""}]}'
where ID = 7708;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“La Llera” és una cooperativa de consum sense ànim de lucre que treballa per a una nova mirada de l''educació cap als infants creant una estructura pedagògica que intenta respectar els seus ritmes, que permet mirar a l''infant amb confiança, on cada un pot trobar-se a ell/ella mateixa i desenvolupar-se segons les necessitats i interessos propis. "}, {"la": "es", "text": ""}]}'
where ID = 7709;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat juvenil del barri de Prosperitat. Gestiona el Casal de Joves de Prospe."}, {"la": "es", "text": ""}]}'
where ID = 7710;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense anim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7711;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“Contribuir, des del seu compromís ètic, mitjançant suports i oportunitats, a què cada persona amb paràlisi cerebral o altres discapacitats similars i la seva família pugui desenvolupar el seu projecte de qualitat de vida, així com a promoure la seva inclusió com a ciutadana de ple dret en una societat justa i solidària. L’Associació Esclat desenvolupa la seva activitat majoritàriament a Catalunya"}, {"la": "es", "text": ""}]}'
where ID = 7712;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense afany de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7713;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Realitzem serveis de formació, consultoria i recerca relacionats amb la participació ciutadana, la gestió comunitària i l''habitatge."}, {"la": "es", "text": ""}]}'
where ID = 7714;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de Desenvolupament Infantil i Atenció Precoç"}, {"la": "es", "text": ""}]}'
where ID = 7715;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Coopertiva enginyeria del món de l''energia"}, {"la": "es", "text": ""}]}'
where ID = 7716;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "serveis especialitzats d’inserció laboral pel col·lectiu de persones diagnosticades d’esclerosi múltiple i altres discapacitats fisiques i/o sensorials"}, {"la": "es", "text": ""}]}'
where ID = 7717;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa ambiental. Desenvolupament rural, estudis de biodiversitat i millora d''espais naturals, comunicació ambiental i ecoturisme"}, {"la": "es", "text": ""}]}'
where ID = 7718;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Situat a Barcelona, és el primer centre de referència de la mobilitat sostenible a l’Estat espanyol i estem especialitzats en el desenvolupament de la bicicleta com a mode de transport a la ciutat. A més de la mobilitat, l’associació de segon grau BiciHub és un projecte que també gira al voltant de l’Economia Social i Solidària (ESS). Apostem per la intercooperació i la gestió comunitària i cooperativa d’equipaments socials. En aquest sentit, volem formar part del patrimoni ciutadà i que els agents locals i el veïnat ens facin seus."}, {"la": "es", "text": ""}]}'
where ID = 7719;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Societat Orgànica+10"}, {"la": "es", "text": ""}]}'
where ID = 7720;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Afavorir la transformació social a partir de la Sobirania Alimentària amb perspectiva feminista"}, {"la": "es", "text": ""}]}'
where ID = 7721;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació cultural sense ànime de lucre especialitzada en el teatre d''intervenció socials, el foment de la cultura i la formació teatral"}, {"la": "es", "text": ""}]}'
where ID = 7723;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7724;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte de roba personalitzada ambientalment sostenible i de producció ètica."}, {"la": "es", "text": ""}]}'
where ID = 7725;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa jurídica multidisciplinària compromesa amb la justícia social"}, {"la": "es", "text": ""}]}'
where ID = 7726;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat representativa del comerç i les empreses del barri de Trinitat Nova"}, {"la": "es", "text": ""}]}'
where ID = 7727;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''Habitatge"}, {"la": "es", "text": ""}]}'
where ID = 7728;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat d''atenció a persones amb paràlisi cerebral"}, {"la": "es", "text": ""}]}'
where ID = 7729;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball dedicat a la producció d''ous ecològics"}, {"la": "es", "text": ""}]}'
where ID = 7730;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "entitat esportiva que promou l''esport per  a persones amb discapacitat intel·lectual."}, {"la": "es", "text": ""}]}'
where ID = 7731;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Osona Ioga és el centre de referència a la comarca on diferents professionals unifiquem processos, eines i experiència en un projecte comú que posem al teu servei. La nostra missió és comprometre’ns amb tu i la teva salut oferint-te un espai de dedicació personal dins la teva vida quotidiana per a millorar el teu benestar i acompanyar-te en el procés d’aconseguir una millor qualitat de vida."}, {"la": "es", "text": ""}]}'
where ID = 7732;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Trabajamos por la inclusión de personas vulnerables. "}, {"la": "es", "text": ""}]}'
where ID = 7733;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Salut Mental Ponent és una entitat sense ànim de lucre i d’iniciativa social de les comarques de Lleida, registrada l’any 1993, creada en el seu origen per familiars i amics de persones amb algun trastorn mental, per tal de proporcionar orientació, informació, sensibilització, atenció social i un suport emocional i psicològic."}, {"la": "es", "text": ""}]}'
where ID = 7734;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa Feminista pel benestar de les persones en l''entorn laboral"}, {"la": "es", "text": ""}]}'
where ID = 7735;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions comunes i participades per a la millora del desenvolupament i la gestió col·lectiva del territori."}, {"la": "es", "text": ""}]}'
where ID = 7736;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7738;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7741;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7742;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació d''iniciativa social."}, {"la": "es", "text": ""}]}'
where ID = 7743;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "promoció de la salut sexual i la prevenció de riscos associa"}, {"la": "es", "text": ""}]}'
where ID = 7744;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d''iniciativa social que vetllem per potenciar l''autonomia i les oportunitats de les persones, tant a nivell individual com col•lectiu, amb la voluntat de contribuir a la transformació d''una societat més justa."}, {"la": "es", "text": ""}]}'
where ID = 7745;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7746;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "disseny"}, {"la": "es", "text": ""}]}'
where ID = 7747;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7748;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7749;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7750;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7751;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7752;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de menjadors escolars i educació del lleure."}, {"la": "es", "text": ""}]}'
where ID = 7753;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat excursionista"}, {"la": "es", "text": ""}]}'
where ID = 7754;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''infraestructures"}, {"la": "es", "text": ""}]}'
where ID = 7755;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Professionals en tècniques de salut per mèdis naturals"}, {"la": "es", "text": ""}]}'
where ID = 7756;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Introduccio a la cultura del treball col·lectius en exclusio"}, {"la": "es", "text": ""}]}'
where ID = 7757;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats educatives dirigides a dones i joves de Dakar."}, {"la": "es", "text": ""}]}'
where ID = 7758;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de mercat social."}, {"la": "es", "text": ""}]}'
where ID = 7759;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola de formació contínua del cooperativisme."}, {"la": "es", "text": ""}]}'
where ID = 7760;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció promoguda per PROMOCIONS i grup persones promotores"}, {"la": "es", "text": ""}]}'
where ID = 7761;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7762;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Energies renovables."}, {"la": "es", "text": ""}]}'
where ID = 7763;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai gastronòmic de l''Ateneu El Centre."}, {"la": "es", "text": ""}]}'
where ID = 7764;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7765;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arts grafiques i Venda de productes ecologics."}, {"la": "es", "text": ""}]}'
where ID = 7766;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma interassociativa d''entitats juvenils de BCN"}, {"la": "es", "text": ""}]}'
where ID = 7767;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de cluns de fitness"}, {"la": "es", "text": ""}]}'
where ID = 7768;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Clúster pel desenvolupament de l''economia social del treball. Està integrat per entitats socials sense ànim de lucre titulars d''un centre especial de treball (CET) o una empresa d''inserció (EI). "}, {"la": "es", "text": ""}]}'
where ID = 7769;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació excursionista."}, {"la": "es", "text": ""}]}'
where ID = 7770;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7771;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria i Consultoria Energètica"}, {"la": "es", "text": ""}]}'
where ID = 7772;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament a les cooperatives"}, {"la": "es", "text": ""}]}'
where ID = 7773;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i desenvolupament en ciències socials."}, {"la": "es", "text": ""}]}'
where ID = 7774;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Importador i distribuidor de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7775;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessorament i recerca per la sostenibilitat."}, {"la": "es", "text": ""}]}'
where ID = 7776;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció laboral mitjançant diverses activitats."}, {"la": "es", "text": ""}]}'
where ID = 7777;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ateneu social i cultural "}, {"la": "es", "text": ""}]}'
where ID = 7778;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluitar contra la pobresa i l''exclusió social."}, {"la": "es", "text": ""}]}'
where ID = 7779;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola d''infantil, primària i secundària."}, {"la": "es", "text": ""}]}'
where ID = 7780;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''edició i creació gràfica."}, {"la": "es", "text": ""}]}'
where ID = 7781;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treball social en inclusió sociolaboral i cohesió veïnal "}, {"la": "es", "text": ""}]}'
where ID = 7782;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir infants i families en risc d''exclusió social"}, {"la": "es", "text": ""}]}'
where ID = 7783;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació sense ànim de lucre amb fins socials"}, {"la": "es", "text": ""}]}'
where ID = 7784;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d´atenció a la persona."}, {"la": "es", "text": ""}]}'
where ID = 7785;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial i  acció cultural sociopolitica i econ. Som de Xes"}, {"la": "es", "text": ""}]}'
where ID = 7786;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers i Acompanyament processos."}, {"la": "es", "text": ""}]}'
where ID = 7787;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de formació per la inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7788;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria Social"}, {"la": "es", "text": ""}]}'
where ID = 7789;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa dedicada a l''assessorament empresarial dins dels àmbits de les vendes i el màrqueting, la gestió orientada en els valors i la formació d''emprenedoria i inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7790;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició publicació La Marea."}, {"la": "es", "text": ""}]}'
where ID = 7791;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Suport a les finances ètiques i a l''emprenedoria social."}, {"la": "es", "text": ""}]}'
where ID = 7792;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Aportem solucions integrals a tot tipus d''indústries mitjançant implantació i certificació de sistemes de Gestió ISO, assessorament i formació perquè aconsegueixin els més alts estàndards de qualitat exigits pels seus clients."}, {"la": "es", "text": ""}]}'
where ID = 7793;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de consum responsable"}, {"la": "es", "text": ""}]}'
where ID = 7794;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de promoció, difusió i distribució de Comerç Just"}, {"la": "es", "text": ""}]}'
where ID = 7795;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONGD"}, {"la": "es", "text": ""}]}'
where ID = 7796;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "MEDIACIÓ ASSEGURANCES PER A ENTITATS SOCIALS"}, {"la": "es", "text": ""}]}'
where ID = 7797;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "centre mèdic"}, {"la": "es", "text": ""}]}'
where ID = 7798;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Tot Raval és una a plataforma de 48 associacions, institucions, persones i empreses vinculades al Raval que es crea el 2002 amb un objectiu comú: la millora de la qualitat de vida en el barri. Es desenvolupa un treball comunitari, partint d’una visió integral del barri, que incideix en els àmbits social, cultural i econòmic i comercial."}, {"la": "es", "text": ""}]}'
where ID = 7799;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions integrals (sectors energètics i industrials)."}, {"la": "es", "text": ""}]}'
where ID = 7800;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció, distribució i venda de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7801;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "impulsar el desenvolupament i l''ús de la custòdia del territori com a eina de conservació."}, {"la": "es", "text": ""}]}'
where ID = 7802;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7803;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "DELTA gent activa es una entitat sense ànim de lucre, ONG/ONL, que treballant per i des de el sector social, es dirigeix cap a les persones grans i a les que sense ser-lo encara, tenen inquietuds a la vora d''aquest sector de la nostra societat i desitgen participar i/o col·laborar de forma ACTIVA per ajudar a la millora de la qualitat de vida del mateix i a la seva plena integració. "}, {"la": "es", "text": ""}]}'
where ID = 7804;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Administració de finques."}, {"la": "es", "text": ""}]}'
where ID = 7805;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que centre la seva activitat a la defensa de la llengua, la cultura, la cohesió social, l''educació i la identitat nacional de Catalunya. \n\n"}, {"la": "es", "text": ""}]}'
where ID = 7806;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "CERESNATURAL és una cooperativa que treballa per la salut a través de la venda de productes ecològics de proximitat i de la realització de teràpies naturals. La nostra motivació és oferir un espai que fomenti àmpliament la sostenibilitat, el comerç just, i la salut. Sent el nostre objectiu final el fet d’aconseguir que cadascú es faci responsable de la seva pròpia salut i la del planeta."}, {"la": "es", "text": ""}]}'
where ID = 7807;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de segon grau de restauració i dinamització social, que ofereix els seus serveis dins del Centre, Ateneu Democràtic i Progressista. Actualment està formada per set persones sòcies."}, {"la": "es", "text": ""}]}'
where ID = 7808;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de productes ecobiològics del barri del Congrés (Barcelona)"}, {"la": "es", "text": ""}]}'
where ID = 7809;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsar i elaborar els materials Escuracassoles."}, {"la": "es", "text": ""}]}'
where ID = 7810;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Ateneu Santcugatenc va ser creat l’any 1956 per un grup de ciutadans santcugatencs afeccionats al col·leccionisme, la numismàtica, la medallística i la filatèlia. L’any 1977 fou registrat com associació sense ànim de lucre amb la finalitat de fomentar també l’expansió artística, científica i/o la divulgació cultural, promovent el seu màxim desenvolupament i facilitant els coneixements que sobre aquestes matèries requereixin els seus associats/des. Amb el pas dels anys va anar incorporant als seus estatuts nous fins socials relacionats amb la cultura, l’associacionisme i el lleure, entre d’altres. \n\nDes de fa més de 15 anys desenvolupa un programa de cursos i tallers amb una bona acceptació per part de la ciutadania. L’entitat té la capacitat d’organitzar un programa trimestral de 70 disciplines diferents que posa a disposició de la població més de 1.400 places de formació i lleure. La xifra d’ocupació del darrer any 2015, només en aquest projecte, ha estat de 3.053 inscripcions. \n\nPel que fa l’àmbit de Cultura, a banda de tota la programació de cursos i tallers, realitza accions per al foment de la lectura, la gastronomia, la cultura popular, exposicions, concurs de pessebres, Festa de Tardor i, esporàdicament, Microespectacles entre d’altres accions. Així com activitats per a nens i nenes com casals d’estiu o vacances de Nadal.\n\nL’Ateneu realitza un seguit d’activitats en el camp de les polítiques socials que l’han fet mereixedor d’un conveni amb aquest àmbit de l’Ajuntament de Sant Cugat, per desenvolupar: l’Oficina del Voluntariat, l’Espai de Lleure (Lleure Social per a persones amb Trastorn Mental Sever), Joves Jubilats (envelliment actiu) i Català per a Tothom (projecte de foment de l’ús social del català i acollida sociocultural de persones nouvingudes). \n\nUn dels altres àmbits on ha tingut intervenció ha estat l’educatiu. Coordinant-se amb el Pla Educatiu d’Entorn dinamitza els Tallers d’Estudi Assistit per alumnes de secundaria de l’institut Arnau Cadell, el col·legi Pureza de Maria i l’Avenç, per lluitar contra el fracàs escolar, així com els grups de conversa i aprenentatge de català, directament a les escoles de primària, amb pares i mares de nens i nenes nouvinguts. \n\nO també activitats de sensibilització ambiental i promoció del consum responsable, ecològic, de proximitat i la reducció de residus, com el Mercat de Pagès, els Mercats de 2a mà o la sensibilització contra l’ús de bosses de plàstic.\n\nA més a més el fet d’haver pogut disposar fins ara de l’edifici de plaça Pep Ventura, 1, al nucli antic de la ciutat, obert a la ciutadania i a les entitats, permet gaudir d’espais de trobada i relació, exposicions, xerrades i conferències, presentacions de llibres, reunions o assajos. En aquests moment diferents entitats o col·lectius usen les instal·lacions de l’Ateneu per a desenvolupar les seves activitats de manera estable, com per exemple: Amics dels Veterans i la Penya Regalèssia, Comissió Festa de Tardor, Institut Trastorn Límit Fundació, Federació d’Associacions de Veïns, Grup de Jocs de Rol, Grup d’estudis de Teosofia, Pipa Club Sant Cugat, Societat Gastronòmica del Vallès. Alguns a més utilitzen la infraestructura i personal de l’Ateneu per a l’organització de les seves activitats, recepció d’usuaris i/o inscripcions.  A aquest cal sumar-hi els que de manera puntual utilitzen també la seu de l’Ateneu.\n"}, {"la": "es", "text": ""}]}'
where ID = 7811;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7812;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció audiovisual i Comunicació integral."}, {"la": "es", "text": ""}]}'
where ID = 7813;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lafede.cat està formada per 117 organitzacions i neix el gener de 2013 com a resultat de la fusió de les antigues Federació Catalana d’ONG per al Desenvolupament-FCONGD, Federació Catalana d’ONG pels Drets Humans-FCONGDH i Federació Catalana d’ONG per la Pau-FCONGPAU (vegeu història de Lafede.cat)Lafede.cat recull l’experiència i la trajectòria associativa de les seves 116 organitzacions federades però, a la vegada, aposta per la integració i l’actualització dels seus discursos, objectius i espais de relació institucional. Després d’un procés participatiu de mesos, a la seva primera Assemblea general, el 19 de juny de 2014."}, {"la": "es", "text": ""}]}'
where ID = 7814;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que va començar amb l''activitat principal d''apadrinar, adoptar i rescatar animals salvatges i de companyia. Arrel de l''experiència adquirida com a professionals del sector, a l''any 2012, FAADA va decidir iniciar un procés de transformació intern per poder incidir a les causes de forma més estratègica des de l''acció en l''àmbit social, legislatiu i educatiu. \nFacilitem informació i assessorament tècnic i jurídic a la societat sobre la protecció els animals i dirigim projectes de sensibilització que plantegin alternatives als sectors empresarials que directa o indirectament causen patiment als animals. Promovem accions formatives que incideixen en el foment de l''empatia i el respecte pels animals en els diferents cicles educatius. "}, {"la": "es", "text": ""}]}'
where ID = 7815;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny per emprenedoria social i iniciatives ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7816;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7817;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7818;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjançant la innovació narrativa, tecnològica i social, a COMPACTO posem la nostra creativitat al servei d’un món més lliure, solidari i participatiu. Des que vam fundar l’empresa el 2010, ens involucrem en projectes ambiciosos que transmetin els valors d’innovació, solidaritat, participació i llibertat, per enfortir l’economia social a través de la cultura i la cultura a través de l’economia social i col·laborativa."}, {"la": "es", "text": ""}]}'
where ID = 7819;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Distribució de productes ecològics, frescos i locals a les botigues i restaurants. Treballem  amb productors, cooperatives i entitats amb dificultats per fer arribar els productes als establiments oferint un servei global dins un àmbit local i creant xarxes de intercooperació local. Cafès La Chapolera és la creació pròpia de cafè ecològic d''origen amb valor social"}, {"la": "es", "text": ""}]}'
where ID = 7820;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La inserció laboral de persones en situació de risc i/o exclusió social, mitjançant la combinació de formació teòric-pràctic, social, personal i laboral de forma  que possibiliti l'' adquisició de hàbits i coneixements suficients per a poder accedir a un lloc de feina normalitzat.\nVenda de productes recicltas de segona mà i subsproductes relacionats amb els següents sectors: mobles, articles textils.."}, {"la": "es", "text": ""}]}'
where ID = 7821;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Portar a terme qualsevol activitats econòmiques de producció de béns, comercialització o de prestació de serveis que tinguin per finalitat primordial la de fer integració sociolaboral de .persones en situació o greu risc d''exclusió social.                                       "}, {"la": "es", "text": ""}]}'
where ID = 7822;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Panorama180 és una iniciativa sense ànim de lucre que es centra en el cinema i l’experimentació audiovisuals realitzats amb paràmetres del segle XXI: digitalització, xarxa, compromís, agitació, canvi, empoderament, comunitat…atentes a les novetats i propostes superadores dels problemes generats per la concepció obsoleta dels drets d’autor en l’era digital."}, {"la": "es", "text": ""}]}'
where ID = 7823;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i informació."}, {"la": "es", "text": ""}]}'
where ID = 7824;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Borda, és una cooperativa d''habitatge en cessió d''us, ubicada al barri de Sants de Barcelona. Un cop acabades les obres (a l''estiu 2018) tindrà 28 habitatges i varis espais comunitàris. Té signat un dret de superficie amb l''Ajuntament per 75 anys. Es construeix en sol públic ( i quedarà public) i té qualificació màxima d''eficiència energètica. L’edifici costarà tres milions d’euros (no arriba a 110.000 per habitatge) i es finançia amb l’aportació inicial dels socis, un préstec de Coop 57, l’emissió de títols participatius per part de la mateixa cooperativa i aportacions o préstecs d’altres entitats. "}, {"la": "es", "text": ""}]}'
where ID = 7825;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de projectes en l´àmbit de la Joventut."}, {"la": "es", "text": ""}]}'
where ID = 7826;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació"}, {"la": "es", "text": ""}]}'
where ID = 7827;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Matriu és un laboratori de democràcia viva, i ofereix formacions, acompanyament, recerca i creació de materials, amb la finalitat d''afavorir que persones i grups esdevinguin agents actius en la construcció de relacions i formes d’organització conscients i respectuoses.\nLa mirada dels feminismes; dóna eines per abordar la complexitat de cada una de les relacions i els grups, en què sovint actuen eixos diversos de desigualtat i opressió. Ens situa davant del repte d’atendre amb cura, a més, les dimensions múltiples de les relacions: emocions, experiències, somnis, posicionaments, coneixements, creences.\nEnfoca l’apoderament de grups i de persones; a través de generar processos de presa de consciència de la diversitat existent i de la distribució de poder que hi opera, com també de transformar els conflictes i establir mecanismes de diàleg i comunicació útils.\nAposta per la cerca de nous imaginaris polítics, noves formes d’organitzar-se, a través de concretar col·lectivament com es pot funcionar perquè les vides que vivim siguin dignes i sostenibles."}, {"la": "es", "text": ""}]}'
where ID = 7828;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ingeniería Social S.A.L. és una empresa d’economia cooperativa especialitzada en la formació, diagnòstic, definició, implementació i verificació de sistemes de gestió en Responsabilitat Social Corporativa. Ubicada a la ciutat de Barcelona, l’empresa compta amb un equip multidisciplinari format per consultores i expertes en matèria de responsabilitat social. \nLa nostra missió és assessorar a les empreses i organitzacions a desenvolupar negocis i organitzacions estables i sostenibles que aportin valor a la societat.\n"}, {"la": "es", "text": ""}]}'
where ID = 7829;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Andròmines te com a missió ser una organització de referència en la lluita i denuncia de l?exclusió social, defensant la dignitat i la igualtat d?oportunitats per totes les persones i la defensa i protecció del medi ambient.\n\nAquest darrer any Andròmines ha augmentat el dissenya i execució de programes de capacitació, formació i inserció laboral que s?adeqüen a les necessitats socials dels col?lectius i a la demanda real del mercat de treball. \n\nles empreses que contracten els serveis de gestió de residus, així com a les persones i organitzacions on arriben les campanyes d''educació ambiental i a les entitats amb les que realitzem treball en xarxa\n"}, {"la": "es", "text": ""}]}'
where ID = 7830;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Elaboració ecològica i autòctona de cerveses, most i maltes."}, {"la": "es", "text": ""}]}'
where ID = 7831;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "gdgfdgfd"}, {"la": "es", "text": ""}]}'
where ID = 7832;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7833;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7834;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7835;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7836;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball associat dedicada a la docència."}, {"la": "es", "text": ""}]}'
where ID = 7837;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA PAGE ORIGINAL, estudi de comunicació visual, es constitueix com empresa l’any 1991.\nEn l’actualitat, els seus socis Sònia Martínez Ruzafa i Josep Martínez Ruzafa (llicenciats en Belles Arts per la Universitat de Barcelona) dirigeixen projectes especialitzats en comunicació social, institucional i empresarial."}, {"la": "es", "text": ""}]}'
where ID = 7838;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7839;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7840;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació Vella Terra és una entitat sense ànim de lucre que neix amb el ferm compromís de promocionar l''autonomia de les persones depenents, i donar continuitat i sentit al seu projecte de vida. Els principals col·lectius atesos son persones grans, persones amb discapacitat, i persones tutelades."}, {"la": "es", "text": ""}]}'
where ID = 7841;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Instituto de Economía Social y Solidaria"}, {"la": "es", "text": ""}]}'
where ID = 7842;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7843;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de Servicios para el Mercado Social de Aragón"}, {"la": "es", "text": ""}]}'
where ID = 7844;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "eee"}, {"la": "es", "text": ""}]}'
where ID = 7845;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7846;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un agència de disseny i desenvolupament web i d’aplicacions al servei de la transformació social.Estem aquí per facilitar a les organitzacions la incorporació de la innovació tecnològica i ajudar-les a superar l''escletxa digital.Posem a l’abast de tothom el potencial que ofereix la tecnologia, adaptant-nos a les necessitats i possibilitats de cada projecte i acompanyant-lo amb un equip creatiu, multidisciplinar i proper que posa l’accent en l’accessibilitat, la pedagogia, el treball col·laboratiu i la perspectiva de gènere.Apostem per les tecnologies lliures com a instrument per assolir la sobirania tecnològica efectiva."}, {"la": "es", "text": ""}]}'
where ID = 7847;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7848;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestiona 19 habitatges dignes per a famílies en situació de vulnerabilitat i/o d''exclusió social. Acolliment i cessió d''ús dels habitatges per un període màxim de 3 anys."}, {"la": "es", "text": ""}]}'
where ID = 7849;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hosteleria mixta."}, {"la": "es", "text": ""}]}'
where ID = 7850;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 7851;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(la)baula: peça en forma d’anella que, enllaçada amb altres similars, forma una cadena.\n\nlabaula és una cooperativa d’arquitectes amb trajectòries professionals diverses que han decidit cooperar i sumar esforços per construir un projecte comú amb una metodologia de treball horitzontal.\n\nEl camp de treball de labaula consisteix en projectes d’edificació, rehabilitació, disseny industrial, urbanisme, espais públics, escenografia i espais efímers. Encarem els treballs amb idèntica dedicació i esforç, i amb independència de les seves dimensions o característiques. Aquests són entesos sempre com un espai de debat on s’incorporen col·laboradors d’altres disciplines per enriquir les perspectives pròpies.\n\nlabaula neix de la nostra il·lusió i inquietud per intentar fomentar la qualitat, el rigor i la responsabilitat en la nostra feina. Esperem poder ser una plataforma des d’on seguir treballant en aquesta direcció."}, {"la": "es", "text": ""}]}'
where ID = 7852;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7853;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fabricació de motlles i matrius per automoció."}, {"la": "es", "text": ""}]}'
where ID = 7854;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Teixits amb tellers manuals."}, {"la": "es", "text": ""}]}'
where ID = 7855;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió veïnal de barri."}, {"la": "es", "text": ""}]}'
where ID = 7856;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocions d´iniciatives socials i económiques."}, {"la": "es", "text": ""}]}'
where ID = 7857;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social."}, {"la": "es", "text": ""}]}'
where ID = 7858;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers."}, {"la": "es", "text": ""}]}'
where ID = 7859;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparacions i reformes."}, {"la": "es", "text": ""}]}'
where ID = 7860;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comercialització de complements."}, {"la": "es", "text": ""}]}'
where ID = 7862;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Menjador social, Cooperativa consum i Secció local coop57."}, {"la": "es", "text": ""}]}'
where ID = 7863;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concepte, disseny, producció i muntatge d´exposicions."}, {"la": "es", "text": ""}]}'
where ID = 7865;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7866;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Roba de llit feta amb justícia a Costa d''Ivori i Burkina"}, {"la": "es", "text": ""}]}'
where ID = 7869;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Auditories Comptes i Subvencions - ROAC 12236."}, {"la": "es", "text": ""}]}'
where ID = 7870;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sistemes avançats d´energia solar tèrmica."}, {"la": "es", "text": ""}]}'
where ID = 7671;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparació i lloguer de bicicletes. Monitors temps lliure."}, {"la": "es", "text": ""}]}'
where ID = 7672;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció socio-laboral de persones amb discapacitats."}, {"la": "es", "text": ""}]}'
where ID = 7673;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperació Internacional."}, {"la": "es", "text": ""}]}'
where ID = 7674;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de les Finances Ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7675;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acció social, cultural i ambiental. Economia social."}, {"la": "es", "text": ""}]}'
where ID = 7676;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Micromecenatge de projectes cívics, col.laboratius i oberts."}, {"la": "es", "text": ""}]}'
where ID = 7677;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoure la innovació social."}, {"la": "es", "text": ""}]}'
where ID = 7678;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Investigació, difusió i suport campanyes."}, {"la": "es", "text": ""}]}'
where ID = 7679;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sensibilització per a la solidaritat i la cooperació."}, {"la": "es", "text": ""}]}'
where ID = 7680;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció social i laboral de persones amb malalties mentals"}, {"la": "es", "text": ""}]}'
where ID = 7681;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Centre, Ateneu democràtic i progressista és una entitat fundada l’any 1865 per les classes populars de Caldes de Montbui, en el bressol del moviment ateneístic de caire humanista de finals del segle XIX, amb l’objectiu de formar lliurepensadors/es per transformar una realitat injusta. Actualment, el Centre té el compromís de continuar amb la mateixa tasca mirant al segle XXI.\n\nEn l’actualitat, a l’Ateneu hi participen 10 entitats i seccions: el concurs de teatre Taca’m, la Coral del Centre, Jazz Sessions Club, Club d’Escacs Caldes, la Cobla Thermalenca, Scena Teatre, Calderins pel programari lliure, el Centre Sona, l’associació juvenil la Guspira i la Cooperativa de consum ecològic el Rusc. Totes aquestes entitats es coordinen a través de la Junta, l’òrgan de decisió del Centre.\n\nA més compta amb El cafè cultural del Centre, un espai que aposta per una oferta de productes de proximitat i de qualitat i amb una programació cultural estable per a totes les edats (de dimarts a diumenge a partir de les 15 h).\n\nEls objectius de l’Ateneu passen per enfortir el projecte associatiu i donar cabuda a d’altres iniciatives que puguin sorgir al municipi, així com generar recursos per acabar de rehabilitar l’edifici i així donar resposta a les necessitats de l’entitat."}, {"la": "es", "text": ""}]}'
where ID = 7682;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arquitectura, urbanisme, participació comunitaria i feminista."}, {"la": "es", "text": ""}]}'
where ID = 7683;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concedir préstecs sense interés per crear llocs de treball."}, {"la": "es", "text": ""}]}'
where ID = 7684;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Federació entitats. Gestió cultural i esportiva"}, {"la": "es", "text": ""}]}'
where ID = 7685;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació que promou l’Agroecologia i la Sobirania Alimentària incidint en els àmbits agroalimentari i del desenvolupament local. Les membres d’Arran de terra comptem amb una formació acadèmica interdisciplinar, que integra aportacions de les ciències naturals i les ciències socials, i amb experiència laboral en recerca, formació, educació, assessorament i dinamització d’iniciatives locals de transició agroecològica."}, {"la": "es", "text": ""}]}'
where ID = 7686;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i altres serveis a les empreses."}, {"la": "es", "text": ""}]}'
where ID = 7687;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Compromeses amb les dones, transformant la societat. l''empoderament és l''eix transversal que articula els àmbits d''acció de Surt, alhora que la filosofía que n''impregna les metodologies de treball"}, {"la": "es", "text": ""}]}'
where ID = 7688;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Participació ciutadana en processos de transformació urbana."}, {"la": "es", "text": ""}]}'
where ID = 7689;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny gràfic i Comunicació visual."}, {"la": "es", "text": ""}]}'
where ID = 7690;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TRACTA''M ÉS UNA COOPERATIVA DE FISIOTERAPEUTES\nNeix amb l’objectiu de crear un projecte on la prioritat és l’atenció personalitzada, directa, amb una visió global de la persona i el seu entorn.\nEstem dins de l’economia social i solidària. Entenent la realitat econòmica i social en la que estem vivint, volem donar facilitats a l’hora d’accedir a aquest servei sanitari amb tarifes assequibles per a tothom, sense perjudicar la qualitat del servei, tant a nivell professional com personal.\nEstem associades de forma cooperativa on no hi ha jerarquies entre les persones treballadores, es prenen les decisions de forma assembleària i amb total transparència. Totes les persones usuàries participen en el creixement de la cooperativa, fent-la sostenible i millorant-ne el servei amb els seus suggeriments."}, {"la": "es", "text": ""}]}'
where ID = 7691;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció i dinamització de fires, foment de la cooperació i associacionisme i serveis complementaris"}, {"la": "es", "text": ""}]}'
where ID = 7692;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de serveis financers islàmics. Finances ètiques i participatives."}, {"la": "es", "text": ""}]}'
where ID = 7693;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Restaurant, càtering, cuines col.lectivitats i infusions ecològiques"}, {"la": "es", "text": ""}]}'
where ID = 7694;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball que gestiona un comerç d''alimentació ecològica al barri de Gràcia"}, {"la": "es", "text": ""}]}'
where ID = 7695;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació privada el Maresme pro persones amb discapacitat intel·lectual és una entitat d''iniciativa social sense afany de lucre que promou i impulsa la integració social i la millora de la qualitat de vida de les persones amb discapacitat intel·lectual de la comarca del Maresme i de les seves famílies.\n\nLa Fundació procura donar resposta a les necessitats i demandes d''aquest col·lectiu organitzant una àmplia xarxa de serveis i centres que ofereixen una atenció amb continuïtat."}, {"la": "es", "text": ""}]}'
where ID = 7696;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ramat d''ovelles amb formatgeria pròpia"}, {"la": "es", "text": ""}]}'
where ID = 7697;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que té com a finalitat principal la reinserció sòcio laboral de persones amb disminució derivada de malaltia mental a partir de la gestió de residus i protecció al Medi Ambient. També portem a terme projectes de cooperació al desenvolupament dins l''àmbit sanitari"}, {"la": "es", "text": ""}]}'
where ID = 7698;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament de grups en l''autopromoció del seu cohabitatge"}, {"la": "es", "text": ""}]}'
where ID = 7699;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball per persones amb discapacitat derivat de malaltia mental. Comarca d''Osona."}, {"la": "es", "text": ""}]}'
where ID = 7700;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir persones afectades de FM,SFC, i altres SSC , mitjançant teràpies alternatives i xerrades informatives."}, {"la": "es", "text": ""}]}'
where ID = 7701;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Tac Osona és l''empresa social de Sant Tomàs, una entitat sense ànim de lucre que vetlla per millorar la qualitat de vida de les persones amb discapacitat intel·lectual i les seves famílies a la comarca d’Osona."}, {"la": "es", "text": ""}]}'
where ID = 7702;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treballem per fomentar el millor desenvolupament i salut mental de nens i adolescents de 0 a 18 anys."}, {"la": "es", "text": ""}]}'
where ID = 7703;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació, té per finalitat la de promoure i dur a terme la gestió, representació i administració de tot tipus de centres residencials, pisos protegits i llars residencials dedicades a l''atenció, asistència i acolliment de malalts psíquics i patologies assimilades."}, {"la": "es", "text": ""}]}'
where ID = 7704;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria/Consultoria Energètica especialitzada en eficiència energètica i energies renovables"}, {"la": "es", "text": ""}]}'
where ID = 7705;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Atenció a les persones amb problemes de salut mental i adiccions de la comarca d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7706;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de barri amb la finalitat de millorar les condicions de vida de les veïnes mijançant el foment de de projectes comunitaris i accions culturals"}, {"la": "es", "text": ""}]}'
where ID = 7707;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa sense ànim de lucre i d’iniciativa social. Treballem en projectes estratègics d’incidència política per a la promoció de l''equitat de gènere. Els nostres eixos de treball són: la comunicació amb perspectiva feminista, l''abordatge estratègic de les violències masclistes i la incorporació de la perspectiva de gènere en organitzacions i polítiques públiques. "}, {"la": "es", "text": ""}]}'
where ID = 7708;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“La Llera” és una cooperativa de consum sense ànim de lucre que treballa per a una nova mirada de l''educació cap als infants creant una estructura pedagògica que intenta respectar els seus ritmes, que permet mirar a l''infant amb confiança, on cada un pot trobar-se a ell/ella mateixa i desenvolupar-se segons les necessitats i interessos propis. "}, {"la": "es", "text": ""}]}'
where ID = 7709;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat juvenil del barri de Prosperitat. Gestiona el Casal de Joves de Prospe."}, {"la": "es", "text": ""}]}'
where ID = 7710;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense anim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7711;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“Contribuir, des del seu compromís ètic, mitjançant suports i oportunitats, a què cada persona amb paràlisi cerebral o altres discapacitats similars i la seva família pugui desenvolupar el seu projecte de qualitat de vida, així com a promoure la seva inclusió com a ciutadana de ple dret en una societat justa i solidària. L’Associació Esclat desenvolupa la seva activitat majoritàriament a Catalunya"}, {"la": "es", "text": ""}]}'
where ID = 7712;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense afany de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7713;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Realitzem serveis de formació, consultoria i recerca relacionats amb la participació ciutadana, la gestió comunitària i l''habitatge."}, {"la": "es", "text": ""}]}'
where ID = 7714;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de Desenvolupament Infantil i Atenció Precoç"}, {"la": "es", "text": ""}]}'
where ID = 7715;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Coopertiva enginyeria del món de l''energia"}, {"la": "es", "text": ""}]}'
where ID = 7716;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "serveis especialitzats d’inserció laboral pel col·lectiu de persones diagnosticades d’esclerosi múltiple i altres discapacitats fisiques i/o sensorials"}, {"la": "es", "text": ""}]}'
where ID = 7717;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa ambiental. Desenvolupament rural, estudis de biodiversitat i millora d''espais naturals, comunicació ambiental i ecoturisme"}, {"la": "es", "text": ""}]}'
where ID = 7718;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Situat a Barcelona, és el primer centre de referència de la mobilitat sostenible a l’Estat espanyol i estem especialitzats en el desenvolupament de la bicicleta com a mode de transport a la ciutat. A més de la mobilitat, l’associació de segon grau BiciHub és un projecte que també gira al voltant de l’Economia Social i Solidària (ESS). Apostem per la intercooperació i la gestió comunitària i cooperativa d’equipaments socials. En aquest sentit, volem formar part del patrimoni ciutadà i que els agents locals i el veïnat ens facin seus."}, {"la": "es", "text": ""}]}'
where ID = 7719;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Societat Orgànica+10"}, {"la": "es", "text": ""}]}'
where ID = 7720;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Afavorir la transformació social a partir de la Sobirania Alimentària amb perspectiva feminista"}, {"la": "es", "text": ""}]}'
where ID = 7721;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació cultural sense ànime de lucre especialitzada en el teatre d''intervenció socials, el foment de la cultura i la formació teatral"}, {"la": "es", "text": ""}]}'
where ID = 7723;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7724;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte de roba personalitzada ambientalment sostenible i de producció ètica."}, {"la": "es", "text": ""}]}'
where ID = 7725;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa jurídica multidisciplinària compromesa amb la justícia social"}, {"la": "es", "text": ""}]}'
where ID = 7726;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat representativa del comerç i les empreses del barri de Trinitat Nova"}, {"la": "es", "text": ""}]}'
where ID = 7727;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''Habitatge"}, {"la": "es", "text": ""}]}'
where ID = 7728;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat d''atenció a persones amb paràlisi cerebral"}, {"la": "es", "text": ""}]}'
where ID = 7729;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball dedicat a la producció d''ous ecològics"}, {"la": "es", "text": ""}]}'
where ID = 7730;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "entitat esportiva que promou l''esport per  a persones amb discapacitat intel·lectual."}, {"la": "es", "text": ""}]}'
where ID = 7731;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Osona Ioga és el centre de referència a la comarca on diferents professionals unifiquem processos, eines i experiència en un projecte comú que posem al teu servei. La nostra missió és comprometre’ns amb tu i la teva salut oferint-te un espai de dedicació personal dins la teva vida quotidiana per a millorar el teu benestar i acompanyar-te en el procés d’aconseguir una millor qualitat de vida."}, {"la": "es", "text": ""}]}'
where ID = 7732;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Trabajamos por la inclusión de personas vulnerables. "}, {"la": "es", "text": ""}]}'
where ID = 7733;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Salut Mental Ponent és una entitat sense ànim de lucre i d’iniciativa social de les comarques de Lleida, registrada l’any 1993, creada en el seu origen per familiars i amics de persones amb algun trastorn mental, per tal de proporcionar orientació, informació, sensibilització, atenció social i un suport emocional i psicològic."}, {"la": "es", "text": ""}]}'
where ID = 7734;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa Feminista pel benestar de les persones en l''entorn laboral"}, {"la": "es", "text": ""}]}'
where ID = 7735;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions comunes i participades per a la millora del desenvolupament i la gestió col·lectiva del territori."}, {"la": "es", "text": ""}]}'
where ID = 7736;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7738;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7741;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7742;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació d''iniciativa social."}, {"la": "es", "text": ""}]}'
where ID = 7743;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "promoció de la salut sexual i la prevenció de riscos associa"}, {"la": "es", "text": ""}]}'
where ID = 7744;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d''iniciativa social que vetllem per potenciar l''autonomia i les oportunitats de les persones, tant a nivell individual com col•lectiu, amb la voluntat de contribuir a la transformació d''una societat més justa."}, {"la": "es", "text": ""}]}'
where ID = 7745;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7746;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "disseny"}, {"la": "es", "text": ""}]}'
where ID = 7747;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7748;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7749;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7750;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7751;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7752;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de menjadors escolars i educació del lleure."}, {"la": "es", "text": ""}]}'
where ID = 7753;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat excursionista"}, {"la": "es", "text": ""}]}'
where ID = 7754;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''infraestructures"}, {"la": "es", "text": ""}]}'
where ID = 7755;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Professionals en tècniques de salut per mèdis naturals"}, {"la": "es", "text": ""}]}'
where ID = 7756;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Introduccio a la cultura del treball col·lectius en exclusio"}, {"la": "es", "text": ""}]}'
where ID = 7757;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats educatives dirigides a dones i joves de Dakar."}, {"la": "es", "text": ""}]}'
where ID = 7758;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de mercat social."}, {"la": "es", "text": ""}]}'
where ID = 7759;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola de formació contínua del cooperativisme."}, {"la": "es", "text": ""}]}'
where ID = 7760;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció promoguda per PROMOCIONS i grup persones promotores"}, {"la": "es", "text": ""}]}'
where ID = 7761;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7762;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Energies renovables."}, {"la": "es", "text": ""}]}'
where ID = 7763;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai gastronòmic de l''Ateneu El Centre."}, {"la": "es", "text": ""}]}'
where ID = 7764;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7765;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arts grafiques i Venda de productes ecologics."}, {"la": "es", "text": ""}]}'
where ID = 7766;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma interassociativa d''entitats juvenils de BCN"}, {"la": "es", "text": ""}]}'
where ID = 7767;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de cluns de fitness"}, {"la": "es", "text": ""}]}'
where ID = 7768;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Clúster pel desenvolupament de l''economia social del treball. Està integrat per entitats socials sense ànim de lucre titulars d''un centre especial de treball (CET) o una empresa d''inserció (EI). "}, {"la": "es", "text": ""}]}'
where ID = 7769;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació excursionista."}, {"la": "es", "text": ""}]}'
where ID = 7770;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7771;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria i Consultoria Energètica"}, {"la": "es", "text": ""}]}'
where ID = 7772;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament a les cooperatives"}, {"la": "es", "text": ""}]}'
where ID = 7773;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i desenvolupament en ciències socials."}, {"la": "es", "text": ""}]}'
where ID = 7774;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Importador i distribuidor de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7775;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessorament i recerca per la sostenibilitat."}, {"la": "es", "text": ""}]}'
where ID = 7776;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció laboral mitjançant diverses activitats."}, {"la": "es", "text": ""}]}'
where ID = 7777;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ateneu social i cultural "}, {"la": "es", "text": ""}]}'
where ID = 7778;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluitar contra la pobresa i l''exclusió social."}, {"la": "es", "text": ""}]}'
where ID = 7779;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola d''infantil, primària i secundària."}, {"la": "es", "text": ""}]}'
where ID = 7780;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''edició i creació gràfica."}, {"la": "es", "text": ""}]}'
where ID = 7781;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treball social en inclusió sociolaboral i cohesió veïnal "}, {"la": "es", "text": ""}]}'
where ID = 7782;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir infants i families en risc d''exclusió social"}, {"la": "es", "text": ""}]}'
where ID = 7783;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació sense ànim de lucre amb fins socials"}, {"la": "es", "text": ""}]}'
where ID = 7784;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d´atenció a la persona."}, {"la": "es", "text": ""}]}'
where ID = 7785;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial i  acció cultural sociopolitica i econ. Som de Xes"}, {"la": "es", "text": ""}]}'
where ID = 7786;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers i Acompanyament processos."}, {"la": "es", "text": ""}]}'
where ID = 7787;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de formació per la inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7788;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria Social"}, {"la": "es", "text": ""}]}'
where ID = 7789;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa dedicada a l''assessorament empresarial dins dels àmbits de les vendes i el màrqueting, la gestió orientada en els valors i la formació d''emprenedoria i inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7790;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició publicació La Marea."}, {"la": "es", "text": ""}]}'
where ID = 7791;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Suport a les finances ètiques i a l''emprenedoria social."}, {"la": "es", "text": ""}]}'
where ID = 7792;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Aportem solucions integrals a tot tipus d''indústries mitjançant implantació i certificació de sistemes de Gestió ISO, assessorament i formació perquè aconsegueixin els més alts estàndards de qualitat exigits pels seus clients."}, {"la": "es", "text": ""}]}'
where ID = 7793;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de consum responsable"}, {"la": "es", "text": ""}]}'
where ID = 7794;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de promoció, difusió i distribució de Comerç Just"}, {"la": "es", "text": ""}]}'
where ID = 7795;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONGD"}, {"la": "es", "text": ""}]}'
where ID = 7796;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "MEDIACIÓ ASSEGURANCES PER A ENTITATS SOCIALS"}, {"la": "es", "text": ""}]}'
where ID = 7797;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "centre mèdic"}, {"la": "es", "text": ""}]}'
where ID = 7798;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Tot Raval és una a plataforma de 48 associacions, institucions, persones i empreses vinculades al Raval que es crea el 2002 amb un objectiu comú: la millora de la qualitat de vida en el barri. Es desenvolupa un treball comunitari, partint d’una visió integral del barri, que incideix en els àmbits social, cultural i econòmic i comercial."}, {"la": "es", "text": ""}]}'
where ID = 7799;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions integrals (sectors energètics i industrials)."}, {"la": "es", "text": ""}]}'
where ID = 7800;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció, distribució i venda de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7801;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "impulsar el desenvolupament i l''ús de la custòdia del territori com a eina de conservació."}, {"la": "es", "text": ""}]}'
where ID = 7802;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7803;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "DELTA gent activa es una entitat sense ànim de lucre, ONG/ONL, que treballant per i des de el sector social, es dirigeix cap a les persones grans i a les que sense ser-lo encara, tenen inquietuds a la vora d''aquest sector de la nostra societat i desitgen participar i/o col·laborar de forma ACTIVA per ajudar a la millora de la qualitat de vida del mateix i a la seva plena integració. "}, {"la": "es", "text": ""}]}'
where ID = 7804;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Administració de finques."}, {"la": "es", "text": ""}]}'
where ID = 7805;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que centre la seva activitat a la defensa de la llengua, la cultura, la cohesió social, l''educació i la identitat nacional de Catalunya. \n\n"}, {"la": "es", "text": ""}]}'
where ID = 7806;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "CERESNATURAL és una cooperativa que treballa per la salut a través de la venda de productes ecològics de proximitat i de la realització de teràpies naturals. La nostra motivació és oferir un espai que fomenti àmpliament la sostenibilitat, el comerç just, i la salut. Sent el nostre objectiu final el fet d’aconseguir que cadascú es faci responsable de la seva pròpia salut i la del planeta."}, {"la": "es", "text": ""}]}'
where ID = 7807;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de segon grau de restauració i dinamització social, que ofereix els seus serveis dins del Centre, Ateneu Democràtic i Progressista. Actualment està formada per set persones sòcies."}, {"la": "es", "text": ""}]}'
where ID = 7808;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de productes ecobiològics del barri del Congrés (Barcelona)"}, {"la": "es", "text": ""}]}'
where ID = 7809;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsar i elaborar els materials Escuracassoles."}, {"la": "es", "text": ""}]}'
where ID = 7810;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Ateneu Santcugatenc va ser creat l’any 1956 per un grup de ciutadans santcugatencs afeccionats al col·leccionisme, la numismàtica, la medallística i la filatèlia. L’any 1977 fou registrat com associació sense ànim de lucre amb la finalitat de fomentar també l’expansió artística, científica i/o la divulgació cultural, promovent el seu màxim desenvolupament i facilitant els coneixements que sobre aquestes matèries requereixin els seus associats/des. Amb el pas dels anys va anar incorporant als seus estatuts nous fins socials relacionats amb la cultura, l’associacionisme i el lleure, entre d’altres. \n\nDes de fa més de 15 anys desenvolupa un programa de cursos i tallers amb una bona acceptació per part de la ciutadania. L’entitat té la capacitat d’organitzar un programa trimestral de 70 disciplines diferents que posa a disposició de la població més de 1.400 places de formació i lleure. La xifra d’ocupació del darrer any 2015, només en aquest projecte, ha estat de 3.053 inscripcions. \n\nPel que fa l’àmbit de Cultura, a banda de tota la programació de cursos i tallers, realitza accions per al foment de la lectura, la gastronomia, la cultura popular, exposicions, concurs de pessebres, Festa de Tardor i, esporàdicament, Microespectacles entre d’altres accions. Així com activitats per a nens i nenes com casals d’estiu o vacances de Nadal.\n\nL’Ateneu realitza un seguit d’activitats en el camp de les polítiques socials que l’han fet mereixedor d’un conveni amb aquest àmbit de l’Ajuntament de Sant Cugat, per desenvolupar: l’Oficina del Voluntariat, l’Espai de Lleure (Lleure Social per a persones amb Trastorn Mental Sever), Joves Jubilats (envelliment actiu) i Català per a Tothom (projecte de foment de l’ús social del català i acollida sociocultural de persones nouvingudes). \n\nUn dels altres àmbits on ha tingut intervenció ha estat l’educatiu. Coordinant-se amb el Pla Educatiu d’Entorn dinamitza els Tallers d’Estudi Assistit per alumnes de secundaria de l’institut Arnau Cadell, el col·legi Pureza de Maria i l’Avenç, per lluitar contra el fracàs escolar, així com els grups de conversa i aprenentatge de català, directament a les escoles de primària, amb pares i mares de nens i nenes nouvinguts. \n\nO també activitats de sensibilització ambiental i promoció del consum responsable, ecològic, de proximitat i la reducció de residus, com el Mercat de Pagès, els Mercats de 2a mà o la sensibilització contra l’ús de bosses de plàstic.\n\nA més a més el fet d’haver pogut disposar fins ara de l’edifici de plaça Pep Ventura, 1, al nucli antic de la ciutat, obert a la ciutadania i a les entitats, permet gaudir d’espais de trobada i relació, exposicions, xerrades i conferències, presentacions de llibres, reunions o assajos. En aquests moment diferents entitats o col·lectius usen les instal·lacions de l’Ateneu per a desenvolupar les seves activitats de manera estable, com per exemple: Amics dels Veterans i la Penya Regalèssia, Comissió Festa de Tardor, Institut Trastorn Límit Fundació, Federació d’Associacions de Veïns, Grup de Jocs de Rol, Grup d’estudis de Teosofia, Pipa Club Sant Cugat, Societat Gastronòmica del Vallès. Alguns a més utilitzen la infraestructura i personal de l’Ateneu per a l’organització de les seves activitats, recepció d’usuaris i/o inscripcions.  A aquest cal sumar-hi els que de manera puntual utilitzen també la seu de l’Ateneu.\n"}, {"la": "es", "text": ""}]}'
where ID = 7811;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7812;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció audiovisual i Comunicació integral."}, {"la": "es", "text": ""}]}'
where ID = 7813;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lafede.cat està formada per 117 organitzacions i neix el gener de 2013 com a resultat de la fusió de les antigues Federació Catalana d’ONG per al Desenvolupament-FCONGD, Federació Catalana d’ONG pels Drets Humans-FCONGDH i Federació Catalana d’ONG per la Pau-FCONGPAU (vegeu història de Lafede.cat)Lafede.cat recull l’experiència i la trajectòria associativa de les seves 116 organitzacions federades però, a la vegada, aposta per la integració i l’actualització dels seus discursos, objectius i espais de relació institucional. Després d’un procés participatiu de mesos, a la seva primera Assemblea general, el 19 de juny de 2014."}, {"la": "es", "text": ""}]}'
where ID = 7814;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que va començar amb l''activitat principal d''apadrinar, adoptar i rescatar animals salvatges i de companyia. Arrel de l''experiència adquirida com a professionals del sector, a l''any 2012, FAADA va decidir iniciar un procés de transformació intern per poder incidir a les causes de forma més estratègica des de l''acció en l''àmbit social, legislatiu i educatiu. \nFacilitem informació i assessorament tècnic i jurídic a la societat sobre la protecció els animals i dirigim projectes de sensibilització que plantegin alternatives als sectors empresarials que directa o indirectament causen patiment als animals. Promovem accions formatives que incideixen en el foment de l''empatia i el respecte pels animals en els diferents cicles educatius. "}, {"la": "es", "text": ""}]}'
where ID = 7815;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny per emprenedoria social i iniciatives ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7816;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7817;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7818;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjançant la innovació narrativa, tecnològica i social, a COMPACTO posem la nostra creativitat al servei d’un món més lliure, solidari i participatiu. Des que vam fundar l’empresa el 2010, ens involucrem en projectes ambiciosos que transmetin els valors d’innovació, solidaritat, participació i llibertat, per enfortir l’economia social a través de la cultura i la cultura a través de l’economia social i col·laborativa."}, {"la": "es", "text": ""}]}'
where ID = 7819;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Distribució de productes ecològics, frescos i locals a les botigues i restaurants. Treballem  amb productors, cooperatives i entitats amb dificultats per fer arribar els productes als establiments oferint un servei global dins un àmbit local i creant xarxes de intercooperació local. Cafès La Chapolera és la creació pròpia de cafè ecològic d''origen amb valor social"}, {"la": "es", "text": ""}]}'
where ID = 7820;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La inserció laboral de persones en situació de risc i/o exclusió social, mitjançant la combinació de formació teòric-pràctic, social, personal i laboral de forma  que possibiliti l'' adquisició de hàbits i coneixements suficients per a poder accedir a un lloc de feina normalitzat.\nVenda de productes recicltas de segona mà i subsproductes relacionats amb els següents sectors: mobles, articles textils.."}, {"la": "es", "text": ""}]}'
where ID = 7821;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Portar a terme qualsevol activitats econòmiques de producció de béns, comercialització o de prestació de serveis que tinguin per finalitat primordial la de fer integració sociolaboral de .persones en situació o greu risc d''exclusió social.                                       "}, {"la": "es", "text": ""}]}'
where ID = 7822;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Panorama180 és una iniciativa sense ànim de lucre que es centra en el cinema i l’experimentació audiovisuals realitzats amb paràmetres del segle XXI: digitalització, xarxa, compromís, agitació, canvi, empoderament, comunitat…atentes a les novetats i propostes superadores dels problemes generats per la concepció obsoleta dels drets d’autor en l’era digital."}, {"la": "es", "text": ""}]}'
where ID = 7823;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i informació."}, {"la": "es", "text": ""}]}'
where ID = 7824;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Borda, és una cooperativa d''habitatge en cessió d''us, ubicada al barri de Sants de Barcelona. Un cop acabades les obres (a l''estiu 2018) tindrà 28 habitatges i varis espais comunitàris. Té signat un dret de superficie amb l''Ajuntament per 75 anys. Es construeix en sol públic ( i quedarà public) i té qualificació màxima d''eficiència energètica. L’edifici costarà tres milions d’euros (no arriba a 110.000 per habitatge) i es finançia amb l’aportació inicial dels socis, un préstec de Coop 57, l’emissió de títols participatius per part de la mateixa cooperativa i aportacions o préstecs d’altres entitats. "}, {"la": "es", "text": ""}]}'
where ID = 7825;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de projectes en l´àmbit de la Joventut."}, {"la": "es", "text": ""}]}'
where ID = 7826;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació"}, {"la": "es", "text": ""}]}'
where ID = 7827;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Matriu és un laboratori de democràcia viva, i ofereix formacions, acompanyament, recerca i creació de materials, amb la finalitat d''afavorir que persones i grups esdevinguin agents actius en la construcció de relacions i formes d’organització conscients i respectuoses.\nLa mirada dels feminismes; dóna eines per abordar la complexitat de cada una de les relacions i els grups, en què sovint actuen eixos diversos de desigualtat i opressió. Ens situa davant del repte d’atendre amb cura, a més, les dimensions múltiples de les relacions: emocions, experiències, somnis, posicionaments, coneixements, creences.\nEnfoca l’apoderament de grups i de persones; a través de generar processos de presa de consciència de la diversitat existent i de la distribució de poder que hi opera, com també de transformar els conflictes i establir mecanismes de diàleg i comunicació útils.\nAposta per la cerca de nous imaginaris polítics, noves formes d’organitzar-se, a través de concretar col·lectivament com es pot funcionar perquè les vides que vivim siguin dignes i sostenibles."}, {"la": "es", "text": ""}]}'
where ID = 7828;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ingeniería Social S.A.L. és una empresa d’economia cooperativa especialitzada en la formació, diagnòstic, definició, implementació i verificació de sistemes de gestió en Responsabilitat Social Corporativa. Ubicada a la ciutat de Barcelona, l’empresa compta amb un equip multidisciplinari format per consultores i expertes en matèria de responsabilitat social. \nLa nostra missió és assessorar a les empreses i organitzacions a desenvolupar negocis i organitzacions estables i sostenibles que aportin valor a la societat.\n"}, {"la": "es", "text": ""}]}'
where ID = 7829;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Andròmines te com a missió ser una organització de referència en la lluita i denuncia de l?exclusió social, defensant la dignitat i la igualtat d?oportunitats per totes les persones i la defensa i protecció del medi ambient.\n\nAquest darrer any Andròmines ha augmentat el dissenya i execució de programes de capacitació, formació i inserció laboral que s?adeqüen a les necessitats socials dels col?lectius i a la demanda real del mercat de treball. \n\nles empreses que contracten els serveis de gestió de residus, així com a les persones i organitzacions on arriben les campanyes d''educació ambiental i a les entitats amb les que realitzem treball en xarxa\n"}, {"la": "es", "text": ""}]}'
where ID = 7830;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Elaboració ecològica i autòctona de cerveses, most i maltes."}, {"la": "es", "text": ""}]}'
where ID = 7831;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "gdgfdgfd"}, {"la": "es", "text": ""}]}'
where ID = 7832;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7833;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7834;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7835;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7836;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball associat dedicada a la docència."}, {"la": "es", "text": ""}]}'
where ID = 7837;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA PAGE ORIGINAL, estudi de comunicació visual, es constitueix com empresa l’any 1991.\nEn l’actualitat, els seus socis Sònia Martínez Ruzafa i Josep Martínez Ruzafa (llicenciats en Belles Arts per la Universitat de Barcelona) dirigeixen projectes especialitzats en comunicació social, institucional i empresarial."}, {"la": "es", "text": ""}]}'
where ID = 7838;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7839;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7840;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació Vella Terra és una entitat sense ànim de lucre que neix amb el ferm compromís de promocionar l''autonomia de les persones depenents, i donar continuitat i sentit al seu projecte de vida. Els principals col·lectius atesos son persones grans, persones amb discapacitat, i persones tutelades."}, {"la": "es", "text": ""}]}'
where ID = 7841;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Instituto de Economía Social y Solidaria"}, {"la": "es", "text": ""}]}'
where ID = 7842;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7843;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de Servicios para el Mercado Social de Aragón"}, {"la": "es", "text": ""}]}'
where ID = 7844;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "eee"}, {"la": "es", "text": ""}]}'
where ID = 7845;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7846;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un agència de disseny i desenvolupament web i d’aplicacions al servei de la transformació social.Estem aquí per facilitar a les organitzacions la incorporació de la innovació tecnològica i ajudar-les a superar l''escletxa digital.Posem a l’abast de tothom el potencial que ofereix la tecnologia, adaptant-nos a les necessitats i possibilitats de cada projecte i acompanyant-lo amb un equip creatiu, multidisciplinar i proper que posa l’accent en l’accessibilitat, la pedagogia, el treball col·laboratiu i la perspectiva de gènere.Apostem per les tecnologies lliures com a instrument per assolir la sobirania tecnològica efectiva."}, {"la": "es", "text": ""}]}'
where ID = 7847;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7848;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestiona 19 habitatges dignes per a famílies en situació de vulnerabilitat i/o d''exclusió social. Acolliment i cessió d''ús dels habitatges per un període màxim de 3 anys."}, {"la": "es", "text": ""}]}'
where ID = 7849;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hosteleria mixta."}, {"la": "es", "text": ""}]}'
where ID = 7850;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 7851;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(la)baula: peça en forma d’anella que, enllaçada amb altres similars, forma una cadena.\n\nlabaula és una cooperativa d’arquitectes amb trajectòries professionals diverses que han decidit cooperar i sumar esforços per construir un projecte comú amb una metodologia de treball horitzontal.\n\nEl camp de treball de labaula consisteix en projectes d’edificació, rehabilitació, disseny industrial, urbanisme, espais públics, escenografia i espais efímers. Encarem els treballs amb idèntica dedicació i esforç, i amb independència de les seves dimensions o característiques. Aquests són entesos sempre com un espai de debat on s’incorporen col·laboradors d’altres disciplines per enriquir les perspectives pròpies.\n\nlabaula neix de la nostra il·lusió i inquietud per intentar fomentar la qualitat, el rigor i la responsabilitat en la nostra feina. Esperem poder ser una plataforma des d’on seguir treballant en aquesta direcció."}, {"la": "es", "text": ""}]}'
where ID = 7852;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7853;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fabricació de motlles i matrius per automoció."}, {"la": "es", "text": ""}]}'
where ID = 7854;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Teixits amb tellers manuals."}, {"la": "es", "text": ""}]}'
where ID = 7855;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió veïnal de barri."}, {"la": "es", "text": ""}]}'
where ID = 7856;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocions d´iniciatives socials i económiques."}, {"la": "es", "text": ""}]}'
where ID = 7857;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social."}, {"la": "es", "text": ""}]}'
where ID = 7858;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers."}, {"la": "es", "text": ""}]}'
where ID = 7859;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparacions i reformes."}, {"la": "es", "text": ""}]}'
where ID = 7860;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comercialització de complements."}, {"la": "es", "text": ""}]}'
where ID = 7862;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Menjador social, Cooperativa consum i Secció local coop57."}, {"la": "es", "text": ""}]}'
where ID = 7863;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concepte, disseny, producció i muntatge d´exposicions."}, {"la": "es", "text": ""}]}'
where ID = 7865;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7866;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Roba de llit feta amb justícia a Costa d''Ivori i Burkina"}, {"la": "es", "text": ""}]}'
where ID = 7869;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Auditories Comptes i Subvencions - ROAC 12236."}, {"la": "es", "text": ""}]}'
where ID = 7870;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Càtering agroecològic."}, {"la": "es", "text": ""}]}'
where ID = 7872;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7874;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjà de comunicació."}, {"la": "es", "text": ""}]}'
where ID = 7875;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Servei de contacte entre persones per tot tipus de serveis."}, {"la": "es", "text": ""}]}'
where ID = 7876;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis del món de les TICs."}, {"la": "es", "text": ""}]}'
where ID = 7878;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dinamització col·lectiva de producció agroecològica local."}, {"la": "es", "text": ""}]}'
where ID = 7879;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Traduccions i interpretacions."}, {"la": "es", "text": ""}]}'
where ID = 7880;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Rutes guiades en bicicleta i bici-escola d''adults."}, {"la": "es", "text": ""}]}'
where ID = 7881;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''educomunicacio dels Països Catalans. Expertes en dinamitzar projectes de ràdio escolar, podcast i justícia global."}, {"la": "es", "text": ""}]}'
where ID = 7882;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició de mitjans digitals."}, {"la": "es", "text": ""}]}'
where ID = 7883;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Casal Popular Boira Baixa de Manlleu. Punt de trobada per l''alliberament nacional, social i de gènere. "}, {"la": "es", "text": ""}]}'
where ID = 7884;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i Formació."}, {"la": "es", "text": ""}]}'
where ID = 7885;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció juvenil mitjançant el reciclatge i reutilització."}, {"la": "es", "text": ""}]}'
where ID = 7886;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsa projectes pel bé comú i l’economia solidària amb l’objectiu que la ciutadania assoleixi els drets fonamentals per una societat justa on totes siguem iguals davant la llei, gaudim de la llibertat en la cultura i fem possible la fraternitat econòmica."}, {"la": "es", "text": ""}]}'
where ID = 7887;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai públic, participació, infància."}, {"la": "es", "text": ""}]}'
where ID = 7888;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai de gestió comunitaria al barri de Porta."}, {"la": "es", "text": ""}]}'
where ID = 7889;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació audiovisual per a la transformació social."}, {"la": "es", "text": ""}]}'
where ID = 7890;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dansa amb impacte social."}, {"la": "es", "text": ""}]}'
where ID = 7891;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Marxandatge ecològic i de comerç just."}, {"la": "es", "text": ""}]}'
where ID = 7892;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sobirania alimentaria bassant en comercialització i consum."}, {"la": "es", "text": ""}]}'
where ID = 7893;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats culturals, artístiques i coaching."}, {"la": "es", "text": ""}]}'
where ID = 7894;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió comunitària d''equipament sociocultural."}, {"la": "es", "text": ""}]}'
where ID = 7895;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Buscar formules cooperatives per lluitar contra el atur."}, {"la": "es", "text": ""}]}'
where ID = 7896;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluita en xarxa contra l''atur, Formació i Auto-ocupació."}, {"la": "es", "text": ""}]}'
where ID = 7897;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recollida de ferralla. Reciclatge. Sensibilització."}, {"la": "es", "text": ""}]}'
where ID = 7898;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Exercici de l''Advocacia."}, {"la": "es", "text": ""}]}'
where ID = 7899;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció cohabitatge."}, {"la": "es", "text": ""}]}'
where ID = 7900;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de l''educació lliure i el lleure en família."}, {"la": "es", "text": ""}]}'
where ID = 7901;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7902;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Somos una asociación ONG de solidaridad internacional a donde uno de los ejes de nuestra acción es la Economía Social."}, {"la": "es", "text": ""}]}'
where ID = 7903;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació creada al 2014 amb l''objectiu de treballar per i amb les famílies del barri de Sants-Montjuïc amb infants d''entre 0 i 6 anys. Els mou el joc lliure, l''educació respectuosa i la creativitat. \nMitjançant una escola d''educació lliure, taller i activitats dirigides d''oci de qualitat en família i La Carpa de Tata Inti, la nostra reversió de les ludoteques tradicionals."}, {"la": "es", "text": ""}]}'
where ID = 7904;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un grup de persones que ens trobem cada setmana per construïr conjuntament una manera de consumir més justa, més conscient i més responsable."}, {"la": "es", "text": ""}]}'
where ID = 7905;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cal Temerari és un equipament cultural, social i cooperatiu ciutadà. Té l''objectiu de potenciar la participació i fomentar iniciatives socials i transformadores a Sant Cugat."}, {"la": "es", "text": ""}]}'
where ID = 7906;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(registre duplicat)\n\nEquipament de gestió comunitària del Barri de Porta, Nou Barris, i la Federació d''Entitats Transforma Porta té un conveni directe amb l''Ajuntament de Barcelona per a poder dinamitzar l''espai. Actualment hi ha 17 col·lectius a la Gestora ("La Totxana"), des d''on de forma assembleària i horitzontal s''organitza la vida de la Bòbila. Les temàtiques principals del projecte són: la transformació del barri de Porta a partir de posar en pràctica el desenvolupament comunitari, l''Economia Social i Solidària, l''agroecologia urbana, el Programari Lliure, les Festes Populars, etc. Al centre de tota l''activitat i volem posar "la cura", per això hem generat una Comissió específica per poder fer de l''Ateneu un espai d''acollida a on fomentar la part reproductiva del projecte i fer visible el que històricament ha estat invisible. En aquest sentit i per aconseguir aquest objectiu, creiem fonamental el paper del Banc Comunitari del Temps de Porta."}, {"la": "es", "text": ""}]}'
where ID = 7907;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7908;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7909;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7910;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Voltes som una cooperativa d’arquitectura i urbanisme que enfoquem la nostra pràctica com una eina de transformació social. La cooperativa està especialitzada en bioconstrucció i sostenibilitat, rehabilitació, i participació ciutadana. "}, {"la": "es", "text": ""}]}'
where ID = 7911;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7912;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7913;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7914;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7915;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7916;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7917;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7918;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7919;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Unió de Cooperadors de Mataró és una entitat privada participada per l’Ajuntament de Mataró i la Unió de Cooperadors de Mataró, que té per objecte la defensa i promoció de l’economia social en l’àmbit local de la ciutat de Mataró, català i a nivell internacional, mitjançant la realització d’actuacions de formació, informació, assessorament i estudi."}, {"la": "es", "text": ""}]}'
where ID = 7920;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7921;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som la federació d’AFA més gran de Catalunya, tant a primària com a secundària. Som més de 2.360 AFA repartides en més de 750 municipis i representem més de 540.000 famílies.\n"}, {"la": "es", "text": ""}]}'
where ID = 7922;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Col·legi Oficial de Psicologia de Catalunya és un espai professional, sense ànim de lucre, que actua com a frontissa entre l''espai acadèmic (ciència) i l''espai públic (societat). \nVetlla per la qualitat de les intervencions professionals, preservant i donant suport a la praxi de cada actuació, garantint als ciutadans l’òptima qualitat de l''atenció i el servei. El COPC innovarà, desenvoluparà i implementarà serveis professionals competitius en consonància amb l''exercici de la professió i fomentarà els avantatges derivats del pes de ser un col·lectiu. El Col·legi de Psicologia de Catalunya es regirà pels valors legals i ètics en vigor, per les pautes que les Institucions determinin i les que la deontologia pròpia dictamini.\nEl COPC vol ésser la principal associació professional de la psicologia a Catalunya, promovent valors que ofereixin rellevància institucional i prestigiïn els seus membres, de manera que aquests trobin en el seu sí, el millor suport per el creixement personal i professional.\n"}, {"la": "es", "text": ""}]}'
where ID = 7923;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una cooperativa dedicada a la sonorització, il·luminació d''esdeveniments, projecció de projectes culturals i serveis tècnics al servei de la cultura."}, {"la": "es", "text": ""}]}'
where ID = 7924;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d’iniciativa social dedicada a la promoció de la diversitat en un sentit ampli, així com a oferir eines per a la seva gestió en diversos àmbits socials. Alhora, ens proposem posar en evidència i contribuir a superar les dinàmiques d’exclusió basades en la diversitat."}, {"la": "es", "text": ""}]}'
where ID = 7925;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sistemes avançats d´energia solar tèrmica."}, {"la": "es", "text": ""}]}'
where ID = 7671;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparació i lloguer de bicicletes. Monitors temps lliure."}, {"la": "es", "text": ""}]}'
where ID = 7672;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció socio-laboral de persones amb discapacitats."}, {"la": "es", "text": ""}]}'
where ID = 7673;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperació Internacional."}, {"la": "es", "text": ""}]}'
where ID = 7674;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de les Finances Ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7675;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acció social, cultural i ambiental. Economia social."}, {"la": "es", "text": ""}]}'
where ID = 7676;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Micromecenatge de projectes cívics, col.laboratius i oberts."}, {"la": "es", "text": ""}]}'
where ID = 7677;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoure la innovació social."}, {"la": "es", "text": ""}]}'
where ID = 7678;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Investigació, difusió i suport campanyes."}, {"la": "es", "text": ""}]}'
where ID = 7679;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sensibilització per a la solidaritat i la cooperació."}, {"la": "es", "text": ""}]}'
where ID = 7680;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció social i laboral de persones amb malalties mentals"}, {"la": "es", "text": ""}]}'
where ID = 7681;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Centre, Ateneu democràtic i progressista és una entitat fundada l’any 1865 per les classes populars de Caldes de Montbui, en el bressol del moviment ateneístic de caire humanista de finals del segle XIX, amb l’objectiu de formar lliurepensadors/es per transformar una realitat injusta. Actualment, el Centre té el compromís de continuar amb la mateixa tasca mirant al segle XXI.\n\nEn l’actualitat, a l’Ateneu hi participen 10 entitats i seccions: el concurs de teatre Taca’m, la Coral del Centre, Jazz Sessions Club, Club d’Escacs Caldes, la Cobla Thermalenca, Scena Teatre, Calderins pel programari lliure, el Centre Sona, l’associació juvenil la Guspira i la Cooperativa de consum ecològic el Rusc. Totes aquestes entitats es coordinen a través de la Junta, l’òrgan de decisió del Centre.\n\nA més compta amb El cafè cultural del Centre, un espai que aposta per una oferta de productes de proximitat i de qualitat i amb una programació cultural estable per a totes les edats (de dimarts a diumenge a partir de les 15 h).\n\nEls objectius de l’Ateneu passen per enfortir el projecte associatiu i donar cabuda a d’altres iniciatives que puguin sorgir al municipi, així com generar recursos per acabar de rehabilitar l’edifici i així donar resposta a les necessitats de l’entitat."}, {"la": "es", "text": ""}]}'
where ID = 7682;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arquitectura, urbanisme, participació comunitaria i feminista."}, {"la": "es", "text": ""}]}'
where ID = 7683;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concedir préstecs sense interés per crear llocs de treball."}, {"la": "es", "text": ""}]}'
where ID = 7684;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Federació entitats. Gestió cultural i esportiva"}, {"la": "es", "text": ""}]}'
where ID = 7685;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació que promou l’Agroecologia i la Sobirania Alimentària incidint en els àmbits agroalimentari i del desenvolupament local. Les membres d’Arran de terra comptem amb una formació acadèmica interdisciplinar, que integra aportacions de les ciències naturals i les ciències socials, i amb experiència laboral en recerca, formació, educació, assessorament i dinamització d’iniciatives locals de transició agroecològica."}, {"la": "es", "text": ""}]}'
where ID = 7686;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i altres serveis a les empreses."}, {"la": "es", "text": ""}]}'
where ID = 7687;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Compromeses amb les dones, transformant la societat. l''empoderament és l''eix transversal que articula els àmbits d''acció de Surt, alhora que la filosofía que n''impregna les metodologies de treball"}, {"la": "es", "text": ""}]}'
where ID = 7688;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Participació ciutadana en processos de transformació urbana."}, {"la": "es", "text": ""}]}'
where ID = 7689;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny gràfic i Comunicació visual."}, {"la": "es", "text": ""}]}'
where ID = 7690;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TRACTA''M ÉS UNA COOPERATIVA DE FISIOTERAPEUTES\nNeix amb l’objectiu de crear un projecte on la prioritat és l’atenció personalitzada, directa, amb una visió global de la persona i el seu entorn.\nEstem dins de l’economia social i solidària. Entenent la realitat econòmica i social en la que estem vivint, volem donar facilitats a l’hora d’accedir a aquest servei sanitari amb tarifes assequibles per a tothom, sense perjudicar la qualitat del servei, tant a nivell professional com personal.\nEstem associades de forma cooperativa on no hi ha jerarquies entre les persones treballadores, es prenen les decisions de forma assembleària i amb total transparència. Totes les persones usuàries participen en el creixement de la cooperativa, fent-la sostenible i millorant-ne el servei amb els seus suggeriments."}, {"la": "es", "text": ""}]}'
where ID = 7691;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció i dinamització de fires, foment de la cooperació i associacionisme i serveis complementaris"}, {"la": "es", "text": ""}]}'
where ID = 7692;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de serveis financers islàmics. Finances ètiques i participatives."}, {"la": "es", "text": ""}]}'
where ID = 7693;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Restaurant, càtering, cuines col.lectivitats i infusions ecològiques"}, {"la": "es", "text": ""}]}'
where ID = 7694;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball que gestiona un comerç d''alimentació ecològica al barri de Gràcia"}, {"la": "es", "text": ""}]}'
where ID = 7695;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació privada el Maresme pro persones amb discapacitat intel·lectual és una entitat d''iniciativa social sense afany de lucre que promou i impulsa la integració social i la millora de la qualitat de vida de les persones amb discapacitat intel·lectual de la comarca del Maresme i de les seves famílies.\n\nLa Fundació procura donar resposta a les necessitats i demandes d''aquest col·lectiu organitzant una àmplia xarxa de serveis i centres que ofereixen una atenció amb continuïtat."}, {"la": "es", "text": ""}]}'
where ID = 7696;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ramat d''ovelles amb formatgeria pròpia"}, {"la": "es", "text": ""}]}'
where ID = 7697;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que té com a finalitat principal la reinserció sòcio laboral de persones amb disminució derivada de malaltia mental a partir de la gestió de residus i protecció al Medi Ambient. També portem a terme projectes de cooperació al desenvolupament dins l''àmbit sanitari"}, {"la": "es", "text": ""}]}'
where ID = 7698;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament de grups en l''autopromoció del seu cohabitatge"}, {"la": "es", "text": ""}]}'
where ID = 7699;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball per persones amb discapacitat derivat de malaltia mental. Comarca d''Osona."}, {"la": "es", "text": ""}]}'
where ID = 7700;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir persones afectades de FM,SFC, i altres SSC , mitjançant teràpies alternatives i xerrades informatives."}, {"la": "es", "text": ""}]}'
where ID = 7701;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Tac Osona és l''empresa social de Sant Tomàs, una entitat sense ànim de lucre que vetlla per millorar la qualitat de vida de les persones amb discapacitat intel·lectual i les seves famílies a la comarca d’Osona."}, {"la": "es", "text": ""}]}'
where ID = 7702;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treballem per fomentar el millor desenvolupament i salut mental de nens i adolescents de 0 a 18 anys."}, {"la": "es", "text": ""}]}'
where ID = 7703;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació, té per finalitat la de promoure i dur a terme la gestió, representació i administració de tot tipus de centres residencials, pisos protegits i llars residencials dedicades a l''atenció, asistència i acolliment de malalts psíquics i patologies assimilades."}, {"la": "es", "text": ""}]}'
where ID = 7704;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria/Consultoria Energètica especialitzada en eficiència energètica i energies renovables"}, {"la": "es", "text": ""}]}'
where ID = 7705;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Atenció a les persones amb problemes de salut mental i adiccions de la comarca d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7706;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de barri amb la finalitat de millorar les condicions de vida de les veïnes mijançant el foment de de projectes comunitaris i accions culturals"}, {"la": "es", "text": ""}]}'
where ID = 7707;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa sense ànim de lucre i d’iniciativa social. Treballem en projectes estratègics d’incidència política per a la promoció de l''equitat de gènere. Els nostres eixos de treball són: la comunicació amb perspectiva feminista, l''abordatge estratègic de les violències masclistes i la incorporació de la perspectiva de gènere en organitzacions i polítiques públiques. "}, {"la": "es", "text": ""}]}'
where ID = 7708;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“La Llera” és una cooperativa de consum sense ànim de lucre que treballa per a una nova mirada de l''educació cap als infants creant una estructura pedagògica que intenta respectar els seus ritmes, que permet mirar a l''infant amb confiança, on cada un pot trobar-se a ell/ella mateixa i desenvolupar-se segons les necessitats i interessos propis. "}, {"la": "es", "text": ""}]}'
where ID = 7709;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat juvenil del barri de Prosperitat. Gestiona el Casal de Joves de Prospe."}, {"la": "es", "text": ""}]}'
where ID = 7710;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense anim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7711;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“Contribuir, des del seu compromís ètic, mitjançant suports i oportunitats, a què cada persona amb paràlisi cerebral o altres discapacitats similars i la seva família pugui desenvolupar el seu projecte de qualitat de vida, així com a promoure la seva inclusió com a ciutadana de ple dret en una societat justa i solidària. L’Associació Esclat desenvolupa la seva activitat majoritàriament a Catalunya"}, {"la": "es", "text": ""}]}'
where ID = 7712;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense afany de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7713;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Realitzem serveis de formació, consultoria i recerca relacionats amb la participació ciutadana, la gestió comunitària i l''habitatge."}, {"la": "es", "text": ""}]}'
where ID = 7714;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de Desenvolupament Infantil i Atenció Precoç"}, {"la": "es", "text": ""}]}'
where ID = 7715;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Coopertiva enginyeria del món de l''energia"}, {"la": "es", "text": ""}]}'
where ID = 7716;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "serveis especialitzats d’inserció laboral pel col·lectiu de persones diagnosticades d’esclerosi múltiple i altres discapacitats fisiques i/o sensorials"}, {"la": "es", "text": ""}]}'
where ID = 7717;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa ambiental. Desenvolupament rural, estudis de biodiversitat i millora d''espais naturals, comunicació ambiental i ecoturisme"}, {"la": "es", "text": ""}]}'
where ID = 7718;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Situat a Barcelona, és el primer centre de referència de la mobilitat sostenible a l’Estat espanyol i estem especialitzats en el desenvolupament de la bicicleta com a mode de transport a la ciutat. A més de la mobilitat, l’associació de segon grau BiciHub és un projecte que també gira al voltant de l’Economia Social i Solidària (ESS). Apostem per la intercooperació i la gestió comunitària i cooperativa d’equipaments socials. En aquest sentit, volem formar part del patrimoni ciutadà i que els agents locals i el veïnat ens facin seus."}, {"la": "es", "text": ""}]}'
where ID = 7719;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Societat Orgànica+10"}, {"la": "es", "text": ""}]}'
where ID = 7720;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Afavorir la transformació social a partir de la Sobirania Alimentària amb perspectiva feminista"}, {"la": "es", "text": ""}]}'
where ID = 7721;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació cultural sense ànime de lucre especialitzada en el teatre d''intervenció socials, el foment de la cultura i la formació teatral"}, {"la": "es", "text": ""}]}'
where ID = 7723;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7724;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte de roba personalitzada ambientalment sostenible i de producció ètica."}, {"la": "es", "text": ""}]}'
where ID = 7725;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa jurídica multidisciplinària compromesa amb la justícia social"}, {"la": "es", "text": ""}]}'
where ID = 7726;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat representativa del comerç i les empreses del barri de Trinitat Nova"}, {"la": "es", "text": ""}]}'
where ID = 7727;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''Habitatge"}, {"la": "es", "text": ""}]}'
where ID = 7728;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat d''atenció a persones amb paràlisi cerebral"}, {"la": "es", "text": ""}]}'
where ID = 7729;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball dedicat a la producció d''ous ecològics"}, {"la": "es", "text": ""}]}'
where ID = 7730;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "entitat esportiva que promou l''esport per  a persones amb discapacitat intel·lectual."}, {"la": "es", "text": ""}]}'
where ID = 7731;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Osona Ioga és el centre de referència a la comarca on diferents professionals unifiquem processos, eines i experiència en un projecte comú que posem al teu servei. La nostra missió és comprometre’ns amb tu i la teva salut oferint-te un espai de dedicació personal dins la teva vida quotidiana per a millorar el teu benestar i acompanyar-te en el procés d’aconseguir una millor qualitat de vida."}, {"la": "es", "text": ""}]}'
where ID = 7732;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Trabajamos por la inclusión de personas vulnerables. "}, {"la": "es", "text": ""}]}'
where ID = 7733;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Salut Mental Ponent és una entitat sense ànim de lucre i d’iniciativa social de les comarques de Lleida, registrada l’any 1993, creada en el seu origen per familiars i amics de persones amb algun trastorn mental, per tal de proporcionar orientació, informació, sensibilització, atenció social i un suport emocional i psicològic."}, {"la": "es", "text": ""}]}'
where ID = 7734;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa Feminista pel benestar de les persones en l''entorn laboral"}, {"la": "es", "text": ""}]}'
where ID = 7735;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions comunes i participades per a la millora del desenvolupament i la gestió col·lectiva del territori."}, {"la": "es", "text": ""}]}'
where ID = 7736;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7738;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7741;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7742;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació d''iniciativa social."}, {"la": "es", "text": ""}]}'
where ID = 7743;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "promoció de la salut sexual i la prevenció de riscos associa"}, {"la": "es", "text": ""}]}'
where ID = 7744;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d''iniciativa social que vetllem per potenciar l''autonomia i les oportunitats de les persones, tant a nivell individual com col•lectiu, amb la voluntat de contribuir a la transformació d''una societat més justa."}, {"la": "es", "text": ""}]}'
where ID = 7745;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7746;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "disseny"}, {"la": "es", "text": ""}]}'
where ID = 7747;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7748;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7749;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7750;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7751;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7752;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de menjadors escolars i educació del lleure."}, {"la": "es", "text": ""}]}'
where ID = 7753;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat excursionista"}, {"la": "es", "text": ""}]}'
where ID = 7754;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''infraestructures"}, {"la": "es", "text": ""}]}'
where ID = 7755;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Professionals en tècniques de salut per mèdis naturals"}, {"la": "es", "text": ""}]}'
where ID = 7756;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Introduccio a la cultura del treball col·lectius en exclusio"}, {"la": "es", "text": ""}]}'
where ID = 7757;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats educatives dirigides a dones i joves de Dakar."}, {"la": "es", "text": ""}]}'
where ID = 7758;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de mercat social."}, {"la": "es", "text": ""}]}'
where ID = 7759;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola de formació contínua del cooperativisme."}, {"la": "es", "text": ""}]}'
where ID = 7760;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció promoguda per PROMOCIONS i grup persones promotores"}, {"la": "es", "text": ""}]}'
where ID = 7761;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7762;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Energies renovables."}, {"la": "es", "text": ""}]}'
where ID = 7763;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai gastronòmic de l''Ateneu El Centre."}, {"la": "es", "text": ""}]}'
where ID = 7764;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7765;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arts grafiques i Venda de productes ecologics."}, {"la": "es", "text": ""}]}'
where ID = 7766;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma interassociativa d''entitats juvenils de BCN"}, {"la": "es", "text": ""}]}'
where ID = 7767;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de cluns de fitness"}, {"la": "es", "text": ""}]}'
where ID = 7768;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Clúster pel desenvolupament de l''economia social del treball. Està integrat per entitats socials sense ànim de lucre titulars d''un centre especial de treball (CET) o una empresa d''inserció (EI). "}, {"la": "es", "text": ""}]}'
where ID = 7769;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació excursionista."}, {"la": "es", "text": ""}]}'
where ID = 7770;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7771;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria i Consultoria Energètica"}, {"la": "es", "text": ""}]}'
where ID = 7772;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament a les cooperatives"}, {"la": "es", "text": ""}]}'
where ID = 7773;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i desenvolupament en ciències socials."}, {"la": "es", "text": ""}]}'
where ID = 7774;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Importador i distribuidor de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7775;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessorament i recerca per la sostenibilitat."}, {"la": "es", "text": ""}]}'
where ID = 7776;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció laboral mitjançant diverses activitats."}, {"la": "es", "text": ""}]}'
where ID = 7777;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ateneu social i cultural "}, {"la": "es", "text": ""}]}'
where ID = 7778;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluitar contra la pobresa i l''exclusió social."}, {"la": "es", "text": ""}]}'
where ID = 7779;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola d''infantil, primària i secundària."}, {"la": "es", "text": ""}]}'
where ID = 7780;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''edició i creació gràfica."}, {"la": "es", "text": ""}]}'
where ID = 7781;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treball social en inclusió sociolaboral i cohesió veïnal "}, {"la": "es", "text": ""}]}'
where ID = 7782;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir infants i families en risc d''exclusió social"}, {"la": "es", "text": ""}]}'
where ID = 7783;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació sense ànim de lucre amb fins socials"}, {"la": "es", "text": ""}]}'
where ID = 7784;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d´atenció a la persona."}, {"la": "es", "text": ""}]}'
where ID = 7785;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial i  acció cultural sociopolitica i econ. Som de Xes"}, {"la": "es", "text": ""}]}'
where ID = 7786;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers i Acompanyament processos."}, {"la": "es", "text": ""}]}'
where ID = 7787;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de formació per la inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7788;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria Social"}, {"la": "es", "text": ""}]}'
where ID = 7789;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa dedicada a l''assessorament empresarial dins dels àmbits de les vendes i el màrqueting, la gestió orientada en els valors i la formació d''emprenedoria i inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7790;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició publicació La Marea."}, {"la": "es", "text": ""}]}'
where ID = 7791;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Suport a les finances ètiques i a l''emprenedoria social."}, {"la": "es", "text": ""}]}'
where ID = 7792;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Aportem solucions integrals a tot tipus d''indústries mitjançant implantació i certificació de sistemes de Gestió ISO, assessorament i formació perquè aconsegueixin els més alts estàndards de qualitat exigits pels seus clients."}, {"la": "es", "text": ""}]}'
where ID = 7793;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de consum responsable"}, {"la": "es", "text": ""}]}'
where ID = 7794;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de promoció, difusió i distribució de Comerç Just"}, {"la": "es", "text": ""}]}'
where ID = 7795;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONGD"}, {"la": "es", "text": ""}]}'
where ID = 7796;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "MEDIACIÓ ASSEGURANCES PER A ENTITATS SOCIALS"}, {"la": "es", "text": ""}]}'
where ID = 7797;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "centre mèdic"}, {"la": "es", "text": ""}]}'
where ID = 7798;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Tot Raval és una a plataforma de 48 associacions, institucions, persones i empreses vinculades al Raval que es crea el 2002 amb un objectiu comú: la millora de la qualitat de vida en el barri. Es desenvolupa un treball comunitari, partint d’una visió integral del barri, que incideix en els àmbits social, cultural i econòmic i comercial."}, {"la": "es", "text": ""}]}'
where ID = 7799;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions integrals (sectors energètics i industrials)."}, {"la": "es", "text": ""}]}'
where ID = 7800;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció, distribució i venda de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7801;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "impulsar el desenvolupament i l''ús de la custòdia del territori com a eina de conservació."}, {"la": "es", "text": ""}]}'
where ID = 7802;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7803;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "DELTA gent activa es una entitat sense ànim de lucre, ONG/ONL, que treballant per i des de el sector social, es dirigeix cap a les persones grans i a les que sense ser-lo encara, tenen inquietuds a la vora d''aquest sector de la nostra societat i desitgen participar i/o col·laborar de forma ACTIVA per ajudar a la millora de la qualitat de vida del mateix i a la seva plena integració. "}, {"la": "es", "text": ""}]}'
where ID = 7804;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Administració de finques."}, {"la": "es", "text": ""}]}'
where ID = 7805;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que centre la seva activitat a la defensa de la llengua, la cultura, la cohesió social, l''educació i la identitat nacional de Catalunya. \n\n"}, {"la": "es", "text": ""}]}'
where ID = 7806;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "CERESNATURAL és una cooperativa que treballa per la salut a través de la venda de productes ecològics de proximitat i de la realització de teràpies naturals. La nostra motivació és oferir un espai que fomenti àmpliament la sostenibilitat, el comerç just, i la salut. Sent el nostre objectiu final el fet d’aconseguir que cadascú es faci responsable de la seva pròpia salut i la del planeta."}, {"la": "es", "text": ""}]}'
where ID = 7807;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de segon grau de restauració i dinamització social, que ofereix els seus serveis dins del Centre, Ateneu Democràtic i Progressista. Actualment està formada per set persones sòcies."}, {"la": "es", "text": ""}]}'
where ID = 7808;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de productes ecobiològics del barri del Congrés (Barcelona)"}, {"la": "es", "text": ""}]}'
where ID = 7809;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsar i elaborar els materials Escuracassoles."}, {"la": "es", "text": ""}]}'
where ID = 7810;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Ateneu Santcugatenc va ser creat l’any 1956 per un grup de ciutadans santcugatencs afeccionats al col·leccionisme, la numismàtica, la medallística i la filatèlia. L’any 1977 fou registrat com associació sense ànim de lucre amb la finalitat de fomentar també l’expansió artística, científica i/o la divulgació cultural, promovent el seu màxim desenvolupament i facilitant els coneixements que sobre aquestes matèries requereixin els seus associats/des. Amb el pas dels anys va anar incorporant als seus estatuts nous fins socials relacionats amb la cultura, l’associacionisme i el lleure, entre d’altres. \n\nDes de fa més de 15 anys desenvolupa un programa de cursos i tallers amb una bona acceptació per part de la ciutadania. L’entitat té la capacitat d’organitzar un programa trimestral de 70 disciplines diferents que posa a disposició de la població més de 1.400 places de formació i lleure. La xifra d’ocupació del darrer any 2015, només en aquest projecte, ha estat de 3.053 inscripcions. \n\nPel que fa l’àmbit de Cultura, a banda de tota la programació de cursos i tallers, realitza accions per al foment de la lectura, la gastronomia, la cultura popular, exposicions, concurs de pessebres, Festa de Tardor i, esporàdicament, Microespectacles entre d’altres accions. Així com activitats per a nens i nenes com casals d’estiu o vacances de Nadal.\n\nL’Ateneu realitza un seguit d’activitats en el camp de les polítiques socials que l’han fet mereixedor d’un conveni amb aquest àmbit de l’Ajuntament de Sant Cugat, per desenvolupar: l’Oficina del Voluntariat, l’Espai de Lleure (Lleure Social per a persones amb Trastorn Mental Sever), Joves Jubilats (envelliment actiu) i Català per a Tothom (projecte de foment de l’ús social del català i acollida sociocultural de persones nouvingudes). \n\nUn dels altres àmbits on ha tingut intervenció ha estat l’educatiu. Coordinant-se amb el Pla Educatiu d’Entorn dinamitza els Tallers d’Estudi Assistit per alumnes de secundaria de l’institut Arnau Cadell, el col·legi Pureza de Maria i l’Avenç, per lluitar contra el fracàs escolar, així com els grups de conversa i aprenentatge de català, directament a les escoles de primària, amb pares i mares de nens i nenes nouvinguts. \n\nO també activitats de sensibilització ambiental i promoció del consum responsable, ecològic, de proximitat i la reducció de residus, com el Mercat de Pagès, els Mercats de 2a mà o la sensibilització contra l’ús de bosses de plàstic.\n\nA més a més el fet d’haver pogut disposar fins ara de l’edifici de plaça Pep Ventura, 1, al nucli antic de la ciutat, obert a la ciutadania i a les entitats, permet gaudir d’espais de trobada i relació, exposicions, xerrades i conferències, presentacions de llibres, reunions o assajos. En aquests moment diferents entitats o col·lectius usen les instal·lacions de l’Ateneu per a desenvolupar les seves activitats de manera estable, com per exemple: Amics dels Veterans i la Penya Regalèssia, Comissió Festa de Tardor, Institut Trastorn Límit Fundació, Federació d’Associacions de Veïns, Grup de Jocs de Rol, Grup d’estudis de Teosofia, Pipa Club Sant Cugat, Societat Gastronòmica del Vallès. Alguns a més utilitzen la infraestructura i personal de l’Ateneu per a l’organització de les seves activitats, recepció d’usuaris i/o inscripcions.  A aquest cal sumar-hi els que de manera puntual utilitzen també la seu de l’Ateneu.\n"}, {"la": "es", "text": ""}]}'
where ID = 7811;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7812;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció audiovisual i Comunicació integral."}, {"la": "es", "text": ""}]}'
where ID = 7813;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lafede.cat està formada per 117 organitzacions i neix el gener de 2013 com a resultat de la fusió de les antigues Federació Catalana d’ONG per al Desenvolupament-FCONGD, Federació Catalana d’ONG pels Drets Humans-FCONGDH i Federació Catalana d’ONG per la Pau-FCONGPAU (vegeu història de Lafede.cat)Lafede.cat recull l’experiència i la trajectòria associativa de les seves 116 organitzacions federades però, a la vegada, aposta per la integració i l’actualització dels seus discursos, objectius i espais de relació institucional. Després d’un procés participatiu de mesos, a la seva primera Assemblea general, el 19 de juny de 2014."}, {"la": "es", "text": ""}]}'
where ID = 7814;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que va començar amb l''activitat principal d''apadrinar, adoptar i rescatar animals salvatges i de companyia. Arrel de l''experiència adquirida com a professionals del sector, a l''any 2012, FAADA va decidir iniciar un procés de transformació intern per poder incidir a les causes de forma més estratègica des de l''acció en l''àmbit social, legislatiu i educatiu. \nFacilitem informació i assessorament tècnic i jurídic a la societat sobre la protecció els animals i dirigim projectes de sensibilització que plantegin alternatives als sectors empresarials que directa o indirectament causen patiment als animals. Promovem accions formatives que incideixen en el foment de l''empatia i el respecte pels animals en els diferents cicles educatius. "}, {"la": "es", "text": ""}]}'
where ID = 7815;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny per emprenedoria social i iniciatives ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7816;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7817;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7818;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjançant la innovació narrativa, tecnològica i social, a COMPACTO posem la nostra creativitat al servei d’un món més lliure, solidari i participatiu. Des que vam fundar l’empresa el 2010, ens involucrem en projectes ambiciosos que transmetin els valors d’innovació, solidaritat, participació i llibertat, per enfortir l’economia social a través de la cultura i la cultura a través de l’economia social i col·laborativa."}, {"la": "es", "text": ""}]}'
where ID = 7819;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Distribució de productes ecològics, frescos i locals a les botigues i restaurants. Treballem  amb productors, cooperatives i entitats amb dificultats per fer arribar els productes als establiments oferint un servei global dins un àmbit local i creant xarxes de intercooperació local. Cafès La Chapolera és la creació pròpia de cafè ecològic d''origen amb valor social"}, {"la": "es", "text": ""}]}'
where ID = 7820;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La inserció laboral de persones en situació de risc i/o exclusió social, mitjançant la combinació de formació teòric-pràctic, social, personal i laboral de forma  que possibiliti l'' adquisició de hàbits i coneixements suficients per a poder accedir a un lloc de feina normalitzat.\nVenda de productes recicltas de segona mà i subsproductes relacionats amb els següents sectors: mobles, articles textils.."}, {"la": "es", "text": ""}]}'
where ID = 7821;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Portar a terme qualsevol activitats econòmiques de producció de béns, comercialització o de prestació de serveis que tinguin per finalitat primordial la de fer integració sociolaboral de .persones en situació o greu risc d''exclusió social.                                       "}, {"la": "es", "text": ""}]}'
where ID = 7822;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Panorama180 és una iniciativa sense ànim de lucre que es centra en el cinema i l’experimentació audiovisuals realitzats amb paràmetres del segle XXI: digitalització, xarxa, compromís, agitació, canvi, empoderament, comunitat…atentes a les novetats i propostes superadores dels problemes generats per la concepció obsoleta dels drets d’autor en l’era digital."}, {"la": "es", "text": ""}]}'
where ID = 7823;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i informació."}, {"la": "es", "text": ""}]}'
where ID = 7824;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Borda, és una cooperativa d''habitatge en cessió d''us, ubicada al barri de Sants de Barcelona. Un cop acabades les obres (a l''estiu 2018) tindrà 28 habitatges i varis espais comunitàris. Té signat un dret de superficie amb l''Ajuntament per 75 anys. Es construeix en sol públic ( i quedarà public) i té qualificació màxima d''eficiència energètica. L’edifici costarà tres milions d’euros (no arriba a 110.000 per habitatge) i es finançia amb l’aportació inicial dels socis, un préstec de Coop 57, l’emissió de títols participatius per part de la mateixa cooperativa i aportacions o préstecs d’altres entitats. "}, {"la": "es", "text": ""}]}'
where ID = 7825;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de projectes en l´àmbit de la Joventut."}, {"la": "es", "text": ""}]}'
where ID = 7826;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació"}, {"la": "es", "text": ""}]}'
where ID = 7827;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Matriu és un laboratori de democràcia viva, i ofereix formacions, acompanyament, recerca i creació de materials, amb la finalitat d''afavorir que persones i grups esdevinguin agents actius en la construcció de relacions i formes d’organització conscients i respectuoses.\nLa mirada dels feminismes; dóna eines per abordar la complexitat de cada una de les relacions i els grups, en què sovint actuen eixos diversos de desigualtat i opressió. Ens situa davant del repte d’atendre amb cura, a més, les dimensions múltiples de les relacions: emocions, experiències, somnis, posicionaments, coneixements, creences.\nEnfoca l’apoderament de grups i de persones; a través de generar processos de presa de consciència de la diversitat existent i de la distribució de poder que hi opera, com també de transformar els conflictes i establir mecanismes de diàleg i comunicació útils.\nAposta per la cerca de nous imaginaris polítics, noves formes d’organitzar-se, a través de concretar col·lectivament com es pot funcionar perquè les vides que vivim siguin dignes i sostenibles."}, {"la": "es", "text": ""}]}'
where ID = 7828;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ingeniería Social S.A.L. és una empresa d’economia cooperativa especialitzada en la formació, diagnòstic, definició, implementació i verificació de sistemes de gestió en Responsabilitat Social Corporativa. Ubicada a la ciutat de Barcelona, l’empresa compta amb un equip multidisciplinari format per consultores i expertes en matèria de responsabilitat social. \nLa nostra missió és assessorar a les empreses i organitzacions a desenvolupar negocis i organitzacions estables i sostenibles que aportin valor a la societat.\n"}, {"la": "es", "text": ""}]}'
where ID = 7829;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Andròmines te com a missió ser una organització de referència en la lluita i denuncia de l?exclusió social, defensant la dignitat i la igualtat d?oportunitats per totes les persones i la defensa i protecció del medi ambient.\n\nAquest darrer any Andròmines ha augmentat el dissenya i execució de programes de capacitació, formació i inserció laboral que s?adeqüen a les necessitats socials dels col?lectius i a la demanda real del mercat de treball. \n\nles empreses que contracten els serveis de gestió de residus, així com a les persones i organitzacions on arriben les campanyes d''educació ambiental i a les entitats amb les que realitzem treball en xarxa\n"}, {"la": "es", "text": ""}]}'
where ID = 7830;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Elaboració ecològica i autòctona de cerveses, most i maltes."}, {"la": "es", "text": ""}]}'
where ID = 7831;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "gdgfdgfd"}, {"la": "es", "text": ""}]}'
where ID = 7832;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7833;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7834;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7835;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7836;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball associat dedicada a la docència."}, {"la": "es", "text": ""}]}'
where ID = 7837;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA PAGE ORIGINAL, estudi de comunicació visual, es constitueix com empresa l’any 1991.\nEn l’actualitat, els seus socis Sònia Martínez Ruzafa i Josep Martínez Ruzafa (llicenciats en Belles Arts per la Universitat de Barcelona) dirigeixen projectes especialitzats en comunicació social, institucional i empresarial."}, {"la": "es", "text": ""}]}'
where ID = 7838;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7839;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7840;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació Vella Terra és una entitat sense ànim de lucre que neix amb el ferm compromís de promocionar l''autonomia de les persones depenents, i donar continuitat i sentit al seu projecte de vida. Els principals col·lectius atesos son persones grans, persones amb discapacitat, i persones tutelades."}, {"la": "es", "text": ""}]}'
where ID = 7841;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Instituto de Economía Social y Solidaria"}, {"la": "es", "text": ""}]}'
where ID = 7842;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7843;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de Servicios para el Mercado Social de Aragón"}, {"la": "es", "text": ""}]}'
where ID = 7844;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "eee"}, {"la": "es", "text": ""}]}'
where ID = 7845;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7846;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un agència de disseny i desenvolupament web i d’aplicacions al servei de la transformació social.Estem aquí per facilitar a les organitzacions la incorporació de la innovació tecnològica i ajudar-les a superar l''escletxa digital.Posem a l’abast de tothom el potencial que ofereix la tecnologia, adaptant-nos a les necessitats i possibilitats de cada projecte i acompanyant-lo amb un equip creatiu, multidisciplinar i proper que posa l’accent en l’accessibilitat, la pedagogia, el treball col·laboratiu i la perspectiva de gènere.Apostem per les tecnologies lliures com a instrument per assolir la sobirania tecnològica efectiva."}, {"la": "es", "text": ""}]}'
where ID = 7847;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7848;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestiona 19 habitatges dignes per a famílies en situació de vulnerabilitat i/o d''exclusió social. Acolliment i cessió d''ús dels habitatges per un període màxim de 3 anys."}, {"la": "es", "text": ""}]}'
where ID = 7849;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hosteleria mixta."}, {"la": "es", "text": ""}]}'
where ID = 7850;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 7851;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(la)baula: peça en forma d’anella que, enllaçada amb altres similars, forma una cadena.\n\nlabaula és una cooperativa d’arquitectes amb trajectòries professionals diverses que han decidit cooperar i sumar esforços per construir un projecte comú amb una metodologia de treball horitzontal.\n\nEl camp de treball de labaula consisteix en projectes d’edificació, rehabilitació, disseny industrial, urbanisme, espais públics, escenografia i espais efímers. Encarem els treballs amb idèntica dedicació i esforç, i amb independència de les seves dimensions o característiques. Aquests són entesos sempre com un espai de debat on s’incorporen col·laboradors d’altres disciplines per enriquir les perspectives pròpies.\n\nlabaula neix de la nostra il·lusió i inquietud per intentar fomentar la qualitat, el rigor i la responsabilitat en la nostra feina. Esperem poder ser una plataforma des d’on seguir treballant en aquesta direcció."}, {"la": "es", "text": ""}]}'
where ID = 7852;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7853;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fabricació de motlles i matrius per automoció."}, {"la": "es", "text": ""}]}'
where ID = 7854;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Teixits amb tellers manuals."}, {"la": "es", "text": ""}]}'
where ID = 7855;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió veïnal de barri."}, {"la": "es", "text": ""}]}'
where ID = 7856;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocions d´iniciatives socials i económiques."}, {"la": "es", "text": ""}]}'
where ID = 7857;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social."}, {"la": "es", "text": ""}]}'
where ID = 7858;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers."}, {"la": "es", "text": ""}]}'
where ID = 7859;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparacions i reformes."}, {"la": "es", "text": ""}]}'
where ID = 7860;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comercialització de complements."}, {"la": "es", "text": ""}]}'
where ID = 7862;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Menjador social, Cooperativa consum i Secció local coop57."}, {"la": "es", "text": ""}]}'
where ID = 7863;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concepte, disseny, producció i muntatge d´exposicions."}, {"la": "es", "text": ""}]}'
where ID = 7865;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7866;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Roba de llit feta amb justícia a Costa d''Ivori i Burkina"}, {"la": "es", "text": ""}]}'
where ID = 7869;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Auditories Comptes i Subvencions - ROAC 12236."}, {"la": "es", "text": ""}]}'
where ID = 7870;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Càtering agroecològic."}, {"la": "es", "text": ""}]}'
where ID = 7872;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7874;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjà de comunicació."}, {"la": "es", "text": ""}]}'
where ID = 7875;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Servei de contacte entre persones per tot tipus de serveis."}, {"la": "es", "text": ""}]}'
where ID = 7876;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis del món de les TICs."}, {"la": "es", "text": ""}]}'
where ID = 7878;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dinamització col·lectiva de producció agroecològica local."}, {"la": "es", "text": ""}]}'
where ID = 7879;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Traduccions i interpretacions."}, {"la": "es", "text": ""}]}'
where ID = 7880;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Rutes guiades en bicicleta i bici-escola d''adults."}, {"la": "es", "text": ""}]}'
where ID = 7881;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''educomunicacio dels Països Catalans. Expertes en dinamitzar projectes de ràdio escolar, podcast i justícia global."}, {"la": "es", "text": ""}]}'
where ID = 7882;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició de mitjans digitals."}, {"la": "es", "text": ""}]}'
where ID = 7883;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Casal Popular Boira Baixa de Manlleu. Punt de trobada per l''alliberament nacional, social i de gènere. "}, {"la": "es", "text": ""}]}'
where ID = 7884;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i Formació."}, {"la": "es", "text": ""}]}'
where ID = 7885;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció juvenil mitjançant el reciclatge i reutilització."}, {"la": "es", "text": ""}]}'
where ID = 7886;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsa projectes pel bé comú i l’economia solidària amb l’objectiu que la ciutadania assoleixi els drets fonamentals per una societat justa on totes siguem iguals davant la llei, gaudim de la llibertat en la cultura i fem possible la fraternitat econòmica."}, {"la": "es", "text": ""}]}'
where ID = 7887;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai públic, participació, infància."}, {"la": "es", "text": ""}]}'
where ID = 7888;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai de gestió comunitaria al barri de Porta."}, {"la": "es", "text": ""}]}'
where ID = 7889;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació audiovisual per a la transformació social."}, {"la": "es", "text": ""}]}'
where ID = 7890;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dansa amb impacte social."}, {"la": "es", "text": ""}]}'
where ID = 7891;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Marxandatge ecològic i de comerç just."}, {"la": "es", "text": ""}]}'
where ID = 7892;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sobirania alimentaria bassant en comercialització i consum."}, {"la": "es", "text": ""}]}'
where ID = 7893;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats culturals, artístiques i coaching."}, {"la": "es", "text": ""}]}'
where ID = 7894;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió comunitària d''equipament sociocultural."}, {"la": "es", "text": ""}]}'
where ID = 7895;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Buscar formules cooperatives per lluitar contra el atur."}, {"la": "es", "text": ""}]}'
where ID = 7896;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluita en xarxa contra l''atur, Formació i Auto-ocupació."}, {"la": "es", "text": ""}]}'
where ID = 7897;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recollida de ferralla. Reciclatge. Sensibilització."}, {"la": "es", "text": ""}]}'
where ID = 7898;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Exercici de l''Advocacia."}, {"la": "es", "text": ""}]}'
where ID = 7899;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció cohabitatge."}, {"la": "es", "text": ""}]}'
where ID = 7900;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de l''educació lliure i el lleure en família."}, {"la": "es", "text": ""}]}'
where ID = 7901;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7902;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Somos una asociación ONG de solidaridad internacional a donde uno de los ejes de nuestra acción es la Economía Social."}, {"la": "es", "text": ""}]}'
where ID = 7903;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació creada al 2014 amb l''objectiu de treballar per i amb les famílies del barri de Sants-Montjuïc amb infants d''entre 0 i 6 anys. Els mou el joc lliure, l''educació respectuosa i la creativitat. \nMitjançant una escola d''educació lliure, taller i activitats dirigides d''oci de qualitat en família i La Carpa de Tata Inti, la nostra reversió de les ludoteques tradicionals."}, {"la": "es", "text": ""}]}'
where ID = 7904;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un grup de persones que ens trobem cada setmana per construïr conjuntament una manera de consumir més justa, més conscient i més responsable."}, {"la": "es", "text": ""}]}'
where ID = 7905;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cal Temerari és un equipament cultural, social i cooperatiu ciutadà. Té l''objectiu de potenciar la participació i fomentar iniciatives socials i transformadores a Sant Cugat."}, {"la": "es", "text": ""}]}'
where ID = 7906;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(registre duplicat)\n\nEquipament de gestió comunitària del Barri de Porta, Nou Barris, i la Federació d''Entitats Transforma Porta té un conveni directe amb l''Ajuntament de Barcelona per a poder dinamitzar l''espai. Actualment hi ha 17 col·lectius a la Gestora ("La Totxana"), des d''on de forma assembleària i horitzontal s''organitza la vida de la Bòbila. Les temàtiques principals del projecte són: la transformació del barri de Porta a partir de posar en pràctica el desenvolupament comunitari, l''Economia Social i Solidària, l''agroecologia urbana, el Programari Lliure, les Festes Populars, etc. Al centre de tota l''activitat i volem posar "la cura", per això hem generat una Comissió específica per poder fer de l''Ateneu un espai d''acollida a on fomentar la part reproductiva del projecte i fer visible el que històricament ha estat invisible. En aquest sentit i per aconseguir aquest objectiu, creiem fonamental el paper del Banc Comunitari del Temps de Porta."}, {"la": "es", "text": ""}]}'
where ID = 7907;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7908;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7909;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7910;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Voltes som una cooperativa d’arquitectura i urbanisme que enfoquem la nostra pràctica com una eina de transformació social. La cooperativa està especialitzada en bioconstrucció i sostenibilitat, rehabilitació, i participació ciutadana. "}, {"la": "es", "text": ""}]}'
where ID = 7911;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7912;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7913;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7914;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7915;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7916;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7917;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7918;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7919;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Unió de Cooperadors de Mataró és una entitat privada participada per l’Ajuntament de Mataró i la Unió de Cooperadors de Mataró, que té per objecte la defensa i promoció de l’economia social en l’àmbit local de la ciutat de Mataró, català i a nivell internacional, mitjançant la realització d’actuacions de formació, informació, assessorament i estudi."}, {"la": "es", "text": ""}]}'
where ID = 7920;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7921;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som la federació d’AFA més gran de Catalunya, tant a primària com a secundària. Som més de 2.360 AFA repartides en més de 750 municipis i representem més de 540.000 famílies.\n"}, {"la": "es", "text": ""}]}'
where ID = 7922;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Col·legi Oficial de Psicologia de Catalunya és un espai professional, sense ànim de lucre, que actua com a frontissa entre l''espai acadèmic (ciència) i l''espai públic (societat). \nVetlla per la qualitat de les intervencions professionals, preservant i donant suport a la praxi de cada actuació, garantint als ciutadans l’òptima qualitat de l''atenció i el servei. El COPC innovarà, desenvoluparà i implementarà serveis professionals competitius en consonància amb l''exercici de la professió i fomentarà els avantatges derivats del pes de ser un col·lectiu. El Col·legi de Psicologia de Catalunya es regirà pels valors legals i ètics en vigor, per les pautes que les Institucions determinin i les que la deontologia pròpia dictamini.\nEl COPC vol ésser la principal associació professional de la psicologia a Catalunya, promovent valors que ofereixin rellevància institucional i prestigiïn els seus membres, de manera que aquests trobin en el seu sí, el millor suport per el creixement personal i professional.\n"}, {"la": "es", "text": ""}]}'
where ID = 7923;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una cooperativa dedicada a la sonorització, il·luminació d''esdeveniments, projecció de projectes culturals i serveis tècnics al servei de la cultura."}, {"la": "es", "text": ""}]}'
where ID = 7924;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d’iniciativa social dedicada a la promoció de la diversitat en un sentit ampli, així com a oferir eines per a la seva gestió en diversos àmbits socials. Alhora, ens proposem posar en evidència i contribuir a superar les dinàmiques d’exclusió basades en la diversitat."}, {"la": "es", "text": ""}]}'
where ID = 7925;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sistemes avançats d´energia solar tèrmica."}, {"la": "es", "text": ""}]}'
where ID = 7671;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparació i lloguer de bicicletes. Monitors temps lliure."}, {"la": "es", "text": ""}]}'
where ID = 7672;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció socio-laboral de persones amb discapacitats."}, {"la": "es", "text": ""}]}'
where ID = 7673;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperació Internacional."}, {"la": "es", "text": ""}]}'
where ID = 7674;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de les Finances Ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7675;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acció social, cultural i ambiental. Economia social."}, {"la": "es", "text": ""}]}'
where ID = 7676;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Micromecenatge de projectes cívics, col.laboratius i oberts."}, {"la": "es", "text": ""}]}'
where ID = 7677;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoure la innovació social."}, {"la": "es", "text": ""}]}'
where ID = 7678;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Investigació, difusió i suport campanyes."}, {"la": "es", "text": ""}]}'
where ID = 7679;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sensibilització per a la solidaritat i la cooperació."}, {"la": "es", "text": ""}]}'
where ID = 7680;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció social i laboral de persones amb malalties mentals"}, {"la": "es", "text": ""}]}'
where ID = 7681;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Centre, Ateneu democràtic i progressista és una entitat fundada l’any 1865 per les classes populars de Caldes de Montbui, en el bressol del moviment ateneístic de caire humanista de finals del segle XIX, amb l’objectiu de formar lliurepensadors/es per transformar una realitat injusta. Actualment, el Centre té el compromís de continuar amb la mateixa tasca mirant al segle XXI.\n\nEn l’actualitat, a l’Ateneu hi participen 10 entitats i seccions: el concurs de teatre Taca’m, la Coral del Centre, Jazz Sessions Club, Club d’Escacs Caldes, la Cobla Thermalenca, Scena Teatre, Calderins pel programari lliure, el Centre Sona, l’associació juvenil la Guspira i la Cooperativa de consum ecològic el Rusc. Totes aquestes entitats es coordinen a través de la Junta, l’òrgan de decisió del Centre.\n\nA més compta amb El cafè cultural del Centre, un espai que aposta per una oferta de productes de proximitat i de qualitat i amb una programació cultural estable per a totes les edats (de dimarts a diumenge a partir de les 15 h).\n\nEls objectius de l’Ateneu passen per enfortir el projecte associatiu i donar cabuda a d’altres iniciatives que puguin sorgir al municipi, així com generar recursos per acabar de rehabilitar l’edifici i així donar resposta a les necessitats de l’entitat."}, {"la": "es", "text": ""}]}'
where ID = 7682;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arquitectura, urbanisme, participació comunitaria i feminista."}, {"la": "es", "text": ""}]}'
where ID = 7683;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concedir préstecs sense interés per crear llocs de treball."}, {"la": "es", "text": ""}]}'
where ID = 7684;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Federació entitats. Gestió cultural i esportiva"}, {"la": "es", "text": ""}]}'
where ID = 7685;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació que promou l’Agroecologia i la Sobirania Alimentària incidint en els àmbits agroalimentari i del desenvolupament local. Les membres d’Arran de terra comptem amb una formació acadèmica interdisciplinar, que integra aportacions de les ciències naturals i les ciències socials, i amb experiència laboral en recerca, formació, educació, assessorament i dinamització d’iniciatives locals de transició agroecològica."}, {"la": "es", "text": ""}]}'
where ID = 7686;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i altres serveis a les empreses."}, {"la": "es", "text": ""}]}'
where ID = 7687;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Compromeses amb les dones, transformant la societat. l''empoderament és l''eix transversal que articula els àmbits d''acció de Surt, alhora que la filosofía que n''impregna les metodologies de treball"}, {"la": "es", "text": ""}]}'
where ID = 7688;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Participació ciutadana en processos de transformació urbana."}, {"la": "es", "text": ""}]}'
where ID = 7689;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny gràfic i Comunicació visual."}, {"la": "es", "text": ""}]}'
where ID = 7690;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TRACTA''M ÉS UNA COOPERATIVA DE FISIOTERAPEUTES\nNeix amb l’objectiu de crear un projecte on la prioritat és l’atenció personalitzada, directa, amb una visió global de la persona i el seu entorn.\nEstem dins de l’economia social i solidària. Entenent la realitat econòmica i social en la que estem vivint, volem donar facilitats a l’hora d’accedir a aquest servei sanitari amb tarifes assequibles per a tothom, sense perjudicar la qualitat del servei, tant a nivell professional com personal.\nEstem associades de forma cooperativa on no hi ha jerarquies entre les persones treballadores, es prenen les decisions de forma assembleària i amb total transparència. Totes les persones usuàries participen en el creixement de la cooperativa, fent-la sostenible i millorant-ne el servei amb els seus suggeriments."}, {"la": "es", "text": ""}]}'
where ID = 7691;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció i dinamització de fires, foment de la cooperació i associacionisme i serveis complementaris"}, {"la": "es", "text": ""}]}'
where ID = 7692;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de serveis financers islàmics. Finances ètiques i participatives."}, {"la": "es", "text": ""}]}'
where ID = 7693;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Restaurant, càtering, cuines col.lectivitats i infusions ecològiques"}, {"la": "es", "text": ""}]}'
where ID = 7694;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball que gestiona un comerç d''alimentació ecològica al barri de Gràcia"}, {"la": "es", "text": ""}]}'
where ID = 7695;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació privada el Maresme pro persones amb discapacitat intel·lectual és una entitat d''iniciativa social sense afany de lucre que promou i impulsa la integració social i la millora de la qualitat de vida de les persones amb discapacitat intel·lectual de la comarca del Maresme i de les seves famílies.\n\nLa Fundació procura donar resposta a les necessitats i demandes d''aquest col·lectiu organitzant una àmplia xarxa de serveis i centres que ofereixen una atenció amb continuïtat."}, {"la": "es", "text": ""}]}'
where ID = 7696;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ramat d''ovelles amb formatgeria pròpia"}, {"la": "es", "text": ""}]}'
where ID = 7697;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que té com a finalitat principal la reinserció sòcio laboral de persones amb disminució derivada de malaltia mental a partir de la gestió de residus i protecció al Medi Ambient. També portem a terme projectes de cooperació al desenvolupament dins l''àmbit sanitari"}, {"la": "es", "text": ""}]}'
where ID = 7698;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament de grups en l''autopromoció del seu cohabitatge"}, {"la": "es", "text": ""}]}'
where ID = 7699;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball per persones amb discapacitat derivat de malaltia mental. Comarca d''Osona."}, {"la": "es", "text": ""}]}'
where ID = 7700;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir persones afectades de FM,SFC, i altres SSC , mitjançant teràpies alternatives i xerrades informatives."}, {"la": "es", "text": ""}]}'
where ID = 7701;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Tac Osona és l''empresa social de Sant Tomàs, una entitat sense ànim de lucre que vetlla per millorar la qualitat de vida de les persones amb discapacitat intel·lectual i les seves famílies a la comarca d’Osona."}, {"la": "es", "text": ""}]}'
where ID = 7702;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treballem per fomentar el millor desenvolupament i salut mental de nens i adolescents de 0 a 18 anys."}, {"la": "es", "text": ""}]}'
where ID = 7703;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació, té per finalitat la de promoure i dur a terme la gestió, representació i administració de tot tipus de centres residencials, pisos protegits i llars residencials dedicades a l''atenció, asistència i acolliment de malalts psíquics i patologies assimilades."}, {"la": "es", "text": ""}]}'
where ID = 7704;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria/Consultoria Energètica especialitzada en eficiència energètica i energies renovables"}, {"la": "es", "text": ""}]}'
where ID = 7705;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Atenció a les persones amb problemes de salut mental i adiccions de la comarca d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7706;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de barri amb la finalitat de millorar les condicions de vida de les veïnes mijançant el foment de de projectes comunitaris i accions culturals"}, {"la": "es", "text": ""}]}'
where ID = 7707;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa sense ànim de lucre i d’iniciativa social. Treballem en projectes estratègics d’incidència política per a la promoció de l''equitat de gènere. Els nostres eixos de treball són: la comunicació amb perspectiva feminista, l''abordatge estratègic de les violències masclistes i la incorporació de la perspectiva de gènere en organitzacions i polítiques públiques. "}, {"la": "es", "text": ""}]}'
where ID = 7708;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“La Llera” és una cooperativa de consum sense ànim de lucre que treballa per a una nova mirada de l''educació cap als infants creant una estructura pedagògica que intenta respectar els seus ritmes, que permet mirar a l''infant amb confiança, on cada un pot trobar-se a ell/ella mateixa i desenvolupar-se segons les necessitats i interessos propis. "}, {"la": "es", "text": ""}]}'
where ID = 7709;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat juvenil del barri de Prosperitat. Gestiona el Casal de Joves de Prospe."}, {"la": "es", "text": ""}]}'
where ID = 7710;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense anim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7711;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "“Contribuir, des del seu compromís ètic, mitjançant suports i oportunitats, a què cada persona amb paràlisi cerebral o altres discapacitats similars i la seva família pugui desenvolupar el seu projecte de qualitat de vida, així com a promoure la seva inclusió com a ciutadana de ple dret en una societat justa i solidària. L’Associació Esclat desenvolupa la seva activitat majoritàriament a Catalunya"}, {"la": "es", "text": ""}]}'
where ID = 7712;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense afany de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7713;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Realitzem serveis de formació, consultoria i recerca relacionats amb la participació ciutadana, la gestió comunitària i l''habitatge."}, {"la": "es", "text": ""}]}'
where ID = 7714;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de Desenvolupament Infantil i Atenció Precoç"}, {"la": "es", "text": ""}]}'
where ID = 7715;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Coopertiva enginyeria del món de l''energia"}, {"la": "es", "text": ""}]}'
where ID = 7716;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "serveis especialitzats d’inserció laboral pel col·lectiu de persones diagnosticades d’esclerosi múltiple i altres discapacitats fisiques i/o sensorials"}, {"la": "es", "text": ""}]}'
where ID = 7717;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa ambiental. Desenvolupament rural, estudis de biodiversitat i millora d''espais naturals, comunicació ambiental i ecoturisme"}, {"la": "es", "text": ""}]}'
where ID = 7718;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Situat a Barcelona, és el primer centre de referència de la mobilitat sostenible a l’Estat espanyol i estem especialitzats en el desenvolupament de la bicicleta com a mode de transport a la ciutat. A més de la mobilitat, l’associació de segon grau BiciHub és un projecte que també gira al voltant de l’Economia Social i Solidària (ESS). Apostem per la intercooperació i la gestió comunitària i cooperativa d’equipaments socials. En aquest sentit, volem formar part del patrimoni ciutadà i que els agents locals i el veïnat ens facin seus."}, {"la": "es", "text": ""}]}'
where ID = 7719;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Societat Orgànica+10"}, {"la": "es", "text": ""}]}'
where ID = 7720;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Afavorir la transformació social a partir de la Sobirania Alimentària amb perspectiva feminista"}, {"la": "es", "text": ""}]}'
where ID = 7721;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació cultural sense ànime de lucre especialitzada en el teatre d''intervenció socials, el foment de la cultura i la formació teatral"}, {"la": "es", "text": ""}]}'
where ID = 7723;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7724;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte de roba personalitzada ambientalment sostenible i de producció ètica."}, {"la": "es", "text": ""}]}'
where ID = 7725;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa jurídica multidisciplinària compromesa amb la justícia social"}, {"la": "es", "text": ""}]}'
where ID = 7726;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat representativa del comerç i les empreses del barri de Trinitat Nova"}, {"la": "es", "text": ""}]}'
where ID = 7727;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''Habitatge"}, {"la": "es", "text": ""}]}'
where ID = 7728;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat d''atenció a persones amb paràlisi cerebral"}, {"la": "es", "text": ""}]}'
where ID = 7729;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball dedicat a la producció d''ous ecològics"}, {"la": "es", "text": ""}]}'
where ID = 7730;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "entitat esportiva que promou l''esport per  a persones amb discapacitat intel·lectual."}, {"la": "es", "text": ""}]}'
where ID = 7731;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Osona Ioga és el centre de referència a la comarca on diferents professionals unifiquem processos, eines i experiència en un projecte comú que posem al teu servei. La nostra missió és comprometre’ns amb tu i la teva salut oferint-te un espai de dedicació personal dins la teva vida quotidiana per a millorar el teu benestar i acompanyar-te en el procés d’aconseguir una millor qualitat de vida."}, {"la": "es", "text": ""}]}'
where ID = 7732;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Trabajamos por la inclusión de personas vulnerables. "}, {"la": "es", "text": ""}]}'
where ID = 7733;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Salut Mental Ponent és una entitat sense ànim de lucre i d’iniciativa social de les comarques de Lleida, registrada l’any 1993, creada en el seu origen per familiars i amics de persones amb algun trastorn mental, per tal de proporcionar orientació, informació, sensibilització, atenció social i un suport emocional i psicològic."}, {"la": "es", "text": ""}]}'
where ID = 7734;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa Feminista pel benestar de les persones en l''entorn laboral"}, {"la": "es", "text": ""}]}'
where ID = 7735;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions comunes i participades per a la millora del desenvolupament i la gestió col·lectiva del territori."}, {"la": "es", "text": ""}]}'
where ID = 7736;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 7738;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7741;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7742;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació d''iniciativa social."}, {"la": "es", "text": ""}]}'
where ID = 7743;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "promoció de la salut sexual i la prevenció de riscos associa"}, {"la": "es", "text": ""}]}'
where ID = 7744;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d''iniciativa social que vetllem per potenciar l''autonomia i les oportunitats de les persones, tant a nivell individual com col•lectiu, amb la voluntat de contribuir a la transformació d''una societat més justa."}, {"la": "es", "text": ""}]}'
where ID = 7745;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7746;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "disseny"}, {"la": "es", "text": ""}]}'
where ID = 7747;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7748;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7749;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7750;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7751;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7752;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de menjadors escolars i educació del lleure."}, {"la": "es", "text": ""}]}'
where ID = 7753;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat excursionista"}, {"la": "es", "text": ""}]}'
where ID = 7754;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''infraestructures"}, {"la": "es", "text": ""}]}'
where ID = 7755;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Professionals en tècniques de salut per mèdis naturals"}, {"la": "es", "text": ""}]}'
where ID = 7756;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Introduccio a la cultura del treball col·lectius en exclusio"}, {"la": "es", "text": ""}]}'
where ID = 7757;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats educatives dirigides a dones i joves de Dakar."}, {"la": "es", "text": ""}]}'
where ID = 7758;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de mercat social."}, {"la": "es", "text": ""}]}'
where ID = 7759;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola de formació contínua del cooperativisme."}, {"la": "es", "text": ""}]}'
where ID = 7760;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció promoguda per PROMOCIONS i grup persones promotores"}, {"la": "es", "text": ""}]}'
where ID = 7761;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7762;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Energies renovables."}, {"la": "es", "text": ""}]}'
where ID = 7763;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai gastronòmic de l''Ateneu El Centre."}, {"la": "es", "text": ""}]}'
where ID = 7764;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7765;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arts grafiques i Venda de productes ecologics."}, {"la": "es", "text": ""}]}'
where ID = 7766;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma interassociativa d''entitats juvenils de BCN"}, {"la": "es", "text": ""}]}'
where ID = 7767;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de cluns de fitness"}, {"la": "es", "text": ""}]}'
where ID = 7768;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Clúster pel desenvolupament de l''economia social del treball. Està integrat per entitats socials sense ànim de lucre titulars d''un centre especial de treball (CET) o una empresa d''inserció (EI). "}, {"la": "es", "text": ""}]}'
where ID = 7769;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació excursionista."}, {"la": "es", "text": ""}]}'
where ID = 7770;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7771;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Enginyeria i Consultoria Energètica"}, {"la": "es", "text": ""}]}'
where ID = 7772;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament a les cooperatives"}, {"la": "es", "text": ""}]}'
where ID = 7773;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i desenvolupament en ciències socials."}, {"la": "es", "text": ""}]}'
where ID = 7774;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Importador i distribuidor de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7775;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessorament i recerca per la sostenibilitat."}, {"la": "es", "text": ""}]}'
where ID = 7776;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció laboral mitjançant diverses activitats."}, {"la": "es", "text": ""}]}'
where ID = 7777;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ateneu social i cultural "}, {"la": "es", "text": ""}]}'
where ID = 7778;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluitar contra la pobresa i l''exclusió social."}, {"la": "es", "text": ""}]}'
where ID = 7779;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola d''infantil, primària i secundària."}, {"la": "es", "text": ""}]}'
where ID = 7780;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d''edició i creació gràfica."}, {"la": "es", "text": ""}]}'
where ID = 7781;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Treball social en inclusió sociolaboral i cohesió veïnal "}, {"la": "es", "text": ""}]}'
where ID = 7782;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acollir infants i families en risc d''exclusió social"}, {"la": "es", "text": ""}]}'
where ID = 7783;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació sense ànim de lucre amb fins socials"}, {"la": "es", "text": ""}]}'
where ID = 7784;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis d´atenció a la persona."}, {"la": "es", "text": ""}]}'
where ID = 7785;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial i  acció cultural sociopolitica i econ. Som de Xes"}, {"la": "es", "text": ""}]}'
where ID = 7786;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers i Acompanyament processos."}, {"la": "es", "text": ""}]}'
where ID = 7787;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de formació per la inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7788;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria Social"}, {"la": "es", "text": ""}]}'
where ID = 7789;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa dedicada a l''assessorament empresarial dins dels àmbits de les vendes i el màrqueting, la gestió orientada en els valors i la formació d''emprenedoria i inserció laboral."}, {"la": "es", "text": ""}]}'
where ID = 7790;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició publicació La Marea."}, {"la": "es", "text": ""}]}'
where ID = 7791;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Suport a les finances ètiques i a l''emprenedoria social."}, {"la": "es", "text": ""}]}'
where ID = 7792;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Aportem solucions integrals a tot tipus d''indústries mitjançant implantació i certificació de sistemes de Gestió ISO, assessorament i formació perquè aconsegueixin els més alts estàndards de qualitat exigits pels seus clients."}, {"la": "es", "text": ""}]}'
where ID = 7793;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de consum responsable"}, {"la": "es", "text": ""}]}'
where ID = 7794;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de promoció, difusió i distribució de Comerç Just"}, {"la": "es", "text": ""}]}'
where ID = 7795;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONGD"}, {"la": "es", "text": ""}]}'
where ID = 7796;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "MEDIACIÓ ASSEGURANCES PER A ENTITATS SOCIALS"}, {"la": "es", "text": ""}]}'
where ID = 7797;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "centre mèdic"}, {"la": "es", "text": ""}]}'
where ID = 7798;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Tot Raval és una a plataforma de 48 associacions, institucions, persones i empreses vinculades al Raval que es crea el 2002 amb un objectiu comú: la millora de la qualitat de vida en el barri. Es desenvolupa un treball comunitari, partint d’una visió integral del barri, que incideix en els àmbits social, cultural i econòmic i comercial."}, {"la": "es", "text": ""}]}'
where ID = 7799;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Solucions integrals (sectors energètics i industrials)."}, {"la": "es", "text": ""}]}'
where ID = 7800;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció, distribució i venda de productes ecològics."}, {"la": "es", "text": ""}]}'
where ID = 7801;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "impulsar el desenvolupament i l''ús de la custòdia del territori com a eina de conservació."}, {"la": "es", "text": ""}]}'
where ID = 7802;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7803;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "DELTA gent activa es una entitat sense ànim de lucre, ONG/ONL, que treballant per i des de el sector social, es dirigeix cap a les persones grans i a les que sense ser-lo encara, tenen inquietuds a la vora d''aquest sector de la nostra societat i desitgen participar i/o col·laborar de forma ACTIVA per ajudar a la millora de la qualitat de vida del mateix i a la seva plena integració. "}, {"la": "es", "text": ""}]}'
where ID = 7804;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Administració de finques."}, {"la": "es", "text": ""}]}'
where ID = 7805;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que centre la seva activitat a la defensa de la llengua, la cultura, la cohesió social, l''educació i la identitat nacional de Catalunya. \n\n"}, {"la": "es", "text": ""}]}'
where ID = 7806;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "CERESNATURAL és una cooperativa que treballa per la salut a través de la venda de productes ecològics de proximitat i de la realització de teràpies naturals. La nostra motivació és oferir un espai que fomenti àmpliament la sostenibilitat, el comerç just, i la salut. Sent el nostre objectiu final el fet d’aconseguir que cadascú es faci responsable de la seva pròpia salut i la del planeta."}, {"la": "es", "text": ""}]}'
where ID = 7807;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de segon grau de restauració i dinamització social, que ofereix els seus serveis dins del Centre, Ateneu Democràtic i Progressista. Actualment està formada per set persones sòcies."}, {"la": "es", "text": ""}]}'
where ID = 7808;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum de productes ecobiològics del barri del Congrés (Barcelona)"}, {"la": "es", "text": ""}]}'
where ID = 7809;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsar i elaborar els materials Escuracassoles."}, {"la": "es", "text": ""}]}'
where ID = 7810;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Ateneu Santcugatenc va ser creat l’any 1956 per un grup de ciutadans santcugatencs afeccionats al col·leccionisme, la numismàtica, la medallística i la filatèlia. L’any 1977 fou registrat com associació sense ànim de lucre amb la finalitat de fomentar també l’expansió artística, científica i/o la divulgació cultural, promovent el seu màxim desenvolupament i facilitant els coneixements que sobre aquestes matèries requereixin els seus associats/des. Amb el pas dels anys va anar incorporant als seus estatuts nous fins socials relacionats amb la cultura, l’associacionisme i el lleure, entre d’altres. \n\nDes de fa més de 15 anys desenvolupa un programa de cursos i tallers amb una bona acceptació per part de la ciutadania. L’entitat té la capacitat d’organitzar un programa trimestral de 70 disciplines diferents que posa a disposició de la població més de 1.400 places de formació i lleure. La xifra d’ocupació del darrer any 2015, només en aquest projecte, ha estat de 3.053 inscripcions. \n\nPel que fa l’àmbit de Cultura, a banda de tota la programació de cursos i tallers, realitza accions per al foment de la lectura, la gastronomia, la cultura popular, exposicions, concurs de pessebres, Festa de Tardor i, esporàdicament, Microespectacles entre d’altres accions. Així com activitats per a nens i nenes com casals d’estiu o vacances de Nadal.\n\nL’Ateneu realitza un seguit d’activitats en el camp de les polítiques socials que l’han fet mereixedor d’un conveni amb aquest àmbit de l’Ajuntament de Sant Cugat, per desenvolupar: l’Oficina del Voluntariat, l’Espai de Lleure (Lleure Social per a persones amb Trastorn Mental Sever), Joves Jubilats (envelliment actiu) i Català per a Tothom (projecte de foment de l’ús social del català i acollida sociocultural de persones nouvingudes). \n\nUn dels altres àmbits on ha tingut intervenció ha estat l’educatiu. Coordinant-se amb el Pla Educatiu d’Entorn dinamitza els Tallers d’Estudi Assistit per alumnes de secundaria de l’institut Arnau Cadell, el col·legi Pureza de Maria i l’Avenç, per lluitar contra el fracàs escolar, així com els grups de conversa i aprenentatge de català, directament a les escoles de primària, amb pares i mares de nens i nenes nouvinguts. \n\nO també activitats de sensibilització ambiental i promoció del consum responsable, ecològic, de proximitat i la reducció de residus, com el Mercat de Pagès, els Mercats de 2a mà o la sensibilització contra l’ús de bosses de plàstic.\n\nA més a més el fet d’haver pogut disposar fins ara de l’edifici de plaça Pep Ventura, 1, al nucli antic de la ciutat, obert a la ciutadania i a les entitats, permet gaudir d’espais de trobada i relació, exposicions, xerrades i conferències, presentacions de llibres, reunions o assajos. En aquests moment diferents entitats o col·lectius usen les instal·lacions de l’Ateneu per a desenvolupar les seves activitats de manera estable, com per exemple: Amics dels Veterans i la Penya Regalèssia, Comissió Festa de Tardor, Institut Trastorn Límit Fundació, Federació d’Associacions de Veïns, Grup de Jocs de Rol, Grup d’estudis de Teosofia, Pipa Club Sant Cugat, Societat Gastronòmica del Vallès. Alguns a més utilitzen la infraestructura i personal de l’Ateneu per a l’organització de les seves activitats, recepció d’usuaris i/o inscripcions.  A aquest cal sumar-hi els que de manera puntual utilitzen també la seu de l’Ateneu.\n"}, {"la": "es", "text": ""}]}'
where ID = 7811;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7812;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció audiovisual i Comunicació integral."}, {"la": "es", "text": ""}]}'
where ID = 7813;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lafede.cat està formada per 117 organitzacions i neix el gener de 2013 com a resultat de la fusió de les antigues Federació Catalana d’ONG per al Desenvolupament-FCONGD, Federació Catalana d’ONG pels Drets Humans-FCONGDH i Federació Catalana d’ONG per la Pau-FCONGPAU (vegeu història de Lafede.cat)Lafede.cat recull l’experiència i la trajectòria associativa de les seves 116 organitzacions federades però, a la vegada, aposta per la integració i l’actualització dels seus discursos, objectius i espais de relació institucional. Després d’un procés participatiu de mesos, a la seva primera Assemblea general, el 19 de juny de 2014."}, {"la": "es", "text": ""}]}'
where ID = 7814;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que va començar amb l''activitat principal d''apadrinar, adoptar i rescatar animals salvatges i de companyia. Arrel de l''experiència adquirida com a professionals del sector, a l''any 2012, FAADA va decidir iniciar un procés de transformació intern per poder incidir a les causes de forma més estratègica des de l''acció en l''àmbit social, legislatiu i educatiu. \nFacilitem informació i assessorament tècnic i jurídic a la societat sobre la protecció els animals i dirigim projectes de sensibilització que plantegin alternatives als sectors empresarials que directa o indirectament causen patiment als animals. Promovem accions formatives que incideixen en el foment de l''empatia i el respecte pels animals en els diferents cicles educatius. "}, {"la": "es", "text": ""}]}'
where ID = 7815;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Disseny per emprenedoria social i iniciatives ètiques."}, {"la": "es", "text": ""}]}'
where ID = 7816;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7817;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7818;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjançant la innovació narrativa, tecnològica i social, a COMPACTO posem la nostra creativitat al servei d’un món més lliure, solidari i participatiu. Des que vam fundar l’empresa el 2010, ens involucrem en projectes ambiciosos que transmetin els valors d’innovació, solidaritat, participació i llibertat, per enfortir l’economia social a través de la cultura i la cultura a través de l’economia social i col·laborativa."}, {"la": "es", "text": ""}]}'
where ID = 7819;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Distribució de productes ecològics, frescos i locals a les botigues i restaurants. Treballem  amb productors, cooperatives i entitats amb dificultats per fer arribar els productes als establiments oferint un servei global dins un àmbit local i creant xarxes de intercooperació local. Cafès La Chapolera és la creació pròpia de cafè ecològic d''origen amb valor social"}, {"la": "es", "text": ""}]}'
where ID = 7820;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La inserció laboral de persones en situació de risc i/o exclusió social, mitjançant la combinació de formació teòric-pràctic, social, personal i laboral de forma  que possibiliti l'' adquisició de hàbits i coneixements suficients per a poder accedir a un lloc de feina normalitzat.\nVenda de productes recicltas de segona mà i subsproductes relacionats amb els següents sectors: mobles, articles textils.."}, {"la": "es", "text": ""}]}'
where ID = 7821;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Portar a terme qualsevol activitats econòmiques de producció de béns, comercialització o de prestació de serveis que tinguin per finalitat primordial la de fer integració sociolaboral de .persones en situació o greu risc d''exclusió social.                                       "}, {"la": "es", "text": ""}]}'
where ID = 7822;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Panorama180 és una iniciativa sense ànim de lucre que es centra en el cinema i l’experimentació audiovisuals realitzats amb paràmetres del segle XXI: digitalització, xarxa, compromís, agitació, canvi, empoderament, comunitat…atentes a les novetats i propostes superadores dels problemes generats per la concepció obsoleta dels drets d’autor en l’era digital."}, {"la": "es", "text": ""}]}'
where ID = 7823;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació i informació."}, {"la": "es", "text": ""}]}'
where ID = 7824;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Borda, és una cooperativa d''habitatge en cessió d''us, ubicada al barri de Sants de Barcelona. Un cop acabades les obres (a l''estiu 2018) tindrà 28 habitatges i varis espais comunitàris. Té signat un dret de superficie amb l''Ajuntament per 75 anys. Es construeix en sol públic ( i quedarà public) i té qualificació màxima d''eficiència energètica. L’edifici costarà tres milions d’euros (no arriba a 110.000 per habitatge) i es finançia amb l’aportació inicial dels socis, un préstec de Coop 57, l’emissió de títols participatius per part de la mateixa cooperativa i aportacions o préstecs d’altres entitats. "}, {"la": "es", "text": ""}]}'
where ID = 7825;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió de projectes en l´àmbit de la Joventut."}, {"la": "es", "text": ""}]}'
where ID = 7826;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació"}, {"la": "es", "text": ""}]}'
where ID = 7827;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Matriu és un laboratori de democràcia viva, i ofereix formacions, acompanyament, recerca i creació de materials, amb la finalitat d''afavorir que persones i grups esdevinguin agents actius en la construcció de relacions i formes d’organització conscients i respectuoses.\nLa mirada dels feminismes; dóna eines per abordar la complexitat de cada una de les relacions i els grups, en què sovint actuen eixos diversos de desigualtat i opressió. Ens situa davant del repte d’atendre amb cura, a més, les dimensions múltiples de les relacions: emocions, experiències, somnis, posicionaments, coneixements, creences.\nEnfoca l’apoderament de grups i de persones; a través de generar processos de presa de consciència de la diversitat existent i de la distribució de poder que hi opera, com també de transformar els conflictes i establir mecanismes de diàleg i comunicació útils.\nAposta per la cerca de nous imaginaris polítics, noves formes d’organitzar-se, a través de concretar col·lectivament com es pot funcionar perquè les vides que vivim siguin dignes i sostenibles."}, {"la": "es", "text": ""}]}'
where ID = 7828;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ingeniería Social S.A.L. és una empresa d’economia cooperativa especialitzada en la formació, diagnòstic, definició, implementació i verificació de sistemes de gestió en Responsabilitat Social Corporativa. Ubicada a la ciutat de Barcelona, l’empresa compta amb un equip multidisciplinari format per consultores i expertes en matèria de responsabilitat social. \nLa nostra missió és assessorar a les empreses i organitzacions a desenvolupar negocis i organitzacions estables i sostenibles que aportin valor a la societat.\n"}, {"la": "es", "text": ""}]}'
where ID = 7829;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Andròmines te com a missió ser una organització de referència en la lluita i denuncia de l?exclusió social, defensant la dignitat i la igualtat d?oportunitats per totes les persones i la defensa i protecció del medi ambient.\n\nAquest darrer any Andròmines ha augmentat el dissenya i execució de programes de capacitació, formació i inserció laboral que s?adeqüen a les necessitats socials dels col?lectius i a la demanda real del mercat de treball. \n\nles empreses que contracten els serveis de gestió de residus, així com a les persones i organitzacions on arriben les campanyes d''educació ambiental i a les entitats amb les que realitzem treball en xarxa\n"}, {"la": "es", "text": ""}]}'
where ID = 7830;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Elaboració ecològica i autòctona de cerveses, most i maltes."}, {"la": "es", "text": ""}]}'
where ID = 7831;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "gdgfdgfd"}, {"la": "es", "text": ""}]}'
where ID = 7832;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7833;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7834;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7835;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7836;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball associat dedicada a la docència."}, {"la": "es", "text": ""}]}'
where ID = 7837;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA PAGE ORIGINAL, estudi de comunicació visual, es constitueix com empresa l’any 1991.\nEn l’actualitat, els seus socis Sònia Martínez Ruzafa i Josep Martínez Ruzafa (llicenciats en Belles Arts per la Universitat de Barcelona) dirigeixen projectes especialitzats en comunicació social, institucional i empresarial."}, {"la": "es", "text": ""}]}'
where ID = 7838;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7839;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7840;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació Vella Terra és una entitat sense ànim de lucre que neix amb el ferm compromís de promocionar l''autonomia de les persones depenents, i donar continuitat i sentit al seu projecte de vida. Els principals col·lectius atesos son persones grans, persones amb discapacitat, i persones tutelades."}, {"la": "es", "text": ""}]}'
where ID = 7841;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Instituto de Economía Social y Solidaria"}, {"la": "es", "text": ""}]}'
where ID = 7842;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7843;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de Servicios para el Mercado Social de Aragón"}, {"la": "es", "text": ""}]}'
where ID = 7844;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "eee"}, {"la": "es", "text": ""}]}'
where ID = 7845;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7846;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un agència de disseny i desenvolupament web i d’aplicacions al servei de la transformació social.Estem aquí per facilitar a les organitzacions la incorporació de la innovació tecnològica i ajudar-les a superar l''escletxa digital.Posem a l’abast de tothom el potencial que ofereix la tecnologia, adaptant-nos a les necessitats i possibilitats de cada projecte i acompanyant-lo amb un equip creatiu, multidisciplinar i proper que posa l’accent en l’accessibilitat, la pedagogia, el treball col·laboratiu i la perspectiva de gènere.Apostem per les tecnologies lliures com a instrument per assolir la sobirania tecnològica efectiva."}, {"la": "es", "text": ""}]}'
where ID = 7847;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7848;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestiona 19 habitatges dignes per a famílies en situació de vulnerabilitat i/o d''exclusió social. Acolliment i cessió d''ús dels habitatges per un període màxim de 3 anys."}, {"la": "es", "text": ""}]}'
where ID = 7849;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hosteleria mixta."}, {"la": "es", "text": ""}]}'
where ID = 7850;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 7851;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(la)baula: peça en forma d’anella que, enllaçada amb altres similars, forma una cadena.\n\nlabaula és una cooperativa d’arquitectes amb trajectòries professionals diverses que han decidit cooperar i sumar esforços per construir un projecte comú amb una metodologia de treball horitzontal.\n\nEl camp de treball de labaula consisteix en projectes d’edificació, rehabilitació, disseny industrial, urbanisme, espais públics, escenografia i espais efímers. Encarem els treballs amb idèntica dedicació i esforç, i amb independència de les seves dimensions o característiques. Aquests són entesos sempre com un espai de debat on s’incorporen col·laboradors d’altres disciplines per enriquir les perspectives pròpies.\n\nlabaula neix de la nostra il·lusió i inquietud per intentar fomentar la qualitat, el rigor i la responsabilitat en la nostra feina. Esperem poder ser una plataforma des d’on seguir treballant en aquesta direcció."}, {"la": "es", "text": ""}]}'
where ID = 7852;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7853;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fabricació de motlles i matrius per automoció."}, {"la": "es", "text": ""}]}'
where ID = 7854;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Teixits amb tellers manuals."}, {"la": "es", "text": ""}]}'
where ID = 7855;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió veïnal de barri."}, {"la": "es", "text": ""}]}'
where ID = 7856;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocions d´iniciatives socials i económiques."}, {"la": "es", "text": ""}]}'
where ID = 7857;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social."}, {"la": "es", "text": ""}]}'
where ID = 7858;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis financers."}, {"la": "es", "text": ""}]}'
where ID = 7859;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Reparacions i reformes."}, {"la": "es", "text": ""}]}'
where ID = 7860;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comercialització de complements."}, {"la": "es", "text": ""}]}'
where ID = 7862;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Menjador social, Cooperativa consum i Secció local coop57."}, {"la": "es", "text": ""}]}'
where ID = 7863;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Concepte, disseny, producció i muntatge d´exposicions."}, {"la": "es", "text": ""}]}'
where ID = 7865;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projectes i Formació pel lleure."}, {"la": "es", "text": ""}]}'
where ID = 7866;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Roba de llit feta amb justícia a Costa d''Ivori i Burkina"}, {"la": "es", "text": ""}]}'
where ID = 7869;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Auditories Comptes i Subvencions - ROAC 12236."}, {"la": "es", "text": ""}]}'
where ID = 7870;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Càtering agroecològic."}, {"la": "es", "text": ""}]}'
where ID = 7872;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria social."}, {"la": "es", "text": ""}]}'
where ID = 7874;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mitjà de comunicació."}, {"la": "es", "text": ""}]}'
where ID = 7875;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Servei de contacte entre persones per tot tipus de serveis."}, {"la": "es", "text": ""}]}'
where ID = 7876;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis del món de les TICs."}, {"la": "es", "text": ""}]}'
where ID = 7878;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dinamització col·lectiva de producció agroecològica local."}, {"la": "es", "text": ""}]}'
where ID = 7879;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Traduccions i interpretacions."}, {"la": "es", "text": ""}]}'
where ID = 7880;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Rutes guiades en bicicleta i bici-escola d''adults."}, {"la": "es", "text": ""}]}'
where ID = 7881;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''educomunicacio dels Països Catalans. Expertes en dinamitzar projectes de ràdio escolar, podcast i justícia global."}, {"la": "es", "text": ""}]}'
where ID = 7882;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Edició de mitjans digitals."}, {"la": "es", "text": ""}]}'
where ID = 7883;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Casal Popular Boira Baixa de Manlleu. Punt de trobada per l''alliberament nacional, social i de gènere. "}, {"la": "es", "text": ""}]}'
where ID = 7884;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca i Formació."}, {"la": "es", "text": ""}]}'
where ID = 7885;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inserció juvenil mitjançant el reciclatge i reutilització."}, {"la": "es", "text": ""}]}'
where ID = 7886;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Impulsa projectes pel bé comú i l’economia solidària amb l’objectiu que la ciutadania assoleixi els drets fonamentals per una societat justa on totes siguem iguals davant la llei, gaudim de la llibertat en la cultura i fem possible la fraternitat econòmica."}, {"la": "es", "text": ""}]}'
where ID = 7887;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai públic, participació, infància."}, {"la": "es", "text": ""}]}'
where ID = 7888;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai de gestió comunitaria al barri de Porta."}, {"la": "es", "text": ""}]}'
where ID = 7889;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunicació audiovisual per a la transformació social."}, {"la": "es", "text": ""}]}'
where ID = 7890;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dansa amb impacte social."}, {"la": "es", "text": ""}]}'
where ID = 7891;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Marxandatge ecològic i de comerç just."}, {"la": "es", "text": ""}]}'
where ID = 7892;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sobirania alimentaria bassant en comercialització i consum."}, {"la": "es", "text": ""}]}'
where ID = 7893;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Activitats culturals, artístiques i coaching."}, {"la": "es", "text": ""}]}'
where ID = 7894;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestió comunitària d''equipament sociocultural."}, {"la": "es", "text": ""}]}'
where ID = 7895;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Buscar formules cooperatives per lluitar contra el atur."}, {"la": "es", "text": ""}]}'
where ID = 7896;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Lluita en xarxa contra l''atur, Formació i Auto-ocupació."}, {"la": "es", "text": ""}]}'
where ID = 7897;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recollida de ferralla. Reciclatge. Sensibilització."}, {"la": "es", "text": ""}]}'
where ID = 7898;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Exercici de l''Advocacia."}, {"la": "es", "text": ""}]}'
where ID = 7899;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció cohabitatge."}, {"la": "es", "text": ""}]}'
where ID = 7900;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció de l''educació lliure i el lleure en família."}, {"la": "es", "text": ""}]}'
where ID = 7901;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7902;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Somos una asociación ONG de solidaridad internacional a donde uno de los ejes de nuestra acción es la Economía Social."}, {"la": "es", "text": ""}]}'
where ID = 7903;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació creada al 2014 amb l''objectiu de treballar per i amb les famílies del barri de Sants-Montjuïc amb infants d''entre 0 i 6 anys. Els mou el joc lliure, l''educació respectuosa i la creativitat. \nMitjançant una escola d''educació lliure, taller i activitats dirigides d''oci de qualitat en família i La Carpa de Tata Inti, la nostra reversió de les ludoteques tradicionals."}, {"la": "es", "text": ""}]}'
where ID = 7904;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som un grup de persones que ens trobem cada setmana per construïr conjuntament una manera de consumir més justa, més conscient i més responsable."}, {"la": "es", "text": ""}]}'
where ID = 7905;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cal Temerari és un equipament cultural, social i cooperatiu ciutadà. Té l''objectiu de potenciar la participació i fomentar iniciatives socials i transformadores a Sant Cugat."}, {"la": "es", "text": ""}]}'
where ID = 7906;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(registre duplicat)\n\nEquipament de gestió comunitària del Barri de Porta, Nou Barris, i la Federació d''Entitats Transforma Porta té un conveni directe amb l''Ajuntament de Barcelona per a poder dinamitzar l''espai. Actualment hi ha 17 col·lectius a la Gestora ("La Totxana"), des d''on de forma assembleària i horitzontal s''organitza la vida de la Bòbila. Les temàtiques principals del projecte són: la transformació del barri de Porta a partir de posar en pràctica el desenvolupament comunitari, l''Economia Social i Solidària, l''agroecologia urbana, el Programari Lliure, les Festes Populars, etc. Al centre de tota l''activitat i volem posar "la cura", per això hem generat una Comissió específica per poder fer de l''Ateneu un espai d''acollida a on fomentar la part reproductiva del projecte i fer visible el que històricament ha estat invisible. En aquest sentit i per aconseguir aquest objectiu, creiem fonamental el paper del Banc Comunitari del Temps de Porta."}, {"la": "es", "text": ""}]}'
where ID = 7907;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7908;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7909;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7910;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Voltes som una cooperativa d’arquitectura i urbanisme que enfoquem la nostra pràctica com una eina de transformació social. La cooperativa està especialitzada en bioconstrucció i sostenibilitat, rehabilitació, i participació ciutadana. "}, {"la": "es", "text": ""}]}'
where ID = 7911;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7912;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7913;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7914;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7915;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7916;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7917;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7918;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7919;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Unió de Cooperadors de Mataró és una entitat privada participada per l’Ajuntament de Mataró i la Unió de Cooperadors de Mataró, que té per objecte la defensa i promoció de l’economia social en l’àmbit local de la ciutat de Mataró, català i a nivell internacional, mitjançant la realització d’actuacions de formació, informació, assessorament i estudi."}, {"la": "es", "text": ""}]}'
where ID = 7920;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7921;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som la federació d’AFA més gran de Catalunya, tant a primària com a secundària. Som més de 2.360 AFA repartides en més de 750 municipis i representem més de 540.000 famílies.\n"}, {"la": "es", "text": ""}]}'
where ID = 7922;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Col·legi Oficial de Psicologia de Catalunya és un espai professional, sense ànim de lucre, que actua com a frontissa entre l''espai acadèmic (ciència) i l''espai públic (societat). \nVetlla per la qualitat de les intervencions professionals, preservant i donant suport a la praxi de cada actuació, garantint als ciutadans l’òptima qualitat de l''atenció i el servei. El COPC innovarà, desenvoluparà i implementarà serveis professionals competitius en consonància amb l''exercici de la professió i fomentarà els avantatges derivats del pes de ser un col·lectiu. El Col·legi de Psicologia de Catalunya es regirà pels valors legals i ètics en vigor, per les pautes que les Institucions determinin i les que la deontologia pròpia dictamini.\nEl COPC vol ésser la principal associació professional de la psicologia a Catalunya, promovent valors que ofereixin rellevància institucional i prestigiïn els seus membres, de manera que aquests trobin en el seu sí, el millor suport per el creixement personal i professional.\n"}, {"la": "es", "text": ""}]}'
where ID = 7923;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una cooperativa dedicada a la sonorització, il·luminació d''esdeveniments, projecció de projectes culturals i serveis tècnics al servei de la cultura."}, {"la": "es", "text": ""}]}'
where ID = 7924;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa d’iniciativa social dedicada a la promoció de la diversitat en un sentit ampli, així com a oferir eines per a la seva gestió en diversos àmbits socials. Alhora, ens proposem posar en evidència i contribuir a superar les dinàmiques d’exclusió basades en la diversitat."}, {"la": "es", "text": ""}]}'
where ID = 7925;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una ONGD catalana fundada al 2004 per un grup de persones amb experiència al camp de la cooperació que van decidir creau un grup per tenir un compromís estable i solidari amb el Colectivo de Mujeres de Matagalpa."}, {"la": "es", "text": ""}]}'
where ID = 7928;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació es va constituir per crear una entitat financera ètica que intentés omplir el forat que ha quedat amb la desaparició i estafa de les caixes d''estalvi."}, {"la": "es", "text": ""}]}'
where ID = 7930;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som – Fundació   Catalana  Tutelar  és una entitat sense ànim de lucre que defensa els drets,  protegeix i vetlla per la qualitat de vida de les persones amb discapacitat intel·lectual o del desenvolupament,  i  amb  la  capacitat modificada judicialment a Catalunya. \n"}, {"la": "es", "text": ""}]}'
where ID = 7931;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social especialista en assegurances personals amb més de vuitanta anys de plena dedicació i compromís amb el mutualisme i vinculada a diferents àmbits i actors de l’economia social. Previsora General treballa per ser una alternativa a l’assegurança tradicional. Posa l’economia al servei de les persones amb l’objectiu d’aconseguir una societat més amable i solidària i busca noves solucions per generar valor per als seus clients."}, {"la": "es", "text": ""}]}'
where ID = 7932;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7933;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Volem transformar la societat des de l’àmbit educatiu tan de lleure com del món comunitari, acompanyant infants, joves i adults perquè esdevinguin lliures i compromeses amb el col·lectiu. "}, {"la": "es", "text": ""}]}'
where ID = 7934;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONG, treballem ESS a Catalunya i en pasïos fora com ara: Palestina, Tunisia, República Dominicana."}, {"la": "es", "text": ""}]}'
where ID = 7935;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Boscana atén a persones amb discapacitat intel·lectual des de 1961 oferint serveis residencials i de teràpia ocupacional."}, {"la": "es", "text": ""}]}'
where ID = 7936;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La FNSDA és una entitat tutelar de persones grans am la capacitat d''obrar modificada per resolució judicial o bé incurses en un procediment de modificació de la capacitat d''obrar. La missió de la Fundació és la protecció de la gent gran en totes les diferents dimensions de la persona fent una aposta decidida per promoure  la seva qualitat  de vida, respectant la seva dignitat i a defensa dels seus drets"}, {"la": "es", "text": ""}]}'
where ID = 7937;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial"}, {"la": "es", "text": ""}]}'
where ID = 7938;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7939;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7940;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Agrupem a més de 230 entitats d’iniciativa social, no lucratives, del sector de la discapacitat intel·lectual, que gestionen més de 700 serveis que cobreixen les necessitats vitals de les persones amb discapacitat: atenció precoç, educació, treball, atenció diürna, habitatge, lleure i tutela.\n\nLa nostra missió és representar i defensar els interessos empresarials de les nostres entitats associades. Treballem per aconseguir el màxim reconeixement i confiança dels associats, com institució que representa els seus interessos i com entitat col·laboradora en l’assoliment dels seus objectius.\n\n\n"}, {"la": "es", "text": ""}]}'
where ID = 7941;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producció agricultura ecológica"}, {"la": "es", "text": ""}]}'
where ID = 7942;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa de treball protegit, Centre Especial de Treball que dóna ocupación a persones amb discapacidad intel·lectual."}, {"la": "es", "text": ""}]}'
where ID = 7943;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Cafè de Mar de Mataró és un espai emblemàtic del cooperativisme a la ciutat amb gairebé 90 anys d’història. Gràcies al treball de la Unió de Cooperadors de Mataró (UCM) i la Fundació Unió de Cooperadors de Mataró (FUCM), actualment, és un edifici catalogat d’interès local que treballa en la difusió i la promoció del cooperativisme i l’Economia Social i Solidària a la ciutat. Un dels espais d’aquest edifici el gestionem una cooperativa jove formada per cinc sòcies; L’Ariet.SCCL, que, amb l’ajuda i el suport de la UCM i de moltes entitats i persones que van confiar en el projecte, el juny de 2014 vam decidir obrir les portes l’Espai Culinari del Cafè de Mar amb la intenció de tornar a reobrir l’històric espai de Cafè del Cafè de Mar. A l’Espai Culinari del Cafè de Mar treballem no només en una oferta de restauració variada i accessible per tothom sinó també en una línia de programació i activitats culturals per tal de teixir una xarxa de convivència i d’intercanvi d’experiències a la ciutat."}, {"la": "es", "text": ""}]}'
where ID = 7944;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "gimnàs social"}, {"la": "es", "text": ""}]}'
where ID = 7945;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "GSIS és una Cooperativa Mixta de treball associat i consum, que té per objecte la integració social de les persones amb discapacitat intel·lectual.\nConstituïda el dos de febrer de mil nou-cents noranta-sis, per un grup de familiars de persones amb discapacitat intel·lectual. GSIS és una Entitat de serveis socials i d’Iniciativa social, sense afany de lucre en cap de les seves activitats."}, {"la": "es", "text": ""}]}'
where ID = 7946;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7947;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa de Green Márketing"}, {"la": "es", "text": ""}]}'
where ID = 7948;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Integramenet vol ser una entitat que potencia la interculturalitat, afavoreix la promoció integral de la persona i facilita el procés d’integració a la nostra ciutat i al propi país adquirint un sentit de pertinença al mateix."}, {"la": "es", "text": ""}]}'
where ID = 7949;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7950;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El COAMB té com a missió representar, defensar i impulsar el paper dels ambientòlegs, així com els professionals del medi ambient i estudiants, ordenar l’exercici de la seva professió, vetllar per l’ètica professional, i promoure el respecte al medi ambient entre la societat.\n\nPel que fa a la visió del Col·legi d’Ambientòlegs, aquesta és esdevenir el referent dels professionals del medi ambient de Catalunya oferint els serveis que aquests requereixen i mantenint la projecció institucional, social i ambiental del Col·legi per tal de reforçar la projecció de la professió.\n\nPer projectar aquesta visió es fomentaran iniciatives centrades en l’impuls de la inserció laboral, l’auto-ocupació i l’emprenedoria; la promoció de la recerca ambiental i el posicionament dels ambientòlegs com a investigadors i docents; la qualitat i la coherència dels plans d’estudis de Ciències Ambientals; i el debat i la creació d’opinió sobre els reptes i les polítiques ambientals i de sostenibilitat a Catalunya.\n\nFinalment, els principals valors del COAMB són: compromís, fer xarxa, il·lusió, professionalitat i transversalitat."}, {"la": "es", "text": ""}]}'
where ID = 7951;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7952;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Accesorios y ropa RESPONSABLE para mascostas , contribuyendo a disminuir el impacto ambiental y social de la actividad empresarial."}, {"la": "es", "text": ""}]}'
where ID = 7953;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Coopdevs és una associació de persones que de forma voluntària i sense ànim de lucre col·laboren per crear solucions tecnològiques de codi obert per a promoure l''economia social. "}, {"la": "es", "text": ""}]}'
where ID = 7954;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7955;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Casal popular autogestionat de Nou Barris"}, {"la": "es", "text": ""}]}'
where ID = 7956;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "empresa instal.ladora d''energies renovables"}, {"la": "es", "text": ""}]}'
where ID = 7957;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una entitat sense ànim de lucre  creada per promoure l''ús dels productes de suport i la tecnologia amb la finalitat de millorar la qualitat de vida de les persones dependents i amb discapacitat"}, {"la": "es", "text": ""}]}'
where ID = 7958;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que treballa per aconseguir la inserció laboral i social de persones en situació de vulnerabilitat"}, {"la": "es", "text": ""}]}'
where ID = 7959;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultora en Sistemes d''informació geogràfica que busca ampliar l''ús dels Sistemes d''informació geogràfica (SIG) , potenciar la seva aplicació en àmbits en els que encara no s''estan utilitzant, i posar-los a l''abast d''empreses i usuaris"}, {"la": "es", "text": ""}]}'
where ID = 7960;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació sense ànim de lucre amb l’objectiu d’enfortir els lligams entre les persones i molt especialment els joves. L’eina escollida en un primer moment va ser el teatre, però després s’hi van afegir altres disciplines artístiques com ara: les arts plàstiques, la dansa, la música i el circ.  En definitiva, ens servim de l’art com a eina per a l’educació i la transformació social."}, {"la": "es", "text": ""}]}'
where ID = 7961;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "és una organització empresarial sense ànim de lucre creada l''any 1992, que agrupa els Centres Especials de Treball de Catalunya."}, {"la": "es", "text": ""}]}'
where ID = 7962;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat no governamental sense ànim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7963;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dimmons fomenta la innovació socioeconòmica a través de la recerca, l''experimentació metodològica i l''acció per a una societat orientada als comuns"}, {"la": "es", "text": ""}]}'
where ID = 7964;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7967;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7968;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre dedicada a l''apodarament de persones perquè es reintegrin al mercat laboral. "}, {"la": "es", "text": ""}]}'
where ID = 7969;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Facilitadora"}, {"la": "es", "text": ""}]}'
where ID = 7970;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació per la inserció laboral de persones amb Esclerosi Múltiple"}, {"la": "es", "text": ""}]}'
where ID = 7971;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació editora del diari digital local elcugatenc.cat"}, {"la": "es", "text": ""}]}'
where ID = 7972;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7973;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació per la lluita contra l''estigma en salut mental"}, {"la": "es", "text": ""}]}'
where ID = 7974;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Mutualitat de previsió social - assegruances per persones"}, {"la": "es", "text": ""}]}'
where ID = 7975;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ENTITAT SENSE ÀNIM DE LUCRE"}, {"la": "es", "text": ""}]}'
where ID = 7976;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7977;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7978;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7979;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense ànim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7980;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "INSERCIÓ SOCIO-LABORAL DE JOVES VULNERABLES"}, {"la": "es", "text": ""}]}'
where ID = 7981;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7982;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espigoladors som una organització social sense afany de lucre que treballem per reduir el malbaratament alimentari. La nostra tasca principal consisteix en recollir, amb col·laboració amb productors del territori, producte agrícola que per diferents motius no es pot vendre o s’ha descartat del circuit comercial i es susceptible de ser rebutjat. La recollida la realitzem amb persones voluntàries i el 95% de la fruita i verdura recuperada la donem a entitats socials que faciliten l''accés d''una alimentació adequada a persones en situació vulnerable. El 5% restant el transformem al nostra obrador per elaborar conserves artesanes, sense additius i amb valor social sota la marca \\"es im-perfect\\". A l''obrador donem segones oportunitats a persones en situació vulnerable. D''altra banda, també fem tasca de sensibilització i projectes sobre malbaratament i, entre altres activitats, oferim tallers de cuina d’aprofitament, campanyes, xerrades i activitats diverses orientades a sensibilitzar i fer front a aquesta problemàtica social, econòmica i ambiental."}, {"la": "es", "text": ""}]}'
where ID = 7983;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7984;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ENTITAT SENSE ÀNIM DE LUCRE QUE REALITZA ACTIVITATS FORMATIVES,SOCIALS, CULTURALS, ARTÍSTIQUES, ESCÈNIQUES I ESPORTIVES"}, {"la": "es", "text": ""}]}'
where ID = 7985;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 7986;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat dedicada a la inserció sociolaboral i a la formació professionalitzadora de persones en situació de vulnerabilitat social a través d''activitats vinculades a la gestió integral de residus. "}, {"la": "es", "text": ""}]}'
where ID = 7987;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Educatiu Waldorf"}, {"la": "es", "text": ""}]}'
where ID = 7988;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de Consum"}, {"la": "es", "text": ""}]}'
where ID = 7989;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum responsable"}, {"la": "es", "text": ""}]}'
where ID = 7990;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació privada sense ànim de lucre"}, {"la": "es", "text": ""}]}'
where ID = 7991;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "iniciativa nascuda de la mà del Professor Muhammad Yunus, Premi Nobel de la Pau 2006, que té com a objectiu impulsar i donar suport a l’empresa social"}, {"la": "es", "text": ""}]}'
where ID = 7992;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Forn de pa ecologic"}, {"la": "es", "text": ""}]}'
where ID = 7993;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Agrobotiga de proximitat"}, {"la": "es", "text": ""}]}'
where ID = 7994;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció"}, {"la": "es", "text": ""}]}'
where ID = 7995;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Busquem oportunitats per les persones amb diversitat funcional d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7996;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre especial de treball de l''Associació Disminuïts Físics d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 7997;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "comerç al menor"}, {"la": "es", "text": ""}]}'
where ID = 7998;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cafeteria dins d''un equipament públic de gestió ciutadana"}, {"la": "es", "text": ""}]}'
where ID = 7999;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació cultural"}, {"la": "es", "text": ""}]}'
where ID = 8000;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8001;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8002;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8003;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "EMPRESA D''INSERCIÓ"}, {"la": "es", "text": ""}]}'
where ID = 8004;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense ànim de lucre que desenvolupa projectes en l''àmbit de la ocupació i la formació "}, {"la": "es", "text": ""}]}'
where ID = 8005;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8006;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Creative Director"}, {"la": "es", "text": ""}]}'
where ID = 8007;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa que ofereix serveis de consultoria, comunicació i producció"}, {"la": "es", "text": ""}]}'
where ID = 8008;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Llibreria"}, {"la": "es", "text": ""}]}'
where ID = 8009;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "’IRTA és un institut d’investigació de la Generalitat de Catalunya, adscrit al Departament d’Agricultura, Ramaderia, Pesca i Alimentació, regulat per la Llei 04/2009 de 15 d’abril, del Parlament de Catalunya, que ajusta la seva activitat a l’ordenament jurídic privat. La finalitat de l’IRTA és contribuir a la modernització, a la millora i l’impuls de la competitivitat; al desenvolupament sostenible dels sectors agrari, alimentari, agroforestal, aqüícola i pesquer, i també dels directament o indirectament relacionats amb el proveïment d’aliments sans i de qualitat als consumidors finals; a la seguretat alimentària i a la transformació dels aliments, i, en general, a la millora del benestar i la salut de la població. Els seus objectius generals són els d’impulsar la recerca i el desenvolupament tecnològic dins l’àmbit agroalimentari, facilitar la transferència dels avenços científics i valorar els avenços tecnològics propis tot cercant la màxima coordinació i col·laboració amb el sector públic i privat. L’IRTA ha promogut, des de la seva creació, l’establiment d’acords permanents de col·laboració amb altres institucions públiques que actuen en l’àmbit de la recerca i desenvolupament tecnològic a Catalunya. Aquesta política ha donat com a resultat l’existència, avui dia a Catalunya, d’una xarxa de centres consorciats (entre l’IRTA, Universitats, CSIC, Diputacions, etc.) que pot definir-se com la d’un sistema cooperatiu d’R+D."}, {"la": "es", "text": ""}]}'
where ID = 8010;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "projectes i serveis integrals d''enginyeria"}, {"la": "es", "text": ""}]}'
where ID = 8011;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8012;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de mares de dia per promoure aquesta figura profesional en la criança"}, {"la": "es", "text": ""}]}'
where ID = 8013;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8014;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8015;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8016;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8017;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Inspiració, eines i mentoring per generar projectes amb impacte positiu"}, {"la": "es", "text": ""}]}'
where ID = 8018;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat sense afany de lucre fundada l''any 2004 a Barcelona amb l''objectiu de contribuir a una transformació social basada en l''educació en valors amb perspectiva feminista i comunitària "}, {"la": "es", "text": ""}]}'
where ID = 8019;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "test"}, {"la": "es", "text": ""}]}'
where ID = 8020;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Quart Món és una entitat de voluntariat sense ànim de lucre que acompanya famílies d’origen galaicoportuguès residents a Barcelona en el seu procés d’inclusió social. "}, {"la": "es", "text": ""}]}'
where ID = 8021;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8022;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de recerca i consultoria en política econòmica i comunitària"}, {"la": "es", "text": ""}]}'
where ID = 8023;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8024;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8025;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Regidoria participacio"}, {"la": "es", "text": ""}]}'
where ID = 8026;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8027;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8028;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Prova Balanç Comunitari d''Orlandai"}, {"la": "es", "text": ""}]}'
where ID = 8029;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8030;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''Habitatge de Lloguer Assequible"}, {"la": "es", "text": ""}]}'
where ID = 8031;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació ecom neix el 2007 amb l''objectiu de fomentar i gestionar el coneixement dels diferents col·lectius de persones amb discapacitat física i les seves necessitats. La seva finalitat és prestar-los la millor atenció, gestionant, quan sigui necessari, i sense entrar en competència amb les organitzacions del moviment associatiu ecom, programes i serveis."}, {"la": "es", "text": ""}]}'
where ID = 8032;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació gestora del Kasal de Joves de Roquetes"}, {"la": "es", "text": ""}]}'
where ID = 8033;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8034;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat constituïda per totes les colles de cultura popular del districte de Gràcia (L''Àliga de Gràcia,Bastoners de Gràcia, Bastoners de Barcelona, Castellers de la Vila de Gràcia, la Diabòlica de Gràcia,el Drac de Gràcia, els Gegants de Gràcia, la Malèfica del Coll, la Vella de Gràcia i els Trabucaires de Gràcia)."}, {"la": "es", "text": ""}]}'
where ID = 8035;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recursos comunitaris de Promoció i Prevenció de la Salut Mental i Benestar Emocional."}, {"la": "es", "text": ""}]}'
where ID = 8036;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Coordinadora d’Entitats per la Lleialtat Santsenca és una entitat sense ànim de lucre, formada pel veïnat, associacions i col·lectius vinculats a Sants, constituïda l’any 2016 per donar vida a l’?Espai de Participació? La Lleialtat Santsenca. La CELS promou la cultura, l’acció comunitària i la cooperació,el respecte i la llibertat; així com un desenvolupament públic i social més proper i participatiu, donant resposta als anhels de construir una societat més justa."}, {"la": "es", "text": ""}]}'
where ID = 8037;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8038;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ateneu Popular de la Coma Cros de Salt"}, {"la": "es", "text": ""}]}'
where ID = 8039;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació Espai comunitari i veïnal autogestionat de Can Batlló"}, {"la": "es", "text": ""}]}'
where ID = 8040;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8041;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8042;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8043;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8044;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Productora audiovisual crítica i independent, que neix amb la vocació de relatar la història dels dels moviments socials des de dins."}, {"la": "es", "text": ""}]}'
where ID = 8045;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8046;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Consell Nacional de la Joventut de Catalunya (CNJC) és la plataforma que aglutina les principals entitats juvenils de Catalunya. Un dels seus objectius principals és defensar els interessos de les persones joves a través de la incidència política, la sensibilització social i la formació."}, {"la": "es", "text": ""}]}'
where ID = 8047;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8048;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ecom és un moviment associatiu integrat per organitzacions de persones amb discapacitat física."}, {"la": "es", "text": ""}]}'
where ID = 8049;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8050;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "COOPERATIVA D''INSTAL.LACIONS D''ENERGIES RENOVABLES"}, {"la": "es", "text": ""}]}'
where ID = 8051;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "PRODUCCIO I COMERCIALITZACIO DE PLANTA ORNAMENTAL"}, {"la": "es", "text": ""}]}'
where ID = 8052;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat de prova"}, {"la": "es", "text": ""}]}'
where ID = 8053;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "recerca i altres històries"}, {"la": "es", "text": ""}]}'
where ID = 8054;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TUTELA DEL MALALT MENTAL"}, {"la": "es", "text": ""}]}'
where ID = 8055;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8056;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8057;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació privada sense afany de lucre de caràcter social, solidari i cultural amb projectes a Barcelona i el Senegal. "}, {"la": "es", "text": ""}]}'
where ID = 8058;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball d''iniciativa social sense ànim de lucre dedicada a vetllar pel dret al joc dels infants."}, {"la": "es", "text": ""}]}'
where ID = 8059;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8060;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai digital on es connecten persones compromeses amb organitzacions socials que necessiten suport. "}, {"la": "es", "text": ""}]}'
where ID = 8061;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8062;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8063;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dona Cançó és una associació sense ànim de lucre que porta més de deu anys treballant per les dones.  Impulsa a dones artistes amb l´objectiu d´incidir en la transformació social i en l´empoderament  de la dona, a través d´espais de creació, esdeveniments, actuacions i tallers, promovent  campanyes per a la igualtat i generant sinergies entre institucions. Crear sinergies entre artistes, programadors i institucions per aconseguir avançar en la paritat cultural. Construir projectes culturals amb perspectiva de gènere i bé social que siguin una resposta actual dels esdeveniments i necessitats que estem vivint."}, {"la": "es", "text": ""}]}'
where ID = 8064;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Incubadora de projectes de moda"}, {"la": "es", "text": ""}]}'
where ID = 8065;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa"}, {"la": "es", "text": ""}]}'
where ID = 8066;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Venda de llet i productes làctics"}, {"la": "es", "text": ""}]}'
where ID = 8067;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8068;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8069;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8070;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grupdem sccl és una cooperativa de treball associat, sense ànim de lucre, creada el 1971 i formada per professionals que uneixen els seus coneixements i esforços en el camp de la iniciativa social. La nostra missió és millorar la qualitat de vida de les persones amb discapacitat intel•lectual i altres col•lectius en risc d’exclusió social. Per tal d’assolir-la gestionem centres ocupacionals, un centre especial de treball i un servei de suport a la llar l’objectiu de les quals es afavorir la inclusió social i laboral de les persones que hi assisteixen. "}, {"la": "es", "text": ""}]}'
where ID = 8071;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "IBOD"}, {"la": "es", "text": ""}]}'
where ID = 8072;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Rw és una plataforma ciutadana que posa en contacte a persones refugiades que formen part del programa estatal de sol·licitud de protecció internacional, amb persones locals que vulguin llogar-los-hi una habitació, amb l''acompanyament d''una persona voluntària (víncle). D''aquesta manera promovem la cultura de benvinguda a les persones refugiades i a la seva inclusió social. "}, {"la": "es", "text": ""}]}'
where ID = 8073;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8074;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8075;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8076;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "asociacio cultural"}, {"la": "es", "text": ""}]}'
where ID = 8077;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "E-commerce de productos para una vida residuo cero"}, {"la": "es", "text": ""}]}'
where ID = 8078;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ONGD"}, {"la": "es", "text": ""}]}'
where ID = 8079;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grefart Associació Professional d’artterapeutes neix amb el principal objectiu d’estar al costat dels professionals, es va constituir l’any 2004 com a Grup de Recerca, Formació i Atenció en Artteràpia i com Associació Professional d’Artterapeutes. Com Associació Professional, vetlla pel desenvolupament professional dels artteapeutes, promovent la disciplina entre les institucions per a la creació de nous llocs de treball, així com la formació continuada als seus socis oferint cursos, monogràfics, supervisió i espais d’intercanvi professionals i de recerca. Els seus professionals treballen en projectes d’intervenció artterapèutica amb diferents col·lectius amb risc d’exclusió social com ara la salut mental, drogodependències, immigració, gent gran, infants i joves, dones i amb persones amb altres capacitats entre d’altres. És una de les cinc associacions professionals fundadores de la Federació Espanyola d’Associacions Professionals d’Artteràpia (FEAPA). Grefart Associació professional d’artterapeutes, està pensada tant per defensar i promoure l’actuació professional dels artterapeutes com per protegir els seus possibles usuaris, pacients i clients de les pràctiques inadequades de què poguessin ser objecte. Són objectius de l’Associació: Donar representació professional als artterapeutes. Defensar als possibles clients i pacients de l’exercici inadequat de la pràctica artterapèutica. Donar a conèixer l’Artteràpia a institucions, entitats i col·lectius professionals. Desenvolupar accions i serveis dirigits als professionals per la creació de llocs de treball així com la supervisió, la formació continuada i  la recerca."}, {"la": "es", "text": ""}]}'
where ID = 8080;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Provem d’avançar l’implementació dels sistemes d’energia renovable ??  ?Genera la teva energia ?Instal·lacions d’energia renovable ?Autoconsum"}, {"la": "es", "text": ""}]}'
where ID = 8081;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Editorial i Impremta Descontrol, enfocada a publicar autors i autores relegats de les grans empreses del món del llibre."}, {"la": "es", "text": ""}]}'
where ID = 8082;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8083;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de consum Ecològic format per 90 famílies, situat al centre de Sant Cugat. Amb dues persones assalariades que gestionen la feina del dia a dia."}, {"la": "es", "text": ""}]}'
where ID = 8084;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8085;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat de segon grau que agrupa mes de cent Associacions veinals de la ciutat de Barcelona"}, {"la": "es", "text": ""}]}'
where ID = 8086;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Obrador cooperatiu de productes artesanals amb ingredients ecològics i de proximitat"}, {"la": "es", "text": ""}]}'
where ID = 8087;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8088;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8089;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8090;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de desenvolupament infantil i Teràpia Ocupacional"}, {"la": "es", "text": ""}]}'
where ID = 8091;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8092;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Operadora de les dues xarxes del tramvia de Barcelona"}, {"la": "es", "text": ""}]}'
where ID = 8093;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "SEBA és una entitat dedicada al diseny, instal·lació, manteniment i gestió de sistemes fotovoltaics. També ralitzem projectes en promoció de l''Estalvi i Eficiència Energètica, així com en  Cooperació per al Desenvolupament Internacional"}, {"la": "es", "text": ""}]}'
where ID = 8094;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8095;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8096;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació Veïnal i gestors de Casal de Barri"}, {"la": "es", "text": ""}]}'
where ID = 8097;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació per l''Impuls dels Supermercats Cooperatius i el Mercat social (més conegut com a Food Coop BCN) neix a partir que un grup de persones, la majoria inspirades i motivades amb la projecció del documental \\"FoodCoop\\", dirigit per Thomas Boothe i Maellanne Bonnicel, decideixin unir-se i impulsar el projecte de supermercat cooperatiu a Barcelona, sota els criteris del consum responsabilitat, ètic i sostenible.  La gran majoria de les persones involucrades treballen de forma voluntària des del març del 2018. "}, {"la": "es", "text": ""}]}'
where ID = 8098;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8099;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8101;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8102;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa ciutadana de dades per a la recerca"}, {"la": "es", "text": ""}]}'
where ID = 8103;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Arquitectura Sostenible"}, {"la": "es", "text": ""}]}'
where ID = 8104;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8105;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Observatori del Tercer Sector i de la Societat Civil és un centre de recerca especialitzat, sense ànim de lucre i independent, que té per finalitat aprofundir i incrementar el coneixement sobre el Tercer Sector i treballar per a la millora en el funcionament de les organitzacions no lucratives."}, {"la": "es", "text": ""}]}'
where ID = 8106;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8107;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una fundació sense afany de lucre que atén a persones, especialment que es trobin en situació de vulnerabilitat social i laboral"}, {"la": "es", "text": ""}]}'
where ID = 8108;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Turó Acció Sòcio-Cultural (T.A.S.C.)   entitat de segon ordre,  formada per vuit entitats del barri: A.VV. Turó de la Peira. Ecologistes en Acció de Catalunya. L’associació cultural “El Casalet”. El Taller jove d’informació urbana. Club de futbol sala Montsant. Secció Esportiva Santa Eulàlia (SESE). Associació Lúdica Educativa “L’Espill”. Centro Cultural Deportivo Turó de la Peira. Va sorgir i s’ha desenvolupat arrel de l’acció ciutadana en clau associativa. Partint de la seva experiència individual i del coneixement del barri vol promoure accions socials i culturals amb la participació activa dels diferents col•lectius i persones.  L’entitat, representativa del teixit associatiu del territori, amb capacitat per a gestionar i amb un projecte al darrera, signa un conveni amb l’Administració per la gestió de l’ equipament Centre Cívic Can Basté , seguint les bases reguladores de la gestió cívica o ciutadana. Aquest acord és renovable cada 4 anys."}, {"la": "es", "text": ""}]}'
where ID = 8109;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació SaóPrat\xa0és una entitat socioeducativa, sense ànim de lucre i declarada d’utilitat pública, que neix el 2004 al Prat de Llobregat amb l’objectiu de donar resposta a les necessitats educatives i socials d’infants, adolescents i joves en risc o en exclusió social. Des de l’associació s’atenen, acompanyen i orienten perquè tinguin, retrobin i puguin aprofitar les oportunitats que els hi pertoquen per viure dignament, sent ells els protagonistes de la seva vida. Es fa a través de projectes educatius, formatius i d’integració social i en el món laboral."}, {"la": "es", "text": ""}]}'
where ID = 8110;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una entitat "}, {"la": "es", "text": ""}]}'
where ID = 8111;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de arquitectura"}, {"la": "es", "text": ""}]}'
where ID = 8112;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8113;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8114;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sóm una cooperativa dedicada a l''educació ambiental, fem tallers, extraescolars, sortides a la natura i remodelació de patis i espais escolars."}, {"la": "es", "text": ""}]}'
where ID = 8115;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8116;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8117;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat de 3r nivell que agrupa 34 federacions"}, {"la": "es", "text": ""}]}'
where ID = 8118;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "federació sense ànim de lucre d''entitats teatral i culturals de la ciutat de Figueres, que dóna estructura al projecte d''Aula de Teatre de Figueres i és un punt de trobada de les entitats i grups de teatre del municipi així com una plataforma de participació ciutadana en l''àmbit cultural local."}, {"la": "es", "text": ""}]}'
where ID = 8119;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som l''àrea de producció de la Fundació Pare Manel, treballem el tèxtil amb perspectiva d''ESS fent productes amb roba reutilitzada pensats per al consum responsable sense plàstic. Ens agradaria tenir a la nostra taula a les \\"Dones amb força\\" (faran inscripció sense taula), un grup comunitari de dones que, des de Perifèrica 9B hem acollit al nostre taller. Tenen un producte propi, amb consciència ecològica i de cures, treballen coixins terapèutics amb productes ecològics i roba reutilitzada. Ens agradaria que tinguin l''experiència de la FESC, i com estan en plena constitució, encara no tenen compte bancari, ens agradaria fer el pagament tot plegat (130+IVA). "}, {"la": "es", "text": ""}]}'
where ID = 8120;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Llibreria de proximitat en format cooperativa"}, {"la": "es", "text": ""}]}'
where ID = 8121;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8122;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8123;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Cooperativa implementa un Sistema d’Intercanvi Ciutadà REC (Recurs Econòmic Ciutadà) com a mitjà de pagament -complementari i paritari a l’euro- que permet realitzar transaccions entre les persones, les entitats i les empreses que l’accepten. Es presenta com una alternativa al sistema econòmic i monetari dominant globalitzat, tot incorporant mecanismes que actuïn de motor de canvi de la realitat econòmica i ajudin a resoldre les necessitats del teixit comercial, empresarial i social."}, {"la": "es", "text": ""}]}'
where ID = 8124;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8125;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8126;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "(a nom de Jordi Biarnés Montoliu) Companyia de Re*Animació i Dinamització Social"}, {"la": "es", "text": ""}]}'
where ID = 8127;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Atenció persones vulnerables i/o amb problemes d''adiccions de la ciutat de Lleida"}, {"la": "es", "text": ""}]}'
where ID = 8128;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''habitatge en cessió d''ús formada per una trentena de persones. Dones, homes, nens i nenes de procedència urbana principalment, preocupades per trobar una alternativa de vida diferent a l’actual model individualista i consumista, on predominin els valors cooperatius, solidaris i ètics. "}, {"la": "es", "text": ""}]}'
where ID = 8129;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8130;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de recerca. La línia de recerca principal té a veure amb el domini públic (commons) i més concretament amb el domini públic com a forma organitzativa de producció (producció entre iguals basada en el domini públic, CBPP). Altres termes que s’adopten algunes vegades per a referir-se a la CBPP són economia col·laborativa o innovació social."}, {"la": "es", "text": ""}]}'
where ID = 8131;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de recerca  biomèdica participat per la Universitat de Barcelona, Institut Català d''Oncologia, Hospital de Bellvitge, Ajuntament de L''Hospitalet i Generalitat de Catalunya."}, {"la": "es", "text": ""}]}'
where ID = 8132;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre de recerca biomèdica"}, {"la": "es", "text": ""}]}'
where ID = 8133;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8134;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8135;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8136;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Acompanyament a persones, col·lectius i equips en processos de creixement personal i desenvolupament de competències a través del coaching i formacions vivencials."}, {"la": "es", "text": ""}]}'
where ID = 8137;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ensenyament"}, {"la": "es", "text": ""}]}'
where ID = 8138;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8139;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat que vol transformar el camp de la discapacitat visual des d''una òptica educativa, social i científica"}, {"la": "es", "text": ""}]}'
where ID = 8140;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8141;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense ànim de lucre formada exclusivament per persones voluntàries que treballen per oferir a la gent gran un lloc on poder aprendre i compartir coneixements i experiències."}, {"la": "es", "text": ""}]}'
where ID = 8142;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8143;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8144;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació per el desenvolupament i formació d''horts i espais d''agricultura urbana"}, {"la": "es", "text": ""}]}'
where ID = 8145;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Arxiu Tobella és una entitat sense ànim de lucre, constituïda com a Fundació Privada l’any 1978. Entre els seus objectius destaquen la de constituir i mantenir un arxiu documental i gràfic sobre la història de la ciutat de Terrassa, fomentar-ne la investigació i tenir cura de la publicació dels treballs que patrocina, així com la promoció de la cultura en tots els seus àmbits."}, {"la": "es", "text": ""}]}'
where ID = 8146;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Formació, Sistemes, Streaming, Assessoria tecnològica free/open source"}, {"la": "es", "text": ""}]}'
where ID = 8147;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació que ocupa i atén persones amb diversitat funcional"}, {"la": "es", "text": ""}]}'
where ID = 8148;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promoció integral de les persones adultes amb discapacitat, principalment intel·lectual, paràlisi cerebral o malaltia mental, més enllà del fet purament assistencial. Les acompanyem de manera personalitzada en el seu recorregut vital i ens centrem a donar resposta a les seves necessitats i millor totes les dimensions de la seva qualitat de vida."}, {"la": "es", "text": ""}]}'
where ID = 8149;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Escola Tecnos va ser fundada com “Academia Tecnos” l’any 1962 per Magí Cadevall i Soler, Llorenç Puig i Mayolas, Jordi Gibert i Masvidal i Ramon Nogué i Serrat.  Posteriorment, l’any 1973, l’Escola Tecnos es fusionà amb l’Escola Social (encara que des de 1969 compartien el local del carrer Garcia Humet), fundada l’any 1923 pel Sr. Guillem Boronat i continuada pel Sr. Joan Artigues i la Sra. Pilar Alsius.  Des de l’any 1983, el nostre Centre funciona com a cooperativa de treball associat, amb el nom oficial d’Agrupació Pedagògica Tecnos Societat Cooperativa Catalana de Responsabilitat Limitada. A partir del curs 2013-2014, vam iniciar la constitució  cooperatives d’alumnes a l’etapa de la Secundària. La finalitat és potenciar l’emprenedoria dels nostres alumnes, els quals, simulant la creació d’empreses cooperatives, n’assumeixen la gestió social i econòmica, alhora que es responsabilitzen dels resultats de la seva activitat.  Els objectius de les cooperatives d''alumnes no han de ser  només lucratius sinó que han de tenir també una  finalitat social. És per això que fem prescriptiva la donació per part de cada cooperativa, del 20% dels beneficis obtinguts al llarg de cada exercici econòmic, a una entitat que treballi amb finalitats socials."}, {"la": "es", "text": ""}]}'
where ID = 8150;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa de consum productes "femenins""}, {"la": "es", "text": ""}]}'
where ID = 8151;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som Mobilitat, SCCL es constitueix com a una Societat Cooperativa de Consumidors i Usuaris, sense ànim de lucre. L’objecte de Som Mobilitat és impulsar totes aquelles accions o projectes que contribueixin al fet que tots els desplaçaments dels seus socis siguin més sostenibles i contaminin menys el medi ambient, reduint el nombre de vehicles acumulats a les nostres ciutats. Per això la seva activitat se centra en:     1. Dissenyar, produir i finançar nous serveis i productes tecnològics     2. Treballar en xarxa amb altres cooperatives     3. Contribuir i facilitar la creació de comunitats i noves cooperatives fora de Catalunya     4. Contribuir a la creació de grups locals i al finançament de projectes locals     5. Implicar els organismes públics  Sempre d''acord amb els sis principis fundacionals:     1. Mobilitat. Ampliem les alternatives en transport sostenible     2. Comunitat. Construïm junts altres opcions de mobilitat     3. Sostenibilitat. Apostem per una mobilitat menys contaminant     4. Ciutat. Compartim vehicles i reduïm el nombre de cotxes     5. Qualitat. Millorem la qualitat de l’aire de les ciutats     6. Estalvi. Reduïm despeses en la mobilitat individual"}, {"la": "es", "text": ""}]}'
where ID = 8152;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Intercooperació I Acompanyament a entitats i projectes d''Economia Social I Solidària"}, {"la": "es", "text": ""}]}'
where ID = 8153;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Synusia és un espai sense ànim de lucre dins de l''economia social i solidària, que explora noves formes de diàleg i intervenció en el teixit de la ciutat i a les xarxes socials. Proposa i genera continguts crítics, tant a través de la seva llibreria, com des de la programació i els cursos d''autoformació que realitza."}, {"la": "es", "text": ""}]}'
where ID = 8154;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hort urbà"}, {"la": "es", "text": ""}]}'
where ID = 8155;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una entitat sense ànim de lucre amb l''objectiu de Informar, sensibilitzar i conscienciar el conjunt de la ciutadania sobre el comerç just. Els objectius són: -\tSensibilització i educació per a la justícia global . -\tRecerca en comerç just tant al Nord com el Sud. -Millorar la qualitat de vida dels grups productors i les seves comunitats -\tIncidència política i social a nivell local i global."}, {"la": "es", "text": ""}]}'
where ID = 8156;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Artepaliativo es una entidad social que acompaña el bienestar emocional de personas que padecen una enfermedad avanzada o a final de vida a través del proceso creativo en sesiones individuales o talleres grupales, en el hospital o a domicilio."}, {"la": "es", "text": ""}]}'
where ID = 8157;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una associació de caràcter cultural i educatiu, sense ànim de lucre, per difondre la cultura tot despertant l’interès i satisfent al màxim possible les inquietuds intel·lectuals de les persones que vulguin ampliar els seus coneixements en diverses matèries (història, ciències, salut, art, música, economia, tecnologia, filosofia, ètica, psicologia…) d’una forma amena, dinàmica i participativa."}, {"la": "es", "text": ""}]}'
where ID = 8158;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L''Associació La Oka és una Associació d''ajuda a les persones amb discapacitat psíquica per acompanyar-los a desembolupar-se en l''àmbit laboral i social a través de la selecció, neteja, arranjaments i venda de roba usada proporcionada pels ciutadans. També facilita als nois poder realitzar treballs manuals i d''aquesta manera desenvolupar les seves capacitats i augmentar la seva autoestima."}, {"la": "es", "text": ""}]}'
where ID = 8159;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som el Grup d’Iniciativa Territorial de Fiare Banca Ètica de Catalunya Transversal. Les sòcies i socis actius de\xa0 Fiare Banca Etica. Junt a milers de persones i organitzacions, participem en el desenvolupament d’una banca transparent i cooperativa, al servei d’una economia real i sostenible amb el nostre compromís voluntari."}, {"la": "es", "text": ""}]}'
where ID = 8160;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Espai d’àpats saludables amb aliments de proximitat i ecològics amb tres espais  separats on es desenvolupant activitats com ioga, ioga per a embarassades, mindfulness, lindy hop, danses de la Polinèssia, claqué, cinefòrums, reunions d''empresa, esdeveniments; teràpies individuals dirigides per professionals de diversos camps del creixement personal, coaching de fertilitat, massatge i fisioteràpia o espai de consulta i Espai on hi realitzem txi kung i algunes classes de meditació. És un espai polivalent en el que també hi oferim càterings i xerrades."}, {"la": "es", "text": ""}]}'
where ID = 8161;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Federació d’Associacions Veïnals de Terrassa (FAVT) és una organització sense ànim de lucre que coordina 25 associacions veïnals de la ciutat i que a través dels seus projectes i campanyes, treballa per millorar la qualitat de vida dels veïns i les veïnes des dels barris de Terrassa."}, {"la": "es", "text": ""}]}'
where ID = 8162;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup Local de la Cooperativa Som Energia"}, {"la": "es", "text": ""}]}'
where ID = 8163;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció per la creació de llocs de treball per a persones vulnerables a través de la recollida selectiva de roba."}, {"la": "es", "text": ""}]}'
where ID = 8164;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Colla Castellera és formada majoritàriament per gent jove, un grup humà unit per l’obra col·lectiva que representen els castells, una tasca d’equip on tothom té el seu lloc, qualsevol, sense distinció d’edat, sexe, condició física o social, pot arribar a ser un bon casteller, tan sols és necessari el desig de ser-ho i un assaig constant."}, {"la": "es", "text": ""}]}'
where ID = 8165;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Tub d’Assaig 7,70 neix la tardor de 2007 a Terrassa de la inquietud de d’un grup d’artistes de circ de compartir un espai on crear i desenvolupar els seus projectes personals i col·lectius."}, {"la": "es", "text": ""}]}'
where ID = 8166;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Centre deEntitat no lucrativa amb la finalitat de ser un recurs d’inserció sòciolaboral per a persones amb especials dificultats, concretament persones amb discapacitat psíquica i/o trastorn mental.  Des de l’any 2000 al Centre hi treballa un equip multidisciplinar d’orientació psicosocial que s’ocupa de desenvolupar els processos d’inserció sòcio-laboral dels usuaris atesos.  Com a centre de jardineria, realitzem manteniment d''espais verds, disseny i creació d''espais enjardinats nous, gestió del medi ambient, treballs forestals i treballs d''arbrat.  En el camp de l''agricultura ecològica, estem treballant les terres de Can Salas per adaptar-les al cultiu ecològic seguint el mètode de les Parades en Crestall per crear els horts educatius. "}, {"la": "es", "text": ""}]}'
where ID = 8167;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8168;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ullastrell Solidari és una associació benèfica sense ànim de lucre. En l’actualitat la formem unes vint persones, totes elles voluntàries, que compartim una il•lusió comuna, la de l´AJUDA HUMANITÀRIA, no tan sols a Ullastrell si no a qualsevol indret del món, que el grup consideri necessari, i estigui dins el nostre abast.\xa0 "}, {"la": "es", "text": ""}]}'
where ID = 8169;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat formada per uns 40 components, dedicada al cant coral. Realització de concerts, per Catalunya i Participació en festivals internacionals. "}, {"la": "es", "text": ""}]}'
where ID = 8170;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una entitat que neix amb el propòsit de resoldre les demandes d’activitats que fins ara s’havien de dur a terme a altres municipis. L’entitat ofereix serveis de qualitat i un espai d’educació i oci per a totes les edats. El projecte està consolidat per professionals que vetllen pel benestar i el millor servei pel poble d’Ullastrell."}, {"la": "es", "text": ""}]}'
where ID = 8171;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Local d’Entitats per la Inclusió és una associació formada per una xarxa de 27 entitats de la ciutat amb diverses línies d’actuació, però totes treballant per la inclusió social i laboral de les persones en risc d’exclusió."}, {"la": "es", "text": ""}]}'
where ID = 8172;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de Teràpies Holístiques"}, {"la": "es", "text": ""}]}'
where ID = 8173;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat declarada d’interès cultural, amb seu social a Terrassa, dedica tots els seus esforços a activitats culturals i de lleure sobretot adreçades a nois i noies. Algunes de les activitats que agrupa van iniciar-se ja fa més de vint-i-cinc anys. L’àmbit de la seva actuació és principalment als països de parla catalana, però amb una projecció que apunta vers l’Europa comunitària."}, {"la": "es", "text": ""}]}'
where ID = 8174;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Viver de planta aquàtica i projectes."}, {"la": "es", "text": ""}]}'
where ID = 8175;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Suport Castellar és una organització sense ànim de lucre que treballa activament per la salut mental de les persones."}, {"la": "es", "text": ""}]}'
where ID = 8176;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis psico-pedagògics de tot tipus i puguin arribar a totes les famílies de tots els nivells econòmics."}, {"la": "es", "text": ""}]}'
where ID = 8177;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’AMPA és una associació sense afany de lucre formada per mares i pares d’alumnes i situada en el mateix centre educatiu amb un objectiu: contribuir a la millora de la qualitat del sistema educatiu públic i de les condicions d’escolarització dels i les alumnes del centre."}, {"la": "es", "text": ""}]}'
where ID = 8178;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Casal Independentista i Popular de Sabadell"}, {"la": "es", "text": ""}]}'
where ID = 8179;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Platafrma d''afectats per la hipoteca i la crisi"}, {"la": "es", "text": ""}]}'
where ID = 8180;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una entitat sense afany de lucre, declarada d’utilitat pública, que té com a objectiu defensar i estudiar la natura."}, {"la": "es", "text": ""}]}'
where ID = 8181;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8182;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8183;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promover la respuesta de los investigadores al desarrollo de la cura de la enfermedad déficit múltiple MSD-Sulfactasa."}, {"la": "es", "text": ""}]}'
where ID = 8184;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La nostra missió és desenvolupar i promoure iniciatives orientades a l''aplicació, divulgació, formació i perfeccionament de l''Economia del Bé Comú a organitzacions, municipis i persones, adaptant els objectius a les singularitats del teixit social, econòmic i cultural de Terrassa."}, {"la": "es", "text": ""}]}'
where ID = 8185;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "GEOINFORMACIO I TERRITORI"}, {"la": "es", "text": ""}]}'
where ID = 8186;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació persones sordes Sabadell"}, {"la": "es", "text": ""}]}'
where ID = 8187;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Colla Castellera de Montcada i Reixac"}, {"la": "es", "text": ""}]}'
where ID = 8188;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Recerca Art i Cultura"}, {"la": "es", "text": ""}]}'
where ID = 8189;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ASSOCIACIÓ DE MARES I PARES DE L´ESCOLA PUBLICA CATALUNYA de SABADELL"}, {"la": "es", "text": ""}]}'
where ID = 8190;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Restaurant - Espai de cuina i cultura"}, {"la": "es", "text": ""}]}'
where ID = 8191;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8192;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Escola de circ"}, {"la": "es", "text": ""}]}'
where ID = 8193;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Pansalet som una associació gastronòmica i cultural fundada el 13 de març del 2014. Amb el seu naixement, construïm un espai per facilitar l''oci i la relació entre uns amics que coincidim en la nostra afició comuna per la gastronomia i la cultura del nostre país."}, {"la": "es", "text": ""}]}'
where ID = 8194;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assemblea Territorial de Sant Cugat de l''Assemblea Nacional Catalana. "}, {"la": "es", "text": ""}]}'
where ID = 8195;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Un espai de subversió i canvi social al voltant de les arts i les seves derives."}, {"la": "es", "text": ""}]}'
where ID = 8196;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Sota un format d’associació Cultural Sense lucre l’equip de SabaCirc crea, gestiona, coordina, organitza, produeix, i assesora sobre accions de circ i circ social a Catalunya."}, {"la": "es", "text": ""}]}'
where ID = 8197;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El nostre principal objectiu és el promoure l''acció comunitària i la solidaritat a favor de les persones en situació o risc d''exclusió social. "}, {"la": "es", "text": ""}]}'
where ID = 8198;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Agro-ecologia"}, {"la": "es", "text": ""}]}'
where ID = 8199;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "NUESTRO PRINCIPAL OBJETIVO ES UN TRABAJO DIGNO PARA TODOS.  ASESORANDO Y AYUDANDO A PERSONAS A SALIR DE ESTA SITUACIÒN."}, {"la": "es", "text": ""}]}'
where ID = 8200;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Comunitat de persones que ens agrada la tecnologia i un dels nostres principals objectius es potenciar el desenvolupament de les diferents intel·ligències dels nens a través de la tecnologia."}, {"la": "es", "text": ""}]}'
where ID = 8201;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8202;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte de recerca i acció per a la regeneració urbana, social i energètica a escala de barri des de l’acció col·lectiva i l’empoderament ciutadà.   Proposa models alternatius d’habitabilitat, convivència i governança, per la millora del metabolisme urbà del barri i la conscienciació envers l’ús eficient de recursos, a través de la plataforma RESSÒ Sant Muç, com a espai col·lectiu catalitzador d’aquest procés autogestionat per a la regeneració a escala de barri."}, {"la": "es", "text": ""}]}'
where ID = 8203;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de mares i pares de l''Institut IES de Sabadell"}, {"la": "es", "text": ""}]}'
where ID = 8204;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "entitat sense ànim de lucre dedicada a l''acció social amb infants i joves "}, {"la": "es", "text": ""}]}'
where ID = 8205;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocionar activitats extraescolars de qualitat ja siguin esportives, artístiques, intel·lectuals o lúdiques en edat escolar"}, {"la": "es", "text": ""}]}'
where ID = 8206;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació sense ànim de lucre, formada per un col·lectiu divers de persones que comparteixen un interès comú: l''agricultura ecològica"}, {"la": "es", "text": ""}]}'
where ID = 8207;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "PiC Vallès és un projecte de trobada i acció entre productores/rs i consumidors/es del Vallès Occidental."}, {"la": "es", "text": ""}]}'
where ID = 8208;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Construïm consum cooperatiu agroecològic"}, {"la": "es", "text": ""}]}'
where ID = 8209;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació de mares i pares de l''escola bressol Joaquim Blume de Sabadell"}, {"la": "es", "text": ""}]}'
where ID = 8210;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "TUS, S. Coop. C. Ltda. (Transports Urbans de Sabadell) fou constituïda l''any 1982 per un grup de treballadors agrupats en una Cooperativa de treball associat, amb la finalitat de realitzar el transport urbà de la ciutat de Sabadell amb l''objectiu d''atendre les necessitats de mobilitat dels ciutadans. Transports Urbans de Sabadell va ampliar el seu servei en número de línies i autobusos, millorant així la seva qualitat."}, {"la": "es", "text": ""}]}'
where ID = 8211;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8212;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8213;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Gestoria"}, {"la": "es", "text": ""}]}'
where ID = 8214;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Habitatge i gestió de l''entorn"}, {"la": "es", "text": ""}]}'
where ID = 8215;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup local de la cooperativa de consumidors Som Energia. Associació que promou un canvi de model energètic i en conseqüència amb la principal activitat, entre d''altres, del creixement de sòcies i usuàries de Som Energia."}, {"la": "es", "text": ""}]}'
where ID = 8216;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Transportes de Sabadell SCCL, tiene como objetivo principal dar un servicio de transporte calidad, generar un clima de confianza y colaboración para resolver sus necesidades de transporte y logística integral."}, {"la": "es", "text": ""}]}'
where ID = 8217;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "A TOTMUSICA Sabadell ens agrada ensenyar la música de manera didàctica i divertida en un ambient agradable i amb personal qualificat."}, {"la": "es", "text": ""}]}'
where ID = 8218;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Neix com un sindicat agrícola de la mà de Mossen Camil Rossell. Des d''aquest moment, es va anar diversificant el negoci al voltant dels productes del camp. El 2001 es van inaugurar les noves instal•lacions, com resposta a aquesta diversificació i a un canvi d''orientació que no oblida el camperol però que incorpora nous mercats. Com a cooperativa, la Perpetuenca gestiona els segurs agrícoles, la DUN, subministra productes als camperols i els assessora tècnicament en el que faci falta. Alhora que pren força l''AGROTENDA."}, {"la": "es", "text": ""}]}'
where ID = 8219;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Guardería, enseñanza educación infantil exclusivamente"}, {"la": "es", "text": ""}]}'
where ID = 8220;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Servei Municipal d''Ocupació de l''Ajuntament de Barberà del Vallès. Treballem pel desenvolupament socioeconòmic, creació i qualitat d’ocupació, desenvolupament personal i riquesa de la ciutadania: empreses, emprenedores, treballadores i persones en cerca de feina."}, {"la": "es", "text": ""}]}'
where ID = 8221;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una associació d’unes 20 famílies que no vol pas crèixer en número sinó en força i cohesió per abarcar temes que ens facin millorar, ser millors coneixedors dels productes que consumim, ser més conscients dels efectes del petit consum en el nostre entorn i ser més capaços d’intervenir per canviar una mica les coses des de la nostra modesta posició."}, {"la": "es", "text": ""}]}'
where ID = 8222;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8223;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat de psicologia social. Projectes d''acció social, empoderament i promoció de la salut mental"}, {"la": "es", "text": ""}]}'
where ID = 8224;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cardedeu"}, {"la": "es", "text": ""}]}'
where ID = 8225;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Versembrant és una escola popular itinerant; un projecte que pretén fomentar la consciència crítica del jovent, mitjançant l’art urbà i el hip hop. Els nostres tallers de rap, de producció i d’arts plàstiques (sobre racisme, xenofòbia, sexisme…) aspiren a posar l’estètica al servei de la crítica, en un procés creatiu del qual els protagonistes no són els talleristes sinó aquells que creant i repensant el seu entorn s’autotransformen en el procés. Versembrant és Filosofia de la praxi feta rap.  Versembrant és una proposta educativa que té com a destinataris tant l’alumnat que cursa l’educació secundària (ESO, Batxillerat i FP), així com joves de tot tipus de centres educatius, centres residencials, d’eduació en el lleure… Versembrant també ofereix formació i eines per als professionals i persones que conformen l’entorn educatiu (mares, pares, tutors legals, professorat, entitats…)."}, {"la": "es", "text": ""}]}'
where ID = 8226;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Aula de So som la teva escola de música de Sant Cugat amb gran diversitat de programes i un equip professional i motivat que t''acompanyarà"}, {"la": "es", "text": ""}]}'
where ID = 8227;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "FUNDACIO ESCOLTA JOSEP CAROL"}, {"la": "es", "text": ""}]}'
where ID = 8228;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8229;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8230;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8231;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Xarxa d''iniciatives de l''ESS d''Osona"}, {"la": "es", "text": ""}]}'
where ID = 8232;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Despatx de Co-Advocats"}, {"la": "es", "text": ""}]}'
where ID = 8233;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Empresa d''inserció de persones en situació de vulnerabilitat social."}, {"la": "es", "text": ""}]}'
where ID = 8234;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8235;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ACTIVITATS DE FOMENT DE LA RESPONSABILITAT SOCIAL EN LES EMPRESES, RECERCA, PUBLICACIONS "}, {"la": "es", "text": ""}]}'
where ID = 8236;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8237;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8238;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8239;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Productes didàctics, jocs educatius i serveis pedagògics"}, {"la": "es", "text": ""}]}'
where ID = 8240;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de repartiment "}, {"la": "es", "text": ""}]}'
where ID = 8241;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup de Consum Sostenible"}, {"la": "es", "text": ""}]}'
where ID = 8242;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8243;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8244;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8245;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8246;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Microfinances Socials"}, {"la": "es", "text": ""}]}'
where ID = 8247;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8248;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8249;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8250;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de trabajo sin ánimo de lucro centrada en la creación, comunicación y diseño de iniciativas por el cambio social y proyectos dentro de la economía solidaria."}, {"la": "es", "text": ""}]}'
where ID = 8251;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som l''Associació laica de l''escoltisme català"}, {"la": "es", "text": ""}]}'
where ID = 8252;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8253;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8254;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8255;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Logística, operacions, anàlisi de processos, màrqueting estratègic i implementació de programes de nivell expert."}, {"la": "es", "text": ""}]}'
where ID = 8256;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Agencia de Marketing Digital y Marketing ético"}, {"la": "es", "text": ""}]}'
where ID = 8257;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Producción biochar /Formación y asesoramiento / Proyectos medioambientales"}, {"la": "es", "text": ""}]}'
where ID = 8258;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8259;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Begudes saludables i sostenibles"}, {"la": "es", "text": ""}]}'
where ID = 8260;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8261;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Platoniq Lab facilita procesos agiles de cooperación e innovación social por medio de dinámicas, metodologías, herramientas digitales y workshops para introducir cambios culturales en organizaciones como ONGs, instituciones, cooperativas o empresas sociales."}, {"la": "es", "text": ""}]}'
where ID = 8262;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8263;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8264;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ecodiseny i produccio de complements de moda sostenibles"}, {"la": "es", "text": ""}]}'
where ID = 8265;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Asociació que gestiona el Centro Cultural L''Occulta, al barri del Raval."}, {"la": "es", "text": ""}]}'
where ID = 8266;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació per la promoció de la bicicleta com a opció de mobilitat"}, {"la": "es", "text": ""}]}'
where ID = 8267;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació dedicada a la promoció, divulgació i formació en dades obertes."}, {"la": "es", "text": ""}]}'
where ID = 8268;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8269;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8270;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Contribuir , des del seu compromís ètic, mitjançant suport i oportunitats, a que cada persona amb paràlisi cerebral o altres discapacitats similars i la seva família pugui desenvolupar el seu projecte de qualitat de vida, així com promoure la seva inclusió com a ciutadana de ple dret en una societat justa i solidàriaLa Residència i Centre de Dia Esclat Marina ofereix un espai de vida flexible, substitutori de la llar, que dóna resposta a les diferents necessitats de las persones amb pluridiscapacitat que requereixen suport extens i generalitzat.."}, {"la": "es", "text": ""}]}'
where ID = 8271;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Deixa''t cuinar. Servei a domicili d''àpats per a gent gran i/o persones depenents."}, {"la": "es", "text": ""}]}'
where ID = 8272;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Servicios Integrales para tu proyecto Herramientas de gestión, consolidación y mejora para la Economía Social y la PIME"}, {"la": "es", "text": ""}]}'
where ID = 8273;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "associació declarada d''utilitat pública que lluita per millorar la qualitat de vida del jovent tutelat i extutelat en risc d''exclusió social, garantint la igualtat d''oportunitats en la seva incorporació a la vida adulta"}, {"la": "es", "text": ""}]}'
where ID = 8274;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Somos una organización dedicada a la consultoría organizacional, la facilitación de grupos y equipos y la capacitación en habilidades de elderazgo, desde el prisma de las organizaciones como sistemas vivos. Acompañamos a personas y organizaciones en sus procesos de cambio hacia una mayor conciencia, adaptabilidad y bienestar.  Aportamos herramientas, técnicas y habilidades para un liderazgo consciente que apoya el trabajo en equipo, la gestión efectiva de los procesos grupales y la resolución colaborativa de posibles conflictos, acompañando a las personas en el proceso de descubrimiento de su máximo potencial y en el uso cuidadoso de su poder como agentes de cambio."}, {"la": "es", "text": ""}]}'
where ID = 8275;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És una associació de consumidors i productors de productes ecològics, persones interessades en la millora de la salut i de l''entorn, disposades a fer un canvi en els hàbits a l''hora d''omplir el cistell..."}, {"la": "es", "text": ""}]}'
where ID = 8276;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8277;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8278;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "EMPRESA INSTAL.LADORA D''ENERGIA SOLAR"}, {"la": "es", "text": ""}]}'
where ID = 8279;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8280;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8281;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Consultoria especialitzada en dret ambiental i sostenibiltat"}, {"la": "es", "text": ""}]}'
where ID = 8282;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Teatre de titularitat públic ai gestió comunitaria, coordinat per una Oficina Tècnica (UTE Teatre Arnau) "}, {"la": "es", "text": ""}]}'
where ID = 8283;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8284;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La misión de Open Cultural Center (OCC) es promover la educación, cultura e integración de las personas refugiadas a través de la creación de espacios, provisión de material y coordinación en los que se favorezca el intercambio cultural."}, {"la": "es", "text": ""}]}'
where ID = 8285;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa d''habitatge en cessió d''ús"}, {"la": "es", "text": ""}]}'
where ID = 8286;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessorament en materia fiscal, laboral i tributaria"}, {"la": "es", "text": ""}]}'
where ID = 8287;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8288;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ACTIVITATS COMUNITÀRIES DE MEMÒRIA I D''ANTROPOLOGIA DE L''ALIMENTACIÓ"}, {"la": "es", "text": ""}]}'
where ID = 8289;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8290;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Bar-cafeteria de l''Ateneu L''Harmonia"}, {"la": "es", "text": ""}]}'
where ID = 8291;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associació per agrupar i promocionar l''economia social i solidària"}, {"la": "es", "text": ""}]}'
where ID = 8292;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Allotjament turistic, espai per esdeveniment, gestió espai agroecològic i venda productes gastronòmics. "}, {"la": "es", "text": ""}]}'
where ID = 8293;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Estudi de gravació dins l''economia social i solidària."}, {"la": "es", "text": ""}]}'
where ID = 8294;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8295;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Ingeniería especializada en energías renovables, eficiencia energética y monitorización energética"}, {"la": "es", "text": ""}]}'
where ID = 8296;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis immobiliaris per a cooperatives "}, {"la": "es", "text": ""}]}'
where ID = 8297;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "CORREDORÍA D''ASSEGURANCES ÈTIQUES I SOLIDÀRIES"}, {"la": "es", "text": ""}]}'
where ID = 8298;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "pt2"}, {"la": "es", "text": ""}]}'
where ID = 8299;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte d''horta agroecologica de temporada i proximitat. Repartim cistelles a domicili i participem als Mercats locals més proxims i amb risc de desaparèixer."}, {"la": "es", "text": ""}]}'
where ID = 8300;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "COOP"}, {"la": "es", "text": ""}]}'
where ID = 8301;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8302;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promocionar els productes elaborats per persones amb capacitats diferents, malalties mentals, amb risc d''exclusió social. Volem crear una CET."}, {"la": "es", "text": ""}]}'
where ID = 8303;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8304;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8305;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Communia és una assessoria tecnològica que ofereix serveis de desenvolupament web, streaming multimèdia, formacions STEAM i tècniques en tecnologies de codi obert, assessorament en estratègies P2P, així com desplegament de software i sistemes informàtics. A Communia només implementem solucions amb programari lliure, creiem que el coneixement ha de ser lliure, obert, horitzontal i accessible a totes les persones. Per això promovem solucions a mida en sistemes de gestió i publicació de continguts digitals, dotació d''infraestructures informàtiques, desenvolupament i/o implementació de programari per a necessitats concretes... Solucions que facilitin les activitats en xarxa de les diferents comunitats d''investigació en les pràctiques P2P, amb tecnologies lliures que vinculen les eines amb la construcció de processos de cooperació social."}, {"la": "es", "text": ""}]}'
where ID = 8306;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Calaixera som el centre especial de treball de l’Associació Arep per la Salut Mental. Vam néixer el 1999 per generar oportunitats per a persones que pateixen l’exclusió per causa de la seva discapacitat per problemes de salut mental, amb la mirada sempre posada a acompanyar-les a assolir una vida plena i autònoma.  Tenim diferents línies d’activitat que han anat canviant els últims anys gràcies al pla estratègic dissenyat el 2009. Recollint els canvis en el mercat laboral, a La Calaixera hem anat apostant per la creació de noves línies de negoci centrades en els serveis."}, {"la": "es", "text": ""}]}'
where ID = 8307;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "DEPATX ADVOCATS"}, {"la": "es", "text": ""}]}'
where ID = 8308;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8309;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8310;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8311;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8312;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Des de la Fundació treballem per aturar l’impacte de la malaltia en la vida de les persones amb esclerosi múltiple i el seu entorn. "}, {"la": "es", "text": ""}]}'
where ID = 8313;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8314;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8315;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8316;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "..."}, {"la": "es", "text": ""}]}'
where ID = 8317;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8318;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de consum"}, {"la": "es", "text": ""}]}'
where ID = 8319;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8320;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8321;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8322;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8323;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8324;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8325;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8326;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "--"}, {"la": "es", "text": ""}]}'
where ID = 8327;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8328;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8329;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8330;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Construcció de cobertes i murs verds"}, {"la": "es", "text": ""}]}'
where ID = 8331;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8332;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Iniciativa que permet posar en contacte productors locals amb el consumidor final, mitjançant la creació de comunitats de consum, anomenades Ruscs."}, {"la": "es", "text": ""}]}'
where ID = 8333;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis de disseny gràfic i comunicació visual. Aposto pel treball amb les mans, l''experimentació i la col·laboració interdisciplinària. Crec en la feina en xarxa, investigar i descobrir nous llenguatges que es puguin aplicar a les necessitats de cada projecte, des de l''inici fins a la producció. Dissenyo amb un compromís amb el medi ambient, amb la voluntat d''aplicar el sentit comú a cada projecte i treballant d''una forma més sostenible i ètica."}, {"la": "es", "text": ""}]}'
where ID = 8334;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Prestació dels serveis públics "}, {"la": "es", "text": ""}]}'
where ID = 8335;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8336;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Cooperativa Gregal es troba al barri del Besòs i el Maresme de la ciutat de Barcelona i actualment es dedica a l''alimentació, a través d''un menjador solidari que treballa en el territori. A més, també treballa en xarxa amb la comunitat i pretén ampliar els àmbits d''actuació."}, {"la": "es", "text": ""}]}'
where ID = 8337;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8338;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8339;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8340;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8341;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8342;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Organització sense ànim de lucre que organitza activitats per alumnes i pares d''alumnes de les Escoles Fàsia. Projecte de construcció d''habitatges tutelats per discapacitats"}, {"la": "es", "text": ""}]}'
where ID = 8343;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8344;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Creador audiovisual freelance"}, {"la": "es", "text": ""}]}'
where ID = 8345;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8346;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte cooperatiu sense ànim de lucre, multi seccional, democràtic, igualitari i solidari.  La nostra missió social, es repartir la riquesa generada per la activitat econòmica entre el màxim nombre de persones possible. Entenem que la divisió del poder econòmic de la societat, es un avenç en el camí d’aconseguir una societat més igualitària que no atorgui més poder a aquelles persones que posseeixen més recursos"}, {"la": "es", "text": ""}]}'
where ID = 8347;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Formació àmbit educatiu a docents, famílies, lleure i administracions."}, {"la": "es", "text": ""}]}'
where ID = 8348;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8349;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat esportiva local sense ànim de lucre que té com a finalitat la promoció de l''atletisme i els hàbits saludables."}, {"la": "es", "text": ""}]}'
where ID = 8350;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8351;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "A la Fundació Canpedró tenim cura de la persona i vetllem per la seva dignitat i autoestima. Oferim un servei individualitzat i global a la persona i a la família, que inclou l’alimentació de qualitat i equilibrada, el seguiment de la salut física i emocional, la cura de la imatge personal, l’acompanyament i orientació laboral i la dinamització social i cultural. A Canpedró el més important no és el plat que donem sinó el suport i l’acollida a les persones desfavorides. Canpedró és un espai humà que dóna a aquell que no té el que altres poden donar."}, {"la": "es", "text": ""}]}'
where ID = 8352;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Projecte cultural, social, inclusiu i comunitari que utilitza la batucada com a eina de transformació social"}, {"la": "es", "text": ""}]}'
where ID = 8353;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8354;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8355;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de segon grau sense ànim de lucre amb la finalitat de crear oportunitats laborals per a col.lectius vulnerables"}, {"la": "es", "text": ""}]}'
where ID = 8356;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cartaes és una empresa d’inserció, amb número de registre 57 del Departament d’Empresa i Ocupació de la Generalitat de Catalunya, i amb forma jurídica SLU, promoguda per Càritas Parroquial de Tàrrega, única sòcia, que neix amb l’objectiu de generar ocupació per aquelles persones amb especials dificultats per treballar i trobar feina."}, {"la": "es", "text": ""}]}'
where ID = 8357;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8358;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "AmbTu realitza serveis de consultoria i de formació destinats als agents de l’ESS, amb la finalitat que incrementin i millorin la gestió i el control dels recursos financers destinats als seus projectes d’elevat impacte social, ambiental i econòmic, tot tenint cura de l’estat emocional de les persones que hi treballen."}, {"la": "es", "text": ""}]}'
where ID = 8359;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Associacio cultural esportiva"}, {"la": "es", "text": ""}]}'
where ID = 8360;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Difusió local de l''ESS i generació d''iniciatives comunitàries."}, {"la": "es", "text": ""}]}'
where ID = 8361;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Nadir és un projecte educatiu format per quatre cineastes que treballa l’educació audiovisual amb infants i joves. D''altra banda també és un projecte que s''especialitza en la documentació de processos artístics i la realització de vídeos didàctics i divulgatius."}, {"la": "es", "text": ""}]}'
where ID = 8362;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Foto - Vídeo i streaming"}, {"la": "es", "text": ""}]}'
where ID = 8363;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Bar cafeteria de l''Ateneu L''Harmonia"}, {"la": "es", "text": ""}]}'
where ID = 8364;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa integral (d''habitatge en cessió d''ús i de persones consumidores i usuàries) a Sant Antoni de Vilamajor, Baix Montseny. Som una masia amb cinc unitats de convivència amb cinc hectàrees de terreny i amb un projecte en construcció que també inclou propostes agroecològiques, culturals i artístiques, socials i d''enxarxament amb el territori."}, {"la": "es", "text": ""}]}'
where ID = 8365;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "-"}, {"la": "es", "text": ""}]}'
where ID = 8366;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8367;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa sense ànim de lucre, basada en els principis de l''ESS (Economia Social i Solidària) i el cooperativisme."}, {"la": "es", "text": ""}]}'
where ID = 8368;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8369;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma de fact-checking"}, {"la": "es", "text": ""}]}'
where ID = 8370;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8371;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8372;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entidad sin animo de lucro comprometida en ayudar, a través del deporte, a personas que han tenido problemas de adicciones."}, {"la": "es", "text": ""}]}'
where ID = 8373;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "En el Sindicato Popular de Vendedores Ambulantes somos trabajadores y luchadores que nos hemos organizado para defender nuestros derechos."}, {"la": "es", "text": ""}]}'
where ID = 8374;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8375;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Organització no governamental, laica, independent i sense ànim de lucre que treballa per millorar la qualitat de l’educació dels infants, i les seves condicions de vida, utilitzant l’art com a eina de transformació social."}, {"la": "es", "text": ""}]}'
where ID = 8376;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Experimentem i promovem models de producció, consum i organització sostenibles"}, {"la": "es", "text": ""}]}'
where ID = 8377;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa dedicada a la creació, desenvolupament, producció, acompanyament, comunicació i agitació de projectes culturals"}, {"la": "es", "text": ""}]}'
where ID = 8378;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una entitat  sense ànim de lucre, declarada d''utilitat pública, no governamental i no confessional, que col·laborem amb les administracions i institucions públiques i privades per dur a terme la nostra missió:     Millorar la qualitat de vida de les persones amb discapacitat intel·lectual o amb dificultats en el desenvolupament i les seves famílies a la comarca d''Osona."}, {"la": "es", "text": ""}]}'
where ID = 8379;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8380;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "a"}, {"la": "es", "text": ""}]}'
where ID = 8381;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa audiovisual"}, {"la": "es", "text": ""}]}'
where ID = 8382;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8383;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8384;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació dedicada a l''àmbit de la Salut Mental, integrant l''assistència, la docència i la recerca, amb una mirada psicològica, social, biològica i espiritual."}, {"la": "es", "text": ""}]}'
where ID = 8385;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "L’Associació Salut Mental Catalunya – Terres de Lleida. Coordinadora d’entitats de salut mental i addiccions, és la unió de diferents associacions de famílies i de persones afectades per problemàtica de salut mental, per tal d’unificar esforços i treballar problemàtiques comuns i específiques del territori lleidatà."}, {"la": "es", "text": ""}]}'
where ID = 8386;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Germanetes Jardins d''Emma és una entitat sense ànim de lucre que té com a primer objectiu treballar per a la millora de la qualitat de vida del veïnat del barri de l''esquerra de l''eixample tant a escala de salut com en l''àmbit de relacions socials. El segon objectiu és fer servir la sostenibilitat, l''educació ambiental i l''horticultura com a eina per cohesionar i conscienciar la comunitat veïnal."}, {"la": "es", "text": ""}]}'
where ID = 8387;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "empresa dedicada al disseny computacional amb l''objectiu d''ajudar als sectors del disseny i l''arquitectura a crear solucions més optimes i compromeses amb el medi ambient i amb les persones."}, {"la": "es", "text": ""}]}'
where ID = 8388;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "cooperativa d''habitatge"}, {"la": "es", "text": ""}]}'
where ID = 8389;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Entitat de suport a persones amb problemes de salut mental. "}, {"la": "es", "text": ""}]}'
where ID = 8390;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Serveis jurídics "}, {"la": "es", "text": ""}]}'
where ID = 8391;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Desde Antígona diseñamos y acompañamos procesos participativos apoyádonos en las metodologías participativas como herramientas fundamentales en los procesos de transformación social."}, {"la": "es", "text": ""}]}'
where ID = 8392;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8393;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8394;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8395;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Assessoria institucional, jurídica i comunicativa."}, {"la": "es", "text": ""}]}'
where ID = 8396;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8397;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8398;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8399;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8400;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Fundació Francesc Ferrer i Guàrdia és una entitat sense afany de lucre que des de 1987 treballa en els àmbits de laïcitat, joventut, educació, participació democràtica, ciutadania europea i polítiques públiques, generant coneixement, aportant propostes estratègiques i assessoraments a les administracions públiques i donant suport al moviment associatiu, per tal de promoure l’emancipació ciutadana, d’acord amb els ideals ferrerians de lliurepensament, autonomia, igualtat i cohesió social."}, {"la": "es", "text": ""}]}'
where ID = 8401;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "És un intercanvi gratuït de coneixements i habilitats entre persones que viuen a Manlleu. Ofereixes un servei i en reps un altre a canvi. Es basa en el principi d''ajuda mútua entre la ciutadania."}, {"la": "es", "text": ""}]}'
where ID = 8402;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Cooperativa de treball associat sense afany de lucre. Desenvolupem projectes culturals transformadors sobre creativitat, paraula i memòria. Treballem de manera transversal des dels principis cooperatius, la perspectiva de gènere, l''atenció a la diversitat i la sostenibilitat."}, {"la": "es", "text": ""}]}'
where ID = 8403;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8404;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Queviures ecològics de proximitat."}, {"la": "es", "text": ""}]}'
where ID = 8405;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Som una cooperativa sense ànim de lucre, on treballem l’educació en valors i emocional mitjançant activitats d’escalada, rutes guiades de coneixença de la biodiversitat i xerrades de conscienciació sobre l’impacte de les activitats de muntanya en el medi.\nL’educació en primer lloc perquè considerem que és una eina de canvi, de transformació social, de transmissió de coneixements i valors. L’escalada com a metodologia, com a recurs pedagògic, pels beneficis que ens suposa la seva pràctica tant en aspectes físics com emocionals."}, {"la": "es", "text": ""}]}'
where ID = 8406;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Certificat en sostenibilitat per organitzacions i cursos amb titulació universitària"}, {"la": "es", "text": ""}]}'
where ID = 8407;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Botiga online per comprar titelles i ninots personalitzats d''artesania en paper maixé per regal original. ."}, {"la": "es", "text": ""}]}'
where ID = 8408;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Casal Popular Boira Baixa de Manlleu. Punt de trobada per l''alliberament nacional, social i de gènere. \nUsuari creat des del backoffice. No es veu el Balanç Comunitari. Hem obert el tiquet #12213 però l''hem deixat en standy."}, {"la": "es", "text": ""}]}'
where ID = 8409;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8410;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "El Grup de Defensa del Ter (GDT) som una associació ecologista formada per ciutadans compromesos que estimem el Ter, la vida i el medi natural."}, {"la": "es", "text": ""}]}'
where ID = 8411;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA NOSTRA MISSIÓ PRINCIPAL ÉS MILLORAR LA VIDA DE PERSONES AMB DISCAPACITAT FÍSICA, ORGÀNICA I/O SENSORIAL"}, {"la": "es", "text": ""}]}'
where ID = 8412;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "LA NOSTRA MISSIÓ ÉS MILLORAR LA VIDA DE PERSONES AMB DISCAPACITAT FÍSICA,ORGÀNICA I/O SENSORIAL"}, {"la": "es", "text": ""}]}'
where ID = 8413;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8414;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Centre Especial de Treball"}, {"la": "es", "text": ""}]}'
where ID = 8415;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "ACRA, l’Associació Catalana de Recursos Assistencials, és una organització empresarial sense ànim de lucre, fundada el 1989, que agrupa la majoria d’empreses i entitats del sector de l’assistència a la gent gran a Catalunya."}, {"la": "es", "text": ""}]}'
where ID = 8416;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": " Som una associació formada per tots els familiars i tutors de les persones amb discapacitat intel·lectual ateses en qualsevol servei de l’entitat."}, {"la": "es", "text": ""}]}'
where ID = 8417;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Fundació humanitària de caire social"}, {"la": "es", "text": ""}]}'
where ID = 8418;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8419;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8420;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Casal que persegueix la bona convivència comunitària i realitza gestió comunitària."}, {"la": "es", "text": ""}]}'
where ID = 8421;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Hola"}, {"la": "es", "text": ""}]}'
where ID = 8422;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La gestió de la construcció és un element clau en el procés de promoció d’habitatge i és un servei que, fins ara, no ofereixen les entitats de l’economia social que operen en aquest sector. Sostre Cívic es va plantejar donar resposta a aquesta necessitat des de l’economia social i per això va impulsar la creació d’una nova constructora cooperativa."}, {"la": "es", "text": ""}]}'
where ID = 8423;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Nau Bostik pretén ser un referent en l’activitat cultural que es duu a terme al barri de La Sagrera. Amb la intenció de donar cobertura a la necessitat d’espais per a la dinamització social i cultural del barri, es vol oferir un gran lloc de trobada per a entitats i veïns, i molt especialment per a totes aquelles persones que tinguin interès en les manifestacions culturals de tota mena i ganes de compartir-les i exterioritzar-les."}, {"la": "es", "text": ""}]}'
where ID = 8424;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Despatx d''arquitectura i espai públic"}, {"la": "es", "text": ""}]}'
where ID = 8425;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Grup cooperatiu d''inserció laboral. Desenvolupem activitats econòmiques i projectes sociolaborals que permeten millorar la qualitat de vida de les persones en situació o risc d’exclusió social, en el marc d’un procés d’acompanyament professional i en xarxa amb altres agents socials i econòmics del territori."}, {"la": "es", "text": ""}]}'
where ID = 8426;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Prova Test"}, {"la": "es", "text": ""}]}'
where ID = 8427;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8428;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "s"}, {"la": "es", "text": ""}]}'
where ID = 8429;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8430;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8431;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Dinamitzem, cohesionem, creem comunitat i removem consciències"}, {"la": "es", "text": ""}]}'
where ID = 8432;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "La Federació d’Entitats Amigues de Can Carol i Consolat (FEACCC) està formada per aquelles entitats amb representació jurídica que han jugat un paper clau en el desenvolupament sociocultural del nostre barri i que alhora han mostrat interès per participar en la futura gestió dels equipaments. "}, {"la": "es", "text": ""}]}'
where ID = 8433;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8434;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Promovem l´atutoocupacio per l´exercici de les vocacions i el desenvolupament professional"}, {"la": "es", "text": ""}]}'
where ID = 8435;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": "Plataforma web col·laborativa d''inclusió social en l''habitatge"}, {"la": "es", "text": ""}]}'
where ID = 8436;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8437;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8438;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8439;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8440;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": None}, {"la": "es", "text": ""}]}'
where ID = 8441;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 8442;
update social_economy_entity
set DESCRIPTION = '{"texts": [{"la": "ca", "text": ""}, {"la": "es", "text": ""}]}'
where ID = 900957;
