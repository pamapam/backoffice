-- # Àmbit FESC 2022 i Estats FESC
-- Crear l'àmbit FESC 2022
INSERT INTO entity_scope(id, name, version) VALUES (5, 'FESC 2022', 0);
COMMIT;

-- Setear el valor del status del año anterior al histórico y dejar el de este año vacío
update social_economy_entity_fesc set fesc_previous_entity_status_id = fesc_entity_status_id;
update social_economy_entity_fesc set fesc_entity_status_id = null;

-- Setear el precio del año anterior al histórico y dejar el de este año vacío
update social_economy_entity_fesc set fesc_previous_fee = fesc_fee;
update social_economy_entity_fesc set fesc_fee = null;

COMMIT;
-- Añadir sector principal para la FESC

ALTER TABLE social_economy_entity_fesc ADD COLUMN main_sector_id bigint(20) DEFAULT NULL;
ALTER TABLE social_economy_entity_fesc ADD CONSTRAINT FK_fesc_main_sector FOREIGN KEY (main_sector_id) REFERENCES sector (ID);

-- Añadir checks para políticas de privacidad

ALTER TABLE social_economy_entity_fesc ADD COLUMN accepted_registration_privacy_policy BOOLEAN DEFAULT FALSE;
ALTER TABLE social_economy_entity_fesc ADD COLUMN accepted_participation_privacy_policy BOOLEAN DEFAULT FALSE;
ALTER TABLE social_economy_entity_fesc ADD COLUMN accepted_registration_privacy_policy_date TIMESTAMP;
ALTER TABLE social_economy_entity_fesc ADD COLUMN accepted_participation_privacy_policy_date TIMESTAMP;
