-- Add filteg tags.
CREATE TABLE entity_filter_tag (entity_id BIGINT NOT NULL, filter_tag_id BIGINT NOT NULL, PRIMARY KEY (entity_id, filter_tag_id));
ALTER TABLE entity_filter_tag ADD CONSTRAINT FK_entity_filter_tag_filter_tag_id FOREIGN KEY (filter_tag_id) REFERENCES text_tag (ID);
ALTER TABLE entity_filter_tag ADD CONSTRAINT FK_entity_filter_tag_entity_id FOREIGN KEY (entity_id) REFERENCES social_economy_entity (ID);