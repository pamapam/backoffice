-- Add binary resources separated table
CREATE TABLE `binary_resource` (
  `id` bigint(20) NOT NULL,
  `contents` longblob,
  `filelength` int(11) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `mimetype` varchar(255) DEFAULT NULL,
  `timestamp` date DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE pamapam_backoffice.social_economy_entity ADD picture_id BIGINT(20) NULL ;
ALTER TABLE pamapam_backoffice.social_economy_entity ADD CONSTRAINT social_economy_entity_binary_resource_FK FOREIGN KEY (picture_id) REFERENCES pamapam_backoffice.binary_resource(id) ON DELETE RESTRICT ON UPDATE RESTRICT ;
CREATE INDEX FK_social_economy_entity_picture_id USING BTREE ON pamapam_backoffice.social_economy_entity (picture_id) ;
INSERT INTO pamapam_backoffice.sequence_table (seq_name, seq_count) VALUES('binary_resource_seq', 999);
ALTER TABLE pamapam_backoffice.social_economy_entity DROP COLUMN PICTURE ;

-- Add registry user
ALTER TABLE pamapam_backoffice.social_economy_entity ADD registry_user_id BIGINT(20) NULL ;
ALTER TABLE pamapam_backoffice.social_economy_entity ADD CONSTRAINT social_economy_entity_user_FK FOREIGN KEY (registry_user_id) REFERENCES pamapam_backoffice.user(id) ON DELETE RESTRICT ON UPDATE RESTRICT ;
CREATE INDEX FK_social_economy_entity_registry_user_id USING BTREE ON pamapam_backoffice.social_economy_entity (registry_user_id) ;
update social_economy_entity
	set registry_user_id = 1002;
ALTER TABLE pamapam_backoffice.`user` DROP COLUMN DTYPE ;

-- Add latitude & longitude
ALTER TABLE pamapam_backoffice.social_economy_entity ADD latitude DOUBLE NULL ;
ALTER TABLE pamapam_backoffice.social_economy_entity ADD longitude DOUBLE NULL ;

-- Increment TAGS length
ALTER TABLE pamapam_backoffice.social_economy_entity MODIFY COLUMN TAGS varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ;

-- Fix column typo
ALTER TABLE pamapam_backoffice.social_economy_entity CHANGE neighbornood_id neighborhood_id bigint(20) NULL ;

-- Increment opening hours length
ALTER TABLE pamapam_backoffice.social_economy_entity MODIFY COLUMN opening_hours varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ;

-- Delete market function column
ALTER TABLE pamapam_backoffice.social_economy_entity DROP FOREIGN KEY FK_social_economy_entity_social_market_function_id ;
ALTER TABLE pamapam_backoffice.social_economy_entity DROP INDEX FK_social_economy_entity_social_market_function_id ;
ALTER TABLE pamapam_backoffice.social_economy_entity DROP COLUMN social_market_function_id ;

-- Delete table market function
DROP TABLE pamapam_backoffice.social_market_function ;

-- Add sectors ccae table
CREATE TABLE `sector_ccae` (
  `ID` bigint(20) NOT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE pamapam_backoffice.social_economy_entity ADD sector_ccae_id BIGINT(20) NULL ;
ALTER TABLE pamapam_backoffice.social_economy_entity ADD CONSTRAINT social_economy_entity_sector_ccae_FK FOREIGN KEY (sector_ccae_id) REFERENCES pamapam_backoffice.sector_ccae(id) ON DELETE RESTRICT ON UPDATE RESTRICT ;
CREATE INDEX FK_social_economy_sector_ccae_id USING BTREE ON pamapam_backoffice.social_economy_entity (sector_ccae_id) ;

-- Add other social economy networks column
ALTER TABLE pamapam_backoffice.social_economy_entity ADD other_social_economy_networks varchar(1024) NULL ;

-- Add quitter column
ALTER TABLE pamapam_backoffice.social_economy_entity ADD QUITTER varchar(255) NULL ;

-- Add product tags table
CREATE TABLE `text_tag` (
  `ID` bigint(20) NOT NULL,
  `tag_type` varchar(31) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Delete old tags column
ALTER TABLE pamapam_backoffice.social_economy_entity DROP COLUMN TAGS ;

-- Add entity product tags table
CREATE TABLE `entity_product_tag` (
  `entity_id` bigint(20) NOT NULL,
  `product_tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`entity_id`,`product_tag_id`),
  KEY `entityproducttagproduct_tag_id` (`product_tag_id`),
  CONSTRAINT `FK_entity_product_tag_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`),
  CONSTRAINT `entityproducttagproduct_tag_id` FOREIGN KEY (`product_tag_id`) REFERENCES `text_tag` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Delete old other social economy networks column
ALTER TABLE pamapam_backoffice.social_economy_entity DROP COLUMN other_social_economy_networks ;

-- Add entity socialeconomy network tags table
CREATE TABLE `entity_social_economy_network_tag` (
  `entity_id` bigint(20) NOT NULL,
  `social_economy_network_tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`entity_id`,`social_economy_network_tag_id`),
  KEY `entitysocialeconomynetwork_tag_id` (`social_economy_network_tag_id`),
  CONSTRAINT `FK_entity_social_economy_network_tag_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`),
  CONSTRAINT `entitysocialeconomynetworktagsocialeconomynetwork_id` FOREIGN KEY (`social_economy_network_tag_id`) REFERENCES `text_tag` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Add entity status table
CREATE TABLE `entity_status` (
  `ID` bigint(20) NOT NULL,
  `status_type` int(2) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE pamapam_backoffice.social_economy_entity ADD entity_status_id BIGINT(20) NULL ;
ALTER TABLE pamapam_backoffice.social_economy_entity ADD CONSTRAINT social_economy_entity_entity_status_FK FOREIGN KEY (entity_status_id) REFERENCES pamapam_backoffice.entity_status(id) ON DELETE RESTRICT ON UPDATE RESTRICT ;
CREATE INDEX FK_social_economy_entity_status_id USING BTREE ON pamapam_backoffice.social_economy_entity (entity_status_id) ;

-- Add initial entity status
INSERT INTO entity_status (ID,STATUS_TYPE,NAME,VERSION) VALUES (1000,0,'{"texts":[{"la":"es"},{"la":"ca","text":"Esborrany"}]}',1);
INSERT INTO entity_status (ID,STATUS_TYPE,NAME,VERSION) VALUES (1001,1,'{"texts":[{"la":"es"},{"la":"ca","text":"Pendent de Revisar"}]}',1);
INSERT INTO entity_status (ID,STATUS_TYPE,NAME,VERSION) VALUES (1002,2,'{"texts":[{"la":"es"},{"la":"ca","text":"Incomplet"}]}',1);
INSERT INTO entity_status (ID,STATUS_TYPE,NAME,VERSION) VALUES (1003,3,'{"texts":[{"la":"es"},{"la":"ca","text":"Publicat"}]}',1);
INSERT INTO entity_status (ID,STATUS_TYPE,NAME,VERSION) VALUES (1004,4,'{"texts":[{"la":"es"},{"la":"ca","text":"Punts Informes"}]}',1);
INSERT INTO entity_status (ID,STATUS_TYPE,NAME,VERSION) VALUES (1005,5,'{"texts":[{"la":"es"},{"la":"ca","text":"Pendent d''actualitzar"}]}',1);

update social_economy_entity
	set entity_status_id = 1000
	where REVIEWED = 0;
	
update social_economy_entity
	set entity_status_id = 1003
	where REVIEWED = 1;

-- Delete reviewed column
ALTER TABLE pamapam_backoffice.social_economy_entity DROP COLUMN reviewed ;

-- Add next status table
CREATE TABLE `entity_status_next_status` (
  `entity_status_id` bigint(20) NOT NULL,
  `next_status_id` bigint(20) NOT NULL,
  PRIMARY KEY (`entity_status_id`,`next_status_id`),
  KEY `entitystatusnext_status_id` (`next_status_id`),
  CONSTRAINT `FK_entity_status_next_status_entity_status_id` FOREIGN KEY (`entity_status_id`) REFERENCES `entity_status` (`ID`),
  CONSTRAINT `FK_entity_status_next_status_next_status_id` FOREIGN KEY (`next_status_id`) REFERENCES `entity_status` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Add Entity status change table
CREATE TABLE `entity_status_change` (
  `id` bigint(20) NOT NULL,
  `change_date` date DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `entity_id` bigint(20) DEFAULT NULL,
  `entity_status_from_id` bigint(20) DEFAULT NULL,
  `entity_status_to_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT fk_entity_status_change_entity_id FOREIGN KEY (entity_id) REFERENCES social_economy_entity(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fk_entity_status_change_entity_status_from_id FOREIGN KEY (entity_status_from_id) REFERENCES entity_status(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fk_entity_status_change_entity_status_to_id FOREIGN KEY (entity_status_to_id) REFERENCES entity_status(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fk_entity_status_change_user_id FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2017-08-31
-- Add Global ID to manage difirint copies of same entity.
ALTER TABLE pamapam_backoffice.social_economy_entity ADD global_id bigint(20) NULL;

update social_economy_entity
	set global_id = RAND() * 1000000;
	
ALTER TABLE pamapam_backoffice.social_economy_entity MODIFY COLUMN global_id bigint(20) NOT NULL;

-- Change evaluations relationship to OneToOne
ALTER TABLE pamapam_backoffice.social_economy_entity ADD entity_evaluation_id bigint(20) NULL ;
ALTER TABLE pamapam_backoffice.social_economy_entity ADD CONSTRAINT social_economy_entity_entity_evaluation_FK FOREIGN KEY (ID) REFERENCES pamapam_backoffice.entity_evaluation(ID) ON DELETE RESTRICT ON UPDATE RESTRICT ;
CREATE INDEX FK_social_economy_entity_entity_evaluation_id USING BTREE ON pamapam_backoffice.social_economy_entity (entity_evaluation_id) ;

update social_economy_entity see
	set entity_evaluation_id = (select id from entity_evaluation ee
		where ee.entity_id = see.id);
		
-- 2017-09-02
-- Add missing sequencesINSERT INTO pamapam_backoffice.sequence_table
INSERT INTO pamapam_backoffice.sequence_table (seq_name, seq_count) VALUES('entity_status_seq', 5000);
INSERT INTO pamapam_backoffice.sequence_table (seq_name, seq_count) VALUES('entity_status_change_seq', 5000);

-- Rename and change type of timestamp column.
ALTER TABLE pamapam_backoffice.entity_status_change CHANGE change_date change_timestamp DATETIME NULL ;
ALTER TABLE pamapam_backoffice.entity_status_change MODIFY COLUMN change_timestamp DATETIME NULL ;

-- 2017-09-06
-- Add notes table.
CREATE TABLE entity_note (ID BIGINT NOT NULL, TEXT VARCHAR(16384), note_timestamp DATETIME, version BIGINT, entity_id BIGINT, user_id BIGINT, PRIMARY KEY (ID));
ALTER TABLE entity_note ADD CONSTRAINT FK_entity_note_entity_id FOREIGN KEY (entity_id) REFERENCES social_economy_entity (ID);
ALTER TABLE entity_note ADD CONSTRAINT FK_entity_note_user_id FOREIGN KEY (user_id) REFERENCES user (ID);
INSERT INTO sequence_table(seq_name, seq_count) values ('entity_note_seq', 5000);

-- 2017-09-11
-- Delete deprecated.
DROP TABLE pamapam_backoffice.social_economy_area;
DROP TABLE pamapam_backoffice.entity_social_economy_area;
DROP TABLE pamapam_backoffice.entity_status_notification_role;
ALTER TABLE pamapam_backoffice.social_economy_entity DROP COLUMN other_social_economy_networks;
ALTER TABLE pamapam_backoffice.`user` DROP COLUMN DTYPE;

-- 2017-09-12
-- Delete back reference
ALTER TABLE pamapam_backoffice.entity_evaluation DROP FOREIGN KEY FK_entity_evaluation_entity_id ;
ALTER TABLE pamapam_backoffice.entity_evaluation DROP COLUMN entity_id ;

-- 2017-09-19
-- Add user description and picture
ALTER TABLE pamapam_backoffice.`user` ADD description varchar(2048) NULL ;
ALTER TABLE pamapam_backoffice.`user` ADD picture_id bigint(20) NULL ;

-- 2017-09-30
-- Add criterion icon.
ALTER TABLE pamapam_backoffice.criterion ADD icon_id bigint(20) NULL ;

-- 2017-10-06
-- Add old version flag.
ALTER TABLE pamapam_backoffice.social_economy_entity ADD old_version tinyint(1) NULL ;

-- Add Sector icon.
ALTER TABLE pamapam_backoffice.sector ADD icon_id bigint(20) NULL ;

-- 2017-10-14
-- Add userid to evaluations
ALTER TABLE pamapam_backoffice.entity_evaluation DROP COLUMN EVALUATOR ;
ALTER TABLE pamapam_backoffice.entity_evaluation ADD user_id bigint(20) DEFAULT NULL NULL ;

-- After import of old evaluations.
update social_economy_entity
	set entity_evaluation_id = id
	where id >= 900000;
	
-- Support old criterions
ALTER TABLE pamapam_backoffice.criterion ADD old_version TINYINT NULL ;
update criterion
	set old_version = 0
	where id < 900000;
	
-- 2017-10-16
-- Add criterion view order
ALTER TABLE pamapam_backoffice.criterion ADD view_order TINYINT UNSIGNED NULL ;

-- 2017-10-18
-- Add extra icon on sectors.
ALTER TABLE pamapam_backoffice.sector CHANGE icon_id map_icon_id bigint(20) DEFAULT NULL NULL ;
ALTER TABLE pamapam_backoffice.sector ADD icon_id bigint(20) NULL ;

-- 2017-10-31
-- Make username unique.
alter table user add constraint unique (username);

select * from user
	group by username
	having count(id) > 1;

-- 2017-11-05
-- Change entity description length
ALTER TABLE pamapam_backoffice.social_economy_entity MODIFY COLUMN DESCRIPTION varchar(16384) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ;

-- 2017-11-06
-- Fix null versions.
update social_economy_entity
	set version = 1
	where version is null;
	
-- 2017-11-19
-- Add main sector column
ALTER TABLE pamapam_backoffice.social_economy_entity ADD main_sector_id bigint(20) NULL ;
-- ejecutar db-migration/main_sector.sql

-- Add normalized name
ALTER TABLE pamapam_backoffice.social_economy_entity MODIFY COLUMN DESCRIPTION varchar(8192) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ;
ALTER TABLE pamapam_backoffice.social_economy_entity ADD normalized_name varchar(255) DEFAULT NULL ;

-- 2017-12-05
-- Create newsletter registration table
CREATE TABLE newsletter_registration (ID BIGINT NOT NULL, EMAIL VARCHAR(255) NOT NULL, NAME VARCHAR(255) NOT NULL, version BIGINT, PRIMARY KEY (ID));
INSERT INTO sequence_table(seq_name, seq_count) values ('newsletter_registration_seq', 999);

-- 2017-12-10
-- fix missing registry_date entries
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-05-09'
WHERE ID=1003 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-05-17'
WHERE ID=1009 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-05-17'
WHERE ID=1010 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-14'
WHERE ID=1018 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-26'
WHERE ID=1020 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-26'
WHERE ID=1021 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-26'
WHERE ID=1022 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-26'
WHERE ID=1023 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-25'
WHERE ID=1026 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1031 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1032 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1033 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1034 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1035 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1036 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1037 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-06-28'
WHERE ID=1038 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-07-04'
WHERE ID=1040 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-07-04'
WHERE ID=1041 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-07-04'
WHERE ID=1042 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-07-04'
WHERE ID=1043 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2017-07-04'
WHERE ID=1044 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2014-10-22'
WHERE ID=900121 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2015-02-28'
WHERE ID=900138 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2013-11-28'
WHERE ID=900152 and registry_date is null;
UPDATE pamapam_backoffice.social_economy_entity
SET registry_date='2015-11-24'
WHERE ID=901001 and registry_date is null;

-- Create missing entity_status_change
select @statusChangeId := max(id) from entity_status_change;
insert into entity_status_change (id, change_timestamp, version, entity_id, entity_status_from_id, entity_status_to_id, user_id)
select (@statusChangeId := @statusChangeId + 1), timestamp(t1.registry_date), 1, t1.id, null, t1.entity_status_id, t1.registry_user_id from social_economy_entity t1
	left outer join entity_status_change t2 on t2.entity_id = t1.ID
	where t2.id is null;
select @statusChangeId := max(id) from entity_status_change;
update sequence_table
	set seq_count = @statusChangeId + 1
	where seq_name = 'entity_status_change_seq';

-- 2017-12-13
ejecutar db-migration/text_tag.sql
ejecutar db-migration/entity_social_economy_network_tag.sql
ejecutar db-migration/entity_social_economy_network.sql

-- 2017-12-14
ejecutar db-migration/main_sector.sql
ejecutar db-migration/social_economy_entity_description.sql

-- 2017-12-20
-- Add territory to user.
ALTER TABLE pamapam_backoffice.`user` ADD province_id bigint(20) NULL ;
ALTER TABLE pamapam_backoffice.`user` ADD region_id bigint(20) NULL ;
ALTER TABLE pamapam_backoffice.`user` ADD town_id bigint(20) NULL ;
ALTER TABLE pamapam_backoffice.`user` ADD district_id bigint(20) NULL ;
ALTER TABLE pamapam_backoffice.`user` ADD neighborhood_id bigint(20) NULL ;

ALTER TABLE user ADD CONSTRAINT FK_user_district_id FOREIGN KEY (district_id) REFERENCES district (ID);
ALTER TABLE user ADD CONSTRAINT FK_user_neighborhood_id FOREIGN KEY (neighborhood_id) REFERENCES neighborhood (ID);
ALTER TABLE user ADD CONSTRAINT FK_user_town_id FOREIGN KEY (town_id) REFERENCES town (ID);
ALTER TABLE user ADD CONSTRAINT FK_user_province_id FOREIGN KEY (province_id) REFERENCES province (ID);
ALTER TABLE user ADD CONSTRAINT FK_user_region_id FOREIGN KEY (region_id) REFERENCES region (ID);

-- Add community to user.
CREATE TABLE community (ID BIGINT NOT NULL, NAME VARCHAR(255), version BIGINT, PRIMARY KEY (ID));
INSERT INTO sequence_table(seq_name, seq_count) values ('community_seq', 999);
ALTER TABLE pamapam_backoffice.`user` ADD community_id bigint(20) NULL ;
ALTER TABLE user ADD CONSTRAINT FK_user_community_id FOREIGN KEY (community_id) REFERENCES community (ID);

-- Replace single community by multiple communities.
ALTER TABLE pamapam_backoffice.`user` DROP FOREIGN KEY FK_user_community_id ;
ALTER TABLE pamapam_backoffice.`user` DROP INDEX FK_user_community_id ;
ALTER TABLE pamapam_backoffice.`user` DROP COLUMN community_id ;
CREATE TABLE user_community (user_id BIGINT NOT NULL, community_id BIGINT NOT NULL, PRIMARY KEY (user_id, community_id));
ALTER TABLE user_community ADD CONSTRAINT FK_user_community_user_id FOREIGN KEY (user_id) REFERENCES user (ID);
ALTER TABLE user_community ADD CONSTRAINT FK_user_community_community_id FOREIGN KEY (community_id) REFERENCES community (ID);
