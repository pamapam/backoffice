drop table entity_filter_tag;
delete from text_tag where tag_type = 'FILTER';
CREATE TABLE external_filter_tag (ID BIGINT NOT NULL, TAGTEXT VARCHAR(255), version BIGINT, PRIMARY KEY (ID));
CREATE TABLE entity_external_filter_tag (entity_id BIGINT NOT NULL, external_filter_tag_id BIGINT NOT NULL, PRIMARY KEY (entity_id, external_filter_tag_id));
ALTER TABLE entity_external_filter_tag ADD CONSTRAINT entity_external_filter_tag_external_filter_tag_id FOREIGN KEY (external_filter_tag_id) REFERENCES external_filter_tag (ID);
ALTER TABLE entity_external_filter_tag ADD CONSTRAINT FK_entity_external_filter_tag_entity_id FOREIGN KEY (entity_id) REFERENCES social_economy_entity (ID);
INSERT INTO sequence_table(seq_name, seq_count) values ('external_filter_tag_seq', 999);