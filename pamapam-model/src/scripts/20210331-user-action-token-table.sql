-- pamapam_backoffice.user_action_token definition

CREATE TABLE `user_action_token` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `valid_to` datetime NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sequence_table (seq_name, seq_count) VALUES('user_action_token_seq', 1);
