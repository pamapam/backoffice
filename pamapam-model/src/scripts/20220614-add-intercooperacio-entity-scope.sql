-- Añadir desde la interfaz del backoffice un nuevo role ROLE_pol_cooperatiu
-- Mirar el id para role_id SELECT * FROM role;
-- Mirar el siguiente id para pamapam_role SELECT * FROM pamapam_role;

-- Insert en pamapam_role para el role nuevo
INSERT INTO pamapam_role (id, version, role_id) values(5004, 0, 5004);

-- Insertar el entity_scope
INSERT INTO entity_scope(id, NAME, version) values(6, 'Intercooperació', 0);

-- Relacionar rol pol cooperatiu con el ámbito Pol Cooperatiu
INSERT INTO pamapam_role_entity_scope(pamapam_role_id, entity_scope_id) VALUES(5004, 6);

-- Añadir tabla text_url

CREATE TABLE text_url (
                          id bigint(20) NOT NULL,
                          name TEXT,
                          url TEXT
);

ALTER TABLE text_url ADD PRIMARY KEY (`id`);
ALTER TABLE text_url ADD COLUMN version BIGINT;

-- Refactor entity_scope_type
ALTER TABLE entity_scope ADD entity_scope_type varchar(255) NULL;

UPDATE entity_scope
SET entity_scope_type = 'DEFAULT'
WHERE NAME = 'Pam a Pam';

UPDATE entity_scope
SET entity_scope_type = 'EINATECA'
WHERE NAME = "Einateca";

UPDATE entity_scope
SET entity_scope_type = 'FESC_2021_ENTITY_SCOPE'
WHERE NAME = "FESC 2021";

UPDATE entity_scope
SET entity_scope_type = 'FESC_2022_ENTITY_SCOPE'
WHERE NAME = "FESC 2022";

UPDATE entity_scope
SET entity_scope_type = 'COMMUNITY_ECONOMY_ENTITY_SCOPE'
WHERE NAME = "Economies Comunitàries";

UPDATE entity_scope
SET entity_scope_type = 'INTERCOOPERACIO_ENTITY_SCOPE'
WHERE NAME = "Intercooperació";



