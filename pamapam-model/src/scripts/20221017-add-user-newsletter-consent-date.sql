ALTER TABLE pamapam_backoffice.`user` ADD newsletter_consent_date date NULL;

-- Update all users who appear as registered for the newsletter with the date they accepted it
UPDATE pamapam_backoffice.`user` u
    JOIN newsletter_registration nr ON u.username = nr.NAME AND u.EMAIL = nr.EMAIL
    set u.newsletter_consent_date = nr.registration_date;