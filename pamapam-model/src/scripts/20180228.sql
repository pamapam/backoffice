ALTER TABLE pamapam_backoffice.`user` ADD TWITTER varchar(255) DEFAULT NULL NULL ;
ALTER TABLE pamapam_backoffice.`user` ADD FACEBOOK varchar(255) DEFAULT NULL NULL ;

alter table entity_collaboration_entity drop foreign key `FK_entity_collaboration_entity_entity_id`;
alter table entity_external_filter_tag drop foreign key `FK_entity_external_filter_tag_entity_id`;
alter table entity_note drop foreign key `FK_entity_note_entity_id`;
alter table entity_person_role drop foreign key `FK_entity_person_role_entity_id`;
alter table entity_product_tag drop foreign key `FK_entity_product_tag_entity_id`;
alter table entity_sector drop foreign key `FK_entity_sector_entity_id`;
alter table entity_social_economy_area drop foreign key `FK_entity_social_economy_area_entity_id`;
alter table entity_social_economy_network drop foreign key `FK_entity_social_economy_network_entity_id`;
alter table entity_social_economy_network_tag drop foreign key `FK_entity_social_economy_network_tag_entity_id`;
alter table entity_status_change drop foreign key `fk_entity_status_change_entity_id`;

alter table entity_collaboration_entity add CONSTRAINT `FK_entity_collaboration_entity_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_external_filter_tag add CONSTRAINT `FK_entity_external_filter_tag_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_note add CONSTRAINT `FK_entity_note_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_person_role add CONSTRAINT `FK_entity_person_role_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_product_tag add CONSTRAINT `FK_entity_product_tag_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_sector add CONSTRAINT `FK_entity_sector_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_social_economy_area add CONSTRAINT `FK_entity_social_economy_area_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_social_economy_network add CONSTRAINT `FK_entity_social_economy_network_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_social_economy_network_tag add CONSTRAINT `FK_entity_social_economy_network_tag_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
alter table entity_status_change add CONSTRAINT `fk_entity_status_change_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`) on delete cascade;
