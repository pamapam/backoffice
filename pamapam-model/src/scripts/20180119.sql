-- Add localized messages table.
CREATE TABLE localized_message (ID BIGINT NOT NULL, COUNTRY VARCHAR(255), DESCRIPTION VARCHAR(2048), message_key VARCHAR(255), LANGUAGE VARCHAR(255), message_value VARCHAR(255), version BIGINT, PRIMARY KEY (ID))
INSERT INTO sequence_table(seq_name, seq_count) values ('localized_message_seq', 999);
