CREATE TABLE entity_evaluation_answer (ID BIGINT NOT NULL, VALUE INTEGER, criterion_answer_id BIGINT, entity_evaluation_criterion_id BIGINT, PRIMARY KEY (ID));

ALTER TABLE entity_evaluation_answer ADD CONSTRAINT FK_entity_evaluation_answer_criterion_answer_id FOREIGN KEY (criterion_answer_id) REFERENCES criterion_answer (ID);
ALTER TABLE entity_evaluation_answer ADD CONSTRAINT entityevaluationanswerentityevaluationcriterion_id FOREIGN KEY (entity_evaluation_criterion_id) REFERENCES entity_evaluation_criterion (ID);

INSERT INTO sequence_table(seq_name, seq_count) values ('entity_evaluation_answer_seq', 999);

update entity_evaluation_criterion
	set level = level * 100
	where level is not null and level >= 0;

INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(76, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 1);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(77, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 2);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(78, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 3);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(79, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 4);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(80, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 5);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(81, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 6);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(82, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 7);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(83, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 8);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(84, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 9);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(85, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 10);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(86, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 11);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(87, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 12);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(88, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 13);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(89, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 14);
INSERT INTO pamapam_backoffice.criterion_answer (ID, ANSWER, answer_order, version, criterion_id) VALUES(90, '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Bones pràctiques"}]}', 6, 0, 15);
