-- #10490 Àmbit FESC 2021 i Estats FESC
-- - Crear l'àmbit FESC 2021
INSERT INTO entity_scope(id, name, version) VALUES (3, 'FESC 2021', 0);
-- - Modificar els estats FESC per convertir-los en estats genèrics i no FESC 2021
UPDATE entity_status es 
SET es.name = REPLACE(es.name, 'FESC2020', 'FESC')
WHERE es.name LIKE '%FESC2020%';

COMMIT;

-- #10491 Formulari d'inscripció
-- - Crear taula modalitats
CREATE TABLE `fesc_registration_modality` (
  `id` bigint(20) NOT NULL,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `share_table_allowed` tinyint(1) NOT NULL default 0,
  PRIMARY KEY (`id`)
);
INSERT INTO sequence_table (seq_name, seq_count) VALUES('fesc_registration_modality_seq', 1);

-- - Crear taula quotes (fee)
CREATE TABLE `fesc_registration_fee` (
  `id` bigint(20) NOT NULL,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `fee` bigint(20) NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO sequence_table (seq_name, seq_count) VALUES('fesc_registration_fee_seq', 1);

-- - Crear taula M-N modalitats-quotes
CREATE TABLE `fesc_registration_modality_fee` (
  `modality_id` bigint(20) NOT NULL,
  `fee_id` bigint(20) NOT NULL,
  PRIMARY KEY (`modality_id`,`fee_id`),
  CONSTRAINT `FK_fesc_registration_modality_fee_modality_id` FOREIGN KEY (`modality_id`) REFERENCES `fesc_registration_modality` (`ID`),
  CONSTRAINT `FK_fesc_registration_modality_fee_fee_id` FOREIGN KEY (`fee_id`) REFERENCES `fesc_registration_fee` (`ID`)
);

-- - Afegir "modality" a SSEFesc i FK
ALTER TABLE social_economy_entity_fesc ADD modality_id bigint(20) DEFAULT NULL;
ALTER TABLE social_economy_entity_fesc ADD CONSTRAINT FK_fesc_registration_modality_id FOREIGN KEY (modality_id) REFERENCES fesc_registration_modality (ID);

-- - Afegir fellow_name a SSEFesc
ALTER TABLE social_economy_entity_fesc ADD fellow_name varchar(255) DEFAULT NULL;

-- # 10492 Canvis pestanya FESC
-- - Estat i quota any anterior
ALTER TABLE social_economy_entity_fesc ADD fesc_previous_entity_status_id bigint(20) DEFAULT NULL;
ALTER TABLE social_economy_entity_fesc ADD CONSTRAINT FK_fesc_previous_entity_status FOREIGN KEY (fesc_previous_entity_status_id) REFERENCES entity_status (ID);
update social_economy_entity_fesc set fesc_previous_entity_status_id = fesc_entity_status_id;
update social_economy_entity_fesc set fesc_entity_status_id = null;

ALTER TABLE social_economy_entity_fesc ADD fesc_previous_fee bigint(20) DEFAULT NULL;
update social_economy_entity_fesc set fesc_previous_fee = fesc_fee;
update social_economy_entity_fesc set fesc_fee = null;

commit;

-- #10493 Canvis pestanya comunicació
-- - Documents audiovisuals
CREATE TABLE `entity_audiovisual` (
  `id` bigint(20) NOT NULL,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` varchar(4096) DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `duration` varchar(10) COLLATE utf8_bin NOT NULL,
  `entity_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_entity_audiovisual_entity` (`entity_id`),
  CONSTRAINT `FK_entity_audiovisual_entity` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`)
);
INSERT INTO sequence_table (seq_name, seq_count) VALUES('entity_audiovisual_seq', 1);
commit;
