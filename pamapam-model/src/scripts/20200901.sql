INSERT INTO sequence_table (seq_name, seq_count) VALUES('entity_discount_seq', 2);

CREATE TABLE `entity_discount` (
  `id` bigint(20) NOT NULL,
  `version` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `description` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `entity_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `discount_FK` (`entity_id`),
  CONSTRAINT `discount_FK` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`)
)

CREATE TABLE `social_economy_entity_product` (
  `id_social_economy_entity` bigint(20) NOT NULL,
  `id_binary_resource` bigint(20) NOT NULL,
  KEY `social_economy_entity_product_FK` (`id_binary_resource`),
  KEY `social_economy_entity_product_FK_1` (`id_social_economy_entity`),
  CONSTRAINT `social_economy_entity_product_FK` FOREIGN KEY (`id_binary_resource`) REFERENCES `binary_resource` (`id`),
  CONSTRAINT `social_economy_entity_product_FK_1` FOREIGN KEY (`id_social_economy_entity`) REFERENCES `social_economy_entity` (`ID`)
)

