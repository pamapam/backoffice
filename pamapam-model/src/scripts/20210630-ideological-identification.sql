CREATE TABLE `ideological_identification` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE social_economy_entity ADD ideological_identification_id bigint(20) DEFAULT NULL;
ALTER TABLE social_economy_entity ADD CONSTRAINT FK_ideological_identification_id FOREIGN KEY (ideological_identification_id) REFERENCES ideological_identification (ID);

INSERT INTO ideological_identification (id,name,version) VALUES
	 (2,'Economia solidària',1),
	 (3,'Agroecologia',1),
	 (4,'Ecologisme',1),
	 (5,'Feminisme',1),
	 (6,'Esquerra independentista',1),
	 (7,'Sindicalisme',1);
	 
INSERT INTO sequence_table (seq_name, seq_count) VALUES('ideological_identification_seq', 8);

