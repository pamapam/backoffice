ALTER TABLE social_economy_entity ADD enterprises TINYINT DEFAULT 0 NOT NULL;
ALTER TABLE social_economy_entity ADD private_activists TINYINT DEFAULT 0 NOT NULL;
ALTER TABLE social_economy_entity ADD private_noobs TINYINT DEFAULT 0 NOT NULL;
