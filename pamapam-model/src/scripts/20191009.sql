ALTER TABLE social_economy_entity ADD last_update_date TIMESTAMP NULL;
 
CREATE TABLE pamapam_backoffice.search_info (
	id BIGINT NOT NULL,
	version BIGINT NOT NULL,
	text VARCHAR(500) NOT NULL,
	`date` TIMESTAMP NOT NULL,
	CONSTRAINT search_info_PK PRIMARY KEY (id)
)

INSERT INTO sequence_table (seq_name, seq_count) VALUES('search_info_seq', 1);