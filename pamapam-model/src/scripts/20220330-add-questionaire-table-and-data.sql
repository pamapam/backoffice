CREATE TABLE questionaire (
    `id` bigint(20) NOT NULL,
    `version` bigint(20) NOT NULL,
    `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
    PRIMARY KEY (`id`)
    );

INSERT INTO sequence_table (seq_name, seq_count) VALUES('questionaire_seq', 1);

INSERT INTO questionaire VALUES (1,0,'{"texts":[{"la":"ca","text":"Economia Social"},{"la":"es","text":""}]}');
INSERT INTO questionaire VALUES (2,0,'{"texts":[{"la":"ca","text":"Agroecològic"},{"la":"es","text":""}]}');

ALTER TABLE criterion ADD questionaire_id bigint(20);

CREATE TABLE criterion_questionaire (
    `id_criterion` bigint(20) NOT NULL,
    `id_questionaire` bigint(20) NOT NULL,
    KEY `criterion_questionaire_FK` (`id_criterion`),
    KEY `criterion_questionaire_FK_1` (`id_questionaire`),
    CONSTRAINT `criterion_questionaire_FK` FOREIGN KEY (`id_criterion`) REFERENCES `criterion` (`id`),
    CONSTRAINT `criterion_questionaire_FK_1` FOREIGN KEY (`id_questionaire`) REFERENCES `questionaire` (`ID`)
    );

SELECT id FROM criterion
WHERE criterion.old_version = 0 ;

INSERT INTO criterion_questionaire values(1, 1);
INSERT INTO criterion_questionaire values(2, 1);
INSERT INTO criterion_questionaire values(3, 1);
INSERT INTO criterion_questionaire values(4, 1);
INSERT INTO criterion_questionaire values(5, 1);
INSERT INTO criterion_questionaire values(6, 1);
INSERT INTO criterion_questionaire values(7, 1);
INSERT INTO criterion_questionaire values(8, 1);
INSERT INTO criterion_questionaire values(9, 1);
INSERT INTO criterion_questionaire values(10, 1);
INSERT INTO criterion_questionaire values(11, 1);
INSERT INTO criterion_questionaire values(12, 1);
INSERT INTO criterion_questionaire values(13, 1);
INSERT INTO criterion_questionaire values(14, 1);
INSERT INTO criterion_questionaire values(15, 1);

ALTER TABLE social_economy_entity ADD COLUMN questionaire_id bigint(20);