CREATE TABLE notification_role (notification_id BIGINT NOT NULL, role_id BIGINT NOT NULL, PRIMARY KEY (notification_id, role_id));

insert into notification_role(notification_id, role_id)
	select id, role_id from notification_config;

ALTER TABLE notification_config DROP FOREIGN KEY FK_notification_config_role_id;
ALTER TABLE notification_config DROP COLUMN role_id;
ALTER TABLE notification_config ADD name varchar(255) NULL;
ALTER TABLE notification_config ADD notification_type varchar(255) NULL;
ALTER TABLE notification_config MODIFY COLUMN user_entity_link_type varchar(255) DEFAULT NULL NULL;

update notification_config
	set notification_type = 'ENTITY_ACTION';

update notification_config
	set user_entity_link_type = 'OWNER'
	where user_entity_link_type = '0';
	
update notification_config
	set user_entity_link_type = 'UNRELATED'
	where user_entity_link_type = '1';

CREATE TABLE mail_template (ID BIGINT NOT NULL, CONTENT LONGTEXT, NAME VARCHAR(255) UNIQUE, version BIGINT, PRIMARY KEY (ID));
ALTER TABLE notification_config ADD mail_template_id bigint NULL;
ALTER TABLE notification_config ADD CONSTRAINT FK_notification_config_mail_template_id FOREIGN KEY (mail_template_id) REFERENCES mail_template (ID);

INSERT INTO sequence_table(seq_name, seq_count) values ('mail_template_seq', 999);

update social_economy_entity s1
	set s1.initiative_user_id = s1.registry_user_id
	where s1.id in (
		select s.id from (select * from social_economy_entity) s
			inner join user u on u.id = s.registry_user_id
			inner join user_role ur on ur.id_user = u.ID
			inner join role r on r.id = ur.id_role
			where r.ROLENAME = 'ROLE_initiative'
				and s.initiative_user_id is null);

update social_economy_entity s1
	set s1.registry_user_id = null
	where s1.id in (
		select s.id from (select * from social_economy_entity) s
			inner join user u on u.id = s.registry_user_id
			inner join user_role ur on ur.id_user = u.ID
			inner join role r on r.id = ur.id_role
			where r.ROLENAME = 'ROLE_initiative'
				and s.initiative_user_id = s.registry_user_id);
