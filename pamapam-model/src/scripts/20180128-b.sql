-- Fix binary resources without version.
update binary_resource
	set version = 1
	where version is null;