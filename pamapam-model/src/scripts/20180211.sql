
ALTER TABLE pamapam_backoffice.social_economy_entity ADD initiative_user_id bigint(20) DEFAULT NULL NULL;
ALTER TABLE social_economy_entity ADD CONSTRAINT FK_social_economy_entity_initiative_user_id FOREIGN KEY (initiative_user_id) REFERENCES user (ID);

update social_economy_entity see
	set initiative_user_id = (select id from user u where u.username = see.EMAIL)
	where initiative_user_id is null;
