-- Create time slot columns

ALTER TABLE social_economy_entity_fesc ADD time_slot VARCHAR(255) NULL;
ALTER TABLE fesc_registration_modality ADD is_time_slot BOOLEAN;
