-- jamgo framework update
ALTER TABLE town ADD province_id BIGINT NULL;
ALTER TABLE language ADD active TINYINT NULL;
ALTER TABLE binary_resource ADD description varchar(1024) NULL;
ALTER TABLE binary_resource CHANGE filelength file_length int(11) NULL;
ALTER TABLE binary_resource CHANGE filename file_name varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE binary_resource CHANGE mimetype mime_type varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE acl CHANGE can_read read_permission tinyint(1) NULL;
ALTER TABLE acl CHANGE can_update write_permission tinyint(1) NULL;
ALTER TABLE acl ADD rol_id BIGINT NULL;
ALTER TABLE acl ADD user_id BIGINT NULL;
ALTER TABLE acl DROP COLUMN secure_identity_id;

-- 20-registre-bulleti-exportable
delete from user_role where id_user = 5293;
delete from user where id = 5293;
update user set username = 'comunicacio@xes.cat', email = 'comunicacio@xes.cat'  where id = 5638;
ALTER TABLE newsletter_registration ADD registration_date TIMESTAMP NULL;
ALTER TABLE acl CHANGE can_read read_permission tinyint(1) NULL;
ALTER TABLE acl CHANGE can_update write_permission tinyint(1) NULL;
ALTER TABLE acl ADD rol_id BIGINT NULL;
ALTER TABLE acl ADD user_id BIGINT NULL;
ALTER TABLE acl DROP COLUMN secure_identity_id;

-- 20-registre-bulleti-exportable
delete from user_role where id_user = 5293;
delete from user where id = 5293;
update user set username = 'comunicacio@xes.cat', email = 'comunicacio@xes.cat'  where id = 5638;



