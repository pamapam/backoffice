
ALTER TABLE pamapam_role ADD CONSTRAINT FK_pamapam_role_role_id FOREIGN KEY (role_id) REFERENCES role (ID);
ALTER TABLE pamapam_role_entity_scope ADD CONSTRAINT FK_pamapam_role_entity_scope_pamapam_role_id FOREIGN KEY (pamapam_role_id) REFERENCES pamapam_role (ID);
ALTER TABLE pamapam_role_entity_scope ADD CONSTRAINT FK_pamapam_role_entity_scope_entity_scope_id FOREIGN KEY (entity_scope_id) REFERENCES entity_scope (ID);
ALTER TABLE entity_entity_scope ADD CONSTRAINT FK_entity_entity_scope_entity_id FOREIGN KEY (entity_id) REFERENCES social_economy_entity (ID);
ALTER TABLE entity_entity_scope ADD CONSTRAINT FK_entity_entity_scope_entity_scope_id FOREIGN KEY (entity_scope_id) REFERENCES entity_scope (ID);

INSERT INTO sequence_table(seq_name, seq_count) values ('entity_scope_seq', 999);
INSERT INTO sequence_table(seq_name, seq_count) values ('pamapam_role_seq', 999);

-- Dos ámbitos: Pam a Pam y Einateca
INSERT INTO entity_scope(id, name, version) VALUES (1, 'Pam a Pam', 0);
INSERT INTO entity_scope(id, name, version) VALUES (2, 'Einateca', 0);

-- Para todos los roles existentes crear una row en pamapam_role
INSERT INTO pamapam_role(id, role_id, version) 
	SELECT ID, id, 0 FROM `role` ;

-- Relacionar todos los roles con el ámbito Pam a Pam
INSERT INTO pamapam_role_entity_scope(pamapam_role_id, entity_scope_id)
	SELECT id, 1 FROM pamapam_role ;

-- Relacionar admin con el ámbito Einateca
INSERT INTO pamapam_role_entity_scope(pamapam_role_id, entity_scope_id) VALUES (1, 2);

-- Agregar rol xinxeta einateca
INSERT INTO `role`(ID, DESCRIPTION, ROLENAME, version) VALUES (5003, 'Xinxeta Einateca', 'ROLE_xinxeta_einateca', 0);
INSERT INTO pamapam_role(id, role_id, version) VALUES (5003, 5003, 0);

-- Relacionar rol xinxeta einateca con el ámbito Einateca
INSERT INTO pamapam_role_entity_scope(pamapam_role_id, entity_scope_id) VALUES(5003, 2);

-- Relacionar todas las entidades con el ámbito Pam a Pam
INSERT INTO entity_entity_scope(entity_id, entity_scope_id)
	SELECT id, 1 FROM social_economy_entity; 

-- Add entity scopes to embedde map config
CREATE TABLE embedded_map_entiny_scope (embedded_map_config_id BIGINT NOT NULL, entity_scope_id BIGINT NOT NULL, PRIMARY KEY (embedded_map_config_id, entity_scope_id));

ALTER TABLE embedded_map_entiny_scope ADD CONSTRAINT embedded_map_entiny_scope_embedded_map_config_id FOREIGN KEY (embedded_map_config_id) REFERENCES embedded_map_config (ID);
ALTER TABLE embedded_map_entiny_scope ADD CONSTRAINT FK_embedded_map_entiny_scope_entity_scope_id FOREIGN KEY (entity_scope_id) REFERENCES entity_scope (ID);

-- Relacionar las configuraciones de embedded maps actuales con en el scope Pam a Pam
INSERT INTO embedded_map_entiny_scope(embedded_map_config_id, entity_scope_id)
	SELECT id, 1 FROM embedded_map_config;
