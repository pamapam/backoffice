-- -El nom de la pestanya seria Avaluació ESS (no Economia Social) i Avaluació agroecològica (no agroecològic)
UPDATE questionaire
SET name = '{"texts":[{"la":"ca","text":"ESS"},{"la":"es","text":""}]}'
WHERE id = 1;

UPDATE questionaire
SET name = '{"texts":[{"la":"ca","text":"Agroecològica"},{"la":"es","text":""}]}'
WHERE id = 2;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 En el cas d''iniciatives familiars, es vetlla per a que es doni un repartiment equitatiu entre l''esfera productiva i domèstica."}]}'
WHERE id = 93;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Donen una revalorització de les tasques de cures de la iniciativa i aquestes estan repartides equitativament (cura de l’espai, cura de les persones, prendre actes…). És a dir, no hi ha divisió sexual del treball clàssica (home= producció i camp; dona= processament aliments, gestió administrativa i domèstica...)"}]}'
WHERE id = 96;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"Han realitzat una anàlisi de les dinàmiques de gènere i poder i existeix un repartiment equitatiu de les tasques de representació (visibilitat pública dels i de les membres, portaveus, conferències...), titularitat/cotitularitat de la finca i de decisió (lideratges col·lectius, distribució del poder, presència paritària als òrgans de govern/ direcció)."}]}'
WHERE id = 97;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 S''articulen amb altres iniciatives (turisme, distribució, educació...) per tal de crear xarxes alimentàries locals o territorials."}]}'
WHERE id = 102;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 S''articulen amb altres productores o elaboradores agroalimentàries per crear xarxes agroecològiques."}]}'
WHERE id = 103;

-- 11 tots els criteris agroeco estan duplicats, cal eliminar-los i deixar els agroeco
DELETE  FROM criterion_answer_questionaire
        WHERE id_criterion_answer IN(51,52,53,54,55,86) AND id_questionaire = 2;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 S''articulen amb altres productores o elaboradores agroalimentàries per crear xarxes agroecològiques."}]}'
WHERE id = 103;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 La iniciativa desenvolupa una activitat econòmica o comunitària vinculada a una necessitat local o la recuperació del patrimoni natural, cultural, històric... (ús o recuperació de llavors de varietats locals adaptades, de camins, d’erms… )"}]}'
WHERE id = 115;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Treballen fent incidència política i/o fent divulgació sobre la reducció de l’impacte ambiental, promovent educació ambiental des de la iniciativa productiva."}]}'
WHERE id = 117;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Prenen mesures per reduir la petjada ecològica en l’activitat econòmica tenint en compte la mobilitat, la temporalitat natural del cultiu de cada varietat, evitant monocultius, utilitzant el mínim d’insums externs..."}]}'
WHERE id = 116;

UPDATE criterion_answer
SET answer = '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Es fa ús del compostatge de matèria orgànica i s’aprofita i es recicla el màxim de subproductes i residus dins la pròpia finca/projecte."}]}'
WHERE id = 118;

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(121,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃  S''afavoreix l''economia circular per tal de reaprofitar material o productes dels que ja no es fa ús. Un residu es converteix en matèria prima per algú altre."}]}',
       4, 0, 14);
INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(121, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(122,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Més de tres quartes parts de les les persones contractades dutant tot l’any gaudeixen de condicions laborals dignes."}]}',
       3, 0, 4);
INSERT INTO criterion_answer_questionaire VALUES(122, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(123,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Més de tres quartes parts de les les persones contractades puntulment gaudeixen de condicions laborals dignes. (no comptabilitzades a l’apartat de Mercat Social del backoffice)"}]}',
       4, 0, 4);
INSERT INTO criterion_answer_questionaire VALUES(123, 2);

DELETE  FROM criterion_answer_questionaire
WHERE id_criterion_answer IN(14,18,19) AND id_questionaire = 2;

