select @statusChangeId := max(id) from entity_status_change;
insert into entity_status_change (id, change_timestamp, version, entity_id, entity_status_from_id, entity_status_to_id, user_id)
select (@statusChangeId := @statusChangeId + 1), timestamp(t1.registry_date), 1, t1.id, null, t1.entity_status_id, t1.registry_user_id from social_economy_entity t1
	left outer join entity_status_change t2 on t2.entity_id = t1.ID  and t2.entity_status_to_id = t1.entity_status_id
	where t2.id is null;
select @statusChangeId := max(id) from entity_status_change;
update sequence_table
	set seq_count = @statusChangeId + 1
	where seq_name = 'entity_status_change_seq';

delete from notification_entity_status
	where notification_id in
		(select id from notification_config where entity_actions = '');

delete from notification_config where entity_actions = '';