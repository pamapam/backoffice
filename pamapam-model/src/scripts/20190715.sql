ALTER TABLE pamapam_backoffice.embedded_map_config ADD latitude DECIMAL(6,3) NULL;
ALTER TABLE pamapam_backoffice.embedded_map_config ADD longitude decimal(6,3) NULL;
ALTER TABLE pamapam_backoffice.embedded_map_config ADD zoom decimal(6,3) NULL;