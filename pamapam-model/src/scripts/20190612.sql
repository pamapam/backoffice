CREATE TABLE model_attribute_def (ID BIGINT NOT NULL, LABEL VARCHAR(255), NAME VARCHAR(255), TYPE INTEGER, version BIGINT, model_def_id BIGINT, PRIMARY KEY (ID));
CREATE TABLE model_def (ID BIGINT NOT NULL, model_name VARCHAR(255), version BIGINT, PRIMARY KEY (ID));
ALTER TABLE model_attribute_def ADD CONSTRAINT FK_model_attribute_def_model_def_id FOREIGN KEY (model_def_id) REFERENCES model_def (ID);
ALTER TABLE social_economy_entity ADD custom_fields TEXT NULL;
INSERT INTO sequence_table(seq_name, seq_count) values ('model_def_seq', 1000);
INSERT INTO sequence_table(seq_name, seq_count) values ('model_attribute_def_seq', 999);
INSERT INTO model_def(ID, model_name, version) values (1000, 'SocialEconomyEntity', 1);
