-- Insert en pamapam_role para el role nuevo
INSERT INTO pamapam_role (id, version, role_id) values(5004, 0, 5);

-- Relacionar rol pol cooperatiu con el ámbito Pol Cooperatiu
INSERT INTO pamapam_role_entity_scope(pamapam_role_id, entity_scope_id) VALUES(5004, 5);

-- Añadir columna pol cooperatiu
ALTER TABLE social_economy_entity ADD COLUMN polcooperatiu TEXT;

-- Tirar la columna polcooperatiu de social_economy_entity
ALTER TABLE social_economy_entity DROP COLUMN polcooperatiu;


-- Añadir tabla text_url

CREATE TABLE text_url (
                          id bigint(20) NOT NULL,
                          name TEXT,
                          url TEXT
);

ALTER TABLE pamapam_backoffice.text_url ADD PRIMARY KEY (`id`);
ALTER TABLE pamapam_backoffice.text_url ADD COLUMN version BIGINT


-- Añadir tabla pivot entre text_url y social_economy_entity

CREATE TABLE pamapam_backoffice.entity_text_url (
                                                    `text_url_id` bigint(20) NOT NULL,
                                                    `entity_id` bigint(20) NOT NULL,
                                                    KEY `entity_text_url_FK` (`text_url_id`),
                                                    KEY `entity_text_url_FK_1` (`entity_id`)
);

ALTER TABLE pamapam_backoffice.entity_text_url ADD CONSTRAINT `entity_text_url_FK_1` FOREIGN KEY (`entity_id`) REFERENCES `social_economy_entity` (`ID`)

ALTER TABLE pamapam_backoffice.entity_text_url ADD CONSTRAINT entity_text_url_FK FOREIGN KEY (text_url_id) REFERENCES pamapam_backoffice.text_url (id);

