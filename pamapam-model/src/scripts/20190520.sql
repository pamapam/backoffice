CREATE TABLE embedded_map_config (ID BIGINT NOT NULL, api_key VARCHAR(255), DOMAIN VARCHAR(255), version BIGINT, PRIMARY KEY (ID));
CREATE TABLE embedded_map_config_tag (embedded_map_config_id BIGINT NOT NULL, external_filter_tag_id BIGINT NOT NULL, PRIMARY KEY (embedded_map_config_id, external_filter_tag_id));
CREATE TABLE embedded_map_config_status (embedded_map_config_id BIGINT NOT NULL, status_id BIGINT NOT NULL, PRIMARY KEY (embedded_map_config_id, status_id));
ALTER TABLE embedded_map_config_tag ADD CONSTRAINT FK_embedded_map_config_tag_external_filter_tag_id FOREIGN KEY (external_filter_tag_id) REFERENCES external_filter_tag (ID);
ALTER TABLE embedded_map_config_tag ADD CONSTRAINT FK_embedded_map_config_tag_embedded_map_config_id FOREIGN KEY (embedded_map_config_id) REFERENCES embedded_map_config (ID);
ALTER TABLE embedded_map_config_status ADD CONSTRAINT embedded_map_config_status_embedded_map_config_id FOREIGN KEY (embedded_map_config_id) REFERENCES embedded_map_config (ID);
ALTER TABLE embedded_map_config_status ADD CONSTRAINT FK_embedded_map_config_status_status_id FOREIGN KEY (status_id) REFERENCES entity_status (ID);
INSERT INTO sequence_table(seq_name, seq_count) values ('embedded_map_config_seq', 999);

