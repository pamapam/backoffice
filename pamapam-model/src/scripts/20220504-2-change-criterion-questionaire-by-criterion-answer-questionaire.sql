DROP TABLE criterion_questionaire;
ALTER TABLE criterion DROP COLUMN questionaire_id;

CREATE TABLE criterion_answer_questionaire (
   `id_criterion_answer` bigint(20) NOT NULL,
   `id_questionaire` bigint(20) NOT NULL,
   KEY `criterion_questionaire_FK` (`id_criterion_answer`),
   KEY `criterion_questionaire_FK_1` (`id_questionaire`),
   CONSTRAINT `criterion_questionaire_FK` FOREIGN KEY (`id_criterion_answer`) REFERENCES `criterion_answer` (`id`),
   CONSTRAINT `criterion_questionaire_FK_1` FOREIGN KEY (`id_questionaire`) REFERENCES `questionaire` (`ID`)
);

-- Añadir CriterionAnswers a cada questionaire
INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(91,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Existeix un espai de presa de decisions formal, periòdic i participat per totes les persones membres de la iniciativa."}]}',
       1,
       0,
       1);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(91, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(92,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Es recullen els acords presos i són accessibles per a la seva revisió i avaluació."}]}',
       2, 0, 1);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(92, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(93,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 En el cas d''iniciatives familiars, es vetlla per a que es doni un repartiment equitatiu entre l''esfera productiva i domèstica."}]}',
       2, 0, 2);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(93, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(94,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Hi ha dones formant part de la iniciativa en l''àmbit formal"}]}',
       1, 0, 3);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(94, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(95,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 S''ha donat una reflexió sobre el gènere en el món rural i es duu a terme alguna mesura al respecte (comunicació no sexista, reivindicació del paper de la dona al camp...)"}]}',
       2, 0, 3);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(95, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(96,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Donen una revalorització de les tasques de cures de la iniciativa i aquestes estan repartides equitativament (cura de l’espai, cura de les persones, prendre actes…). És a dir, no hi ha divisió sexual del treball clàssica (home= producció i camp; dona= processament aliments, gestió administrativa i domèstica...)"}]}',
       3, 0, 3);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(96, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(97,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Han realitzat una anàlisi de les dinàmiques de gènere i poder i existeix un repartiment equitatiu de les tasques de representació (visibilitat pública dels i de les membres, portaveus, conferències...), titularitat/cotitularitat de la finca i de decisió (lideratges col·lectius, distribució del poder, presència paritària als òrgans de govern/ direcció)."}]}',
       4, 0, 3);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(97, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(98,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Prioritzen el consum i/o distribució de béns i serveis locals, apostant pels Circuits Curts de Comercialització (CCC)."}]}',
       1, 0, 5);
INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(98, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(99,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Els insums necessaris per a la producció són respectuosos ecosocialment (no s''usen fitosanitaris ni fertilitzants químics de síntesis, ni herbicides, ni llavors modificades genèticament (OMG))  i han estat produïts respectant condicions de treball dignes."}]}',
       2, 0, 5);
INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(99, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(100,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃  És política interna de la iniciativa prioritzar proveïdors de l’ESS/del moviment agroecològic quan es necessita un producte o servei."}]}',
       1, 0, 6);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(100, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(101,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Existeix una política de preus específica (preu social, intercanvi...) per als clients/es de l''ESS i/o les xarxes agroecològiques / alimentàries locals "}]}',
       2, 0, 6);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(101, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(102,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 S''articulen amb altres organitzacions que operen en el sector agroalimentari en activitats complementàries per tal de crear xarxes alimentàries locals o territorials."}]}',
       3, 0, 6);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(102, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(103,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 S''articulen amb altres organitzacions del mateix sector agroalimentari per tal de crear xarxes agroecològiques  i no competir."}]}',
       4, 0, 6);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(103, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(104,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Compleixen els anteriors criteris i, a més a més, contribueixen estratègicament a la creació de mercat social articulant-se amb organitzacions que no pertanyen al sector agroalimentari."}]}',
       5, 0, 6);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(104, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(105,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 És política interna de la iniciativa publicar la major part del contingut que es produeix sota llicències lliures i compartir-ho amb la comunitat (inclou ús de bancs de llavors, recuperació i intercanvi d''espècies locals...)"}]}',
       4, 0, 7);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(105, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(106,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Fan tasques de sensibilització i difusió sobre l''ús i creació de continguts sota llicències lliures (inclou ús de bancs de llavors, recuperació i intercanvi d''espècies locals...)"}]}',
       5, 0, 7);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(106, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id, questionaire_id)
VALUES(107,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 [Transparència externa] Mostren proactivament l’organigrama de les persones que conformen la iniciativa i/o els objectius i valors de la iniciativa"}]}',
       1, 0, 8, 2);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(107, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(108,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 [Transparència externa] Fan ús d''algun sistema d''auditoria, certificació o segell que garanteix la producció agroecològica"}]}',
       4, 0, 8);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(108, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id, questionaire_id)
VALUES(109,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 [Transparència externa]Es promou algun Sistema de Garantia Participativa (SPG), promovent processos de confiança i empoderament entre productores i consumidores"}]}',
       5, 0, 8, 2);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(109, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(110,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Tenen estratègies per fomentar que qualsevol persona participi activament en la iniciativa com a voluntària o en plantilla (per exemple, pràctiques, “woofers”, plans ocupacionals...)."}]}',
       3, 0, 10);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(110, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(111,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 La voluntat de la iniciativa és promoure la transformació ecosocial/sobirania alimentària i ho expliciten a través dels seus estatuts, manifestos o documents interns i/o públics."}]}',
       1, 0, 11);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(111, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(112,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Es col·labora regularment amb diferents moviments per la transformació ecosocial/sobirania alimentària."}]}',
       2, 0, 11);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(112, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(113,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Participen en xarxes i articulacions polítiques per a la transformació ecosocial/sobirania alimentària fent incidència política."}]}',
       4, 0, 11);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(113, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(114,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Treballen proposant un model socioeconòmic per superar el model capitalista (més enllà de la sobirania alimentària)"}]}',
       5, 0, 11);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(114, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(115,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 La iniciativa desenvolupa una activitat econòmica o comunitària vinculada a una necessitat local o la recuperació del patrimoni natural, cultural, històric... (ús o recuperació de llavors de varietats locals, regeneració del sòl o gestió del paisatge -mitjançant les pastures, el manteniment dels marges i del mosaic agroforestal, la millora de la biodiversitat dins i/o al voltant de la finca, la gestió forestal- ... )"}]}',
       4, 0, 11);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(115, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(116,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Prenen mesures per reduir la petjada ecològica en l’activitat econòmica tenint en compte la mobilitat, la temporalitat natural del cultiu de cada varietat, evitant monocultius, utilitzant el mínim d’insums externs"}]}',
       3, 0, 13);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(116, 2);

INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(117,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Treballen fent incidència política i/o fent divulgació sobre la reducció de l’impacte ambiental, promovent educació ambiental des de la iniciativa productiva..."}]}',
       4, 0, 13);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(117, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(118,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Es fa ús del compostatge de matèria orgànica i s’aprofita i es recicla el màxim de subproductes i residus."}]}',
       3, 0, 14);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(118, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(119,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 Es disposa de sistemes de captació i emmagatzematge de l''aigua de pluja."}]}',
       1, 0, 15);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(119, 2);


INSERT INTO criterion_answer (ID, ANSWER, answer_order, version, criterion_id)
VALUES(120,
       '{"texts":[{"la":"es","co":"ES","text":""},{"la":"ca","co":"ES","text":"🍃 S''han generat cicles d''energia dins la mateixa finca: plaques solars, biomassa..."}]}',
       4, 0, 15);

INSERT INTO criterion_answer_questionaire (id_criterion_answer, id_questionaire) values(120, 2);

-- Vincular los criterios que ya existían pero están presentes en el nuevo tipo de evaluación
INSERT INTO criterion_answer_questionaire (id_criterion_answer,id_questionaire) VALUES
                                                                                                       (3,2),
                                                                                                       (4,2),
                                                                                                       (5,2),
                                                                                                       (6,2),
                                                                                                       (8,2),
                                                                                                       (9,2),
                                                                                                       (10,2),
                                                                                                       (15,2),
                                                                                                       (16,2),
                                                                                                       (17,2);

INSERT INTO criterion_answer_questionaire (id_criterion_answer,id_questionaire) VALUES
                                                                                                       (18,2),
                                                                                                       (19,2),
                                                                                                       (20,2),
                                                                                                       (23,2),
                                                                                                       (24,2),
                                                                                                       (25,2),
                                                                                                       (31,2),
                                                                                                       (32,2),
                                                                                                       (33,2),
                                                                                                       (37,2);

INSERT INTO criterion_answer_questionaire (id_criterion_answer,id_questionaire) VALUES
                                                                                                       (38,2),
                                                                                                       (41,2),
                                                                                                       (42,2),
                                                                                                       (43,2),
                                                                                                       (44,2),
                                                                                                       (45,2),
                                                                                                       (46,2),
                                                                                                       (47,2),
                                                                                                       (49,2),
                                                                                                       (50,2);

INSERT INTO criterion_answer_questionaire (id_criterion_answer,id_questionaire) VALUES
                                                                                                       (51,2),
                                                                                                       (52,2),
                                                                                                       (53,2),
                                                                                                       (54,2),
                                                                                                       (55,2),
                                                                                                       (56,2),
                                                                                                       (57,2),
                                                                                                       (58,2),
                                                                                                       (60,2),
                                                                                                       (61,2);

INSERT INTO criterion_answer_questionaire (id_criterion_answer,id_questionaire) VALUES
                                                                                                       (62,2),
                                                                                                       (65,2),
                                                                                                       (66,2),
                                                                                                       (67,2),
                                                                                                       (69,2),
                                                                                                       (70,2),
                                                                                                       (73,2),
                                                                                                       (74,2),
                                                                                                       (75,2),
                                                                                                       (76,2);
INSERT INTO criterion_answer_questionaire (id_criterion_answer,id_questionaire) VALUES
                                                                                                       (77,2),
                                                                                                       (78,2),
                                                                                                       (79,2),
                                                                                                       (80,2),
                                                                                                       (85,2),
                                                                                                       (86,2),
                                                                                                       (87,2),
                                                                                                       (88,2),
                                                                                                       (89,2),
                                                                                                       (90,2);

