set -x
# ... Dump sample_ tables.
docker exec -i pamapam-backoffice-db mysqldump -u root -pjamgo --complete-insert --no-create-db --no-create-info --no-tablespaces --skip-add-drop-table --skip-add-locks --skip-comments --skip-compact --skip-extended-insert --skip-quote-names --ignore-table=pamapam_backoffice.binary_resource --ignore-table=pamapam_backoffice.sequence_table pamapam_backoffice > .temp-sample.sql
# ... Remove comment and empty lines.
cat .temp-sample.sql | grep -v -e "/\*.*\*/" -e "^[[:space:]]*$" | cat - > .temp-sample-1.sql
# ... Replace escaped chars, prefixes and uppercase.
sed -e "s/\\\'/\'\'/g" \
	-e 's/\\"/"/g' \
	-e "s/INSERT INTO/insert into/" \
	-e "s/) VALUES (/) values (/" \
	.temp-sample-1.sql > .temp-sample-2.sql
# ... Adds header and footer.
sed -e "1iset referential_integrity false;" \
	-e "\$aset referential_integrity true;" \
	.temp-sample-2.sql > .temp-sample-3.sql
# ... Remove temporary files.
mv .temp-sample-3.sql test-database.sql
rm .temp*