package org.pamapam.repository.search;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Set;

import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamapam.model.config.PamapamBackofficeModelTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { PamapamBackofficeModelTestConfig.class, FullTextServiceTest.Config.class })
public class FullTextServiceTest {

	@Configuration
	static class Config {
		@Bean
		public FullTextService fullTextService() {
			return new FullTextService();
		}
	}

	@Autowired
	private FullTextService fullTextService;

	@Test
	@Ignore // FIXME: Disabled until regeneration of obfuscated test data.
	public void tesFindEntities() throws IOException, ParseException {
		Set<Long> entityIds = this.fullTextService.findEntities("empresa");
		// ...	TODO: Do a real test.
		assertNotNull(entityIds);
	}
}
