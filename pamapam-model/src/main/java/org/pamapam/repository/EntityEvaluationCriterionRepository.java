package org.pamapam.repository;

import org.pamapam.model.EntityEvaluationCriterion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityEvaluationCriterionRepository extends JpaRepository<EntityEvaluationCriterion, Long> {

}
