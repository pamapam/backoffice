package org.pamapam.repository;

import org.pamapam.model.EmbeddedMapConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmbeddedMapConfigRepository extends JpaRepository<EmbeddedMapConfig, Long> {

	EmbeddedMapConfig findByDomainAndApiKey(String domain, String apiKey);
}
