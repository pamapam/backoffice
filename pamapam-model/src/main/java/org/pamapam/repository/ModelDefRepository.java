package org.pamapam.repository;

import org.pamapam.model.ModelDef;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelDefRepository extends JpaRepository<ModelDef, Long> {

	ModelDef findByModelName(String modelName);
}
