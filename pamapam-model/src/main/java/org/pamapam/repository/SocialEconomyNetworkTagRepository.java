package org.pamapam.repository;

import org.pamapam.model.SocialEconomyNetworkTag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SocialEconomyNetworkTagRepository extends JpaRepository<SocialEconomyNetworkTag, Long> {

	List<SocialEconomyNetworkTag> findByTag(String tag);
}
