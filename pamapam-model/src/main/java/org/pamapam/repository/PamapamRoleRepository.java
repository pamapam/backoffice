package org.pamapam.repository;

import java.util.List;
import java.util.Set;

import org.jamgo.model.entity.Role;
import org.pamapam.model.PamapamRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PamapamRoleRepository extends JpaRepository<PamapamRole, Long> {

	PamapamRole findByRole(Role role);

	@Query("select pr from PamapamRole pr where pr.role in :roles")
	List<PamapamRole> findByRoleIn(@Param("roles") Set<Role> roles);
}
