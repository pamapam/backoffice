package org.pamapam.repository;

import org.pamapam.model.CollaborationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CollaborationEntityRepository extends JpaRepository<CollaborationEntity, Long> {

}
