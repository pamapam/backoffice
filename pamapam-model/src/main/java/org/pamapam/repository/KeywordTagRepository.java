package org.pamapam.repository;

import java.util.List;

import org.pamapam.model.KeywordTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeywordTagRepository extends JpaRepository<KeywordTag, Long> {

	List<KeywordTag> findByTag(String tag);
}
