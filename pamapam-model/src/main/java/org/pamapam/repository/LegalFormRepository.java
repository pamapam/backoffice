package org.pamapam.repository;

import org.pamapam.model.LegalForm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LegalFormRepository extends JpaRepository<LegalForm, Long> {

}
