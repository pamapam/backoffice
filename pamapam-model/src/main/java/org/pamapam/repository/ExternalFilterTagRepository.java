package org.pamapam.repository;

import java.util.Collection;
import java.util.Set;

import org.pamapam.model.ExternalFilterTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ExternalFilterTagRepository extends JpaRepository<ExternalFilterTag, Long> {

	@Query("select e from ExternalFilterTag e where e.tagText in :tagTexts")
	Set<ExternalFilterTag> findByTagTextIn(@Param("tagTexts") Collection<String> tagTexts);

}
