package org.pamapam.repository;

import org.pamapam.model.SectorCcae;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorCcaeRepository extends JpaRepository<SectorCcae, Long> {

}
