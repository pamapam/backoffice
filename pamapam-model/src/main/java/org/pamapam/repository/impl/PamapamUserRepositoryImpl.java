package org.pamapam.repository.impl;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.queries.ScrollableCursor;
import org.pamapam.model.PamapamUser;
import org.pamapam.repository.PamapamUserCustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

public class PamapamUserRepositoryImpl implements PamapamUserCustomRepository {

	@Autowired
	private EntityManager entityManager;

	@Override
	public Stream<PamapamUser> streamAll() {
		return this.streamAll(null);
	}

	@Override
	public Stream<PamapamUser> streamAll(Specification<PamapamUser> specification) {
		CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<PamapamUser> criteriaQuery = criteriaBuilder.createQuery(PamapamUser.class);
		if (specification != null) {
			Root<PamapamUser> root = criteriaQuery.from(PamapamUser.class);
			Predicate predicate = specification.toPredicate(root, criteriaQuery, criteriaBuilder);
			criteriaQuery.where(predicate);
		}
		Query query = this.entityManager.createQuery(criteriaQuery);
		query.setHint("eclipselink.cursor.scrollable", true);
		ScrollableCursor scrollableCursor = (ScrollableCursor) query.getSingleResult();
		@SuppressWarnings("unchecked")
		Spliterator<PamapamUser> spliterator = Spliterators.spliteratorUnknownSize(scrollableCursor, Spliterator.NONNULL);
		return StreamSupport.stream(spliterator, false);
	}

}
