package org.pamapam.repository.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.persistence.queries.ScrollableCursor;
import org.jamgo.model.entity.*;
import org.pamapam.model.*;
import org.pamapam.repository.SocialEconomyEntityCustomRepository;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class SocialEconomyEntityRepositoryImpl implements SocialEconomyEntityCustomRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Stream<SocialEconomyEntity> streamAll() {
        return this.streamAll(null);
    }

    @Override
    public Stream<SocialEconomyEntity> streamAll(final Specification<SocialEconomyEntity> specification) {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<SocialEconomyEntity> criteriaQuery = criteriaBuilder.createQuery(SocialEconomyEntity.class);
        if (specification != null) {
            final Root<SocialEconomyEntity> root = criteriaQuery.from(SocialEconomyEntity.class);
            final Predicate predicate = specification.toPredicate(root, criteriaQuery, criteriaBuilder);
            criteriaQuery.where(predicate);
        }
        final Query query = this.entityManager.createQuery(criteriaQuery);
        query.setHint("eclipselink.cursor.scrollable", true);
        final ScrollableCursor scrollableCursor = (ScrollableCursor) query.getSingleResult();
        @SuppressWarnings("unchecked") final Spliterator<SocialEconomyEntity> spliterator = Spliterators.spliteratorUnknownSize(scrollableCursor, Spliterator.NONNULL);
        return StreamSupport.stream(spliterator, false);
    }

    @Override
    public List<Tuple> findTuples(final Specification<SocialEconomyEntity> specification, final PageRequest pageRequest) {
        SocialEconomyEntitySpecification socialEconomyEntitySpecification = (SocialEconomyEntitySpecification) specification;
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();

        final Root<SocialEconomyEntity> root = criteriaQuery.from(SocialEconomyEntity.class);

        final Join<SocialEconomyEntity, BinaryResource> picture = root.join("picture", JoinType.LEFT);
        final Join<SocialEconomyEntity, Sector> mainSector = root.join("mainSector", JoinType.LEFT);
        final Join<SocialEconomyEntity, Sector> mainOfficeMainSector = root.join("mainOffice", JoinType.LEFT).join("mainSector", JoinType.LEFT);
        final Join<SocialEconomyEntity, Region> town = root.join("town", JoinType.LEFT);
        final Join<SocialEconomyEntity, EntityStatus> status = root.join("entityStatus", JoinType.LEFT);

        criteriaQuery
                .multiselect(
                        root.get("id").alias("id"),
                        root.get("name").alias("name"),
                        root.get("phone").alias("phone"),
                        root.get("normalizedName").alias("normalizedName"),
                        root.get("description").alias("description"),
                        root.get("latitude").alias("latitude"),
                        root.get("longitude").alias("longitude"),
                        root.get("xesBalance").alias("xesBalance"),
                        root.get("greenCommerce").alias("greenCommerce"),
                        root.get("urlBalancSocial").alias("xesBalanceUrl"),
                        picture.get("id").alias("pictureId"),
                        mainSector.get("id").alias("mainSectorId"),
                        mainOfficeMainSector.get("id").alias("mainOfficeMainSectorId"),
                        status.get("id").alias("statusId"),
                        status.get("entityStatusType").alias("statusType"),
                        town.get("id").alias("townId"))
//                .orderBy(criteriaBuilder.asc(root.get("name")))
                .distinct(true);

        if (socialEconomyEntitySpecification != null) {
            final Predicate predicate = socialEconomyEntitySpecification.toPredicate(root, criteriaQuery, criteriaBuilder);
            criteriaQuery.where(predicate);
            Set<Sector> sectors = socialEconomyEntitySpecification.getSearchObject().getSectorsAndSubsectors();
            if(sectors != null && !sectors.isEmpty()) {
                Set<Long> sectorsIds = sectors.stream().map(sector -> sector.getId()).collect(Collectors.toSet());
                Expression<Integer> caseExp = criteriaBuilder.<Integer>selectCase()
                        .when(mainSector.get("id").in(sectorsIds), 1)
                        .when(mainOfficeMainSector.get("id").in(sectorsIds), 2)
                        .when(criteriaBuilder.or(
                                root.join("sectors", JoinType.LEFT).in(sectorsIds),
                                root.join("mainSector", JoinType.LEFT).in(sectorsIds)), 3)
                        .otherwise(4);
                criteriaQuery.orderBy(Arrays.asList(criteriaBuilder.asc(caseExp), criteriaBuilder.asc(root.get("name"))));
            }
        }

        final Query query = this.entityManager.createQuery(criteriaQuery);
        return query
                .setFirstResult(pageRequest.getOffset())
                .setMaxResults(pageRequest.getPageSize())
                .getResultList();
    }

    @Override
    public List<Tuple> findDetailTuples(final Specification<SocialEconomyEntity> specification) {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();

        final Root<SocialEconomyEntity> root = criteriaQuery.from(SocialEconomyEntity.class);

        final Join<SocialEconomyEntity, BinaryResource> picture = root.join("picture", JoinType.LEFT);
        final Join<SocialEconomyEntity, Sector> mainSector = root.join("mainSector", JoinType.LEFT);
        final Join<SocialEconomyEntity, Sector> mainOfficeMainSector = root.join("mainOffice", JoinType.LEFT).join("mainSector", JoinType.LEFT);
        final Join<SocialEconomyEntity, Region> region = root.join("region", JoinType.LEFT);
        final Join<SocialEconomyEntity, Town> town = root.join("town", JoinType.LEFT);
        final Join<SocialEconomyEntity, Neighborhood> neighborhood = root.join("neighborhood", JoinType.LEFT);
        final Join<SocialEconomyEntity, District> district = root.join("district", JoinType.LEFT);
        final Path<Set<Long>> sectors = root.join("sectors", JoinType.LEFT).get("id");
        final Path<Set<Long>> productTags = root.join("productTag", JoinType.LEFT).get("tag");
        final Path<Set<Long>> entityEvaluation = root.join("entityEvaluation", JoinType.LEFT).get("id");
        final Path<Set<Long>> legalForm = root.join("legalForm", JoinType.LEFT).get("name");
        final Path<Set<Long>> socialEconomyNetwork = root.join("socialEconomyNetworks", JoinType.LEFT).get("name");
        final Path<Set<Long>> otherSocialEconomyNetwork = root.join("otherSocialEconomyNetworks", JoinType.LEFT).get("tag");
        final Path<Set<Long>> keywordTags = root.join("keywordTags", JoinType.LEFT).get("tag");

        criteriaQuery
                .multiselect(
                        root.get("id").alias("id"),
                        root.get("name").alias("name"),
                        root.get("normalizedName").alias("normalizedName"),
                        root.get("description").alias("description"),
                        root.get("latitude").alias("latitude"),
                        root.get("longitude").alias("longitude"),
                        root.get("web").alias("web"),
                        root.get("email").alias("email"),
                        root.get("phone").alias("phone"),
                        root.get("twitter").alias("twitter"),
                        root.get("facebook").alias("facebook"),
                        root.get("instagram").alias("instagram"),
                        root.get("pinterest").alias("pinterest"),
                        root.get("quitter").alias("quitter"),
                        root.get("address").alias("address"),
                        root.get("openingHours").alias("openingHours"),
                        root.get("xesBalance").alias("xesBalance"),
                        picture.get("id").alias("pictureId"),
                        mainSector.get("id").alias("mainSectorId"),
                        mainOfficeMainSector.get("id").alias("mainOfficeMainSectorId"),
                        region.get("id").alias("regionId"),
                        town.get("id").alias("townId"),
                        neighborhood.get("id").alias("neighborhoodId"),
                        district.get("id").alias("districtId"),
                        sectors.alias("sectorId"),
                        productTags.alias("productTag"),
                        entityEvaluation.alias("entityEvaluationId"),
                        legalForm.alias("legalFormName"),
                        socialEconomyNetwork.alias("socialEconomyNetwork"),
                        otherSocialEconomyNetwork.alias("otherSocialEconomyNetwork"),
                        keywordTags.alias("keywordTag"))
                .orderBy(criteriaBuilder.asc(root.get("name")))
                .distinct(true);

        if (specification != null) {
            final Predicate predicate = specification.toPredicate(root, criteriaQuery, criteriaBuilder);
            criteriaQuery.where(predicate);
        }

        final Query query = this.entityManager.createQuery(criteriaQuery);
        return query
                .getResultList();
    }

    @Override
    public Stream<Tuple> streamGeoJsonTuples(final Specification<SocialEconomyEntity> specification) {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();

        final Root<SocialEconomyEntity> root = criteriaQuery.from(SocialEconomyEntity.class);

        final Join<SocialEconomyEntity, Sector> mainSector = root.join("mainSector", JoinType.LEFT);
        final Join<SocialEconomyEntity, Sector> mainOfficeMainSector = root.join("mainOffice", JoinType.LEFT).join("mainSector", JoinType.LEFT);

        criteriaQuery
                .multiselect(
                        root.get("id").alias("id"),
                        root.get("name").alias("name"),
                        root.get("latitude").alias("latitude"),
                        root.get("longitude").alias("longitude"),
                        root.get("greenCommerce").alias("greenCommerce"),
                        root.get("entityStatus").alias("entityStatus"),
                        root.get("xesBalance").alias("xesBalance"),
                        mainSector.get("id").alias("mainSectorId"),
                        mainOfficeMainSector.get("id").alias("mainOfficeMainSectorId"))
                .distinct(true);

        if (specification != null) {
            final Predicate predicate = specification.toPredicate(root, criteriaQuery, criteriaBuilder);
            criteriaQuery.where(predicate);
        }

        final Query query = this.entityManager.createQuery(criteriaQuery);
        query.setHint("eclipselink.cursor.scrollable", true);
        final ScrollableCursor scrollableCursor = (ScrollableCursor) query.getSingleResult();
        @SuppressWarnings("unchecked") final Spliterator<Tuple> spliterator = Spliterators.spliteratorUnknownSize(scrollableCursor, Spliterator.NONNULL);
        return StreamSupport.stream(spliterator, false);
    }

    @Override
    public List<Tuple> getFescEntitiesTuples(final Collection<EntityStatus> entityStatuses) {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        final Root<SocialEconomyEntity> root = criteriaQuery.from(SocialEconomyEntity.class);

        final Join<SocialEconomyEntity, Sector> mainSector = root.join("mainSector");
        final Join<SocialEconomyEntity, BinaryResource> picture = root.join("picture", JoinType.LEFT);
        final Join<SocialEconomyEntity, Town> town = root.join("town", JoinType.LEFT);
        final Join<SocialEconomyEntity, SocialEconomyEntityFesc> fescData = root.join("fescData", JoinType.LEFT);
        final Join<SocialEconomyEntityFesc, FescRegistrationModality> fescRegistrationModality = fescData.join("modality", JoinType.LEFT);
        final Join<SocialEconomyEntityFesc, Sector> fescMainSector = fescData.join("mainSector", JoinType.LEFT);
        final Path<String> fescVideo = root.join("fescData", JoinType.LEFT).get("video");
        final Path<Set<Long>> sectors = root.join("sectors", JoinType.LEFT).get("id");
        final Path<Set<Long>> discounts = root.join("discounts", JoinType.LEFT).get("id");
        final Path<Set<Long>> audiovisuals = root.join("audiovisualDocuments", JoinType.LEFT).get("id");
        final Path<Set<Long>> productPictures = root.join("productPictures", JoinType.LEFT).get("id");

        criteriaQuery
                .multiselect(
                        root.get("id").alias("id"),
                        root.get("name").alias("name"),
                        root.get("description").alias("description"),
                        root.get("email").alias("email"),
                        root.get("phone").alias("phone"),
                        root.get("web").alias("web"),
                        root.get("twitter").alias("twitter"),
                        root.get("facebook").alias("facebook"),
                        root.get("instagram").alias("instagram"),
                        root.get("pinterest").alias("pinterest"),
                        root.get("quitter").alias("quitter"),
                        root.get("publicAdministration").alias("publicAdministration"),
                        root.get("enterprises").alias("enterprises"),
                        root.get("privateActivists").alias("privateActivists"),
                        root.get("privateNoobs").alias("privateNoobs"),
                        mainSector.get("id").alias("mainSectorId"),
                        town.get("id").alias("townId"),
                        fescData.get("id").alias("fescDataId"),
                        picture.get("id").alias("pictureId"),
                        sectors.alias("sectorId"),
                        discounts.alias("discountId"),
                        audiovisuals.alias("audiovisualId"),
                        productPictures.alias("productPictureId"),
                        fescVideo.alias("fescVideo"),
                        fescRegistrationModality.get("id").alias("modalityId"),
                        fescMainSector.get("id").alias("fescMainSectorId"))
                .distinct(true)
                .where(root.get("fescData").get("entityStatus").in(entityStatuses));

        final List<Tuple> tuples = this.entityManager.createQuery(criteriaQuery)
                .getResultList();

        return tuples;
    }

    @Override
    public List<Tuple> getProductPictureTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {

            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<BinaryResource> root = criteriaQuery.from(BinaryResource.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("description").alias("description"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }

        return tuples;
    }

    @Override
    public List<Tuple> getParentSectorTuples() {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        final Root<Sector> root = criteriaQuery.from(Sector.class);

        final Join<Sector, Sector> parent = root.join("parent", JoinType.LEFT);
        final Join<Sector, BinaryResource> icon = root.join("icon", JoinType.LEFT);
        final Join<Sector, BinaryResource> mapIcon = root.join("mapIcon", JoinType.LEFT);

        criteriaQuery
                .multiselect(
                        root.get("id").alias("id"),
                        root.get("name").alias("name"),
                        root.get("description").alias("description"),
                        parent.get("id").alias("parentId"),
                        icon.get("id").alias("iconId"),
                        mapIcon.get("id").alias("mapIconId"))
                .distinct(true)
                .where(parent.get("id").isNull());

        final List<Tuple> tuples = this.entityManager.createQuery(criteriaQuery)
                .getResultList();

        return tuples;
    }

    @Override
    public List<Tuple> getSectorTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<Sector> root = criteriaQuery.from(Sector.class);

            final Join<Sector, Sector> parent = root.join("parent", JoinType.LEFT);
            final Join<Sector, BinaryResource> icon = root.join("icon", JoinType.LEFT);
            final Join<Sector, BinaryResource> mapIcon = root.join("mapIcon", JoinType.LEFT);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"),
                            root.get("description").alias("description"),
                            parent.get("id").alias("parentId"),
                            icon.get("id").alias("iconId"),
                            mapIcon.get("id").alias("mapIconId"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }

        return tuples;
    }

    @Override
    public List<Tuple> getDiscountTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<EntityDiscount> root = criteriaQuery.from(EntityDiscount.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"),
                            root.get("description").alias("description"),
                            root.get("beginDate").alias("beginDate"),
                            root.get("endDate").alias("endDate"),
                            root.get("url").alias("url"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }

        return tuples;
    }

    @Override
    public List<Tuple> getAudiovisualTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<EntityAudiovisual> root = criteriaQuery.from(EntityAudiovisual.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"),
                            root.get("description").alias("description"),
                            root.get("url").alias("url"),
                            root.get("duration").alias("duration"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }
        return tuples;
    }

    @Override
    public List<Tuple> getRegionTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<Region> root = criteriaQuery.from(Region.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }
        return tuples;
    }

    @Override
    public List<Tuple> getDistrictTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<District> root = criteriaQuery.from(District.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }
        return tuples;
    }

    @Override
    public List<Tuple> getNeighborhoodTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<Neighborhood> root = criteriaQuery.from(Neighborhood.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }
        return tuples;
    }

    @Override
    public List<Tuple> getEntityEvaluationTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<EntityEvaluation> root = criteriaQuery.from(EntityEvaluation.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"))
//							root.get("entityEvaluationCriterions").alias("criterions"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }
        return tuples;
    }


    @Override
    public List<Tuple> getTownTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<Town> root = criteriaQuery.from(Town.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }

        return tuples;
    }

    @Override
    public List<Tuple> getModalityTuples(final Collection<Long> ids) {
        List<Tuple> tuples = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(tuples)) {
            final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            final Root<FescRegistrationModality> root = criteriaQuery.from(FescRegistrationModality.class);

            criteriaQuery
                    .multiselect(
                            root.get("id").alias("id"),
                            root.get("name").alias("name"))
                    .distinct(true)
                    .where(root.get("id").in(ids));

            tuples = this.entityManager.createQuery(criteriaQuery)
                    .getResultList();
        }

        return tuples;
    }

}
