package org.pamapam.repository;

import java.util.Collection;
import java.util.Set;

import org.pamapam.model.EntityScope;
import org.pamapam.model.enums.EntityScopeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EntityScopeRepository extends JpaRepository<EntityScope, Long> {

	@Query("select e from EntityScope e where e.name in :entityScopeNames")
	Set<EntityScope> findByNameIn(@Param("entityScopeNames") Collection<String> entityScopeNames);
	@Query("select e from EntityScope e where e.entityScopeType = :entityScopeType")
	Set<EntityScope> findByEntityScopeType(@Param("entityScopeType") EntityScopeType entityScopeType);

}
