package org.pamapam.repository;

import java.util.Collection;
import java.util.List;

import org.pamapam.model.SocialEconomyNetwork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SocialEconomyNetworkRepository extends JpaRepository<SocialEconomyNetwork, Long> {

	@Query("select sen from SocialEconomyNetwork sen where sen.id in :ids")
	List<SocialEconomyNetwork> findByIdIn(@Param("ids") Collection<Long> ids);
}
