package org.pamapam.repository;

import java.util.stream.Stream;

import org.pamapam.model.PamapamUser;
import org.springframework.data.jpa.domain.Specification;

public interface PamapamUserCustomRepository {

	Stream<PamapamUser> streamAll();

	Stream<PamapamUser> streamAll(Specification<PamapamUser> specification);

}
