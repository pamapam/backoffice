package org.pamapam.repository;

import org.pamapam.model.Questionaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionaireRepository extends JpaRepository<Questionaire, Long> {

}
