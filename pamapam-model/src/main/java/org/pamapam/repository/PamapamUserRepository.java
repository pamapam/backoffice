package org.pamapam.repository;

import java.util.List;
import java.util.Set;

import org.jamgo.model.entity.RoleImpl;
import org.jamgo.model.repository.UserRepository;
import org.pamapam.model.PamapamUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PamapamUserRepository extends UserRepository<PamapamUser>, JpaSpecificationExecutor<PamapamUser>, PamapamUserCustomRepository {

	@Query("select u from PamapamUser u inner join u.roles r where r = :role")
	List<PamapamUser> findByRole(@Param("role") RoleImpl role);

	@Query("select u from PamapamUser u inner join u.roles r where r.rolename = :rolename")
	List<PamapamUser> findByRolename(@Param("rolename") String rolename);
	
	@Query("select u from PamapamUser u inner join u.roles r where r.rolename in :rolenames")
	List<PamapamUser> findByRolenames(@Param("rolenames") Set<String> rolenames);

	@Query("select u from PamapamUser u inner join u.roles r where r in :roles")
	List<PamapamUser> findByRoles(@Param("roles") Set<RoleImpl> role);

	@Query("select u from SocialEconomyEntity s " +
		"join s.registryUser u " +
		"join u.roles r on r.rolename in ('ROLE_admin', 'ROLE_xinxeta', 'ROLE_superxinxeta', 'ROLE_comunicacio') " +
		"left join s.entityStatusChanges c on c.entityStatusTo.id = 1003 " +
		"where s.entityStatus.id = 1003 and u.privacyConsentDate is not null " +
		"group by u.id " +
		"order by max(c.changeTimestamp) desc, u.username")
	Page<PamapamUser> findSortedByActivity(Pageable pageable);

}
