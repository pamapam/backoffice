package org.pamapam.repository;

import java.util.List;

import org.pamapam.model.FescRegistrationFee;
import org.pamapam.model.FescRegistrationModality;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FescRegistrationFeeRepository extends JpaRepository<FescRegistrationFee, Long> {

	List<FescRegistrationFee> findByModalities(FescRegistrationModality modality);

}
