package org.pamapam.repository;

import java.util.List;

import org.pamapam.model.NotificationConfig;
import org.pamapam.model.enums.NotificationType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationConfigRepository extends JpaRepository<NotificationConfig, Long> {

	List<NotificationConfig> findByNotificationType(NotificationType notificationType);
}
