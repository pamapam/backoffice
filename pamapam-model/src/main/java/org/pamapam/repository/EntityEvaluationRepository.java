package org.pamapam.repository;

import org.pamapam.model.EntityEvaluation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityEvaluationRepository extends JpaRepository<EntityEvaluation, Long> {

}
