package org.pamapam.repository;

import java.util.List;

import org.pamapam.model.UserActionToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserActionTokenRepository extends JpaRepository<UserActionToken, Long>, JpaSpecificationExecutor<UserActionToken> {

	List<UserActionToken> findByUsername(String username);

	List<UserActionToken> findByUserIdAndTokenAndAction(Long userId, String token, String action);

}
