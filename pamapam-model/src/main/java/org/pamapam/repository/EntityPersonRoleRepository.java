package org.pamapam.repository;

import org.pamapam.model.EntityPersonRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityPersonRoleRepository extends JpaRepository<EntityPersonRole, Long> {

}
