package org.pamapam.repository;

import org.pamapam.model.TextTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TextTagRepository extends JpaRepository<TextTag, Long> {

}
