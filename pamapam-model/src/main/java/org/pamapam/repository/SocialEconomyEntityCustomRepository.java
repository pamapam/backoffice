package org.pamapam.repository;

import org.pamapam.model.EntityStatus;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.Tuple;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public interface SocialEconomyEntityCustomRepository {

	Stream<SocialEconomyEntity> streamAll();

	Stream<SocialEconomyEntity> streamAll(Specification<SocialEconomyEntity> specification);

	Stream<Tuple> streamGeoJsonTuples(final Specification<SocialEconomyEntity> specification);

	List<Tuple> getFescEntitiesTuples(final Collection<EntityStatus> entityStatuses);

	List<Tuple> getProductPictureTuples(final Collection<Long> ids);

	List<Tuple> getParentSectorTuples();

	List<Tuple> getSectorTuples(final Collection<Long> ids);

	List<Tuple> getDiscountTuples(final Collection<Long> ids);

	List<Tuple> getAudiovisualTuples(final Collection<Long> ids);

	List<Tuple> getRegionTuples(final Collection<Long> ids);
	List<Tuple> getEntityEvaluationTuples(final Collection<Long> ids);

	List<Tuple> getTownTuples(final Collection<Long> ids);

	List<Tuple> getDistrictTuples(final Collection<Long> ids);
	List<Tuple> getNeighborhoodTuples(final Collection<Long> ids);

	List<Tuple> findTuples(Specification<SocialEconomyEntity> specification, PageRequest pageRequest);

	List<Tuple> findDetailTuples(Specification<SocialEconomyEntity> specification);

	List<Tuple> getModalityTuples(final Collection<Long> ids);
}
