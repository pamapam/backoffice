package org.pamapam.repository.search;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.queryparser.classic.ParseException;
import org.jamgo.model.search.SearchSpecification;
import org.pamapam.model.EntityScope;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.Sector;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.model.enums.EntityScopeType;
import org.pamapam.repository.EntityScopeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SocialEconomyEntitySpecification extends SearchSpecification<SocialEconomyEntity, SocialEconomyEntitySearch> {

	@Autowired
	private FullTextService fullTextService;

	@Autowired
	private EntityScopeRepository entityScopeRepository;

	public SocialEconomyEntitySpecification() {
		super();
		this.searchObject = new SocialEconomyEntitySearch();
	}

	@Override
	public Predicate toPredicate(final Root<SocialEconomyEntity> root, final CriteriaQuery<?> cq, final CriteriaBuilder cb) {
		final List<Predicate> predicates = new ArrayList<>();

		if (StringUtils.isNotBlank(this.searchObject.getText())) {
			Set<Long> entityIds = new HashSet<>();
			try {
				entityIds = this.fullTextService.findEntities(this.searchObject.getText());
			} catch (final IOException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (!entityIds.isEmpty()) {
				predicates.add(root.get("id").in(entityIds));
			} else {
				// ...	FIXME
				predicates.add(cb.equal(root.get("id"), 0));
			}
		}
		if (this.searchObject.getId() != null) {
			predicates.add(cb.equal(root.get("id"), this.searchObject.getId()));
		}
		if (!CollectionUtils.isEmpty(this.searchObject.getEntityScopes())) {
			if (this.searchObject.includeMainOffices()) {
				predicates.add(root.join("entityScopes", JoinType.LEFT).in(this.searchObject.getEntityScopes()));
			} else {
				predicates.add(cb.or(
					root.join("entityScopes", JoinType.LEFT).in(this.searchObject.getEntityScopes()),
					root.join("mainOffice", JoinType.LEFT).join("entityScopes", JoinType.LEFT).in(this.searchObject.getEntityScopes())));
			}
		}
		if (StringUtils.isNotBlank(this.searchObject.getName())) {
			predicates.add(cb.like(cb.lower(root.get("name")), "%" + this.searchObject.getName().toLowerCase() + "%"));
		}
		if (StringUtils.isNotBlank(this.searchObject.getNormalizedName())) {
			predicates.add(cb.equal(root.get("normalizedName"), this.searchObject.getNormalizedName()));
		}
		if (this.searchObject.getDateFrom() != null) {
			predicates.add(cb.greaterThanOrEqualTo(root.get("registryDate"), this.searchObject.getDateFrom()));
		}
		if (this.searchObject.getDateTo() != null) {
			predicates.add(cb.lessThanOrEqualTo(root.get("registryDate"), this.searchObject.getDateTo()));
		}
		if (!CollectionUtils.isEmpty(this.searchObject.getEntityStatuses())) {
			// FIXME: compare by id instead of by the whole object to avoid java heap size generating an out of memory exception
//			predicates.add(root.get("entityStatus").in(this.searchObject.getEntityStatuses()));
			predicates.add(root.get("entityStatus").get("id").in(this.searchObject.getEntityStatuses().stream().map(EntityStatus::getId).collect(Collectors.toList())));
		}
		if (!CollectionUtils.isEmpty(this.searchObject.getSectors())) {
			final Set<Sector> sectors = new HashSet<>();
			sectors.addAll(this.searchObject.getSectors());
			if (CollectionUtils.isEmpty(this.searchObject.getSubSectors())) {
				for (final Sector eachSector : this.searchObject.getSectors()) {
					sectors.addAll(eachSector.getSectors());
				}
			} else {
				sectors.addAll(this.searchObject.getSubSectors());
			}

			predicates.add(cb.or(
				cb.or(
					root.join("sectors", JoinType.LEFT).in(sectors),
					root.join("mainSector", JoinType.LEFT).in(sectors)),
				cb.or(
					root.join("mainOffice", JoinType.LEFT).join("sectors", JoinType.LEFT).in(sectors),
					root.join("mainOffice", JoinType.LEFT).join("mainSector", JoinType.LEFT).in(sectors))));
		}
		if (this.searchObject.getLegalForm() != null) {
			predicates.add(cb.equal(root.get("legalForm"), this.searchObject.getLegalForm()));
		}

		if (this.searchObject.getProvince() != null) {
			predicates.add(cb.equal(root.get("province"), this.searchObject.getProvince()));
		}
		if (this.searchObject.getRegion() != null) {
			predicates.add(cb.equal(root.get("region"), this.searchObject.getRegion()));
		}
		if (this.searchObject.getTown() != null) {
			predicates.add(cb.equal(root.get("town"), this.searchObject.getTown()));
		}
		if (this.searchObject.getDistrict() != null) {
			predicates.add(cb.equal(root.get("district"), this.searchObject.getDistrict()));
		}
		if (this.searchObject.getNeighborhood() != null) {
			predicates.add(cb.equal(root.get("neighborhood"), this.searchObject.getNeighborhood()));
		}

		if (this.searchObject.includeMainOffices() ^ this.searchObject.includeOffices()) {
			if (this.searchObject.includeMainOffices()) {
				predicates.add(root.get("mainOffice").isNull());
			} else {
				predicates.add(root.get("mainOffice").isNotNull());
			}
		}

		if (this.searchObject.getMainOffice() != null) {
			predicates.add(cb.equal(root.get("mainOffice"), this.searchObject.getMainOffice()));
		}

		if (this.searchObject.getRegistryUser() != null) {
			predicates.add(cb.equal(root.get("registryUser"), this.searchObject.getRegistryUser()));
		}
		if (this.searchObject.getInitiativeUser() != null) {
			predicates.add(cb.equal(root.get("initiativeUser"), this.searchObject.getInitiativeUser()));
		}
		if (!CollectionUtils.isEmpty(this.searchObject.getExternalFilterTags())) {
			predicates.add(
				root.join("externalFilterTags", JoinType.LEFT).in(this.searchObject.getExternalFilterTags()));
				
		}

		if (!CollectionUtils.isEmpty(this.searchObject.getQuestionaires())) {
			predicates.add(root.join("questionaire", JoinType.LEFT).in(this.searchObject.getQuestionaires()));
		}

		if (this.searchObject.isFescRegistered()) {
			predicates.add(root.get("fescData").get("entityStatus").isNotNull());
		} else {
			// FIXME this is returning empty result
//			predicates.add(cb.isNull(root.get("fescData")));
		}
		if (!CollectionUtils.isEmpty(this.searchObject.getEntityFescStatuses())) {
			predicates.add(root.get("fescData").get("entityStatus").get("id").in(this.searchObject.getEntityFescStatuses().stream().map(EntityStatus::getId).collect(Collectors.toList())));
		}
		if (StringUtils.isNotBlank(this.searchObject.getFescMail())) {
			predicates.add(cb.like(root.get("fescData").get("email"), "%" + this.searchObject.getFescMail() + "%"));
		}

		if (this.searchObject.isPublicAdministration()) {
			predicates.add(cb.equal(root.get("publicAdministration"), this.searchObject.isPublicAdministration()));
		}

		if (this.searchObject.isPrivateActivists()) {
			predicates.add(cb.equal(root.get("privateActivists"), this.searchObject.isPrivateActivists()));
		}

		if (this.searchObject.isPrivateNoobs()) {
			predicates.add(cb.equal(root.get("privateNoobs"), this.searchObject.isPrivateNoobs()));
		}

		if (this.searchObject.isEnterprises()) {
			predicates.add(cb.equal(root.get("enterprises"), this.searchObject.isEnterprises()));
		}

		if (this.searchObject.getStatusScopes() != null && !this.searchObject.getStatusScopes().isEmpty()) {
			final Map<EntityScopeType, List<EntityStatus>> statusScopes = this.searchObject.getStatusScopes();
			final List<Predicate> predicatesStatusScopes = new ArrayList<>();

			for (final Map.Entry<EntityScopeType, List<EntityStatus>> each : statusScopes.entrySet()) {
				final List<EntityStatus> statuses = each.getValue();
				final EntityScopeType scopeType = each.getKey();

				final EntityScope scope = this.entityScopeRepository.findByEntityScopeType(scopeType)
					.stream()
					.findFirst()
					.orElse(null);

				final Predicate eachPredicate = cb.and(
					root.join("entityScopes", JoinType.LEFT).in(scope.getId()),
					root.get("entityStatus")
						.get("id")
						.in(statuses.stream()
							.map(EntityStatus::getId)
							.collect(Collectors.toList()))
				);

				predicatesStatusScopes.add(eachPredicate);
			}

			predicates.add(cb.or(predicatesStatusScopes.toArray(new Predicate[predicatesStatusScopes.size()])));
		}

		cq.distinct(true);

		return this.andTogether(root, predicates, cb);
	}
	public SocialEconomyEntitySearch getSearchObject() {
		return this.searchObject;
	}
}
