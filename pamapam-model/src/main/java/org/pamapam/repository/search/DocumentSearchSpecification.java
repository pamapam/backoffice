package org.pamapam.repository.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jamgo.model.search.SearchSpecification;
import org.pamapam.model.Document;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Primary
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DocumentSearchSpecification  extends SearchSpecification<Document, Document> {

//	private Document searchObject;
	
	public DocumentSearchSpecification() {
		super();
		this.searchObject = new Document();
	}

	@Override
	public Predicate toPredicate(Root<Document> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();

//		if (this.searchObject.getMimeType() != null) {
//			predicates.add(cb.equal(root.get("mimeType"), this.searchObject.getMimeType()));
//		}
//		
//		if (this.searchObject.getFileName() != null) {
//			predicates.add(cb.equal(root.get("fileName"), this.searchObject.getFileName()));
//		} else {
//			predicates.add(root.get("fileName").isNotNull());
//		}
		
		return cb.and(predicates.toArray(new Predicate[0]));
	}

}
