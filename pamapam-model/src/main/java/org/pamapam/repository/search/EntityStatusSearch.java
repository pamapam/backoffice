package org.pamapam.repository.search;

import java.util.Set;

import org.pamapam.model.enums.EntityStatusType;

public class EntityStatusSearch {

	private Set<EntityStatusType> types;

	public Set<EntityStatusType> getTypes() {
		return this.types;
	}

	public void setTypes(final Set<EntityStatusType> types) {
		this.types = types;
	}

}
