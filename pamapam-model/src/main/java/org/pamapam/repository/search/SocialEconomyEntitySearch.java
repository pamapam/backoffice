package org.pamapam.repository.search;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.jamgo.model.entity.*;
import org.pamapam.model.*;
import org.pamapam.model.enums.EntityScopeType;

import java.util.*;

public class SocialEconomyEntitySearch {

	private Long id;
	private String name;
	private String normalizedName;
	private Set<EntityStatus> entityStatuses;
	private Set<Long> entityStatusIds;
	private Set<Sector> sectors;
	private Set<Sector> subSectors;
	private Date dateFrom;
	private Date dateTo;
	private LegalForm legalForm;
	private Province province;
	private Region region;
	private Town town;
	private District district;
	private Neighborhood neighborhood;
	private PamapamUser registryUser;
	private String text;
	private Set<ExternalFilterTag> externalFilterTags;
	private PamapamUser initiativeUser;
	private boolean enterprises;
	private boolean privateNoobs;
	private boolean privateActivists;
	private boolean publicAdministration;
	private boolean includeMainOffices;
	private boolean includeOffices;
	private SocialEconomyEntity mainOffice;
	private Set<EntityScope> entityScopes;
	private boolean greenCommerce;

	/** Conditions for searching by Scopetype and EntityStatus.
	 *
	 * For example, I want all the entities of the social economy that have FESC scope and are
	 * published or are external and all those of the pam a pam scope that are published.
	 *
	 */
	private Map<EntityScopeType, List<EntityStatus>> statusScopes;

	// fesc data filters
	private boolean fescRegistered;
	private Set<EntityStatus> entityFescStatuses;
	private String fescMail;

	private Set<Questionaire> questionaires;
	private String nif;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getNormalizedName() {
		return this.normalizedName;
	}

	public void setNormalizedName(final String normalizedName) {
		this.normalizedName = normalizedName;
	}

	public Set<EntityStatus> getEntityStatuses() {
		return this.entityStatuses;
	}

	public void setEntityStatuses(final Set<EntityStatus> entityStatuses) {
		this.entityStatuses = entityStatuses;
	}

	public Set<Sector> getSectors() {
		return this.sectors;
	}

	public Set<Sector> getSectorsAndSubsectors() {
		final Set<Sector> sectorsAndSubsectors = new HashSet<>();
		if (!CollectionUtils.isEmpty(this.getSectors())) {
			sectorsAndSubsectors.addAll(this.getSectors());
			if (CollectionUtils.isEmpty(this.getSubSectors())) {
				for (final Sector eachSector : this.getSectors()) {
					sectorsAndSubsectors.addAll(eachSector.getSectors());
				}
			} else {
				sectorsAndSubsectors.addAll(this.getSubSectors());
			}
		}
		return sectorsAndSubsectors;
	}
	public void setSectors(final Set<Sector> sectors) {
		this.sectors = sectors;
	}

	public Sector getSector() {
		if (CollectionUtils.isNotEmpty(this.sectors)) {
			return this.sectors.stream().findFirst().get();
		} else {
			return null;
		}
	}

	public void setSector(final Sector sector) {
		if (sector == null) {
			this.sectors = Sets.newHashSet();
		} else {
			this.sectors = Sets.newHashSet(sector);
		}
	}

	public Set<Sector> getSubSectors() {
		return this.subSectors;
	}

	public void setSubSectors(final Set<Sector> subSectors) {
		this.subSectors = subSectors;
	}

	public Date getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(final Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(final Date dateTo) {
		this.dateTo = dateTo;
	}

	public LegalForm getLegalForm() {
		return this.legalForm;
	}

	public void setLegalForm(final LegalForm legalForm) {
		this.legalForm = legalForm;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(final Province province) {
		this.province = province;
	}

	public Region getRegion() {
		return this.region;
	}

	public void setRegion(final Region region) {
		this.region = region;
	}

	public Town getTown() {
		return this.town;
	}

	public void setTown(final Town town) {
		this.town = town;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(final District district) {
		this.district = district;
	}

	public Neighborhood getNeighborhood() {
		return this.neighborhood;
	}

	public void setNeighborhood(final Neighborhood neighborhood) {
		this.neighborhood = neighborhood;
	}

	public void setTerritory(final Territory territory) {
		switch (territory.getTerritoryType()) {
			case PROVINCE:
				this.setProvince((Province) territory);
				break;
			case REGION:
				this.setRegion((Region) territory);
				break;
			case TOWN:
				this.setTown((Town) territory);
				break;
			case DISTRICT:
				this.setDistrict((District) territory);
				break;
			case NEIGHBORHOOD:
				this.setNeighborhood((Neighborhood) territory);
				break;
			default:
				break;
		}
	}

	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public Set<ExternalFilterTag> getExternalFilterTags() {
		return this.externalFilterTags;
	}

	public void setExternalFilterTags(final Set<ExternalFilterTag> externalFilterTags) {
		this.externalFilterTags = externalFilterTags;
	}

	public PamapamUser getInitiativeUser() {
		return this.initiativeUser;
	}

	public void setInitiativeUser(final PamapamUser initiativeUser) {
		this.initiativeUser = initiativeUser;
	}

	public PamapamUser getRegistryUser() {
		return this.registryUser;
	}

	public void setRegistryUser(final PamapamUser registryUser) {
		this.registryUser = registryUser;
	}

	public Set<Long> getEntityStatusIds() {
		return this.entityStatusIds;
	}

	public void setEntityStatusIds(final Set<Long> entityStatusIds) {
		this.entityStatusIds = entityStatusIds;
	}

	public boolean isFescRegistered() {
		return this.fescRegistered;
	}

	public void setFescRegistered(final boolean fescRegistered) {
		this.fescRegistered = fescRegistered;
	}

	public Set<EntityStatus> getEntityFescStatuses() {
		return this.entityFescStatuses;
	}

	public void setEntityFescStatuses(final Set<EntityStatus> entityFescStatuses) {
		this.entityFescStatuses = entityFescStatuses;
	}

	public String getFescMail() {
		return this.fescMail;
	}

	public void setFescMail(final String fescMail) {
		this.fescMail = fescMail;
	}

	public boolean isPublicAdministration() {
		return this.publicAdministration;
	}

	public void setPublicAdministration(final boolean publicAdministration) {
		this.publicAdministration = publicAdministration;
	}

	public boolean isEnterprises() {
		return this.enterprises;
	}

	public void setEnterprises(final boolean enterprises) {
		this.enterprises = enterprises;
	}

	public boolean isPrivateNoobs() {
		return this.privateNoobs;
	}

	public void setPrivateNoobs(final boolean privateNoobs) {
		this.privateNoobs = privateNoobs;
	}

	public boolean isPrivateActivists() {
		return this.privateActivists;
	}

	public void setPrivateActivists(final boolean privateActivists) {
		this.privateActivists = privateActivists;
	}

	public boolean includeMainOffices() {
		return this.includeMainOffices;
	}

	public void setIncludeMainOffices(final boolean includeMainOffices) {
		this.includeMainOffices = includeMainOffices;
	}

	public boolean includeOffices() {
		return this.includeOffices;
	}

	public void setIncludeOffices(final boolean includeOffices) {
		this.includeOffices = includeOffices;
	}

	public SocialEconomyEntity getMainOffice() {
		return this.mainOffice;
	}

	public void setMainOffice(final SocialEconomyEntity mainOffice) {
		this.mainOffice = mainOffice;
	}

	public Set<EntityScope> getEntityScopes() {
		return this.entityScopes;
	}

	public void setEntityScopes(final Set<EntityScope> entityScopes) {
		this.entityScopes = entityScopes;
	}

	public Set<Questionaire> getQuestionaires() {
		return this.questionaires;
	}

	public void setQuestionaires(final Set<Questionaire> questionaires) {
		this.questionaires = questionaires;
	}

	public String getNif() {
		return this.nif;
	}

	public void setNif(final String nif) {
		this.nif = nif;
	}

	public Map<EntityScopeType, List<EntityStatus>> getStatusScopes() {
		return this.statusScopes;
	}

	public void setStatusScopes(final Map<EntityScopeType, List<EntityStatus>> statusScopes) {
		this.statusScopes = statusScopes;
	}

	public boolean isGreenCommerce() {
		return greenCommerce;
	}

	public void setGreenCommerce(boolean greenCommerce) {
		this.greenCommerce = greenCommerce;
	}
	
}
