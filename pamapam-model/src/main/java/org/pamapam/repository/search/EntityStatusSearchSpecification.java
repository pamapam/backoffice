package org.pamapam.repository.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jamgo.model.search.SearchSpecification;
import org.pamapam.model.EntityStatus;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Primary
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EntityStatusSearchSpecification extends SearchSpecification<EntityStatus, EntityStatusSearch> {

	public EntityStatusSearchSpecification(final EntityStatusSearch searchObject) {
		super();
		this.searchObject = searchObject;
	}

	@Override
	public Predicate toPredicate(final Root<EntityStatus> root, final CriteriaQuery<?> cq, final CriteriaBuilder cb) {
		final List<Predicate> predicates = new ArrayList<>();

		if (this.searchObject.getTypes() != null) {
			predicates.add(root.get("entityStatusType").in(this.searchObject.getTypes()));
		}

		return this.andTogether(root, predicates, cb);
	}

}
