package org.pamapam.repository.search;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.ca.CatalanAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LatLonPoint;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.pamapam.model.KeywordTag;
import org.pamapam.model.ProductTag;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;

@Service
public class FullTextService {

	@Autowired
	private SocialEconomyEntityRepository socialEconomyEntityRepository;
	
	@Value("${pamapam.index:true}")
	private boolean index;

	private Analyzer analyzer;
	private Directory directory;
	private IndexWriterConfig config;
	private IndexWriter writer;

	@PostConstruct
	private void init() throws IOException {
		this.analyzer = new CatalanAnalyzer();
		this.directory = new RAMDirectory();
		this.config = new IndexWriterConfig(this.analyzer);
		this.writer = new IndexWriter(this.directory, this.config);
		if (this.index) {
			for (SocialEconomyEntity eachEntity : this.socialEconomyEntityRepository.findAll()) {
				this.indexEntity(eachEntity);
			}
		}
	}

	public void indexEntity(SocialEconomyEntity entity) throws IOException {
		Document document = new Document();
		document.add(new StoredField("id", entity.getId()));
		document.add(new TextField("name", entity.getName(), Store.YES));
		if ((entity.getDescription() != null) && (entity.getDescription().getDefaultText() != null)) {
			document.add(new TextField("description", entity.getDescription().getDefaultText(), Store.YES));
		}
		if (entity.getAddress() != null) {
			document.add(new TextField("address", entity.getAddress(), Store.YES));
		}
		if (entity.getProvince() != null) {
			document.add(new TextField("province", entity.getProvince().getName().getDefaultText(), Store.YES));
		}
		if (entity.getRegion() != null) {
			document.add(new TextField("region", entity.getRegion().getName().getDefaultText(), Store.YES));
		}
		if (entity.getTown() != null) {
			document.add(new TextField("town", entity.getTown().getName().getDefaultText(), Store.YES));
		}
		if (entity.getDistrict() != null) {
			document.add(new TextField("district", entity.getDistrict().getName().getDefaultText(), Store.YES));
		}
		if (entity.getNeighborhood() != null) {
			document.add(new TextField("neighborhood", entity.getNeighborhood().getName().getDefaultText(), Store.YES));
		}
		if ((entity.getLatitude() != null) && (entity.getLongitude() != null)) {
			document.add(new LatLonPoint("geolocation", entity.getLatitude(), entity.getLongitude()));
		}
		Optional.ofNullable(entity.getProductTag()).ifPresent(tags -> 
			document.add(new TextField("productTags", tags.stream().map(ProductTag::getTag).collect(Collectors.joining(" ")), Store.YES))
		);
		Optional.ofNullable(entity.getKeywordTags()).ifPresent(tags -> 
			document.add(new TextField("keywordTags", tags.stream().map(KeywordTag::getTag).collect(Collectors.joining(" ")), Store.YES))
		);
		this.writer.addDocument(document);
		this.writer.commit();
	}

	public Set<Long> findEntities(String searchText) throws IOException, ParseException {
		IndexReader reader = DirectoryReader.open(this.directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		String[] fields = { "name", "description", "address", "province", "region", "town", "district", "neighborhood", "productTags", "keywordTags" };
//		BooleanClause.Occur[] flags = {
//			BooleanClause.Occur.SHOULD,
//			BooleanClause.Occur.SHOULD,
//			BooleanClause.Occur.SHOULD,
//			BooleanClause.Occur.SHOULD,
//			BooleanClause.Occur.SHOULD,
//			BooleanClause.Occur.SHOULD,
//			BooleanClause.Occur.SHOULD,
//			BooleanClause.Occur.SHOULD };
		
		MultiFieldQueryParser queryParser = new MultiFieldQueryParser(fields, this.analyzer);
		queryParser.setDefaultOperator(Operator.AND);
		Query query = queryParser.parse(searchText);
		
		TopDocs topDocs = searcher.search(query, 900);
		Set<Long> entityIds = new HashSet<>();
		for (ScoreDoc eachScoreDoc : topDocs.scoreDocs) {
			Document eachDoc = searcher.doc(eachScoreDoc.doc, Sets.newHashSet("id"));
			entityIds.add(Long.valueOf(eachDoc.get("id")));
		}
		return entityIds;
	}

	public Set<Long> findByProximity(Double latitude, Double longitude, int count) throws IOException {
		IndexReader reader = DirectoryReader.open(this.directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		Query query = LatLonPoint.newDistanceQuery("geolocation", latitude, longitude, 1000);
		TopDocs topDocs = searcher.search(query, count);
		Set<Long> entityIds = new HashSet<>();
		for (ScoreDoc eachScoreDoc : topDocs.scoreDocs) {
			Document eachDoc = searcher.doc(eachScoreDoc.doc, Sets.newHashSet("id"));
			entityIds.add(Long.valueOf(eachDoc.get("id")));
		}
		return entityIds;
	}

}