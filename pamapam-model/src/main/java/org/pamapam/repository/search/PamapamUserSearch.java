package org.pamapam.repository.search;

import java.util.Set;

import org.jamgo.model.entity.District;
import org.jamgo.model.entity.Neighborhood;
import org.jamgo.model.entity.Province;
import org.jamgo.model.entity.Region;
import org.jamgo.model.entity.RoleImpl;
import org.jamgo.model.entity.Territory;
import org.jamgo.model.entity.Town;
import org.pamapam.model.Community;

public class PamapamUserSearch {

	private String username;
	private String name;
	private String email;
	private Set<RoleImpl> roles;
	private Boolean enabled = true;
	private Province province;
	private Region region;
	private Town town;
	private District district;
	private Neighborhood neighborhood;
	private Set<Community> communities;
	
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<RoleImpl> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<RoleImpl> roles) {
		this.roles = roles;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Town getTown() {
		return this.town;
	}

	public void setTown(Town town) {
		this.town = town;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Neighborhood getNeighborhood() {
		return this.neighborhood;
	}

	public void setNeighborhood(Neighborhood neighborhood) {
		this.neighborhood = neighborhood;
	}

	public void setTerritory(Territory territory) {
		switch (territory.getTerritoryType()) {
			case PROVINCE:
				this.setProvince((Province) territory);
				break;
			case REGION:
				this.setRegion((Region) territory);
				break;
			case TOWN:
				this.setTown((Town) territory);
				break;
			case DISTRICT:
				this.setDistrict((District) territory);
				break;
			case NEIGHBORHOOD:
				this.setNeighborhood((Neighborhood) territory);
				break;
			default:
				break;
		}
	}

	public Set<Community> getCommunities() {
		return this.communities;
	}

	public void setCommunities(Set<Community> communities) {
		this.communities = communities;
	}

}
