package org.pamapam.repository.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.pamapam.model.Sector;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SectorSpecification implements Specification<Sector> {

	private List<Long> ids;

	public SectorSpecification(List<Long> ids) {
		super();
		this.ids = ids;
	}

	public List<Long> getIds() {
		return this.ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	@Override
	public Predicate toPredicate(Root<Sector> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(this.ids)) {
			predicates.add(root.get("id").in(this.ids));
		}

		return cb.and(predicates.toArray(new Predicate[0]));
	}

}
