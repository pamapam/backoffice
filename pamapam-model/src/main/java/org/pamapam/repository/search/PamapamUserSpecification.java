package org.pamapam.repository.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jamgo.model.search.SearchSpecification;
import org.pamapam.model.PamapamUser;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PamapamUserSpecification extends SearchSpecification<PamapamUser, PamapamUserSearch> {

	public PamapamUserSpecification() {
		super();
		this.searchObject = new PamapamUserSearch();
	}

	@Override
	public Predicate toPredicate(Root<PamapamUser> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();

		if (StringUtils.isNotBlank(this.searchObject.getUsername())) {
			predicates.add(cb.like(cb.lower(root.get("username")), "%" + this.searchObject.getUsername().toLowerCase() + "%"));
		}
		if (StringUtils.isNotBlank(this.searchObject.getName())) {
			predicates.add(cb.like(cb.lower(root.get("name")), "%" + this.searchObject.getName().toLowerCase() + "%"));
		}
		if (StringUtils.isNotBlank(this.searchObject.getEmail())) {
			predicates.add(cb.like(cb.lower(root.get("email")), "%" + this.searchObject.getEmail().toLowerCase() + "%"));
		}
		if (!CollectionUtils.isEmpty(this.searchObject.getRoles())) {
			predicates.add(root.join("roles").in(this.searchObject.getRoles()));
		}
		if (this.searchObject.getEnabled() != null) {
			if (this.searchObject.getEnabled()) {
				predicates.add(cb.isTrue(root.get("enabled")));
			} else {
				predicates.add(cb.isFalse(root.get("enabled")));
			}
		}
		if (this.searchObject.getProvince() != null) {
			predicates.add(cb.equal(root.get("province"), this.searchObject.getProvince()));
		}
		if (this.searchObject.getRegion() != null) {
			predicates.add(cb.equal(root.get("region"), this.searchObject.getRegion()));
		}
		if (this.searchObject.getTown() != null) {
			predicates.add(cb.equal(root.get("town"), this.searchObject.getTown()));
		}
		if (this.searchObject.getDistrict() != null) {
			predicates.add(cb.equal(root.get("district"), this.searchObject.getDistrict()));
		}
		if (this.searchObject.getNeighborhood() != null) {
			predicates.add(cb.equal(root.get("neighborhood"), this.searchObject.getNeighborhood()));
		}

		if (!CollectionUtils.isEmpty(this.searchObject.getCommunities())) {
			predicates.add(root.join("communities").in(this.searchObject.getCommunities()));
		}

		return this.andTogether(root, predicates, cb);
	}

}
