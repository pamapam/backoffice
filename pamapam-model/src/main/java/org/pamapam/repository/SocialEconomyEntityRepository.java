package org.pamapam.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.pamapam.model.EntityScope;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SocialEconomyEntityRepository extends JpaRepository<SocialEconomyEntity, Long>, JpaSpecificationExecutor<SocialEconomyEntity>, SocialEconomyEntityCustomRepository {

	List<SocialEconomyEntity> findByIdNotAndGlobalId(Long id, Long globalId);

	@Query("select e from SocialEconomyEntity e where e.id in :ids")
	List<SocialEconomyEntity> findByIdIn(@Param("ids") Collection<Long> ids);

	@Query("select e from SocialEconomyEntity e " +
//		"inner join e.entityScopes es on es in :entityScopes " +
		"left join e.entityStatusChanges s on s.entityStatusTo in :entityStatuses " +
		"where e.entityStatus in :entityStatuses and e.entityScopes in :entityScopes " +
		"group by s.entity " +
		"order by max(s.changeTimestamp) desc")
	Page<SocialEconomyEntity> findTopByStatus(@Param("entityStatuses") List<EntityStatus> entityStatuses, @Param("entityScopes") Set<EntityScope> entityScopes, Pageable pageable);

	// ...	TODO: Ver que esta versión devuelve lo mismo y es más eficiente.
//	@Query("select s.entity from EntityStatusChange s " +
//		"where s.entity.entityStatus in :entityStatuses and s.entityStatusTo in :entityStatuses " +
//		"group by s.entity, s.entityStatusTo " +
//		"order by max(s.changeTimestamp) desc")
//	Page<SocialEconomyEntity> findTopByStatus(@Param("entityStatuses") List<EntityStatus> entityStatuses, Pageable pageable);

	@Query("select e from SocialEconomyEntity e " +
//		"inner join e.entityScopes es on es in :entityScopes " +
		"left join e.entityStatusChanges s on s.entityStatusTo in :entityStatuses " +
		"where e.entityStatus in :entityStatuses and e.registryUser = :user and e.entityScopes in :entityScopes  " +
		"group by s.entity " +
		"order by max(s.changeTimestamp) desc")
	Page<SocialEconomyEntity> findTopByStatusAndUser(@Param("entityStatuses") List<EntityStatus> entityStatuses, @Param("user") PamapamUser user, @Param("entityScopes") Set<EntityScope> entityScopes, Pageable pageable);

	@Query("select e from SocialEconomyEntity e " +
//		"inner join e.entityScopes es on es in :entityScopes " +
		"left join e.entityStatusChanges s on s.entityStatusTo in :entityStatuses " +
		"where e.entityStatus in :entityStatuses and e.initiativeUser = :user and e.entityScopes in :entityScopes  " +
		"group by s.entity, s.entityStatusTo " +
		"order by max(s.changeTimestamp) desc")
	Page<SocialEconomyEntity> findTopByStatusAndInitiativeUser(@Param("entityStatuses") List<EntityStatus> entityStatuses, @Param("user") PamapamUser user, @Param("entityScopes") Set<EntityScope> entityScopes, Pageable pageable);

	@Query(value = "select max(discount) from (select count(entity_id) as discount from entity_discount ed group by ed.entity_id) as dc", nativeQuery = true)
	int countMaxDiscounts();

	@Query("select e from SocialEconomyEntity e " +
		"join SocialEconomyEntityFesc fesc on fesc.id = e.id " +
		"where fesc.entityStatus in :entityFescStatuses")
	List<SocialEconomyEntity> getFescEntities(@Param("entityFescStatuses") Collection<EntityStatus> entityStatuses);

	List<SocialEconomyEntity> findByMainOffice(SocialEconomyEntity mainOffice);

	long countByMainOffice(SocialEconomyEntity mainOffice);

}
