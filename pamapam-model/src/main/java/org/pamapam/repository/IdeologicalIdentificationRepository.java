package org.pamapam.repository;

import org.pamapam.model.IdeologicalIdentification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdeologicalIdentificationRepository extends JpaRepository<IdeologicalIdentification, Long> {

}
