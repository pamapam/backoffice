package org.pamapam.repository;

import org.pamapam.model.ModelAttributeDef;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelAttributeDefRepository extends JpaRepository<ModelAttributeDef, Long> {

}
