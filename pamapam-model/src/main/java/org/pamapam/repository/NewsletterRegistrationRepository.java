package org.pamapam.repository;

import org.pamapam.model.NewsletterRegistration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsletterRegistrationRepository extends JpaRepository<NewsletterRegistration, Long> {

}
