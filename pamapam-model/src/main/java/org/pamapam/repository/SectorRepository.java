package org.pamapam.repository;

import java.util.Collection;
import java.util.List;

import org.pamapam.model.Sector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SectorRepository extends JpaRepository<Sector, Long>, JpaSpecificationExecutor<Sector> {

	List<Sector> findByParentIsNotNull();

	List<Sector> findByParentIsNull();

	@Query("select sec from Sector sec where sec.id in :ids")
	List<Sector> findByIdIn(@Param("ids") Collection<Long> ids);
}
