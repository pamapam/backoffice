package org.pamapam.repository;

import java.util.Collection;
import java.util.Set;

import org.pamapam.model.EntityStatus;
import org.pamapam.model.enums.EntityStatusType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EntityStatusRepository extends JpaRepository<EntityStatus, Long>, JpaSpecificationExecutor<EntityStatus> {

	EntityStatus findFirstByEntityStatusType(EntityStatusType entityStatusType);

	Set<EntityStatus> findByEntityStatusTypeNot(EntityStatusType entityStatusType);

	@Query("select e from EntityStatus e where e.entityStatusType in :entityStatusTypes")
	Set<EntityStatus> findByEntityStatusTypeIn(@Param("entityStatusTypes") Collection<EntityStatusType> entityStatusTypes);

}
