package org.pamapam.repository;

import org.pamapam.model.Document;
import org.pamapam.repository.search.DocumentSearchSpecification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DocumentRepository extends JpaRepository<Document, Long>, JpaSpecificationExecutor<DocumentSearchSpecification> {

}
