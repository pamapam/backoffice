package org.pamapam.repository;

import org.pamapam.model.PersonRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRoleRepository extends JpaRepository<PersonRole, Long> {

}
