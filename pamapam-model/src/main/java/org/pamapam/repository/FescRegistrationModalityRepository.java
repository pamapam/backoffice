package org.pamapam.repository;

import org.pamapam.model.FescRegistrationModality;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FescRegistrationModalityRepository extends JpaRepository<FescRegistrationModality, Long> {

}
