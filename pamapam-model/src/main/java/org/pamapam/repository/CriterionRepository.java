package org.pamapam.repository;

import java.util.List;

import org.pamapam.model.Criterion;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CriterionRepository extends JpaRepository<Criterion, Long> {

	List<Criterion> findByOldVersionFalse(Sort sort);

	List<Criterion> findByOldVersionTrue(Sort sort);

}
