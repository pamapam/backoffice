package org.pamapam.repository;

import org.pamapam.model.MailTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailTemplateRepository extends JpaRepository<MailTemplate, Long> {

	MailTemplate findByName(String name);
}
