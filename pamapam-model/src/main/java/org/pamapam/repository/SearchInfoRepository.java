package org.pamapam.repository;

import org.pamapam.model.SearchInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SearchInfoRepository extends JpaRepository<SearchInfo, Long> {

}
