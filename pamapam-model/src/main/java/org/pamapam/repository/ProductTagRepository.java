package org.pamapam.repository;

import java.util.List;

import org.pamapam.model.ProductTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTagRepository extends JpaRepository<ProductTag, Long> {

	List<ProductTag> findByTag(String tag);
}
