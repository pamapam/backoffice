package org.pamapam.repository;

import org.pamapam.model.CriterionAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CriterionAnswerRepository extends JpaRepository<CriterionAnswer, Long> {

}
