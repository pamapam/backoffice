package org.pamapam.model;

import org.jamgo.model.entity.Model;

import javax.persistence.*;
import java.util.Comparator;
import java.util.Objects;

@Entity
@Table(name = "text_tag")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tag_type")
public abstract class TextTag extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<TextTag> TEXT_ORDER = new Comparator<TextTag>() {
		@Override
		public int compare(final TextTag objectA, final TextTag objectB) {
			return objectA.getTag().compareToIgnoreCase(objectB.getTag());
		}
	};

	@Id
	@TableGenerator(name = "text_tag_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "sector_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "text_tag_id_gen")
	private Long id;

	@Column(name = "tag_type")
	private String tagType;

	@Column
	private String tag;

	public TextTag() {
		super();
	}

	public TextTag(final String tagType) {
		super();
		this.tagType = tagType;
	}

	public TextTag(final Long id, final String tag, final String tagType) {
		super();
		this.id = id;
		this.tag = tag;
		this.tagType = tagType;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public String getTagType() {
		return this.tagType;
	}

	public void setTagType(final String type) {
		this.tagType = type;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(final String tag) {
		this.tag = tag;
	}

	@Override
	public boolean equals(final Object obj) {

		if (obj == this) {
			return true;
		}
		if (!(obj instanceof TextTag)) {
			return false;
		}
		final TextTag other = (TextTag) obj;
		return Objects.equals(this.tagType, other.tagType) &&
			Objects.equals(this.tag, other.tag);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.tagType, this.tag);
	}

}
