package org.pamapam.model;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.pamapam.model.enums.FescTimeSlot;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

@Entity
@Table(name = "social_economy_entity_fesc")
@Cacheable(false)
public class SocialEconomyEntityFesc {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "id")
	private SocialEconomyEntity see;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fesc_previous_entity_status_id")
	private EntityStatus previousEntityStatus;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fesc_entity_status_id")
	private EntityStatus entityStatus;

	@Column(name = "fesc_email")
	private String email;

	@Column(name = "fesc_video")
	private String video;

	@Convert(converter = LocalizedAttributeConverter.class)
	@Column(name = "fesc_offer", length = 2048)
	private LocalizedString offer;

	@Column(name = "invoice_name")
	private String invoiceName;

	@Column(name = "invoice_email")
	private String invoiceEmail;

	@Column(name = "invoice_address")
	private String invoiceAddress;

	@Column(name = "invoice_postal_code")
	private String invoicePostalCode;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "modality_id")
	private FescRegistrationModality modality;

	@Column(name = "fellow_name")
	private String fellowName;

	@Column(name = "fesc_previous_fee")
	private Integer previousFee;

	@Column(name = "fesc_fee")
	private Integer fee;

	@Column(name = "invoice_account")
	private String invoiceAccount;

	@Column(name = "accepted_registration_privacy_policy")
	private Boolean acceptedRegistrationPrivacyPolicy;

	@Column(name = "accepted_registration_privacy_policy_date")
	private Timestamp acceptedRegistrationPrivacyPolicyDate;

	@Column(name = "accepted_participation_privacy_policy")
	private Boolean acceptedParticipationPrivacyPolicy;

	@Column(name = "accepted_participation_privacy_policy_date")
	private Timestamp acceptedParticipationPrivacyPolicyDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "main_sector_id")
	private Sector mainSector;

	/** Time slot for FESC. Only useful if the choosen modality is
	 *  the time slot one.
	 *
	 */
	@Column(name = "time_slot")
	private FescTimeSlot timeSlot;

	public SocialEconomyEntityFesc() {
		super();
	}

	public SocialEconomyEntityFesc(final SocialEconomyEntityFesc source, final SocialEconomyEntity entity) {
		this();
		this.see = entity;
		this.entityStatus = source.entityStatus;
		this.email = source.email;
		this.video = source.video;
		// ...	FIXME: Workaround to avoid error on LocalizedStrings with null raw values.
		//		Jamgo framework converter should generate a null column value, but instead
		//		generates a wrong json string.
		this.offer = Optional.ofNullable(source.offer).map(o -> o.getRawValue()).map(o -> source.offer).orElse(null);
		this.invoiceName = source.invoiceName;
		this.invoiceEmail = source.invoiceEmail;
		this.invoiceAddress = source.invoiceAddress;
		this.invoicePostalCode = source.invoicePostalCode;
		this.fee = source.fee;
		this.invoiceAccount = source.invoiceAccount;
		this.mainSector = source.mainSector;
		this.timeSlot = source.timeSlot;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public SocialEconomyEntity getSee() {
		return this.see;
	}

	public void setSee(final SocialEconomyEntity see) {
		this.see = see;
	}

	public EntityStatus getPreviousEntityStatus() {
		return this.previousEntityStatus;
	}

	public void setPreviousEntityStatus(final EntityStatus previousEntityStatus) {
		this.previousEntityStatus = previousEntityStatus;
	}

	public EntityStatus getEntityStatus() {
		return this.entityStatus;
	}

	public void setEntityStatus(final EntityStatus entityStatus) {
		this.entityStatus = entityStatus;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getVideo() {
		return this.video;
	}

	public void setVideo(final String video) {
		this.video = video;
	}

	public LocalizedString getOffer() {
		return this.offer;
	}

	public void setOffer(final LocalizedString offer) {
		this.offer = offer;
	}

	public String getInvoiceName() {
		return this.invoiceName;
	}

	public void setInvoiceName(final String invoiceName) {
		this.invoiceName = invoiceName;
	}

	public String getInvoiceEmail() {
		return this.invoiceEmail;
	}

	public void setInvoiceEmail(final String invoiceEmail) {
		this.invoiceEmail = invoiceEmail;
	}

	public String getInvoiceAddress() {
		return this.invoiceAddress;
	}

	public void setInvoiceAddress(final String invoiceAddress) {
		this.invoiceAddress = invoiceAddress;
	}

	public String getInvoicePostalCode() {
		return this.invoicePostalCode;
	}

	public void setInvoicePostalCode(final String invoicePostalCode) {
		this.invoicePostalCode = invoicePostalCode;
	}

	public FescRegistrationModality getModality() {
		return this.modality;
	}

	public void setModality(final FescRegistrationModality modality) {
		this.modality = modality;
	}

	public String getFellowName() {
		return this.fellowName;
	}

	public void setFellowName(final String fellowName) {
		this.fellowName = fellowName;
	}

	public Integer getPreviousFee() {
		return this.previousFee;
	}

	public void setPreviousFee(final Integer previousFee) {
		this.previousFee = previousFee;
	}

	public Integer getFee() {
		return this.fee;
	}

	public void setFee(final Integer fee) {
		this.fee = fee;
	}

	public String getInvoiceAccount() {
		return this.invoiceAccount;
	}

	public void setInvoiceAccount(final String invoiceAccount) {
		this.invoiceAccount = invoiceAccount;
	}

	public Sector getMainSector() {
		return this.mainSector;
	}

	public void setMainSector(final Sector mainSector) {
		this.mainSector = mainSector;
	}

	public Boolean getAcceptedRegistrationPrivacyPolicy() {
		return this.acceptedRegistrationPrivacyPolicy;
	}

	public void setAcceptedRegistrationPrivacyPolicy(final Boolean acceptedRegistrationPrivacyPolicy) {
		this.setAcceptedRegistrationPrivacyPolicyDate(Timestamp.from(Instant.now()));
		this.acceptedRegistrationPrivacyPolicy = acceptedRegistrationPrivacyPolicy;
	}

	public Boolean getAcceptedParticipationPrivacyPolicy() {
		return this.acceptedParticipationPrivacyPolicy;
	}

	public void setAcceptedParticipationPrivacyPolicy(final Boolean acceptedParticipationPrivacyPolicy) {
		this.setAcceptedParticipationPrivacyPolicyDate(Timestamp.from(Instant.now()));
		this.acceptedParticipationPrivacyPolicy = acceptedParticipationPrivacyPolicy;
	}

	public Timestamp getAcceptedRegistrationPrivacyPolicyDate() {
		return this.acceptedRegistrationPrivacyPolicyDate;
	}

	public void setAcceptedRegistrationPrivacyPolicyDate(final Timestamp acceptedRegistrationPrivacyPolicyDate) {
		this.acceptedRegistrationPrivacyPolicyDate = acceptedRegistrationPrivacyPolicyDate;
	}

	public Timestamp getAcceptedParticipationPrivacyPolicyDate() {
		return this.acceptedParticipationPrivacyPolicyDate;
	}

	public void setAcceptedParticipationPrivacyPolicyDate(final Timestamp acceptedParticipationPrivacyPolicyDate) {
		this.acceptedParticipationPrivacyPolicyDate = acceptedParticipationPrivacyPolicyDate;
	}

	public FescTimeSlot getTimeSlot() {
		return this.timeSlot;
	}

	public void setTimeSlot(final FescTimeSlot timeSlot) {
		this.timeSlot = timeSlot;
	}
}
