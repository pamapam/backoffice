package org.pamapam.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.Model;

@Entity
@Table(name = "document")
public class Document extends Model{

	private static final long serialVersionUID = 1L;
	
	@Id
	@TableGenerator(name = "document_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "document_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "document_id_gen")
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_binary_resource")
	private BinaryResource binaryResource;
	
	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public BinaryResource getBinaryResource() {
		return this.binaryResource;
	}

	public void setBinaryResource(BinaryResource binaryResource) {
		this.binaryResource = binaryResource;
	}

}
