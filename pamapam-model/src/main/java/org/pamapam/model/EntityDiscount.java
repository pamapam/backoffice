package org.pamapam.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "entity_discount")
public class EntityDiscount extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "entity_discount_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_discount_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_discount_id_gen")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "url")
	private String url;

	@Column(name = "begin_date")
	private Date beginDate;

	@Column(name = "end_date")
	private Date endDate;

	@ManyToOne
	@JoinColumn(name = "entity_id")
	private SocialEconomyEntity entity;

	public EntityDiscount() {
		super();
	}

	public EntityDiscount(final EntityDiscount source, final SocialEconomyEntity entity) {
		this();
		this.name = source.name;
		this.description = source.description;
		this.url = source.url;
		this.beginDate = source.beginDate;
		this.endDate = source.endDate;
		this.entity = entity;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public Date getBeginDate() {
		return this.beginDate;
	}

	public void setBeginDate(final Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	public SocialEconomyEntity getEntity() {
		return this.entity;
	}

	public void setEntity(final SocialEconomyEntity entity) {
		this.entity = entity;
	}

}
