package org.pamapam.model;

import java.util.Comparator;
import java.util.Set;

import javax.persistence.*;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@javax.persistence.Entity
@Table(name = "criterion_answer")
public class CriterionAnswer extends Model {
	
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "criterion_answer_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "criterion_answer_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "criterion_answer_id_gen")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "criterion_id")
	private Criterion criterion;

	@Column(name = "answer_order")
	private Integer answerOrder;

	@Convert(converter = LocalizedAttributeConverter.class)
	@Column(length = 1024)
	private LocalizedString answer;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "criterion_answer_questionaire", joinColumns = { @JoinColumn(name = "id_criterion_answer") }, inverseJoinColumns = { @JoinColumn(name = "id_questionaire", referencedColumnName = "id") })
	private Set<Questionaire> questionaires;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Criterion getCriterion() {
		return this.criterion;
	}

	public void setCriterion(Criterion criterion) {
		this.criterion = criterion;
	}

	public Integer getAnswerOrder() {
		return this.answerOrder;
	}

	public void setAnswerOrder(Integer answerOrder) {
		this.answerOrder = answerOrder;
	}

	public LocalizedString getAnswer() {
		return this.answer;
	}

	public void setAnswer(LocalizedString answer) {
		this.answer = answer;
	}

	public void setAnswerText(String answer) {
		this.answer.set("ca", answer);
	}

	public Set<Questionaire> getQuestionaires() {
	return questionaires;
	}

	public void setQuestionaires(Set<Questionaire> questionaires) {
		this.questionaires = questionaires;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}

		CriterionAnswer that = (CriterionAnswer) o;

		if (this.id != null ? !this.id.equals(that.id) : that.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = 31 * (this.id != null ? this.id.intValue() : 0);
		return result;
	}
	
	public Questionaire getQuestionaire() {
		if (this.getQuestionaires().isEmpty()) 
		{ 
			return null;
		}
		return this.getQuestionaires().iterator().next();
	}

	public static final Comparator<CriterionAnswer> ANSWER_ORDER = new Comparator<CriterionAnswer>() {
		@Override
		public int compare(CriterionAnswer objectA, CriterionAnswer objectB) {
			return objectA.getAnswerOrder().compareTo(objectB.getAnswerOrder());
		}
	};
	
	public static final Comparator<CriterionAnswer> QUESTIONARY_ANSWER_ORDER = new Comparator<CriterionAnswer>() {
		@Override
		public int compare(CriterionAnswer objectA, CriterionAnswer objectB) {
			if (objectA.getQuestionaire() != null && objectB.getQuestionaire() != null) {
				if (!objectA.getQuestionaire().equals(objectB.getQuestionaire())) {
					return objectA.getQuestionaire().getId().compareTo(objectB.getQuestionaire().getId());
				}
			}
			return objectA.getAnswerOrder().compareTo(objectB.getAnswerOrder());
		}
	};



}
