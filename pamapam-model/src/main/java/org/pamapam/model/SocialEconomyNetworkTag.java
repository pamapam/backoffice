package org.pamapam.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = SocialEconomyNetworkTag.TAG_TYPE)
public class SocialEconomyNetworkTag extends TextTag {

	private static final long serialVersionUID = 1L;

	public static final String TAG_TYPE = "SENET";

	public SocialEconomyNetworkTag() {
		super(TAG_TYPE);
	}

	public SocialEconomyNetworkTag(Long id, String tag) {
		super(id, tag, TAG_TYPE);
	}

}
