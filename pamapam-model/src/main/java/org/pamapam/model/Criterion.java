package org.pamapam.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.*;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@javax.persistence.Entity
@Table(name = "criterion")
public class Criterion extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<Criterion> ID_ORDER = new Comparator<Criterion>() {
		@Override
		public int compare(Criterion objectA, Criterion objectB) {
			return objectA.getId().compareTo(objectB.getId());
		}
	};
	public static final Comparator<Criterion> VIEW_ORDER = new Comparator<Criterion>() {
		@Override
		public int compare(Criterion objectA, Criterion objectB) {
			return objectA.getViewOrder().compareTo(objectB.getViewOrder());
		}
	};

	public static final Integer MAX_VALUE = 500;

	@Id
	@TableGenerator(name = "criterion_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "criterion_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "criterion_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString question;

	@Convert(converter = LocalizedAttributeConverter.class)
	@Column(length = 1024)
	private LocalizedString description;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "icon_id")
	private BinaryResource icon;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "criterion", cascade = CascadeType.ALL)
	private List<CriterionAnswer> criterionAnswers;

	@Column(name = "view_order")
	private Integer viewOrder;

	@Column(name = "old_version")
	private Boolean oldVersion;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}

	public LocalizedString getQuestion() {
		return this.question;
	}

	public void setQuestion(LocalizedString question) {
		this.question = question;
	}

	public LocalizedString getDescription() {
		return this.description;
	}

	public void setDescription(LocalizedString description) {
		this.description = description;
	}

	public BinaryResource getIcon() {
		return this.icon;
	}

	public void setIcon(BinaryResource icon) {
		this.icon = icon;
	}

	public List<CriterionAnswer> getCriterionAnswers() {
		return this.criterionAnswers;
	}

	public List<CriterionAnswer> getCriterionAnswers(Questionaire questionaire) {
		return this.criterionAnswers.stream()
				.sorted(CriterionAnswer.ANSWER_ORDER)
				.filter(criterionAnswer -> criterionAnswer.getQuestionaires().contains(questionaire))
				.collect(Collectors.toList());
	}

	public void setCriterionAnswers(List<CriterionAnswer> criterionAnswers) {
		this.criterionAnswers = criterionAnswers;
	}

	public Integer getViewOrder() {
		return this.viewOrder;
	}

	public void setViewOrder(Integer viewOrder) {
		this.viewOrder = viewOrder;
	}

	public Boolean getOldVersion() {
		return this.oldVersion;
	}

	public void setOldVersion(Boolean oldVersion) {
		this.oldVersion = oldVersion;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
			.appendSuper(super.hashCode())
			.append(this.oldVersion)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Criterion other = (Criterion) obj;
		return new EqualsBuilder()
			.appendSuper(super.equals(obj))
			.append(this.oldVersion, other.oldVersion)
			.isEquals();
	}

}
