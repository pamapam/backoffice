package org.pamapam.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "entity_evaluation_criterion", uniqueConstraints = @UniqueConstraint(columnNames = { "entity_evaluation_id", "criterion_id" }))
@Cacheable(false)
public class EntityEvaluationCriterion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "entity_evaluation_criterion_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_evaluation_criterion_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_evaluation_criterion_id_gen")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "entity_evaluation_id")
	private EntityEvaluation entityEvaluation;

	@ManyToOne
	@JoinColumn(name = "criterion_id")
	private Criterion criterion;

	/**
	 * Level of compliance with criteria. Each circle of the interface represents the number 100.
	 */
	@Column
	private Integer level;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "entityEvaluationCriterion", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EntityEvaluationCriterionAnswer> entityEvaluationCriterionAnswers;

	@Column(length = 2048)
	private String notes;

	public EntityEvaluationCriterion() {
		super();
	}

	public EntityEvaluationCriterion(final EntityEvaluation entityEvaluation, final Criterion criterion) {
		this();
		this.entityEvaluation = entityEvaluation;
		this.criterion = criterion;
		this.entityEvaluationCriterionAnswers = new ArrayList<>();
	}

	public EntityEvaluationCriterion(final EntityEvaluationCriterion source, final EntityEvaluation entityEvaluation) {
		this();
		this.entityEvaluation = entityEvaluation;
		this.criterion = source.criterion;
		this.level = source.level;

		this.entityEvaluationCriterionAnswers = new ArrayList<>();
		source.entityEvaluationCriterionAnswers.forEach(each -> this.entityEvaluationCriterionAnswers.add(new EntityEvaluationCriterionAnswer(each, this)));

		this.notes = source.notes;
	}

	public EntityEvaluation getEntityEvaluation() {
		return this.entityEvaluation;
	}

	public void setEntityEvaluation(final EntityEvaluation entityEvaluation) {
		this.entityEvaluation = entityEvaluation;
	}

	public Criterion getCriterion() {
		return this.criterion;
	}

	public void setCriterion(final Criterion criterion) {
		this.criterion = criterion;
	}

	public void setLevel(final Integer level) {
		this.level = level;
	}

	public Integer getLevel() {
		Integer calculatedLevel = null;
		if ((this.entityEvaluationCriterionAnswers != null) && !this.entityEvaluationCriterionAnswers.isEmpty()) {
			calculatedLevel = Math.min(this.entityEvaluationCriterionAnswers.stream().mapToInt(each -> each.getValue()).sum(), Criterion.MAX_VALUE);
		}

		return Optional.ofNullable(calculatedLevel).orElse(this.level);
	}

	public List<EntityEvaluationCriterionAnswer> getEntityEvaluationCriterionAnswers() {
		return this.entityEvaluationCriterionAnswers;
	}

	public void setEntityEvaluationCriterionAnswers(final List<EntityEvaluationCriterionAnswer> entityEvaluationCriterionAnswers) {
		this.entityEvaluationCriterionAnswers = entityEvaluationCriterionAnswers;
	}

	public void addEntityEvaluationCriterionAnswer(final EntityEvaluationCriterionAnswer entityEvaluationCriterionAnswer) {
		this.entityEvaluationCriterionAnswers.add(entityEvaluationCriterionAnswer);
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(final String notes) {
		this.notes = notes;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if ((o == null) || (this.getClass() != o.getClass())) {
			return false;
		}

		final EntityEvaluationCriterion that = (EntityEvaluationCriterion) o;

		if (this.entityEvaluation != null ? !this.entityEvaluation.equals(that.entityEvaluation) : that.entityEvaluation != null) {
			return false;
		}
		if (this.criterion != null ? !this.criterion.equals(that.criterion) : that.criterion != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (this.entityEvaluation != null ? this.entityEvaluation.hashCode() : 0);
		result = (31 * result) + (this.criterion != null ? this.criterion.hashCode() : 0);
		return result;
	}

}
