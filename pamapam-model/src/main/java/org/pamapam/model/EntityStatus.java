package org.pamapam.model;

import java.util.Comparator;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;
import org.pamapam.model.enums.EntityStatusType;

@Entity
@Table(name = "entity_status")
public class EntityStatus extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<EntityStatus> NAME_ORDER = new Comparator<EntityStatus>() {
		@Override
		public int compare(EntityStatus objectA, EntityStatus objectB) {
			return objectA.getName().getDefaultText().compareToIgnoreCase(objectB.getName().getDefaultText());
		}
	};

	@Id
	@TableGenerator(name = "entity_status_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_status_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_status_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Column(name = "status_type")
	@Enumerated
	private EntityStatusType entityStatusType;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "entity_status_next_status", joinColumns = { @JoinColumn(name = "entity_status_id") }, inverseJoinColumns = { @JoinColumn(name = "next_status_id", referencedColumnName = "id") })
	private Set<EntityStatus> nextStatuses;

	public EntityStatus() {
		super();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}

	public EntityStatusType getEntityStatusType() {
		return this.entityStatusType;
	}

	public void setEntityStatusType(EntityStatusType entityStatusType) {
		this.entityStatusType = entityStatusType;
	}

	public boolean isDraft() {
		return EntityStatusType.DRAFT.equals(this.entityStatusType);
	}

	public boolean isPrepublished() {
		return EntityStatusType.PREPUBLISHED.equals(this.entityStatusType);
	}

	public boolean isPublished() {
		return EntityStatusType.PUBLISHED.equals(this.entityStatusType);
	}
	public Set<EntityStatus> getNextStatuses() {
		return this.nextStatuses;
	}

	public void setNextStatuses(Set<EntityStatus> nextStatuses) {
		this.nextStatuses = nextStatuses;
	}

	public String getColor() {
		return this.getEntityStatusType().getColor();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == this) {
			return true;
		}
		if (!(obj instanceof EntityStatus)) {
			return false;
		}
		EntityStatus other = (EntityStatus) obj;
		return Objects.equals(this.id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public String toString() {
		return "{" + name + '}';
	}
}