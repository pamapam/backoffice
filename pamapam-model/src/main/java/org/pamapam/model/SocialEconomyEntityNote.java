package org.pamapam.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "entity_note")
public class SocialEconomyEntityNote extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "entity_note_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_note_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_note_id_gen")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "entity_id")
	private SocialEconomyEntity entity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private PamapamUser user;

	@Column(name = "note_timestamp")
	private Timestamp timestamp;

	@Column(length = 16384)
	private String text;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public SocialEconomyEntity getEntity() {
		return this.entity;
	}

	public void setEntity(SocialEconomyEntity entity) {
		this.entity = entity;
	}

	public PamapamUser getUser() {
		return this.user;
	}

	public void setUser(PamapamUser user) {
		this.user = user;
	}

	public Timestamp getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
