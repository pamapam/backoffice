package org.pamapam.model.enums;

public enum FescTimeSlot {
	SATURDAY_MORNING,
	SATURDAY_AFTERNOON,
	SUNDAY_MORNING
}
