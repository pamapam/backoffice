package org.pamapam.model.enums;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class EntityActionSetConverter implements AttributeConverter<Set<Enum<EntityAction>>, String> {

	@Override
	public String convertToDatabaseColumn(Set<Enum<EntityAction>> attribute) {
		StringBuilder columnValueBuilder = new StringBuilder();
		if (attribute != null) {
			String separator = "";
			for (Enum<EntityAction> eachEnum : attribute) {
				columnValueBuilder.append(separator).append(eachEnum.name());
				separator = ",";
			}
		}
		return columnValueBuilder.toString().isEmpty() ? null : columnValueBuilder.toString();
	}

	@Override
	public Set<Enum<EntityAction>> convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		Set<Enum<EntityAction>> entityAttribute = new HashSet<>();
		String[] enumValues = dbData.split(",");
		for (String eachEnumValue : enumValues) {
			entityAttribute.add(Enum.valueOf(EntityAction.class, eachEnumValue));
		}
		return entityAttribute;
	}
}
