package org.pamapam.model.enums;

public enum ModelAttributeType {
	STRING,
	INTEGER,
	LONG,
	DECIMAL,
	BOOLEAN,
	DATETIME;

}
