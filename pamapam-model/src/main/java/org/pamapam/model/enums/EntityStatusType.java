package org.pamapam.model.enums;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum EntityStatusType {
	DRAFT(EntityStatusType.ORIGIN_PAMAPAM),
	REVIEW_PENDING(EntityStatusType.ORIGIN_PAMAPAM),
	INCOMPLETE(EntityStatusType.ORIGIN_PAMAPAM),
	PUBLISHED(EntityStatusType.ORIGIN_PAMAPAM),
	UNPUBLISHED(EntityStatusType.ORIGIN_PAMAPAM),
	UPDATE_PENDING(EntityStatusType.ORIGIN_PAMAPAM),
	INITIATIVE(EntityStatusType.ORIGIN_PAMAPAM),
	PROPOSED(EntityStatusType.ORIGIN_PAMAPAM),
	ARCHIVED(EntityStatusType.ORIGIN_PAMAPAM),
	DELETED(EntityStatusType.ORIGIN_PAMAPAM),
	EXTERNAL(EntityStatusType.ORIGIN_PAMAPAM),
	/* NEW FESC TYPES*/
	FESC_PROPOSED(EntityStatusType.ORIGIN_FESC),
	FESC_IN_PROGRESS(EntityStatusType.ORIGIN_FESC),
	FESC_UNDECIDED(EntityStatusType.ORIGIN_FESC),
	FESC_ACCEPTED(EntityStatusType.ORIGIN_FESC),
	FESC_INVOICED(EntityStatusType.ORIGIN_FESC),
	FESC_PAID(EntityStatusType.ORIGIN_FESC),
	FESC_REJECTED(EntityStatusType.ORIGIN_FESC),
	PREPUBLISHED(EntityStatusType.ORIGIN_PAMAPAM);
	// ... Einateca
	//EINATECA("pamapam");

	private static final String ORIGIN_PAMAPAM = "pamapam";
	private static final String ORIGIN_FESC = "fesc";

	private String origin;

	EntityStatusType(final String origin) {
		this.origin = origin;
	}

	public String getColor() {
		switch (this) {
			case PROPOSED:
			case INITIATIVE:
				return "blue";
			case DRAFT:
			case INCOMPLETE:
				return "yellow";
			case REVIEW_PENDING:
			case UPDATE_PENDING:
				return "orange";
			case PREPUBLISHED:
				return "cyan";
			case PUBLISHED:
				return "green";
			case UNPUBLISHED:
				return "red";
			case EXTERNAL:
				return "lilac";
//			case EINATECA:
//				return "pink";

			default:
				return "white";
		}
	}

	public static Set<EntityStatusType> getPamapamTypes() {
		return EntityStatusType.getTypesByOrigin(EntityStatusType.ORIGIN_PAMAPAM);
	}

	public static Set<EntityStatusType> getFescTypes() {
		return EntityStatusType.getTypesByOrigin(EntityStatusType.ORIGIN_FESC);
	}

	public static Set<EntityStatusType> getTypesByOrigin(final String origin) {
		return Arrays.stream(EntityStatusType.values())
			.filter(type -> origin.equalsIgnoreCase(type.origin))
			.collect(Collectors.toSet());
	}

	public static Set<EntityStatusType> sameOriginValues(final EntityStatusType type) {
		return EntityStatusType.getTypesByOrigin(type.origin);
	}
}
