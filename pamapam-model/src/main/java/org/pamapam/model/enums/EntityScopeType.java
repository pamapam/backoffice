package org.pamapam.model.enums;
//Pam a Pam es el entityScope por default
//Ver si el hecho de que sea Pam A Pam afecta a la lógica de la aplicación
//Si afecta a la lógica, debe ser un tipo

//Esta enumeración se crea para definir una lista de tipos
//Los tipos sirven para definir el comportamiento del tratamiento de los datos
//¡¡Ten en cuenta que puede haber más de un registro con el mismo tipo de ámbito!!
public enum EntityScopeType {
	DEFAULT,
	EINATECA,
	FESC_2021_ENTITY_SCOPE,
	FESC_2022_ENTITY_SCOPE,
	COMMUNITY_ECONOMY_ENTITY_SCOPE,
	INTERCOOPERACIO_ENTITY_SCOPE,
	SOCIAL_BALANCE_ENTITY_SCOPE, //scope for the social economy entities that come from the XES' social balance sheet or have the XES' social balance sheet done
	GREEN_COMMERCE_ENTITY_SCOPE //for entities comming from green commerce excel import process.
}
