package org.pamapam.model.enums;

public enum EntityAction {
	CREATE,
	UPDATE,
	UPDATE_STATUS,
	DELETE
}
