package org.pamapam.model.enums;

public enum UserEntityLinkType {
	OWNER,
	UNRELATED
}
