package org.pamapam.model.enums;

public enum NotificationType {
	ENTITY_ACTION,
	ADD_USER,
	RESET_PASSWORD
}
