package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@javax.persistence.Entity
@Table(name = "social_economy_network")
public class SocialEconomyNetwork extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<SocialEconomyNetwork> DEFAULT_ORDER = new Comparator<SocialEconomyNetwork>() {
		@Override
		public int compare(SocialEconomyNetwork objectA, SocialEconomyNetwork objectB) {
			if (objectA.getName() != null && objectB.getName() != null) {
				return objectA.getName().getDefaultText().compareTo(objectB.getName().getDefaultText());
			} else if (objectA.getName() == null) {
				return -1;
			} else {
				return 1;
			}
		}
	};

	@Id
	@TableGenerator(name = "social_economy_network_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "social_economy_network_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "social_economy_network_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
