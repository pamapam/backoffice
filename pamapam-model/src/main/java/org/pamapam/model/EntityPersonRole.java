package org.pamapam.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "entity_person_role", uniqueConstraints = @UniqueConstraint(columnNames = { "entity_id", "person_role_id" }))
@Cacheable(false)
public class EntityPersonRole implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "entity_person_role_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_person_role_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_person_role_id_gen")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "entity_id")
	private SocialEconomyEntity entity;

	@ManyToOne
	@JoinColumn(name = "person_role_id")
	private PersonRole personRole;

	@Column
	private Integer maleCount;

	@Column
	private Integer femaleCount;

	@Column
	private Integer otherCount;

	public EntityPersonRole() {
		super();
	}

	public EntityPersonRole(SocialEconomyEntity entity, PersonRole personRole) {
		this();
		this.entity = entity;
		this.personRole = personRole;
	}

	public EntityPersonRole(EntityPersonRole source) {
		this();
		this.entity = source.entity;
		this.personRole = source.personRole;
		this.maleCount = source.maleCount;
		this.femaleCount = source.femaleCount;
		this.otherCount = source.otherCount;
	}

	public EntityPersonRole(EntityPersonRole source, SocialEconomyEntity entity) {
		this(source);
		this.entity = entity;
	}

	public SocialEconomyEntity getEntity() {
		return this.entity;
	}

	public void setEntity(SocialEconomyEntity entity) {
		this.entity = entity;
	}

	public PersonRole getPersonRole() {
		return this.personRole;
	}

	public void setPersonRole(PersonRole personRole) {
		this.personRole = personRole;
	}

	public Integer getMaleCount() {
		return this.maleCount;
	}

	public void setMaleCount(Integer maleCount) {
		this.maleCount = maleCount;
	}

	public Integer getFemaleCount() {
		return this.femaleCount;
	}

	public void setFemaleCount(Integer femaleCount) {
		this.femaleCount = femaleCount;
	}

	public Integer getOtherCount() {
		return this.otherCount;
	}

	public void setOtherCount(Integer otherCount) {
		this.otherCount = otherCount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}

		EntityPersonRole that = (EntityPersonRole) o;

		if (this.entity != null ? !this.entity.equals(that.entity) : that.entity != null) {
			return false;
		}
		if (this.personRole != null ? !this.personRole.equals(that.personRole) : that.personRole != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (this.entity != null ? this.entity.hashCode() : 0);
		result = 31 * result + (this.personRole != null ? this.personRole.hashCode() : 0);
		return result;
	}

}
