package org.pamapam.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = KeywordTag.TAG_TYPE)
public class KeywordTag extends TextTag {

	private static final long serialVersionUID = 1L;

	public static final String TAG_TYPE = "KEYW";

	public KeywordTag() {
		super(KeywordTag.TAG_TYPE);
	}

	public KeywordTag(final Long id, final String tag) {
		super(id, tag, KeywordTag.TAG_TYPE);
	}

}
