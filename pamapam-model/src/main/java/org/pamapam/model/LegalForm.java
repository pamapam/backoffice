package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@Entity
@Table(name = "legal_form")
public class LegalForm extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<LegalForm> NAME_ORDER = new Comparator<LegalForm>() {
		@Override
		public int compare(LegalForm objectA, LegalForm objectB) {
			return objectA.getName().getDefaultText().compareToIgnoreCase(objectB.getName().getDefaultText());
		}
	};

	@Id
	@TableGenerator(name = "legal_form_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "legal_form_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "legal_form_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
