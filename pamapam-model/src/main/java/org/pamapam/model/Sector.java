package org.pamapam.model;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

import javax.persistence.*;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "sector")
public class Sector extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<Sector> NAME_ORDER = new Comparator<Sector>() {
		@Override
		public int compare(final Sector objectA, final Sector objectB) {
			return objectA.getName().getDefaultText().compareTo(objectB.getName().getDefaultText());
		}
	};

	public static final Comparator<Sector> NAME_HIERARCHICAL_ORDER = new Comparator<Sector>() {
		@Override
		public int compare(final Sector objectA, final Sector objectB) {
			if ((objectA.getParent() == null) && (objectB.getParent() == null)) {
				return objectA.getName().getDefaultText().compareToIgnoreCase(objectB.getName().getDefaultText());
			} else if (objectA.getParent() == null) {
				return objectA.getName().getDefaultText().compareToIgnoreCase(objectB.getParent().getName().getDefaultText());
			} else if (objectB.getParent() == null) {
				return objectA.getParent().getName().getDefaultText().compareToIgnoreCase(objectB.getName().getDefaultText());
			} else if (Objects.equals(objectA.getParent(), objectB.getParent())) {
				return objectA.getName().getDefaultText().compareToIgnoreCase(objectB.getName().getDefaultText());
			} else {
				return objectA.getParent().getName().getDefaultText().compareToIgnoreCase(objectB.getParent().getName().getDefaultText());
			}
		}
	};

	public static final Comparator<Sector> FULLNAME_ORDER = new Comparator<Sector>() {
		@Override
		public int compare(final Sector objectA, final Sector objectB) {
			return objectA.getFullName().compareTo(objectB.getFullName());
		}
	};

	@Id
	@TableGenerator(name = "sector_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "sector_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "sector_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString description;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "icon_id")
	private BinaryResource icon;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "map_icon_id")
	private BinaryResource mapIcon;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private Sector parent;

	@OneToMany(mappedBy = "parent")
	private List<Sector> sectors;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(final LocalizedString name) {
		this.name = name;
	}

	public LocalizedString getDescription() {
		return this.description;
	}

	public void setDescription(final LocalizedString description) {
		this.description = description;
	}

	public BinaryResource getIcon() {
		if (this.icon != null) {
			return this.icon;
		} else if (this.parent != null) {
			return this.parent.getIcon();
		} else {
			return null;
		}
	}

	public void setIcon(final BinaryResource icon) {
		this.icon = icon;
	}

	public BinaryResource getMapIcon() {
		if (this.mapIcon != null) {
			return this.mapIcon;
		} else if (this.parent != null) {
			return this.parent.getMapIcon();
		} else {
			return null;
		}
	}

	public void setMapIcon(final BinaryResource icon) {
		this.mapIcon = icon;
	}

	public Sector getParent() {
		return this.parent;
	}

	public void setParent(final Sector parent) {
		this.parent = parent;
	}

	public Sector getMainSector() {
		if (this.parent == null) {
			return this;
		} else {
			return this.parent.getMainSector();
		}
	}

	public List<Sector> getSectors() {
		return this.sectors;
	}

	public void setSectors(final List<Sector> sectors) {
		this.sectors = sectors;
	}

	public String getFullName() {
		final StringBuilder fullNameBuilder = new StringBuilder();
		if (this.parent != null) {
			fullNameBuilder.append(this.parent.getName().getDefaultText()).append(" - ");
		}
		if (this.name != null) {
			fullNameBuilder.append(this.name.getDefaultText());
		}
		return fullNameBuilder.toString();
	}

	public boolean isSubsector() {
		return this.parent != null;
	}

	public boolean belongsToSector(final Sector sector) {
		if (this.id == sector.id) {
			return true;
		} else if (this.parent != null) {
			return this.parent.belongsToSector(sector);
		} else {
			return false;
		}
	}
}
