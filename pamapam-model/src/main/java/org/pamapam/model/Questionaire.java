package org.pamapam.model;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

import javax.persistence.*;
import java.util.Set;

/**
 * This class serves as a label that will group {@link Criterion} to create Questionaire types to be used in
 * {@link SocialEconomyEntity}'s {@link EntityEvaluation}.
 *
 */
@Entity
@Table(name = "questionaire")
public class Questionaire extends Model {

    private static final long serialVersionUID = 1L;

    @Id
    @TableGenerator(name = "questionaire_id_generator", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "questionaire_seq", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_status_id_gen")
    Long id;

    @Convert(converter = LocalizedAttributeConverter.class)
    private LocalizedString name;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public LocalizedString getName() {
        return this.name;
    }

    public void setName(LocalizedString name) {
        this.name = name;
    }
}
