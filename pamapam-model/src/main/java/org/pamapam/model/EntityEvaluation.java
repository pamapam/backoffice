package org.pamapam.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "entity_evaluation")
@Cacheable(false)
public class EntityEvaluation extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "entity_evaluation_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_evaluation_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_evaluation_id_gen")
	private Long id;

	@Column(name = "date")
	@Temporal(TemporalType.DATE)
	private Date date;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private PamapamUser user;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "entityEvaluation", cascade = CascadeType.ALL)
	private List<EntityEvaluationCriterion> entityEvaluationCriterions;

	public EntityEvaluation() {
		super();
	}

	public EntityEvaluation(EntityEvaluation source, SocialEconomyEntity entity) {
		this();
		this.date = source.date;
		this.user = source.user;
		List<EntityEvaluationCriterion> newEntityEvaluationCriterions = new ArrayList<>();
		source.entityEvaluationCriterions.forEach(each -> newEntityEvaluationCriterions.add(new EntityEvaluationCriterion(each, this)));
		this.entityEvaluationCriterions = newEntityEvaluationCriterions;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public PamapamUser getUser() {
		return this.user;
	}

	public void setUser(PamapamUser user) {
		this.user = user;
	}

	public List<EntityEvaluationCriterion> getEntityEvaluationCriterions() {
		return this.entityEvaluationCriterions;
	}

	public void setEntityEvaluationCriterions(List<EntityEvaluationCriterion> entityEvaluationCriterions) {
		this.entityEvaluationCriterions = entityEvaluationCriterions;
	}

	/*
	 * ... Returns all the evaluation criterions with level >= 1
	 */

	public List<Criterion> getAccomplishedCriteria() {
		return this.getEntityEvaluationCriterions().stream()
			.filter(each -> Optional.ofNullable(each.getLevel()).orElse(0) >= 1)
			.map(each -> each.getCriterion())
			.collect(Collectors.toList());
	}
}
