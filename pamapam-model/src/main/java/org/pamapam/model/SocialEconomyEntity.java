package org.pamapam.model;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.*;
import org.pamapam.model.enums.FescTimeSlot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "social_economy_entity")
@Cacheable(false)
public class SocialEconomyEntity extends Model {
    private static final long serialVersionUID = 1L;
    @Transient
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Id
    @TableGenerator(name = "social_economy_entity_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "social_economy_entity_seq", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "social_economy_entity_id_gen")
    private Long id;

    @Column
    private String nif;

    @Column(nullable = false)
    private String name;

    @Column(name = "normalized_name")
    private String normalizedName;

    @Column
    private Integer foundationYear; //balanç creationYear

    @Convert(converter = LocalizedAttributeConverter.class)
    @Column(length = 2048)
    private LocalizedString description;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id")
    private BinaryResource picture;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "legal_form_id")
    private LegalForm legalForm;

    @Column(name = "contact_person")
    private String contactPerson;

    @Column
    private String address;

    @Column(name = "postal_code")
    private String postalCode;

    @Column
    private String web;

    @Column(name = "online_shopping_web")
    private String onlineShoppingWeb;

    @Column(name = "la_zona_web")
    private String laZonaWeb;

    @Column
    private String email;

    @Column
    private String phone;

    @Column
    private String twitter;

    @Column
    private String facebook;

    @Column
    private String instagram;

    @Column
    private String pinterest;

    @Column
    private String quitter;

    @Column(name = "opening_hours", length = 1024)
    private String openingHours;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "main_sector_id")
    private Sector mainSector;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_sector", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "sector_id", referencedColumnName = "id")})
    private Set<Sector> sectors;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sector_ccae_id")
    private SectorCcae sectorCcae;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "province_id")
    private Province province;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id")
    private Region region;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "town_id")
    private Town town;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district_id")
    private District district;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "neighborhood_id")
    private Neighborhood neighborhood;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_social_economy_network", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "social_economy_network_id", referencedColumnName = "id")})
    private Set<SocialEconomyNetwork> socialEconomyNetworks;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_social_economy_network_tag", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "social_economy_network_tag_id", referencedColumnName = "id")})
    private Set<SocialEconomyNetworkTag> otherSocialEconomyNetworks;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_collaboration_entity", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "collaboration_entity_id", referencedColumnName = "id")})
    private Set<CollaborationEntity> collaborationEntities;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_product_tag", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "product_tag_id", referencedColumnName = "id")})
    private Set<ProductTag> productTag;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_keyword_tag", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "keyword_tag_id", referencedColumnName = "id")})
    private Set<KeywordTag> keywordTags;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_external_filter_tag", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "external_filter_tag_id", referencedColumnName = "id")})
    private Set<ExternalFilterTag> externalFilterTags;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "entity_entity_scope", joinColumns = {@JoinColumn(name = "entity_id")}, inverseJoinColumns = {@JoinColumn(name = "entity_scope_id", referencedColumnName = "id")})
    private Set<EntityScope> entityScopes;

    @Column(name = "xes_balance")
    private Boolean xesBalance; // si viene de balanç, sí

    @Column(name = "green_commerce")
    private Boolean greenCommerce; 

    @Column(name = "public_administration")
    private Boolean publicAdministration;

    @Column(name = "enterprises")
    private Boolean enterprises;

    @Column(name = "private_activists")
    private Boolean privateActivists;

    @Column(name = "private_noobs")
    private Boolean privateNoobs;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "language_id")
    private Language language;

    @Column(name = "registry_date")
    @Temporal(TemporalType.DATE)
    private Date registryDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "registry_user_id")
    private PamapamUser registryUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "initiative_user_id")
    private PamapamUser initiativeUser;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "entity", cascade = CascadeType.ALL)
    private List<EntityPersonRole> entityPersonRoles;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "entity_evaluation_id")
    private EntityEvaluation entityEvaluation;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "entity_status_id")
    private EntityStatus entityStatus;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "entity", cascade = CascadeType.ALL)
    private List<EntityStatusChange> entityStatusChanges;

    @Column(name = "last_update_date")
    private Timestamp lastUpdateDate;

    @Column(name = "global_id")
    private Long globalId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "entity", cascade = CascadeType.ALL)
    private List<SocialEconomyEntityNote> notes;

    @Column(name = "old_version")
    private Boolean oldVersion;

    @Column
    private BigDecimal invoicing;

    @Column(name = "custom_fields")
    private String customFields;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "social_economy_entity_product", joinColumns = @JoinColumn(name = "id_social_economy_entity"), inverseJoinColumns = @JoinColumn(name = "id_binary_resource"))
    private List<BinaryResource> productPictures;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "entity", orphanRemoval = true)
    private List<EntityDiscount> discounts;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "entity", orphanRemoval = true)
    private List<EntityAudiovisual> audiovisualDocuments;

    // FESC  fields
    @OneToOne(mappedBy = "see", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private SocialEconomyEntityFesc fescData;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_office_id")
    private SocialEconomyEntity mainOffice;

    @Column(name = "user_conditions_accepted")
    private Boolean userConditionsAccepted;
    @Column(name = "user_conditions_date")
    private Timestamp userConditionsDate;

    @Column(name = "newsletters_accepted")
    private Boolean newslettersAccepted;
    @Column(name = "newsletters_date")
    private Timestamp newslettersDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ideological_identification_id")
    private IdeologicalIdentification ideologicalIdentification;

    @ManyToOne
    @JoinColumn(name = "questionaire_id")
    private Questionaire questionaire;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "entity", orphanRemoval = true)
    private List<TextUrl> intercooperacioLinks;

    public List<TextUrl> getIntercooperacioLinks() {
        return this.intercooperacioLinks;
    }

    public void setIntercooperacioLinks(final List<TextUrl> intercooperacioLinks) {
        this.intercooperacioLinks = intercooperacioLinks;
    }

    /**
     * Url linking Pam a Pam SocialEconomyEntity with
     * XES' Balanç Social results: infography or pdf results.
     */
    @Column(name = "url_balanc_social")
    private String urlBalancSocial;

    public SocialEconomyEntity() {
        super();
        this.notes = new ArrayList<>();
        this.oldVersion = false;
    }

    public SocialEconomyEntity(final SocialEconomyEntity source) {
        this();
        this.nif = source.nif;
        this.name = source.name;
        this.foundationYear = source.foundationYear;
        this.description = source.description;
        Optional.ofNullable(source.picture).ifPresent(o -> this.picture = new BinaryResource(o));
        this.legalForm = source.legalForm;
        this.contactPerson = source.contactPerson;
        this.address = source.address;
        this.postalCode = source.postalCode;
        this.web = source.web;
        this.onlineShoppingWeb = source.onlineShoppingWeb;
        this.laZonaWeb = source.laZonaWeb;
        this.email = source.email;
        this.phone = source.phone;
        this.twitter = source.twitter;
        this.facebook = source.facebook;
        this.instagram = source.instagram;
        this.pinterest = source.pinterest;
        this.quitter = source.quitter;
        this.openingHours = source.openingHours;
        this.sectors = source.sectors;
        this.mainSector = source.mainSector;
        this.sectorCcae = source.sectorCcae;
        this.province = source.province;
        this.region = source.region;
        this.town = source.town;
        this.district = source.district;
        this.neighborhood = source.neighborhood;
        this.socialEconomyNetworks = source.socialEconomyNetworks;
        this.otherSocialEconomyNetworks = source.otherSocialEconomyNetworks;
        this.collaborationEntities = source.collaborationEntities;
        this.productTag = source.productTag;
        this.keywordTags = source.keywordTags;
        this.externalFilterTags = source.externalFilterTags;
        this.entityScopes = source.entityScopes;
        this.xesBalance = source.xesBalance;
        this.publicAdministration = source.publicAdministration;
        this.enterprises = source.enterprises;
        this.privateActivists = source.privateActivists;
        this.privateNoobs = source.privateNoobs;
        this.language = source.language;
        this.registryDate = source.registryDate;
        this.registryUser = source.registryUser;
        this.initiativeUser = source.initiativeUser;
        final List<EntityPersonRole> newEntityPersonRoles = new ArrayList<>();
        source.entityPersonRoles.forEach(each -> newEntityPersonRoles.add(new EntityPersonRole(each, this)));
        this.entityPersonRoles = newEntityPersonRoles;
        this.entityEvaluation = Optional.ofNullable(source.entityEvaluation)
                .map(sourceEntityEvaluation -> new EntityEvaluation(sourceEntityEvaluation, this))
                .orElse(null);
        this.latitude = source.latitude;
        this.longitude = source.longitude;
        this.entityStatus = source.entityStatus;
        this.entityStatusChanges = source.entityStatusChanges;
        this.normalizedName = source.normalizedName;
        this.globalId = source.globalId;
        this.oldVersion = source.oldVersion;
        this.invoicing = source.invoicing;
        this.customFields = source.customFields;
        this.productPictures = new ArrayList<>();
        Optional.ofNullable(source.getProductPictures()).ifPresent(o -> o.forEach(each -> {
            // ...	FIXME: Current version of jamgo framework copies also id and version.
            final BinaryResource productPictureCopy = new BinaryResource(each);
            productPictureCopy.setId(null);
            productPictureCopy.setVersion(null);
            this.productPictures.add(productPictureCopy);
        }));
        this.discounts = new ArrayList<>();
        Optional.ofNullable(source.getDiscounts()).ifPresent(o -> o.forEach(each -> this.discounts.add(new EntityDiscount(each, this))));
        Optional.ofNullable(source.fescData).ifPresent(o -> this.fescData = new SocialEconomyEntityFesc(o, this));
        this.mainOffice = source.mainOffice;
        this.userConditionsAccepted = source.userConditionsAccepted;
        this.newslettersAccepted = source.newslettersAccepted;
        this.ideologicalIdentification = source.ideologicalIdentification;
        this.urlBalancSocial = source.urlBalancSocial;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public String getNif() {
        return this.nif;
    }

    public void setNif(final String nif) {
        this.nif = nif;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Integer getFoundationYear() {
        return this.foundationYear;
    }

    public void setFoundationYear(final Integer foundationYear) {
        this.foundationYear = foundationYear;
    }

    public String getNormalizedName() {
        return this.normalizedName;
    }

    public void setNormalizedName(final String normalizedName) {
        this.normalizedName = normalizedName;
    }

    public LocalizedString getDescription() {
        return this.description;
    }

    public void setDescription(final LocalizedString description) {
        this.description = description;
    }

    public BinaryResource getPicture() {
        return this.picture;
    }

    public void setPicture(final BinaryResource picture) {
        this.picture = picture;
    }

    public LegalForm getLegalForm() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getLegalForm();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.legalForm;
        } else {
            return this.legalForm;
        }
    }

    public void setLegalForm(final LegalForm legalForm) {
        this.legalForm = legalForm;
    }

    public String getContactPerson() {
        return this.contactPerson;
    }

    public void setContactPerson(final String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    public String getWeb() {
        return this.web;
    }

    public void setWeb(final String web) {
        this.web = web;
    }

    public String getOnlineShoppingWeb() {
        return this.onlineShoppingWeb;
    }

    public void setOnlineShoppingWeb(final String onlineShoppingWeb) {
        this.onlineShoppingWeb = onlineShoppingWeb;
    }

    public String getLaZonaWeb() {
        return this.laZonaWeb;
    }

    public void setLaZonaWeb(final String laZonaWeb) {
        this.laZonaWeb = laZonaWeb;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getTwitter() {
        return this.twitter;
    }

    public void setTwitter(final String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return this.facebook;
    }

    public void setFacebook(final String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return this.instagram;
    }

    public void setInstagram(final String instagram) {
        this.instagram = instagram;
    }

    public String getPinterest() {
        return this.pinterest;
    }

    public void setPinterest(final String pinterest) {
        this.pinterest = pinterest;
    }

    public String getQuitter() {
        return this.quitter;
    }

    public void setQuitter(final String quitter) {
        this.quitter = quitter;
    }

    public String getOpeningHours() {
        return this.openingHours;
    }

    public void setOpeningHours(final String openingHours) {
        this.openingHours = openingHours;
    }

    public Sector getMainSector() {
        if(this.mainSector != null) {
            return this.mainSector;
        }
        if (this.getMainOffice() != null && !this.mainOffice.equals(this)) {
            return this.getMainOfficeSector();
        }
        return null;
    }

    public Sector getMainOfficeSector() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getMainSector();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.mainSector;
        } else {
            return this.mainSector;
        }
    }

    public Boolean isMainSectorEqualToMainOfficeMainSector() {
        return this.getMainOffice().getMainSector().equals(this.getMainSector());
    }

    public void setMainSector(final Sector mainSector) {
        this.mainSector = mainSector;
    }

    public Set<Sector> getSectors() {
        return this.sectors;
    }

    public Set<Sector> getMainOfficeSectors() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getSectors();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.sectors;
        } else {
            return this.sectors;
        }
    }

    public Boolean areSectorsEqualToMainOfficeSectors() {
        Set<Sector> difference = Sets.symmetricDifference(this.getMainOffice().getSectors(), this.getSectors());
        return difference.isEmpty();
    }

    public void setSectors(final Set<Sector> sectors) {
        this.sectors = sectors;
    }

    public SectorCcae getSectorCcae() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getSectorCcae();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.sectorCcae;
        } else {
            return this.sectorCcae;
        }
    }

    public void setSectorCcae(final SectorCcae sectorCcae) {
        this.sectorCcae = sectorCcae;
    }

    public Province getProvince() {
        return this.province;
    }

    public void setProvince(final Province province) {
        this.province = province;
    }

    public Region getRegion() {
        return this.region;
    }

    public void setRegion(final Region region) {
        this.region = region;
    }

    public Town getTown() {
        return this.town;
    }

    public void setTown(final Town town) {
        this.town = town;
    }

    public District getDistrict() {
        return this.district;
    }

    public void setDistrict(final District district) {
        this.district = district;
    }

    public Neighborhood getNeighborhood() {
        return this.neighborhood;
    }

    public void setNeighborhood(final Neighborhood neighborhood) {
        this.neighborhood = neighborhood;
    }

    public Set<SocialEconomyNetwork> getSocialEconomyNetworks() {
        return this.socialEconomyNetworks;
    }

    public void setSocialEconomyNetworks(final Set<SocialEconomyNetwork> socialEconomyNetworks) {
        this.socialEconomyNetworks = socialEconomyNetworks;
    }

    public Set<SocialEconomyNetworkTag> getOtherSocialEconomyNetworks() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getOtherSocialEconomyNetworks();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.otherSocialEconomyNetworks;
        } else {
            return this.otherSocialEconomyNetworks;
        }
    }

    public void setOtherSocialEconomyNetworks(final Set<SocialEconomyNetworkTag> otherSocialEconomyNetworks) {
        this.otherSocialEconomyNetworks = otherSocialEconomyNetworks;
    }

    public Set<CollaborationEntity> getCollaborationEntities() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getCollaborationEntities();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.collaborationEntities;
        } else {
            return this.collaborationEntities;
        }
    }

    public void setCollaborationEntities(final Set<CollaborationEntity> collaborationEntities) {
        this.collaborationEntities = collaborationEntities;
    }

    public Set<ProductTag> getProductTag() {
        return this.productTag;
    }

    public void setProductTag(final Set<ProductTag> productTag) {
        this.productTag = productTag;
    }

    public Set<KeywordTag> getKeywordTags() {
        return this.keywordTags;
    }

    public void setKeywordTags(final Set<KeywordTag> keywordTags) {
        this.keywordTags = keywordTags;
    }

    public Set<ExternalFilterTag> getExternalFilterTags() {
        if (this.externalFilterTags == null) {
            return this.getMainOfficeExternalFilterTags();
        } else {
            return this.externalFilterTags;
        }
    }

    public Boolean areExternalFilterTagsEqualToMainOfficeExternalFilterTags() {
        Set<ExternalFilterTag> difference = Sets.symmetricDifference(this.getMainOffice().getExternalFilterTags(), this.getExternalFilterTags());
        return difference.isEmpty();
    }

    private Set<ExternalFilterTag> getMainOfficeExternalFilterTags() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getExternalFilterTags();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.externalFilterTags;
        } else {
            return this.externalFilterTags;
        }
    }

    public void setExternalFilterTags(final Set<ExternalFilterTag> externalFilterTags) {
        this.externalFilterTags = externalFilterTags;
    }

    public Set<EntityScope> getEntityScopes() {
        if (this.entityScopes == null) {
            return this.getMainOfficeEntityScopes();
        } else {
            return this.entityScopes;
        }
    }

    public Boolean areEntityScopesEqualToMainOfficeEntityScopes() {
        Set<EntityScope> difference = Sets.symmetricDifference(this.getMainOffice().getEntityScopes(), this.getEntityScopes());
        return difference.isEmpty();
    }

    public Set<EntityScope> getMainOfficeEntityScopes() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.mainOffice.getEntityScopes();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.entityScopes;
        } else {
            return this.entityScopes;
        }
    }

    public void setEntityScopes(final Set<EntityScope> entityScopes) {
        this.entityScopes = entityScopes;
    }

    public String getEntityScopesString() {
        String scopesString = "";
        if (CollectionUtils.isNotEmpty(this.getEntityScopes())) {
            scopesString = this.getEntityScopes().stream()
                    .map(each -> each.getName())
                    .sorted()
                    .collect(Collectors.joining(", "));
        }
        return scopesString;
    }

    public Boolean getXesBalance() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getXesBalance();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.xesBalance;
        } else {
            return this.xesBalance;
        }
    }

    public void setXesBalance(final Boolean xesBalance) {
        this.xesBalance = xesBalance;
    }

    public Boolean getGreenCommerce() {
		return greenCommerce;
	}

	public void setGreenCommerce(Boolean greenCommerce) {
		this.greenCommerce = greenCommerce;
	}

	public Boolean getPublicAdministration() {
        return this.publicAdministration;
    }

    public void setPublicAdministration(final Boolean publicAdministration) {
        this.publicAdministration = publicAdministration;
    }

    public Boolean isOldVersion() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().isOldVersion();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return Boolean.TRUE.equals(this.oldVersion);
        } else {
            return Boolean.TRUE.equals(this.oldVersion);
        }
    }

    public void setOldVersion(final Boolean oldVersion) {
        this.oldVersion = oldVersion;
    }

    public Language getLanguage() {
        return this.language;
    }

    public void setLanguage(final Language language) {
        this.language = language;
    }

    public Date getRegistryDate() {
        return this.registryDate;
    }

    public void setRegistryDate(final Date registryDate) {
        this.registryDate = registryDate;
    }

    public Date getLastPublishedDate() {
        final List<EntityStatusChange> publishedChanges = this.getEntityStatusChanges().stream()
                .filter(change -> change.getEntityStatusTo().isPublished())
                .sorted(EntityStatusChange.TIME_ORDER)
                .collect(Collectors.toList());

        return publishedChanges.get(publishedChanges.size() - 1).getChangeTimestamp();
    }

    public List<EntityPersonRole> getEntityPersonRoles() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getEntityPersonRoles();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.entityPersonRoles;
        } else {
            return this.entityPersonRoles;
        }
    }

    public void setEntityPersonRoles(final List<EntityPersonRole> entityPersonRoles) {
        this.entityPersonRoles = entityPersonRoles;
    }

    public EntityEvaluation getEntityEvaluation() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getEntityEvaluation();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.entityEvaluation;
        } else {
            return this.entityEvaluation;
        }
    }

    public void setEntityEvaluation(final EntityEvaluation entityEvaluation) {
        this.entityEvaluation = entityEvaluation;
    }

    public EntityStatus getEntityStatus() {
        return this.entityStatus;
    }

    public void setEntityStatus(final EntityStatus entityStatus) {
        this.entityStatus = entityStatus;
    }

    public PamapamUser getRegistryUser() {
        return this.registryUser;
    }

    public void setRegistryUser(final PamapamUser registryUser) {
        this.registryUser = registryUser;
    }

    public PamapamUser getInitiativeUser() {
        return this.initiativeUser;
    }

    public void setInitiativeUser(final PamapamUser initiativeUser) {
        this.initiativeUser = initiativeUser;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public Long getGlobalId() {
        return this.globalId;
    }

    public void setGlobalId(final Long globalId) {
        this.globalId = globalId;
    }

    public List<SocialEconomyEntityNote> getNotes() {
        return this.notes;
    }

    public void setNotes(final List<SocialEconomyEntityNote> notes) {
        this.notes = notes;
    }

    public BigDecimal getInvoicing() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getInvoicing();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.invoicing;
        } else {
            return this.invoicing;
        }
    }

    public void setInvoicing(final BigDecimal invoicing) {
        this.invoicing = invoicing;
    }

    public Boolean belongsToSector(final Sector sector) {
        return this.sectors.stream().anyMatch(each -> each.belongsToSector(sector));
    }

    public String getColor() {
        return this.getEntityStatus().getColor();
    }

    public void updateGlobalId() {
        if (this.globalId == null) {
            this.globalId = RandomUtils.nextLong();
        }
    }

    public List<EntityStatusChange> getEntityStatusChanges() {
        if (this.entityStatusChanges == null) {
            this.entityStatusChanges = new ArrayList<>();
        }
        return this.entityStatusChanges;
    }

    public void addEntityStatusChange(final EntityStatusChange entityStatusChange) {
        entityStatusChange.setEntity(this);
        this.getEntityStatusChanges().add(entityStatusChange);
    }

    public Date getCurrentStatusDate() {
        final List<EntityStatusChange> currentStatusChanges = this.getEntityStatusChanges();
//		.stream()
//			.filter(each -> each.getEntityStatusTo().getEntityStatusType() == this.entityStatus.getEntityStatusType())
//			.collect(Collectors.toList());
        Collections.sort(currentStatusChanges, EntityStatusChange.TIME_ORDER);
        return currentStatusChanges.get(currentStatusChanges.size() - 1).getChangeTimestamp();
    }

    public List<Criterion> getAccomplishedCriteria() {
        final List<Criterion> criteria = Optional.ofNullable(this.getEntityEvaluation()).map(obj -> obj.getAccomplishedCriteria()).orElse(new ArrayList<>());
        Collections.sort(criteria, Criterion.VIEW_ORDER);
        return criteria;
    }

    public String getLastTerritoryName() {
        if (this.neighborhood != null) {
            return this.neighborhood.getName().getDefaultText();
        } else if (this.district != null) {
            return this.district.getName().getDefaultText();
        } else if (this.town != null) {
            return this.town.getName().getDefaultText();
        } else if (this.region != null) {
            return this.region.getName().getDefaultText();
        } else if (this.province != null) {
            return this.province.getName().getDefaultText();
        } else {
            return "";
        }
    }

    public String getCustomFields() {
        if (this.mainOffice != null && !this.mainOffice.equals(this)) {
            return this.getMainOffice().getCustomFields();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            return this.customFields;
        } else {
            return this.customFields;
        }
    }

    public void setCustomFields(final String customFields) {
        this.customFields = customFields;
    }

    public Timestamp getLastUpdateDate() {
        return this.lastUpdateDate;
    }

    public void setLastUpdateDate(final Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public SocialEconomyEntityFesc getFescData() {
        if (this.mainOffice != null && !this.mainOffice.equals(this) && this.fescData != null) {
            return this.getMainOffice().getFescData();
        } else if (Objects.equals(this.mainOffice, this)) {
            this.logger.error("mainOffice cannot be the same as socialeconomy entity with id " + this.getId());
            if (this.fescData == null) {
                this.fescData = new SocialEconomyEntityFesc();
                this.fescData.setSee(this);
            }
            return this.fescData;
        } else {
            if (this.fescData == null) {
                this.fescData = new SocialEconomyEntityFesc();
                this.fescData.setSee(this);
            }
            return this.fescData;
        }
    }

    public void setFescData(final SocialEconomyEntityFesc fescData) {
        if (this.fescData != null) {
            this.fescData = fescData;
        }
    }

    // Delegate methods for fescData

    public EntityStatus getFescPreviousEntityStatus() {
        return Optional.ofNullable(this.getFescData()).map(fescData -> fescData.getPreviousEntityStatus()).orElse(null);
    }

    public void setFescPreviousEntityStatus(final EntityStatus entityStatus) {
        if (this.fescData != null) {
            this.getFescData().setPreviousEntityStatus(entityStatus);
        }
    }

    public EntityStatus getFescEntityStatus() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getEntityStatus).orElse(null);
    }

    public void setFescEntityStatus(final EntityStatus entityStatus) {
        this.getFescData().setEntityStatus(entityStatus);
    }

    public String getFescEmail() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getEmail).orElse(null);
    }

    public void setFescEmail(final String email) {
        this.getFescData().setEmail(email);
    }

    public String getFescVideo() {
        return Optional.ofNullable(this.getFescData()).map(fescData -> fescData.getVideo()).orElse(null);
    }

    public void setFescVideo(final String video) {
        if (video != null) {
            this.getFescData().setVideo(video);
        }
    }

    public LocalizedString getFescOffer() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getOffer).orElse(null);
    }

    public void setFescOffer(final LocalizedString offer) {
        this.getFescData().setOffer(offer);
    }

    public String getFescInvoiceName() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getInvoiceName).orElse(null);
    }

    public void setFescInvoiceName(final String invoiceName) {
        this.getFescData().setInvoiceName(invoiceName);
    }

    public String getFescInvoiceEmail() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getInvoiceEmail).orElse(null);
    }

    public void setFescInvoiceEmail(final String invoiceEmail) {
        this.getFescData().setInvoiceEmail(invoiceEmail);
    }

    public String getFescInvoiceAddress() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getInvoiceAddress).orElse(null);
    }

    public void setFescInvoiceAddress(final String invoiceAddress) {
        this.getFescData().setInvoiceAddress(invoiceAddress);
    }

    public String getFescInvoicePostalCode() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getInvoicePostalCode).orElse(null);
    }

    public void setFescInvoicePostalCode(final String invoicePostalCode) {
        this.getFescData().setInvoicePostalCode(invoicePostalCode);
    }

    public FescRegistrationModality getModality() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getModality).orElse(null);
    }

    public void setModality(final FescRegistrationModality modality) {
        this.getFescData().setModality(modality);
    }

    public String getFescModalityName() {
        return Optional.ofNullable(this.getFescData())
                .map(SocialEconomyEntityFesc::getModality)
                .map(FescRegistrationModality::getName)
                .map(LocalizedString::getDefaultText)
                .orElse("");
    }

    public String getFellowName() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getFellowName).orElse(null);
    }

    public void setFellowName(final String fellowName) {
        this.getFescData().setFellowName(fellowName);
    }

    public Integer getFescPreviousFee() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getPreviousFee).orElse(null);
    }

    public void setFescPreviousFee(final Integer fee) {
        this.getFescData().setPreviousFee(fee);
    }

    public Integer getFescFee() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getFee).orElse(null);
    }

    public void setFescFee(final Integer fee) {
        this.getFescData().setFee(fee);
    }

    public String getFescInvoiceAccount() {
        return Optional.ofNullable(this.getFescData()).map(SocialEconomyEntityFesc::getInvoiceAccount).orElse(null);
    }

    public void setFescInvoiceAccount(final String invoiceAccount) {
        this.getFescData().setInvoiceAccount(invoiceAccount);
    }

    public FescTimeSlot getFescTimeSlot() {
        return Optional.ofNullable(this.getFescData())
                .map(SocialEconomyEntityFesc::getTimeSlot)
                .orElse(null);
    }

    public void setFescTimeSLot(final FescTimeSlot fescTimeSlot) {
        if (this.fescData != null) {
            this.fescData.setTimeSlot(fescTimeSlot);
        }
    }

    public String getFescTimeSlotNameReference() {
        String reference = "";
        final FescTimeSlot timeSlot = Optional.ofNullable(this.getFescData())
                .map(SocialEconomyEntityFesc::getTimeSlot)
                .orElse(null);

        if (timeSlot != null) {
            reference = "fesc.registration.invoice.timeSlot." + timeSlot.toString();
        }

        return reference;
    }

    public List<BinaryResource> getProductPictures() {
        return this.productPictures;
    }

    public void setProductPictures(final List<BinaryResource> productPictures) {
        this.productPictures = productPictures;
    }

    public List<EntityDiscount> getDiscounts() {
        return this.discounts;
    }

    public void setDiscounts(final List<EntityDiscount> discounts) {
        this.discounts = discounts;
    }

    public List<EntityAudiovisual> getAudiovisualDocuments() {
        return this.audiovisualDocuments;
    }

    public void setAudiovisualDocuments(final List<EntityAudiovisual> audiovisualDocuments) {
        this.audiovisualDocuments = audiovisualDocuments;
    }

    public Boolean getEnterprises() {
        return this.enterprises;
    }

    public void setEnterprises(final Boolean enterprises) {
        this.enterprises = enterprises;
    }

    public Boolean getPrivateActivists() {
        return this.privateActivists;
    }

    public void setPrivateActivists(final Boolean privateActivists) {
        this.privateActivists = privateActivists;
    }

    public Boolean getPrivateNoobs() {
        return this.privateNoobs;
    }

    public void setPrivateNoobs(final Boolean privateNoobs) {
        this.privateNoobs = privateNoobs;
    }

    public SocialEconomyEntity getMainOffice() {
        return this.mainOffice;
    }

    public void setMainOffice(final SocialEconomyEntity mainOffice) {
        this.mainOffice = mainOffice;
    }

    public boolean isMainOffice() {
        return this.mainOffice == null;
    }

    public void updateFromMainOffice(final SocialEconomyEntity mainOffice) {
        this.oldVersion = mainOffice.oldVersion;
        this.legalForm = mainOffice.legalForm;
        this.mainSector = mainOffice.mainSector;
        this.sectors = mainOffice.sectors;
        this.sectorCcae = mainOffice.sectorCcae;
        this.otherSocialEconomyNetworks = mainOffice.otherSocialEconomyNetworks;
        this.collaborationEntities = mainOffice.collaborationEntities;
        this.externalFilterTags = mainOffice.externalFilterTags;
        this.entityScopes = mainOffice.getEntityScopes();
        this.xesBalance = mainOffice.xesBalance;
        final List<EntityPersonRole> newEntityPersonRoles = new ArrayList<>();
        mainOffice.entityPersonRoles.forEach(each -> newEntityPersonRoles.add(new EntityPersonRole(each, this)));
        this.entityPersonRoles = newEntityPersonRoles;
        this.invoicing = mainOffice.invoicing;
        this.customFields = mainOffice.customFields;
        this.entityEvaluation = new EntityEvaluation(mainOffice.entityEvaluation, this);
        this.fescData = new SocialEconomyEntityFesc(mainOffice.fescData, this);
        this.ideologicalIdentification = mainOffice.ideologicalIdentification;
    }

    public Boolean getUserConditionsAccepted() {
        return this.userConditionsAccepted;
    }

    public void setUserConditionsAccepted(final Boolean userConditionsAccepted) {
        this.userConditionsAccepted = userConditionsAccepted;
    }

    public Timestamp getUserConditionsDate() {
        return this.userConditionsDate;
    }

    public void setUserConditionsDate(final Timestamp userConditionsDate) {
        this.userConditionsDate = userConditionsDate;
    }

    public Boolean getNewslettersAccepted() {
        return this.newslettersAccepted;
    }

    public void setNewslettersAccepted(final Boolean newslettersAccepted) {
        this.setNewslettersDate(Timestamp.from(Instant.now()));
        this.newslettersAccepted = newslettersAccepted;
    }

    public Timestamp getNewslettersDate() {
        return this.newslettersDate;
    }

    public void setNewslettersDate(final Timestamp newslettersDate) {
        this.newslettersDate = newslettersDate;
    }

    public IdeologicalIdentification getIdeologicalIdentification() {
        return this.ideologicalIdentification;
    }

    public void setIdeologicalIdentification(final IdeologicalIdentification ideologicalIdentification) {
        this.ideologicalIdentification = ideologicalIdentification;
    }

    public Questionaire getQuestionaire() {
        return this.questionaire;
    }

    public void setQuestionaire(final Questionaire questionaire) {
        this.questionaire = questionaire;
    }

    public Sector getFescMainSector() {
        return Optional.ofNullable(this.fescData)
                .map(SocialEconomyEntityFesc::getMainSector)
                .orElse(null);
    }

    public void setFescMainSector(final Sector sector) {
        if (this.fescData != null) {
            this.fescData.setMainSector(sector);
        }
    }

    public boolean getFescRegistrationPrivacyPolicy() {
        return Optional.ofNullable(this.fescData)
                .map(o -> o.getAcceptedRegistrationPrivacyPolicy())
                .orElse(false);
    }

    public void setFescRegistrationPrivacyPolicy(final Boolean accepted) {
        if (this.fescData != null) {
            this.fescData.setAcceptedRegistrationPrivacyPolicy(accepted);
        }
    }

    public boolean getFescParticipationPrivacyPolicy() {
        return Optional.ofNullable(this.fescData)
                .map(o -> o.getAcceptedParticipationPrivacyPolicy())
                .orElse(false);
    }

    public Timestamp getFescParticipationPrivacyPolicyDate() {
        return Optional.ofNullable(this.fescData)
                .map(o -> o.getAcceptedParticipationPrivacyPolicyDate())
                .orElse(null);
    }

    public void setFescParticipationPrivacyPolicyDate(final Timestamp date) {
        if (this.fescData != null) {
            this.fescData.setAcceptedParticipationPrivacyPolicyDate(date);
        }
    }

    public void setFescRegistrationPrivacyPolicyDate(final Timestamp date) {
        if (this.fescData != null) {
            this.fescData.setAcceptedRegistrationPrivacyPolicyDate(date);
        }
    }

    public void setFescParticipationPrivacyPolicy(final Boolean accepted) {
        if (this.fescData != null) {
            this.fescData.setAcceptedParticipationPrivacyPolicy(accepted);
        }
    }

    public Timestamp getFescRegistrationPrivacyPolicyDate() {
        return Optional.ofNullable(this.fescData)
                .map(o -> o.getAcceptedRegistrationPrivacyPolicyDate())
                .orElse(null);
    }

    public Boolean isIntercooperacio() {
        return this.entityScopes.stream()
                .anyMatch(EntityScope::isIntercooperacio);
    }

    public String getUrlBalancSocial() {
        return this.urlBalancSocial;
    }

    public void setUrlBalancSocial(final String urlBalancSocial) {
        this.urlBalancSocial = urlBalancSocial;
    }

    @Override
    public String toString() {
        final String status = Optional.ofNullable(this.entityStatus)
                .map(EntityStatus::getName)
                .map(LocalizedString::getDefaultText)
                .orElse("");

        return status + " - " + Optional.ofNullable(this.name).orElse("") + " - " + Optional.ofNullable(this.nif).orElse("");
    }
}
