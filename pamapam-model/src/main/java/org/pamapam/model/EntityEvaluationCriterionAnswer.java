package org.pamapam.model;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "entity_evaluation_answer", uniqueConstraints = @UniqueConstraint(columnNames = { "entity_evaluation_id", "criterion_answer_id" }))
@Cacheable(false)
public class EntityEvaluationCriterionAnswer implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final Comparator<EntityEvaluationCriterionAnswer> ANSWER_ORDER = new Comparator<EntityEvaluationCriterionAnswer>() {
		@Override
		public int compare(EntityEvaluationCriterionAnswer objectA, EntityEvaluationCriterionAnswer objectB) {
			return objectA.getCriterionAnswer().getAnswerOrder().compareTo(objectB.getCriterionAnswer().getAnswerOrder());
		}
	};

	@Id
	@TableGenerator(name = "entity_evaluation_answer_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_evaluation_answer_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_evaluation_answer_id_gen")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "entity_evaluation_criterion_id")
	private EntityEvaluationCriterion entityEvaluationCriterion;

	@ManyToOne
	@JoinColumn(name = "criterion_answer_id")
	private CriterionAnswer criterionAnswer;

	@Column
	private Integer value;

	public EntityEvaluationCriterionAnswer() {
		super();
	}

	public EntityEvaluationCriterionAnswer(EntityEvaluationCriterion entityEvaluationCriterion, CriterionAnswer criterionAnswer, Integer value) {
		this();
		this.entityEvaluationCriterion = entityEvaluationCriterion;
		this.criterionAnswer = criterionAnswer;
		this.value = value;
	}

	public EntityEvaluationCriterionAnswer(EntityEvaluationCriterionAnswer source, EntityEvaluationCriterion entityEvaluationCriterion) {
		this();
		this.entityEvaluationCriterion = entityEvaluationCriterion;
		this.criterionAnswer = source.criterionAnswer;
		this.value = source.value;
	}

	public EntityEvaluationCriterion getEntityEvaluationCriterion() {
		return this.entityEvaluationCriterion;
	}

	public void setEntityEvaluationCriterion(EntityEvaluationCriterion entityEvaluationCriterion) {
		this.entityEvaluationCriterion = entityEvaluationCriterion;
	}

	public CriterionAnswer getCriterionAnswer() {
		return this.criterionAnswer;
	}

	public void setCriterion(CriterionAnswer criterionAnswer) {
		this.criterionAnswer = criterionAnswer;
	}

	public Integer getValue() {
		return this.value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}

		EntityEvaluationCriterionAnswer that = (EntityEvaluationCriterionAnswer) o;

		if (this.entityEvaluationCriterion != null ? !this.entityEvaluationCriterion.equals(that.entityEvaluationCriterion) : that.entityEvaluationCriterion != null) {
			return false;
		}
		if (this.criterionAnswer != null ? !this.criterionAnswer.equals(that.criterionAnswer) : that.criterionAnswer != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (this.entityEvaluationCriterion != null ? this.entityEvaluationCriterion.hashCode() : 0);
		result = 31 * result + (this.criterionAnswer != null ? this.criterionAnswer.hashCode() : 0);
		return result;
	}

}
