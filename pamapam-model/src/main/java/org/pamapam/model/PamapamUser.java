package org.pamapam.model;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
@Cacheable(false)
public class PamapamUser extends UserImpl {

	private static final long serialVersionUID = 1L;

	public static final String ROLE_REGISTERED = "ROLE_registered";
	public static final String ROLE_INITIATIVE = "ROLE_initiative";
	public static final String ROLE_XINXETA = "ROLE_xinxeta";
	public static final String ROLE_SUPERXINXETA = "ROLE_superxinxeta";
	public static final String ROLE_COORDINATION = "ROLE_coordination";
	public static final String ROLE_ADMIN = "ROLE_admin";
	public static final String ROLE_EXTERNAL = "ROLE_external";
	public static final String ROLE_XINXETA_EINATECA = "ROLE_xinxeta_einateca";
	public static final String ROLE_POL_COOPERATIU = "ROLE_pol_cooperatiu";

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "picture_id")
	private BinaryResource picture;

	@Convert(converter = LocalizedAttributeConverter.class)
	@Column(length = 2048)
	private LocalizedString description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "province_id")
	private Province province;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region_id")
	private Region region;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "town_id")
	private Town town;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "district_id")
	private District district;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "neighborhood_id")
	private Neighborhood neighborhood;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_community", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "community_id", referencedColumnName = "id") })
	private Set<Community> communities;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "registryUser")
	private List<SocialEconomyEntity> socialEconomyEntities;

	@Column
	private String twitter;

	@Column
	private String facebook;

	@Column
	private String instagram;

	@Column
	private String gitlab;

	@Column
	private String fediverse;

	@Column(name = "privacy_consent_date")
	@Temporal(TemporalType.DATE)
	private Date privacyConsentDate;

	@Column(name = "newsletter_consent_date")
	@Temporal(TemporalType.DATE)
	private Date newsletterConsentDate;
	@Transient
	private String rawPassword;

	public BinaryResource getPicture() {
		return this.picture;
	}

	public void setPicture(final BinaryResource picture) {
		this.picture = picture;
	}

	public LocalizedString getDescription() {
		return this.description;
	}

	public void setDescription(final LocalizedString description) {
		this.description = description;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(final Province province) {
		this.province = province;
	}

	public Region getRegion() {
		return this.region;
	}

	public void setRegion(final Region region) {
		this.region = region;
	}

	public Town getTown() {
		return this.town;
	}

	public void setTown(final Town town) {
		this.town = town;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(final District district) {
		this.district = district;
	}

	public Neighborhood getNeighborhood() {
		return this.neighborhood;
	}

	public void setNeighborhood(final Neighborhood neighborhood) {
		this.neighborhood = neighborhood;
	}

	public Set<Community> getCommunities() {
		return this.communities;
	}

	public void setCommunities(final Set<Community> communities) {
		this.communities = communities;
	}

	public List<SocialEconomyEntity> getSocialEconomyEntities() {
		return this.socialEconomyEntities;
	}

	public void setSocialEconomyEntities(final List<SocialEconomyEntity> socialEconomyEntities) {
		this.socialEconomyEntities = socialEconomyEntities;
	}

	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(final String twitter) {
		this.twitter = twitter;
	}

	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(final String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return this.instagram;
	}

	public void setInstagram(final String instagram) {
		this.instagram = instagram;
	}

	public String getGitlab() {
		return this.gitlab;
	}

	public void setGitlab(final String gitlab) {
		this.gitlab = gitlab;
	}

	public String getFediverse() {
		return this.fediverse;
	}

	public void setFediverse(final String fediverse) {
		this.fediverse = fediverse;
	}

	public Role getMainRole() {
		return this.getRoles().stream().reduce(null, (roleA, roleB) -> {
			if (roleA == null) {
				return roleB;
			} else {
				final Integer priorityRoleA = PamapamUser.getRolePriority(roleA);
				final Integer priorityRoleB = PamapamUser.getRolePriority(roleB);
				return priorityRoleA < priorityRoleB ? roleA : roleB;
			}
		});
	}

	// FIXME: Must be a role object attribute.
	private static Integer getRolePriority(final Role role) {
		switch (role.getRolename()) {
			case ROLE_ADMIN:
				return 0;
			case ROLE_COORDINATION:
				return 1;
			case ROLE_SUPERXINXETA:
				return 2;
			case ROLE_XINXETA:
			case ROLE_XINXETA_EINATECA:
				return 3;
			case ROLE_INITIATIVE:
				return 4;
			case ROLE_REGISTERED:
				return 5;
			default:
				return 99;
		}
	}

	public boolean isInitiative() {
		return PamapamUser.ROLE_INITIATIVE.equals(this.getMainRole().getRolename());
	}

	public Date getPrivacyConsentDate() {
		return this.privacyConsentDate;
	}

	public void setPrivacyConsentDate(final Date privacyConsentDate) {
		this.privacyConsentDate = privacyConsentDate;
	}

	public String getRawPassword() {
		return this.rawPassword;
	}

	public void setRawPassword(final String rawPassword) {
		this.rawPassword = rawPassword;
	}

	public Date getNewsletterConsentDate() {
		return this.newsletterConsentDate;
	}

	public void setNewsletterConsentDate(final Date newsletterConsentDate) {
		this.newsletterConsentDate = newsletterConsentDate;
	}
}
