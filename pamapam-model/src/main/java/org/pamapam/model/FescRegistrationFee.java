package org.pamapam.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@Entity
@Table(name = "fesc_registration_fee")
public class FescRegistrationFee extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "fesc_registration_fee_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "fesc_registration_fee_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "fesc_registration_fee_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@ManyToMany(mappedBy = "fees")
	private List<FescRegistrationModality> modalities;

	@Column(name = "fee")
	private Integer fee;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(final LocalizedString name) {
		this.name = name;
	}

	public List<FescRegistrationModality> getModalities() {
		return this.modalities;
	}

	public void setModalities(final List<FescRegistrationModality> modalities) {
		this.modalities = modalities;
	}

	public Integer getFee() {
		return this.fee;
	}

	public void setFee(final Integer fee) {
		this.fee = fee;
	}

}
