package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "mail_template")
public class MailTemplate extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<MailTemplate> NAME_ORDER = new Comparator<MailTemplate>() {
		@Override
		public int compare(MailTemplate objectA, MailTemplate objectB) {
			return objectA.getName().compareToIgnoreCase(objectB.getName());
		}
	};

	@Id
	@TableGenerator(name = "mail_template_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "mail_template_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "mail_template_id_gen")
	private Long id;

	@Column(unique = true)
	private String name;

	@Lob
	@Column
	private String content;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
