package org.pamapam.model;

import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "user_action_token")
@Cacheable(false)
public class UserActionToken extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "user_action_token_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "user_action_token_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "user_action_token_id_gen")
	private Long id;

	@Column(name = "user_id")
	private Long userId;
	@Column
	private String username;
	@Column
	private String action;
	@Column
	private String token;
	@Column
	private String email;
	@Column(name = "valid_to")
	private Timestamp validTo;

	public UserActionToken() {
	}

	public UserActionToken(Long userId, String username, String action, String token, Timestamp validTo, String email) {
		this.userId = userId;
		this.username = username;
		this.action = action;
		this.token = token;
		this.email = email;
		this.validTo = validTo;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(final Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(final String action) {
		this.action = action;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(final String token) {
		this.token = token;
	}

	public Timestamp getValidTo() {
		return this.validTo;
	}

	public void setValidTo(final Timestamp validTo) {
		this.validTo = validTo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "UserActionToken{" +
				"id=" + id +
				", userId=" + userId +
				", username='" + username + '\'' +
				", action='" + action + '\'' +
				", token='" + token + '\'' +
				", email='" + email + '\'' +
				", validTo=" + validTo +
				'}';
	}
}
