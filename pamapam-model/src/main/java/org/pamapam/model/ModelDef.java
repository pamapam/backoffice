package org.pamapam.model;

import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "model_def")
public class ModelDef extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<ModelDef> NAME_ORDER = new Comparator<ModelDef>() {
		@Override
		public int compare(ModelDef objectA, ModelDef objectB) {
			return objectA.getModelName().compareToIgnoreCase(objectB.getModelName());
		}
	};

	@Id
	@TableGenerator(name = "model_def_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "model_def_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "model_def_id_gen")
	private Long id;

	@Column(name = "model_name")
	private String modelName;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "modelDef", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ModelAttributeDef> attributesDef;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getModelName() {
		return this.modelName;
	}

	public void setModelName(String name) {
		this.modelName = name;
	}

	public List<ModelAttributeDef> getAttributesDef() {
		return this.attributesDef;
	}

	public void setAttributesDef(List<ModelAttributeDef> attributesDef) {
		this.attributesDef = attributesDef;
	}

}
