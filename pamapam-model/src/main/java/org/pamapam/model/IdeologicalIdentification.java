package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "ideological_identification")
public class IdeologicalIdentification extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<IdeologicalIdentification> NAME_ORDER = new Comparator<IdeologicalIdentification>() {
		@Override
		public int compare(final IdeologicalIdentification objectA, final IdeologicalIdentification objectB) {
			return objectA.getName().compareToIgnoreCase(objectB.getName());
		}
	};

	@Id
	@TableGenerator(name = "ideological_identification_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "ideological_identification_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ideological_identification_id_gen")
	private Long id;

	@Column
	private String name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
