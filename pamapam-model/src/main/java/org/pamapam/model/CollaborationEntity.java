package org.pamapam.model;

import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@javax.persistence.Entity
@Table(name = "collaboration_entity")
public class CollaborationEntity extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "collaboration_entity_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "collaboration_entity_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "collaboration_entity_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
