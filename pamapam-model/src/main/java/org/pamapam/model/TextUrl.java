package org.pamapam.model;

import org.jamgo.model.entity.Model;

import javax.persistence.*;

/** Entity to store links with text and URI.
 *
 * Originally created to link links from social and solidarity economy entities to pam a pam entities that have the
 * associated scope of Pol Cooperatiu.
 *
 */
@Entity
@Table(name = "text_url")
public class TextUrl extends Model {
	@Id
	@TableGenerator(name = "text_url_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "sector_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "text_url_id_gen")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "url")
	private String url;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "social_economy_entity_id")
	private SocialEconomyEntity entity;

	public TextUrl() {
		super();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public SocialEconomyEntity getEntity() {
		return this.entity;
	}

	public void setEntity(final SocialEconomyEntity entity) {
		this.entity = entity;
	}
}
