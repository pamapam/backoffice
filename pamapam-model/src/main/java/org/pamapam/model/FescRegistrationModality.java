package org.pamapam.model;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "fesc_registration_modality")
public class FescRegistrationModality extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "fesc_registration_modality_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "fesc_registration_modality_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "fesc_registration_modality_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Column(name = "share_table_allowed")
	private boolean shareTableAllowed;

	@Column(name = "is_time_slot")
	private boolean isTimeSlot;

	@ManyToMany
	@JoinTable(
		name = "fesc_registration_modality_fee",
		joinColumns = @JoinColumn(name = "modality_id"),
		inverseJoinColumns = @JoinColumn(name = "fee_id"))
	private Set<FescRegistrationFee> fees;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(final LocalizedString name) {
		this.name = name;
	}

	public boolean isShareTableAllowed() {
		return this.shareTableAllowed;
	}

	public void setShareTableAllowed(final boolean shareTableAllowed) {
		this.shareTableAllowed = shareTableAllowed;
	}

	public Set<FescRegistrationFee> getFees() {
		return this.fees;
	}

	public void setFees(final Set<FescRegistrationFee> fees) {
		this.fees = fees;
	}

	public boolean isTimeSlot() {
		return this.isTimeSlot;
	}

	public void setTimeSlot(final boolean timeSlot) {
		this.isTimeSlot = timeSlot;
	}
}
