package org.pamapam.model;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "entity_status_change")
public class EntityStatusChange extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<EntityStatusChange> TIME_ORDER = new Comparator<EntityStatusChange>() {
		@Override
		public int compare(EntityStatusChange objectA, EntityStatusChange objectB) {
			return objectA.getChangeTimestamp().compareTo(objectB.getChangeTimestamp());
		}
	};

	@Id
	@TableGenerator(name = "entity_status_change_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_status_change_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_status_change_id_gen")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "entity_id")
	private SocialEconomyEntity entity;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "entity_status_from_id")
	private EntityStatus entityStatusFrom;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "entity_status_to_id")
	private EntityStatus entityStatusTo;

	@Column(name = "change_timestamp")
	private Timestamp changeTimestamp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private PamapamUser user;

	public EntityStatusChange() {
		super();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public SocialEconomyEntity getEntity() {
		return this.entity;
	}

	public void setEntity(SocialEconomyEntity entity) {
		this.entity = entity;
	}

	public EntityStatus getEntityStatusFrom() {
		return this.entityStatusFrom;
	}

	public void setEntityStatusFrom(EntityStatus entityStatusFrom) {
		this.entityStatusFrom = entityStatusFrom;
	}

	public EntityStatus getEntityStatusTo() {
		return this.entityStatusTo;
	}

	public void setEntityStatusTo(EntityStatus entityStatusTo) {
		this.entityStatusTo = entityStatusTo;
	}

	public Date getChangeTimestamp() {
		return this.changeTimestamp;
	}

	public void setChangeTimestamp(Timestamp changeTimestamp) {
		this.changeTimestamp = changeTimestamp;
	}

	public PamapamUser getUser() {
		return this.user;
	}

	public void setUser(PamapamUser user) {
		this.user = user;
	}

}