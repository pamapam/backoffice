package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.*;

import org.jamgo.model.entity.Model;
import org.pamapam.model.enums.EntityScopeType;
import org.pamapam.model.enums.NotificationType;

@Entity
@Table(name = "entity_scope")
public class EntityScope extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<EntityScope> NAME_ORDER = new Comparator<EntityScope>() {
		@Override
		public int compare(final EntityScope objectA, final EntityScope objectB) {
			return objectA.getName().compareToIgnoreCase(objectB.getName());
		}
	};

	@Id
	@TableGenerator(name = "entity_scope_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_scope_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_scope_id_gen")
	private Long id;

	@Column
	private String name;

	@Column(name = "entity_scope_type")
	@Enumerated(EnumType.STRING)
	private EntityScopeType entityScopeType;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public EntityScopeType getEntityScopeType() {
		return entityScopeType;
	}

	public void setEntityScopeType(EntityScopeType entityScopeType) {
		this.entityScopeType = entityScopeType;
	}

	public boolean isIntercooperacio() {
		return this.entityScopeType.equals(EntityScopeType.INTERCOOPERACIO_ENTITY_SCOPE);
	}
}
