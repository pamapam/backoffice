package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "external_filter_tag")
public class ExternalFilterTag extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<ExternalFilterTag> TAG_TEXT_ORDER = new Comparator<ExternalFilterTag>() {
		@Override
		public int compare(ExternalFilterTag objectA, ExternalFilterTag objectB) {
			return objectA.getTagText().compareToIgnoreCase(objectB.getTagText());
		}
	};

	@Id
	@TableGenerator(name = "external_filter_tag_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "external_filter_tag_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "external_filter_tag_id_gen")
	private Long id;

	@Column
	private String tagText;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getTagText() {
		return this.tagText;
	}

	public void setTagText(String tagText) {
		this.tagText = tagText;
	}

}
