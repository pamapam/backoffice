package org.pamapam.model;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "embedded_map_config")
public class EmbeddedMapConfig extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<EmbeddedMapConfig> DOMAIN_ORDER = new Comparator<EmbeddedMapConfig>() {
		@Override
		public int compare(final EmbeddedMapConfig objectA, final EmbeddedMapConfig objectB) {
			return objectA.getDomain().compareToIgnoreCase(objectB.getDomain());
		}
	};

	@Id
	@TableGenerator(name = "embedded_map_config_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "embedded_map_config_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "embedded_map_config_id_gen")
	private Long id;

	@Column(length = 1024)
	private String domain;
	@Column(name = "api_key")
	private String apiKey;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "embedded_map_config_tag", joinColumns = { @JoinColumn(name = "embedded_map_config_id") }, inverseJoinColumns = { @JoinColumn(name = "external_filter_tag_id", referencedColumnName = "id") })
	private Set<ExternalFilterTag> externalFilterTags;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "embedded_map_config_status", joinColumns = { @JoinColumn(name = "embedded_map_config_id") }, inverseJoinColumns = { @JoinColumn(name = "status_id", referencedColumnName = "id") })
	private Set<EntityStatus> statuses;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "embedded_map_entiny_scope", joinColumns = { @JoinColumn(name = "embedded_map_config_id") }, inverseJoinColumns = { @JoinColumn(name = "entity_scope_id", referencedColumnName = "id") })
	private Set<EntityScope> entityScopes;
	@Column
	private String latitude;
	@Column
	private String longitude;
	@Column
	private BigDecimal zoom;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(final String domain) {
		this.domain = domain;
	}

	public String getApiKey() {
		return this.apiKey;
	}

	public void setApiKey(final String apiKey) {
		this.apiKey = apiKey;
	}

	public Set<ExternalFilterTag> getExternalFilterTags() {
		return this.externalFilterTags;
	}

	public void setExternalFilterTags(final Set<ExternalFilterTag> externalFilterTags) {
		this.externalFilterTags = externalFilterTags;
	}

	public Set<EntityStatus> getStatuses() {
		return this.statuses;
	}

	public void setStatuses(final Set<EntityStatus> statuses) {
		this.statuses = statuses;
	}

	public Set<EntityScope> getEntityScopes() {
		return this.entityScopes;
	}

	public void setEntityScopes(final Set<EntityScope> entityScopes) {
		this.entityScopes = entityScopes;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(final String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(final String longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getZoom() {
		return this.zoom;
	}

	public void setZoom(final BigDecimal zoom) {
		this.zoom = zoom;
	}

}
