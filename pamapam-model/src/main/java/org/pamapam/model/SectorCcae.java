package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@Entity
@Table(name = "sector_ccae")
public class SectorCcae extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<SectorCcae> CODE_ORDER = new Comparator<SectorCcae>() {
		@Override
		public int compare(SectorCcae objectA, SectorCcae objectB) {
			return objectA.getCode().compareToIgnoreCase(objectB.getCode());
		}
	};

	public static final Comparator<SectorCcae> NAME_ORDER = new Comparator<SectorCcae>() {
		@Override
		public int compare(SectorCcae objectA, SectorCcae objectB) {
			return objectA.getName().getDefaultText().compareToIgnoreCase(objectB.getName().getDefaultText());
		}
	};

	@Id
	@TableGenerator(name = "sector_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "sector_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "sector_id_gen")
	private Long id;

	@Column
	private String code;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}

}
