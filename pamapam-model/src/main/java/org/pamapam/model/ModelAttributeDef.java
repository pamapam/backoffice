package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;
import org.pamapam.model.enums.ModelAttributeType;

@Entity
@Table(name = "model_attribute_def")
public class ModelAttributeDef extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<ModelAttributeDef> NAME_ORDER = new Comparator<ModelAttributeDef>() {
		@Override
		public int compare(ModelAttributeDef objectA, ModelAttributeDef objectB) {
			return objectA.getName().compareToIgnoreCase(objectB.getName());
		}
	};

	@Id
	@TableGenerator(name = "model_attribute_def_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "model_attribute_def_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "model_attribute_def_id_gen")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "model_def_id")
	private ModelDef modelDef;

	@Column
	private String name;

	@Column
	private String label;

	@Column
	@Enumerated
	private ModelAttributeType type;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public ModelDef getModelDef() {
		return this.modelDef;
	}

	public void setModelDef(ModelDef modelDef) {
		this.modelDef = modelDef;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ModelAttributeType getType() {
		return this.type;
	}

	public void setType(ModelAttributeType type) {
		this.type = type;
	}

}
