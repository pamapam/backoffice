package org.pamapam.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = ProductTag.TAG_TYPE)
public class ProductTag extends TextTag {

	private static final long serialVersionUID = 1L;

	public static final String TAG_TYPE = "PROD";

	public ProductTag() {
		super(TAG_TYPE);
	}

	public ProductTag(Long id, String tag) {
		super(id, tag, TAG_TYPE);
	}

}
