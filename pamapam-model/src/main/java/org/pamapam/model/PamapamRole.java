package org.pamapam.model;

import java.util.Comparator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.Role;
import org.jamgo.model.entity.RoleImpl;

@Entity
@Table(name = "pamapam_role")
public class PamapamRole extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<PamapamRole> ID_ORDER = new Comparator<PamapamRole>() {
		@Override
		public int compare(final PamapamRole objectA, final PamapamRole objectB) {
			return objectA.getId().compareTo(objectB.getId());
		}
	};

	@Id
	@TableGenerator(name = "pamapam_role_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "pamapam_role_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "pamapam_role_id_gen")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "role_id")
	private RoleImpl role;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "pamapam_role_entity_scope", joinColumns = { @JoinColumn(name = "pamapam_role_id") }, inverseJoinColumns = { @JoinColumn(name = "entity_scope_id", referencedColumnName = "id") })
	private Set<EntityScope> entityScopes;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(final RoleImpl role) {
		this.role = role;
	}

	public Set<EntityScope> getEntityScopes() {
		return this.entityScopes;
	}

	public void setEntityScopes(final Set<EntityScope> entityScopes) {
		this.entityScopes = entityScopes;
	}

}
