package org.pamapam.model;

import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.RoleImpl;
import org.pamapam.model.enums.EntityAction;
import org.pamapam.model.enums.EntityActionSetConverter;
import org.pamapam.model.enums.NotificationType;
import org.pamapam.model.enums.UserEntityLinkType;

@Entity
@Table(name = "notification_config")
public class NotificationConfig extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<NotificationConfig> NAME_ORDER = new Comparator<NotificationConfig>() {
		@Override
		public int compare(NotificationConfig objectA, NotificationConfig objectB) {
			return Optional.ofNullable(objectA.getName()).orElse("Z")
				.compareTo(Optional.ofNullable(objectB.getName()).orElse("Z"));
		}
	};

	@Id
	@TableGenerator(name = "notification_config_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "notification_config_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "notification_config_id_gen")
	private Long id;

	@Column
	private String name;

	@Column(name = "notification_type")
	@Enumerated(EnumType.STRING)
	private NotificationType notificationType;

	@ManyToMany
	@JoinTable(name = "notification_role", joinColumns = { @JoinColumn(name = "notification_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") })
	private Set<RoleImpl> roles;

	@Column(name = "user_entity_link_type")
	@Enumerated(EnumType.STRING)
	private UserEntityLinkType userEntityLinkType;

	@Convert(converter = EntityActionSetConverter.class)
	@Column(name = "entity_actions")
	private Set<EntityAction> entityActions;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "notification_entity_status", joinColumns = { @JoinColumn(name = "notification_id") }, inverseJoinColumns = { @JoinColumn(name = "entity_status_id", referencedColumnName = "id") })
	private Set<EntityStatus> entityStatuses;

	@Convert(converter = LocalizedAttributeConverter.class)
	@Column(name = "mail_subject")
	private LocalizedString mailSubject;

	@Convert(converter = LocalizedAttributeConverter.class)
	@Column(name = "mail_title")
	private LocalizedString mailTitle;

	@Convert(converter = LocalizedAttributeConverter.class)
	@Column(name = "mail_text", length = 2048)
	private LocalizedString mailText;

	@ManyToOne
	@JoinColumn(name = "mail_template_id")
	private MailTemplate mailTemplate;

	public NotificationConfig() {
		super();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NotificationType getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public Set<RoleImpl> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<RoleImpl> roles) {
		this.roles = roles;
	}

	public UserEntityLinkType getUserEntityLinkType() {
		return this.userEntityLinkType;
	}

	public void setUserEntityLinkType(UserEntityLinkType userEntityLinkType) {
		this.userEntityLinkType = userEntityLinkType;
	}

	public Set<EntityAction> getEntityActions() {
		return this.entityActions;
	}

	public void setEntityActions(Set<EntityAction> entityActions) {
		this.entityActions = entityActions;
	}

	public Set<EntityStatus> getEntityStatuses() {
		return this.entityStatuses;
	}

	public void setEntityStatuses(Set<EntityStatus> entityStatuses) {
		this.entityStatuses = entityStatuses;
	}

	public LocalizedString getMailSubject() {
		return this.mailSubject;
	}

	public void setMailSubject(LocalizedString mailSubject) {
		this.mailSubject = mailSubject;
	}

	public LocalizedString getMailTitle() {
		return this.mailTitle;
	}

	public void setMailTitle(LocalizedString mailTitle) {
		this.mailTitle = mailTitle;
	}

	public LocalizedString getMailText() {
		return this.mailText;
	}

	public void setMailText(LocalizedString mailText) {
		this.mailText = mailText;
	}

	public MailTemplate getMailTemplate() {
		return this.mailTemplate;
	}

	public void setMailTemplate(MailTemplate mailTemplate) {
		this.mailTemplate = mailTemplate;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == this) {
			return true;
		}
		if (!(obj instanceof NotificationConfig)) {
			return false;
		}
		NotificationConfig other = (NotificationConfig) obj;
		return Objects.equals(this.id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

}