package org.pamapam.model;

import java.util.Comparator;

import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.converter.LocalizedAttributeConverter;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;

@javax.persistence.Entity
@Table(name = "person_role")
public class PersonRole extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<PersonRole> ID_ORDER = new Comparator<PersonRole>() {
		@Override
		public int compare(PersonRole objectA, PersonRole objectB) {
			return objectA.getId().compareTo(objectB.getId());
		}
	};

	@Id
	@TableGenerator(name = "person_role_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "person_role_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "person_role_id_gen")
	private Long id;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString description;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}

	public LocalizedString getDescription() {
		return this.description;
	}

	public void setDescription(LocalizedString description) {
		this.description = description;
	}

}
