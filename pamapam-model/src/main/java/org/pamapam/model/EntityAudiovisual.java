package org.pamapam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "entity_audiovisual")
public class EntityAudiovisual extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "entity_audiovisual_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "entity_audiovisual_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "entity_audiovisual_id_gen")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "url")
	private String url;

	@Column(name = "duration")
	private String duration;

	@ManyToOne
	@JoinColumn(name = "entity_id")
	private SocialEconomyEntity entity;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getDuration() {
		return this.duration;
	}

	public void setDuration(final String duration) {
		this.duration = duration;
	}

	public SocialEconomyEntity getEntity() {
		return this.entity;
	}

	public void setEntity(final SocialEconomyEntity entity) {
		this.entity = entity;
	}

}
