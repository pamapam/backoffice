package org.pamapam.exception;

public class EmptyEmailException extends Exception {
	private static final long serialVersionUID = 1L;

	public EmptyEmailException(final String msg) {
		super(msg);
	}
}