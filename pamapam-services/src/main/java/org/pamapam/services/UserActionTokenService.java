package org.pamapam.services;

import org.jamgo.model.entity.UserImpl;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.model.UserActionToken;
import org.pamapam.model.utils.TokenUtils;
import org.pamapam.repository.UserActionTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Optional;

@Service
public class UserActionTokenService extends ModelService<UserActionToken, UserActionToken> {
    private static final Logger logger = LoggerFactory.getLogger(UserActionTokenService.class);
    @Autowired
    UserActionTokenRepository userActionTokenRepository;

    @Override
    public Page<UserActionToken> findAll(PageRequest pageRequest) {
        return null;
    }

    @Override
    public UserActionToken findOne(Long id) {
        return null;
    }

    public UserActionToken save(final UserActionToken userActionToken) {
        UserActionToken newUserActionToken = null;

        try {
          newUserActionToken = this.userActionTokenRepository.save(userActionToken);
          newUserActionToken = userActionToken;
        } catch (Exception e) {
            logger.error("There was an error when trying to save a new user action token", e.getMessage());
        }

        return newUserActionToken;
    }

    public UserActionToken generateEntityToken(SocialEconomyEntity entity, String action, Timestamp validTo) {
        String token = TokenUtils.generate();
        String email = Optional.ofNullable(entity.getEmail())
                .orElse("");
        return new UserActionToken(entity.getId(), entity.getName(), action, token, validTo, email);
    }
}
