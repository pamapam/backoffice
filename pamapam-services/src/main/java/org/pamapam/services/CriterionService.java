package org.pamapam.services;

import java.util.List;

import org.pamapam.model.Criterion;
import org.pamapam.repository.CriterionRepository;
import org.pamapam.services.dto.CriterionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CriterionService extends ModelService<Criterion, CriterionDto> {

	@Autowired
	private CriterionRepository criterionRepository;

	@Override
	public Page<Criterion> findAll(PageRequest pageRequest) {
		return this.criterionRepository.findAll(pageRequest);
	}

	@Override
	public Criterion findOne(Long id) {
		return this.criterionRepository.findOne(id);
	}

	public List<Criterion> findAll(Sort sort) {
		return this.criterionRepository.findByOldVersionFalse(sort);
	}

	public List<CriterionDto> findAllDto(Sort sort) {
		return this.convertToDto(this.findAll(sort), CriterionDto.class);
	}

	public List<CriterionDto> findAllOldDto(Sort sort) {
		return this.convertToDto(this.criterionRepository.findByOldVersionTrue(sort), CriterionDto.class);
	}

}
