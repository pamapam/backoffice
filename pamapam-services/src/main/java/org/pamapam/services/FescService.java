package org.pamapam.services;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.jamgo.model.entity.LocalizedString;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.services.config.PamapamUrlHelper;
import org.pamapam.services.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.*;

@Service
public class FescService extends ModelService<SocialEconomyEntity, FescEntityDto> {

	@Autowired
	SectorService sectorService;
	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private SocialEconomyEntityRepository socialEconomyEntityRepository;

	@Autowired
	private PamapamUrlHelper pamapamUrlHelper;

	@Override
	public Page<SocialEconomyEntity> findAll(final PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SocialEconomyEntity findOne(final Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	/** We consider FESC an entity that has the EntityStatusType of type FESC_PAID.
	 *
	 * That is, it has paid the FESC fee for this or the last year. This status is updated every year for all entities, when FESC registrations are made.
	 * A SQL script is manually sent to the database, which updates the status of all entities to unpaid.
	 * Then, depending on the time of the year when we make this call and if this script has been launched, it will give some results or others.
	 *
	 * @return Returns all entities with FESC Confirmed AND Accepted.
	 */
	public List<FescEntityDto> getFescEntities() {
		final Set<EntityStatus> entityStatuses = this.entityStatusRepository.findByEntityStatusTypeIn(Arrays.asList(EntityStatusType.FESC_PAID, EntityStatusType.FESC_ACCEPTED));
		final List<Tuple> entityTuples = this.socialEconomyEntityRepository.getFescEntitiesTuples(entityStatuses);

		final Map<Long, Tuple> entities = new HashMap<>();
		final MultiValuedMap<Long, Long> sectorIds = new HashSetValuedHashMap<>();
		final MultiValuedMap<Long, Long> productPictureIds = new HashSetValuedHashMap<>();
		final MultiValuedMap<Long, Long> discountIds = new HashSetValuedHashMap<>();
		final MultiValuedMap<Long, Long> audiovisualIds = new HashSetValuedHashMap<>();
		final MultiValuedMap<Long, Long> townIds = new HashSetValuedHashMap<>();
		final MultiValuedMap<Long, Long> modalityIds = new HashSetValuedHashMap<>();
		final Set<Long> allSectorIds = new HashSet<>();

		entityTuples.forEach(each -> {
			final Long id = (Long) each.get("id");
			entities.putIfAbsent(id, each);
			allSectorIds.add((Long) each.get("mainSectorId"));
			allSectorIds.add((Long) each.get("fescMainSectorId"));
			sectorIds.put(id, (Long) each.get("sectorId"));
			productPictureIds.put(id, (Long) each.get("productPictureId"));
			discountIds.put(id, (Long) each.get("discountId"));
			audiovisualIds.put(id, (Long) each.get("audiovisualId"));
			townIds.put(id, (Long) each.get("townId"));
			modalityIds.put(id, (Long) each.get("modalityId"));
		});
		allSectorIds.addAll(sectorIds.values());

		final Map<Long, SectorDto> parentSectors = new HashMap<>();
		final Map<Long, SectorDto> sectors = new HashMap<>();
		final Map<Long, BinaryResourceDto> productPictures = new HashMap<>();
		final Map<Long, DiscountDto> discounts = new HashMap<>();
		final Map<Long, AudiovisualDto> audiovisuals = new HashMap<>();
		final Map<Long, TownDto> towns = new HashMap<>();
		final Map<Long, ModalityDto> modalities = new HashMap<>();

		this.socialEconomyEntityRepository.getProductPictureTuples(Sets.newHashSet(productPictureIds.values()))
			.forEach(each -> productPictures.put((Long) each.get("id"), this.convertToBinaryResourceDto(each)));
		this.socialEconomyEntityRepository.getParentSectorTuples().forEach(each -> parentSectors.put((Long) each.get("id"), this.convertToSectorDto(each, null)));
		this.socialEconomyEntityRepository.getSectorTuples(allSectorIds).forEach(each -> sectors.put((Long) each.get("id"), this.convertToSectorDto(each, parentSectors)));
		this.socialEconomyEntityRepository.getDiscountTuples(Sets.newHashSet(discountIds.values())).forEach(each -> discounts.put((Long) each.get("id"), FescService.convertToDiscountDto(each)));
		this.socialEconomyEntityRepository.getAudiovisualTuples(Sets.newHashSet(audiovisualIds.values())).forEach(each -> audiovisuals.put((Long) each.get("id"), FescService.convertToAudiovisualDto(each)));
		this.socialEconomyEntityRepository.getTownTuples(Sets.newHashSet(townIds.values())).forEach(each -> towns.put((Long) each.get("id"), FescService.convertToTownDto(each)));
		this.socialEconomyEntityRepository.getModalityTuples(Sets.newHashSet(modalityIds.values())).forEach(each -> modalities.put((Long) each.get("id"), FescService.convertToModalityDto(each)));

		final List<FescEntityDto> result = new ArrayList<>();
		entities.forEach((id, tuple) -> {
			final List<SectorDto> entitySectors = new ArrayList<>();
			final List<BinaryResourceDto> entityProductPictures = new ArrayList<>();
			final List<DiscountDto> entityDiscounts = new ArrayList<>();
			final List<AudiovisualDto> entityAudiovisuals = new ArrayList<>();

			sectorIds.get(id)
				.forEach(each -> Optional.ofNullable(each)
					.ifPresent(sectorId -> entitySectors.add(sectors.get(sectorId))));

			productPictureIds.get(id).forEach(each -> Optional.ofNullable(each).ifPresent(productPictureId -> entityProductPictures.add(productPictures.get(productPictureId))));
			discountIds.get(id).forEach(each -> Optional.ofNullable(each).ifPresent(discountId -> entityDiscounts.add(discounts.get(discountId))));
			audiovisualIds.get(id).forEach(each -> Optional.ofNullable(each).ifPresent(audiovisualId -> entityAudiovisuals.add(audiovisuals.get(audiovisualId))));

			SectorDto entityMainSector = Optional.ofNullable(tuple.get("mainSectorId")).map(o -> sectors.get(o)).orElse(null);
			if (entityMainSector.getParent() != null) {
				entityMainSector = entityMainSector.getParent();
			}

			SectorDto fescMainSector = Optional.ofNullable(tuple.get("fescMainSectorId"))
				.map(o -> sectors.get(o)).orElse(null);

			if (fescMainSector != null && fescMainSector.getParent() != null) {
				fescMainSector = fescMainSector.getParent();
			}

			final TownDto entityTown = Optional.ofNullable(tuple.get("townId")).map(o -> towns.get(o)).orElse(null);
			final ModalityDto entityModality = Optional.ofNullable(tuple.get("modalityId")).map(o -> modalities.get(o)).orElse(null);

			final FescEntityDto entityDto = this.convertFescEntityToDto(id,
				tuple,
				entitySectors,
				entityProductPictures,
				entityDiscounts,
				entityAudiovisuals,
				entityMainSector,
				fescMainSector,
				entityTown,
				entityModality);

			result.add(entityDto);
		});

		return result;
	}

	private BinaryResourceDto convertToBinaryResourceDto(final Tuple tuple) {
		final BinaryResourceDto binaryResourceDto = new BinaryResourceDto();
		binaryResourceDto.setId((Long) tuple.get("id"));
		binaryResourceDto.setPictureUrl(this.pamapamUrlHelper.getImageUrl((Long) tuple.get("id")));
		binaryResourceDto.setDescription((String) tuple.get("description"));

		return binaryResourceDto;
	}

	private SectorDto convertToSectorDto(final Tuple tuple, final Map<Long, SectorDto> parentSectors) {
		final SectorDto sectorDto = new SectorDto();
		sectorDto.setId((Long) tuple.get("id"));
		sectorDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());
		sectorDto.setDescription(((LocalizedString) tuple.get("description")).getDefaultText());
		final Long parentId = (Long) tuple.get("parentId");
		if (parentSectors != null && parentId != null) {
			final SectorDto parentSector = parentSectors.get(parentId);
			sectorDto.setParent(parentSector);
			sectorDto.setIconUrl(parentSector.getIconUrl());
			sectorDto.setMapIconUrl(parentSector.getMapIconUrl());
		} else {
			sectorDto.setIconUrl(Optional.ofNullable(tuple.get("iconId"))
				.map(o -> this.pamapamUrlHelper.getImageUrl((Long) o))
				.orElse(null));
			sectorDto.setMapIconUrl(Optional.ofNullable(tuple.get("mapIconId"))
				.map(o -> this.pamapamUrlHelper.getImageUrl((Long) o))
				.orElse(null));
		}

		return sectorDto;
	}

	private static DiscountDto convertToDiscountDto(final Tuple tuple) {
		final DiscountDto discountDto = new DiscountDto();
		discountDto.setName((String) tuple.get("name"));
		discountDto.setDescription((String) tuple.get("description"));
		discountDto.setBeginDate((Date) tuple.get("beginDate"));
		discountDto.setEndDate((Date) tuple.get("endDate"));
		discountDto.setUrl((String) tuple.get("url"));

		return discountDto;
	}

	private static AudiovisualDto convertToAudiovisualDto(final Tuple tuple) {
		final AudiovisualDto audiovisualDto = new AudiovisualDto();
		audiovisualDto.setName((String) tuple.get("name"));
		audiovisualDto.setDescription((String) tuple.get("description"));
		audiovisualDto.setUrl((String) tuple.get("url"));
		audiovisualDto.setDuration((String) tuple.get("duration"));

		return audiovisualDto;
	}

	private static TownDto convertToTownDto(final Tuple tuple) {
		final TownDto townDto = new TownDto();
		townDto.setId((Long) tuple.get("id"));
		townDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());

		return townDto;
	}

	private static ModalityDto convertToModalityDto(final Tuple tuple) {
		final ModalityDto modalityDto = new ModalityDto();
		modalityDto.setId((Long) tuple.get("id"));
		modalityDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());
		return modalityDto;
	}

	private FescEntityDto convertFescEntityToDto(
		final Long id,
		final Tuple entityTuple,
		final List<SectorDto> entitySectors,
		final List<BinaryResourceDto> entityProductPictures,
		final List<DiscountDto> entityDiscounts,
		final List<AudiovisualDto> entityAudiovisuals,
		final SectorDto entityMainSector,
		final SectorDto fescMainSector,
		final TownDto entityTown,
		final ModalityDto entityModality) {

		final FescEntityDto fescEntityDto = new FescEntityDto();
		fescEntityDto.setId(id);
		fescEntityDto.setName((String) entityTuple.get("name"));
		fescEntityDto.setDescription(((LocalizedString) entityTuple.get("description")).getDefaultText());
		fescEntityDto.setPictureUrl(Optional.ofNullable(entityTuple.get("pictureId"))
			.map(o -> this.pamapamUrlHelper.getImageUrl((Long) o))
			.orElse(null));
		fescEntityDto.setEmail((String) entityTuple.get("email"));
		fescEntityDto.setPhone((String) entityTuple.get("phone"));
		fescEntityDto.setWeb((String) entityTuple.get("web"));
		fescEntityDto.setTwitter((String) entityTuple.get("twitter"));
		fescEntityDto.setFacebook((String) entityTuple.get("facebook"));
		fescEntityDto.setInstagram((String) entityTuple.get("instagram"));
		fescEntityDto.setPinterest((String) entityTuple.get("pinterest"));
		fescEntityDto.setQuitter((String) entityTuple.get("quitter"));
		fescEntityDto.setPublicAdministration((Boolean) entityTuple.get("publicAdministration"));
		fescEntityDto.setEnterprises((Boolean) entityTuple.get("enterprises"));
		fescEntityDto.setPrivateActivists((Boolean) entityTuple.get("privateActivists"));
		fescEntityDto.setPrivateNoobs((Boolean) entityTuple.get("privateNoobs"));
		fescEntityDto.setUrlVideo((String) entityTuple.get("fescVideo"));
		fescEntityDto.setMainSector(entityMainSector);
		fescEntityDto.setSectors(entitySectors);
		fescEntityDto.setTown(entityTown);
		fescEntityDto.setDiscounts(entityDiscounts);
		fescEntityDto.setAudiovisualDocuments(entityAudiovisuals);
		fescEntityDto.setProductPictures(entityProductPictures);
		fescEntityDto.setModality(entityModality);
		fescEntityDto.setMainFescSector(fescMainSector);

		return fescEntityDto;
	}

}
