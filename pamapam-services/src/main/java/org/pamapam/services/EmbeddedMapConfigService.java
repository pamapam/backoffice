package org.pamapam.services;

import java.util.Optional;

import org.pamapam.model.EmbeddedMapConfig;
import org.pamapam.repository.EmbeddedMapConfigRepository;
import org.pamapam.services.dto.EmbeddedMapConfigDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class EmbeddedMapConfigService extends ModelService<EmbeddedMapConfig, EmbeddedMapConfigDto> {

	@Autowired
	private EmbeddedMapConfigRepository embeddedMapConfigRepository;

	public EmbeddedMapConfig findByDomainAndApiKey(final String domain, final String apiKey) {
		return this.embeddedMapConfigRepository.findByDomainAndApiKey(domain, apiKey);
	}

	public EmbeddedMapConfigDto findByDomainAndApiKeyDto(final String domain, final String apiKey) {
		return Optional.ofNullable(this.findByDomainAndApiKey(domain, apiKey)).map(o -> this.convertToDto(o, EmbeddedMapConfigDto.class)).orElse(null);
	}

	@Override
	public Page<EmbeddedMapConfig> findAll(final PageRequest pageRequest) {
		return this.embeddedMapConfigRepository.findAll(pageRequest);
	}

	@Override
	public EmbeddedMapConfig findOne(final Long id) {
		return this.embeddedMapConfigRepository.findOne(id);
	}

}
