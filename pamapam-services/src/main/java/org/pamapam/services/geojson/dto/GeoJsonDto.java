package org.pamapam.services.geojson.dto;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "type", "geometry", "properties" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class GeoJsonDto {

	@JsonProperty
	private String type;

	@JsonProperty
	private GeoJsonGeometryDto geometry;

	@JsonProperty
	private Map<String, Object> properties;

	public GeoJsonDto(String type) {
		this.type = type;
		this.properties = new HashMap<>();
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public GeoJsonGeometryDto getGeometry() {
		return this.geometry;
	}

	public void setGeometry(GeoJsonGeometryDto geometry) {
		this.geometry = geometry;
	}

	public Map<String, Object> getProperties() {
		return this.properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	public void addProperty(String name, Object value) {
		this.properties.put(name, value);
	}
}
