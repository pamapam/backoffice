package org.pamapam.services;

import java.util.List;

import org.jamgo.model.entity.Town;
import org.jamgo.model.repository.TownRepository;
import org.pamapam.services.dto.TownDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class TownService extends ModelService<Town, TownDto> {

	@Autowired
	private TownRepository townRepository;

	public List<Town> findAll() {
		return this.townRepository.findAll();
	}

	@Override
	public Page<Town> findAll(PageRequest pageRequest) {
		return this.townRepository.findAll(pageRequest);
	}

	@Override
	public Town findOne(Long id) {
		return this.townRepository.findOne(id);
	}

}
