package org.pamapam.services;

import java.util.List;

import org.pamapam.model.FescRegistrationModality;
import org.pamapam.repository.FescRegistrationModalityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FescRegistrationModalityService {

	@Autowired
	private FescRegistrationModalityRepository repository;

	public List<FescRegistrationModality> findAll() {
		return this.repository.findAll();
	}

}
