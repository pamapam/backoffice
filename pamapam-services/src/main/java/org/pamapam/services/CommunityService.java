package org.pamapam.services;

import java.util.List;

import org.pamapam.model.Community;
import org.pamapam.repository.CommunityRepository;
import org.pamapam.services.dto.CommunityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CommunityService extends ModelService<Community, CommunityDto> {

	@Autowired
	private CommunityRepository communityRepository;

	@Override
	public Page<Community> findAll(PageRequest pageRequest) {
		return this.communityRepository.findAll(pageRequest);
	}

	public List<CommunityDto> findAllDto(Sort sort) {
		return this.convertToDto(this.communityRepository.findAll(sort), CommunityDto.class);
	}

	@Override
	public Community findOne(Long id) {
		return this.communityRepository.findOne(id);
	}

}
