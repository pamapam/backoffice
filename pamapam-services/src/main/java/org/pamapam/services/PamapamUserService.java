package org.pamapam.services;

import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.io.FileUtils;
import org.jamgo.model.entity.Role;
import org.jamgo.model.repository.RoleRepository;
import org.jamgo.model.search.SearchSpecification;
import org.jamgo.model.util.PasswordUtils;
import org.jamgo.services.dto.PageDto;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.message.LocalizedMessageService;
import org.modelmapper.TypeToken;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.repository.PamapamUserRepository;
import org.pamapam.repository.search.PamapamUserSearch;
import org.pamapam.repository.search.PamapamUserSpecification;
import org.pamapam.services.dto.NewsletterRegistrationDto;
import org.pamapam.services.dto.PamapamUserDto;
import org.pamapam.services.dto.PamapamUserSearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PamapamUserService extends ModelService<PamapamUser, PamapamUserDto> {

	private static final List<String> VISIBLE_ROLES = Lists.newArrayList("ROLE_admin", "ROLE_xinxeta", "ROLE_superxinxeta", "ROLE_comunicacio");

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private PamapamUserRepository userRepository;
	@Autowired
	protected RoleRepository rolesRepository;
	@Autowired
	private SessionRegistry sessionRegistry;
	@Autowired
	private AsyncTaskExecutor asyncTaskExecutor;
	//	@Autowired
//	private MailService mailService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private TerritoryService territoryService;
	@Autowired
	private CommunityService communityService;
	@Autowired
	private SocialEconomyEntityService socialEconomyEntityService;
	@Autowired
	private LocalizedMessageService messageSource;
	@Autowired
	private NewsletterRegistrationService newsletterRegistrationService;

	private PamapamUserSpecification searchSpecification;

	@PostConstruct
	private void init() {
		this.searchSpecification = this.applicationContext.getBean(PamapamUserSpecification.class);
	}

	// ...	Find methods.

	@Override
	public Page<PamapamUser> findAll(final PageRequest pageRequest) {
		return this.userRepository.findAll(pageRequest);
	}

	@Override
	public PamapamUser findOne(final Long id) {
		return this.userRepository.findOne(id);
	}

	public PamapamUser findByUsername(final String username) {
		return this.userRepository.findByUsername(username);
	}

	public PamapamUser findByEmail(final String email) {
		PamapamUser pamapamUser = null;
		try {
			pamapamUser = this.userRepository.findByEmail(email);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return pamapamUser;
	}

	public Page<PamapamUser> findSortedByActivity(final PageRequest pageRequest) {
		return this.userRepository.findSortedByActivity(pageRequest);
	}

	public void save(final PamapamUser pamapamUser) {
		this.userRepository.save(pamapamUser);
	}

	public PageDto<PamapamUserDto> findSortedByActivityDto(final PageRequest pageRequest) {
		final Page<PamapamUser> page = this.findSortedByActivity(pageRequest);

		final PageDto<PamapamUserDto> pageDto = new PageDto<>();
		pageDto.setTotalElements(page.getTotalElements());
		pageDto.setLastPage(page.isLast());
		final List<PamapamUser> usersPage = page.getContent();
		pageDto.setContent(this.modelMapper.map(usersPage, new TypeToken<List<PamapamUserDto>>() {
		}.getType()));
		return pageDto;
	}

	public Page<PamapamUser> search(final PamapamUserSearchDto searchObjectDto, final PageRequest pageRequest) {
		Page<PamapamUser> resultPage = null;
		if ((searchObjectDto.getName() == null) && (searchObjectDto.getCommunityId() == null) && (searchObjectDto.getTerritoryId() == null)) {
			resultPage = this.userRepository.findSortedByActivity(pageRequest);
		} else {
			final PamapamUserSearch searchObject = new PamapamUserSearch();
			if (searchObjectDto.getName() != null) {
				searchObject.setName(searchObjectDto.getName());
			}
			if (searchObjectDto.getTerritoryId() != null) {
				searchObject.setTerritory(this.territoryService.findOne(searchObjectDto.getTerritoryId(), searchObjectDto.getTerritoryType()));
			}
			if (searchObjectDto.getCommunityId() != null) {
				searchObject.setCommunities(Sets.newHashSet(this.communityService.findOne(searchObjectDto.getCommunityId())));
			}
			searchObject.setRoles(this.rolesRepository.findAll().stream()
				.filter(each -> PamapamUserService.VISIBLE_ROLES.contains(each.getRolename()))
				.collect(Collectors.toSet()));
			this.searchSpecification.setSearchObject(searchObject);

			resultPage = this.userRepository.findAll(this.searchSpecification, pageRequest);
		}

		return resultPage;
	}

	public PageDto<PamapamUserDto> searchDto(final PamapamUserSearchDto searchObjectDto, final PageRequest pageRequest) {
		return this.convertToDto(this.search(searchObjectDto, pageRequest), new TypeToken<List<PamapamUserDto>>() {
		}.getType());
	}

	public PamapamUser getCurrentUser() {
		PamapamUser currentUser = null;
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			currentUser = this.userRepository.findByUsername(authentication.getName());
		}
		return currentUser;
	}

	public List<String> getActiveSessions() {
		return this.sessionRegistry.getAllPrincipals().stream()
			.filter(u -> !this.sessionRegistry.getAllSessions(u, false).isEmpty())
			.map(Object::toString)
			.collect(Collectors.toList());
	}

	public PamapamUser addUser(final PamapamUserDto userDto) {
		final PamapamUser savedUser = this.basicAddUser(userDto);
		this.sendUserCreatedNotification(savedUser, savedUser.getRawPassword());
		return savedUser;
	}

	public PamapamUser addFescUser(final PamapamUserDto userDto) {
		final PamapamUser savedUser = this.basicAddUser(userDto);
		this.sendFescUserCreatedNotification(savedUser, savedUser.getRawPassword());
		return savedUser;
	}

	public PamapamUser basicAddUser(final PamapamUserDto userDto) {
		final PamapamUser user = new PamapamUser();
		if (userDto.getUsername() == null) {
			user.setUsername(userDto.getName());
		} else {
			user.setUsername(userDto.getUsername().trim());
		}
		final Set<Role> roles = new HashSet<>();
		for (final String roleName : Optional.ofNullable(userDto.getRoleNames()).orElse(Lists.newArrayList(PamapamUser.ROLE_REGISTERED))) {
			roles.add(this.rolesRepository.findByRolename(roleName));
		}
//		String password = PasswordUtils.generatePassword();
//		user.setRawPassword(password);
//		user.setPassword(PasswordUtils.encodePassword(password));
		PamapamUserService.assignPassword(user);
		user.setRoles(roles);
		user.setName(userDto.getName());
		user.setDescription(this.createLocalizedString(userDto.getDescription()));
		if (userDto.getEmail() != null) {
			user.setEmail(userDto.getEmail().trim());
		}
		if (userDto.getProvinceId() != null) {
			user.setProvince(this.territoryService.findProvince(userDto.getProvinceId()));
		}
		if (userDto.getRegionId() != null) {
			user.setRegion(this.territoryService.findRegion(userDto.getRegionId()));
		}
		if (userDto.getTownId() != null) {
			user.setTown(this.territoryService.findTown(userDto.getTownId()));
		}
		if (userDto.getDistrictId() != null) {
			user.setDistrict(this.territoryService.findDistrict(userDto.getDistrictId()));
		}
		if (userDto.getNeighborhoodId() != null) {
			user.setNeighborhood(this.territoryService.findNeighborhood(userDto.getNeighborhoodId()));
		}
		if (userDto.isUserConditionsAccepted()) {
			user.setPrivacyConsentDate(Date.from(Instant.now()));
		}
		if (userDto.isNewslettersAccepted()) {
			final NewsletterRegistrationDto newsletterRegistrationDto = new NewsletterRegistrationDto(user.getName(), user.getEmail());
			try {
				this.newsletterRegistrationService.addNewsletterRegistration(newsletterRegistrationDto);
				user.setNewsletterConsentDate(Date.from(Instant.now()));
			} catch (final CrudException e) {
				e.printStackTrace();
			}
		}

		user.setTwitter(userDto.getTwitter());
		user.setFacebook(userDto.getFacebook());
		user.setInstagram(userDto.getInstagram());
		user.setGitlab(userDto.getGitlab());
		user.setFediverse(userDto.getFediverse());
		user.setEnabled(true);
		final PamapamUser savedUser = this.userRepository.save(user);
		return savedUser;
	}

	public void resetPassword(final PamapamUser user) throws CrudException {
		final String newPassword = PasswordUtils.generatePassword();
		user.setPassword(PasswordUtils.encodePassword(newPassword));
		this.userRepository.save(user);
		this.asyncTaskExecutor.execute(this.sendPasswordResetNotification(user, newPassword));
	}

	public boolean hasPrivacyConsent(final String username) {
		final PamapamUser user = this.findByUsername(username);
		return Optional.ofNullable(user).map(o -> o.getPrivacyConsentDate() != null).orElse(false);
	}

	public static void assignPassword(final PamapamUser user) {
		final String password = PasswordUtils.generatePassword();
		user.setRawPassword(password);
		user.setPassword(PasswordUtils.encodePassword(password));
	}

	/*
	 * ...	User is not allowed to login if has only ROLE_REGISTERED.
	 */

	public boolean isLoginAllowed(final String username) {
		final PamapamUser user = this.findByUsername(username);
		return !((user.getRoles().size() == 1) && PamapamUser.ROLE_REGISTERED.equals(user.getRoles().stream().findFirst().get().getRolename()));
	}

	public void setPrivacyConsent(final String username, final Boolean privacyConsent) {
		final PamapamUser user = this.findByUsername(username);
		user.setPrivacyConsentDate(new Date());
		this.userRepository.save(user);
	}

	private Runnable sendPasswordResetNotification(final PamapamUser user, final String password) {
		return () -> {
			this.notificationService.sendPasswordResetNotification(user, password);
//			this.mailService.sendPasswordResetMail(user, password);
		};
	}

	public void sendUserCreatedNotification(final PamapamUser user, final String password) {
		this.asyncTaskExecutor.execute(() -> {
			this.notificationService.sendUserCreatedNotification(user, password);
//			this.mailService.sendUserCreatedMail(user, password);
		});
	}

	public void sendFescUserCreatedNotification(final PamapamUser user, final String password) {
		this.asyncTaskExecutor.execute(() -> {
			this.notificationService.sendFescUserCreatedNotification(user, password);
		});
	}

	public void sendAutoproposedInitiativeWelcomeNotification(final PamapamUser user, final String password) {
		this.asyncTaskExecutor.execute(() -> {
			this.notificationService.sendAutoproposedInitiativeWelcomeNotification(user, password);
		});
	}

	public String exportToCsv(final SearchSpecification<PamapamUser, PamapamUserSearch> searchSpecification) throws IOException, JsonMappingException {
		final CsvMapper csvMapper = new CsvMapper();
		csvMapper.configure(Feature.AUTO_CLOSE_TARGET, false);

		final CsvSchema csvSchema = this.createSchema();
		final ObjectWriter writer = csvMapper.writer(csvSchema);
		final String fileName = "users-export-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss")) + ".csv";
		final File tempFile = FileUtils.getFile(FileUtils.getTempDirectory(), fileName);

		final Stream<PamapamUser> stream = this.userRepository.streamAll(searchSpecification);
		final List<List<String>> sectorsData = new ArrayList<>();
		final FileOutputStream tempFileOutputStream = new FileOutputStream(tempFile);
		final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
		final OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, "UTF-8");
		stream.forEach(each -> {
			final List<String> row = this.buildRow(each);
			sectorsData.add(row);
		});
		writer.writeValue(writerOutputStream, sectorsData);

		tempFileOutputStream.flush();
		tempFileOutputStream.close();
		return tempFile.getAbsolutePath();
	}

	private CsvSchema createSchema() {
		final CsvSchema.Builder csvBuilder = CsvSchema.builder();
		csvBuilder
			.addColumn(this.messageSource.getMessage("user.username"))
			.addColumn(this.messageSource.getMessage("user.name"))
			.addColumn(this.messageSource.getMessage("user.surname"))
			.addColumn(this.messageSource.getMessage("user.email"))
			.addColumn(this.messageSource.getMessage("user.role"))
			.addColumn(this.messageSource.getMessage("user.acceptedUserTerms"))
			.addColumn(this.messageSource.getMessage("user.newsletterConsent.date"));

		return csvBuilder.build().withHeader();
	}

	private List<String> buildRow(final PamapamUser user) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		final List<String> entityRow = new ArrayList<>();
		entityRow.add(Optional.ofNullable(user.getUsername()).orElse(""));
		entityRow.add(Optional.ofNullable(user.getName()).orElse(""));
		entityRow.add(Optional.ofNullable(user.getSurname()).orElse(""));
		entityRow.add(Optional.ofNullable(user.getEmail()).orElse(""));
		entityRow.add(Optional.ofNullable(user.getMainRole().getRolename()).orElse(""));

		entityRow.add(Optional.ofNullable(user.getPrivacyConsentDate())
			.map(dateFormat::format)
			.orElse("no"));

		entityRow.add(Optional.ofNullable(user.getNewsletterConsentDate())
			.map(dateFormat::format)
			.orElse("no"));

		if (user.isInitiative()) {
			final List<SocialEconomyEntity> socialEconomyEntities = this.socialEconomyEntityService.findByInitiativeUser(user);

			for (final SocialEconomyEntity entity : socialEconomyEntities) {
				entityRow.add(entity.toString());
			}
		}

		return entityRow;
	}

}
