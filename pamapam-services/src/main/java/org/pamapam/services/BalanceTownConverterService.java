package org.pamapam.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.pamapam.services.dto.TownDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class BalanceTownConverterService implements InitializingBean {
	//Hay que coger todas las Town de Balanç, buscar el equivalente en pamapam
	// con una query dentro de JSON
	// insertarlos en
	//balance_town_converter
	//balance_id, pamapam_id
	private static final Logger logger = LoggerFactory.getLogger(SocialBalanceService.class);

	private RestTemplate restTemplate;

	@Value("${xes.socialbalance.api.domain}")
	private String envDomain;
	private ObjectMapper objectMapper;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.restTemplate = new RestTemplate();
		this.objectMapper = new ObjectMapper();
		this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public List<TownDto> fetchAllBalanceTowns() {
		//Todos los pueblos de Cataluña
		final String resourceUrl = this.envDomain + "/territory/community/9/towns";

		// Fetch JSON response as String wrapped in ResponseEntity
		final ResponseEntity<String> response = this.restTemplate.getForEntity(resourceUrl, String.class);
		final String townsJson = response.getBody();
		List<TownDto> socialBalanceTowns = new ArrayList<>();
		//Convert to TownDto's
		try {
			socialBalanceTowns = Arrays.asList(this.objectMapper.readValue(townsJson, TownDto[].class));
		} catch (final Exception e) {
			logger.error("Unable to read xes social balance Town dtos");
			logger.error(e.getMessage());
		}

		//Insertar y vincular ids de Towns que vienen de Balanç con los de Pam a Pam
		return socialBalanceTowns;
	}
}
