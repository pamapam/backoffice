package org.pamapam.services;

import org.pamapam.model.EntityScope;
import org.pamapam.model.PamapamRole;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.enums.EntityScopeType;
import org.pamapam.repository.EntityScopeRepository;
import org.pamapam.repository.PamapamRoleRepository;
import org.pamapam.services.dto.EntityScopeDto;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EntityScopeService extends ModelService<EntityScope, EntityScopeDto> implements InitializingBean {

	@Autowired
	private EntityScopeRepository entityScopeRepository;
	@Autowired
	private PamapamRoleRepository pamapamRoleRepository;
	@Autowired
	private PamapamUserService userService;

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	@Override
	public Page<EntityScope> findAll(final PageRequest pageRequest) {
		return this.entityScopeRepository.findAll(pageRequest);
	}

	public List<EntityScopeDto> findAllDto(final Sort sort) {
		return this.convertToDto(this.entityScopeRepository.findAll(sort), EntityScopeDto.class);
	}

	@Override
	public EntityScope findOne(final Long id) {
		return this.entityScopeRepository.findOne(id);
	}

	public EntityScope getDefaultEntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.DEFAULT)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public EntityScope getEinatecaEntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.EINATECA)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public EntityScope getPolIntercoopEntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.INTERCOOPERACIO_ENTITY_SCOPE)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public EntityScope getFesc2021EntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.FESC_2021_ENTITY_SCOPE)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public EntityScope getFesc2022EntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.FESC_2022_ENTITY_SCOPE)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public EntityScope getCommunityEconomyEntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.COMMUNITY_ECONOMY_ENTITY_SCOPE)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public Set<EntityScope> getDefaultEntityScopes() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.DEFAULT);
	}

	public Set<EntityScope> getEinatecaEntityScopes() {
		final Set<EntityScope> einatecaEntityScopes = this.entityScopeRepository.findByEntityScopeType(EntityScopeType.EINATECA);
		//add Pam a Pam EntityScope because new Einateca SocialEconomyEntity should be appearing on admin dashboard issue #13042
		einatecaEntityScopes.addAll(this.getDefaultEntityScopes());
		return einatecaEntityScopes;
	}

	public Set<EntityScope> getFesc2021EntityScopes() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.FESC_2021_ENTITY_SCOPE);
	}

	public Set<EntityScope> getCommunityEconomyEntityScopes() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.COMMUNITY_ECONOMY_ENTITY_SCOPE);
	}

	public EntityScope getIntercooperacioEntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.INTERCOOPERACIO_ENTITY_SCOPE)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public EntityScope getSocialBalanceEntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.SOCIAL_BALANCE_ENTITY_SCOPE)
			.stream()
			.findFirst()
			.orElse(null);
	}
	

	public EntityScope getGreenCommerceEntityScope() {
		return this.entityScopeRepository.findByEntityScopeType(EntityScopeType.GREEN_COMMERCE_ENTITY_SCOPE)
			.stream()
			.findFirst()
			.orElse(null);
	}

	public Set<EntityScope> getCurrentEntityScopes() {
		final PamapamUser currentUser = this.userService.getCurrentUser();
		if (currentUser == null) {
			return this.getDefaultEntityScopes();
		} else {
			final List<PamapamRole> pamapamRoles = this.pamapamRoleRepository.findByRoleIn(currentUser.getRoles());
			final Set<EntityScope> entityScopes = pamapamRoles.stream()
				.map(each -> each.getEntityScopes())
				.flatMap(each -> each.stream())
				.collect(Collectors.toSet());
			return entityScopes;
		}
	}
}
