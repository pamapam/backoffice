package org.pamapam.services.config;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.LocalizedString;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.pamapam.model.*;
import org.pamapam.services.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

import java.util.List;
import java.util.Optional;

@Configuration
public class PamapamBackofficeServicesConfig {
    private static final Logger logger = LoggerFactory.getLogger(PamapamBackofficeServicesConfig.class);

    @Autowired
    private PamapamUrlHelper pamapamUrlHelper;

    @Bean
    public ModelMapper modelMapper() {
        final ModelMapper modelMapper = new ModelMapper();
        modelMapper.addConverter(PamapamBackofficeServicesConfig.getLocalizedStringConverter());
        modelMapper.addConverter(this.getBinaryResourceConverter());
        modelMapper.addConverter(this.getBinaryResourceToProductPictureConverter());
        modelMapper.addConverter(PamapamBackofficeServicesConfig.getTextTagConverter());
//		modelMapper.addConverter(this.getExternalFilterTagConverter());

        PamapamBackofficeServicesConfig.addPamapamUserTypeMap(modelMapper);
        PamapamBackofficeServicesConfig.addSocialEconomyEntityTypeMap(modelMapper);
        PamapamBackofficeServicesConfig.addSectorTypeMap(modelMapper);
        PamapamBackofficeServicesConfig.addCriterionTypeMap(modelMapper);
        PamapamBackofficeServicesConfig.addFescTypeMap(modelMapper);
        PamapamBackofficeServicesConfig.addDiscountTypeMap(modelMapper);

        return modelMapper;
    }

    private static void addPamapamUserTypeMap(final ModelMapper modelMapper) {
        modelMapper.createTypeMap(PamapamUser.class, PamapamUserDto.class)
                .addMapping(src -> src.getPicture(), (dest, v) -> dest.setPictureUrl((String) v));
    }

    private static void addSocialEconomyEntityTypeMap(final ModelMapper modelMapper) {
        modelMapper.createTypeMap(SocialEconomyEntity.class, SocialEconomyEntityDto.class)
                .addMapping(src -> src.getPicture(), (dest, v) -> dest.setPictureUrl((String) v))
                .addMapping(src -> src.getProductTag(), (dest, v) -> dest.setTags((List<String>) v))
                .addMapping(src -> src.isIntercooperacio(), (dest, v) -> dest.setIsIntercooperacio((Boolean) v));
    }

    private static void addFescTypeMap(final ModelMapper modelMapper) {
        modelMapper.createTypeMap(SocialEconomyEntity.class, FescEntityDto.class)
                .addMapping(src -> src.getPicture(), (dest, v) -> dest.setPictureUrl((String) v))
                .addMapping(src -> src.getFescData().getVideo(), (dest, v) -> dest.setUrlVideo((String) v))
                .addMapping(src -> src.getProductPictures(), (dest, v) -> dest.setProductPictures((List<BinaryResourceDto>) v));
    }

    private static void addSectorTypeMap(final ModelMapper modelMapper) {
        modelMapper.createTypeMap(Sector.class, SectorDto.class)
                .addMapping(src -> {
//                    logger.info(Optional.ofNullable(src.getFullName()).orElse("Empty fullName")
//                            + " para icono con id "
//                            + Optional.ofNullable(src.getIcon()).map(icon -> icon.getId()).orElse(null));
                    return src.getIcon();
                }, (dest, v) -> dest.setIconUrl((String) v))
                .addMapping(src -> src.getMapIcon(), (dest, v) -> dest.setMapIconUrl((String) v))
                .addMapping(src -> src.getFullName(), (dest, v) -> dest.setName((String) v));
    }

    private static void addCriterionTypeMap(final ModelMapper modelMapper) {
        modelMapper.createTypeMap(Criterion.class, CriterionDto.class)
                .addMapping(src -> src.getIcon(), (dest, v) -> dest.setIconUrl((String) v));
    }

    private static void addDiscountTypeMap(final ModelMapper modelMapper) {
        modelMapper.createTypeMap(EntityDiscount.class, DiscountDto.class);
    }

    private static Converter<LocalizedString, String> getLocalizedStringConverter() {
        return new Converter<LocalizedString, String>() {
            @Override
            public String convert(final MappingContext<LocalizedString, String> context) {
                return context.getSource() == null ? null : context.getSource().getDefaultText();
            }
        };
    }

    private static <T extends ProductTag> Converter<T, String> getTextTagConverter() {
        return new Converter<T, String>() {
            @Override
            public String convert(final MappingContext<T, String> context) {
                return context.getSource() == null ? null : context.getSource().getTag();
            }
        };
    }

//	private Converter<ExternalFilterTag, String> getExternalFilterTagConverter() {
//		return new Converter<ExternalFilterTag, String>() {
//			@Override
//			public String convert(MappingContext<ExternalFilterTag, String> context) {
//				return context.getSource() == null ? null : context.getSource().getTagText();
//			}
//		};
//	}

    private Converter<BinaryResource, String> getBinaryResourceConverter() {
        return new Converter<BinaryResource, String>() {
            @Override
            public String convert(final MappingContext<BinaryResource, String> context) {
                return context.getSource() == null ? null : PamapamBackofficeServicesConfig.this.pamapamUrlHelper.getImageUrl(context.getSource().getId());
            }
        };
    }

    private Converter<BinaryResource, BinaryResourceDto> getBinaryResourceToProductPictureConverter() {
        return new Converter<BinaryResource, BinaryResourceDto>() {
            @Override
            public BinaryResourceDto convert(final MappingContext<BinaryResource, BinaryResourceDto> context) {
                return Optional.ofNullable(context.getSource()).map(binaryResource -> {
                    final BinaryResourceDto productPictureDto = new BinaryResourceDto();
                    productPictureDto.setDescription(binaryResource.getDescription());
                    productPictureDto.setPictureUrl(PamapamBackofficeServicesConfig.this.pamapamUrlHelper.getImageUrl(binaryResource.getId()));
                    return productPictureDto;
                }).orElse(null);
            }
        };
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}