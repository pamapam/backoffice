package org.pamapam.services.config;

import org.pamapam.services.dto.SectorDto;
import org.springframework.stereotype.Component;

@Component
public class PamapamSectorHelper {
    public String composeFullName(SectorDto sectorDto) {
        final StringBuilder fullNameBuilder = new StringBuilder();
        if (sectorDto.getParent() != null) {
            fullNameBuilder.append(sectorDto.getParent().getName()).append(" - ");
        }
        if (sectorDto.getName() != null) {
            fullNameBuilder.append(sectorDto.getName());
        }
        return fullNameBuilder.toString();
    }
}
