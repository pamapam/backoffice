package org.pamapam.services.config;

import org.pamapam.model.EntityScope;
import org.pamapam.services.EntityScopeService;
import org.pamapam.services.SocialEconomyEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PamapamUrlHelper {
	@Value("${session.images.path}")
	private String imagesPath;

	public String getResourcesUrl() {
		return null;
	}

	public String getImageUrl(Long binaryResourceId) {
		return this.imagesPath + String.valueOf(binaryResourceId);
	}
}
