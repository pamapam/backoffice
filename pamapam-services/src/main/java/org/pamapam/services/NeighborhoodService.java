package org.pamapam.services;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.jamgo.model.entity.Neighborhood;
import org.jamgo.model.repository.NeighborhoodRepository;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.services.dto.NeighborhoodDto;
import org.pamapam.services.dto.converters.NeighborhoodDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NeighborhoodService extends ModelService<Neighborhood, NeighborhoodDto> {

	@Autowired
	private NeighborhoodRepository neighborhoodRepository;
	@Autowired
	private SocialEconomyEntityRepository socialEconomyEntityRepository;
	@Autowired
	private NeighborhoodDtoConverter neighborhoodDtoConverter;

	public List<Neighborhood> findAll() {
		return this.neighborhoodRepository.findAll();
	}

	@Override
	public Page<Neighborhood> findAll(PageRequest pageRequest) {
		return this.neighborhoodRepository.findAll(pageRequest);
	}

	@Override
	public Neighborhood findOne(Long id) {
		return this.neighborhoodRepository.findOne(id);
	}

	public Map<Long, NeighborhoodDto> getNeighborhoodDtosFromEntityTuples(final List<Tuple> entityTuples) {
		final MultiValuedMap<Long, Long> neighborhoodIds = new HashSetValuedHashMap<>();
		entityTuples.forEach(each -> {
			final Long id = (Long) each.get("id");
			neighborhoodIds.put(id, (Long) each.get("neighborhoodId"));
		});

		final Map<Long, NeighborhoodDto> neighborhoods = new HashMap<>();
		this.socialEconomyEntityRepository.getNeighborhoodTuples(Sets.newHashSet(neighborhoodIds.values()))
				.forEach(each -> neighborhoods.put((Long) each.get("id"), this.neighborhoodDtoConverter.convert(each)));
		return neighborhoods;
	}
}
