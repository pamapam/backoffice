package org.pamapam.services.dto;

import java.util.Comparator;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "name" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class SocialEconomyNetworkDto extends ModelDto {

	public static final Comparator<SocialEconomyNetworkDto> DEFAULT_ORDER = new Comparator<SocialEconomyNetworkDto>() {
		@Override
		public int compare(SocialEconomyNetworkDto objectA, SocialEconomyNetworkDto objectB) {
			if (objectA.getName() != null && objectB.getName() != null) {
				return objectA.getName().compareTo(objectB.getName());
			} else if (objectA.getName() == null) {
				return -1;
			} else {
				return 1;
			}
		}
	};

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
