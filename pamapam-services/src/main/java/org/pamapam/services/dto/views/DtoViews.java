package org.pamapam.services.dto.views;

public class DtoViews {

	public static class DetailedList {
	}

	public static class Full extends DetailedList {
	}

	public static class Internal extends Full {
	}

}
