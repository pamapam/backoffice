package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;

import java.math.BigDecimal;

@JsonPropertyOrder({})
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SocialBalanceEntityDto {

	@JsonProperty
	@JsonView(Object.class)
	private String id;

	@JsonProperty
	@JsonView(Object.class)
	private String nif;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private String url;

	@JsonProperty
	@JsonView(Object.class)
	private String lastQuestionaryYear;

	@JsonProperty
	@JsonView(Object.class)
	private String shortName;
	@JsonProperty
	@JsonView(Object.class)
	private String description;
	@JsonProperty
	@JsonView(Object.class)
	private String web;
	@JsonProperty
	@JsonView(Object.class)
	private String address;
	@JsonProperty
	@JsonView(Object.class)
	private BigDecimal latitude;
	@JsonProperty
	@JsonView(Object.class)
	private BigDecimal longitude;
	@JsonProperty
	@JsonView(Object.class)
	private String cp;
	@JsonProperty
	@JsonView(Object.class)
	private String phone;
	@JsonProperty
	@JsonView(Object.class)
	private String email;

	@JsonProperty
	@JsonView(Object.class)
	private TownDto town;
	@JsonProperty
	@JsonView(Object.class)
	private String otherTown;

	@JsonProperty
	@JsonView(Object.class)
	private RegionDto region;

	@JsonProperty
	@JsonView(Object.class)
	private ProvinceDto province;

	@JsonProperty
	@JsonView(Object.class)
	private LegalFormDto legalForm;

	@JsonProperty
	@JsonView(Object.class)
	private SectorDto sector;
	//private Set<Association> associations;
	@JsonProperty
	@JsonView(Object.class)
	private String pamAPamUrl;

	@JsonProperty
	@JsonView(Object.class)
	private boolean hasLogo;

	@JsonProperty
	@JsonView(Object.class)
	private byte[] logo;
	@JsonProperty
	@JsonView(Object.class)
	private boolean transversal;
	//private Set<ModuleStateData> modulesState;
	@JsonProperty
	@JsonView(Object.class)
	private Boolean allowPublic;
	@JsonProperty
	@JsonView(Object.class)
	private boolean stamp = false;
	@JsonProperty
	@JsonView(Object.class)
	private boolean[] nivell = new boolean[] { false, false, false };
	@JsonProperty
	@JsonView(Object.class)
	private String creationYear;
	@JsonProperty
	@JsonView(Object.class)
	private String twitter;
	@JsonProperty
	@JsonView(Object.class)
	private String facebook;
	@JsonProperty
	@JsonView(Object.class)
	private String instagram;
	@JsonProperty
	@JsonView(Object.class)
	private String neighborhood;

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getNif() {
		return this.nif;
	}

	public void setNif(final String nif) {
		this.nif = nif;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getLastQuestionaryYear() {
		return this.lastQuestionaryYear;
	}

	public void setLastQuestionaryYear(final String lastQuestionaryYear) {
		this.lastQuestionaryYear = lastQuestionaryYear;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(final String web) {
		this.web = web;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(final BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(final BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getCp() {
		return this.cp;
	}

	public void setCp(final String cp) {
		this.cp = cp;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getOtherTown() {
		return this.otherTown;
	}

	public void setOtherTown(final String otherTown) {
		this.otherTown = otherTown;
	}

	public String getPamAPamUrl() {
		return this.pamAPamUrl;
	}

	public void setPamAPamUrl(final String pamAPamUrl) {
		this.pamAPamUrl = pamAPamUrl;
	}

	public boolean isHasLogo() {
		return this.hasLogo;
	}

	public void setHasLogo(final boolean hasLogo) {
		this.hasLogo = hasLogo;
	}

	public byte[] getLogo() {
		return this.logo;
	}

	public void setLogo(final byte[] logo) {
		this.logo = logo;
	}

	public boolean isTransversal() {
		return this.transversal;
	}

	public void setTransversal(final boolean transversal) {
		this.transversal = transversal;
	}

	public boolean isAllowPublic() {
		return this.allowPublic;
	}

	public void setAllowPublic(final boolean allowPublic) {
		this.allowPublic = allowPublic;
	}

	public boolean isStamp() {
		return this.stamp;
	}

	public void setStamp(final boolean stamp) {
		this.stamp = stamp;
	}

	public boolean[] getNivell() {
		return this.nivell;
	}

	public void setNivell(final boolean[] nivell) {
		this.nivell = nivell;
	}

	public String getCreationYear() {
		return this.creationYear;
	}

	public void setCreationYear(final String creationYear) {
		this.creationYear = creationYear;
	}

	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(final String twitter) {
		this.twitter = twitter;
	}

	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(final String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return this.instagram;
	}

	public void setInstagram(final String instagram) {
		this.instagram = instagram;
	}

	public TownDto getTown() {
		return this.town;
	}

	public void setTown(final TownDto town) {
		this.town = town;
	}

	public RegionDto getRegion() {
		return this.region;
	}

	public void setRegion(final RegionDto region) {
		this.region = region;
	}

	public ProvinceDto getProvince() {
		return this.province;
	}

	public void setProvince(final ProvinceDto province) {
		this.province = province;
	}

	public LegalFormDto getLegalForm() {
		return this.legalForm;
	}

	public void setLegalForm(final LegalFormDto legalForm) {
		this.legalForm = legalForm;
	}

	public String getNeighborhood() {
		return this.neighborhood;
	}

	public void setNeighborhood(final String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public SectorDto getSector() {
		return this.sector;
	}

	public void setSector(final SectorDto sector) {
		this.sector = sector;
	}

	//	public String getNeighborhood() {
//		return this.neighborhood;
//	}
//
//	public void setNeighborhood(final String neighborhood) {
//		this.neighborhood = neighborhood;
//	}
}
