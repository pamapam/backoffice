package org.pamapam.services.dto;

public class LocalizedStringDto {

	String text;

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return this.text;
	}
}
