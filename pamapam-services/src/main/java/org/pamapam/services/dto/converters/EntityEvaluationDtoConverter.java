package org.pamapam.services.dto.converters;

import org.pamapam.model.EntityEvaluation;
import org.pamapam.repository.EntityEvaluationRepository;
import org.pamapam.services.dto.EntityEvaluationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;

@Service
public class EntityEvaluationDtoConverter {
    @Autowired
    private EntityEvaluationRepository entityEvaluationRepository;
    @Autowired
    private EntityEvaluationCriterionConverter entityEvaluationCriterionConverter;

    public EntityEvaluationDto convert(Tuple tuple) {
        EntityEvaluation entityEvaluation = this.entityEvaluationRepository.findOne((Long) tuple.get("id"));
        EntityEvaluationDto entityEvaluationDto = new EntityEvaluationDto();
        entityEvaluationDto.setId(entityEvaluation.getId());
        entityEvaluationDto.setEntityEvaluationCriterions(this.entityEvaluationCriterionConverter.convertToEntityEvaluationCriterionDto(entityEvaluation.getEntityEvaluationCriterions()));
        return entityEvaluationDto;
    }
}
