package org.pamapam.services.dto.converters;

import org.pamapam.model.EntityEvaluationCriterion;
import org.pamapam.services.dto.EntityEvaluationCriterionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EntityEvaluationCriterionConverter {
    @Autowired
    CriterionDtoConverter criterionDtoConverter;
    public List<EntityEvaluationCriterionDto> convertToEntityEvaluationCriterionDto(List<EntityEvaluationCriterion> entityEvaluationCriteria) {
        List<EntityEvaluationCriterionDto> entityEvaluationCriterionDtoList = new ArrayList<>();
        entityEvaluationCriteria.forEach(
                each -> {
                    entityEvaluationCriterionDtoList.add(this.convertToEntityEvaluationCriterionDto(each));
                }
        );
        return entityEvaluationCriterionDtoList;
    }

    public EntityEvaluationCriterionDto convertToEntityEvaluationCriterionDto(EntityEvaluationCriterion entityEvaluationCriterion) {
        EntityEvaluationCriterionDto entityEvaluationCriterionDto = new EntityEvaluationCriterionDto();
        entityEvaluationCriterionDto.setCriterion(this.criterionDtoConverter.convertToCriterionDto(entityEvaluationCriterion.getCriterion()));
        entityEvaluationCriterionDto.setLevel(entityEvaluationCriterion.getLevel());
        return entityEvaluationCriterionDto;
    }
}
