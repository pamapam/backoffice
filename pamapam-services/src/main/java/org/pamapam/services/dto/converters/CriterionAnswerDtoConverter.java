package org.pamapam.services.dto.converters;

import org.pamapam.model.CriterionAnswer;
import org.pamapam.services.dto.CriterionAnswerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CriterionAnswerDtoConverter {
    @Autowired
    CriterionDtoConverter criterionDtoConverter;
    public List<CriterionAnswerDto> convertToCriterionAnswersDto(List<CriterionAnswer> criterionAnswers) {
        List<CriterionAnswerDto> criterionAnswerDtoList = new ArrayList<>();
        criterionAnswers.forEach(each -> {
            criterionAnswerDtoList.add(this.convertToCriterionAnswerDto(each));
        });
        return criterionAnswerDtoList;
    }

    private CriterionAnswerDto convertToCriterionAnswerDto(CriterionAnswer criterionAnswer) {
        CriterionAnswerDto criterionAnswerDto = new CriterionAnswerDto();
        criterionAnswerDto.setAnswer(criterionAnswer.getAnswer().getDefaultText());
        criterionAnswerDto.setCriterion(this.criterionDtoConverter.convertToCriterionDto(criterionAnswer.getCriterion()));
        criterionAnswerDto.setAnswerOrder(criterionAnswer.getAnswerOrder());
        return criterionAnswerDto;
    }
}
