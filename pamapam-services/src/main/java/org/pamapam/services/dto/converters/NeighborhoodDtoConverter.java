package org.pamapam.services.dto.converters;

import org.jamgo.model.entity.LocalizedString;
import org.pamapam.services.dto.NeighborhoodDto;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;

@Service
public class NeighborhoodDtoConverter {
    public NeighborhoodDto convert(final Tuple tuple) {
        final NeighborhoodDto neighborhoodDto = new NeighborhoodDto();
        neighborhoodDto.setId((Long) tuple.get("id"));
        neighborhoodDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());
        return neighborhoodDto;
    }
}
