package org.pamapam.services.dto;

import java.util.Comparator;
import java.util.List;

import org.jamgo.services.dto.ModelDto;
import org.pamapam.model.enums.EntityStatusType;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "name", "entityStatusType", "nextStatuses" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class EntityStatusDto extends ModelDto {

	public static final Comparator<EntityStatusDto> NAME_ORDER = new Comparator<EntityStatusDto>() {
		@Override
		public int compare(EntityStatusDto objectA, EntityStatusDto objectB) {
			return objectA.getName().compareTo(objectB.getName());
		}
	};

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private String entityStatusType;

	@JsonProperty
	@JsonView(EntityStatusDto.class)
	private List<EntityStatusDto> nextStatuses;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEntityStatusType() {
		return this.entityStatusType;
	}

	public void setEntityStatusType(String entityStatusType) {
		this.entityStatusType = entityStatusType;
	}

	public List<EntityStatusDto> getNextStatuses() {
		return this.nextStatuses;
	}

	public void setNextStatuses(List<EntityStatusDto> nextStatuses) {
		this.nextStatuses = nextStatuses;
	}

}
