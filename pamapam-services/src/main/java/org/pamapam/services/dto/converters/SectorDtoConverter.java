package org.pamapam.services.dto.converters;

import org.jamgo.model.entity.LocalizedString;
import org.pamapam.model.Sector;
import org.pamapam.services.config.PamapamSectorHelper;
import org.pamapam.services.config.PamapamUrlHelper;
import org.pamapam.services.dto.SectorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.Map;
import java.util.Optional;

@Service
public class SectorDtoConverter {
    @Autowired
    private PamapamUrlHelper pamapamUrlHelper;
    @Autowired
    private PamapamSectorHelper pamapamSectorHelper;

    public SectorDto convert(final Tuple tuple, final Map<Long, SectorDto> parentSectors) {
        final SectorDto sectorDto = new SectorDto();
        sectorDto.setId((Long) tuple.get("id"));
        sectorDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());
        sectorDto.setDescription(((LocalizedString) tuple.get("description")).getDefaultText());
        final Long parentId = (Long) tuple.get("parentId");
        if ((parentSectors != null) && (parentId != null)) {
            final SectorDto parentSector = parentSectors.get(parentId);
            sectorDto.setParent(parentSector);
            sectorDto.setIconUrl(parentSector.getIconUrl());
            sectorDto.setMapIconUrl(parentSector.getMapIconUrl());
        } else {
            sectorDto.setIconUrl(Optional.ofNullable(tuple.get("iconId"))
                    .map(o -> this.pamapamUrlHelper.getImageUrl((Long) o))
                    .orElse(null));
            sectorDto.setMapIconUrl(Optional.ofNullable(tuple.get("mapIconId"))
                    .map(o -> this.pamapamUrlHelper.getImageUrl((Long) o))
                    .orElse(null));
        }

        sectorDto.setFullName(this.pamapamSectorHelper.composeFullName(sectorDto));
        return sectorDto;
    }

    public SectorDto convert(Sector sector) {
        final SectorDto sectorDto = new SectorDto();
        sectorDto.setId(sector.getId());
        sectorDto.setName(sector.getName().getDefaultText());
        sectorDto.setDescription(sector.getDescription().getDefaultText());

        sectorDto.setIconUrl(Optional.ofNullable(sector.getIcon().getId())
                .map(o -> this.pamapamUrlHelper.getImageUrl((Long) o))
                .orElse(null));
        sectorDto.setMapIconUrl(Optional.ofNullable(sector.getMapIcon().getId())
                .map(o -> this.pamapamUrlHelper.getImageUrl((Long) o))
                .orElse(null));

        sectorDto.setFullName(this.pamapamSectorHelper.composeFullName(sectorDto));
        if (sector.getParent() != null) {
            final SectorDto parentSector = new SectorDto();
            parentSector.setId(sector.getParent().getId());
            sectorDto.setParent(parentSector);
        }

        return sectorDto;
    }
}
