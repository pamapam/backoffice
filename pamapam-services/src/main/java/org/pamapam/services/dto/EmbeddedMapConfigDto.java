package org.pamapam.services.dto;

import java.util.List;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "domain", "apiKey", "externalFilterTags", "statuses", "entityScopes" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class EmbeddedMapConfigDto extends ModelDto {

	@JsonProperty
	@JsonView(Object.class)
	private String domain;
	@JsonProperty
	@JsonView(Object.class)
	private String apiKey;
	@JsonProperty
	@JsonView(Object.class)
	private List<ExternalFilterTagDto> externalFilterTags;
	@JsonProperty
	@JsonView(Object.class)
	private List<EntityStatusDto> statuses;
	@JsonProperty
	@JsonView(Object.class)
	private List<EntityScopeDto> entityScopes;

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(final String domain) {
		this.domain = domain;
	}

	public String getApiKey() {
		return this.apiKey;
	}

	public void setApiKey(final String apiKey) {
		this.apiKey = apiKey;
	}

	public List<ExternalFilterTagDto> getExternalFilterTags() {
		return this.externalFilterTags;
	}

	public void setExternalFilterTags(final List<ExternalFilterTagDto> externalFilterTags) {
		this.externalFilterTags = externalFilterTags;
	}

	public List<EntityStatusDto> getStatuses() {
		return this.statuses;
	}

	public void setStatuses(final List<EntityStatusDto> statuses) {
		this.statuses = statuses;
	}

	public List<EntityScopeDto> getEntityScopes() {
		return this.entityScopes;
	}

	public void setEntityScopes(final List<EntityScopeDto> entityScopes) {
		this.entityScopes = entityScopes;
	}

}
