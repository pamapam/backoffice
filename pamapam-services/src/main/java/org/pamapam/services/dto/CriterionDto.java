package org.pamapam.services.dto;

import java.util.Comparator;
import java.util.List;

import org.jamgo.services.dto.ModelDto;
import org.pamapam.services.dto.views.DtoViews;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "name", "description", "iconUrl", "question", "criterionAnswers" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class CriterionDto extends ModelDto {

	public static final Comparator<CriterionDto> ID_ORDER = new Comparator<CriterionDto>() {
		@Override
		public int compare(CriterionDto objectA, CriterionDto objectB) {
			return objectA.getId().compareTo(objectB.getId());
		}
	};

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(DtoViews.Internal.class)
	private String question;

	@JsonProperty
	@JsonView(DtoViews.Internal.class)
	private String description;

	@JsonProperty
	@JsonView(DtoViews.DetailedList.class)
	private String iconUrl;

	@JsonProperty
	@JsonView(DtoViews.Internal.class)
	private List<CriterionAnswerDto> criterionAnswers;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public List<CriterionAnswerDto> getCriterionAnswers() {
		return this.criterionAnswers;
	}

	public void setCriterionAnswers(List<CriterionAnswerDto> criterionAnswers) {
		this.criterionAnswers = criterionAnswers;
	}

}
