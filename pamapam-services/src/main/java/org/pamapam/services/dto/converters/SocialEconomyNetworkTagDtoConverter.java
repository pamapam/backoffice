package org.pamapam.services.dto.converters;

import org.pamapam.services.dto.SocialEconomyNetworkTagDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SocialEconomyNetworkTagDtoConverter {

    public List<SocialEconomyNetworkTagDto> convertToDtoList(List<String> socialEconomyNetworksStrings) {
        List<SocialEconomyNetworkTagDto> socialEconomyNetworkTagDtoList = new ArrayList<>();
        socialEconomyNetworksStrings.forEach(
                each -> {
                    SocialEconomyNetworkTagDto socialEconomyNetworkTagDto = new SocialEconomyNetworkTagDto();
                    socialEconomyNetworkTagDto.setTag(each);
                    socialEconomyNetworkTagDtoList.add(socialEconomyNetworkTagDto);
                }
        );
        return socialEconomyNetworkTagDtoList;
    }
}
