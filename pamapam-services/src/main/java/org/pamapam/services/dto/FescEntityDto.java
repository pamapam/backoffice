package org.pamapam.services.dto;

import java.util.List;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class FescEntityDto extends ModelDto {

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private String description;

	@JsonProperty
	@JsonView(Object.class)
	private String pictureUrl;

	@JsonProperty
	@JsonView(Object.class)
	private String email;

	@JsonProperty
	@JsonView(Object.class)
	private String phone;

	@JsonProperty
	@JsonView(Object.class)
	private String web;

	@JsonProperty
	@JsonView(Object.class)
	private String twitter;

	@JsonProperty
	@JsonView(Object.class)
	private String facebook;

	@JsonProperty
	@JsonView(Object.class)
	private String googlePlus;

	@JsonProperty
	@JsonView(Object.class)
	private String instagram;

	@JsonProperty
	@JsonView(Object.class)
	private String pinterest;

	@JsonProperty
	@JsonView(Object.class)
	private String quitter;

	@JsonProperty
	@JsonView(Object.class)
	private SectorDto mainSector;

	@JsonProperty
	@JsonView(Object.class)
	private SectorDto mainFescSector;

	@JsonProperty
	@JsonView(Object.class)
	private List<SectorDto> sectors;

	@JsonProperty
	@JsonView(Object.class)
	private TownDto town;

	@JsonProperty
	@JsonView(Object.class)
	private String urlVideo;

	@JsonProperty
	@JsonView(Object.class)
	private List<DiscountDto> discounts;

	@JsonProperty
	@JsonView(Object.class)
	private List<AudiovisualDto> audiovisualDocuments;
	
	@JsonProperty
	@JsonView(Object.class)
	private List<BinaryResourceDto> productPictures;

	@JsonProperty
	@JsonView(Object.class)
	private Boolean publicAdministration;

	@JsonProperty
	@JsonView(Object.class)
	private Boolean enterprises;

	@JsonProperty
	@JsonView(Object.class)
	private Boolean privateActivists;

	@JsonProperty
	@JsonView(Object.class)
	private Boolean privateNoobs;

	@JsonProperty
	@JsonView(Object.class)
	private ModalityDto modality;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getPictureUrl() {
		return this.pictureUrl;
	}

	public void setPictureUrl(final String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(final String web) {
		this.web = web;
	}

	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(final String twitter) {
		this.twitter = twitter;
	}

	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(final String facebook) {
		this.facebook = facebook;
	}

	public String getGooglePlus() {
		return this.googlePlus;
	}

	public void setGooglePlus(final String googlePlus) {
		this.googlePlus = googlePlus;
	}

	public String getInstagram() {
		return this.instagram;
	}

	public void setInstagram(final String instagram) {
		this.instagram = instagram;
	}

	public String getPinterest() {
		return this.pinterest;
	}

	public void setPinterest(final String pinterest) {
		this.pinterest = pinterest;
	}

	public String getQuitter() {
		return this.quitter;
	}

	public void setQuitter(final String quitter) {
		this.quitter = quitter;
	}

	public SectorDto getMainSector() {
		return this.mainSector;
	}

	public void setMainSector(final SectorDto mainSector) {
		this.mainSector = mainSector;
	}

	public List<SectorDto> getSectors() {
		return this.sectors;
	}

	public void setSectors(final List<SectorDto> sectors) {
		this.sectors = sectors;
	}

	public SectorDto getMainFescSector() {
		return mainFescSector;
	}

	public void setMainFescSector(SectorDto mainFescSector) {
		this.mainFescSector = mainFescSector;
	}

	public TownDto getTown() {
		return this.town;
	}

	public void setTown(final TownDto town) {
		this.town = town;
	}

	public String getUrlVideo() {
		return this.urlVideo;
	}

	public void setUrlVideo(final String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public List<DiscountDto> getDiscounts() {
		return this.discounts;
	}

	public void setDiscounts(final List<DiscountDto> discounts) {
		this.discounts = discounts;
	}

	public List<AudiovisualDto> getAudiovisualDocuments() {
		return audiovisualDocuments;
	}
	
	public void setAudiovisualDocuments(List<AudiovisualDto> audiovisualDocuments) {
		this.audiovisualDocuments = audiovisualDocuments;
	}
	
	public List<BinaryResourceDto> getProductPictures() {
		return this.productPictures;
	}

	public void setProductPictures(final List<BinaryResourceDto> productPictures) {
		this.productPictures = productPictures;
	}

	public Boolean getPublicAdministration() {
		return this.publicAdministration;
	}

	public void setPublicAdministration(final Boolean publicAdministration) {
		this.publicAdministration = publicAdministration;
	}

	public Boolean getEnterprises() {
		return this.enterprises;
	}

	public void setEnterprises(final Boolean enterprises) {
		this.enterprises = enterprises;
	}

	public Boolean getPrivateActivists() {
		return this.privateActivists;
	}

	public void setPrivateActivists(final Boolean privateActivists) {
		this.privateActivists = privateActivists;
	}

	public Boolean getPrivateNoobs() {
		return this.privateNoobs;
	}

	public void setPrivateNoobs(final Boolean privateNoobs) {
		this.privateNoobs = privateNoobs;
	}

	public ModalityDto getModality() {
		return modality;
	}
	
	public void setModality(ModalityDto modality) {
		this.modality = modality;
	}
	
}
