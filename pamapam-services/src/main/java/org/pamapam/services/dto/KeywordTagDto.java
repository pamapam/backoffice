package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import org.jamgo.services.dto.ModelDto;

@JsonPropertyOrder({ "id", "tagText" })
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KeywordTagDto extends ModelDto {
    @JsonProperty
    @JsonView(Object.class)
    private String tagText;

    public String getTagText() {
        return this.tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }
}
