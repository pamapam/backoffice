package org.pamapam.services.dto;

import java.util.List;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "name", "regions" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class ProvinceDto extends ModelDto {

	@JsonProperty
	@JsonView(ProvinceDto.class)
	private List<RegionDto> regions;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	public List<RegionDto> getRegions() {
		return this.regions;
	}

	public void setRegions(List<RegionDto> regions) {
		this.regions = regions;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
