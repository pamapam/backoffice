package org.pamapam.services.dto;

import java.util.List;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "name", "towns", "province" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class RegionDto extends ModelDto {

	@JsonProperty
	@JsonView(RegionDto.class)
	private List<TownDto> towns;

	@JsonProperty
	@JsonView(RegionDto.class)
	private ProvinceDto provinceDto;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	public List<TownDto> getTowns() {
		return this.towns;
	}

	public void setTowns(List<TownDto> towns) {
		this.towns = towns;
	}

	public ProvinceDto getProvince() {
		return this.provinceDto;
	}

	public void setProvince(ProvinceDto provinceDto) {
		this.provinceDto = provinceDto;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
