package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.jamgo.model.entity.Territory.TerritoryType;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.enums.EntityScopeType;

import java.util.List;
import java.util.Map;

@JsonPropertyOrder({ "text", "territoryId", "territoryType", "sectorIds", "userId", "page", "size", "entityStatusTypes", "externalFilterTags" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class SocialEconomyEntitySearchDto {

	@JsonProperty
	@JsonView(Object.class)
	private String text;

	@JsonProperty
	@JsonView(Object.class)
	private Long territoryId;

	@JsonProperty
	@JsonView(Object.class)
	private TerritoryType territoryType;

	@JsonProperty
	@JsonView(Object.class)
	private List<Long> sectorIds;
	
	@JsonProperty
	@JsonView(Object.class)
	private Boolean greenCommerce;

	@JsonProperty
	@JsonView(Object.class)
	private Long userId;

	@JsonProperty
	@JsonView(Object.class)
	private Integer page;

	@JsonProperty
	@JsonView(Object.class)
	private Integer size;

	@JsonProperty
	@JsonView(Object.class)
	private String refererDomain;

	@JsonProperty
	@JsonView(Object.class)
	private String apiKey;

	@JsonProperty
	@JsonView(Object.class)
	private Boolean details;

	@JsonProperty
	@JsonView(Object.class)
	private Map<EntityScopeType, List<EntityStatus>> statusScopes;

	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public Long getTerritoryId() {
		return this.territoryId;
	}

	public void setTerritoryId(final Long territoryId) {
		this.territoryId = territoryId;
	}

	public TerritoryType getTerritoryType() {
		return this.territoryType;
	}

	public void setTerritoryType(final TerritoryType territoryType) {
		this.territoryType = territoryType;
	}

	public List<Long> getSectorIds() {
		return this.sectorIds;
	}

	public void setSectorIds(final List<Long> sectorIds) {
		this.sectorIds = sectorIds;
	}

	public Boolean isGreenCommerce() {
		return greenCommerce;
	}

	public void setGreenCommerce(Boolean greenCommerce) {
		this.greenCommerce = greenCommerce;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(final Long userId) {
		this.userId = userId;
	}

	public Integer getPage() {
		return this.page;
	}

	public void setPage(final Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return this.size;
	}

	public void setSize(final Integer size) {
		this.size = size;
	}

	public String getRefererDomain() {
		return this.refererDomain;
	}

	public void setRefererDomain(final String refererDomain) {
		this.refererDomain = refererDomain;
	}

	public String getApiKey() {
		return this.apiKey;
	}

	public void setApiKey(final String apiKey) {
		this.apiKey = apiKey;
	}

	public Boolean getDetails() {
		return this.details;
	}

	public void setDetails(final Boolean details) {
		this.details = details;
	}

	public Map<EntityScopeType, List<EntityStatus>> getStatusScopes() {
		return this.statusScopes;
	}

	public void setStatusScopes(final Map<EntityScopeType, List<EntityStatus>> statusScopes) {
		this.statusScopes = statusScopes;
	}
}
