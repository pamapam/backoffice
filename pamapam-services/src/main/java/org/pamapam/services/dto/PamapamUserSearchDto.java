package org.pamapam.services.dto;

import org.jamgo.model.entity.Territory.TerritoryType;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "name", "territoryId", "territoryType", "communityId", "page", "size" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class PamapamUserSearchDto {

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private Long territoryId;

	@JsonProperty
	@JsonView(Object.class)
	private TerritoryType territoryType;

	@JsonProperty
	@JsonView(Object.class)
	private Long communityId;

	@JsonProperty
	@JsonView(Object.class)
	private Integer page;

	@JsonProperty
	@JsonView(Object.class)
	private Integer size;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCommunityId() {
		return this.communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public Long getTerritoryId() {
		return this.territoryId;
	}

	public void setTerritoryId(Long territoryId) {
		this.territoryId = territoryId;
	}

	public TerritoryType getTerritoryType() {
		return this.territoryType;
	}

	public void setTerritoryType(TerritoryType territoryType) {
		this.territoryType = territoryType;
	}

	public Integer getPage() {
		return this.page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return this.size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

}
