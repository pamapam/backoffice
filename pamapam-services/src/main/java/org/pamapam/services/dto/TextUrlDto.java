package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import org.jamgo.services.dto.ModelDto;

@JsonPropertyOrder({ "name", "url" })
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TextUrlDto extends ModelDto {
    @JsonProperty
    @JsonView(Object.class)
    private String name;

    @JsonProperty
    @JsonView(Object.class)
    private String url;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
