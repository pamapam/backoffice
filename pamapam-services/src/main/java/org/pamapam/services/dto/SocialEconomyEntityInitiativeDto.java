package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

@JsonPropertyOrder({ "name" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class SocialEconomyEntityInitiativeDto {

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private String nif;

	@JsonProperty
	@JsonView(Object.class)
	private String description;

	@JsonProperty
	@JsonView(Object.class)
	private Long provinceId;

	@JsonProperty
	@JsonView(Object.class)
	private Long regionId;

	@JsonProperty
	@JsonView(Object.class)
	private Long townId;

	@JsonProperty
	@JsonView(Object.class)
	private Long districtId;

	@JsonProperty
	@JsonView(Object.class)
	private Long neighborhoodId;

	@JsonProperty
	@JsonView(Object.class)
	private String address;

	@JsonProperty
	@JsonView(Object.class)
	private String number;

	@JsonProperty
	@JsonView(Object.class)
	private String postalCode;

	@JsonProperty
	@JsonView(Object.class)
	private String openingHours;

	@JsonProperty
	@JsonView(Object.class)
	private String productTags;

	@JsonProperty
	@JsonView(Object.class)
	private String contactPerson;

	@JsonProperty
	@JsonView(Object.class)
	private String email;

	@JsonProperty
	@JsonView(Object.class)
	private String web;

	@JsonProperty
	@JsonView(Object.class)
	private String phone;

	@JsonProperty
	@JsonView(Object.class)
	private String twitter;

	@JsonProperty
	@JsonView(Object.class)
	private String facebook;

	@JsonProperty
	@JsonView(Object.class)
	private String googlePlus;

	@JsonProperty
	@JsonView(Object.class)
	private String instagram;

	@JsonProperty
	@JsonView(Object.class)
	private String pinterest;

	@JsonProperty
	@JsonView(Object.class)
	private String quitter;

	@JsonProperty
	@JsonView(Object.class)
	private Long mainSectorId;

	@JsonProperty
	@JsonView(Object.class)
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<Long> sectorsIds;

	@JsonProperty
	@JsonView(Object.class)
	private Long legalFormId;

	@JsonProperty
	@JsonView(Object.class)
	private List<Long> socialEconomyNetworkIds;

	@JsonProperty
	@JsonView(Object.class)
	private String otherSocialEconomyNetworks;

	@JsonProperty
	@JsonView(Object.class)
	private Integer socialBalance;

	@JsonProperty
	@JsonView(Object.class)
	private boolean userConditionsAccepted;

	@JsonProperty
	@JsonView(Object.class)
	private boolean newslettersAccepted;

	@JsonProperty
	@JsonView(Object.class)
	private String scope;

	@JsonProperty
	@JsonView(Object.class)
	private String externalFilterTags;

	@JsonProperty
	@JsonView(Object.class)
	private String picture;

	@JsonProperty
	@JsonView(Object.class)
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<TextUrlDto> polCoopLinks;

	@JsonProperty
	@JsonView(Object.class)
	private Integer foundationYear;

	public SocialEconomyEntityInitiativeDto() {
		super();
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(final String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Long getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(final Long provinceId) {
		this.provinceId = provinceId;
	}

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(final Long regionId) {
		this.regionId = regionId;
	}

	public Long getTownId() {
		return this.townId;
	}

	public void setTownId(final Long townId) {
		this.townId = townId;
	}

	public Long getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(final Long districtId) {
		this.districtId = districtId;
	}

	public Long getNeighborhoodId() {
		return this.neighborhoodId;
	}

	public void setNeighborhoodId(final Long neighborhoodId) {
		this.neighborhoodId = neighborhoodId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(final String number) {
		this.number = number;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(final String postalCode) {
		this.postalCode = postalCode;
	}

	public String getNif() {
		return this.nif;
	}

	public void setNif(final String nif) {
		this.nif = nif;
	}

	public String getOpeningHours() {
		return this.openingHours;
	}

	public void setOpeningHours(final String openingHours) {
		this.openingHours = openingHours;
	}

	public String getProductTags() {
		return this.productTags;
	}

	public void setProductTags(final String productTags) {
		this.productTags = productTags;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(final String web) {
		this.web = web;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(final String twitter) {
		this.twitter = twitter;
	}

	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(final String facebook) {
		this.facebook = facebook;
	}

	public String getGooglePlus() {
		return this.googlePlus;
	}

	public void setGooglePlus(final String googlePlus) {
		this.googlePlus = googlePlus;
	}

	public String getInstagram() {
		return this.instagram;
	}

	public void setInstagram(final String instagram) {
		this.instagram = instagram;
	}

	public String getPinterest() {
		return this.pinterest;
	}

	public void setPinterest(final String pinterest) {
		this.pinterest = pinterest;
	}

	public String getQuitter() {
		return this.quitter;
	}

	public void setQuitter(final String quitter) {
		this.quitter = quitter;
	}

	public Long getMainSectorId() {
		return this.mainSectorId;
	}

	public void setMainSectorId(final Long sectorId) {
		this.mainSectorId = sectorId;
	}

	public List<Long> getSectorsIds() {
		return this.sectorsIds;
	}

	public void setSectorsIds(final List<Long> sectorsIds) {
		this.sectorsIds = sectorsIds;
	}

	public Long getLegalFormId() {
		return this.legalFormId;
	}

	public void setLegalFormId(final Long legalFormId) {
		this.legalFormId = legalFormId;
	}

	public List<Long> getSocialEconomyNetworkIds() {
		return this.socialEconomyNetworkIds;
	}

	public void setSocialEconomyNetworkIds(final List<Long> socialEconomyNetworkIds) {
		this.socialEconomyNetworkIds = socialEconomyNetworkIds;
	}

	public Integer getSocialBalance() {
		return this.socialBalance;
	}

	public void setSocialBalance(final Integer socialBalance) {
		this.socialBalance = socialBalance;
	}

	public boolean isUserConditionsAccepted() {
		return this.userConditionsAccepted;
	}

	public void setUserConditionsAccepted(final boolean userConditionsAccepted) {
		this.userConditionsAccepted = userConditionsAccepted;
	}

	public boolean isNewslettersAccepted() {
		return this.newslettersAccepted;
	}

	public void setNewslettersAccepted(final boolean newslettersAccepted) {
		this.newslettersAccepted = newslettersAccepted;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(final String scope) {
		this.scope = scope;
	}

	public String getExternalFilterTags() {
		return this.externalFilterTags;
	}

	public void setExternalFilterTags(final String externalFilterTags) {
		this.externalFilterTags = externalFilterTags;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

	public List<TextUrlDto> getPolCoopLinks() {
		return this.polCoopLinks;
	}

	public void setPolCoopLinks(final List<TextUrlDto> polCoopLinks) {
		this.polCoopLinks = polCoopLinks;
	}

	public String getOtherSocialEconomyNetworks() {
		return this.otherSocialEconomyNetworks;
	}

	public void setOtherSocialEconomyNetworks(final String otherSocialEconomyNetworks) {
		this.otherSocialEconomyNetworks = otherSocialEconomyNetworks;
	}

	public Integer getFoundationYear() {
		return this.foundationYear;
	}

	public void setFoundationYear(final Integer foundationYear) {
		this.foundationYear = foundationYear;
	}
}
