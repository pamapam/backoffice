package org.pamapam.services.dto;

import java.util.Date;
import java.util.List;

import org.jamgo.services.dto.ModelDto;
import org.pamapam.services.dto.views.DtoViews;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "entity", "date", "user", "entityEvaluationCriterions" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class EntityEvaluationDto extends ModelDto {

	@JsonProperty
	@JsonBackReference
	@JsonView(DtoViews.DetailedList.class)
	private SocialEconomyEntityDto entity;

	@JsonProperty
	@JsonView(Object.class)
	private Date date;

	@JsonProperty
	@JsonView(Object.class)
	private PamapamUserDto user;

	@JsonProperty
	@JsonView(Object.class)
	private List<EntityEvaluationCriterionDto> entityEvaluationCriterions;

	public SocialEconomyEntityDto getEntity() {
		return this.entity;
	}

	public void setEntity(SocialEconomyEntityDto entity) {
		this.entity = entity;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public PamapamUserDto getUser() {
		return this.user;
	}

	public void setUser(PamapamUserDto user) {
		this.user = user;
	}

	public List<EntityEvaluationCriterionDto> getEntityEvaluationCriterions() {
		return this.entityEvaluationCriterions;
	}

	public void setEntityEvaluationCriterions(List<EntityEvaluationCriterionDto> entityEvaluationCriterions) {
		this.entityEvaluationCriterions = entityEvaluationCriterions;
	}

}
