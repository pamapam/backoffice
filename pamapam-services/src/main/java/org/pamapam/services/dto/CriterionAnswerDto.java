package org.pamapam.services.dto;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "criterion", "answerOrder", "answer" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class CriterionAnswerDto extends ModelDto {

	@JsonProperty
	@JsonBackReference
	@JsonView(CriterionAnswerDto.class)
	private CriterionDto criterion;

	@JsonProperty
	@JsonView(Object.class)
	private Integer answerOrder;

	@JsonProperty
	@JsonView(Object.class)
	private String answer;

	public CriterionDto getCriterion() {
		return this.criterion;
	}

	public void setCriterion(CriterionDto criterion) {
		this.criterion = criterion;
	}

	public Integer getAnswerOrder() {
		return this.answerOrder;
	}

	public void setAnswerOrder(Integer answerOrder) {
		this.answerOrder = answerOrder;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

}
