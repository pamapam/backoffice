package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.time.Instant;
import java.util.Date;

@JsonPropertyOrder({ "name" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class NewsletterRegistrationDto {

	public NewsletterRegistrationDto() {
	}

	public NewsletterRegistrationDto(final String name, final String email) {
		this.name = name;
		this.email = email;
		this.registrationDate = Date.from(Instant.now());
	}

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private String email;

	@JsonProperty
	@JsonView(Object.class)
	private Date registrationDate;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(final Date registrationDate) {
		this.registrationDate = registrationDate;
	}

}
