package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.jamgo.services.dto.ModelDto;

import java.util.Comparator;
import java.util.List;

@JsonPropertyOrder({ "id", "username", "name", "pictureUrl", "description", "surname", "email", "roles", "twitter", "facebook", "instagram", "gitlab" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class PamapamUserDto extends ModelDto {

	public static final Comparator<PamapamUserDto> NAME_ORDER = new Comparator<PamapamUserDto>() {
		@Override
		public int compare(final PamapamUserDto objectA, final PamapamUserDto objectB) {
			return objectA.getName().compareTo(objectB.getName());
		}
	};

	@JsonProperty
	@JsonView(Object.class)
	private String username;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private String pictureUrl;

	@JsonProperty
	@JsonView(Object.class)
	private String description;

	@JsonProperty
	@JsonView(Object.class)
	private String surname;

	@JsonProperty
	@JsonView(Object.class)
	private String email;

	@JsonProperty
	@JsonView(Object.class)
	private Long provinceId;

	@JsonProperty
	@JsonView(Object.class)
	private Long regionId;

	@JsonProperty
	@JsonView(Object.class)
	private Long townId;

	@JsonProperty
	@JsonView(Object.class)
	private Long districtId;
	@JsonProperty
	@JsonView(Object.class)
	private Long neighborhoodId;

	@JsonProperty
	@JsonView(Object.class)
	private List<String> roleNames;

	@JsonProperty
	@JsonView(Object.class)
	private String twitter;

	@JsonProperty
	@JsonView(Object.class)
	private String facebook;

	@JsonProperty
	@JsonView(Object.class)
	private String instagram;

	@JsonProperty
	@JsonView(Object.class)
	private String gitlab;

	@JsonProperty
	@JsonView(Object.class)
	private String fediverse;

	@JsonProperty
	@JsonView(Object.class)
	private boolean userConditionsAccepted;

	@JsonProperty
	@JsonView(Object.class)
	private boolean newslettersAccepted;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getPictureUrl() {
		return this.pictureUrl;
	}

	public void setPictureUrl(final String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public Long getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(final Long provinceId) {
		this.provinceId = provinceId;
	}

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(final Long regionId) {
		this.regionId = regionId;
	}

	public Long getTownId() {
		return this.townId;
	}

	public void setTownId(final Long townId) {
		this.townId = townId;
	}

	public Long getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(final Long districtId) {
		this.districtId = districtId;
	}

	public Long getNeighborhoodId() {
		return this.neighborhoodId;
	}

	public void setNeighborhoodId(final Long neighborhoodId) {
		this.neighborhoodId = neighborhoodId;
	}

	public List<String> getRoleNames() {
		return this.roleNames;
	}

	public void setRoleNames(final List<String> roleNames) {
		this.roleNames = roleNames;
	}

	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(final String twitter) {
		this.twitter = twitter;
	}

	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(final String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return this.instagram;
	}

	public void setInstagram(final String instagram) {
		this.instagram = instagram;
	}

	public String getGitlab() {
		return this.gitlab;
	}

	public void setGitlab(final String gitlab) {
		this.gitlab = gitlab;
	}

	public String getFediverse() {
		return this.fediverse;
	}

	public void setFediverse(final String fediverse) {
		this.fediverse = fediverse;
	}

	public boolean isUserConditionsAccepted() {
		return this.userConditionsAccepted;
	}

	public void setUserConditionsAccepted(final boolean userConditionsAccepted) {
		this.userConditionsAccepted = userConditionsAccepted;
	}

	public boolean isNewslettersAccepted() {
		return this.newslettersAccepted;
	}

	public void setNewslettersAccepted(final boolean newslettersAccepted) {
		this.newslettersAccepted = newslettersAccepted;
	}
}
