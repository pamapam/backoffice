package org.pamapam.services.dto.converters;

import org.pamapam.model.Criterion;
import org.pamapam.services.dto.CriterionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CriterionDtoConverter {
    @Autowired
    CriterionAnswerDtoConverter criterionAnswerDtoConverter;
    public CriterionDto convertToCriterionDto(Criterion criterion) {
        CriterionDto criterionDto = new CriterionDto();
        criterionDto.setName(criterion.getName().getDefaultText());
        criterionDto.setDescription(criterion.getDescription().getDefaultText());
        criterionDto.setIconUrl("/image/"+criterion.getIcon().getId());
        criterionDto.setQuestion(criterion.getQuestion().getDefaultText());
        return criterionDto;
    }
}
