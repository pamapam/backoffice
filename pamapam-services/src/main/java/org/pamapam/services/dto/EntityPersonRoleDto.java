package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "entity", "personRole", "maleCount", "femaleCount", "otherCount" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class EntityPersonRoleDto {

	@JsonProperty
	@JsonBackReference
	@JsonView(EntityPersonRoleDto.class)
	private SocialEconomyEntityDto entity;

	@JsonProperty
	@JsonView(EntityPersonRoleDto.class)
	private PersonRoleDto personRole;

	@JsonProperty
	@JsonView(Object.class)
	private Integer maleCount;

	@JsonProperty
	@JsonView(Object.class)
	private Integer femaleCount;

	@JsonProperty
	@JsonView(Object.class)
	private Integer otherCount;

	public EntityPersonRoleDto() {
		super();
	}

	public EntityPersonRoleDto(SocialEconomyEntityDto entity, PersonRoleDto personRole) {
		this();
		this.entity = entity;
		this.personRole = personRole;
	}

	public SocialEconomyEntityDto getEntity() {
		return this.entity;
	}

	public void setEntity(SocialEconomyEntityDto entity) {
		this.entity = entity;
	}

	public PersonRoleDto getPersonRole() {
		return this.personRole;
	}

	public void setPersonRole(PersonRoleDto personRole) {
		this.personRole = personRole;
	}

	public Integer getMaleCount() {
		return this.maleCount;
	}

	public void setMaleCount(Integer maleCount) {
		this.maleCount = maleCount;
	}

	public Integer getFemaleCount() {
		return this.femaleCount;
	}

	public void setFemaleCount(Integer femaleCount) {
		this.femaleCount = femaleCount;
	}

	public Integer getOtherCount() {
		return this.otherCount;
	}

	public void setOtherCount(Integer otherCount) {
		this.otherCount = otherCount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}

		EntityPersonRoleDto that = (EntityPersonRoleDto) o;

		if (this.entity != null ? !this.entity.equals(that.entity) : that.entity != null) {
			return false;
		}
		if (this.personRole != null ? !this.personRole.equals(that.personRole) : that.personRole != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (this.entity != null ? this.entity.hashCode() : 0);
		result = 31 * result + (this.personRole != null ? this.personRole.hashCode() : 0);
		return result;
	}

}
