package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import org.jamgo.services.dto.ModelDto;

import java.util.Comparator;

@JsonPropertyOrder({ "id", "name" })
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SocialEconomyNetworkTagDto extends ModelDto {
    public static final Comparator<SocialEconomyNetworkTagDto> DEFAULT_ORDER = new Comparator<SocialEconomyNetworkTagDto>() {
        @Override
        public int compare(SocialEconomyNetworkTagDto objectA, SocialEconomyNetworkTagDto objectB) {
            if (objectA.getTag() != null && objectB.getTag() != null) {
                return objectA.getTag().compareTo(objectB.getTag());
            } else if (objectA.getTag() == null) {
                return -1;
            } else {
                return 1;
            }
        }
    };

    @JsonProperty
    @JsonView(Object.class)
    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
