package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "name" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class SocialEconomyEntityProposedDto {

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@JsonProperty
	@JsonView(Object.class)
	private Long provinceId;

	@JsonProperty
	@JsonView(Object.class)
	private Long regionId;

	@JsonProperty
	@JsonView(Object.class)
	private Long townId;

	@JsonProperty
	@JsonView(Object.class)
	private Long districtId;

	@JsonProperty
	@JsonView(Object.class)
	private Long neighborhoodId;

	@JsonProperty
	@JsonView(Object.class)
	private String address;

	@JsonProperty
	@JsonView(Object.class)
	private String number;

	@JsonProperty
	@JsonView(Object.class)
	private String email;

	@JsonProperty
	@JsonView(Object.class)
	private String phone;

	@JsonProperty
	@JsonView(Object.class)
	private String web;

	@JsonProperty
	@JsonView(Object.class)
	private String description;

	@JsonProperty
	@JsonView(Object.class)
	private String proposerName;

	@JsonProperty
	@JsonView(Object.class)
	private String proposerEmail;

	@JsonProperty
	@JsonView(Object.class)
	private String comments;

	@JsonProperty
	@JsonView(Object.class)
	private String scope;

	@JsonProperty
	@JsonView(Object.class)
	private String externalFilterTags;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Long getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(final Long provinceId) {
		this.provinceId = provinceId;
	}

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(final Long regionId) {
		this.regionId = regionId;
	}

	public Long getTownId() {
		return this.townId;
	}

	public void setTownId(final Long townId) {
		this.townId = townId;
	}

	public Long getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(final Long districtId) {
		this.districtId = districtId;
	}

	public Long getNeighborhoodId() {
		return this.neighborhoodId;
	}

	public void setNeighborhoodId(final Long neighborhoodId) {
		this.neighborhoodId = neighborhoodId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(final String number) {
		this.number = number;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(final String web) {
		this.web = web;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getProposerName() {
		return this.proposerName;
	}

	public void setProposerName(final String proposerName) {
		this.proposerName = proposerName;
	}

	public String getProposerEmail() {
		return this.proposerEmail;
	}

	public void setProposerEmail(final String proposerEmail) {
		this.proposerEmail = proposerEmail;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(final String comments) {
		this.comments = comments;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(final String scope) {
		this.scope = scope;
	}

	public String getExternalFilterTags() {
		return this.externalFilterTags;
	}

	public void setExternalFilterTags(final String externalFilterTags) {
		this.externalFilterTags = externalFilterTags;
	}

}
