package org.pamapam.services.dto;

import java.util.List;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "name", "neighborhoods", "town" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class DistrictDto extends ModelDto {

	@JsonProperty
	@JsonView(DistrictDto.class)
	private List<NeighborhoodDto> neighborhoods;

	@JsonProperty
	@JsonView(DistrictDto.class)
	private TownDto townDto;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	public List<NeighborhoodDto> getNeighborhoods() {
		return this.neighborhoods;
	}

	public void setNeighborhoods(List<NeighborhoodDto> neighborhoods) {
		this.neighborhoods = neighborhoods;
	}

	public TownDto getTown() {
		return this.townDto;
	}

	public void setTown(TownDto townDto) {
		this.townDto = townDto;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
