package org.pamapam.services.dto;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "name", "district" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class NeighborhoodDto extends ModelDto {

	@JsonProperty
	@JsonView(NeighborhoodDto.class)
	private DistrictDto district;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	public DistrictDto getDistrict() {
		return this.district;
	}

	public void setDistrict(DistrictDto district) {
		this.district = district;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
