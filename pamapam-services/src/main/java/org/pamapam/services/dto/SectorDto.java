package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.jamgo.services.dto.ModelDto;
import org.pamapam.services.dto.views.DtoViews;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@JsonPropertyOrder({"id", "name", "description", "iconUrl", "mapIconUrl", "parent", "sectors", "fullName"})
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
// **PROVISIONAL** for fixing ticket #14580 punts seu hadn't sectors list due to recursive problem on bidirectional relationships with jackson annotations
// I'm not sure of if this is going to generate problems on other endpoints of the API, if so review this.
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "id")
public class SectorDto extends ModelDto {

    public static final Comparator<SectorDto> ALPHAHIERARCHICAL_ORDER = new Comparator<SectorDto>() {
        @Override
        public int compare(final SectorDto objectA, final SectorDto objectB) {
            if (Objects.equals(objectA.getParent(), objectB.getParent())) {
                return objectA.getName().compareToIgnoreCase(objectB.getName());
            } else {
                if (objectA.getParent() == null) {
                    return objectA.getId().compareTo(objectB.getParent().getId());
                } else if (objectB.getParent() == null) {
                    return objectA.getParent().getId().compareTo(objectB.getId());
                } else {
                    return objectA.getParent().getId().compareTo(objectB.getParent().getId());
                }
            }
        }
    };

    @JsonProperty
    @JsonView(Object.class)
    private String name;

    @JsonProperty
    @JsonView(Object.class)
    private String description;

    @JsonProperty
    @JsonView(Object.class)
    private String iconUrl;

    @JsonProperty
    @JsonView(Object.class)
    private String mapIconUrl;

    @JsonProperty
    @JsonView(DtoViews.Internal.class)
    private SectorDto parent;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private List<SectorDto> sectors;

    @JsonProperty
    @JsonView(Object.class)
    private String fullName;

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(final String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getMapIconUrl() {
        return this.mapIconUrl;
    }

    public void setMapIconUrl(final String mapIconUrl) {
        this.mapIconUrl = mapIconUrl;
    }

    public SectorDto getParent() {
        return this.parent;
    }

    public void setParent(final SectorDto parent) {
        this.parent = parent;
    }

    public List<SectorDto> getSectors() {
        return this.sectors;
    }

    public void setSectors(final List<SectorDto> sectors) {
        this.sectors = sectors;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

}
