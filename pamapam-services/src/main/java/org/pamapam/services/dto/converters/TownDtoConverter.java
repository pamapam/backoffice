package org.pamapam.services.dto.converters;

import org.jamgo.model.entity.LocalizedString;
import org.pamapam.services.dto.TownDto;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;

@Service
public class TownDtoConverter {
    public TownDto convert(final Tuple tuple) {
        final TownDto townDto = new TownDto();
        townDto.setId((Long) tuple.get("id"));
        townDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());

        return townDto;
    }
}
