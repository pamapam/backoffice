package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.jamgo.model.entity.Language;
import org.jamgo.services.dto.ModelDto;
import org.pamapam.services.dto.views.DtoViews;

import java.util.Date;
import java.util.List;
import java.util.Set;

@JsonPropertyOrder({"id", "nif", "name", "normalizedName", "description", "pictureUrl", "legalForm", "contactPerson", "address",
        "web", "email", "phone", "twitter", "facebook", "googlePlus", "instagram", "pinterest", "quitter", "openingHours", "mainSector",
        "sectors", "sectorCcae", "province", "region", "town", "district", "neighborhood", "socialEconomyNetworks",
        "collaborationEntities", "socialMarketFunction", "tags", "xesBalance", "language", "registryDate",
        "entityPersonRoles", "entityEvaluations", "latitude", "longitude", "entityStatus", "oldVersion"})
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialEconomyEntityDto extends ModelDto {

    @JsonProperty
    @JsonView(Object.class)
    private String nif;

    @JsonProperty
    @JsonView(Object.class)
    private String name;

    @JsonProperty
    @JsonView(Object.class)
    private String shortName;

    @JsonProperty
    @JsonView(Object.class)
    private String normalizedName;

    @JsonProperty
    @JsonView(Object.class)
    private String description;

    @JsonProperty
    @JsonView(Object.class)
    private Integer foundationYear;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private String pictureUrl;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private LegalFormDto legalForm;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String contactPerson;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private String address;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private String postalCode;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private String web;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private String onlineShoppingWeb;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private String laZonaWeb;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String email;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private String phone;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String twitter;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String facebook;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String googlePlus;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String instagram;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String pinterest;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String quitter;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String openingHours;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private SectorDto mainSector;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private Set<SectorDto> sectors;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private SectorCcaeDto sectorCcae;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private ProvinceDto province;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private RegionDto region;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private TownDto town;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private DistrictDto district;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private NeighborhoodDto neighborhood;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Set<SocialEconomyNetworkDto> socialEconomyNetworks;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Set<SocialEconomyNetworkTagDto> otherSocialEconomyNetworks;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Set<CollaborationEntityDto> collaborationEntities;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private SocialMarketFunctionDto socialMarketFunction;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private List<String> tags;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Boolean xesBalance;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Language language;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private Date registryDate;

    @JsonProperty
    @JsonView(DtoViews.Internal.class)
    private List<EntityPersonRoleDto> entityPersonRoles;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private EntityEvaluationDto entityEvaluation;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Double latitude;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Double longitude;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private EntityStatusDto entityStatus;

    @JsonProperty
    @JsonView(DtoViews.DetailedList.class)
    private Boolean oldVersion;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Set<TextUrlDto> intercooperacioLinks;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Boolean isIntercooperacio;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Set<KeywordTagDto> keywordTags;
    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private String xesBalanceUrl;

    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Boolean greenCommerce;
    
    @JsonProperty
    @JsonView(DtoViews.Full.class)
    private Boolean greenCommerceSector;
    
    public String getNif() {
        return this.nif;
    }

    public void setNif(final String nif) {
        this.nif = nif;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getNormalizedName() {
        return this.normalizedName;
    }

    public void setNormalizedName(final String normalizedName) {
        this.normalizedName = normalizedName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return this.pictureUrl;
    }

    public void setPictureUrl(final String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public LegalFormDto getLegalForm() {
        return this.legalForm;
    }

    public void setLegalForm(final LegalFormDto legalForm) {
        this.legalForm = legalForm;
    }

    public String getContactPerson() {
        return this.contactPerson;
    }

    public void setContactPerson(final String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getWeb() {
        return this.web;
    }

    public void setWeb(final String web) {
        this.web = web;
    }

    public String getOnlineShoppingWeb() {
        return this.onlineShoppingWeb;
    }

    public void setOnlineShoppingWeb(final String onlineShoppingWeb) {
        this.onlineShoppingWeb = onlineShoppingWeb;
    }

    public String getLaZonaWeb() {
        return this.laZonaWeb;
    }

    public void setLaZonaWeb(final String laZonaWeb) {
        this.laZonaWeb = laZonaWeb;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getTwitter() {
        return this.twitter;
    }

    public void setTwitter(final String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return this.facebook;
    }

    public void setFacebook(final String facebook) {
        this.facebook = facebook;
    }

    public String getGooglePlus() {
        return this.googlePlus;
    }

    public void setGooglePlus(final String googlePlus) {
        this.googlePlus = googlePlus;
    }

    public String getInstagram() {
        return this.instagram;
    }

    public void setInstagram(final String instagram) {
        this.instagram = instagram;
    }

    public String getPinterest() {
        return this.pinterest;
    }

    public void setPinterest(final String pinterest) {
        this.pinterest = pinterest;
    }

    public String getOpeningHours() {
        return this.openingHours;
    }

    public void setOpeningHours(final String openingHours) {
        this.openingHours = openingHours;
    }

    public SectorDto getMainSector() {
        return this.mainSector;
    }

    public void setMainSector(final SectorDto mainSector) {
        this.mainSector = mainSector;
    }

    public Set<SectorDto> getSectors() {
        return this.sectors;
    }

    public void setSectors(final Set<SectorDto> sectors) {
        this.sectors = sectors;
    }

    public ProvinceDto getProvince() {
        return this.province;
    }

    public void setProvince(final ProvinceDto province) {
        this.province = province;
    }

    public RegionDto getRegion() {
        return this.region;
    }

    public void setRegion(final RegionDto region) {
        this.region = region;
    }

    public TownDto getTown() {
        return this.town;
    }

    public void setTown(final TownDto town) {
        this.town = town;
    }

    public DistrictDto getDistrict() {
        return this.district;
    }

    public void setDistrict(final DistrictDto district) {
        this.district = district;
    }

    public NeighborhoodDto getNeighborhood() {
        return this.neighborhood;
    }

    public void setNeighborhood(final NeighborhoodDto neighborhood) {
        this.neighborhood = neighborhood;
    }

    public Set<SocialEconomyNetworkDto> getSocialEconomyNetworks() {
        return this.socialEconomyNetworks;
    }

    public void setSocialEconomyNetworks(final Set<SocialEconomyNetworkDto> socialEconomyNetworks) {
        this.socialEconomyNetworks = socialEconomyNetworks;
    }

    public Set<SocialEconomyNetworkTagDto> getOtherSocialEconomyNetworks() {
        return otherSocialEconomyNetworks;
    }

    public void setOtherSocialEconomyNetworks(Set<SocialEconomyNetworkTagDto> otherSocialEconomyNetworks) {
        this.otherSocialEconomyNetworks = otherSocialEconomyNetworks;
    }

    public Set<CollaborationEntityDto> getCollaborationEntities() {
        return this.collaborationEntities;
    }

    public void setCollaborationEntities(final Set<CollaborationEntityDto> collaborationEntities) {
        this.collaborationEntities = collaborationEntities;
    }

    public SocialMarketFunctionDto getSocialMarketFunction() {
        return this.socialMarketFunction;
    }

    public void setSocialMarketFunction(final SocialMarketFunctionDto socialMarketFunction) {
        this.socialMarketFunction = socialMarketFunction;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public void setTags(final List<String> tags) {
        this.tags = tags;
    }

    public Boolean getXesBalance() {
        return this.xesBalance;
    }

    public void setXesBalance(final Boolean xesBalance) {
        this.xesBalance = xesBalance;
    }

    public Language getLanguage() {
        return this.language;
    }

    public void setLanguage(final Language language) {
        this.language = language;
    }

    public Date getRegistryDate() {
        return this.registryDate;
    }

    public void setRegistryDate(final Date registryDate) {
        this.registryDate = registryDate;
    }

    public List<EntityPersonRoleDto> getEntityPersonRoles() {
        return this.entityPersonRoles;
    }

    public void setEntityPersonRoles(final List<EntityPersonRoleDto> entityPersonRoles) {
        this.entityPersonRoles = entityPersonRoles;
    }

    public EntityEvaluationDto getEntityEvaluation() {
        return this.entityEvaluation;
    }

    public void setEntityEvaluation(final EntityEvaluationDto entityEvaluation) {
        this.entityEvaluation = entityEvaluation;
    }

    public SectorCcaeDto getSectorCcae() {
        return this.sectorCcae;
    }

    public void setSectorCcae(final SectorCcaeDto sectorCcae) {
        this.sectorCcae = sectorCcae;
    }

    public EntityStatusDto getEntityStatus() {
        return this.entityStatus;
    }

    public void setEntityStatus(final EntityStatusDto entityStatus) {
        this.entityStatus = entityStatus;
    }

    public String getQuitter() {
        return this.quitter;
    }

    public void setQuitter(final String quitter) {
        this.quitter = quitter;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getOldVersion() {
        return this.oldVersion;
    }

    public void setOldVersion(final Boolean oldVersion) {
        this.oldVersion = oldVersion;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    public Set<TextUrlDto> getIntercooperacioLinks() {
        return this.intercooperacioLinks;
    }

    public void setIntercooperacioLinks(final Set<TextUrlDto> intercooperacioLinks) {
        this.intercooperacioLinks = intercooperacioLinks;
    }

    public Boolean isIntercooperacio() {
        return this.isIntercooperacio;
    }

    public void setIsIntercooperacio(final Boolean intercoopScope) {
        this.isIntercooperacio = intercoopScope;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    public Integer getFoundationYear() {
        return this.foundationYear;
    }

    public void setFoundationYear(final Integer foundationYear) {
        this.foundationYear = foundationYear;
    }

    public Set<KeywordTagDto> getKeywordTags() {
        return keywordTags;
    }

    public void setKeywordTags(Set<KeywordTagDto> keywordTags) {
        this.keywordTags = keywordTags;
    }

    public String getXesBalanceUrl() {
        return xesBalanceUrl;
    }

    public void setXesBalanceUrl(String xesBalanceUrl) {
        this.xesBalanceUrl = xesBalanceUrl;
    }
    
    public Boolean getGreenCommerce() {
        return this.greenCommerce;
    }

    public void setGreenCommerce(final Boolean greenCommerce) {
        this.greenCommerce = greenCommerce;
    }

	public Boolean getGreenCommerceSector() {
		return greenCommerceSector;
	}

	public void setGreenCommerceSector(Boolean greenCommerceSector) {
		this.greenCommerceSector = greenCommerceSector;
	}
    
}
