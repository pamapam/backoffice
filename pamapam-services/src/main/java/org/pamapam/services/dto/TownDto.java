package org.pamapam.services.dto;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.jamgo.services.dto.ModelDto;

import java.util.List;

@JsonPropertyOrder({ "id", "name", "districts", "region" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class TownDto extends ModelDto {

	@JsonProperty
	@JsonView(Object.class)
	private Long id;

	@JsonProperty
	@JsonView(TownDto.class)
	private List<DistrictDto> districts;

	@JsonProperty
	@JsonView(TownDto.class)
	private RegionDto region;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	public List<DistrictDto> getDistricts() {
		return this.districts;
	}

	public void setDistricts(final List<DistrictDto> districts) {
		this.districts = districts;
	}

	public RegionDto getRegion() {
		return this.region;
	}

	public void setRegion(final RegionDto region) {
		this.region = region;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}
}
