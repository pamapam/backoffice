package org.pamapam.services.dto;

import org.pamapam.services.dto.views.DtoViews;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "entityEvaluation", "criterion", "level", "notes" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class EntityEvaluationCriterionDto {

	@JsonProperty
	@JsonBackReference
	@JsonView(EntityEvaluationCriterionDto.class)
	private EntityEvaluationDto entityEvaluation;

	@JsonProperty
	@JsonView(Object.class)
	private CriterionDto criterion;

	@JsonProperty
	@JsonView(Object.class)
	private Integer level;

	@JsonProperty
	@JsonView(DtoViews.Internal.class)
	private String notes;

	public EntityEvaluationCriterionDto() {
		super();
	}

	public EntityEvaluationCriterionDto(EntityEvaluationDto entityEvaluation, CriterionDto criterion) {
		this();
		this.entityEvaluation = entityEvaluation;
		this.criterion = criterion;
	}

	public EntityEvaluationDto getEntityEvaluation() {
		return this.entityEvaluation;
	}

	public void setEntityEvaluation(EntityEvaluationDto entityEvaluation) {
		this.entityEvaluation = entityEvaluation;
	}

	public CriterionDto getCriterion() {
		return this.criterion;
	}

	public void setCriterion(CriterionDto criterion) {
		this.criterion = criterion;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}

		EntityEvaluationCriterionDto that = (EntityEvaluationCriterionDto) o;

		if (this.entityEvaluation != null ? !this.entityEvaluation.equals(that.entityEvaluation) : that.entityEvaluation != null) {
			return false;
		}
		if (this.criterion != null ? !this.criterion.equals(that.criterion) : that.criterion != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (this.entityEvaluation != null ? this.entityEvaluation.hashCode() : 0);
		result = 31 * result + (this.criterion != null ? this.criterion.hashCode() : 0);
		return result;
	}

}
