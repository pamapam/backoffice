package org.pamapam.services.dto.converters;

import org.jamgo.model.entity.LocalizedString;
import org.pamapam.services.dto.DistrictDto;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;

@Service
public class DistrictDtoConverter {
    public DistrictDto convert(final Tuple tuple) {
        final DistrictDto districtDto = new DistrictDto();
        districtDto.setId((Long) tuple.get("id"));
        districtDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());
        return districtDto;
    }
}
