package org.pamapam.services.dto;

import java.util.Comparator;

import org.jamgo.services.dto.ModelDto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "code", "name" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class SectorCcaeDto extends ModelDto {

	public static final Comparator<SectorCcaeDto> NAME_ORDER = new Comparator<SectorCcaeDto>() {
		@Override
		public int compare(SectorCcaeDto objectA, SectorCcaeDto objectB) {
			return objectA.getName().compareTo(objectB.getName());
		}
	};

	@JsonProperty
	@JsonView(Object.class)
	private String code;

	@JsonProperty
	@JsonView(Object.class)
	private String name;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
