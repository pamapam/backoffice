package org.pamapam.services.dto.converters;

import org.pamapam.services.dto.SocialEconomyNetworkDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class SocialEconomyNetworkDtoConverter {

    public List<SocialEconomyNetworkDto> convertToDtoList(List<String> entitySocialEconomyNetworks) {
        List<SocialEconomyNetworkDto> socialEconomyNetworkDtoList = new ArrayList<>();
        entitySocialEconomyNetworks.forEach(
                each -> {
                    SocialEconomyNetworkDto socialEconomyNetworkDto = new SocialEconomyNetworkDto();
                    socialEconomyNetworkDto.setName(each);
                    socialEconomyNetworkDtoList.add(socialEconomyNetworkDto);
                }
        );
        return socialEconomyNetworkDtoList;
    }
}
