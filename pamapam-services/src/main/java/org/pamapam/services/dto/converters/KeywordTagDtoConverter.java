package org.pamapam.services.dto.converters;

import org.pamapam.services.dto.KeywordTagDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class KeywordTagDtoConverter {
    public List<KeywordTagDto> convertToDtoList(List<String> keywordTagsString) {
        List<KeywordTagDto> keywordTagDtoList = new ArrayList<>();
        keywordTagsString.forEach(
                each -> {
                    KeywordTagDto keywordTagDto = new KeywordTagDto();
                    keywordTagDto.setTagText(each);
                    keywordTagDtoList.add(keywordTagDto);
                }
        );
        return keywordTagDtoList;
    }
}
