package org.pamapam.services.dto.converters;

import org.jamgo.model.entity.LocalizedString;
import org.pamapam.services.dto.RegionDto;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;

@Service
public class RegionDtoConverter {
    public static RegionDto convert(final Tuple tuple) {
        final RegionDto regionDto = new RegionDto();
        regionDto.setId((Long) tuple.get("id"));
        regionDto.setName(((LocalizedString) tuple.get("name")).getDefaultText());

        return regionDto;
    }

}
