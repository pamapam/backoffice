package org.pamapam.services;

import java.text.Normalizer;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.jamgo.model.entity.LocalizedString;
import org.pamapam.model.Sector;
import org.pamapam.repository.SectorRepository;
import org.pamapam.services.dto.SectorDto;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class SectorService extends ModelService<Sector, SectorDto> {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SectorService.class);

	@Autowired
	private EntityManager entityManager;
	@Autowired
	private SectorRepository sectorRepository;

	@Override
	public Page<Sector> findAll(final PageRequest pageRequest) {
		return this.sectorRepository.findAll(pageRequest);
	}

	@Override
	public Sector findOne(final Long id) {
		return this.sectorRepository.findOne(id);
	}

	/** Finds the equivalent sector from pam to pam by receiving a sector dto from Balanç.
	 * If it does not find an equivalent, it creates a new Sector from the Balanç data and returns it.
	 *
	 * @param sectorDto
	 * @return
	 */
	public Sector balanceConverter(final SectorDto sectorDto) {
		String sectorNameWithoutParenthesis = sectorDto.getName().replaceAll("\\(.*\\)", ""); //parentheses
		Sector sector = this.findByName(sectorNameWithoutParenthesis)
			.stream()
			.findFirst()
			.orElse(null);

		if (sector == null) {
			final LocalizedString localizedString = new LocalizedString();
			localizedString.add("ca", sectorDto.getName());

			sector = new Sector();
			sector.setName(localizedString);
			this.save(sector);
		}

		return sector;
	}

	private void save(final Sector sector) {
		try {
			this.sectorRepository.save(sector);
		} catch (final Exception e) {
			logger.error("Error attempting to save Sector with name {}", sector.getName(), e);
		}
	}

	public List<Sector> findByName(final String name) {
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Sector> criteriaQuery = criteriaBuilder.createQuery(Sector.class);
		final Root<Sector> root = criteriaQuery.from(Sector.class);

		final Query query = this.entityManager.createQuery(criteriaQuery);
		final List<Sector> sectors = query.getResultList();

		if (sectors.size() > 1) {
			logger.warn("Found more than one result for sector with name " + name + ".");
		}

		return sectors;
	}
	
	public List<Sector> findByNormalizedName(final String name) {
		final CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Sector> criteriaQuery = cb.createQuery(Sector.class);
		final Root<Sector> root = criteriaQuery.from(Sector.class);

		String normalizedName =  name.replaceAll("[^a-zA-Z ]", "%");
			
		Expression<Object> proximityOrder = cb.selectCase()
				.when(cb.like(root.get("name"), "\""+normalizedName+"\""), 1)
				.otherwise(2);
		
		criteriaQuery.select(root).where(cb.like(root.get("name"), "%" + normalizedName + "%"))
		.orderBy(cb.asc(proximityOrder));
		

		final Query query = this.entityManager.createQuery(criteriaQuery);
		final List<Sector> sectors = query.getResultList();

		if (sectors.size() > 1) {
			logger.warn("Found more than one result for sector with name " + name + ".");
		}

		return sectors;
	}
}
