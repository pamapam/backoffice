package org.pamapam.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.pamapam.model.SearchInfo;
import org.pamapam.repository.SearchInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class SearchInfoService {

	@Autowired
	private SearchInfoRepository searchInfoRepository;
	
	public void addSearchInfo(String text, Date date) {
		SearchInfo searchInfo = new SearchInfo();
		searchInfo.setText(text);
		searchInfo.setDate(new Timestamp(date.getTime()));
		this.searchInfoRepository.save(searchInfo);
	}

	public List<SearchInfo> getAll() {
		return this.searchInfoRepository.findAll();
	}
	
	public InputStream exportToCsv(List<SearchInfo> searchInfoList) throws IOException, JsonMappingException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		
		Workbook wb = new XSSFWorkbook();
		try {
			Sheet quantitativeSheet = wb.createSheet();
			Row headerRow = quantitativeSheet.createRow(0);
			headerRow.createCell(0).setCellValue("Text");
			headerRow.createCell(1).setCellValue("Date");
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm");
			
			int rowIndex = 1;
			for(SearchInfo searchInfo : searchInfoList) {
				Row row = quantitativeSheet.createRow(rowIndex);
				row.createCell(0).setCellValue(searchInfo.getText());
				row.createCell(1).setCellValue(sdf.format(searchInfo.getDate()));
				rowIndex++;
			}
			wb.write(os);
			return new ByteArrayInputStream(os.toByteArray());
		} finally {
			wb.close();
		}
	}
}
