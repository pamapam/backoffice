package org.pamapam.services;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.services.dto.SocialBalanceEntityDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SocialBalanceService implements InitializingBean {

	//http://localhost:8081/entities/community/9
	//http://localhost:8080/api/test-balanc

	private static final String MIME_TYPE = "application/json";

	private static final Logger logger = LoggerFactory.getLogger(SocialBalanceService.class);

	private ObjectMapper objectMapper;

	private JsonFactory jsonFactory;

	private RestTemplate restTemplate;

	@Value("${xes.socialbalance.api.domain}")
	private String envDomain;

	@Autowired
	private SocialEconomyEntityService socialEconomyEntityService;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.restTemplate = new RestTemplate();
		this.objectMapper = new ObjectMapper();
		this.jsonFactory = this.objectMapper.getFactory();
		this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	/**
	 *
	 */
	public String updateOrInsertAllPamAPamSocialBalanceEntities() {
		List<SocialBalanceEntityDto> balanceEntityDtoList = new ArrayList<>();
		String message = "action.update.socialBalance.success";
		try {
			balanceEntityDtoList = this.fetchAllSocialBalanceEntities();
		} catch (final Exception e) {
			logger.error("There was a problem when trying to fetch all social balance entities: \n\n{}", e.getMessage());
			message = "action.update.socialBalance.error";
		}
		balanceEntityDtoList.forEach(this::insertSocialBalanceEntity);
		return message;
	}

	/** Fetch all Social Balance Entities.
	 *
	 */
	public List<SocialBalanceEntityDto> fetchAllSocialBalanceEntities() throws IOException {
		//Todas las entidades de Cataluña que son activas
		final String resourceUrl = this.envDomain + "/entities/community/9/pamapam";

		logger.info("Calling to " + resourceUrl + "\n");
		// Fetch JSON response as String wrapped in ResponseEntity
		final ResponseEntity<String> response = this.restTemplate.getForEntity(resourceUrl, String.class);
		final String entitiesJson = response.getBody();

		//Convert to Dto's
		final List<SocialBalanceEntityDto> socialBalanceEntities = Arrays.asList(this.objectMapper.readValue(entitiesJson, SocialBalanceEntityDto[].class));

		//Set URL for each DTO
		return socialBalanceEntities.stream()
			.map(this::setBalanceUrl)
			.collect(Collectors.toList());
	}

	/** If entity is not already a Pam a Pam {@link SocialEconomyEntity}, insert it.
	 * If it is, add Social Balance results URL and last answered Social Balance date.
	 *
	 */
	@Transactional
	public void insertSocialBalanceEntity(final SocialBalanceEntityDto entityDto) {
		logger.trace("ENTRANDO EN INSERT SOCIAL BALANCE ENTITY");
		logger.trace("NOMBRE: " + entityDto.getName());
		if (!this.isPaPSocialEconomyEntity(entityDto)) {
			try {
				this.socialEconomyEntityService.saveSocialBalanceEntity(entityDto);
			} catch (final Exception e) {
				logger.error("Something went wrong on SocialBalanceEntity insert with name " + entityDto.getName() + " " + entityDto.getNif());
				logger.error(e.getMessage() + " \n");
			}
		} else {
			logger.trace(entityDto.getName() + " tiene nif compartido en pamapam");
			logger.trace(entityDto.getName() + " se actualiza");
			this.socialEconomyEntityService.updateUrlAndBalanceXes(entityDto);
		}
	}

	/** If the NIF is on Pam a Pam database, we consider the row as a matching entity.
	 *
	 * @return true if Pam a Pam database has a matching NIF
	 */
	public boolean isPaPSocialEconomyEntity(final SocialBalanceEntityDto entityDto) {
		final String nif = Optional.ofNullable(entityDto.getNif()).orElse("");
		final List<SocialEconomyEntity> entities = this.socialEconomyEntityService.findByNif(nif);
		return (entities != null && !entities.isEmpty());
	}

	/** Sets balance url if the entity has checked balance as public, null if it doesnt.
	 *
	 * @param socialBalanceEntity
	 * @return
	 */
	public SocialBalanceEntityDto setBalanceUrl(final SocialBalanceEntityDto socialBalanceEntity) {
		String balanceUrl = null;
		if (socialBalanceEntity.isAllowPublic()) {
			balanceUrl = this.getBalanceUrl(socialBalanceEntity);
		}
		socialBalanceEntity.setUrl(balanceUrl);

		return socialBalanceEntity;
	}

	/**
	 *
	 * @param socialBalanceEntity
	 * @return
	 */
	public String getBalanceUrl(final SocialBalanceEntityDto socialBalanceEntity) {
		String url = null;
		final String resourceUrl = this.envDomain + "/document/entity/infography/" + socialBalanceEntity.getId();

		try {
			logger.info("Calling to " + resourceUrl + "\n");
			final ResponseEntity<String> response = this.restTemplate.getForEntity(resourceUrl, String.class);
			url = response.getBody();
		} catch (final Exception e) {
			logger.error("API call to " + resourceUrl + "failed");
			logger.error(e.getMessage() + "\n");
		}
		return url;
	}

	/** Calling to the API of balanç social takes the byte array that represents the logo of an entity.
	 * Identifies the entity from its NIF.
	 *
	 * @param nif of the entity
	 * @return byte array representing the logo coming from the balanç social api
	 */
	public byte[] getEntityLogo(final String nif) {
		byte[] logo = null;
		final String resourceUrl = this.envDomain + "/entities/logo/" + nif;
		try {
			logger.info("Calling to " + resourceUrl + "\n");
			final ResponseEntity<String> response = this.restTemplate.getForEntity(resourceUrl, String.class);
			Thread.sleep(4000);
			final String body = response.getBody();

			logo = java.util.Base64.getDecoder().decode(body);
		} catch (final Exception e) {
			logger.error("API call to " + resourceUrl + "failed");
			logger.error(e.getMessage() + "\n");
		}
		return logo;
	}
}
