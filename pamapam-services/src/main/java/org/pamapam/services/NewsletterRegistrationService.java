package org.pamapam.services;

import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.pamapam.model.NewsletterRegistration;
import org.pamapam.model.PamapamUser;
import org.pamapam.repository.NewsletterRegistrationRepository;
import org.pamapam.services.dto.NewsletterRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;

@Service
public class NewsletterRegistrationService extends ModelService<NewsletterRegistration, NewsletterRegistrationDto> {

	@Autowired
	private NewsletterRegistrationRepository newsletterRegistrationRepository;
	@Autowired
	private CrudServices crudServices;
	@Autowired
	private PamapamUserService pamapamUserService;

	// ...	Find methods.

	// ...	Action methods.

	@Transactional
	public void addNewsletterRegistration(final NewsletterRegistrationDto proposedDto) throws CrudException {
		final NewsletterRegistration entity = new NewsletterRegistration();
		entity.setName(proposedDto.getName());
		entity.setEmail(proposedDto.getEmail());
		entity.setRegistrationDate(new Date());
		this.crudServices.save(entity);
		final PamapamUser user = this.pamapamUserService.findByUsername(proposedDto.getName());
		if (user != null) {
			user.setNewsletterConsentDate(Date.from(Instant.now()));
		}
	}

	@Override
	public Page<NewsletterRegistration> findAll(final PageRequest pageRequest) {
		return this.newsletterRegistrationRepository.findAll(pageRequest);
	}

	@Override
	public NewsletterRegistration findOne(final Long id) {
		return this.newsletterRegistrationRepository.findOne(id);
	}

}
