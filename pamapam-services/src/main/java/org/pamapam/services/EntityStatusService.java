package org.pamapam.services;

import org.pamapam.model.EntityStatus;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.services.dto.EntityStatusDto;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EntityStatusService extends ModelService<EntityStatus, EntityStatusDto> implements InitializingBean {
	@Autowired
	private EntityStatusRepository entityStatusRepository;

	@Override
	public Page<EntityStatus> findAll(final PageRequest pageRequest) {
		return this.entityStatusRepository.findAll(pageRequest);
	}

	@Override
	public EntityStatus findOne(final Long id) {
		return this.entityStatusRepository.findOne(id);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	public EntityStatus findByEntityStatusType(final EntityStatusType entityStatusType) {
		return this.entityStatusRepository.findFirstByEntityStatusType(entityStatusType);
	}

	public Set<EntityStatus> fescTypesWithNumbers() {
		final Set<EntityStatus> fescTypes = this.entityStatusRepository.findByEntityStatusTypeIn(EntityStatusType.getFescTypes());

		return fescTypes.stream()
			.filter(o -> o.getName().getDefaultText().matches(".*[0-9].*"))
			.collect(Collectors.toSet());
	}
}
