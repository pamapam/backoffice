package org.pamapam.services;

import org.jamgo.model.entity.*;
import org.jamgo.model.entity.Territory.TerritoryType;
import org.pamapam.services.dto.ProvinceDto;
import org.pamapam.services.dto.RegionDto;
import org.pamapam.services.dto.TerritoryDto;
import org.pamapam.services.dto.TownDto;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class TerritoryService extends ModelService<Territory, TerritoryDto> {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SectorService.class);

	@Autowired
	private EntityManager entityManager;
	@Autowired
	private ProvinceService provinceService;
	@Autowired
	private RegionService regionService;
	@Autowired
	private TownService townService;
	@Autowired
	private DistrictService districtService;
	@Autowired
	private NeighborhoodService neighborhoodService;

	public Province findProvince(final Long id) {
		return this.provinceService.findOne(id);
	}

	public Region findRegion(final Long id) {
		return this.regionService.findOne(id);
	}

	public Town findTown(final Long id) {
		return this.townService.findOne(id);
	}

	public District findDistrict(final Long id) {
		return this.districtService.findOne(id);
	}

	public Neighborhood findNeighborhood(final Long id) {
		return this.neighborhoodService.findOne(id);
	}

	public Territory findOne(final Long id, final TerritoryType type) {
		switch (type) {
			case PROVINCE:
				return this.findProvince(id);
			case REGION:
				return this.findRegion(id);
			case TOWN:
				return this.findTown(id);
			case DISTRICT:
				return this.findDistrict(id);
			case NEIGHBORHOOD:
				return this.findNeighborhood(id);
			default:
				return null;
		}
	}

	public List<Territory> findAll() {
		final List<Territory> allTerritories = new ArrayList<>();
		allTerritories.addAll(this.provinceService.findAll());
		allTerritories.addAll(this.regionService.findAll());
		allTerritories.addAll(this.townService.findAll());
		allTerritories.addAll(this.districtService.findAll());
		allTerritories.addAll(this.neighborhoodService.findAll());
		Collections.sort(allTerritories, Territory.NAME_ORDER);
		return allTerritories;
	}

	public List<TerritoryDto> findAllDto() {
		return this.convertToDto(this.findAll(), TerritoryDto.class);
	}

	public Province provinceBalanceConverter(final ProvinceDto provinceDto) {
		Province province = null;
		if (provinceDto.getName() != null) {
			//find{territory}ByName executes a LIKE statement, making all our nulls strings
			//there are places in Catalonia which contain the string "null" inside their name
			//and places that somehow match with an empty LIKE string statement
			province = this.findProvinceByName(provinceDto.getName())
				.stream()
				.findFirst()
				.orElse(null);
		}

		return province;
	}

	public Region regionBalanceConverter(final RegionDto regionDto) {
		Region region = null;
		if (regionDto.getName() != null) {
			//find{territory}ByName executes a LIKE statement, making all our nulls strings
			//there are places in Catalonia which contain the string "null" inside their name
			//and places that somehow match with an empty LIKE string statement
			region = this.findRegionByName(regionDto.getName())
				.stream()
				.findFirst()
				.orElse(null);
		}
		return region;
	}

	public Town townBalanceConverter(final TownDto townDto) {
		Town town = null;
		if (townDto.getName() != null) {
			//find{territory}ByName executes a LIKE statement, making all our nulls strings
			//there are places in Catalonia which contain the string "null" inside their name
			//and places that somehow match with an empty LIKE string statement
			town = this.findTownByName(townDto.getName())
				.stream()
				.findFirst()
				.orElse(null);
		}

		return town;
	}

	public Neighborhood neighborhoodBalanceConverter(final String neighborhoodName) {
		Neighborhood neighborhood = null;
		if (!neighborhoodName.isEmpty()) {
			//find{territory}ByName executes a LIKE statement, making all our nulls strings
			//there are places in Catalonia which contain the string "null" inside their name
			//and places that somehow match with an empty LIKE string statement
			neighborhood = this.findNeighborhoodByName(neighborhoodName)
				.stream()
				.findFirst()
				.orElse(null);
		}

		return neighborhood;
	}

	public List<Province> findProvinceByName(final String name) {
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Province> criteriaQuery = criteriaBuilder.createQuery(Province.class);
		final Root<Province> root = criteriaQuery.from(Province.class);

		criteriaQuery.select(root).where(criteriaBuilder.like(root.get("name"), "%" + name + "%"));

		final Query query = this.entityManager.createQuery(criteriaQuery);
		final List<Province> provinces = query.getResultList();

		if (provinces.size() > 1) {
			logger.warn("Found more than one result for province with name " + name + ".");
		}

		return provinces;
	}

	public List<Region> findRegionByName(final String name) {
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Region> criteriaQuery = criteriaBuilder.createQuery(Region.class);
		final Root<Region> root = criteriaQuery.from(Region.class);

		criteriaQuery.select(root).where(criteriaBuilder.like(root.get("name"), "%" + name + "%"));

		final Query query = this.entityManager.createQuery(criteriaQuery);
		final List<Region> regions = query.getResultList();

		if (regions.size() > 1) {
			logger.warn("Found more than one result for region with name " + name + ".");
		}

		return regions;
	}

	public List<Town> findTownByName(final String name) {
		final CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Town> criteriaQuery = cb.createQuery(Town.class);
		final Root<Town> root = criteriaQuery.from(Town.class);

		Expression<Object> proximityOrder = cb.selectCase()
			.when(cb.like(root.get("name"), "\""+name+"\""), 1)
			.otherwise(2);
		
		criteriaQuery.select(root).where(cb.like(root.get("name"), cb.literal("%" + name + "%")))
		.orderBy(cb.asc(proximityOrder));

		final Query query = this.entityManager.createQuery(criteriaQuery);
		final List<Town> towns = query.getResultList();
		
		

		if (towns.size() > 1) {
			logger.warn("Found more than one result for town with name " + name + ".");
		}

		return towns;
	}

	public List<Neighborhood> findNeighborhoodByName(final String name) {
		final CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Neighborhood> criteriaQuery = cb.createQuery(Neighborhood.class);
		final Root<Neighborhood> root = criteriaQuery.from(Neighborhood.class);

		Expression<Object> proximityOrder = cb.selectCase()
				.when(cb.like(root.get("name"), "\""+name+"\""), 1)
				.otherwise(2);
		
		criteriaQuery.select(root).where(cb.like(root.get("name"), "%" + name + "%"))
		.orderBy(cb.asc(proximityOrder));

		final Query query = this.entityManager.createQuery(criteriaQuery);
		final List<Neighborhood> neighborhoods = query.getResultList();

		if (neighborhoods.size() > 1) {
			logger.warn("Found more than one result for neighborhood with name " + name + ".");
		}

		return neighborhoods;
	}

	@Override
	public Page<Territory> findAll(final PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Territory findOne(final Long id) {
		// TODO Auto-generated method stub
		return null;
	}
}
