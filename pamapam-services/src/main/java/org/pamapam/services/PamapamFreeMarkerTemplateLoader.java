package org.pamapam.services;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.Instant;
import java.util.Optional;

import org.pamapam.model.MailTemplate;
import org.pamapam.repository.MailTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.cache.TemplateLoader;

@Service
public class PamapamFreeMarkerTemplateLoader implements TemplateLoader {

	@Autowired
	private MailTemplateRepository mailTemplateRepository;

	@Override
	public Object findTemplateSource(String name) throws IOException {
		MailTemplate template = this.mailTemplateRepository.findByName(name);
		return Optional.ofNullable(template).map(t -> new PamapamFreeMarkerTemplateSource(name, t.getContent(), Instant.now().getEpochSecond())).orElse(null);
	}

	@Override
	public long getLastModified(Object templateSource) {
		return ((PamapamFreeMarkerTemplateSource) templateSource).lastModified;
	}

	@Override
	public Reader getReader(Object templateSource, String encoding) throws IOException {
		return new StringReader(((PamapamFreeMarkerTemplateSource) templateSource).templateContent);
	}

	@Override
	public void closeTemplateSource(Object templateSource) throws IOException {
	}

	private static class PamapamFreeMarkerTemplateSource {
		private final String name;
		private final String templateContent;
		private final long lastModified;

		PamapamFreeMarkerTemplateSource(String name, String templateContent, long lastModified) {
			if (name == null) {
				throw new IllegalArgumentException("name == null");
			}
			if (templateContent == null) {
				throw new IllegalArgumentException("source == null");
			}
			if (lastModified < -1L) {
				throw new IllegalArgumentException("lastModified < -1L");
			}
			this.name = name;
			this.templateContent = templateContent;
			this.lastModified = lastModified;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (this.getClass() != obj.getClass()) {
				return false;
			}
			PamapamFreeMarkerTemplateSource other = (PamapamFreeMarkerTemplateSource) obj;
			if (this.name == null) {
				if (other.name != null) {
					return false;
				}
			} else if (!this.name.equals(other.name)) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return this.name;
		}

	}

}
