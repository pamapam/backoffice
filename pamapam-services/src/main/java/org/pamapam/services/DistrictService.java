package org.pamapam.services;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.jamgo.model.entity.District;
import org.jamgo.model.repository.DistrictRepository;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.services.dto.DistrictDto;
import org.pamapam.services.dto.converters.DistrictDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DistrictService extends ModelService<District, DistrictDto> {

	@Autowired
	private DistrictRepository districtRepository;
	@Autowired
	private SocialEconomyEntityRepository socialEconomyEntityRepository;
	@Autowired
	private DistrictDtoConverter districtDtoConverter;

	public List<District> findAll() {
		return this.districtRepository.findAll();
	}

	@Override
	public Page<District> findAll(PageRequest pageRequest) {
		return this.districtRepository.findAll(pageRequest);
	}

	@Override
	public District findOne(Long id) {
		return this.districtRepository.findOne(id);
	}

	public Map<Long, DistrictDto> getDistrictDtosFromEntityTuples(final List<Tuple> entityTuples) {
		final MultiValuedMap<Long, Long> districtIds = new HashSetValuedHashMap<>();
		entityTuples.forEach(each -> {
			final Long id = (Long) each.get("id");
			districtIds.put(id, (Long) each.get("districtId"));
		});

		final Map<Long, DistrictDto> districts = new HashMap<>();
		this.socialEconomyEntityRepository.getDistrictTuples(Sets.newHashSet(districtIds.values()))
				.forEach(each -> districts.put((Long) each.get("id"), this.districtDtoConverter.convert(each)));
		return districts;
	}
}
