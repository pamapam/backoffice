package org.pamapam.services;


import org.pamapam.model.Questionaire;
import org.pamapam.repository.QuestionaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class QuestionaireService extends ModelService<Questionaire, Questionaire>{
    @Autowired
    QuestionaireRepository questionaireRepository;

    @Override
    public Page<Questionaire> findAll(PageRequest pageRequest) {
        return null;
    }

    @Override
    public Questionaire findOne(Long id) {
        return null;
    }

    /** This method will work only while we keep having just two types of {@link Questionaire}.
     * All its iterations must be refactored if we have more than two types.
     *
     * @param questionaire
     * @return
     */
    public Questionaire findTheOtherQuestionaire(Questionaire questionaire) {
        List<Questionaire> questionaires = this.questionaireRepository.findAll();

        return questionaires.stream()
                .filter(q -> !q.equals(questionaire))
                .findFirst()
                .orElse(null);
    }
}
