package org.pamapam.services;

import com.google.common.collect.Lists;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jamgo.services.message.LocalizedMessageService;
import org.pamapam.exception.EmptyEmailException;
import org.pamapam.model.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

@Service
public class MailService {

	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private Configuration freemarkerConfiguration;
	@Autowired
	private LocalizedMessageService messagesSource;

	@Value("${mail.from.info}")
	private String fromInfo;
	@Value("${mail.from.register}")
	private String fromRegister;
	@Value("${mail.from.fesc.register}")
	private String fromFescRegister;
	@Value("${mail.enabled}")
	private Boolean enabled;
	@Value("${mail.images.url}")
	private String imagesUrl;
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MailService.class);

	public class TemplateInfo {
		private String confidentialityTitle;
		private String confidentialityMessage;
		private String footer;
		private String imagesUrl;
		protected Logger logger;

		public TemplateInfo() {

		}

		public String getConfidentialityTitle() {
			return this.confidentialityTitle;
		}

		public void setConfidentialityTitle(final String confidentialityTitle) {
			this.confidentialityTitle = confidentialityTitle;
		}

		public String getConfidentialityMessage() {
			return this.confidentialityMessage;
		}

		public void setConfidentialityMessage(final String confidentialityMessage) {
			this.confidentialityMessage = confidentialityMessage;
		}

		public String getFooter() {
			return this.footer;
		}

		public void setFooter(final String footer) {
			this.footer = footer;
		}

		public String getImagesUrl() {
			return this.imagesUrl;
		}

		public void setImagesUrl(final String imagesUrl) {
			this.imagesUrl = imagesUrl;
		}
	}

	private TemplateInfo getTemplateInfo() {
		final TemplateInfo templateInfo = new TemplateInfo();
		templateInfo.setConfidentialityTitle(this.messagesSource.getMessage("mail.confidentiality.title"));
		templateInfo.setConfidentialityMessage(this.messagesSource.getMessage("mail.confidentiality.body"));
		templateInfo.setFooter(this.messagesSource.getMessage("mail.footer"));
		templateInfo.setImagesUrl(this.imagesUrl);
		return templateInfo;
	}

	public void sendEntityActionMail(final SocialEconomyEntity entity, final PamapamUser owner, final PamapamUser user, final NotificationConfig notificationConfig) throws MailException {
		this.sendEntityActionMail(entity, owner, Lists.newArrayList(user), notificationConfig);
	}

	public void sendEntityActionMail(final SocialEconomyEntity entity, final PamapamUser owner, final List<PamapamUser> users, final NotificationConfig notificationConfig) throws MailException {
		final List<Address> addresses = new ArrayList<>();
		for (final PamapamUser eachUser : users) {
			if (!StringUtils.isBlank(eachUser.getEmail())) {
				try {
					addresses.add(new InternetAddress(eachUser.getEmail()));
				} catch (final AddressException e) {
					// ...	Do nothing, ignore mail
				}
			}
		}
		if (!addresses.isEmpty()) {
			this.sendEntityActionMail(entity, owner, addresses.toArray(new Address[0]), notificationConfig);
		}
	}

	public void sendEntityActionMail(final SocialEconomyEntity entity, final PamapamUser owner, final Address[] addresses, final NotificationConfig notificationConfig) throws MailException {
		final MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void prepare(final MimeMessage message) throws Exception {
				if (addresses.length > 1) {
					message.addRecipients(Message.RecipientType.BCC, addresses);
				} else {
					message.addRecipients(Message.RecipientType.TO, addresses);
				}
				message.setFrom(MailService.this.fromInfo);
				message.setSubject(Optional.ofNullable(notificationConfig.getMailSubject()).map(o -> o.getDefaultText()).orElse(""));
				message.setSentDate(new java.util.Date());
				final Map model = new HashMap<>();
				model.put("templateInfo", MailService.this.getTemplateInfo());
				model.put("userName", Optional.ofNullable(owner).map(value -> value.getFullName()).orElse(""));
				model.put("entityName", entity.getName());
				model.put("entityDescription", Optional.ofNullable(entity.getDescription()).map(o -> o.getDefaultText()).orElse(""));
				model.put("entityStatus", entity.getEntityStatus().getName().getDefaultText());

				// ...	Hola Xinxeta
				model.put("welcome", MailService.this.messagesSource.getMessage("mail.entity.updated.welcome"));

				// ...	S'ha actualitzat un punt amb les següents dades:
				model.put("intro", MailService.this.messagesSource.getMessage("mail.entity.updated.intro"));

				model.put("name", MailService.this.messagesSource.getMessage("mail.entity.updated.name"));
				model.put("description", MailService.this.messagesSource.getMessage("mail.entity.updated.description"));
				model.put("status", MailService.this.messagesSource.getMessage("mail.entity.updated.status"));

				model.put("paragraph1", Optional.ofNullable(notificationConfig.getMailText()).map(o -> o.getDefaultText()).orElse(""));

				final String templateName = Optional.ofNullable(notificationConfig.getMailTemplate()).map(t -> t.getName()).orElse("");
				message.setContent(MailService.this.getMultiPart(message, templateName, model), "text/html; charset=utf-8");
			}
		};

		if (this.enabled) {
			this.mailSender.send(preparator);
		} else {
			final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
			try {
				preparator.prepare(mimeMessage);
				final OutputStream mailOutputStream = FileUtils.openOutputStream(new File("/tmp/mail-" + owner.getUsername() + "-" + String.valueOf((new Date()).getTime())));
				mimeMessage.writeTo(mailOutputStream);
				mailOutputStream.flush();
				mailOutputStream.close();
			} catch (final Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void sendUserCreatedMail(final PamapamUser user, final String password, final NotificationConfig notificationConfig) throws MailException {
		String userSection = null;
		if (user.isInitiative()) {
			userSection = "initiativeUser";
		} else {
			userSection = "user";
		}
		this.sendUserCreatedMail(user, password, notificationConfig, notificationConfig.getMailSubject().getDefaultText(), this.fromInfo, "mail." + userSection);
	}

	public void sendFescUserCreatedMail(final PamapamUser user, final String password, final NotificationConfig notificationConfig) throws MailException {
		this.sendUserCreatedMail(user, password, notificationConfig, this.messagesSource.getMessage("mail.fesc.created.subject"), this.fromFescRegister, "mail.fesc");
	}

	public void sendAutoproposedInitiativeWelcomeNotification(final PamapamUser user, final NotificationConfig notificationConfig) {
		this.sendWelcomeEmail(user, notificationConfig, notificationConfig.getMailSubject().getDefaultText(), this.fromInfo);
	}

	public void sendWelcomeEmail(final PamapamUser user, final NotificationConfig notificationConfig, final String mailSubject, final String mailFrom) throws MailException {
		final MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void prepare(final MimeMessage message) throws Exception {
				message.addRecipients(Message.RecipientType.TO, user.getEmail());
				message.setFrom(mailFrom);
				message.setSubject(mailSubject);
				message.setSentDate(new java.util.Date());

				final Map model = new HashMap<>();
				model.put("templateInfo", MailService.this.getTemplateInfo());
				model.put("paragraph1", MailService.this.messagesSource.getLocalizedMessage("mail.initiativeUser.autoproposal.welcome").getDescription());

				final String templateName = Optional.ofNullable(notificationConfig.getMailTemplate()).map(MailTemplate::getName).orElse("");
				message.setContent(MailService.this.getMultiPart(message, templateName, model), "text/html; charset=utf-8");
			}
		};

		if (this.enabled) {
			this.mailSender.send(preparator);
		} else {
			final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
			try {
				preparator.prepare(mimeMessage);
				final OutputStream mailOutputStream = FileUtils.openOutputStream(new File("/tmp/mail-" + user.getUsername() + "-" + String.valueOf((new Date()).getTime())));
				mimeMessage.writeTo(mailOutputStream);
				mailOutputStream.flush();
				mailOutputStream.close();
			} catch (final Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void sendUserCreatedMail(final PamapamUser user, final String password, final NotificationConfig notificationConfig, final String mailSubject, final String mailFrom, final String mailPropertiesPrefix) throws MailException {
		final String welcome = this.messagesSource.getMessage(mailPropertiesPrefix + ".created.welcome");
		final String intro = this.messagesSource.getMessage(mailPropertiesPrefix + ".created.intro");
		final String userNameLabel = this.messagesSource.getMessage(mailPropertiesPrefix + ".created.userNameLabel");
		final String instructions = this.messagesSource.getLocalizedMessage(mailPropertiesPrefix + ".created.instructions").getDescription();
		final MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void prepare(final MimeMessage message) throws Exception {
				message.addRecipients(Message.RecipientType.TO, user.getEmail());
				message.setFrom(mailFrom);
				message.setSubject(mailSubject);
				message.setSentDate(new java.util.Date());

				final Map model = new HashMap<>();
				model.put("templateInfo", MailService.this.getTemplateInfo());
				model.put("userName", user.getUsername());
				model.put("name", user.getFullName());
				model.put("welcome", welcome);
				model.put("intro", intro);
				model.put("userNameLabel", userNameLabel);
				model.put("passwordLabel", MailService.this.messagesSource.getMessage("mail.password.reset.passwordLabel"));
				model.put("password", password);
				model.put("instructions", instructions);
				model.put("paragraph1", "");

				final String templateName = Optional.ofNullable(notificationConfig.getMailTemplate())
					.map(t -> t.getName())
					.orElse("");

				message.setContent(MailService.this.getMultiPart(message, templateName, model), "text/html; charset=utf-8");
			}
		};

		if (this.enabled) {
			this.mailSender.send(preparator);
		} else {
			final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
			try {
				preparator.prepare(mimeMessage);
				final OutputStream mailOutputStream = FileUtils.openOutputStream(new File("/tmp/mail-" + user.getUsername() + "-" + String.valueOf((new Date()).getTime())));
				mimeMessage.writeTo(mailOutputStream);
				mailOutputStream.flush();
				mailOutputStream.close();
			} catch (final Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void sendPasswordResetMail(final PamapamUser user, final String password, final NotificationConfig notificationConfig) throws MailException {
		final MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void prepare(final MimeMessage message) throws Exception {
				message.addRecipients(Message.RecipientType.TO, user.getEmail());
				message.setFrom(MailService.this.fromInfo);
				message.setSubject(notificationConfig.getMailSubject().getDefaultText());
//				message.setSubject(MailService.this.messagesSource.getMessage("mail.password.reset.subject"));
				message.setSentDate(new java.util.Date());

				final Map model = new HashMap<>();
				model.put("templateInfo", MailService.this.getTemplateInfo());
				model.put("userName", user.getUsername());
				model.put("welcome", MailService.this.messagesSource.getMessage("mail.password.reset.welcome"));
				model.put("intro", MailService.this.messagesSource.getMessage("mail.password.reset.intro"));
				model.put("userNameLabel", MailService.this.messagesSource.getMessage("mail.password.reset.userNameLabel"));
				model.put("passwordLabel", MailService.this.messagesSource.getMessage("mail.password.reset.passwordLabel"));
				model.put("password", password);
				model.put("instructions", MailService.this.messagesSource.getMessage("mail.password.reset.instructions"));
				model.put("paragraph1", "");

				final String templateName = Optional.ofNullable(notificationConfig.getMailTemplate()).map(t -> t.getName()).orElse("");
				message.setContent(MailService.this.getMultiPart(message, templateName, model), "text/html; charset=utf-8");
//				message.setContent(MailService.this.getMultiPart(message, "password-reset.html", model), "text/html; charset=utf-8");
			}
		};

		if (this.enabled) {
			this.mailSender.send(preparator);
		} else {
			final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
			try {
				preparator.prepare(mimeMessage);
				final OutputStream mailOutputStream = FileUtils.openOutputStream(new File("/tmp/mail-" + user.getUsername() + "-" + String.valueOf((new Date()).getTime())));
				mimeMessage.writeTo(mailOutputStream);
				mailOutputStream.flush();
				mailOutputStream.close();
			} catch (final Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void sendConditionsAcceptanceMail(final UserActionToken userActionToken) throws EmptyEmailException {
		if (userActionToken.getEmail().isEmpty()) {
			throw new EmptyEmailException(this.messagesSource.getMessage("email.fail.empty"));
		}
		final MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void prepare(final MimeMessage message) throws Exception {
				message.addRecipients(Message.RecipientType.TO, userActionToken.getEmail());
				message.setFrom(MailService.this.fromInfo);
				message.setSubject(MailService.this.messagesSource.getMessage("mail.terms&conditions.accept.subject"));
				message.setSentDate(new java.util.Date());

				final Map model = new HashMap<>();
				model.put("userId", userActionToken.getUserId());
				model.put("userName", userActionToken.getUsername());
				model.put("userToken", userActionToken.getToken());

				final String templateName = "acceptance-user-conditions.html";
				message.setContent(MailService.this.getMultiPart(message, templateName, model), "text/html; charset=utf-8");
			}
		};

		final Timestamp now = Timestamp.from(Instant.now());
		if (this.enabled && now.before(userActionToken.getValidTo())) {
			this.logger.info("Sending email to " + userActionToken.getEmail());
			this.mailSender.send(preparator);
		} else {
			final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
			try {
				preparator.prepare(mimeMessage);
				final OutputStream mailOutputStream = FileUtils.openOutputStream(new File("/tmp/mail-" + userActionToken.getUsername() + "-" + String.valueOf((new Date()).getTime())));
				mimeMessage.writeTo(mailOutputStream);
				mailOutputStream.flush();
				mailOutputStream.close();
			} catch (final Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private Multipart getMultiPart(final MimeMessage message, final String templatePath, final Map<String, Object> model) throws IOException, TemplateException, MessagingException {
		final MimeMultipart multipart = new MimeMultipart("related");

		final BodyPart messageBodyPart = new MimeBodyPart();
		final String text = FreeMarkerTemplateUtils.processTemplateIntoString(MailService.this.freemarkerConfiguration.getTemplate(templatePath), model);
		message.setText(text);
		messageBodyPart.setContent(text, "text/html; charset=utf-8");
		multipart.addBodyPart(messageBodyPart);

//		multipart.addBodyPart(MailService.this.getBodyPart("logo", "images/logo.png", "image/png"));
		return multipart;
	}

	private static BodyPart getBodyPart(final String contentId, final String resourcePath, final String contentType) throws MessagingException, IOException {
		final BodyPart logoBodyPart = new MimeBodyPart();
		logoBodyPart.setHeader("Content-ID", "<" + contentId + ">");
		final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath);
		final DataSource fds = new ByteArrayDataSource(is, contentType);
		logoBodyPart.setDataHandler(new DataHandler(fds));
		return logoBodyPart;
	}
}
