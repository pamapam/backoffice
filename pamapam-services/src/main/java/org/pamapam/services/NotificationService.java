package org.pamapam.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.pamapam.model.NotificationConfig;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.model.enums.EntityAction;
import org.pamapam.model.enums.NotificationType;
import org.pamapam.model.enums.UserEntityLinkType;
import org.pamapam.repository.NotificationConfigRepository;
import org.pamapam.repository.PamapamUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

	@Autowired
	private MailService mailService;
	@Autowired
	private NotificationConfigRepository notificationConfigRepository;
	@Autowired
	private PamapamUserRepository userRepository;

//	private List<NotificationConfig> notificationConfigs;

	public void changedSocialEconomyEntity(final SocialEconomyEntity entity, final EntityAction entityAction) {
		try {
			final List<NotificationConfig> notificationConfigs = this.notificationConfigRepository.findByNotificationType(NotificationType.ENTITY_ACTION);
			if (entity.getRegistryUser() != null) {
				this.sendOwnerNotifications(entity, entityAction, notificationConfigs);
			}
			this.sendOthersNotifications(entity, entityAction, notificationConfigs);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private void sendOwnerNotifications(final SocialEconomyEntity entity, final EntityAction entityAction, final List<NotificationConfig> notificationConfigs) {
		final PamapamUser user = entity.getRegistryUser();
		List<NotificationConfig> matchingNotificationConfigs = new ArrayList<>();
		matchingNotificationConfigs = notificationConfigs.stream()
			.filter(each -> {
				return UserEntityLinkType.OWNER.equals(each.getUserEntityLinkType())
					&& CollectionUtils.containsAny(user.getRoles(), each.getRoles())
					&& each.getEntityActions().contains(entityAction);
			})
			.collect(Collectors.toList());
		if (entityAction.equals(EntityAction.UPDATE_STATUS)) {
			matchingNotificationConfigs = matchingNotificationConfigs.stream()
				.filter(each -> each.getEntityStatuses().contains(entity.getEntityStatus()))
				.collect(Collectors.toList());
		}
		for (final NotificationConfig eachNotificationConfig : matchingNotificationConfigs) {
			this.mailService.sendEntityActionMail(entity, user, user, eachNotificationConfig);
		}
	}

	private void sendOthersNotifications(final SocialEconomyEntity entity, final EntityAction entityAction, final List<NotificationConfig> notificationConfigs) {
		List<NotificationConfig> matchingNotificationConfigs = new ArrayList<>();
		matchingNotificationConfigs = notificationConfigs.stream()
			.filter(each -> {
				return UserEntityLinkType.UNRELATED.equals(each.getUserEntityLinkType())
					&& each.getEntityActions().contains(entityAction);
			})
			.collect(Collectors.toList());
		if (entityAction.equals(EntityAction.UPDATE_STATUS)) {
			matchingNotificationConfigs = matchingNotificationConfigs.stream()
				.filter(each -> each.getEntityStatuses().contains(entity.getEntityStatus()))
				.collect(Collectors.toList());
		}
		for (final NotificationConfig eachNotificationConfig : matchingNotificationConfigs) {
			final List<PamapamUser> notifiableUsers = this.userRepository.findByRoles(eachNotificationConfig.getRoles());
			this.mailService.sendEntityActionMail(entity, entity.getRegistryUser(), notifiableUsers, eachNotificationConfig);
		}
	}

	public void sendUserCreatedNotification(final PamapamUser user, final String password) {
		final List<NotificationConfig> matchingNotificationConfigs = this.notificationConfigRepository.findByNotificationType(NotificationType.ADD_USER);
		for (final NotificationConfig eachNotificationConfig : matchingNotificationConfigs) {
			if (CollectionUtils.containsAny(user.getRoles(), eachNotificationConfig.getRoles())) {
				this.mailService.sendUserCreatedMail(user, password, eachNotificationConfig);
			}
		}
	}

	public void sendFescUserCreatedNotification(final PamapamUser user, final String password) {
		final List<NotificationConfig> matchingNotificationConfigs = this.notificationConfigRepository.findByNotificationType(NotificationType.ADD_USER);
		for (final NotificationConfig eachNotificationConfig : matchingNotificationConfigs) {
			if (CollectionUtils.containsAny(user.getRoles(), eachNotificationConfig.getRoles())) {
				this.mailService.sendFescUserCreatedMail(user, password, eachNotificationConfig);
			}
		}
	}

	public void sendPasswordResetNotification(final PamapamUser user, final String password) {
		final List<NotificationConfig> matchingNotificationConfigs = this.notificationConfigRepository.findByNotificationType(NotificationType.RESET_PASSWORD);
		for (final NotificationConfig eachNotificationConfig : matchingNotificationConfigs) {
			if (CollectionUtils.containsAny(user.getRoles(), eachNotificationConfig.getRoles())) {
				this.mailService.sendPasswordResetMail(user, password, eachNotificationConfig);
			}
		}
	}

	public void sendAutoproposedInitiativeWelcomeNotification(final PamapamUser user, final String password) {
		final List<NotificationConfig> matchingNotificationConfigs = this.notificationConfigRepository.findByNotificationType(NotificationType.ENTITY_ACTION);
		for (final NotificationConfig eachNotificationConfig : matchingNotificationConfigs) {
			if (CollectionUtils.containsAny(user.getRoles(), eachNotificationConfig.getRoles())) {
				this.mailService.sendAutoproposedInitiativeWelcomeNotification(user, eachNotificationConfig);
			}
		}
	}

//	public List<NotificationConfig> getNotificationConfigs(NotificationType notificationType) {
//		if (this.notificationConfigs == null) {
//			this.notificationConfigs = this.notificationConfigRepository.findByNotificationType(notificationType);
//		}
//		return this.notificationConfigs;
//	}

//	public void setNotificationConfigs(List<NotificationConfig> notificationConfigs) {
//		this.notificationConfigs = notificationConfigs;
//	}

}
