package org.pamapam.services;

import java.util.List;

import org.pamapam.model.FescRegistrationFee;
import org.pamapam.model.FescRegistrationModality;
import org.pamapam.repository.FescRegistrationFeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FescRegistrationFeeService {

	@Autowired
	private FescRegistrationFeeRepository repository;

	public List<FescRegistrationFee> findAll() {
		return this.repository.findAll();
	}

	public List<FescRegistrationFee> findByModality(final FescRegistrationModality modality) {
		return this.repository.findByModalities(modality);
	}

}
