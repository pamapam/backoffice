package org.pamapam.services;

import java.util.List;

import org.pamapam.model.KeywordTag;
import org.pamapam.repository.KeywordTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KeywordTagService {

	@Autowired
	private KeywordTagRepository keywordTagRepository;

	public KeywordTag findOrCreate(final String tag) {
		KeywordTag matchingTag = this.keywordTagRepository.findByTag(tag).stream().findFirst().orElse(null);
		if (matchingTag == null) {
			matchingTag = new KeywordTag();
			matchingTag.setTag(tag);
			matchingTag = this.keywordTagRepository.save(matchingTag);
		}
		return matchingTag;
	}

	public List<KeywordTag> findAll() {
		return this.keywordTagRepository.findAll();
	}
}
