package org.pamapam.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import org.pamapam.model.SocialEconomyEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class GeoLocationService {
	
	private static final Logger logger = LoggerFactory.getLogger(GeoLocationService.class);

	private ObjectMapper mapper = new ObjectMapper();

	private static String SERVICE_URL = "https://nominatim.openstreetmap.org/search?";
	private static String FORMAT_PARAMETER = "&format=json";
    public void retrieveCoordinatesFromOpenStreetApi(String address, String postalCode, String town, SocialEconomyEntity entity) throws IOException, InterruptedException {
    	try {
    	TimeUnit.SECONDS.sleep(1);
    	String addressRequestString = URLEncoder.encode(address, "UTF-8");
    	URL url = new URL(SERVICE_URL 
    			+ "street="+addressRequestString 
    			+ "&postalCode=" + postalCode
    			+ "&city=" + town
    			+ FORMAT_PARAMETER);
    	HttpURLConnection con = (HttpURLConnection) url.openConnection();
    	con.setRequestMethod("GET");
    	con.setRequestProperty("http.referer", "test.comerce-importer.cat");
    	con.setRequestProperty("Content-Type", "application/json");
    	con.setConnectTimeout(5000);
    	con.setReadTimeout(5000);
    	
    	int status = con.getResponseCode();

    	BufferedReader in = new BufferedReader(
    	  new InputStreamReader(con.getInputStream()));
    	String inputLine;
    	StringBuffer content = new StringBuffer();
    	while ((inputLine = in.readLine()) != null) {
    	    content.append(inputLine);
    	}
    	in.close();
    	con.disconnect();
    	
    	JsonNode[] geoPlaces = mapper.readValue(content.toString(), JsonNode[].class);
    	if (geoPlaces.length == 0) {
    		return;
    	}
    	JsonNode geoPlace = geoPlaces[0];
    	Double latitude = geoPlace.get("lat").asDouble();
    	Double longitude = geoPlace.get("lon").asDouble();
    	entity.setLatitude(latitude);
    	entity.setLongitude(longitude);
    	} catch (IOException e) {
    		logger.error("Could not retrieve geolocation for " + entity.getNif());
    		logger.error(e.getMessage());
    	}
    	
    }}
