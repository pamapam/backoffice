package org.pamapam.services;

import org.pamapam.model.SocialEconomyNetwork;
import org.pamapam.repository.SocialEconomyNetworkRepository;
import org.pamapam.services.dto.SocialEconomyNetworkDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class SocialEconomyNetworkService extends ModelService<SocialEconomyNetwork, SocialEconomyNetworkDto> {

	@Autowired
	private SocialEconomyNetworkRepository socialEconomyNetworkRepository;

	@Override
	public Page<SocialEconomyNetwork> findAll(final PageRequest pageRequest) {
		return this.socialEconomyNetworkRepository.findAll(pageRequest);
	}

	@Override
	public SocialEconomyNetwork findOne(final Long id) {
		return this.socialEconomyNetworkRepository.findOne(id);
	}
}
