package org.pamapam.services;

import org.pamapam.model.SocialEconomyNetworkTag;
import org.pamapam.repository.SocialEconomyNetworkTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class SocialEconomyNetworkTagService {

	@Autowired
	private SocialEconomyNetworkTagRepository socialEconomyNetworkTagRepository;

	public SocialEconomyNetworkTag findOrCreate(final String tag) {
		SocialEconomyNetworkTag matchingTag = this.socialEconomyNetworkTagRepository.findByTag(tag)
			.stream()
			.findFirst()
			.orElse(null);

		if (matchingTag == null) {
			matchingTag = new SocialEconomyNetworkTag();
			matchingTag.setTag(tag);
			matchingTag = this.socialEconomyNetworkTagRepository.save(matchingTag);
		}
		return matchingTag;
	}

	public Page<SocialEconomyNetworkTag> findAll(final PageRequest pageRequest) {
		return this.socialEconomyNetworkTagRepository.findAll(pageRequest);
	}

	public SocialEconomyNetworkTag findOne(final Long id) {
		return this.socialEconomyNetworkTagRepository.findOne(id);
	}
}
