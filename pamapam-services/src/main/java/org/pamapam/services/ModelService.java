package org.pamapam.services;

import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.services.dto.PageDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public abstract class ModelService<T, S> {

	@Autowired
	protected ModelMapper modelMapper;
	@Autowired
	protected LanguageRepository languageRepository;

	public ModelService() {
		super();
	}

	public PageDto<S> convertToDto(Page<T> page, Type type) {
		PageDto<S> pageDto = new PageDto<>();
		pageDto.setTotalElements(page.getTotalElements());
		pageDto.setLastPage(page.isLast());
		pageDto.setContent(this.modelMapper.map(page.getContent(), type));
		return pageDto;
	}

	public List<S> convertToDto(List<T> elements, Class<S> dtoClass) {
		return elements.stream().map(each -> this.convertToDto(each, dtoClass)).collect(Collectors.toList());
	}

	public S convertToDto(T object, Class<S> dtoClass) {
		return this.modelMapper.map(object, dtoClass);
	};

	public PageDto<S> findAllDto(PageRequest pageRequest, Type type) {
		return this.convertToDto(this.findAll(pageRequest), type);
	}

	public S findOneDto(Long id, Class<S> dtoClass) {
		return this.convertToDto(this.findOne(id), dtoClass);
	}

	public abstract Page<T> findAll(PageRequest pageRequest);

	public abstract T findOne(Long id);

	protected LocalizedString createLocalizedString(String value) {
		LocalizedString localizedString = new LocalizedString();
		for (Language eachLanguage : this.languageRepository.findAll()) {
			localizedString.add(eachLanguage.getLanguageCode(), value);
		}
		return localizedString;
	}

}