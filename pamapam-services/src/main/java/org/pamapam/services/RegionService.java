package org.pamapam.services;

import java.util.List;

import org.jamgo.model.entity.Region;
import org.jamgo.model.repository.RegionRepository;
import org.pamapam.services.dto.RegionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class RegionService extends ModelService<Region, RegionDto> {

	@Autowired
	private RegionRepository regionRepository;

	public List<Region> findAll() {
		return this.regionRepository.findAll();
	}

	@Override
	public Page<Region> findAll(PageRequest pageRequest) {
		return this.regionRepository.findAll(pageRequest);
	}

	@Override
	public Region findOne(Long id) {
		return this.regionRepository.findOne(id);
	}

}
