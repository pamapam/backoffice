package org.pamapam.services;

import java.util.ArrayList;
import java.util.List;

import org.pamapam.model.ModelAttributeDef;
import org.pamapam.model.ModelDef;
import org.pamapam.repository.ModelDefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModelDefService {

	@Autowired
	private ModelDefRepository modelDefRepository;

	public List<ModelAttributeDef> getModelAttributeDefs(String modelName) {
		ModelDef modelDef = this.modelDefRepository.findByModelName(modelName);
		if (modelDef != null) {
			return modelDef.getAttributesDef();
		} else {
			return new ArrayList<>();
		}
	}

}
