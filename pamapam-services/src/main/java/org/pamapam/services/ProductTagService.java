package org.pamapam.services;

import java.util.List;

import org.pamapam.model.ProductTag;
import org.pamapam.repository.ProductTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductTagService {

	@Autowired
	private ProductTagRepository productTagRepository;

	public ProductTag findOrCreate(String tag) {
		ProductTag matchingTag = this.productTagRepository.findByTag(tag).stream().findFirst().orElse(null);
		if (matchingTag == null) {
			matchingTag = new ProductTag();
			matchingTag.setTag(tag);
			matchingTag = this.productTagRepository.save(matchingTag);
		}
		return matchingTag;
	}

	public List<ProductTag> findAll() {
		return this.productTagRepository.findAll();
	}
}
