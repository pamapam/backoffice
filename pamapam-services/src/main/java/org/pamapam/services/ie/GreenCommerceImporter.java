package org.pamapam.services.ie;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Neighborhood;
import org.jamgo.model.entity.Province;
import org.jamgo.model.entity.Region;
import org.jamgo.model.entity.Town;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.portable.ImportStats;
import org.jamgo.services.DatasourceServices;
import org.jamgo.services.ie.ExcelImporter;
import org.pamapam.model.Sector;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.services.GeoLocationService;
import org.pamapam.services.SectorService;
import org.pamapam.services.SocialEconomyEntityService;
import org.pamapam.services.TerritoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class GreenCommerceImporter extends ExcelImporter<SocialEconomyEntity>{

	private static final Logger logger = LoggerFactory.getLogger(GreenCommerceImporter.class);
	
    private static final int NAME_CELL = 0;
    private static final int NIF_CELL = 1;
    private static final int ADDRESS_CELL = 2;
    private static final int CP_CELL = 3;
    private static final int TOWN_CELL = 4;
    private static final int NEIGHBOURHOOD_CELL = 5;
    private static final int SECTOR_CELL = 7;
    private static final int SUB_SECTOR_CELL = 8;
    private static final int SUB_SECTOR_REZERO_CELL = 9;
    private static final int EMAIL_CELL = 10;
    private static final int PHONE_CELL = 11;
    private static final int SECOND_PHONE_CELL = 12;
    private static final int WEB_CELL = 13;
    private static final int FACEBOOK_CELL = 14;
    private static final int INSTAGRAM_CELL = 15;
    private static final int TWITTER_CELL = 16;
    
	@Autowired
	private DatasourceServices datasourceServices;
	
	@Autowired
	private TerritoryService territoryService;
	
	@Autowired
	private SectorService SectorService;
	
	@Autowired
	private SocialEconomyEntityService seeService;
	
	@Autowired
	private GeoLocationService geoLocationService;
	
	@Override
	public ImportStats<SocialEconomyEntity> updateImportCounts() {
		return this.getExtractedStats();
	}

	@Override
	public String[] getColumnNames() {
		// TODO Auto-generated method stub
		return new String[]{"id existent, nom", "nif", "email", "telefon", "addreça", "municipi", "barri"};
	}

	@Override
	public String[] getColumnHeaders() {
		return new String[] {"id", "name", "nif", "email", "phone", "address", "town", "neighborhood"};
	}

	@Override
	public ImportStats<SocialEconomyEntity> doImport() {
		this.getList().stream().forEach(item -> {
			try {
				this.seeService.saveGreenCommerceEntity(item);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return this.getExtractedStats();
	}
    
	@Override
    protected SocialEconomyEntity processExtractedRow(Sheet sheet, Row rowToProcess, SocialEconomyEntity previousEntity) {
	            this.logger.info("SocialEconomyImporter::processExtractedRow::row: " + rowToProcess.getRowNum());
	            SocialEconomyEntity result = new SocialEconomyEntity();
	            result.setNewslettersAccepted(false);
	            result.setNewslettersDate(null);
            	Neighborhood neighborhood = null;
            	
            	String neighborhoodName = this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.NEIGHBOURHOOD_CELL);
            	String townName = this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.TOWN_CELL);

            	if (StringUtils.isNotEmpty(neighborhoodName)) {
            		List<Neighborhood> neighborhoods = this.territoryService.findNeighborhoodByName(neighborhoodName);
            		if (CollectionUtils.isNotEmpty(neighborhoods)) {
                		neighborhood = neighborhoods.stream()
                				.filter(each -> each.getDistrict().getTown().getName().getRawValue().contains(townName))
                				.findFirst().orElse(null);
            		}
            	}
            	Town town = null;
            	Region region = null;
            	Province province = null;
            	
                result.setNif(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.NIF_CELL));
                List<SocialEconomyEntity> existing = this.seeService.findByNif(result.getNif());
                if (!CollectionUtils.isEmpty(existing)) {
                	result = existing.get(0);
                    result.setGreenCommerce(true);
                	this.numberOfUpdates++;
                } else {
                    this.numberOfInserts++;
                    result.setGreenCommerce(true);
	            	if (neighborhood != null) {
	            		town = neighborhood.getDistrict().getTown();
	            	} else {
	                	List<Town> towns = this.territoryService.findTownByName(townName);
	                	town = CollectionUtils.isEmpty(towns)? null : towns.get(0);
	            	}
	            	
	            	if (town != null) {
	            		region = town.getRegion();
	            		province = region.getProvince();
	            	} 
	            	
	                result.setName(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.NAME_CELL));
	                LocalizedString description = new LocalizedString();
	                description.add("ca", "Aquest és un Comerç Verd certificat per Rezero, que garanteix que hi ha una oferta que fomenta la reducció de residus, incorpora productes locals i ecològics, permet la reutilització d’envasos i/o té bones pràctiques que puguin reduir l’impacte ambiental del consum.");
	                result.setDescription(description);
	                result.setEmail(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.EMAIL_CELL));
	                try {
	                	result.setPhone(this.getCellNumericIntValue(sheet, rowToProcess, GreenCommerceImporter.PHONE_CELL).toString());
	                } catch (Exception e) {
	                	result.setPhone(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.PHONE_CELL));
	                }
	                result.setWeb(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.WEB_CELL));
	                String address = this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.ADDRESS_CELL);
	                String postalCode = "";
	                try {
	                	postalCode = this.getCellNumericIntValue(sheet, rowToProcess, GreenCommerceImporter.CP_CELL).toString();
	                } catch (Exception e) {
	                	postalCode = this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.CP_CELL);
	                }
	                result.setPostalCode(postalCode);
	                result.setAddress(address);
	                
	    	        if (StringUtils.isNotBlank(address)) {
	    	        	//get coordinates from openstreetmap api
	    	        	try {
	    					this.geoLocationService.retrieveCoordinatesFromOpenStreetApi(address, postalCode, townName, result);
	    				} catch (IOException e) {
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	    	        }
	        	        
	                result.setFacebook(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.FACEBOOK_CELL));
	                result.setInstagram(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.INSTAGRAM_CELL));
	                result.setTwitter(this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.TWITTER_CELL));
	
	                result.setTown(town);
	                result.setNeighborhood(neighborhood);
	                result.setRegion(region);
	                result.setProvince(province);
                              
	                String sectorName = this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.SECTOR_CELL);
	                String subSectorName = this.getCellStringValue(sheet, rowToProcess, GreenCommerceImporter.SUB_SECTOR_CELL);
	                
	                List<Sector> sectors = this.SectorService.findByNormalizedName(sectorName);
	                Sector sector = CollectionUtils.isEmpty(sectors)? null : sectors.get(0);
	                result.setMainSector(sector);
	                if (sector != null) {
	                    Sector subSector = sector.getSectors().stream()
		                    .filter(each -> each.getName().getRawValue().contains(subSectorName))
		    				.findFirst().orElse(null);
	                    if (subSector != null) {
	                    	result.setMainSector(subSector);
	//                    	result.setSectors(new HashSet<>(Arrays.asList(subSector)));
	                    } else {
	                    	this.logger.trace("entity with unknwown subsector: " + result.getNif());
	                    }
	                }
                }   
            return result;
    }


	@Override
	public SocialEconomyEntity getNewInstance() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SocialEconomyEntity findInstance(Object... value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getEntitiesCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected Object getObjectFileId(SocialEconomyEntity entity) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public InputStream generateSample() {
		ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		Workbook wb = null;
		try {
			wb = new XSSFWorkbook(super.generateSample());
			Sheet sheet = wb.createSheet("sectors");
			Row headerRow = sheet.createRow(0);
			headerRow.createCell(0).setCellValue("id");
			headerRow.createCell(1).setCellValue("name");
			this.datasourceServices.getAll(Sector.class).stream().forEach(each -> {
				Row sectorRow = sheet.createRow(sheet.getLastRowNum() + 1);
				sectorRow.createCell(0).setCellValue(each.getId());
				sectorRow.createCell(1).setCellValue(each.getName().getDefaultText());
			});
			wb.write(os);
		} catch (IOException | RepositoryForClassNotDefinedException e) {
			GreenCommerceImporter.logger.error(e.getMessage(), e);
			return null;
		} finally {
			try {
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				GreenCommerceImporter.logger.error(e.getMessage(), e);
				return null;
			}
		}
		return new ByteArrayInputStream(os.toByteArray());
	}

}
