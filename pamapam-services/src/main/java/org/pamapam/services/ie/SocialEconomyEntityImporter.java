package org.pamapam.services.ie;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jamgo.model.exception.JmgImportException;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.portable.ImportStats;
import org.jamgo.services.DatasourceServices;
import org.jamgo.services.ie.ExcelImporter;
import org.pamapam.model.Sector;
import org.pamapam.model.SocialEconomyEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SocialEconomyEntityImporter extends ExcelImporter<SocialEconomyEntity>{

	private static final Logger logger = LoggerFactory.getLogger(SocialEconomyEntityImporter.class);
	
	@Autowired
	private DatasourceServices datasourceServices;
	
	@Override
	public ImportStats<SocialEconomyEntity> updateImportCounts() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getColumnNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getColumnHeaders() {
		return new String[] {"name"};
	}

	@Override
	public ImportStats<SocialEconomyEntity> doImport() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected SocialEconomyEntity processExtractedRow(Sheet sheet, Row nextRow, SocialEconomyEntity previousEntity)
			throws JmgImportException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SocialEconomyEntity getNewInstance() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SocialEconomyEntity findInstance(Object... value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getEntitiesCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected Object getObjectFileId(SocialEconomyEntity entity) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public InputStream generateSample() {
		ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		Workbook wb = null;
		try {
			wb = new XSSFWorkbook(super.generateSample());
			Sheet sheet = wb.createSheet("sectors");
			Row headerRow = sheet.createRow(0);
			headerRow.createCell(0).setCellValue("id");
			headerRow.createCell(1).setCellValue("name");
			this.datasourceServices.getAll(Sector.class).stream().forEach(each -> {
				Row sectorRow = sheet.createRow(sheet.getLastRowNum() + 1);
				sectorRow.createCell(0).setCellValue(each.getId());
				sectorRow.createCell(1).setCellValue(each.getName().getDefaultText());
			});
			wb.write(os);
		} catch (IOException | RepositoryForClassNotDefinedException e) {
			SocialEconomyEntityImporter.logger.error(e.getMessage(), e);
			return null;
		} finally {
			try {
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				SocialEconomyEntityImporter.logger.error(e.getMessage(), e);
				return null;
			}
		}
		return new ByteArrayInputStream(os.toByteArray());
	}

}
