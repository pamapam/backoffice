package org.pamapam.services;

import org.pamapam.model.LegalForm;
import org.pamapam.repository.LegalFormRepository;
import org.pamapam.services.dto.LegalFormDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Service
public class LegalFormService extends ModelService<LegalForm, LegalFormDto> {

	@Autowired
	private LegalFormRepository legalFormRepository;
	@Autowired
	private EntityManager entityManager;

	@Override
	public Page<LegalForm> findAll(final PageRequest pageRequest) {
		return this.legalFormRepository.findAll(pageRequest);
	}

	@Override
	public LegalForm findOne(final Long id) {
		return this.legalFormRepository.findOne(id);
	}

	public LegalForm findByName(final String name) {
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<LegalForm> criteriaQuery = criteriaBuilder.createQuery(LegalForm.class);
		final Root<LegalForm> root = criteriaQuery.from(LegalForm.class);

		criteriaQuery.select(root).where(criteriaBuilder.like(root.get("name"), "%" + name + "%"));

		final Query query = this.entityManager.createQuery(criteriaQuery);
		final LegalForm legalForm = (LegalForm) query.getResultList().stream().findFirst().orElse(null);

		return legalForm;
	}
}
