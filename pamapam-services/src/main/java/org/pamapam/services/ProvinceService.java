package org.pamapam.services;

import java.util.List;

import org.jamgo.model.entity.Province;
import org.jamgo.model.repository.ProvinceRepository;
import org.pamapam.services.dto.ProvinceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProvinceService extends ModelService<Province, ProvinceDto> {

	@Autowired
	private ProvinceRepository provinceRepository;

	public List<Province> findAll() {
		return this.provinceRepository.findAll();
	}

	@Override
	public Page<Province> findAll(PageRequest pageRequest) {
		return this.provinceRepository.findAll(pageRequest);
	}

	@Override
	public Province findOne(Long id) {
		return this.provinceRepository.findOne(id);
	}

}
