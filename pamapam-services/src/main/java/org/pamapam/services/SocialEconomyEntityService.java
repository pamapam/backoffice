package org.pamapam.services;

import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.github.ooxi.jdatauri.DataUri;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.ca.CatalanAnalyzer;
import org.apache.lucene.analysis.es.SpanishAnalyzer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.ElisionFilter;
import org.jamgo.model.entity.*;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.search.SearchSpecification;
import org.jamgo.services.DatasourceServices;
import org.jamgo.services.dto.PageDto;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.message.LocalizedMessageService;
import org.modelmapper.TypeToken;
import org.pamapam.model.*;
import org.pamapam.model.enums.EntityAction;
import org.pamapam.model.enums.EntityScopeType;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.*;
import org.pamapam.repository.search.FullTextService;
import org.pamapam.repository.search.SectorSpecification;
import org.pamapam.repository.search.SocialEconomyEntitySearch;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.pamapam.services.config.PamapamUrlHelper;
import org.pamapam.services.dto.*;
import org.pamapam.services.dto.converters.*;
import org.pamapam.services.exception.UserAlreadyExistsException;
import org.pamapam.services.geojson.dto.GeoJsonDto;
import org.pamapam.services.geojson.dto.GeoJsonGeometryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
public class SocialEconomyEntityService extends ModelService<SocialEconomyEntity, SocialEconomyEntityDto> {

    private static final CharArraySet DEFAULT_ARTICLES = CharArraySet.unmodifiableSet(
            new CharArraySet(Arrays.asList("d", "l", "m", "n", "s", "t"), true));
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private SocialEconomyEntityRepository socialEconomyEntityRepository;
    @Autowired
    private SectorRepository sectorRepository;
    @Autowired
    private PamapamUserService userService;
    @Autowired
    private CriterionRepository criterionRepository;
    @Autowired
    private PersonRoleRepository personRoleRepository;
    @Autowired
    private LocalizedMessageService messageSource;
    @Autowired
    private EntityStatusRepository entityStatusRepository;
    @Autowired
    private LegalFormRepository legalFormRepository;
    @Autowired
    private ProductTagService productTagService;
    @Autowired
    private ExternalFilterTagRepository externalFilterTagRepository;
    @Autowired
    private SocialEconomyNetworkRepository socialEconomyNetworkRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CrudServices crudServices;
    @Autowired
    private DatasourceServices datasourceServices;
    @Autowired
    private AsyncTaskExecutor asyncTaskExecutor;
    @Autowired
    private PamapamUrlHelper pamapamUrlHelper;
    @Autowired
    private TerritoryService territoryService;
    @Autowired
    private DistrictService districtService;
    @Autowired
    private NeighborhoodService neighborhoodService;
    @Autowired
    private FullTextService fullTextService;
    @Autowired
    private LegalFormService legalFormService;
    @Autowired
    private QuestionaireRepository questionaireRepository;
    @Autowired
    private UserActionTokenRepository userActionTokenRepository;
    @Autowired
    private EntityScopeService entityScopeService;
    @Autowired
    private SectorService sectorService;
    @Autowired
    private EntityStatusService entityStatusService;
    @Autowired
    private EmbeddedMapConfigService embeddedMapConfigService;
    @Autowired
    private SocialEconomyEntityService socialEconomyEntityService;
    @Autowired
    private SocialBalanceService socialBalanceService;
    @Autowired
    private SocialEconomyNetworkTagService socialEconomyNetworkTagService;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private TownDtoConverter townDtoConverter;
    @Autowired
    private SectorDtoConverter sectorDtoConverter;
    @Autowired
    private EntityEvaluationDtoConverter entityEvaluationDtoConverter;
    @Autowired
    private SocialEconomyNetworkDtoConverter socialEconomyNetworkDtoConverter;
    @Autowired
    private SocialEconomyNetworkTagDtoConverter socialEconomyNetworkTagDtoConverter;
    @Autowired
    private KeywordTagDtoConverter keywordTagDtoConverter;
    @Autowired
    private RegionDtoConverter regionDtoConverter;
    private SocialEconomyEntitySpecification searchSpecification;

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");


    @PostConstruct
    private void init() {
        this.searchSpecification = this.applicationContext.getBean(SocialEconomyEntitySpecification.class);
    }

    public List<SocialEconomyEntity> findByNif(final String nif) {

        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<SocialEconomyEntity> criteriaQuery = criteriaBuilder.createQuery(SocialEconomyEntity.class);
        final Root<SocialEconomyEntity> root = criteriaQuery.from(SocialEconomyEntity.class);

        final Expression<String> replacedNif = criteriaBuilder.function("replace",
                String.class, root.get("nif"), criteriaBuilder.literal("-"),
                criteriaBuilder.literal(""));

        criteriaQuery.select(root).where(criteriaBuilder.equal(replacedNif, nif));

        final Query query = this.entityManager.createQuery(criteriaQuery);
        final List<SocialEconomyEntity> entities = query.getResultList();

        if (entities.size() > 1) {
            this.logger.warn("Found more than one result for entity with nif " + nif);
        }

        return entities;
    }

    public SocialEconomyEntityDto findByEntityId(final String stringId) {
        if (StringUtils.isNumeric(stringId)) {
            final Long id = Long.valueOf(stringId);
            return this.convertToDto(this.findByEntityId(id), SocialEconomyEntityDto.class);
        } else {
            SocialEconomyEntity socialEconomyEntity = this.findByNormalizedName(stringId);
            SocialEconomyEntityDto socialEconomyEntityDto = this.convertToDto(socialEconomyEntity, SocialEconomyEntityDto.class);
            SectorDto sectorDto = this.sectorDtoConverter.convert(socialEconomyEntity.getMainSector());
            socialEconomyEntityDto.setMainSector(sectorDto);
            socialEconomyEntityDto.setXesBalanceUrl(socialEconomyEntity.getUrlBalancSocial());
            socialEconomyEntityDto.setGreenCommerce(socialEconomyEntity.getEntityScopes().contains(this.entityScopeService.getGreenCommerceEntityScope()));
            return socialEconomyEntityDto;
        }
    }

    public SocialEconomyEntity findByEntityId(final Long id) {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
        searchObject.setEntityStatuses(Sets.newHashSet(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.PUBLISHED)));
        searchObject.setId(id);
        this.searchSpecification.setSearchObject(searchObject);
        return this.socialEconomyEntityRepository.findOne(this.searchSpecification);
    }

    public List<SocialEconomyEntity> findByInitiativeUser(final PamapamUser user) {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
        searchObject.setInitiativeUser(user);
        this.searchSpecification.setSearchObject(searchObject);
        return this.socialEconomyEntityRepository.findAll(this.searchSpecification);
    }

    public SocialEconomyEntity findByNormalizedName(final String normalizedName) {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
        searchObject.setEntityStatuses(Sets.newHashSet(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.PUBLISHED), this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.EXTERNAL)));
        searchObject.setNormalizedName(normalizedName);
        this.searchSpecification.setSearchObject(searchObject);
        return this.socialEconomyEntityRepository.findOne(this.searchSpecification);
    }

    @Override
    public Page<SocialEconomyEntity> findAll(final PageRequest pageRequest) {
        return this.socialEconomyEntityRepository.findAll(pageRequest);
    }

    public Page<SocialEconomyEntity> findAllPublished(final PageRequest pageRequest) {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
        searchObject.setEntityStatuses(Sets.newHashSet(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.PUBLISHED)));
        searchObject.setEntityScopes(this.entityScopeService.getDefaultEntityScopes());
        this.searchSpecification.setSearchObject(searchObject);
        return this.socialEconomyEntityRepository.findAll(this.searchSpecification, pageRequest);
    }

    public List<SocialEconomyEntity> findAllMainOffices() {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
        searchObject.setIncludeMainOffices(true);
        searchObject.setIncludeOffices(false);
        searchObject.setEntityScopes(this.entityScopeService.getCurrentEntityScopes());
        this.searchSpecification.setSearchObject(searchObject);
        return this.socialEconomyEntityRepository.findAll(this.searchSpecification);
    }

    public Page<Tuple> searchPublished(final SocialEconomyEntitySearchDto searchObjectDto, final PageRequest pageRequest) {
        final Map<EntityScopeType, List<EntityStatus>> statusScopes = new HashMap<>();
        final EntityStatus published = this.entityStatusService.findByEntityStatusType(EntityStatusType.PUBLISHED);
        statusScopes.put(EntityScopeType.DEFAULT, Arrays.asList(published));

        searchObjectDto.setStatusScopes(statusScopes);
        return this.search(
                searchObjectDto,
                pageRequest);
    }

    public Page<Tuple> searchWithStatusScopes(final SocialEconomyEntitySearchDto searchObjectDto, final PageRequest pageRequest) {
        return this.search(
                searchObjectDto,
                pageRequest);
    }

    public Page<Tuple> search(final SocialEconomyEntitySearchDto searchObjectDto, final PageRequest pageRequest) {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
        if (searchObjectDto.getText() != null) {
            searchObject.setText(searchObjectDto.getText());
        }

        if (searchObjectDto.getTerritoryId() != null) {
            searchObject.setTerritory(this.territoryService.findOne(searchObjectDto.getTerritoryId(), searchObjectDto.getTerritoryType()));
        }

        if (CollectionUtils.isNotEmpty(searchObjectDto.getSectorIds())) {
            searchObject.setSectors(Sets.newHashSet(this.sectorRepository.findAll(new SectorSpecification(searchObjectDto.getSectorIds()))));
        }
        if (searchObjectDto.getUserId() != null) {
            searchObject.setRegistryUser(this.userService.findOne(searchObjectDto.getUserId()));
        }
        if (searchObjectDto.getStatusScopes() != null) {
            searchObject.setStatusScopes(searchObjectDto.getStatusScopes());
        }
        if (Boolean.TRUE.equals(searchObjectDto.isGreenCommerce())) {
        	searchObject.setGreenCommerce(true);
        }

        final String domain = Optional.ofNullable(searchObjectDto.getRefererDomain())
                .map(o -> o.replace('_', '.'))
                .orElse(null);
        final String apiKey = searchObjectDto.getApiKey();

        if (domain != null && apiKey != null) {
            final EmbeddedMapConfig embeddedMapConfig = this.embeddedMapConfigService.findByDomainAndApiKey(domain, apiKey);
            if (embeddedMapConfig != null) {
                if (CollectionUtils.isNotEmpty(embeddedMapConfig.getEntityScopes())) {
                    searchObject.setEntityScopes(embeddedMapConfig.getEntityScopes());
                    searchObject.setStatusScopes(null); //crappy way to disable the query by state-scope without making a major refactor
                } else {
                    searchObject.setEntityScopes(this.entityScopeService.getDefaultEntityScopes());
                }

                if (CollectionUtils.isNotEmpty(embeddedMapConfig.getStatuses())) {
                    searchObject.setEntityStatuses(embeddedMapConfig.getStatuses());
                }
                if (CollectionUtils.isNotEmpty(embeddedMapConfig.getExternalFilterTags())) {
                    searchObject.setExternalFilterTags(embeddedMapConfig.getExternalFilterTags());
                }
            }
        }

        this.searchSpecification.setSearchObject(searchObject);

        List<Tuple> tuples = null;
        if (Boolean.TRUE.equals(searchObjectDto.getDetails())) {
            tuples = this.socialEconomyEntityRepository.findDetailTuples(this.searchSpecification);
        } else {
            tuples = this.socialEconomyEntityRepository.findTuples(this.searchSpecification, pageRequest);
        }

        return new PageImpl<>(tuples);
    }

    public PageDto<SocialEconomyEntityDto> findAllPublishedDto(final PageRequest pageRequest) {
        return this.convertToDto(this.findAllPublished(pageRequest), new TypeToken<List<SocialEconomyEntityDto>>() {
        }.getType());
    }

    public PageDto<SocialEconomyEntityDto> searchAllPamapamPublishedAndSocialBalancePublishedAndExternal(final SocialEconomyEntitySearchDto searchObjectDto, final PageRequest pageRequest) {
        final Map<EntityScopeType, List<EntityStatus>> statusScopes = new HashMap<>();
        final EntityStatus published = this.entityStatusService.findByEntityStatusType(EntityStatusType.PUBLISHED);
        final EntityStatus external = this.entityStatusService.findByEntityStatusType(EntityStatusType.EXTERNAL);
        
        statusScopes.put(EntityScopeType.DEFAULT, Arrays.asList(published));
        statusScopes.put(EntityScopeType.SOCIAL_BALANCE_ENTITY_SCOPE, Arrays.asList(published, external));
        if (Boolean.TRUE.equals(searchObjectDto.isGreenCommerce())) {
        	statusScopes.put(EntityScopeType.GREEN_COMMERCE_ENTITY_SCOPE, Arrays.asList(published, external));
        }
        searchObjectDto.setStatusScopes(statusScopes);

        final List<Tuple> tuples = this.searchWithStatusScopes(searchObjectDto, pageRequest).getContent();
        final List<SocialEconomyEntityDto> entitiesDto = this.createSocialEconomyEntityDtos(tuples);
        final PageDto<SocialEconomyEntityDto> pageDto = new PageDto<>();
        pageDto.setTotalElements(entitiesDto.size());
        pageDto.setLastPage(entitiesDto.isEmpty());
        pageDto.setContent(entitiesDto);
        return pageDto;
    }

    public PageDto<SocialEconomyEntityDto> searchDto(final SocialEconomyEntitySearchDto searchObjectDto, final PageRequest pageRequest) {
        final Map<EntityScopeType, List<EntityStatus>> statusScopes = new HashMap<>();
        final EntityStatus published = this.entityStatusService.findByEntityStatusType(EntityStatusType.PUBLISHED);
        statusScopes.put(EntityScopeType.DEFAULT, Arrays.asList(published));

        searchObjectDto.setStatusScopes(statusScopes);
        final List<Tuple> tuples = this.search(searchObjectDto, pageRequest).getContent();
        List<SocialEconomyEntityDto> entitiesDto = null;
        if (Boolean.TRUE.equals(searchObjectDto.getDetails())) {
            entitiesDto = this.createSocialEconomyEntityDetailsDtos(tuples);
        } else {
            entitiesDto = this.createSocialEconomyEntityDtos(tuples);
        }
        final PageDto<SocialEconomyEntityDto> pageDto = new PageDto<>();
        pageDto.setTotalElements(entitiesDto.size());
        pageDto.setLastPage(entitiesDto.isEmpty());
        pageDto.setContent(entitiesDto);
        return pageDto;
    }

    @Override
    public SocialEconomyEntity findOne(final Long id) {
        return this.socialEconomyEntityRepository.findOne(id);
    }

    public List<SocialEconomyEntityDto> findRandomRelatedDtoBySector(final Long id, final Integer count) {
        final SocialEconomyEntity referenceEntity = this.socialEconomyEntityRepository.findOne(id);
        final List<SocialEconomyEntity> matchingEntities;
        if (CollectionUtils.isNotEmpty(referenceEntity.getSectors())) {
            final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
            searchObject.setEntityScopes(this.entityScopeService.getDefaultEntityScopes());
            searchObject.setSectors(Sets.newHashSet(referenceEntity.getSectors().stream().findFirst().get()));
            searchObject.setEntityStatuses(Sets.newHashSet(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.PUBLISHED)));
            this.searchSpecification.setSearchObject(searchObject);
            final List<SocialEconomyEntity> relatedBySector = this.socialEconomyEntityRepository.findAll(this.searchSpecification);
            relatedBySector.remove(referenceEntity);
            Collections.shuffle(relatedBySector);
            matchingEntities = relatedBySector.subList(0, count);
        } else {
            matchingEntities = new ArrayList<>();
        }
        return this.convertToDto(matchingEntities, SocialEconomyEntityDto.class);
    }

    public List<SocialEconomyEntityDto> findRandomRelatedDtoByProximity(final Long id, final Integer count) {
        final SocialEconomyEntity referenceEntity = this.socialEconomyEntityRepository.findOne(id);
        List<SocialEconomyEntity> matchingEntities;
        if ((referenceEntity.getLatitude() != null) && (referenceEntity.getLongitude() != null)) {
            final Set<Long> relatedIds;
            try {
                relatedIds = this.fullTextService.findByProximity(referenceEntity.getLatitude(), referenceEntity.getLongitude(), count * 3);
                final List<SocialEconomyEntity> relatedByProximity = this.socialEconomyEntityRepository.findByIdIn(relatedIds);
                relatedByProximity.remove(referenceEntity);
                Collections.shuffle(relatedByProximity);
                matchingEntities = relatedByProximity.subList(0, Math.min(relatedByProximity.size(), count));
            } catch (final IOException e) {
                // ... TODO Log error in search.
                e.printStackTrace();
                matchingEntities = new ArrayList<>();
            }
        } else {
            matchingEntities = new ArrayList<>();
        }
        return this.convertToDto(matchingEntities, SocialEconomyEntityDto.class);
    }

    public List<GeoJsonDto> findAllGeoJson() {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
        searchObject.setEntityScopes(this.entityScopeService.getDefaultEntityScopes());
        this.searchSpecification.setSearchObject(searchObject);
        return this.createGeoJsonDtos(this.socialEconomyEntityRepository.streamGeoJsonTuples(this.searchSpecification));
    }

    public List<GeoJsonDto> searchGeoJson(final SocialEconomyEntitySearchDto searchObjectDto) {
        final String domain = Optional.ofNullable(searchObjectDto.getRefererDomain())
                .map(o -> o.replace('_', '.'))
                .orElse(null);
        final String apiKey = searchObjectDto.getApiKey();

        List<GeoJsonDto> result = new ArrayList<>();
        if (domain != null && apiKey != null) {
            final EmbeddedMapConfig embeddedMapConfig = this.embeddedMapConfigService.findByDomainAndApiKey(domain, apiKey);
            if (embeddedMapConfig != null) {
                result = this.searchGeoJson(searchObjectDto, embeddedMapConfig);
            }
        }

        return result;
    }

    public List<GeoJsonDto> findAllGeoJsonPamapamPublishedAndSocialBalancePublishedAndExternal(final SocialEconomyEntitySearchDto searchObjectDto) {
        final EmbeddedMapConfig embeddedMapConfig = new EmbeddedMapConfig();
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();

        final Map<EntityScopeType, List<EntityStatus>> statusScopes = new HashMap<>();
        final EntityStatus published = this.entityStatusService.findByEntityStatusType(EntityStatusType.PUBLISHED);
        final EntityStatus external = this.entityStatusService.findByEntityStatusType(EntityStatusType.EXTERNAL);

        embeddedMapConfig.setEntityScopes(Collections.<EntityScope>emptySet());
        embeddedMapConfig.setStatuses(Collections.<EntityStatus>emptySet());

        statusScopes.put(EntityScopeType.DEFAULT, Arrays.asList(published));
        statusScopes.put(EntityScopeType.SOCIAL_BALANCE_ENTITY_SCOPE, Arrays.asList(published, external));
        if (Boolean.TRUE.equals(searchObjectDto.isGreenCommerce())) {
//        	searchObjectDto.setGreenCommerce(true);
        	statusScopes.put(EntityScopeType.GREEN_COMMERCE_ENTITY_SCOPE, Arrays.asList(published, external));
        }
        searchObject.setStatusScopes(statusScopes);
        this.searchSpecification.setSearchObject(searchObject);

        searchObjectDto.setStatusScopes(statusScopes);

        return this.searchGeoJson(searchObjectDto, embeddedMapConfig);

    }

    public GeoJsonDto findGeoJson(final Long id) {
        final SocialEconomyEntity socialEconomyEntity = this.findOne(id);
        return this.createFullGeoJsonDto(socialEconomyEntity);
    }

    private List<GeoJsonDto> searchGeoJson(final SocialEconomyEntitySearchDto searchObjectDto, final EmbeddedMapConfig embeddedMapConfig) {
        final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();

        searchObject.setText(searchObjectDto.getText());
        if (CollectionUtils.isNotEmpty(searchObjectDto.getSectorIds())) {
            searchObject.setSectors(Sets.newHashSet(this.sectorRepository.findAll(new SectorSpecification(searchObjectDto.getSectorIds()))));
        }
        if (searchObjectDto.getStatusScopes() != null) {
            searchObject.setStatusScopes(searchObjectDto.getStatusScopes());
        }
        if (CollectionUtils.isNotEmpty(embeddedMapConfig.getEntityScopes())) {
            searchObject.setEntityScopes(embeddedMapConfig.getEntityScopes());
        }
        if (CollectionUtils.isNotEmpty(embeddedMapConfig.getStatuses())) {
            searchObject.setEntityStatuses(embeddedMapConfig.getStatuses());
        }
        if (CollectionUtils.isNotEmpty(embeddedMapConfig.getExternalFilterTags())) {
            searchObject.setExternalFilterTags(embeddedMapConfig.getExternalFilterTags());
        }

        if (searchObjectDto.getTerritoryId() != null) {
            searchObject.setIncludeMainOffices(true);
            searchObject.setTerritory(this.territoryService.findOne(searchObjectDto.getTerritoryId(), searchObjectDto.getTerritoryType()));
            searchObject.setIncludeOffices(true);
        }

        this.searchSpecification.setSearchObject(searchObject);

        final Stream<Tuple> entityTuples = this.socialEconomyEntityRepository.streamGeoJsonTuples(this.searchSpecification);
        return this.createGeoJsonDtos(entityTuples);
    }

    private GeoJsonDto createFullGeoJsonDto(final SocialEconomyEntity entity) {
        final GeoJsonDto geoJsonDto = new GeoJsonDto("Feature");
        final GeoJsonGeometryDto geoJsonGeometryDto = new GeoJsonGeometryDto("Point");
        geoJsonGeometryDto.setCoordinates(Lists.newArrayList(entity.getLongitude(), entity.getLatitude()));
        geoJsonDto.setGeometry(geoJsonGeometryDto);
        geoJsonDto.addProperty("id", entity.getId());
        geoJsonDto.addProperty("name", entity.getName());
        geoJsonDto.addProperty("normalizedName", entity.getNormalizedName());
        geoJsonDto.addProperty("description", entity.getDescription().getDefaultText());
        if (entity.getTown() != null) {
            geoJsonDto.addProperty("town", entity.getTown().getName().getDefaultText());
        }
        Sector mainSector = entity.getMainSector();
        if (mainSector == null) {
            mainSector = entity.getSectors().stream().findFirst().orElse(null);
        }
        final Set<String> sectorIconUrls = new LinkedHashSet<>();
        if (mainSector != null) {
            geoJsonDto.addProperty("sector", mainSector.getFullName());
            if (mainSector.getMapIcon() != null) {
                geoJsonDto.addProperty("sectorMapIconUrl", this.pamapamUrlHelper.getImageUrl(mainSector.getMapIcon().getId()));
            }
            if (mainSector.getIcon() != null) {
                sectorIconUrls.add(this.pamapamUrlHelper.getImageUrl(mainSector.getIcon().getId()));
            }
        }
        sectorIconUrls.addAll(entity.getSectors().stream()
                .map(each -> this.pamapamUrlHelper.getImageUrl(each.getIcon().getId()))
                .collect(Collectors.toSet()));
        geoJsonDto.addProperty("sectorIconUrls", sectorIconUrls);
        final List<String> criteriaIconUrls = entity.getAccomplishedCriteria().stream()
                .map(each -> this.pamapamUrlHelper.getImageUrl(each.getIcon().getId()))
                .collect(Collectors.toList());
        geoJsonDto.addProperty("criteriaIconUrls", criteriaIconUrls);
        geoJsonDto.addProperty("address", entity.getAddress());
        geoJsonDto.addProperty("phone", entity.getPhone());
        if (entity.getPicture() != null) {
            geoJsonDto.addProperty("pictureUrl", this.pamapamUrlHelper.getImageUrl(entity.getPicture().getId()));
        } else if (entity.getGreenCommerce()) {
            geoJsonDto.addProperty("greenCommerce", true);

        }
        geoJsonDto.addProperty("openingHours", entity.getOpeningHours());
        geoJsonDto.addProperty("openingHours", entity.getOpeningHours());
        geoJsonDto.addProperty("web", entity.getWeb());
        geoJsonDto.addProperty("onlineShoppingWeb", entity.getOnlineShoppingWeb());
        geoJsonDto.addProperty("laZonaWeb", entity.getLaZonaWeb());
        geoJsonDto.addProperty("email", entity.getEmail());
        geoJsonDto.addProperty("status", entity.getEntityStatus().getEntityStatusType().toString());
        geoJsonDto.addProperty("twitter", entity.getTwitter());
        geoJsonDto.addProperty("facebook", entity.getFacebook());
        geoJsonDto.addProperty("instagram", entity.getInstagram());
        geoJsonDto.addProperty("pinterest", entity.getPinterest());
        geoJsonDto.addProperty("quitter", entity.getQuitter());
        geoJsonDto.addProperty("isIntercoop", this.isIntercoop(entity));
        geoJsonDto.addProperty("xesBalance", entity.getXesBalance());
        geoJsonDto.addProperty("xesBalanceUrl", entity.getUrlBalancSocial());
        final Boolean isXesBalanceEntityScopeType = entity.getEntityScopes().contains(this.entityScopeService.getSocialBalanceEntityScope());
        geoJsonDto.addProperty("xesBalanceScope", isXesBalanceEntityScopeType);
        final Boolean isGreenCommerceEntityScopeType = entity.getEntityScopes().contains(this.entityScopeService.getGreenCommerceEntityScope());
        geoJsonDto.addProperty("greenCommerceScope", isGreenCommerceEntityScopeType);

        return geoJsonDto;
    }

    private List<GeoJsonDto> createGeoJsonDtos(final Stream<Tuple> entityTuples) {
        final List<GeoJsonDto> allEntitiesGeoJson = new ArrayList<>();

        final Map<Long, Tuple> entities = new HashMap<>();
        final Set<Long> allSectorIds = new HashSet<>();

        entityTuples.forEach(each -> {
            final Long id = (Long) each.get("id");
            entities.putIfAbsent(id, each);
            if (each.get("mainSectorId") != null) {
                allSectorIds.add((Long) each.get("mainSectorId"));
            } else {
                allSectorIds.add((Long) each.get("mainOfficeMainSectorId"));
            }
        });

        final Map<Long, SectorDto> parentSectors = new HashMap<>();
        final Map<Long, SectorDto> sectors = new HashMap<>();

        this.socialEconomyEntityRepository.getParentSectorTuples()
                .forEach(each -> parentSectors.put((Long) each.get("id"), this.sectorDtoConverter.convert(each, null)));
        if (CollectionUtils.isNotEmpty(allSectorIds)) {
            this.socialEconomyEntityRepository.getSectorTuples(allSectorIds)
                    .forEach(each -> sectors.put((Long) each.get("id"), this.sectorDtoConverter.convert(each, parentSectors)));
        }

        entities.forEach((id, tuple) -> {
            if ((tuple.get("latitude") == null) || (tuple.get("longitude") == null)) {
                return;
            }
            final GeoJsonDto geoJsonDto = new GeoJsonDto("Feature");
            final GeoJsonGeometryDto geoJsonGeometryDto = new GeoJsonGeometryDto("Point");
            geoJsonGeometryDto.setCoordinates(Lists.newArrayList((Double) tuple.get("longitude"), (Double) tuple.get("latitude")));
            geoJsonDto.setGeometry(geoJsonGeometryDto);
            geoJsonDto.addProperty("id", tuple.get("id"));
            geoJsonDto.addProperty("name", tuple.get("name"));
            final Long mainSectorId = Optional.ofNullable((Long) tuple.get("mainSectorId")).orElse((Long) tuple.get("mainOfficeMainSectorId"));
            final SectorDto entityMainSector = Optional.ofNullable(mainSectorId)
                    .map(o -> sectors.get(o))
                    .orElse(null);

            final boolean greenCommerce = Optional.ofNullable((Boolean) tuple.get("greenCommerce")).orElse(false);
            final boolean xesBalance = Optional.ofNullable((Boolean) tuple.get("xesBalance")).orElse(false);
            final EntityStatus entityStatus = Optional.ofNullable((EntityStatus) tuple.get("entityStatus")).orElse(null);
            
            EntityStatus externalStatus = this.entityStatusService.findByEntityStatusType(EntityStatusType.EXTERNAL);
            
            if (greenCommerce && entityStatus.equals(externalStatus) && xesBalance == false) {
                geoJsonDto.addProperty("greenCommerce", true);  	
            } else {
            	if (greenCommerce) {
                    geoJsonDto.addProperty("greenCommerceSector", true);  	
            	}
            	if (entityMainSector != null) {
            		geoJsonDto.addProperty("sectorMapIconUrl", entityMainSector.getMapIconUrl());
            	}
            }
            allEntitiesGeoJson.add(geoJsonDto);
        });

        return allEntitiesGeoJson;
    }

    private List<SocialEconomyEntityDto> createSocialEconomyEntityDtos(final List<Tuple> entityTuples) {
        final List<SocialEconomyEntityDto> result = new ArrayList<>();

        final Map<Long, Tuple> entities = new HashMap<>();
        final Set<Long> allSectorIds = new HashSet<>();

        entityTuples.forEach(each -> {
            final Long id = (Long) each.get("id");
            entities.putIfAbsent(id, each);
            if (each.get("mainSectorId") != null) {
                allSectorIds.add((Long) each.get("mainSectorId"));
            } else {
                allSectorIds.add((Long) each.get("mainOfficeMainSectorId"));
            }
        });

        final Map<Long, SectorDto> parentSectors = new HashMap<>();
        final Map<Long, SectorDto> sectors = new HashMap<>();

        this.socialEconomyEntityRepository.getParentSectorTuples()
                .forEach(each -> parentSectors.put((Long) each.get("id"), this.sectorDtoConverter.convert(each, null)));
        if (CollectionUtils.isNotEmpty(allSectorIds)) {
            this.socialEconomyEntityRepository.getSectorTuples(allSectorIds)
                    .forEach(each -> sectors.put((Long) each.get("id"), this.sectorDtoConverter.convert(each, parentSectors)));
        }

        final MultiValuedMap<Long, Long> townIds = new HashSetValuedHashMap<>();
        entityTuples.forEach(each -> {
            final Long id = (Long) each.get("id");
            townIds.put(id, (Long) each.get("townId"));
        });

        final Map<Long, TownDto> towns = new HashMap<>();
        this.socialEconomyEntityRepository.getTownTuples(Sets.newHashSet(townIds.values()))
                .forEach(each -> towns.put((Long) each.get("id"), this.townDtoConverter.convert(each)));

        entities.forEach((id, tuple) -> {
            if ((tuple.get("latitude") == null) || (tuple.get("longitude") == null)) {
                return;
            }

            final SocialEconomyEntityDto socialEconomyEntityDto = new SocialEconomyEntityDto();
            socialEconomyEntityDto.setId((Long) tuple.get("id"));
            socialEconomyEntityDto.setName((String) tuple.get("name"));
            socialEconomyEntityDto.setNormalizedName((String) tuple.get("normalizedName"));
            socialEconomyEntityDto.setDescription(((LocalizedString) tuple.get("description")).getDefaultText());
            socialEconomyEntityDto.setPictureUrl(this.pamapamUrlHelper.getImageUrl((Long) tuple.get("pictureId")));
            
            socialEconomyEntityDto.setTown(towns.get(tuple.get("townId")));
            socialEconomyEntityDto.setXesBalance((Boolean) tuple.get("xesBalance"));
            socialEconomyEntityDto.setXesBalanceUrl((String) tuple.get("xesBalanceUrl"));
            socialEconomyEntityDto.setGreenCommerce((Boolean) tuple.get("greenCommerce"));
            EntityStatusDto entityStatusDto = new EntityStatusDto();
            entityStatusDto.setId((Long) tuple.get("id"));
            EntityStatusType entityStatusType = (EntityStatusType) tuple.get("statusType");
            entityStatusDto.setEntityStatusType(entityStatusType.toString());
            socialEconomyEntityDto.setEntityStatus(entityStatusDto);

			final boolean xesBalance = Optional.ofNullable((Boolean) tuple.get("xesBalance")).orElse(false);

			final boolean greenCommerce = Optional.ofNullable((Boolean) tuple.get("greenCommerce")).orElse(false);

            if (greenCommerce && entityStatusType.equals(EntityStatusType.EXTERNAL) && xesBalance == false) {
            	socialEconomyEntityDto.setGreenCommerce(greenCommerce);
            } else {
            	if (greenCommerce) {
            		socialEconomyEntityDto.setGreenCommerceSector(true);
            	}
            }
            
            SectorDto entityMainSector = Optional.ofNullable(tuple.get("mainSectorId")).map(o -> sectors.get(o)).orElse(null);
            if (entityMainSector == null) {
                entityMainSector = Optional.ofNullable(tuple.get("mainOfficeMainSectorId")).map(o -> sectors.get(o)).orElse(null);
            }
            //if (entityMainSector != null && entityMainSector.getParent() != null) {
            //    entityMainSector = entityMainSector.getParent();
            //}
            socialEconomyEntityDto.setMainSector(entityMainSector);
            socialEconomyEntityDto.setTown(towns.get(tuple.get("townId")));
            result.add(socialEconomyEntityDto);
        });

        result.sort((a, b) -> a.getName().compareTo(b.getName()));

        return result;
    }

    private List<SocialEconomyEntityDto> createSocialEconomyEntityDetailsDtos(final List<Tuple> entityTuples) {
        final List<SocialEconomyEntityDto> result = new ArrayList<>();

        final Map<Long, Tuple> entities = new HashMap<>();
        final Set<Long> allSectorIds = new HashSet<>();
        final MultiValuedMap<Long, Long> regionIds = new HashSetValuedHashMap<>();
        final MultiValuedMap<Long, Long> entityEvaluationIds = new HashSetValuedHashMap<>();
        final MultiValuedMap<Long, Long> townIds = new HashSetValuedHashMap<>();
        final MultiValuedMap<Long, Long> sectorIds = new HashSetValuedHashMap<>();
        final MultiValuedMap<Long, String> productTags = new HashSetValuedHashMap<>();
        final MultiValuedMap<Long, LocalizedString> socialEconomyNetworks = new HashSetValuedHashMap<>();
        final MultiValuedMap<Long, String> otherSocialEconomyNetworks = new HashSetValuedHashMap<>();
        final MultiValuedMap<Long, String> keywordTags = new HashSetValuedHashMap<>();

        entityTuples.forEach(each -> {
            final Long id = (Long) each.get("id");
            entities.putIfAbsent(id, each);
            if (each.get("mainSectorId") != null) {
                allSectorIds.add((Long) each.get("mainSectorId"));
            } else {
                allSectorIds.add((Long) each.get("mainOfficeMainSectorId"));
            }
            regionIds.put(id, (Long) each.get("regionId"));
            entityEvaluationIds.put(id, (Long) each.get("entityEvaluationId"));
            townIds.put(id, (Long) each.get("townId"));
            sectorIds.put(id, (Long) each.get("sectorId"));
            productTags.put(id, (String) each.get("productTag"));
            socialEconomyNetworks.put(id, (LocalizedString) each.get("socialEconomyNetwork"));
            otherSocialEconomyNetworks.put(id, (String) each.get("otherSocialEconomyNetwork"));
            keywordTags.put(id, (String) each.get("keywordTag"));
        });
        allSectorIds.addAll(sectorIds.values());

        final Map<Long, SectorDto> parentSectors = new HashMap<>();
        final Map<Long, SectorDto> sectors = new HashMap<>();
        final Map<Long, RegionDto> regions = new HashMap<>();
        final Map<Long, EntityEvaluationDto> evaluations = new HashMap<>();
        final Map<Long, TownDto> towns = new HashMap<>();

        this.socialEconomyEntityRepository.getParentSectorTuples()
                .forEach(each -> parentSectors.put((Long) each.get("id"), this.sectorDtoConverter.convert(each, null)));
        if (CollectionUtils.isNotEmpty(allSectorIds)) {
            this.socialEconomyEntityRepository.getSectorTuples(allSectorIds)
                    .forEach(each -> sectors.put((Long) each.get("id"), this.sectorDtoConverter.convert(each, parentSectors)));
        }
        this.socialEconomyEntityRepository.getRegionTuples(Sets.newHashSet(regionIds.values()))
                .forEach(each -> regions.put((Long) each.get("id"), this.regionDtoConverter.convert(each)));
        this.socialEconomyEntityRepository.getEntityEvaluationTuples(Sets.newHashSet(entityEvaluationIds.values()))
                .forEach(each -> evaluations.put((Long) each.get("id"), this.entityEvaluationDtoConverter.convert(each)));
        this.socialEconomyEntityRepository.getTownTuples(Sets.newHashSet(townIds.values()))
                .forEach(each -> towns.put((Long) each.get("id"), this.townDtoConverter.convert(each)));

        final Map<Long, DistrictDto> districts = this.districtService.getDistrictDtosFromEntityTuples(entityTuples);
        final Map<Long, NeighborhoodDto> neighborhoods = this.neighborhoodService.getNeighborhoodDtosFromEntityTuples(entityTuples);

        entities.forEach((id, tuple) -> {
            final SocialEconomyEntityDto socialEconomyEntityDto = new SocialEconomyEntityDto();
            socialEconomyEntityDto.setId((Long) tuple.get("id"));
            socialEconomyEntityDto.setName((String) tuple.get("name"));
            socialEconomyEntityDto.setPhone((String) tuple.get("phone"));
            socialEconomyEntityDto.setAddress((String) tuple.get("address"));
            socialEconomyEntityDto.setFacebook((String) tuple.get("facebook"));
            socialEconomyEntityDto.setTwitter((String) tuple.get("twitter"));
            socialEconomyEntityDto.setInstagram((String) tuple.get("instagram"));
            socialEconomyEntityDto.setPinterest((String) tuple.get("pinterest"));
            socialEconomyEntityDto.setQuitter((String) tuple.get("quitter"));
            socialEconomyEntityDto.setDescription(((LocalizedString) tuple.get("description")).getDefaultText());
            socialEconomyEntityDto.setPictureUrl(this.pamapamUrlHelper.getImageUrl((Long) tuple.get("pictureId")));
            socialEconomyEntityDto.setWeb((String) tuple.get("web"));
            socialEconomyEntityDto.setEmail((String) tuple.get("email"));
            socialEconomyEntityDto.setLatitude((Double) tuple.get("latitude"));
            socialEconomyEntityDto.setLongitude((Double) tuple.get("longitude"));
            socialEconomyEntityDto.setXesBalance((Boolean) tuple.get("xesBalance"));

            LegalFormDto legalFormDto = new LegalFormDto();
            LocalizedString legalFormLocalizedString = (LocalizedString) tuple.get("legalFormName");
            legalFormDto.setName(legalFormLocalizedString.getDefaultText());
            socialEconomyEntityDto.setLegalForm(legalFormDto);

            SectorDto entityMainSector = Optional.ofNullable(tuple.get("mainSectorId")).map(o -> sectors.get(o)).orElse(null);
            if (entityMainSector == null) {
                entityMainSector = Optional.ofNullable(tuple.get("mainOfficeMainSectorId")).map(o -> sectors.get(o)).orElse(null);
            }
            if (entityMainSector != null && entityMainSector.getParent() != null) {
                entityMainSector = entityMainSector.getParent();
            }
            socialEconomyEntityDto.setMainSector(entityMainSector);

            final Set<SectorDto> entitySectors = new HashSet<>();
            sectorIds.get(id).forEach(each -> Optional.ofNullable(each).ifPresent(sectorId -> entitySectors.add(sectors.get(sectorId))));
            socialEconomyEntityDto.setSectors(entitySectors);

            final List<String> entityProductTags = productTags.get(id).stream()
                    .filter(each -> each != null)
                    .collect(Collectors.toList());
            socialEconomyEntityDto.setTags(entityProductTags);

            final List<String> socialEconomyNetworksStrings = socialEconomyNetworks.get(id).stream()
                    .filter(each -> each != null)
                    .map(each -> each.getDefaultText())
                    .collect(Collectors.toList());
            socialEconomyEntityDto.setSocialEconomyNetworks(new HashSet<>(this.socialEconomyNetworkDtoConverter.convertToDtoList(socialEconomyNetworksStrings)));

            final List<String> otherSocialEconomyNetworksStrings = otherSocialEconomyNetworks.get(id).stream()
                    .filter(each -> each != null)
                    .collect(Collectors.toList());
            socialEconomyEntityDto.setOtherSocialEconomyNetworks(new HashSet<>(this.socialEconomyNetworkTagDtoConverter.convertToDtoList(otherSocialEconomyNetworksStrings)));

            final List<String> keywordTagsString = keywordTags.get(id).stream()
                    .filter(each -> each != null)
                    .collect(Collectors.toList());
            socialEconomyEntityDto.setKeywordTags(new HashSet<>(this.keywordTagDtoConverter.convertToDtoList(keywordTagsString)));

            socialEconomyEntityDto.setRegion(regions.get(tuple.get("regionId")));
            socialEconomyEntityDto.setTown(towns.get(tuple.get("townId")));
            socialEconomyEntityDto.setNeighborhood(neighborhoods.get(tuple.get("neighborhoodId")));
            socialEconomyEntityDto.setDistrict(districts.get(tuple.get("districtId")));
            socialEconomyEntityDto.setEntityEvaluation(evaluations.get(tuple.get("entityEvaluationId")));
            result.add(socialEconomyEntityDto);

        });

        result.sort((a, b) -> a.getName().compareTo(b.getName()));

        return result;
    }


    public Page<SocialEconomyEntity> findTopEntities(final List<EntityStatus> entityStatus, final PamapamUser user, final Set<EntityScope> entityScopes, final int count) {
        final PageRequest pageRequest = new PageRequest(0, count);

        if (user == null) {
            return this.socialEconomyEntityRepository.findTopByStatus(entityStatus, entityScopes, pageRequest);
        } else if (user.isInitiative()) {
            return this.socialEconomyEntityRepository.findTopByStatusAndInitiativeUser(entityStatus, user, entityScopes, pageRequest);
        } else {
            return this.socialEconomyEntityRepository.findTopByStatusAndUser(entityStatus, user, entityScopes, pageRequest);
        }
    }

    public List<SocialEconomyEntity> findSiblingEntities(final SocialEconomyEntity entity) {
        return this.socialEconomyEntityRepository.findByIdNotAndGlobalId(entity.getId(), entity.getGlobalId());
    }

    public List<SocialEconomyEntity> findOfficeEntities(final SocialEconomyEntity entity) {
        return this.socialEconomyEntityRepository.findByMainOffice(entity);
    }

    public long countOfficeEntities(final SocialEconomyEntity entity) {
        return this.socialEconomyEntityRepository.countByMainOffice(entity);
    }

    public boolean hasDraftSibling(final SocialEconomyEntity entity) {
        return this.findSiblingEntities(entity).stream().anyMatch(each -> each.getEntityStatus().isDraft());
    }

    // ...	Action methods.

    public void addPamapamProposed(final SocialEconomyEntityProposedDto proposedDto) throws CrudException {
        this.addProposed(proposedDto, this.entityScopeService.getDefaultEntityScopes());
    }

    public void addPamaPamPolIntercoop(final SocialEconomyEntityInitiativeDto proposedDto) throws CrudException, UserAlreadyExistsException {
        final Questionaire essQuestionaire = this.questionaireRepository.findOne(1L);
        final EntityScope intercoop = this.entityScopeService.getPolIntercoopEntityScope();
        final EntityScope pamapam = this.entityScopeService.getDefaultEntityScope();
        final Set<EntityScope> entityScopes = new HashSet<>(Arrays.asList(intercoop, pamapam));

        this.addIntercoopInitiative(proposedDto, entityScopes, essQuestionaire);
    }

    public void addProposed(final SocialEconomyEntityProposedDto proposedDto, final Set<EntityScope> entityScopes) throws CrudException {
        final SocialEconomyEntity entity = new SocialEconomyEntity();
        entity.setName(proposedDto.getName());
        entity.setEntityScopes(entityScopes);
        if (proposedDto.getProvinceId() != null) {
            entity.setProvince(this.territoryService.findProvince(proposedDto.getProvinceId()));
        }
        if (proposedDto.getRegionId() != null) {
            entity.setRegion(this.territoryService.findRegion(proposedDto.getRegionId()));
        }
        if (proposedDto.getTownId() != null) {
            entity.setTown(this.territoryService.findTown(proposedDto.getTownId()));
        }
        if (proposedDto.getDistrictId() != null) {
            entity.setDistrict(this.territoryService.findDistrict(proposedDto.getDistrictId()));
        }
        if (proposedDto.getNeighborhoodId() != null) {
            entity.setNeighborhood(this.territoryService.findNeighborhood(proposedDto.getNeighborhoodId()));
        }
        entity.setAddress(proposedDto.getAddress());
        entity.setEmail(proposedDto.getEmail());
        entity.setPhone(proposedDto.getPhone());
        entity.setWeb(proposedDto.getWeb());
        entity.setDescription(this.createLocalizedString(proposedDto.getDescription()));
        entity.setEntityStatus(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.PROPOSED));
        entity.setRegistryDate(new Date());

        if (proposedDto.getExternalFilterTags() != null) {
            final Set<ExternalFilterTag> externalFilterTags = new HashSet<>();
            final Set<String> tags = Sets.newHashSet(proposedDto.getExternalFilterTags().split(",")).stream()
                    .map(o -> o.trim())
                    .collect(Collectors.toSet());
            externalFilterTags.addAll(this.externalFilterTagRepository.findByTagTextIn(tags));
            entity.setExternalFilterTags(externalFilterTags);
        }

        this.save(entity, null);
    }

    @Transactional
    public void addPamapamInitiative(final SocialEconomyEntityInitiativeDto initiativeDto) throws CrudException, UserAlreadyExistsException {
        this.addInitiative(initiativeDto, this.entityScopeService.getDefaultEntityScopes());
    }

    @Transactional
    public void addIntercoopInitiative(final SocialEconomyEntityInitiativeDto initiativeDto, final Set<EntityScope> entityScopes, final Questionaire questionaire) throws CrudException, UserAlreadyExistsException {
        this.addInitiative(initiativeDto, entityScopes);
    }

    @Transactional
    public void addEinatecaInitiative(final SocialEconomyEntityInitiativeDto initiativeDto) throws CrudException, UserAlreadyExistsException {
        this.addInitiative(initiativeDto, this.entityScopeService.getEinatecaEntityScopes());
    }

    @Transactional
    public void addInitiative(final SocialEconomyEntityInitiativeDto initiativeDto, final Set<EntityScope> entityScopes) throws CrudException, UserAlreadyExistsException {
        final PamapamUser initiativeUser = this.createInitiativeUser(initiativeDto.getEmail(), initiativeDto.getName());

        final SocialEconomyEntity entity = new SocialEconomyEntity();
        entity.setEntityScopes(entityScopes);
        entity.setName(initiativeDto.getName());
        entity.setNif(initiativeDto.getNif());
        if (initiativeDto.getProvinceId() != null) {
            entity.setProvince(this.territoryService.findProvince(initiativeDto.getProvinceId()));
        }
        if (initiativeDto.getRegionId() != null) {
            entity.setRegion(this.territoryService.findRegion(initiativeDto.getRegionId()));
        }
        if (initiativeDto.getTownId() != null) {
            entity.setTown(this.territoryService.findTown(initiativeDto.getTownId()));
        }
        if (initiativeDto.getDistrictId() != null) {
            entity.setDistrict(this.territoryService.findDistrict(initiativeDto.getDistrictId()));
        }
        if (initiativeDto.getNeighborhoodId() != null) {
            entity.setNeighborhood(this.territoryService.findNeighborhood(initiativeDto.getNeighborhoodId()));
        }
        entity.setAddress(initiativeDto.getAddress());
        entity.setPostalCode(initiativeDto.getPostalCode());
        entity.setOpeningHours(initiativeDto.getOpeningHours());

        if (initiativeDto.getProductTags() != null) {
            final Set<ProductTag> productTags = new HashSet<>();
            final String[] tags = initiativeDto.getProductTags().split(",");
            for (final String eachTag : tags) {
                productTags.add(this.productTagService.findOrCreate(eachTag.trim()));
            }
            entity.setProductTag(productTags);
        }

        if (initiativeDto.getOtherSocialEconomyNetworks() != null) {
            final Set<SocialEconomyNetworkTag> socialEconomyNetworkTags = new HashSet<>();
            final String[] tags = initiativeDto.getOtherSocialEconomyNetworks().split(",");
            for (final String eachTag : tags) {
                socialEconomyNetworkTags.add(this.socialEconomyNetworkTagService.findOrCreate(eachTag.trim()));
            }
            entity.setOtherSocialEconomyNetworks(socialEconomyNetworkTags);
        }

        if (initiativeDto.getFoundationYear() != null) {
            entity.setFoundationYear(initiativeDto.getFoundationYear());
        }

        if (initiativeDto.getExternalFilterTags() != null) {
            final Set<ExternalFilterTag> externalFilterTags = new HashSet<>();
            final Set<String> tags = Sets.newHashSet(initiativeDto.getExternalFilterTags().split(",")).stream()
                    .map(o -> o.trim())
                    .collect(Collectors.toSet());
            externalFilterTags.addAll(this.externalFilterTagRepository.findByTagTextIn(tags));
            entity.setExternalFilterTags(externalFilterTags);
        }

        if (initiativeDto.getSocialBalance() != null) {
            switch (initiativeDto.getSocialBalance()) {
                case 1:
                    entity.setXesBalance(true);
                    break;
                case 2:
                    entity.setXesBalance(false);
                    break;

                default:
                    break;
            }
        }

        entity.setEmail(initiativeDto.getEmail());
        entity.setWeb(initiativeDto.getWeb());
        entity.setPhone(initiativeDto.getPhone());
        entity.setTwitter(initiativeDto.getTwitter());
        entity.setFacebook(initiativeDto.getFacebook());
        entity.setInstagram(initiativeDto.getInstagram());
        entity.setPinterest(initiativeDto.getPinterest());
        entity.setQuitter(initiativeDto.getQuitter());
        if (initiativeDto.getMainSectorId() != null) {
            entity.setMainSector(this.sectorRepository.findOne(initiativeDto.getMainSectorId()));
        }
        if (!CollectionUtils.isEmpty(initiativeDto.getSectorsIds())) {
            entity.setSectors(Sets.newHashSet(this.sectorRepository.findByIdIn(initiativeDto.getSectorsIds())));
        }
        if (initiativeDto.getLegalFormId() != null) {
            entity.setLegalForm(this.legalFormRepository.findOne(initiativeDto.getLegalFormId()));
        }
        if (!CollectionUtils.isEmpty(initiativeDto.getSocialEconomyNetworkIds())) {
            entity.setSocialEconomyNetworks(Sets.newHashSet(this.socialEconomyNetworkRepository.findByIdIn(initiativeDto.getSocialEconomyNetworkIds())));
        }
        entity.setDescription(this.createLocalizedString(initiativeDto.getDescription()));
        entity.setContactPerson(initiativeDto.getContactPerson());
        entity.setEntityStatus(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.INITIATIVE));
        entity.setInitiativeUser(initiativeUser);
        entity.setRegistryDate(new Date());
        if (initiativeDto.isUserConditionsAccepted()) {
            entity.setUserConditionsAccepted(true);
            entity.setUserConditionsDate(Timestamp.from(Instant.now()));
        }
        if (initiativeDto.isNewslettersAccepted()) {
            entity.setNewslettersAccepted(true);
            entity.setNewslettersDate(Timestamp.from(Instant.now()));
        }

        if (initiativeDto.getPicture() != null) {
            final DataUri pictureDataUri = DataUri.parse(initiativeDto.getPicture(), Charset.forName("UTF-8"));
            final BinaryResource picture = new BinaryResource(pictureDataUri.getData());
            picture.setMimeType(pictureDataUri.getMime());
            picture.setFileName(pictureDataUri.getFilename());
            entity.setPicture(picture);
        }

        //We add the ess questionnaire as the default questionnaire because it was the first one to exist.
        final Questionaire ess = this.questionaireRepository.findOne(1L);
        entity.setQuestionaire(ess);

        this.save(entity, null);

        if (initiativeDto.getPolCoopLinks() != null) {

            final List<TextUrl> textUrlList = initiativeDto.getPolCoopLinks()
                    .stream()
                    .map(textUrlDto -> this.modelMapper.map(textUrlDto, TextUrl.class))
                    .peek(textUrl -> textUrl.setEntity(entity))
                    .collect(Collectors.toList());

            entity.setIntercooperacioLinks(textUrlList);
            this.save(entity, entity.getEntityStatus());
        }

        this.userService.sendAutoproposedInitiativeWelcomeNotification(initiativeUser, initiativeUser.getRawPassword());
    }

    public String exportToCsv(final SearchSpecification<SocialEconomyEntity, SocialEconomyEntitySearch> searchSpecification) throws IOException, JsonMappingException {
        final List<Sector> sectors = this.sectorRepository.findByParentIsNull();
        Collections.sort(sectors, Sector.NAME_ORDER);

        final CsvMapper csvMapper = new CsvMapper();
        csvMapper.configure(Feature.AUTO_CLOSE_TARGET, false);
        final List<Criterion> newCriteria = this.criterionRepository.findByOldVersionFalse(new Sort("viewOrder"));
        final List<Criterion> oldCriteria = this.criterionRepository.findByOldVersionTrue(new Sort("viewOrder"));
        final List<PersonRole> personRoles = this.personRoleRepository.findAll(new Sort("id"));
        final int discountCount = this.socialEconomyEntityRepository.countMaxDiscounts();

        final CsvSchema csvSchema = this.createSchema(sectors, newCriteria, oldCriteria, personRoles, discountCount);
        final ObjectWriter writer = csvMapper.writer(csvSchema);
        final String fileName = "export-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss")) + ".csv";
        final File tempFile = FileUtils.getFile(FileUtils.getTempDirectory(), fileName);

        final Stream<SocialEconomyEntity> stream = this.socialEconomyEntityRepository.streamAll(searchSpecification);
        final List<List<String>> sectorsData = new ArrayList<>();
        final FileOutputStream tempFileOutputStream = new FileOutputStream(tempFile);
        final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
        final OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, "UTF-8");
        stream.forEach(each -> {
            sectorsData.add(this.buildRow(each, sectors, newCriteria, oldCriteria, personRoles, discountCount));
        });
        writer.writeValue(writerOutputStream, sectorsData);

        tempFileOutputStream.flush();
        tempFileOutputStream.close();
        return tempFile.getAbsolutePath();
    }

    private CsvSchema createSchema(final List<Sector> sectors, final List<Criterion> newCriteria, final List<Criterion> oldCriteria, final List<PersonRole> personRoles, final int discountCount) {
        final CsvSchema.Builder csvBuilder = CsvSchema.builder();
        csvBuilder
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.registrationDate"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.publicationDate"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.name"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.nif"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.foundationYear"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.legalForm"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.entityStatus"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.invoicing"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.email"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.user"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.mainOffice"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.address"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.postalCode"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.latitude"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.longitude"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.province"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.region"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.town"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.district"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.neighborhood"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.userConditionsAccepted"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.userConditionsDate"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.newslettersAccepted"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.newslettersDate"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.entityScopes"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.ideologicalIdentification"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.mainSector"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.mainSubsector"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.url"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.evaluation"));
        for (final Sector eachSector : sectors) {
            final String sectorName = eachSector.getName().getDefaultText();
            csvBuilder
                    .addColumn(sectorName)
                    .addColumn(this.messageSource.getMessage("socialEconomyEntity.subsectors") + " " + sectorName);
        }

        csvBuilder
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.twitter"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.facebook"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.instagram"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.pinterest"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.quitter"));
        for (final PersonRole eachPersonRole : personRoles) {
            final String personRoleName = eachPersonRole.getName().getDefaultText();
            csvBuilder.addColumn(personRoleName + "(" + this.messageSource.getMessage("socialEconomyEntity.personRoles.female") + ")");
            csvBuilder.addColumn(personRoleName + "(" + this.messageSource.getMessage("socialEconomyEntity.personRoles.male") + ")");
            csvBuilder.addColumn(personRoleName + "(" + this.messageSource.getMessage("socialEconomyEntity.personRoles.other") + ")");
        }
        /* ticket https://tickets.jamgo.org/issues/15048 remove fesc fields from export
        csvBuilder
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.urlVideo"));

        csvBuilder.addColumn("(FESC)".concat(this.messageSource.getMessage("socialEconomyEntity.entityStatus")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.contact.email")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.basic.enterprises")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.basic.privateActivists")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.basic.privateNoobs")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.basic.publicAdministration.short")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.fee")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.account")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.name")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.email")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.address")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.postalCode")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.basic.fescSector.csv")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.backoffice.participationPrivacyPolicy")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.backoffice.privacyPolicy")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.modality.name")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.fellowName")))
                .addColumn("(FESC)".concat(this.messageSource.getMessage("fesc.registration.invoice.timeSlot")));

        for (int i = 0; i < discountCount; i++) {
            csvBuilder
                    .addColumn("(" + this.messageSource.getMessage("socialEconomyEntity.discounts") + ")" + this.messageSource.getMessage("socialEconomyEntity.discounts.name"))
                    .addColumn(this.messageSource.getMessage("socialEconomyEntity.discounts.description"))
                    .addColumn(this.messageSource.getMessage("socialEconomyEntity.discounts.url"))
                    .addColumn(this.messageSource.getMessage("socialEconomyEntity.discounts.beginDate"))
                    .addColumn(this.messageSource.getMessage("socialEconomyEntity.discounts.endDate"));
        }
*/
        csvBuilder
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.applyingCriterionCount"))
                .addColumn(this.messageSource.getMessage("socialEconomyEntity.fulfillmentPercent"));
        for (final Criterion eachCriterion : newCriteria) {
        	
        	String criterionViewOrder = String.valueOf(eachCriterion.getViewOrder());
        	
            csvBuilder
                    .addColumn(criterionViewOrder + "_" + eachCriterion.getName().getDefaultText())
                    /*The evaluation of each criteria*/
                    .addColumn(criterionViewOrder + "_" + eachCriterion.getName().getDefaultText() + "_notes");
            final List<CriterionAnswer> criterionAnswers = Lists.newArrayList(eachCriterion.getCriterionAnswers());
            Collections.sort(criterionAnswers, CriterionAnswer.QUESTIONARY_ANSWER_ORDER);
            for (final CriterionAnswer eachCriterionAnswer : criterionAnswers) {
            	String questionaireName = eachCriterionAnswer.getQuestionaires().isEmpty() 
            			? "" 
            			: eachCriterionAnswer.getQuestionaires().iterator().next().getName().getDefaultText();
                csvBuilder.addColumn("c_" + criterionViewOrder + "_" + String.valueOf(eachCriterionAnswer.getAnswerOrder() + "_"+questionaireName));
            }
        }
        for (final Criterion eachCriterion : oldCriteria) {
            csvBuilder
                    .addColumn(eachCriterion.getName().getDefaultText() + "_old")
                    /*The evaluation of each criteria*/
                    .addColumn(eachCriterion.getName().getDefaultText() + "_old_avaluacio");
        }

        return csvBuilder.build().withHeader();
    }

    private List<String> buildRow(final SocialEconomyEntity entity, final List<Sector> sectors, final List<Criterion> newCriteria, final List<Criterion> oldCriteria, final List<PersonRole> personRoles, final int discountCount) {
        final List<String> entityRow = new ArrayList<>();
        final SimpleDateFormat dayMonthYear = new SimpleDateFormat("dd/MM/yyyy");
        entityRow.add(Optional.ofNullable(entity.getRegistryDate()).map(o -> dayMonthYear.format(o)).orElse(""));
        if (entity.getEntityStatus().isPublished()) {
            entityRow.add(Optional.ofNullable(entity.getLastPublishedDate()).map(o -> dayMonthYear.format(o)).orElse(""));
        } else {
            entityRow.add("No publicat");
        }
        entityRow.add(Optional.ofNullable(entity.getName()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getNif()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFoundationYear()).map(o -> String.valueOf(o)).orElse(""));
        //entityRow.add("");
        entityRow.add(Optional.ofNullable(entity.getLegalForm()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getEntityStatus()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getInvoicing()).map(o -> String.valueOf(o)).orElse(""));
        //entityRow.add(String.valueOf(Optional.ofNullable(entity.getInvoicing()).orElse("")));
        //entityRow.add("");
        entityRow.add(Optional.ofNullable(entity.getEmail()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getRegistryUser()).map(o -> o.getFullName()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getMainOffice()).map(o -> o.getName()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getAddress()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getPostalCode()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getLatitude()).map(o -> entity.getLatitude().toString()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getLongitude()).map(o -> entity.getLongitude().toString()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getProvince()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getRegion()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getTown()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getDistrict()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getNeighborhood()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getUserConditionsAccepted()).map(o -> o ? "1" : "0").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getUserConditionsDate()).map(o -> dayMonthYear.format(o)).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getNewslettersAccepted()).map(o -> o ? "1" : "0").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getNewslettersDate()).map(o -> dayMonthYear.format(o)).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getEntityScopesString()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getIdeologicalIdentification()).map(o -> o.getName()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getMainSector())
                .map(o -> Optional.ofNullable(o.getParent()).orElse(o))
                .map(o -> o.getName())
                .map(o -> o.getDefaultText())
                .orElse(""));
        entityRow.add(Optional.ofNullable(entity.getMainSector()).map(o -> o.getName()).map(o -> o.getDefaultText()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getNormalizedName()).map(o -> "https://pamapam.cat/directori/" + o).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getQuestionaire()).map(o -> o.getName().getDefaultText()).orElse(""));
        for (final Sector eachSector : sectors) {
            if (entity.belongsToSector(eachSector)) {
                entityRow.add("1");
                final Joiner subsectorsJoiner = Joiner.on(" - ");
                final String subsectorsString = subsectorsJoiner.join(entity.getSectors().stream()
                        .filter(each -> each.isSubsector() && each.belongsToSector(eachSector))
                        .map(each -> each.getName().getDefaultText())
                        .toArray());
                entityRow.add(subsectorsString);
            } else {
                entityRow.add("0");
                entityRow.add("");
            }
        }

        entityRow.add(Optional.ofNullable(entity.getTwitter()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFacebook()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getInstagram()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getPinterest()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getQuitter()).orElse(""));

        final Map<Long, List<Integer>> personRolesMap = entity.getEntityPersonRoles()
                .stream().collect(Collectors.toMap(o -> o.getPersonRole().getId(), o -> Lists.newArrayList(o.getFemaleCount(), o.getMaleCount(), o.getOtherCount())));
        for (final PersonRole eachPersonRole : personRoles) {
            final List<Integer> personRoleValues = Optional.ofNullable(personRolesMap.get(eachPersonRole.getId())).orElse(Lists.newArrayList());
            if (!personRoleValues.isEmpty()) {
                entityRow.add(String.valueOf(Optional.ofNullable(personRoleValues.get(0)).orElse(0)));
                entityRow.add(String.valueOf(Optional.ofNullable(personRoleValues.get(1)).orElse(0)));
                entityRow.add(String.valueOf(Optional.ofNullable(personRoleValues.get(2)).orElse(0)));
            } else {
                entityRow.add("");
                entityRow.add("");
                entityRow.add("");
            }
        }
        /* ticket https://tickets.jamgo.org/issues/15048 remove fesc fields from export

        entityRow.add(Optional.ofNullable(entity.getFescVideo()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescEntityStatus()).map(EntityStatus::getName).map(LocalizedString::getDefaultText).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescEmail()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getEnterprises()).map(e -> e ? "1" : "0").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getPrivateActivists()).map(pa -> pa ? "1" : "0").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getPrivateNoobs()).map(pn -> pn ? "1" : "0").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getPublicAdministration()).map(pa -> pa ? "1" : "0").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescFee()).map(Object::toString).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescInvoiceAccount()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescInvoiceName()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescInvoiceEmail()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescInvoiceAddress()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescInvoicePostalCode()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescMainSector()).map(Sector::getName).map(LocalizedString::getDefaultText).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescParticipationPrivacyPolicy()).map(o -> o ? "sí" : "no").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescRegistrationPrivacyPolicy()).map(o -> o ? "sí" : "no").orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFescModalityName()).orElse(""));
        entityRow.add(Optional.ofNullable(entity.getFellowName()).orElse(""));
        entityRow.add(this.messageSource.getMessage(entity.getFescTimeSlotNameReference()));

        for (int i = 0; i < discountCount; i++) {
            if (i < entity.getDiscounts().size()) {
                entityRow.add(Optional.ofNullable(entity.getDiscounts().get(i).getName()).orElse(""));
                entityRow.add(Optional.ofNullable(entity.getDiscounts().get(i).getDescription()).orElse(""));
                entityRow.add(Optional.ofNullable(entity.getDiscounts().get(i).getUrl()).orElse(""));
                entityRow.add(Optional.ofNullable(entity.getDiscounts().get(i).getBeginDate()).map(date -> this.sdf.format(date)).orElse(""));
                entityRow.add(Optional.ofNullable(entity.getDiscounts().get(i).getEndDate()).map(date -> this.sdf.format(date)).orElse(""));
            } else {
                entityRow.addAll(Arrays.asList("", "", "", "", ""));
            }
        }
*/
        if (entity.getEntityEvaluation() != null) {
            /**
             * Rules for exporting evaluations:
             * · If criteria is not complied: 0
             * · If evaluation is a half completion: 0.5
             * · If evaluation is a completed completion: 1
             * · If criteria doesn't apply : ""
             * · If Level is set explicitly instead of calculating using the sum of all criteria: ""
             */

            final Map<Criterion, EntityEvaluationCriterion> criterionValuesMap = entity.getEntityEvaluation().getEntityEvaluationCriterions()
                    .stream().collect(Collectors.toMap(each -> each.getCriterion(), each -> each));
            final long applyingCriterionCount = criterionValuesMap.values().stream().filter(o -> Optional.ofNullable(o).map(x -> x.getLevel()).orElse(0) >= 0).count();
            entityRow.add(String.valueOf(applyingCriterionCount));
            double fulfillmentPercent = 0;
            if (applyingCriterionCount > 0) {
                fulfillmentPercent = (double) criterionValuesMap.values().stream().filter(o -> Optional.ofNullable(o).map(x -> x.getLevel()).orElse(0) > 0).count() / applyingCriterionCount;
            }
            entityRow.add(String.valueOf((new DecimalFormat("###.##")).format(fulfillmentPercent * 100)));
            for (final Criterion eachCriterion : newCriteria) {
                final EntityEvaluationCriterion entityEvaluationCriterion = criterionValuesMap.get(eachCriterion);
                Double level = null;
                if (entityEvaluationCriterion != null) {
                    level = Optional.ofNullable(entityEvaluationCriterion.getLevel()).map(o -> (double) o >= 0 ? Double.valueOf(o) / 100 : -1d).orElse(null);
                }
                entityRow.add(Optional.ofNullable(level).map(o -> o == -1d ? "" : (new DecimalFormat("##")).format(Math.floor(o))).orElse("0"));
                //Evaluation notes
                entityRow.add(Optional.ofNullable(entityEvaluationCriterion).map(o -> o.getNotes()).orElse(""));
                if (entityEvaluationCriterion != null) {
                    final Map<CriterionAnswer, EntityEvaluationCriterionAnswer> criterionAnswersMap = entityEvaluationCriterion.getEntityEvaluationCriterionAnswers()
                            .stream().collect(Collectors.toMap(each -> each.getCriterionAnswer(), each -> each));
                    
                    final List<CriterionAnswer> criterionAnswers = Lists.newArrayList(eachCriterion.getCriterionAnswers());
                    Collections.sort(eachCriterion.getCriterionAnswers(), CriterionAnswer.QUESTIONARY_ANSWER_ORDER);
                    
                    for (final CriterionAnswer eachCriterionAnswer : criterionAnswers) {
                        if (level == null) {
                            if (criterionAnswersMap.isEmpty()) {
                                // it means criteria is 0
                                entityRow.add("0");
                            } else {
                                // this shouldn't happen
                                entityRow.add("");
                            }
                        } else if (criterionAnswersMap.isEmpty()) {
                            // means level has been old style, directly typed without individual criteria evaluation
                            entityRow.add("");
//						} else if (!criterionAnswersMap.isEmpty()) {
//							entityRow.add("0");
                        } else if (level.equals(-1d)) {
                            // it means criteria doesn't apply
                            entityRow.add("");
                        } else {
                            final EntityEvaluationCriterionAnswer evaluationCriterionAnswer = criterionAnswersMap.get(eachCriterionAnswer);
                            entityRow.add(Optional.ofNullable(evaluationCriterionAnswer).map(o -> (new DecimalFormat("###.#")).format(Double.valueOf(o.getValue()) / 100)).orElse("0"));
                        }
                    }
                } else {
                    IntStream.range(0, eachCriterion.getCriterionAnswers().size()).forEach(j -> entityRow.add(""));
                }
            }
            for (final Criterion eachCriterion : oldCriteria) {
                final Integer level = Optional.ofNullable(criterionValuesMap.get(eachCriterion)).map(o -> o.getLevel()).orElse(null);
                entityRow.add(Optional.ofNullable(level).map(o -> String.valueOf(o / 100)).orElse(""));
                //Evaluation notes
                entityRow.add(Optional.ofNullable(criterionValuesMap.get(eachCriterion)).map(o -> o.getNotes()).orElse(""));
            }
        }

        return entityRow;
    }

    /**
     * Saves in the database a SocialEconomyEntity that generates based on a DTO of those returned by the api of XES' social balance.
     * Adds the Social Balance EntityScope, sets the EntityStatus to EXTERNAL and, as it comes from the balance api, sets XesBalance to true.
     *
     * @param entityDto
     * @return saved SocialEconomyEntity
     * @throws CrudException
     */
    @Transactional
    public SocialEconomyEntity saveSocialBalanceEntity(final SocialBalanceEntityDto entityDto) throws CrudException {
        final EntityStatus external = this.entityStatusService.findByEntityStatusType(EntityStatusType.EXTERNAL);

        final LocalizedString description = new LocalizedString();
        description.set("ca", entityDto.getDescription());
        description.set("es", "");

        final Double latitude = Optional.ofNullable(entityDto.getLatitude())
                .map(BigDecimal::doubleValue)
                .orElse(null);

        final Double longitude = Optional.ofNullable(entityDto.getLongitude())
                .map(BigDecimal::doubleValue)
                .orElse(null);

        final String legalFormName = Optional.ofNullable(entityDto.getLegalForm()).map(LegalFormDto::getName).orElse(null);
        final LegalForm legalForm = this.legalFormService.findByName(legalFormName);
        final Sector mainSector = Optional.ofNullable(this.sectorService.balanceConverter(entityDto.getSector())).orElse(null);
        final Province province = this.territoryService.provinceBalanceConverter(entityDto.getProvince());
        final Region region = this.territoryService.regionBalanceConverter(entityDto.getRegion());
        final Town town = this.territoryService.townBalanceConverter(entityDto.getTown());
        final Neighborhood neighborhood = this.territoryService.neighborhoodBalanceConverter(entityDto.getNeighborhood());
        final EntityScope socialBalanceEntityScope = this.entityScopeService.getSocialBalanceEntityScope();
        final EntityEvaluation emptyEntityEvaluation = new EntityEvaluation();
        final BinaryResource logo = new BinaryResource(this.socialBalanceService.getEntityLogo(entityDto.getNif()));
        final String socialBalanceReportUrl = this.socialBalanceService.getBalanceUrl(entityDto);

        final SocialEconomyEntity socialEconomyEntity = new SocialEconomyEntity();
        socialEconomyEntity.setEntityScopes(Sets.newHashSet(socialBalanceEntityScope));    //EntityScope de Balanç Social
        socialEconomyEntity.setNif(entityDto.getNif());
        socialEconomyEntity.setName(entityDto.getName());
        final Integer foundationYear = (entityDto.getCreationYear() == null || entityDto.getCreationYear().isEmpty()) ? null : Integer.parseInt(entityDto.getCreationYear());
        socialEconomyEntity.setFoundationYear(foundationYear);
        socialEconomyEntity.setEntityStatus(external);
        socialEconomyEntity.setDescription(description);
        socialEconomyEntity.setPicture(logo);
        socialEconomyEntity.setLegalForm(legalForm);
        socialEconomyEntity.setContactPerson(entityDto.getEmail());
        socialEconomyEntity.setAddress(entityDto.getAddress());
        socialEconomyEntity.setPostalCode(entityDto.getCp());
        socialEconomyEntity.setWeb(entityDto.getWeb());
        socialEconomyEntity.setEmail(entityDto.getEmail());
        socialEconomyEntity.setPhone(entityDto.getPhone());
        socialEconomyEntity.setTwitter(entityDto.getTwitter());
        socialEconomyEntity.setFacebook(entityDto.getFacebook());
        socialEconomyEntity.setInstagram(entityDto.getInstagram());
        socialEconomyEntity.setMainSector(mainSector);
        socialEconomyEntity.setProvince(province);
        socialEconomyEntity.setRegion(region);
        socialEconomyEntity.setTown(town);
        socialEconomyEntity.setNeighborhood(neighborhood);
        socialEconomyEntity.setXesBalance(true); //If we are taking the entity from the balanç api is because it has made the balanç.
        socialEconomyEntity.setLatitude(latitude);
        socialEconomyEntity.setLongitude(longitude);
        socialEconomyEntity.setUrlBalancSocial(socialBalanceReportUrl);
        socialEconomyEntity.setRegistryDate(new Date());

        //Default values
        socialEconomyEntity.setCustomFields("{}");
        socialEconomyEntity.setEnterprises(false);
        socialEconomyEntity.setLaZonaWeb("");
        socialEconomyEntity.setNewslettersAccepted(false);
        socialEconomyEntity.setOnlineShoppingWeb("");
        socialEconomyEntity.setOpeningHours("");
        socialEconomyEntity.setPinterest("");
        socialEconomyEntity.setPrivateActivists(false);
        socialEconomyEntity.setPrivateNoobs(false);
        socialEconomyEntity.setPublicAdministration(false);
        socialEconomyEntity.setQuitter("");
        socialEconomyEntity.setUrlBalancSocial("");
        socialEconomyEntity.setUserConditionsAccepted(false);
        socialEconomyEntity.setEntityEvaluation(emptyEntityEvaluation);
        socialEconomyEntity.setFacebook("");
        socialEconomyEntity.setInstagram("");
        socialEconomyEntity.setTwitter("");
        //Setting entityPersonRoles with the logic found previously on SocialEconomyEntityDetailsLayout fixes a bug
        //where mainSector icons weren't sent to the frontend when it was a SocialBalance External entity.
        //IDK why but without this entityPersonRoles the SectorDto recursive modelmapper were unable to get iconUrl when converting
        //the entity to the dto.
        socialEconomyEntity.setEntityPersonRoles(this.createNewEntityPersonRoles(socialEconomyEntity));
        return this.save(socialEconomyEntity, null);
    }

    /**
     * Saves in the database a SocialEconomyEntity that generates based on a DTO of those returned by the green commerce import process.
     * Adds the green commerce EntityScope, sets the EntityStatus to EXTERNAL and, as it comes from the green commerce import, sets greenCommerce to true.
     *
     * @param entityDto
     * @return saved SocialEconomyEntity
     * @throws CrudException
     */
    @Transactional
    public SocialEconomyEntity saveGreenCommerceEntity(final SocialEconomyEntity socialEconomyEntity) throws CrudException {
        final EntityStatus external = this.entityStatusService.findByEntityStatusType(EntityStatusType.EXTERNAL);

        final EntityScope greenCommerceEntityScope = this.entityScopeService.getGreenCommerceEntityScope();
        final EntityEvaluation emptyEntityEvaluation = new EntityEvaluation();

        SocialEconomyEntity result = null;

        if (socialEconomyEntity.getId() == null) {
        	socialEconomyEntity.setGreenCommerce(true);
        	socialEconomyEntity.setEntityScopes(Sets.newHashSet(greenCommerceEntityScope)); 
	        socialEconomyEntity.setEntityStatus(external);
	        socialEconomyEntity.setXesBalance(false); 
	        
	        socialEconomyEntity.setRegistryDate(new Date());
	        socialEconomyEntity.setNewslettersAccepted(false);
	        socialEconomyEntity.setNewslettersDate(null);
	        
//	        //Setting entityPersonRoles with the logic found previously on SocialEconomyEntityDetailsLayout fixes a bug
	        //where mainSector icons weren't sent to the frontend when it was a SocialBalance External entity.
	        //IDK why but without this entityPersonRoles the SectorDto recursive modelmapper were unable to get iconUrl when converting
	        //the entity to the dto.
	        socialEconomyEntity.setEntityPersonRoles(this.createNewEntityPersonRoles(socialEconomyEntity));
        
	        //Default values
	        socialEconomyEntity.setCustomFields("{}");
	        socialEconomyEntity.setEnterprises(false);
	        socialEconomyEntity.setLaZonaWeb("");
	        socialEconomyEntity.setNewslettersAccepted(false);
	        socialEconomyEntity.setOnlineShoppingWeb("");
	        socialEconomyEntity.setOpeningHours("");
	        socialEconomyEntity.setPinterest("");
	        socialEconomyEntity.setPrivateActivists(false);
	        socialEconomyEntity.setPrivateNoobs(false);
	        socialEconomyEntity.setPublicAdministration(false);
	        socialEconomyEntity.setQuitter("");
	        socialEconomyEntity.setUrlBalancSocial("");
	        socialEconomyEntity.setUserConditionsAccepted(false);
	        socialEconomyEntity.setEntityEvaluation(emptyEntityEvaluation);
	        EntityStatusChange entityStatusChange = new EntityStatusChange();
	        entityStatusChange.setEntity(socialEconomyEntity);
	        entityStatusChange.setEntityStatusFrom(external);
	        entityStatusChange.setEntityStatusTo(external);
	        socialEconomyEntity.addEntityStatusChange(entityStatusChange);
	        
	        try {
	        	result = this.save(socialEconomyEntity, null);
	        	return result;
	        } catch ( Exception e) {
	        	e.getMessage();
	        }
	        
        } else {
            List<SocialEconomyEntity> existingMatrixAndOtherEntityLocations = this.findByNif(socialEconomyEntity.getNif());
            for (SocialEconomyEntity eachEntity : existingMatrixAndOtherEntityLocations) {
            	eachEntity.setGreenCommerce(true);

	        	Set<EntityScope> scopes = eachEntity.getEntityScopes();
	        	if ( scopes == null ) {
	        		eachEntity.setEntityScopes(Sets.newHashSet(greenCommerceEntityScope)); 
	        	} else {
	        		scopes.add(greenCommerceEntityScope); 
	        	}
	        	
	        	try {
	            	result = this.save(eachEntity, null);
	            	//WARNING: as return is not actually being used, we'll return the last occurrence in this case
	        	} catch ( Exception e) {
	            	e.getMessage();
	            }
            
            }
        }
        
        return result;
    }

    
    /**
     * Inspired on what was already in SocialEconomyDetailsLayout.
     */
    private List<PersonRole> getAllPersonRoles() {
        List<PersonRole> allPersenRoles = Lists.newArrayList();
        try {
            allPersenRoles = this.datasourceServices.getAll(PersonRole.class);
            Collections.sort(allPersenRoles, PersonRole.ID_ORDER);
        } catch (final RepositoryForClassNotDefinedException e) {
            // ...	Do nothing. Leaves allCriteria list empty.
        }
        return allPersenRoles;
    }

    /**
     * Inspired on what was already in SocialEconomyDetailsLayout.
     */
    public List<EntityPersonRole> createNewEntityPersonRoles(final SocialEconomyEntity entity) {
        final List<EntityPersonRole> entityPersonRoles = Lists.newArrayList();
        for (final PersonRole eachPersonRole : this.getAllPersonRoles()) {
            entityPersonRoles.add(new EntityPersonRole(entity, eachPersonRole));
        }
        return entityPersonRoles;
    }

    public void updateUrlAndBalanceXes(final SocialBalanceEntityDto entityDto) {
        final List<SocialEconomyEntity> entities = this.socialEconomyEntityService.findByNif(entityDto.getNif());
        final EntityScope socialBalanceEntityScope = this.entityScopeService.getSocialBalanceEntityScope();

        entities.forEach(entity -> {
            final Set<EntityScope> scopes = entity.getEntityScopes();
            scopes.add(socialBalanceEntityScope);

            entity.setXesBalance(true);
            entity.setUrlBalancSocial(entityDto.getUrl());
            entity.setEntityScopes(scopes);

            try {
                this.save(entity, entity.getEntityStatus());
            } catch (final Exception e) {
                this.logger.error("Has been an error updating balance data of " + entity.getName());
                e.printStackTrace();
            }
        });
    }
    
//    public void updateUrlAndGreenCommerce(final GreenCommerceEntityDto entityDto) {
//        final List<SocialEconomyEntity> entities = this.socialEconomyEntityService.findByNif(entityDto.getNif());
//        final EntityScope greenCommerceEntityScope = this.entityScopeService.getGreenCommerceEntityScope();
//
//        entities.forEach(entity -> {
//            final Set<EntityScope> scopes = entity.getEntityScopes();
//            scopes.add(greenCommerceEntityScope);
//
//            entity.setGreenCommerce(true);
//            entity.setEntityScopes(scopes);
//
//            try {
//                this.save(entity, entity.getEntityStatus());
//            } catch (final Exception e) {
//                this.logger.error("Has been an error updating green commerce data of " + entity.getName());
//                e.printStackTrace();
//            }
//        });
//    }

    public SocialEconomyEntity savePendingSocialEconomyEntity(final SocialEconomyEntity entity) throws CrudException {
        final List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();
        final EntityStatus pendingStatus = entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.REVIEW_PENDING).findFirst().get();
        return this.saveWithStatus(entity, pendingStatus);
    }

    public SocialEconomyEntity saveDraftSocialEconomyEntity(final SocialEconomyEntity entity) throws CrudException {
        final List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();
        final EntityStatus pendingStatus = entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.DRAFT).findFirst().get();
        return this.saveWithStatus(entity, pendingStatus);
    }

    public SocialEconomyEntity saveWithStatus(final SocialEconomyEntity entity, final EntityStatus newStatus) throws CrudException {
        final EntityStatus previousEntityStatus = entity.getEntityStatus();
        entity.setEntityStatus(newStatus);
        return this.save(entity, previousEntityStatus);
    }

    public SocialEconomyEntity save(final SocialEconomyEntity entity, final EntityStatus previousEntityStatus) throws CrudException {
        EntityAction entityAction = null;
        if (entity.getId() == null) {
            entityAction = EntityAction.CREATE;
            this.updateEntityStatusChange(entity, null, entity.getEntityStatus());
        } else if (!entity.getEntityStatus().equals(previousEntityStatus)) {
            entityAction = EntityAction.UPDATE_STATUS;
            this.updateEntityStatusChange(entity, previousEntityStatus, entity.getEntityStatus());
        } else {
            entityAction = EntityAction.UPDATE;
        }

        if (CollectionUtils.isEmpty(entity.getEntityScopes())) {
            entity.setEntityScopes(this.entityScopeService.getCurrentEntityScopes());
        }

        entity.setLastUpdateDate(Timestamp.from(Instant.now()));
        entity.updateGlobalId();

        if (entity.getRegistryUser() != null && entity.getRegistryUser().isInitiative() && entity.getInitiativeUser() == null) {
            entity.setInitiativeUser(entity.getRegistryUser());
        }
        entity.setGreenCommerce(entity.getEntityScopes().contains(this.entityScopeService.getGreenCommerceEntityScope()));


        if (entity.getEntityStatus().isPublished()) {
            final SocialEconomyEntity publishedSibling = this.findSiblingEntities(entity).stream()
                    .filter(each -> each.getEntityStatus().isPublished()).findFirst().orElse(null);
            if (publishedSibling != null) {
                publishedSibling.setEntityStatus(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.ARCHIVED));
                this.crudServices.save(publishedSibling);
            } else if (entity.getEmail() != null) {
                PamapamUser initiativeUser = this.userService.findByUsername(StringUtils.trim(entity.getEmail()));
                if (initiativeUser == null) {
                    initiativeUser = this.createInitiativeUser(entity.getEmail(), entity.getContactPerson());
                    this.userService.sendUserCreatedNotification(initiativeUser, initiativeUser.getRawPassword());
                } else if (initiativeUser.getPassword() == null) {
                    PamapamUserService.assignPassword(initiativeUser);
                    this.userService.save(initiativeUser);
                    this.userService.sendUserCreatedNotification(initiativeUser, initiativeUser.getRawPassword());
                }
                if (!initiativeUser.equals(entity.getInitiativeUser())) {
                    entity.setInitiativeUser(initiativeUser);
                }
            }
        }

        if (StringUtils.isEmpty(entity.getNormalizedName()) &&
                (entity.getEntityStatus().getEntityStatusType() == EntityStatusType.PUBLISHED ||
                        entity.getEntityStatus().getEntityStatusType() == EntityStatusType.EXTERNAL)) {

            this.normalizeName(entity);
        }

        final SocialEconomyEntity updatedEntity = this.crudServices.save(entity);
        try {
            this.fullTextService.indexEntity(updatedEntity);
        } catch (final IOException e) {
            // TODO Log lucene indexing fail.
            e.printStackTrace();
        }
        this.asyncTaskExecutor.execute(this.sendNotification(updatedEntity, entityAction));
        return updatedEntity;
    }

    public void acceptConditionsFromPrepublishedEmail(final SocialEconomyEntity entity) throws CrudException {

        entity.setLastUpdateDate(Timestamp.from(Instant.now()));
        entity.updateGlobalId();

        if (entity.getRegistryUser() != null && entity.getRegistryUser().isInitiative() && entity.getInitiativeUser() == null) {
            entity.setInitiativeUser(entity.getRegistryUser());
        }

        if (entity.getEntityStatus().isPublished()) {
            final SocialEconomyEntity publishedSibling = this.findSiblingEntities(entity).stream()
                    .filter(each -> each.getEntityStatus().isPublished()).findFirst().orElse(null);
            if (publishedSibling != null) {
                publishedSibling.setEntityStatus(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.ARCHIVED));
                this.crudServices.save(publishedSibling);
            } else if (entity.getEmail() != null) {
                final PamapamUser initiativeUser = this.createInitiativeUser(entity.getEmail(), entity.getContactPerson());
                entity.setInitiativeUser(initiativeUser);
                if (entity.getInitiativeUser().getRawPassword() == null) {
                    PamapamUserService.assignPassword(initiativeUser);
                    this.userService.save(initiativeUser);
                    entity.setInitiativeUser(initiativeUser);
                }
                this.userService.sendUserCreatedNotification(initiativeUser, initiativeUser.getRawPassword());
            }
        }

        if (StringUtils.isEmpty(entity.getNormalizedName()) &&
                (entity.getEntityStatus().getEntityStatusType() == EntityStatusType.PUBLISHED ||
                        entity.getEntityStatus().getEntityStatusType() == EntityStatusType.EXTERNAL)) {

            this.normalizeName(entity);
        }

        final SocialEconomyEntity updatedEntity = this.crudServices.save(entity);
        try {
            this.fullTextService.indexEntity(updatedEntity);
        } catch (final IOException e) {
            this.logger.error("There was an error when indexing entity with id {} and name {} after accepting conditions from prepublished email.\n\n {}", updatedEntity.getId(), updatedEntity.getName(), e.getMessage());
        }
        this.asyncTaskExecutor.execute(this.sendNotification(updatedEntity, EntityAction.UPDATE));
    }

    public static SocialEconomyEntity setEntityStatusToFirstNext(final SocialEconomyEntity entity) {
        final Set<EntityStatus> nextStatuses = Optional.ofNullable(entity.getEntityStatus())
                .map(EntityStatus::getNextStatuses).orElse(null);
        //mejor name=publicat || Publicat
        final EntityStatus firstNextStatus = nextStatuses.iterator().next();
        entity.setEntityStatus(firstNextStatus);

        return entity;
    }

    private void updateEntityStatusChange(final SocialEconomyEntity entity, final EntityStatus previousStatus, final EntityStatus nextStatus) {
        final EntityStatusChange entityStatusChange = new EntityStatusChange();
        entityStatusChange.setEntityStatusFrom(previousStatus);
        entityStatusChange.setEntityStatusTo(nextStatus);
        entityStatusChange.setChangeTimestamp(Timestamp.from(Instant.now()));
        entityStatusChange.setUser(this.userService.getCurrentUser());
        entity.addEntityStatusChange(entityStatusChange);
    }

    private PamapamUser createInitiativeUser(final String email, final String name) throws CrudException {
        PamapamUser initiativeUser = this.userService.findByUsername(StringUtils.trim(email));
        // ...	Check if user already exists.
        if (initiativeUser == null) {
            final PamapamUserDto userDto = new PamapamUserDto();
            userDto.setUsername(email);
            userDto.setEmail(email);
            if (!StringUtils.isEmpty(name)) {
                userDto.setName(name);
            } else {
                userDto.setName(email);
            }
            userDto.setRoleNames(Lists.newArrayList(PamapamUser.ROLE_INITIATIVE));
            initiativeUser = this.userService.basicAddUser(userDto);
        } else {
            if (initiativeUser.getPassword() == null) {
                PamapamUserService.assignPassword(initiativeUser);
                this.userService.save(initiativeUser);
            }
        }
        return initiativeUser;
    }

    private Runnable sendNotification(final SocialEconomyEntity entity, final EntityAction entityAction) {
        return () -> {
            this.notificationService.changedSocialEconomyEntity(entity, entityAction);
        };
    }

    private void normalizeName(final SocialEconomyEntity entity) {
        if (!StringUtils.isEmpty(entity.getName())) {
            String normalizedName = SocialEconomyEntityService.normalizeText(entity.getName());

            // ...	If normalized name already exists, add territory to text.
            SocialEconomyEntity matchingEntity = this.findByNormalizedName(normalizedName);
            if ((matchingEntity != null) && (matchingEntity.getGlobalId() != entity.getGlobalId())) {
                normalizedName = SocialEconomyEntityService.normalizeText(entity.getName() + " " + entity.getLastTerritoryName());
            }

            // ...	If normalized name with territory already exists, add globalId as suffix.
            matchingEntity = this.findByNormalizedName(normalizedName);
            if ((matchingEntity != null) && (matchingEntity.getGlobalId() != entity.getGlobalId())) {
                normalizedName = normalizedName + "-" + String.valueOf(entity.getGlobalId());
            }

            System.out.println(String.valueOf(entity.getId()) + "|" + normalizedName);
            entity.setNormalizedName(normalizedName);
        }
    }

    private static String normalizeText(final String text) {
        final Tokenizer tokenizer = new StandardTokenizer();
        tokenizer.setReader(new StringReader(text));
        TokenStream tokenStream = new StandardFilter(tokenizer);
        tokenStream = new ElisionFilter(tokenStream, SocialEconomyEntityService.DEFAULT_ARTICLES);
        tokenStream = new LowerCaseFilter(tokenStream);
        tokenStream = new StopFilter(tokenStream, CatalanAnalyzer.getDefaultStopSet());
        tokenStream = new StopFilter(tokenStream, SpanishAnalyzer.getDefaultStopSet());
        tokenStream = new ASCIIFoldingFilter(tokenStream);
        String normalizedName = null;
        try {
            tokenStream.reset();
            final CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);
            final StringBuilder normalizedNameBuilder = new StringBuilder();
            String separator = "";
            while (tokenStream.incrementToken()) {
                normalizedNameBuilder.append(separator).append(Normalizer.normalize(token.toString(), Form.NFD)
                        .replaceAll("[^A-Za-z0-9 ]+", ""));
                separator = "-";
            }
            normalizedName = normalizedNameBuilder.toString();
            tokenStream.close();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return normalizedName;
    }

    public boolean conditionsAcceptance(final Integer which, final Long userId, final String token) {
        List<UserActionToken> userActionTokens = this.userActionTokenRepository.findByUserIdAndTokenAndAction(userId, token, "conditionsAcceptance");
        userActionTokens = userActionTokens.stream()
                .filter(each -> Timestamp.from(Instant.now()).before(each.getValidTo()))
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(userActionTokens)) {
            // TODO: return no active token feedback
            return false;
        } else {
            final boolean userConditionsAccepted = which > 0;
            final Timestamp userConditionDate = userConditionsAccepted ? Timestamp.from(Instant.now()) : null;
            final boolean newslettersAccepted = which > 1;
            final Timestamp newslettersDate = newslettersAccepted ? Timestamp.from(Instant.now()) : null;

            final SocialEconomyEntity socialEconomyEntity = this.socialEconomyEntityRepository.findOne(userId);
            socialEconomyEntity.setUserConditionsAccepted(userConditionsAccepted);
            socialEconomyEntity.setUserConditionsDate(userConditionDate);
            socialEconomyEntity.setNewslettersAccepted(newslettersAccepted);
            socialEconomyEntity.setNewslettersDate(newslettersDate);

            try {
                this.save(socialEconomyEntity, socialEconomyEntity.getEntityStatus());
            } catch (final CrudException e) {
                throw new RuntimeException(e);
            }

            return true;

//			final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
//			searchObject.setInitiativeUser(initiativeUser);

//			this.searchSpecification.setSearchObject(searchObject);
//			this.socialEconomyEntityRepository.streamAll(this.searchSpecification).forEach(each -> {
//				each.setUserConditionsAccepted(userConditionsAccepted);
//				each.setUserConditionsDate(userConditionDate);
//				each.setNewslettersAccepted(newslettersAccepted);
//				each.setNewslettersDate(newslettersDate);
//				try {
//					this.save(each, each.getEntityStatus());
//				} catch (final CrudException e) {
//					throw new RuntimeException(e);
//				}
//			});
//			// TODO: return ok feedback
//			return true;
//		}
        }
    }

    public boolean isIntercoop(final SocialEconomyEntity entity) {
        final EntityScope intercoop = this.entityScopeService.getIntercooperacioEntityScope();
        return entity.getEntityScopes().contains(intercoop);
    }

}
