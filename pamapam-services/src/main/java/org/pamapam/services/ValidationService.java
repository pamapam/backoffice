package org.pamapam.services;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.IBANValidator;
import org.apache.commons.validator.routines.UrlValidator;
import org.jamgo.model.entity.LocalizedString;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * We assume all null values are valid values.
 *
 * @author aheredia
 *
 */
@Service
public class ValidationService {

	public boolean isValidIBAN(final String code) {
		if (StringUtils.isEmpty(code)) {
			return true;
		}
		// Removing spaces in code
		return IBANValidator.getInstance().isValid(code.replace(" ", ""));
	}

	public boolean isValidEmail(final String email) {
		if (StringUtils.isEmpty(email)) {
			return true;
		}
		return EmailValidator.getInstance().isValid(email);
	}

	public boolean isValidUrl(final String url) {
		if (StringUtils.isEmpty(url)) {
			return true;
		}
		return UrlValidator.getInstance().isValid(url);
	}

	public boolean isValidRequiredLocalizedString(final LocalizedString email) {
		if (StringUtils.isEmpty(email.getRawValue())) {
			return false;
		}

		return true;
	}

}
