package org.pamapam.services;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamapam.services.config.PamapamBackofficeServicesTestConfig;
import org.pamapam.services.dto.SectorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { PamapamBackofficeServicesTestConfig.class, SectorServiceTest.Config.class })
public class SectorServiceTest {

	@Configuration
	static class Config {
		@Bean
		public SectorService sectorService() {
			return new SectorService();
		}
	}

	@Autowired
	private SectorService sectorService;

	@Test
	@Ignore // FIXME: Disabled until regeneration of obfuscated test data.
	public void testFindOneDto() {
		SectorDto sectorDto = this.sectorService.findOneDto(1L, SectorDto.class);
		Assert.assertNotNull(sectorDto);
	}
}
