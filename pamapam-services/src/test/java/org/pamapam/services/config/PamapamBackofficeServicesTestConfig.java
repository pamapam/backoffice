package org.pamapam.services.config;

import org.jamgo.services.session.SessionContext;
import org.jamgo.services.session.SessionContextImpl;
import org.pamapam.model.config.PamapamBackofficeModelTestConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@Import({ PamapamBackofficeServicesConfig.class, PamapamBackofficeModelTestConfig.class })
@ComponentScan(basePackageClasses = {
	org.jamgo.services.PackageMarker.class,
	org.pamapam.services.PackageMarker.class,
})
public class PamapamBackofficeServicesTestConfig {

	@Bean
	public PamapamUrlHelper pamapamUrlHelper() {
		return new PamapamUrlHelper();
	}

	@Bean
	public JavaMailSender javaMailSender() {
		return new JavaMailSenderImpl();
	}

	@Bean
	public freemarker.template.Configuration configuration() {
		return new freemarker.template.Configuration(freemarker.template.Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
	}

	@Bean
	public AsyncTaskExecutor asyncTaskExecutor() {
		return new SimpleAsyncTaskExecutor();
	}

	@Bean(name = "permissionProperties")
	public Properties properties() {
		return new Properties();
	}

	@Bean
	public SessionContext sessionContext() {
		return new SessionContextImpl();
	}

}
