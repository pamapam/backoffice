package org.pamapam.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.jamgo.model.entity.LocalizedString;
import org.jamgo.services.message.LocalizedMessageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.pamapam.model.Criterion;
import org.pamapam.model.CriterionAnswer;
import org.pamapam.model.EntityDiscount;
import org.pamapam.model.EntityEvaluation;
import org.pamapam.model.EntityEvaluationCriterion;
import org.pamapam.model.EntityEvaluationCriterionAnswer;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.repository.CriterionRepository;
import org.pamapam.repository.PersonRoleRepository;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.pamapam.services.config.PamapamBackofficeServicesTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { PamapamBackofficeServicesTestConfig.class })
public class SocialEconomyEntityServiceTest {
	
	@MockBean
	private SocialEconomyEntityRepository seeRepository;
	@MockBean
	private CriterionRepository criterionRepository;
	@MockBean
	private PersonRoleRepository personRoleRepository;
	
	@Autowired
	private SocialEconomyEntityService seeService;
	@Autowired
	private LocalizedMessageService messsageService;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
	
	@Test
//	@Ignore
	public void exportToCsvTest() {
		Date currentDate = new Date();
		
		SocialEconomyEntitySpecification seeSpecification = new SocialEconomyEntitySpecification();
		Stream.Builder<SocialEconomyEntity> seeStreamBuilder = Stream.builder();
		SocialEconomyEntity see = new SocialEconomyEntity();
		see.setName("Jamgo");
		see.setFacebook("facebook");
		see.setQuitter("quitter");
		see.setEntityPersonRoles(Collections.emptyList());
		see.setSocialEconomyNetworks(Collections.emptySet());
		see.setOtherSocialEconomyNetworks(Collections.emptySet());
		see.setCollaborationEntities(Collections.emptySet());
		see.setProductTag(Collections.emptySet());
		see.setExternalFilterTags(Collections.emptySet());
		see.setNotes(Collections.emptyList());
		
		EntityDiscount discount = new EntityDiscount();
		discount.setName("Discount name");
		discount.setDescription("Discount description");
		discount.setUrl("www.meloquitandelasmanos.org");
		discount.setBeginDate(currentDate);
		discount.setEndDate(currentDate);
		see.setDiscounts(Collections.singletonList(discount));
		
		Mockito.when(this.seeRepository.countMaxDiscounts()).thenReturn(Integer.valueOf(1));
		Mockito.when(this.seeRepository.streamAll(seeSpecification)).thenReturn(seeStreamBuilder.add(see).build());
		
		// new criteria
		Criterion newCriteria = new Criterion();
		LocalizedString newCriteriaName = new LocalizedString();
		newCriteriaName.add("ca", "New criteria name");
		newCriteria.setName(newCriteriaName);
		CriterionAnswer newCriterionAnswer = new CriterionAnswer();
		newCriterionAnswer.setAnswer(new LocalizedString());
		newCriterionAnswer.setAnswerText("New criterion answer");
		newCriteria.setCriterionAnswers(Collections.singletonList(newCriterionAnswer));
		List<Criterion> newCriteriaList = Collections.singletonList(newCriteria);
		
		EntityEvaluationCriterion evaluationCriterion1 = new EntityEvaluationCriterion();
		evaluationCriterion1.setCriterion(newCriteria);
		evaluationCriterion1.setNotes("Evaluation notes");
		EntityEvaluationCriterionAnswer evaluationCriterionAnswer1 = new EntityEvaluationCriterionAnswer();
		CriterionAnswer criterionAnswer1 = new CriterionAnswer();
		criterionAnswer1.setAnswer(new LocalizedString());
		criterionAnswer1.setAnswerText("New answer");
		evaluationCriterionAnswer1.setCriterion(criterionAnswer1);
		evaluationCriterionAnswer1.setValue(500);
		evaluationCriterion1.setEntityEvaluationCriterionAnswers(Collections.singletonList(evaluationCriterionAnswer1));
		
		// old criteria
		Criterion oldCriteria = new Criterion();
		LocalizedString oldCriteriaName = new LocalizedString();
		oldCriteriaName.add("ca", "Old criteria name");
		oldCriteria.setName(oldCriteriaName);
		CriterionAnswer oldCriterionAnswer = new CriterionAnswer();
		oldCriterionAnswer.setAnswer(new LocalizedString());
		oldCriterionAnswer.setAnswerText("Old criterion answer");
		oldCriteria.setCriterionAnswers(Collections.singletonList(oldCriterionAnswer));
		List<Criterion> oldCriteriaList = Collections.singletonList(oldCriteria);
		
		EntityEvaluation evaluation = new EntityEvaluation();
		EntityEvaluationCriterion evaluationCriterion2 = new EntityEvaluationCriterion();
		evaluationCriterion2.setCriterion(oldCriteria);
		evaluationCriterion2.setLevel(35);
		evaluationCriterion2.setNotes("Old criterion evaluation notes");
		EntityEvaluationCriterionAnswer evaluationCriterionAnswer2 = new EntityEvaluationCriterionAnswer();
		CriterionAnswer criterionAnswer2 = new CriterionAnswer();
		criterionAnswer2.setAnswer(new LocalizedString());
		criterionAnswer2.setAnswerText("Old answer");
		evaluationCriterionAnswer2.setCriterion(criterionAnswer2);
		evaluationCriterionAnswer2.setValue(350);
		evaluationCriterion2.setEntityEvaluationCriterionAnswers(Collections.singletonList(evaluationCriterionAnswer2));
		evaluation.setEntityEvaluationCriterions(Arrays.asList(evaluationCriterion1, evaluationCriterion2));
		see.setEntityEvaluation(evaluation);
		
		Mockito.when(this.criterionRepository.findByOldVersionFalse(new Sort("viewOrder"))).thenReturn(newCriteriaList);
		Mockito.when(this.criterionRepository.findByOldVersionTrue(new Sort("viewOrder"))).thenReturn(oldCriteriaList);
		Mockito.when(this.personRoleRepository.findAll(new Sort("id"))).thenReturn(Collections.EMPTY_LIST);
		
		try {
			String exportedFilePath = this.seeService.exportToCsv(seeSpecification);
			FileReader fr = new FileReader(exportedFilePath);
			BufferedReader br = new BufferedReader(fr);
			String headerLine = br.readLine();
			String[] headerLineParts = headerLine.split(",");
			Assert.assertEquals(48, headerLineParts.length);
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.name"), headerLineParts[1].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.facebook"), headerLineParts[19].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.quitter"), headerLineParts[22].replace("\"", ""));
			Assert.assertEquals("(" + this.messsageService.getMessage("socialEconomyEntity.discounts") + ")" + this.messsageService.getMessage("socialEconomyEntity.discounts.name"), headerLineParts[36].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.discounts.description"), headerLineParts[37].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.discounts.url"), headerLineParts[38].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.discounts.beginDate"), headerLineParts[39].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.discounts.endDate"), headerLineParts[40].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.applyingCriterionCount"), headerLineParts[41].replace("\"", ""));
			Assert.assertEquals(this.messsageService.getMessage("socialEconomyEntity.fulfillmentPercent"), headerLineParts[42].replace("\"", ""));
			Assert.assertEquals(newCriteria.getName().get("ca"), headerLineParts[43].replace("\"", ""));
			Assert.assertEquals(newCriteria.getName().get("ca").concat("_avaluacio"), headerLineParts[44].replace("\"", ""));
			Assert.assertEquals(oldCriteria.getName().get("ca").concat("_old"), headerLineParts[46].replace("\"", ""));
			Assert.assertEquals(oldCriteria.getName().get("ca").concat("_old_avaluacio"), headerLineParts[47].replace("\"", ""));
			
			String entityLine = br.readLine();
			String[] entityLineParts = entityLine.split(",");
			Assert.assertEquals(48, entityLineParts.length);
			Assert.assertEquals(see.getName(), entityLineParts[1].replace("\"", ""));
			Assert.assertEquals(see.getFacebook(), entityLineParts[19].replace("\"", ""));
			Assert.assertEquals(see.getQuitter(), entityLineParts[22].replace("\"", ""));
			Assert.assertEquals(discount.getName(), entityLineParts[36].replace("\"", ""));
			Assert.assertEquals(discount.getDescription(), entityLineParts[37].replace("\"", ""));
			Assert.assertEquals(discount.getUrl(), entityLineParts[38].replace("\"", ""));
			Assert.assertEquals(this.sdf.format(discount.getBeginDate()), entityLineParts[39].replace("\"", ""));
			Assert.assertEquals(this.sdf.format(discount.getEndDate()), entityLineParts[40].replace("\"", ""));
			Assert.assertEquals(String.valueOf(evaluation.getEntityEvaluationCriterions().size()), entityLineParts[41]);
			Assert.assertEquals("100", entityLineParts[42]);
			Assert.assertEquals("5", entityLineParts[43]);
			Assert.assertEquals(evaluationCriterion1.getNotes(), entityLineParts[44].replace("\"", ""));
			Assert.assertEquals("3", entityLineParts[46].replace("\"", "")); // foor rounded value of 350/100
			Assert.assertEquals(evaluationCriterion2.getNotes(), entityLineParts[47].replace("\"", ""));
			
			br.close();
			// TODO try to test it using csvMapper and parsing directly to the SocialEconomyEntity instance
//			final CsvMapper csvMapper = new CsvMapper();
//			final CsvSchema csvSchema = this.seeService.createSchema(Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST, this.seeRepository.countMaxDiscounts());
//	        ObjectReader reader = csvMapper.readerFor(SocialEconomyEntity.class).with(csvSchema);
//	        MappingIterator<Object> seeIt = reader.readValues(new File(exportedFilePath));
//			SocialEconomyEntity parsedSee = seeIt.next();
//	        Object parsedSee = seeIt.next();
			
//	        Assert.assertEquals(see.getName(), parsedSee.getName());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
