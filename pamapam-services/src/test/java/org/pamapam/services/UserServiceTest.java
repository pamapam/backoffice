package org.pamapam.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamapam.services.config.PamapamBackofficeServicesTestConfig;
import org.pamapam.services.dto.PamapamUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { PamapamBackofficeServicesTestConfig.class, UserServiceTest.Config.class })
public class UserServiceTest {

	@Configuration
	static class Config {
		@Bean
		public PamapamUserService pamapamUserService() {
			return new PamapamUserService();
		}
	}

	@Autowired
	private PamapamUserService userService;

	@Test
	@Ignore // FIXME: Disabled until regeneration of obfuscated test data.
	public void testFindOneDto() {
		PamapamUserDto pamapamUserDto = this.userService.findOneDto(900262L, PamapamUserDto.class);
		assertNotNull(pamapamUserDto);
		assertEquals("/image/800262", pamapamUserDto.getPictureUrl());
	}
}
