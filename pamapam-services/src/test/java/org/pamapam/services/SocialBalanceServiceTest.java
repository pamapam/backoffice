package org.pamapam.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamapam.services.config.PamapamBackofficeServicesTestConfig;
import org.pamapam.services.dto.SocialBalanceEntityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { PamapamBackofficeServicesTestConfig.class })
public class SocialBalanceServiceTest {

	@Configuration
	static class Config {
		@Bean
		public SocialBalanceService socialBalanceService() {
			return new SocialBalanceService();
		}
	}

	@Autowired
	private SocialBalanceService socialBalanceService;

	@Test
	public void callPamaPamApiTest() throws IOException {
		final List<SocialBalanceEntityDto> balanceEntityDtoList = this.socialBalanceService.fetchAllSocialBalanceEntities();
		System.out.println(balanceEntityDtoList);
	}
}
