package org.pamapam.web.services.application.docs;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamapam.web.services.application.config.PamapamBackofficeWebServicesTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.SnippetRegistry;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PamapamBackofficeWebServicesTestConfig.class })
@Ignore
public class TerritoryControllerDocumentation {

	@Rule
	public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext context;

//	@Autowired
//	private Filter springSecurityFilterChain;

	private MockMvc mockMvc;
//	private RestDocumentationResultHandler documentHandler;

	@Before
	public void setUp() {
//		this.documentHandler = document("{method-name}",
//			preprocessRequest(prettyPrint()),
//			preprocessResponse(
//				ResponseModifyingPreprocessors.limitJsonArrayLength(this.objectMapper),
//				prettyPrint()));
//		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
//			.apply(documentationConfiguration(this.restDocumentation))
//			.alwaysDo(this.documentHandler)
//			.build();
		this.mockMvc = MockMvcBuilders
			.webAppContextSetup(this.context)
			//			.addFilters(this.springSecurityFilterChain)
			.alwaysDo(JacksonResultHandlers.prepareJackson(this.objectMapper))
			.alwaysDo(MockMvcRestDocumentation.document("{class-name}/{method-name}",
				Preprocessors.preprocessRequest(),
				Preprocessors.preprocessResponse(
					ResponseModifyingPreprocessors.replaceBinaryContent(),
					ResponseModifyingPreprocessors.limitJsonArrayLength(this.objectMapper),
					Preprocessors.prettyPrint())))
			.apply(MockMvcRestDocumentation.documentationConfiguration(this.restDocumentation)
				.uris()
				.withScheme("http")
				.withHost("localhost")
				.withPort(8080)
				.and().snippets()
				.withDefaults(CliDocumentation.curlRequest(),
					HttpDocumentation.httpRequest(),
					HttpDocumentation.httpResponse(),
					AutoDocumentation.requestFields(),
					AutoDocumentation.responseFields(),
					AutoDocumentation.pathParameters(),
					AutoDocumentation.requestParameters(),
					AutoDocumentation.description(),
					AutoDocumentation.methodAndPath(),
					//					AutoDocumentation.section(),
					AutoDocumentation.sectionBuilder()
						.snippetNames(
							SnippetRegistry.PATH_PARAMETERS)
						.skipEmpty(true)
						.build()))
			.build();
	}

	@Test
	public void provincesAll() throws Exception {
		this.mockMvc.perform(get("/provinces").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void provincesOne() throws Exception {
		this.mockMvc.perform(get("/provinces/{id}", 1).accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void regionsAll() throws Exception {
		this.mockMvc.perform(get("/regions").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void regionsOne() throws Exception {
		this.mockMvc.perform(get("/regions/1").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void townsAll() throws Exception {
		this.mockMvc.perform(get("/towns").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void townsOne() throws Exception {
		this.mockMvc.perform(get("/towns/1").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void districtsAll() throws Exception {
		this.mockMvc.perform(get("/districts").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void districtsOne() throws Exception {
		this.mockMvc.perform(get("/districts/1").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void neighborhoodsAll() throws Exception {
		this.mockMvc.perform(get("/neighborhoods").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	@Test
	public void neighborhoodsOne() throws Exception {
		this.mockMvc.perform(get("/neighborhoods/1").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

//	@Test
//	public void creatingANote() throws JsonProcessingException, Exception {
//		String noteLocation = this.createNote();
//		MvcResult note = this.getNote(noteLocation);
//
//		String tagLocation = this.createTag();
//		this.getTag(tagLocation);
//
//		String taggedNoteLocation = this.createTaggedNote(tagLocation);
//		MvcResult taggedNote = this.getNote(taggedNoteLocation);
//		this.getTags(this.getLink(taggedNote, "tags"));
//
//		this.tagExistingNote(noteLocation, tagLocation);
//		this.getTags(this.getLink(note, "tags"));
//	}
//
//	String createNote() throws Exception {
//		Map<String, String> note = new HashMap<String, String>();
//		note.put("title", "Note creation with cURL");
//		note.put("body", "An example of how to create a note using cURL");
//
//		String noteLocation = this.mockMvc
//			.perform(
//				post("/notes").contentType(MediaTypes.HAL_JSON).content(
//					this.objectMapper.writeValueAsString(note)))
//			.andExpect(status().isCreated())
//			.andExpect(header().string("Location", notNullValue()))
//			.andReturn().getResponse().getHeader("Location");
//		return noteLocation;
//	}
//
//	MvcResult getNote(String noteLocation) throws Exception {
//		return this.mockMvc.perform(get(noteLocation))
//			.andExpect(status().isOk())
//			.andExpect(jsonPath("title", is(notNullValue())))
//			.andExpect(jsonPath("body", is(notNullValue())))
//			.andExpect(jsonPath("_links.tags", is(notNullValue())))
//			.andReturn();
//	}
//
//	String createTag() throws Exception, JsonProcessingException {
//		Map<String, String> tag = new HashMap<String, String>();
//		tag.put("name", "getting-started");
//
//		String tagLocation = this.mockMvc
//			.perform(
//				post("/tags").contentType(MediaTypes.HAL_JSON).content(
//					this.objectMapper.writeValueAsString(tag)))
//			.andExpect(status().isCreated())
//			.andExpect(header().string("Location", notNullValue()))
//			.andReturn().getResponse().getHeader("Location");
//		return tagLocation;
//	}
//
//	void getTag(String tagLocation) throws Exception {
//		this.mockMvc.perform(get(tagLocation)).andExpect(status().isOk())
//			.andExpect(jsonPath("name", is(notNullValue())))
//			.andExpect(jsonPath("_links.notes", is(notNullValue())));
//	}
//
//	String createTaggedNote(String tag) throws Exception {
//		Map<String, Object> note = new HashMap<String, Object>();
//		note.put("title", "Tagged note creation with cURL");
//		note.put("body", "An example of how to create a tagged note using cURL");
//		note.put("tags", Arrays.asList(tag));
//
//		String noteLocation = this.mockMvc
//			.perform(
//				post("/notes").contentType(MediaTypes.HAL_JSON).content(
//					this.objectMapper.writeValueAsString(note)))
//			.andExpect(status().isCreated())
//			.andExpect(header().string("Location", notNullValue()))
//			.andReturn().getResponse().getHeader("Location");
//		return noteLocation;
//	}
//
//	void getTags(String noteTagsLocation) throws Exception {
//		this.mockMvc.perform(get(noteTagsLocation))
//			.andExpect(status().isOk())
//			.andExpect(jsonPath("_embedded.tags", hasSize(1)));
//	}
//
//	void tagExistingNote(String noteLocation, String tagLocation) throws Exception {
//		Map<String, Object> update = new HashMap<String, Object>();
//		update.put("tags", Arrays.asList(tagLocation));
//
//		this.mockMvc.perform(
//			patch(noteLocation).contentType(MediaTypes.HAL_JSON).content(
//				this.objectMapper.writeValueAsString(update)))
//			.andExpect(status().isNoContent());
//	}
//
//	MvcResult getTaggedExistingNote(String noteLocation) throws Exception {
//		return this.mockMvc.perform(get(noteLocation))
//			.andExpect(status().isOk())
//			.andReturn();
//	}
//
//	void getTagsForExistingNote(String noteTagsLocation) throws Exception {
//		this.mockMvc.perform(get(noteTagsLocation))
//			.andExpect(status().isOk())
//			.andExpect(jsonPath("_embedded.tags", hasSize(1)));
//	}
//
//	private String getLink(MvcResult result, String rel)
//		throws UnsupportedEncodingException {
//		return JsonPath.parse(result.getResponse().getContentAsString()).read(
//			"_links." + rel + ".href");
//	}

}
