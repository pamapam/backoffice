package org.pamapam.web.services.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;

@Ignore // FIXME: problem with pamapam resources in tests classpath
public class SocialEconomyEntityControllerTest extends ServiceControllerTest {

	@Test
	public void testGetEntities_All() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/entities").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode pageDtoNode = responseDtoNode.get("response");

		assertEquals(2, pageDtoNode.get("totalElements").asLong());
		// ...	Default page size is 20
		assertEquals(2, pageDtoNode.get("content").size());
	}

	@Test
	public void testGetEntities_AllPaginated() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/entities?page=0&size=12").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode pageDtoNode = responseDtoNode.get("response");

		assertEquals(2, pageDtoNode.get("totalElements").asLong());
		assertEquals(2, pageDtoNode.get("content").size());
	}

	@Test
	public void testGetEntities_One() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/entities/1034").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode dataNode = responseDtoNode.get("response");

		assertEquals("F66705013", dataNode.get("nif").asText());
	}

	@Test
	public void testGetRelatedSocialEconomyEntitiesBySector() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/relatedEntitiesBySector/1034/3").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode dataNode = responseDtoNode.get("response");

		assertEquals(3, dataNode.size());
	}

}
