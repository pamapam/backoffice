package org.pamapam.web.services.application;

import static org.junit.Assert.assertEquals;

import java.net.HttpURLConnection;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.pamapam.web.services.application.config.PamapamBackofficeWebServicesTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JsonNode;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PamapamBackofficeWebServicesTestConfig.class })
@Ignore // FIXME: problem with pamapam resources in tests classpath
public abstract class ServiceControllerTest {

	@Autowired
	protected WebApplicationContext context;

	protected MockMvc mockMvc;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders
			.webAppContextSetup(this.context)
			.build();
	}

	protected void assertStatus(JsonNode responseDtoNode) {
		this.assertStatus(responseDtoNode, HttpURLConnection.HTTP_OK, null);
	}

	protected void assertStatus(JsonNode responseDtoNode, Integer expectedStatusId, String expectedMessage) {
		assertEquals((int) expectedStatusId, responseDtoNode.get("status").asInt());
		if (expectedMessage != null) {
			assertEquals(expectedMessage, responseDtoNode.get("message").asText());
		}
	}
}
