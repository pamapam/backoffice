package org.pamapam.web.services.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pamapam.web.services.application.config.PamapamBackofficeWebServicesTestConfig;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PamapamBackofficeWebServicesTestConfig.class })
@Ignore // FIXME: problem with pamapam resources in tests classpath
public class TerritoryControllerTest extends ServiceControllerTest {

	@Test
	public void testGetProvinces_All() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/provinces").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode pageDtoNode = responseDtoNode.get("response");

		assertEquals(4, pageDtoNode.get("totalElements").asLong());
		assertEquals(4, pageDtoNode.get("content").size());
	}

	@Test
	public void testGetProvinces_One() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/provinces/1").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode dataNode = responseDtoNode.get("response");

		assertEquals("Barcelona", dataNode.get("name").asText());
		assertEquals(13, dataNode.get("regions").size());
	}

	@Test
	public void testGetRegions_All() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/regions").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode pageDtoNode = responseDtoNode.get("response");

		assertEquals(46, pageDtoNode.get("totalElements").asLong());
		// ...	Default page size is 20
		assertEquals(20, pageDtoNode.get("content").size());
	}

	@Test
	public void testGetRegions_One() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/regions/5").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode dataNode = responseDtoNode.get("response");

		assertEquals("Barcelonès", dataNode.get("name").asText());
		assertEquals(5, dataNode.get("towns").size());
	}

	@Test
	public void testGetTowns_All() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/towns").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode pageDtoNode = responseDtoNode.get("response");

		assertEquals(948, pageDtoNode.get("totalElements").asLong());
		// ...	Default page size is 20
		assertEquals(20, pageDtoNode.get("content").size());
	}

	@Test
	public void testGetTowns_One() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/towns/122").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode dataNode = responseDtoNode.get("response");

		assertEquals("Barcelona", dataNode.get("name").asText());
		assertEquals(10, dataNode.get("districts").size());
	}

	@Test
	public void testGetDistricts_All() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/districts").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode pageDtoNode = responseDtoNode.get("response");

		assertEquals(10, pageDtoNode.get("totalElements").asLong());
		assertEquals(10, pageDtoNode.get("content").size());
	}

	@Test
	public void testGetDistricts_One() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/districts/6").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode dataNode = responseDtoNode.get("response");

		assertEquals("Gràcia", dataNode.get("name").asText());
		assertEquals(5, dataNode.get("neighborhoods").size());
	}

	@Test
	public void testGetNeighborhoods_All() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/neighborhoods").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode pageDtoNode = responseDtoNode.get("response");

		assertEquals(75, pageDtoNode.get("totalElements").asLong());
		// ...	Default page size is 20
		assertEquals(20, pageDtoNode.get("content").size());
	}

	@Test
	public void testGetNeighborhoods_One() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/neighborhoods/70").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			.andReturn();

		assertNotNull(result);
		MockHttpServletResponse mockResponse = result.getResponse();

		assertTrue(!mockResponse.getContentAsString().isEmpty());
		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(mockResponse.getContentAsString());
		JsonNode responseDtoNode = jsonParser.readValueAsTree();

		this.assertStatus(responseDtoNode);
		JsonNode dataNode = responseDtoNode.get("response");

		assertEquals("el Poblenou", dataNode.get("name").asText());
	}

}
