package org.pamapam.web.services.application.config;

import java.util.HashMap;
import java.util.Map;

import org.pamapam.services.config.PamapamBackofficeServicesTestConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@Import({ PamapamBackofficeServicesTestConfig.class })
@ComponentScan(basePackageClasses = {
	org.pamapam.web.services.PackageMarker.class
})
@EnableWebMvc
@EnableSpringDataWebSupport
public class PamapamBackofficeWebServicesTestConfig extends WebMvcConfigurerAdapter {

	private static final Map<String, String> JSON_RESPONSES;
	static {
		JSON_RESPONSES = new HashMap<String, String>();
		JSON_RESPONSES.put("http://www.dev.roca.es/tienda/apirest/links/", "ecommerce/links.json");
		JSON_RESPONSES.put("http://www.dev.roca.es/tienda/apirest/session/", "ecommerce/session.json");
		JSON_RESPONSES.put("http://www.dev.roca.es/tienda/apirest/productlist/store/es_es/filter.distributor.code/4252/detail/sku_only", "ecommerce/products.json");
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	@Bean
	public JavaMailSender javaMailSender() {
		return new JavaMailSenderImpl();
	}

	@Bean
	public freemarker.template.Configuration configuration() {
		return new freemarker.template.Configuration(freemarker.template.Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
	}

	@Bean
	public AsyncTaskExecutor asyncTaskExecutor() {
		return new SimpleAsyncTaskExecutor();
	}

//	@Bean
//	public Validator validatorFactory() {
//		return new LocalValidatorFactoryBean();
//	}
}
