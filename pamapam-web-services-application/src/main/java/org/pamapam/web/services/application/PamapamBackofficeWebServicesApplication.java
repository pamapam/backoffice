package org.pamapam.web.services.application;

import org.pamapam.services.config.PamapamBackofficeServicesConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@Import(value = { PamapamBackofficeServicesConfig.class })
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@ComponentScan(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	org.jamgo.services.PackageMarker.class,
	org.pamapam.repository.PackageMarker.class,
	org.pamapam.services.PackageMarker.class,
	org.pamapam.web.services.PackageMarker.class,
	org.pamapam.web.services.application.PamapamBackofficeWebServicesApplication.class
})
@EntityScan(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	org.pamapam.model.PackageMarker.class,
	org.pamapam.web.services.application.PamapamBackofficeWebServicesApplication.class
})
@EnableJpaRepositories(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	org.pamapam.repository.PackageMarker.class
})
public class PamapamBackofficeWebServicesApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(PamapamBackofficeWebServicesApplication.class, args);
	}

}
