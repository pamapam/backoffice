package org.pamapam.web.services.application.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.jta.JtaTransactionManager;

@Configuration
@EnableAutoConfiguration
@org.springframework.context.annotation.PropertySource(name = "jpaVendorProperties", value = "classpath:jpa-vendor.properties")
public class PamapamEclipselinkJpaConfiguration extends JpaBaseConfiguration {

	@Autowired
	private Environment env;

	protected PamapamEclipselinkJpaConfiguration(DataSource dataSource, JpaProperties properties, ObjectProvider<JtaTransactionManager> jtaTransactionManager, ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
		super(dataSource, properties, jtaTransactionManager, transactionManagerCustomizers);
	}

	@Override
	protected AbstractJpaVendorAdapter createJpaVendorAdapter() {
		return new EclipseLinkJpaVendorAdapter();
	}

	@Override
	protected Map<String, Object> getVendorProperties() {
		MapPropertySource jpaVendorProperties = null;
		for (PropertySource<?> eachPropertySource : ((AbstractEnvironment) this.env).getPropertySources()) {
			if ("jpaVendorProperties".equals(eachPropertySource.getName())) {
				jpaVendorProperties = (MapPropertySource) eachPropertySource;
				break;
			}
		}

		// configuration is in persistence.xml, but here it is necessary as well
		Map<String, Object> map = new HashMap<String, Object>();
		if (jpaVendorProperties != null) {
			for (String eachPropertyKey : jpaVendorProperties.getPropertyNames()) {
				map.put(eachPropertyKey, jpaVendorProperties.getProperty(eachPropertyKey));
			}
		} else {
			map.put("eclipselink.weaving", "static");
			map.put("eclipselink.weaving.lazy", "true");
			map.put("eclipselink.weaving.internal", "true");
			map.put("eclipselink.logging.level", "INFO");
			map.put("eclipselink.logging.parameters", "true");
		}

		return map;
	}

//	@ConditionalOnProperty(name = "spring.datasource.driver-class-name", havingValue = "org.h2.Driver")
//	@Bean(initMethod = "start", destroyMethod = "stop")
//	public Server h2Server() throws SQLException {
//		return Server.createTcpServer("-tcp", "-tcpAllowOthers");
//	}

}
