<!--ts-->
   * [Requisitos de memoria.](#requisitos-de-memoria)
   * [Instalación](#instalación)
      * [Linux (Docker)](#linux-docker)
         * [JDK](#jdk)
         * [Clone](#clone)
         * [Build](#build)
         * [Runtime install](#runtime-install)
         * [Database container](#database-container)
         * [Application container](#application-container)
         * [Start de la aplicación.](#start-de-la-aplicación)
         * [Entrar en la aplicación.](#entrar-en-la-aplicación)
         * [Configuración del fronted local.](#configuración-del-fronted-local)
      * [OS/X (Docker)](#osx-docker)
         * [JDK](#jdk-1)
         * [Clone](#clone-1)
         * [Build](#build-1)
         * [Runtime install](#runtime-install-1)
         * [Database container](#database-container-1)
         * [Application container](#application-container-1)
         * [Start de la aplicación.](#start-de-la-aplicación-1)
         * [Entrar en la aplicación.](#entrar-en-la-aplicación-1)
         * [Configuración del fronted local.](#configuración-del-fronted-local-1)

<!-- Added by: martin, at: 2019-04-03T20:59+02:00 -->

<!--te-->

# Requisitos de memoria.

* Frontend + fontend db ~ __1GB Ram__
* Backoffice + backoffice db __~3GB Ram__

Para tener instaladas ambas cosas y las mínimas herramientos de desarrollo __hay que tener entonces al menos entre 6GB-8GB de Ram__ en nuestro ordenador.

Para mirar cuánta Ram tenemos:

* __Linux__: Desde una terminal: `free -g`. Columna _Total_.
* __OS/X__: Apple menu -> About This Mac -> Overview (Memory).
* __Windows 10__: Windows menu -> Windows System -> Task Manager -> Performance -> Memory

# Instalación

El documento está escrito asumiendo ciertos directorios de instalación por defecto:

* __~/pamapam__ -> Directorio raiz para el repositorio git.
* __~/pamapam/docker-data__ -> Directorio raiz donde se crearán los subdirectorios correspondientes a los volúmenes mapeados en los diferentes contenedores docker.

```bash
mkdir ~/pamapam
```

## Linux (Docker)

### JDK

El backoffice es una aplicación Java que consta de varios módulos. Para poder hacer el build de la aplicación necesitamos tener un JDK 8 instalado. Aunque es posible que funcione todo correctamente con versiones del Java > 8, por ahora solo se ha probado con java 8.

```bash
apt-get install openjdk-8-jdk
```

### Clone

```bash
cd ~/pamapam
git clone https://gitlab.com/pamapam/backoffice.git
```

### Build

El backoffice utiliza [Gradle](https://docs.gradle.org/current/userguide/userguide.html) como build tool. Una vez clonado el repositorio, hay que hacer un build inicial para obtener los ficheros runtime necesarios para ejecutar la aplicación.

```bash
cd ~/pamapam/backoffice
./gradlew build
```

### Runtime install

Una vez completado el build hay que instalar los ficheros de runtime en el directorio que será utilizado como volumen por el contenedor docker correspondiente.
El runtime del backoffice se compone de dos ficheros:

* __pamapam-backoffice.war__ -> Todo el código se empaqueta en este único fichero.
* __resources.zip__ -> Fichero con recursos (configuración, traducciones, imágenes estáticas). Estos recursos se empaquetan por fuera del WAR porque muchos de ellos serán diferentes según el entorno en donde se haga la instalación (development, quality, production).

```bash
mkdir -p ~/pamapam/docker-data/back
cp ~/pamapam/backoffice/pamapam-backoffice-application/build/libs/pamapam-backoffice-application-master-SNAPSHOT.war ~/pamapam/docker-data/back/pamapam-backoffice.war
cp ~/pamapam/backoffice/pamapam-resources/build/development.zip ~/pamapam/docker-data/back/resources.zip
```

### Database container

Creación del contenedor para la base de datos del Backoffice. Los usuarios y passwords utilizados en estas instrucciones son evidentemente débiles, pensados solamente para un entorno de desarrollo local.

```bash
docker run --name pamapam-back-db \
 -v ~/pamapam/docker-data/back-db/:/var/lib/mysql/ \
 -e MYSQL_ROOT_PASSWORD=jamgo \
 -d mariadb:10
```

Para que todos los contenedores implicados en la web y aplicación Pam a Pam sean visibles entre si, tienen que estar conectados a una docker network común.

```bash
docker network create pamapam-net
docker network connect pamapam-net --alias mariadb pamapam-back-db
```

A continuación hay que crear el usuario y la base de datos, en donde se importará luego el dump con los datos iniciales del entorno de desarrollo. Este dump inicial tiene datos cargados similares a un entorno de producción pero ofuscando la información sensible. 

```bash
docker exec pamapam-back-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create user jamgo@'\''%'\'' identified by '\''jamgo'\'';"'
docker exec pamapam-back-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create database pamapam_backoffice default character set utf8 collate utf8_general_ci;"'
docker exec pamapam-back-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "grant all on pamapam_backoffice.* to jamgo@'\''%'\'' identified by '\''jamgo'\'';"'
```

Import de los datos iniciales.

```bash
curl https://gitlab.com/jamgo/backoffice-dev/raw/master/resources/pamapam_backoffice_dev.sql > /tmp/pamapam_backoffice_dev.sql
docker exec -i pamapam-back-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" pamapam_backoffice ' < /tmp/pamapam_backoffice_dev.sql
```

### Application container

En la creación del contenedor para la aplicación se mapea el puerto 8080 del ordenador con el 8080 del contenedor (_-p 8080:8080_). 

Si ya tenemos alguna otra aplicación utilizando este mismo puerto, no podremos hacer luego el start porque habrá colisión. Si no es posible detener la otra aplicación tendremos que mapear un puerto diferente: _-p XXXX:8080_ (el puerto interno del contenedor será siempre el 8080).

```bash
docker create --name pamapam-back \
 -p 8080:8080 \
 -v ~/pamapam/docker-data/back:/mnt \
 --workdir /mnt \
 -v /etc/localtime:/etc/localtime:ro \
 -v /etc/timezone:/etc/timezone:ro \
 adoptopenjdk/openjdk8:alpine-slim sh -c "java -cp pamapam-backoffice.war:resources.zip org.springframework.boot.loader.WarLauncher"
```

Hay que conectar también este contenedor a la docker network creada anteriormente.

```bash
docker network connect pamapam-net pamapam-back
```

### Start de la aplicación.

Po último solo queda hacer el sart del contenedor de la aplicación y probar que todo funcione correctamente.

```bash
docker start pamapam-back
```

El start de la aplicación puede tardar un poco, la mejor manera de asegurarse que el start ha finalizado es mirar los logs del container:

```bash
docker logs -f pamapam-back
```
... hasta ver unas lineas similares a estas:

```
2019-04-03 18:51:18.550  INFO 14671 --- [           main] b.c.e.u.UndertowEmbeddedServletContainer : Undertow started on port(s) 8080 (http)
2019-04-03 18:51:18.554  INFO 14671 --- [           main] o.p.b.PamapamBackofficeApplication       : Started PamapamBackofficeApplication in 10.539 seconds (JVM running for 10.77)
```

__Ctrl+C para salir de la visualización de los logs.__ Generalmente es bueno tener una pestaña o ventana de terminal siempre mostrando los logs y seguir trabajando en otra.

### Entrar en la aplicación.

http://localhost:8080

* user: __admin__
* password: __pamapam__

Todas las usuarias importadas en la base de datos de desarrollo tienen como password __pamapam__.

### Configuración del fronted local.

La instalación por defecto del **frontend** crea una configuración para trabajar con el backoffice de qa. Si queremos conectar nuestro fontend local con el backoffice que acabamos de instalar, hay que modificar la configuración del contenedor Nginx.

Estamos asumiendo también que los directorios de los volúmenes correspondientes a los contenedores del frontend están en *~/pamapam/docker-data*.

```bash
sed -e "s/qa\.pamapam\.org/pamapam-back:8080/g" -i ~/pamapam/docker-data/nginx/default.conf
sed -e "s/pamapam-back:8080\/services/pamapam-back:8080\/api/g" -i "" ~/pamapam/docker-data/nginx/default.conf
```

Y tenemos que conectar también los contenedores del fronted a la docker network creada.

```bash
docker network connect pamapam-net --alias pamapam.local pamapam
docker network connect pamapam-net pamapam-web
```

Por último hay que reiniciar el contenedor del nginx para activar la nueva redirección al backoffice local que hemos configurado:

```bash
docker restart pamapam
```

## OS/X (Docker)

### JDK

El backoffice es una aplicación Java que consta de varios módulos. Para poder hacer el build de la aplicación necesitamos tener un JDK 8 instalado. Aunque es posible que funcione todo correctamente con versiones del Java > 8, por ahora solo se ha probado con java 8.

```bash
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk8
```

### Clone

```bash
cd ~/pamapam
git clone https://gitlab.com/pamapam/backoffice.git
```

### Build

El backoffice utiliza [Gradle](https://docs.gradle.org/current/userguide/userguide.html) como build tool. Una vez clonado el repositorio, hay que hacer un build inicial para obtener los ficheros runtime necesarios para ejecutar la aplicación.

```bash
cd ~/pamapam/backoffice
./gradlew build
```

### Runtime install

Una vez completado el build hay que instalar los ficheros de runtime en el directorio que será utilizado como volumen por el contenedor docker correspondiente.
El runtime del backoffice se compone de dos ficheros:

* __pamapam-backoffice.war__ -> Todo el código se empaqueta en este único fichero.
* __resources.zip__ -> Fichero con recursos (configuración, traducciones, imágenes estáticas). Estos recursos se empaquetan por fuera del WAR porque muchos de ellos serán diferentes según el entorno en donde se haga la instalación (development, quality, production).

```bash
mkdir -p ~/pamapam/docker-data/back
cp ~/pamapam/backoffice/pamapam-backoffice-application/build/libs/pamapam-backoffice-application-master-SNAPSHOT.war ~/pamapam/docker-data/back/pamapam-backoffice.war
cp ~/pamapam/backoffice/pamapam-resources/build/development.zip ~/pamapam/docker-data/back/resources.zip
```

### Database container

Creación del contenedor para la base de datos del Backoffice. Los usuarios y passwords utilizados en estas instrucciones son evidentemente débiles, pensados solamente para un entorno de desarrollo local.

```bash
docker run --name pamapam-back-db \
 -v ~/pamapam/docker-data/back-db/:/var/lib/mysql/ \
 -e MYSQL_ROOT_PASSWORD=jamgo \
 -d mariadb:10
```

Para que todos los contenedores implicados en la web y aplicación Pam a Pam sean visibles entre si, tienen que estar conectados a una docker network común.

```bash
docker network create pamapam-net
docker network connect pamapam-net --alias mariadb pamapam-back-db
```

A continuación hay que crear el usuario y la base de datos, en donde se importará luego el dump con los datos iniciales del entorno de desarrollo. Este dump inicial tiene datos cargados similares a un entorno de producción pero ofuscando la información sensible. 

```bash
docker exec pamapam-back-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create user jamgo@'\''%'\'' identified by '\''jamgo'\'';"'
docker exec pamapam-back-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create database pamapam_backoffice default character set utf8 collate utf8_general_ci;"'
docker exec pamapam-back-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "grant all on pamapam_backoffice.* to jamgo@'\''%'\'' identified by '\''jamgo'\'';"'
```

Import de los datos iniciales.

```bash
curl https://gitlab.com/jamgo/backoffice-dev/raw/master/resources/pamapam_backoffice_dev.sql > /tmp/pamapam_backoffice_dev.sql
docker exec -i pamapam-back-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" pamapam_backoffice ' < /tmp/pamapam_backoffice_dev.sql
```

### Application container

En la creación del contenedor para la aplicación se mapea el puerto 8080 del ordenador con el 8080 del contenedor (_-p 8080:8080_). 

Si ya tenemos alguna otra aplicación utilizando este mismo puerto, no podremos hacer luego el start porque habrá colisión. Si no es posible detener la otra aplicación tendremos que mapear un puerto diferente: _-p XXXX:8080_ (el puerto interno del contenedor será siempre el 8080).

```bash
docker create --name pamapam-back \
 -p 8080:8080 \
 -v ~/pamapam/docker-data/back:/mnt \
 --workdir /mnt \
 adoptopenjdk/openjdk8:alpine-slim sh -c "java -cp pamapam-backoffice.war:resources.zip org.springframework.boot.loader.WarLauncher"
```

Hay que conectar también este contenedor a la docker network creada anteriormente.

```bash
docker network connect pamapam-net pamapam-back
```

### Start de la aplicación.

Po último solo queda hacer el sart del contenedor de la aplicación y probar que todo funcione correctamente.

```bash
docker start pamapam-back
```

El start de la aplicación puede tardar un poco, la mejor manera de asegurarse que el start ha finalizado es mirar los logs del container:

```bash
docker logs -f pamapam-back
```
... hasta ver unas lineas similares a estas:

```
2019-04-03 18:51:18.550  INFO 14671 --- [           main] b.c.e.u.UndertowEmbeddedServletContainer : Undertow started on port(s) 8080 (http)
2019-04-03 18:51:18.554  INFO 14671 --- [           main] o.p.b.PamapamBackofficeApplication       : Started PamapamBackofficeApplication in 10.539 seconds (JVM running for 10.77)
```

__Ctrl+C para salir de la visualización de los logs.__ Generalmente es bueno tener una pestaña o ventana de terminal siempre mostrando los logs y seguir trabajando en otra.

### Entrar en la aplicación.

http://localhost:8080

* user: __admin__
* password: __pamapam__

Todas las usuarias importadas en la base de datos de desarrollo tienen como password __pamapam__.

### Configuración del fronted local.

La instalación por defecto del **frontend** crea una configuración para trabajar con el backoffice de qa. Si queremos conectar nuestro fontend local con el backoffice que acabamos de instalar, hay que modificar la configuración del contenedor Nginx.

Estamos asumiendo también que los directorios de los volúmenes correspondientes a los contenedores del frontend están en *~/pamapam/docker-data*.

```bash
sed -e "s/qa\.pamapam\.org/pamapam-back:8080/g" -i "" ~/pamapam/docker-data/nginx/default.conf
sed -e "s/pamapam-back:8080\/services/pamapam-back:8080\/api/g" -i "" ~/pamapam/docker-data/nginx/default.conf
```

Y tenemos que conectar también los contenedores del fronted a la docker network creada.

```bash
docker network connect pamapam-net --alias pamapam.local pamapam
docker network connect pamapam-net pamapam-web
```

Por último hay que reiniciar el contenedor del nginx para activar la nueva redirección al backoffice local que hemos configurado:

```bash
docker restart pamapam
```
