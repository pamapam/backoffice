package org.pamapam.backoffice.api.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.stream.Collectors;

import org.jamgo.model.entity.Role;
import org.jamgo.web.services.controller.ServiceController;
import org.pamapam.model.PamapamUser;
import org.pamapam.repository.PamapamUserRepository;
import org.pamapam.services.dto.PamapamUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Servlet Controller for SearchCategories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 *
 */
@Controller
public class UserController extends ServiceController {

	@Autowired
	private PamapamUserRepository userRepository;

	@RequestMapping(value = "/api/user", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(PamapamUserDto.class)
	protected PamapamUserDto getCurrentUser(Principal principal) {

		PamapamUserDto pamapamUserDto = new PamapamUserDto();
		if (principal != null) {
			PamapamUser user = this.userRepository.findByUsername(principal.getName());
			// ...	FIXME.
			pamapamUserDto.setId(user.getId());
			pamapamUserDto.setUsername(user.getUsername());
			pamapamUserDto.setName(user.getName());
			pamapamUserDto.setSurname(user.getSurname());
			pamapamUserDto.setRoleNames(user.getRoles().stream().map(Role::getRolename).collect(Collectors.toList()));
		}

		return pamapamUserDto;
	}

}
