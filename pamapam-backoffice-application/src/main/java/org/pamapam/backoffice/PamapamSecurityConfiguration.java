package org.pamapam.backoffice;

import javax.sql.DataSource;

import org.jamgo.model.util.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy;
import org.vaadin.spring.annotation.EnableVaadinExtensions;
import org.vaadin.spring.http.HttpService;
import org.vaadin.spring.security.annotation.EnableVaadinSharedSecurity;
import org.vaadin.spring.security.config.VaadinSharedSecurityConfiguration;
import org.vaadin.spring.security.shared.VaadinAuthenticationSuccessHandler;
import org.vaadin.spring.security.web.VaadinRedirectStrategy;

//@EnableWebSecurity(debug = true)
@EnableWebSecurity()
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
@EnableVaadinSharedSecurity
@EnableVaadinExtensions
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class PamapamSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private ClientDetailsService clientDetailsService;

	@Autowired
	protected DataSource dataSource;

	@Override
	@Bean
	public JdbcUserDetailsManager userDetailsService() {
		final JdbcUserDetailsManager service = new JdbcUserDetailsManager();
		service.setDataSource(this.dataSource);
		service.setUsersByUsernameQuery("select username,password,enabled from user where username=?");
		service.setRolePrefix("");
		return service;
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	// @formatter:off

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
			auth.jdbcAuthentication().dataSource(this.dataSource)
				.rolePrefix("")
			 	.passwordEncoder(PasswordUtils.getEncoder())
			 	.usersByUsernameQuery("select username,password,enabled from user where username=?")
			 	.authoritiesByUsernameQuery("select u.username,r.rolename "
			 		+ "from user_role ur "
			 		+ "join role r on ur.id_role = r.id "
			 		+ "join user u on ur.id_user = u.id "
			 		+ "where u.username =?");
	}

	// @formatter:off

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http
			.cors()
				.and()
			.authorizeRequests()
				.antMatchers("/api/**").permitAll()
				.antMatchers("/ui/login/**").permitAll()
				.antMatchers("/ui/UIDL/**").permitAll()
				.antMatchers("/ui/HEARTBEAT/**").permitAll()
				.antMatchers("/ui/APP/**").permitAll()
				.antMatchers("/VAADIN/**").permitAll()
				.antMatchers("/oauth/authorize").permitAll()
				.antMatchers("/oauth/token").permitAll()
				.antMatchers("/ui/fesc/registration/public").permitAll()
				.anyRequest().authenticated()
				.and()
			.csrf().disable()
			.httpBasic().disable()
			.exceptionHandling()
				.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/ui/login"))
				.and()
			.headers()
				.frameOptions().disable()
				.and()
			.sessionManagement()
				.sessionAuthenticationStrategy(this.sessionAuthenticationStrategy());
	}

	// @formatter:on
	@Override
	public void configure(final WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/css/*"); // Static resources are ignored
	}

	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}

	@Bean
	@Autowired
	public TokenStoreUserApprovalHandler userApprovalHandler(final TokenStore tokenStore) {
		final TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
		handler.setTokenStore(tokenStore);
		handler.setRequestFactory(new DefaultOAuth2RequestFactory(this.clientDetailsService));
		handler.setClientDetailsService(this.clientDetailsService);
		return handler;
	}

	@Bean
	@Autowired
	public ApprovalStore approvalStore(final TokenStore tokenStore) throws Exception {
		final TokenApprovalStore store = new TokenApprovalStore();
		store.setTokenStore(tokenStore);
		return store;
	}

	@Bean
	public SessionAuthenticationStrategy sessionAuthenticationStrategy() {
		return new SessionFixationProtectionStrategy();
	}

	@Bean(name = VaadinSharedSecurityConfiguration.VAADIN_AUTHENTICATION_SUCCESS_HANDLER_BEAN)
	VaadinAuthenticationSuccessHandler vaadinAuthenticationSuccessHandler(
		final HttpService httpService,
		final VaadinRedirectStrategy vaadinRedirectStrategy) {
		return new PamapamAuthenticationSuccessHandler(httpService, vaadinRedirectStrategy, "/ui");
//		return new SavedRequestAwareVaadinAuthenticationSuccessHandler(httpService, vaadinRedirectStrategy, "/ui");
	}

}
