package org.pamapam.backoffice;

import org.jamgo.layout.details.RoleDetailsLayout;
import org.jamgo.model.entity.*;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.CustomLayoutDef;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.CrudTableLayout;
import org.jamgo.ui.layout.details.LanguageDetailsLayout;
import org.jamgo.ui.layout.details.LocalizedMessageDetailsLayout;
import org.pamapam.backoffice.layout.DashboardLayout;
import org.pamapam.backoffice.layout.MyProfileLayout;
import org.pamapam.backoffice.layout.details.*;
import org.pamapam.backoffice.layout.details.see.SocialEconomyEntityDetailsLayout;
import org.pamapam.backoffice.layout.details.see.SocialEconomyEntityTableLayout;
import org.pamapam.backoffice.layout.ie.NewsletterRegistrationExportLayout;
import org.pamapam.backoffice.layout.ie.SocialEconomyEntityImportLayout;
import org.pamapam.backoffice.layout.search.PamapamUserSearchLayout;
import org.pamapam.backoffice.layout.search.SocialEconomyEntitySearchLayout;
import org.pamapam.model.*;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.search.EntityStatusSearch;
import org.pamapam.repository.search.EntityStatusSearchSpecification;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Optional;

@Component
public class PamapamBackofficeApplicationDef extends BackofficeApplicationDef {

	private final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
	private final SimpleDateFormat sdtf = new SimpleDateFormat("dd MMM yyyy,  HH:mm");

	@Override
	@PostConstruct
	public void init() {
		this.initCrudLayoutDefs();
	}

	// ...	@formatter:off

	@SuppressWarnings("unchecked")
	private void initCrudLayoutDefs() {
		this.layoutDefs = new LinkedHashMap<>();

		this.layoutDefs.put(DashboardLayout.class, CustomLayoutDef.builder()
			.setName(this.messageSource.getMessage("dashboard.title"))
			.setLayoutClass(DashboardLayout.class)
			.setOpenOnStart(true)
			.build());

		this.layoutDefs.put(MyProfileLayout.class, CustomLayoutDef.builder()
				.setName(this.messageSource.getMessage("myProfile.title"))
				.setLayoutClass(MyProfileLayout.class)
				.build());

		this.layoutDefs.put(PamapamUser.class, CrudLayoutDef.<PamapamUser> builder()
			.setName(this.messageSource.getMessage("table.users"))
			.setEntityClass(PamapamUser.class)
			.columnDef().setId("username").setCaption("User name").setOrderAsc().setValueProvider(obj -> obj.getUsername()).end()
			.columnDef().setId("enabled").setCaption("Enabled").setValueProvider(obj -> obj.getEnabled()).end()
			.detailsLayoutClass(PamapamUserDetailsLayout.class)
			.setSearchEnabled(true)
			.searchLayoutClass(PamapamUserSearchLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(RoleImpl.class, CrudLayoutDef.<RoleImpl> builder()
			.setName(this.messageSource.getMessage("table.roles"))
			.setEntityClass(RoleImpl.class)
			.columnDef().setId("rolename").setCaption("Role name").setOrderAsc().setValueProvider(obj -> obj.getRolename()).end()
			.columnDef().setId("description").setCaption("Description").setValueProvider(obj -> obj.getDescription()).end()
			.detailsLayoutClass(RoleDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(LocalizedMessage.class, CrudLayoutDef.<LocalizedMessage> builder()
			.setName(this.messageSource.getMessage("table.localizedMessages"))
			.setEntityClass(LocalizedMessage.class)
			.columnDef().setId("key").setCaption("Key").setOrderAsc().setValueProvider(obj -> obj.getKey()).end()
			.columnDef().setId("language").setCaption("Languaje").setOrderAsc().setValueProvider(obj -> obj.getLanguage()).end()
			.columnDef().setId("country").setCaption("Country").setOrderAsc().setValueProvider(obj -> obj.getCountry()).end()
			.columnDef().setId("value").setCaption("Value").setValueProvider(obj -> obj.getValue()).end()
			.detailsLayoutClass(LocalizedMessageDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Language.class, CrudLayoutDef.<Language> builder()
			.setName(this.messageSource.getMessage("table.languages"))
			.setEntityClass(Language.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName()).end()
			.columnDef().setId("languageCode").setCaption("Language Code").setValueProvider(obj -> obj.getLanguageCode()).end()
			.columnDef().setId("countryCode").setCaption("Country Code").setValueProvider(obj -> obj.getCountryCode()).end()
			.detailsLayoutClass(LanguageDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Province.class, CrudLayoutDef.<Province> builder()
			.setName(this.messageSource.getMessage("table.province"))
			.setEntityClass(Province.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(ProvinceDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Region.class, CrudLayoutDef.<Region> builder()
			.setName(this.messageSource.getMessage("table.region"))
			.setEntityClass(Region.class)
			.columnDef().setId("province").setCaption("Province").setValueProvider(obj -> obj.getProvince().getName().getDefaultText()).end()
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(RegionDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Sector.class, CrudLayoutDef.<Sector> builder()
			.setName(this.messageSource.getMessage("table.sector"))
			.setEntityClass(Sector.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.columnDef().setId("description").setCaption("Description").setValueProvider(obj -> Optional.ofNullable(obj.getDescription()).map(LocalizedString::getDefaultText).orElse(null)).end()
			.detailsLayoutClass(SectorDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(SectorCcae.class, CrudLayoutDef.<SectorCcae> builder()
			.setName(this.messageSource.getMessage("table.sectorCcae"))
			.setEntityClass(SectorCcae.class)
			.columnDef().setId("code").setCaption("Code").setOrderAsc().setValueProvider(obj -> obj.getCode()).end()
			.columnDef().setId("name").setCaption("Name").setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(SectorCcaeDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Town.class, CrudLayoutDef.<Town> builder()
			.setName(this.messageSource.getMessage("table.town"))
			.setEntityClass(Town.class)
			.columnDef().setId("region").setCaption("Region").setValueProvider(obj -> obj.getRegion().getName().getDefaultText()).end()
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(TownDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(District.class, CrudLayoutDef.<District> builder()
			.setName(this.messageSource.getMessage("table.district"))
			.setEntityClass(District.class)
			.columnDef().setId("town").setCaption("Town").setValueProvider(obj -> obj.getTown().getName().getDefaultText()).end()
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(DistrictDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Neighborhood.class, CrudLayoutDef.<Neighborhood> builder()
			.setName(this.messageSource.getMessage("table.neighborhood"))
			.setEntityClass(Neighborhood.class)
			.columnDef().setId("district").setCaption("District").setValueProvider(obj -> obj.getDistrict().getName().getDefaultText()).end()
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(NeighborhoodDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(LegalForm.class, CrudLayoutDef.<LegalForm> builder()
			.setName(this.messageSource.getMessage("table.legalForm"))
			.setEntityClass(LegalForm.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(LegalFormDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(ExternalFilterTag.class, CrudLayoutDef.<ExternalFilterTag> builder()
			.setName(this.messageSource.getMessage("table.externalFilterTag"))
			.setEntityClass(ExternalFilterTag.class)
			.columnDef().setId("tagText").setCaption("Text").setOrderAsc().setValueProvider(obj -> obj.getTagText()).end()
			.detailsLayoutClass(ExternalFilterTagDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(EmbeddedMapConfig.class, CrudLayoutDef.<EmbeddedMapConfig> builder()
			.setName(this.messageSource.getMessage("table.embeddedMapConfig"))
			.setEntityClass(EmbeddedMapConfig.class)
			.columnDef().setId("domain").setCaption("Domain").setOrderAsc().setValueProvider(obj -> obj.getDomain()).end()
			.columnDef().setId("apiKey").setCaption("Api Key").setOrderAsc().setValueProvider(obj -> obj.getApiKey()).end()
			.detailsLayoutClass(EmbeddedMapConfigDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(ModelDef.class, CrudLayoutDef.<ModelDef> builder()
			.setName(this.messageSource.getMessage("table.modelDef"))
			.setEntityClass(ModelDef.class)
			.columnDef().setId("modelName").setCaption("Model Name").setOrderAsc().setValueProvider(obj -> obj.getModelName()).end()
			.detailsLayoutClass(ModelDefDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.setAddEnabled(false)
			.setRemoveEnabled(false)
			.build());

		this.layoutDefs.put(Community.class, CrudLayoutDef.<Community> builder()
			.setName(this.messageSource.getMessage("table.community"))
			.setEntityClass(Community.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(CommunityDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(EntityScope.class, CrudLayoutDef.<EntityScope> builder()
			.setName(this.messageSource.getMessage("table.scope"))
			.setEntityClass(EntityScope.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName()).end()
			.detailsLayoutClass(EntityScopeDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(IdeologicalIdentification.class, CrudLayoutDef.<IdeologicalIdentification> builder()
			.setName(this.messageSource.getMessage("table.ideologicalIdentification"))
			.setEntityClass(IdeologicalIdentification.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName()).end()
			.detailsLayoutClass(IdeologicalIdentificationDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Document.class, CrudLayoutDef.<Document> builder()
				.setName(this.messageSource.getMessage("table.document"))
				.setEntityClass(Document.class)
				.columnDef().setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getBinaryResource() != null ? obj.getBinaryResource().getFileName() : "").end()
				.columnDef().setCaption("File Name").setOrderAsc().setValueProvider(obj -> obj.getBinaryResource() != null ? obj.getBinaryResource().getFileName() : "").end()
				.columnDef().setCaption("Description").setValueProvider(obj -> obj.getBinaryResource() != null ? obj.getBinaryResource().getDescription() : "").end()
//				.columnDef().setCaption("Type").setValueProvider(obj -> obj.getBinaryResource() != null ? obj.getBinaryResource().getMimeType()).end()
				.detailsLayoutClass(DocumentDetailsLayout.class)
//				.setSearchEnabled(true)
//				.setSearchSpecification(this.documentSearchSpecification)
//				.searchLayoutClass(DocumentSearchLayout.class)
				.tableLayoutVerticalConfig()
				.build());

		this.layoutDefs.put(PersonRole.class, CrudLayoutDef.<PersonRole> builder()
			.setName(this.messageSource.getMessage("table.personRole"))
			.setEntityClass(PersonRole.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.columnDef().setId("description").setCaption("Description").setValueProvider(obj -> obj.getDescription().getDefaultText()).end()
			.detailsLayoutClass(PersonRoleDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(SocialEconomyNetwork.class, CrudLayoutDef.<SocialEconomyNetwork> builder()
			.setName(this.messageSource.getMessage("table.socialEconomyNetwork"))
			.setEntityClass(SocialEconomyNetwork.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(SocialEconomyNetworkDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(Criterion.class, CrudLayoutDef.<Criterion> builder()
			.setName(this.messageSource.getMessage("table.criterion"))
			.setEntityClass(Criterion.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.columnDef().setId("question").setCaption("Question").setValueProvider(obj -> obj.getQuestion().getDefaultText()).end()
			.detailsLayoutClass(CriterionDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

//		this.layoutDefs.put(EntityEvaluation.class, CrudLayoutDef.<EntityEvaluation> builder()
//			.setName(this.messageSource.getMessage("table.entityEvaluation"))
//			.setEntityClass(EntityEvaluation.class)
//			.columnDef().setId("entity").setCaption("Entity").setValueProvider(obj -> obj.getEntity().getName()).end()
//			.columnDef().setId("date").setCaption("Date").setValueProvider(obj -> obj.getDate()).end()
//			.detailsLayoutClass(EntityEvaluationDetailsLayout.class)
//			.tableLayoutVerticalConfig()
//			.build());

		this.layoutDefs.put(SearchInfo.class, CustomLayoutDef.builder()
				.setName(this.messageSource.getMessage("table.searchInfo"))
				.setLayoutClass(SearchInfoDetailsLayout.class)
				.build());

		this.layoutDefs.put(EntityStatus.class, CrudLayoutDef.<EntityStatus> builder()
			.setName(this.messageSource.getMessage("table.entityStatus"))
			.setEntityClass(EntityStatus.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(EntityStatusDetailsLayout.class)
			.setSearchSpecification(PamapamBackofficeApplicationDef.getPamapamEntityStatusSearchSpecification())
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(NotificationConfig.class, CrudLayoutDef.<NotificationConfig> builder()
			.setName(this.messageSource.getMessage("table.notificationConfig"))
			.setEntityClass(NotificationConfig.class)
			.columnDef()
				.setId("name")
				.setCaption("Name")
				.setOrderAsc()
				.setValueProvider(obj -> Optional.ofNullable(obj.getName()).orElse("")).end()
			.columnDef()
				.setId("userEntityLinkType")
				.setCaption("Punts")
				.setValueProvider(obj -> Optional.ofNullable(obj.getUserEntityLinkType())
					.map(x -> this.messageSource.getMessage("notificationConfig.userEntityLinkType." + x.name().toLowerCase())).orElse(""))
				.end()
			.columnDef()
				.setId("mailSubject")
				.setCaption("Mail Subject")
				.setValueProvider(obj -> Optional.ofNullable(obj.getMailSubject()).map(x -> x.getDefaultText()).orElse("")).end()
			.detailsLayoutClass(NotificationConfigDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(MailTemplate.class, CrudLayoutDef.<MailTemplate> builder()
			.setName(this.messageSource.getMessage("table.mailTemplate"))
			.setEntityClass(MailTemplate.class)
			.columnDef()
				.setId("name")
				.setCaption("Name")
				.setOrderAsc()
				.setValueProvider(obj -> Optional.ofNullable(obj.getName()).orElse("")).end()
			.detailsLayoutClass(MailTemplateDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(NewsletterRegistration.class, CrudLayoutDef.<NewsletterRegistration> builder()
			.setName(this.messageSource.getMessage("table.newsletterRegistration"))
			.setEntityClass(NewsletterRegistration.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName()).end()
			.columnDef().setId("email").setCaption("Mail").setOrderAsc().setValueProvider(obj -> obj.getEmail()).end()
			.columnDef().setId("registrationDate").setCaption("Registration Date").setOrderAsc().setValueProvider(obj -> obj.getRegistrationDate() != null ? this.sdf.format(obj.getRegistrationDate()) : "").end()
			.detailsLayoutClass(NewsletterRegistrationDetailsLayout.class)
			.setExportEnabled(true)
			.exportFileNameKey("table.newsletterRegistration")
			.exportLayoutClass(NewsletterRegistrationExportLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(SocialEconomyEntity.class, CrudLayoutDef.<SocialEconomyEntity> builder()
			.setName(this.messageSource.getMessage("table.socialEconomyEntity"))
			.setEntityClass(SocialEconomyEntity.class)
			.setOpenOnStart(true)
			.tableLayoutClass((Class<CrudTableLayout<SocialEconomyEntity>>) (Class<?>) SocialEconomyEntityTableLayout.class)
			.columnDef().setId("registryDate").setCaption(this.messageSource.getMessage("socialEconomyEntity.registryDate")).setValueProvider(obj -> this.sdf.format(Optional.ofNullable(obj.getRegistryDate()).orElse(new Date()))).end()
			.columnDef().setId("name").setCaption(this.messageSource.getMessage("socialEconomyEntity.name")).setOrderAsc().setValueProvider(obj -> obj.getName()).end()
			.columnDef()
				.setId("mainOffice")
				.setCaption(this.messageSource.getMessage("socialEconomyEntity.officeType"))
				.setValueProvider(obj -> {
					if (obj.isMainOffice() ) {
						return this.messageSource.getMessage("socialEconomyEntity.mainOffice");
					} else {
						return this.messageSource.getMessage("socialEconomyEntity.office");
					}
				})
				.end()
			.columnDef().setId("town").setCaption(this.messageSource.getMessage("socialEconomyEntity.town")).setValueProvider(obj -> Optional.ofNullable(obj.getTown()).map(x -> x.getName().getDefaultText()).orElse("")).end()
			.columnDef().setId("registryUser").setCaption(this.messageSource.getMessage("socialEconomyEntity.user")).setValueProvider(obj -> Optional.ofNullable(obj.getRegistryUser())
				.map(user -> user.getFullName())
				.orElse("")).end()
			.columnDef().setId("entityStatus").setCaption(this.messageSource.getMessage("socialEconomyEntity.entityStatus")).setValueProvider(obj -> obj.getEntityStatus().getName().getDefaultText()).end()
			.columnDef().setId("lastUpdateDate").setCaption(this.messageSource.getMessage("socialEconomyEntity.lastUpdateDate")).setValueProvider(obj -> this.sdtf.format(obj.getLastUpdateDate())).end()
			.columnDef().setId("entityScopes").setCaption(this.messageSource.getMessage("socialEconomyEntity.entityScopes")).setValueProvider(obj -> obj.getEntityScopesString()).end()
			.detailsLayoutClass(SocialEconomyEntityDetailsLayout.class)
			.setSearchEnabled(true)
			.searchLayoutClass(SocialEconomyEntitySearchLayout.class)
			.importLayoutClass(SocialEconomyEntityImportLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		/* FESC layouts */

		this.layoutDefs.put(FescEntityStatus.class, CrudLayoutDef.<EntityStatus> builder()
			.setId("fesc-entityStatus")
			.setName(this.messageSource.getMessage("table.entityStatus"))
			.setEntityClass(EntityStatus.class)
			.columnDef().setId("name").setCaption("Name").setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(EntityStatusDetailsLayout.class)
			.setSearchSpecification(PamapamBackofficeApplicationDef.getFescEntityStatusSearchSpecification())
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(FescRegistrationModality.class, CrudLayoutDef.<FescRegistrationModality> builder()
			.setName(this.messageSource.getMessage("table.fescRegistrationModality"))
			.setEntityClass(FescRegistrationModality.class)
			.columnDef().setId("name").setCaptionSupplier(() -> this.messageSource.getMessage("fesc.registration.invoice.modality.name")).setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.detailsLayoutClass(FescRegistrationModalityDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

		this.layoutDefs.put(FescRegistrationFee.class, CrudLayoutDef.<FescRegistrationFee> builder()
			.setName(this.messageSource.getMessage("table.fescRegistrationFee"))
			.setEntityClass(FescRegistrationFee.class)
			.columnDef().setId("name").setCaptionSupplier(() -> this.messageSource.getMessage("fesc.registration.invoice.fee.name")).setOrderAsc().setValueProvider(obj -> obj.getName().getDefaultText()).end()
			.columnDef().setId("fee").setCaptionSupplier(() -> this.messageSource.getMessage("fesc.registration.invoice.fee.fee")).setOrderAsc().setValueProvider(obj -> Optional.ofNullable(obj.getFee()).map(i -> i.toString()).orElse("")).end()
			.detailsLayoutClass(FescRegistrationFeeDetailsLayout.class)
			.tableLayoutVerticalConfig()
			.build());

	}

	private static EntityStatusSearchSpecification getPamapamEntityStatusSearchSpecification() {
		final EntityStatusSearch searchObject = new EntityStatusSearch();
		searchObject.setTypes(EntityStatusType.getPamapamTypes());
		return new EntityStatusSearchSpecification(searchObject);
	}

	private static EntityStatusSearchSpecification getFescEntityStatusSearchSpecification() {
		final EntityStatusSearch searchObject = new EntityStatusSearch();
		searchObject.setTypes(EntityStatusType.getFescTypes());
		return new EntityStatusSearchSpecification(searchObject);
	}

	// ...	@formatter:on

}
