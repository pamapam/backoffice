package org.pamapam.backoffice;

import org.pamapam.services.PamapamFreeMarkerTemplateLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;

@Configuration
public class PamapamFreeMarkerConfiguration {

	@Autowired
	private PamapamFreeMarkerTemplateLoader pamapamFreemakerTemplateLoader;

	@Bean
	public FreeMarkerConfigurer freeMarkerConfigurer() {
		FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
		freemarker.template.Configuration freeMarkerConfiguration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_28);
		ClassTemplateLoader classTemplateLoader = new ClassTemplateLoader(FreeMarkerConfigurer.class, "/templates");
		MultiTemplateLoader templateLoader = new MultiTemplateLoader(new TemplateLoader[] { classTemplateLoader, this.pamapamFreemakerTemplateLoader });
		freeMarkerConfiguration.setTemplateLoader(templateLoader);
		configurer.setConfiguration(freeMarkerConfiguration);
		return configurer;
	}

}
