package org.pamapam.backoffice.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.message.LocalizedMessageService;
import org.pamapam.backoffice.PamapamSession;
import org.pamapam.backoffice.layout.FescRegistrationLayout;
import org.pamapam.backoffice.layout.FescSeeSelectionLayout;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.repository.search.SocialEconomyEntitySearch;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.pamapam.services.EntityScopeService;
import org.pamapam.services.SocialEconomyEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Optional;

@SpringUI(path = "/fesc/registration/pamapam")
@Theme("fescTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class FescRegistration extends UI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private LocalizedMessageService messageSource;
	@Autowired
	private PamapamSession session;
	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private SocialEconomyEntityRepository socialEconomyEntityRepository;
	@Autowired
	private SocialEconomyEntityService socialEconomyEntityService;
	@Autowired
	private EntityScopeService entityScopeService;
	private FescRegistrationLayout registrationLayout;

	@Value("${fesc.registration.successUrl}")
	private String successUrl;

	@Override
	protected void init(final VaadinRequest request) {
		this.setNavigator(null);

		final PamapamUser user = this.session.getCurrentUser();
		final long countSee = this.countUserInitiatives();
		if (countSee <= 0) {
			MessageBox.createError()
				.withMessage(this.messageSource.getMessage("user.fesc.noEntity"))
				.withOkButton(() -> this.reloadPage(), ButtonOption.caption(this.messageSource.getMessage("dialog.ok")))
				.asModal(true)
				.open();
		} else if (countSee > 1) {
			this.showSeeSelectionLayout(user);
		} else {
			final SocialEconomyEntity see = this.getConnectedSocialEconomyEntity();
			this.showRegistrationLayout(Optional.ofNullable(see).orElse(new SocialEconomyEntity()));
		}

		this.setSizeFull();
	}

	private void reloadPage() {
		this.getSession().close();
		SecurityContextHolder.clearContext();
		this.getUI().getPage().reload();
	}

	private void showSeeSelectionLayout(final PamapamUser user) {
		final FescSeeSelectionLayout layout = this.applicationContext.getBean(FescSeeSelectionLayout.class);
		layout.initialize(false);
		layout.setOkHandler(this::resumeRegistration);
		this.setContent(layout);
	}

	private void resumeRegistration(final FescSeeSelectionLayout layout) {
		final SocialEconomyEntity see = layout.getSelectedSee();
		this.showRegistrationLayout(see);
	}

	private Long countUserInitiatives() {
		final SocialEconomyEntitySpecification spec = this.getSeeSpecification();
		return this.socialEconomyEntityRepository.count(spec);
	}

	private SocialEconomyEntity getConnectedSocialEconomyEntity() {
		final SocialEconomyEntitySpecification spec = this.getSeeSpecification();
		return this.socialEconomyEntityRepository.findOne(spec);
	}

	private SocialEconomyEntitySpecification getSeeSpecification() {
		final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
		searchObject.setInitiativeUser(this.session.getCurrentUser());
		final SocialEconomyEntitySpecification spec = new SocialEconomyEntitySpecification();
		spec.setSearchObject(searchObject);
		return spec;
	}

	private void showRegistrationLayout(final SocialEconomyEntity see) {
		this.registrationLayout = this.applicationContext.getBean(FescRegistrationLayout.class);
		this.registrationLayout.initialize();
		this.registrationLayout.setTargetObject(see);
		this.registrationLayout.setOkHandler(this::saveRegistration);
		this.setContent(this.registrationLayout);
	}

	private void saveRegistration(final FescRegistrationLayout layout) {
		SocialEconomyEntity savedEntity = null;
		try {
			final SocialEconomyEntity entity = layout.getTargetObject();
			final List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();
			final EntityStatus fescEntityStatus = entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.FESC_PROPOSED).findFirst().get();
			entity.setFescEntityStatus(fescEntityStatus);
			entity.getEntityScopes().add(this.entityScopeService.getFesc2022EntityScope());
			savedEntity = this.socialEconomyEntityService.save(entity, entity.getEntityStatus());
			final Notification savedNotification = new Notification(this.messageSource.getMessage("feedback.saved"), Type.TRAY_NOTIFICATION);
			savedNotification.setStyleName("emphasized");
			savedNotification.setDelayMsec(3000);
			savedNotification.show(Page.getCurrent());
			this.registrationLayout.setTargetObject(savedEntity);

			// Navigate to external page
			if (this.successUrl != null) {
				this.getSession().close();
				SecurityContextHolder.clearContext();
				this.getUI().getPage().setLocation(this.successUrl);
			}
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}

	}

}
