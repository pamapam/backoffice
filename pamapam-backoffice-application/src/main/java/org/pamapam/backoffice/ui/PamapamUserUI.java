package org.pamapam.backoffice.ui;

import java.util.Optional;

import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.message.LocalizedMessageService;
import org.pamapam.backoffice.layout.details.PamapamUserDetailsLayout;
import org.pamapam.model.PamapamUser;
import org.pamapam.services.PamapamUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

@SpringUI(path = "/userProfile")
@Theme("demoTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class PamapamUserUI extends UI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	private PamapamUserService userService;
	@Autowired
	protected CrudServices crudServices;

	private PamapamUserDetailsLayout detailsLayout;

	@Override
	protected void init(VaadinRequest request) {
		this.setNavigator(null);

		this.detailsLayout = this.applicationContext.getBean(PamapamUserDetailsLayout.class);
		this.detailsLayout.initialize();
		this.detailsLayout.setOkHandler(obj -> this.saveDetails((PamapamUserDetailsLayout) obj));
		this.detailsLayout.setCancelHandler(obj -> this.cancelDetails((PamapamUserDetailsLayout) obj));

		PamapamUser currentUser = this.userService.getCurrentUser();
		this.detailsLayout.updateFields(currentUser);
		this.setContent(this.detailsLayout);
		this.setSizeFull();
	}

	protected void saveDetails(PamapamUserDetailsLayout detailsLayout) {
		PamapamUser savedItem = null;
		try {
			savedItem = this.crudServices.save(detailsLayout.getTargetObject());
		} catch (CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		detailsLayout.updateFields(savedItem);
	}

	protected void cancelDetails(PamapamUserDetailsLayout detailsLayout) {
		// ...	TODO
	}

//	private void updateForm(PamapamUser model) {
//		this.modelTabSheet.setSizeFull();
//		this.modelTabSheet.init(null, null);
//		this.modelTabSheet.getSearchButton().setVisible(false);
//		this.modelTabSheet.getCancelButton().setVisible(false);
//
//		PamapamUserDetailsLayout pamapamUserDetailsLayout = this.applicationContext.getBean(PamapamUserDetailsLayout.class);
//		pamapamUserDetailsLayout.setCaption(this.messageSource.getMessage("form.basic.info"));
//		pamapamUserDetailsLayout.setModelTabSheet(this.modelTabSheet);
//		pamapamUserDetailsLayout.init(model, true);
//
//
//		this.modelTabSheet.addFormTab(pamapamUserDetailsLayout).setCaption(pamapamUserDetailsLayout.getCaption());
//
//		List<Layout> relatedForms = pamapamUserDetailsLayout.getRelatedForms();
//		if (relatedForms != null) {
//			for (Layout relatedForm : relatedForms) {
//				String tabLabel = "";
//				if (ModelFormLayout.class.isAssignableFrom(relatedForm.getClass())) {
//					tabLabel = ((ModelFormLayout<?>) relatedForm).getName();
//				} else {
//					tabLabel = relatedForm.getCaption();
//				}
//				this.modelTabSheet.addFormTab(relatedForm).setCaption(tabLabel);
//			}
//		}
//	}

}
