package org.pamapam.backoffice.ui;

import org.jamgo.ui.MainUI;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;

@SpringUI(path = "/")
@SpringViewDisplay
@Theme("demoTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class PamapamMainUI extends MainUI {

	private static final long serialVersionUID = 1L;

}
