package org.pamapam.backoffice.ui;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.jamgo.services.exception.CrudException;
import org.jamgo.services.message.LocalizedMessageService;
import org.pamapam.backoffice.layout.FescRegistrationLayout;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.services.EntityScopeService;
import org.pamapam.services.PamapamUserService;
import org.pamapam.services.SocialEconomyEntityService;
import org.pamapam.services.dto.PamapamUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Sets;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.data.ValidationException;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

@SpringUI(path = "/fesc/registration/public")
@Theme("fescTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class FescPublicRegistration extends UI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private LocalizedMessageService messageSource;
	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private PamapamUserService pamapamUserService;
	@Autowired
	private SocialEconomyEntityService socialEconomyEntityService;
	@Autowired
	private EntityScopeService entityScopeService;

	private FescRegistrationLayout registrationLayout;

	@Value("${fesc.registration.successUrl}")
	private String successUrl;

	@Override
	protected void init(final VaadinRequest request) {
		this.setNavigator(null);

		this.registrationLayout = this.applicationContext.getBean(FescRegistrationLayout.class);
		this.registrationLayout.setPublicRegistration(true);
		this.registrationLayout.initialize();
		this.registrationLayout.setTargetObject(new SocialEconomyEntity());
		this.registrationLayout.setOkHandler(this::saveRegistration);

		this.setContent(this.registrationLayout);
		this.setSizeFull();
	}

	private void saveRegistration(final FescRegistrationLayout layout) {
		try {
			layout.updateTargetObject();
			this.saveUser(layout.getTargetObject());
			this.saveEntity(layout.getTargetObject());

			final Notification savedNotification = new Notification(this.messageSource.getMessage("feedback.saved"), Type.TRAY_NOTIFICATION);
			savedNotification.setStyleName("emphasized");
			savedNotification.setDelayMsec(3000);
			savedNotification.show(Page.getCurrent());

			// Navigate to external page
			if (this.successUrl != null) {
				this.getUI().getPage().setLocation(this.successUrl);
			}
		} catch (final ValidationException e) {
			final Notification validationNotification = new Notification(this.messageSource.getMessage("feedback.validationErrors"), Type.WARNING_MESSAGE);
			validationNotification.setDelayMsec(3000);
			validationNotification.show(Page.getCurrent());
		}
	}

	private void saveUser(final SocialEconomyEntity entity) {
		final String fescEmail = entity.getFescEmail();
		final String entityEmail = entity.getEmail();
		final String contactPersonName = entity.getContactPerson();

		// Search user with fescEmail
		PamapamUser user = this.pamapamUserService.findByUsername(fescEmail);
		// - if not found, create it
		if (user == null) {
			final PamapamUserDto userDto = new PamapamUserDto();
			userDto.setName(contactPersonName);
			userDto.setUsername(entityEmail);
			userDto.setEmail(fescEmail);
			userDto.setRoleNames(Arrays.asList(PamapamUser.ROLE_INITIATIVE));
			user = this.pamapamUserService.addFescUser(userDto);
		}
		// attach user with the entity as initiativeUser
		entity.setInitiativeUser(user);
	}

	private void saveEntity(final SocialEconomyEntity entity) {
		SocialEconomyEntity savedEntity = null;
		try {
			final List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();

			//Els punts no Pam a pam i sí FESC – tenen un estat Pam a pam «fitxa bàsica» i un estat FESC Y
			final EntityStatus entityStatus = entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.INITIATIVE).findFirst().get();
			entity.setEntityStatus(entityStatus);
			final EntityStatus fescEntityStatus = entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.FESC_PROPOSED).findFirst().get();
			entity.setFescEntityStatus(fescEntityStatus);
			entity.setRegistryDate(new Date());
			// #10491 Assignar a l'entitat l'àmbit FESC2021
			if (entity.isMainOffice()) {
				if (CollectionUtils.isEmpty(entity.getEntityScopes())) {
					entity.setEntityScopes(Sets.newLinkedHashSet());
				}
				entity.getEntityScopes().add(this.entityScopeService.getFesc2022EntityScope());
			}

			savedEntity = this.socialEconomyEntityService.save(entity, entity.getEntityStatus());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		this.registrationLayout.setTargetObject(savedEntity);
	}

}
