package org.pamapam.backoffice.ui;

import com.vaadin.server.FontIcon;

public enum PamapamIcons implements FontIcon {

	FULL_POINT(0xE900), EMPTY_POINT(0xE901), HALF_POINT(0xE902);

	private static final String fontFamily = "Pamapam-Icons";
	private int codepoint;

	PamapamIcons(int codepoint) {
		this.codepoint = codepoint;
	}

	@Override
	public String getMIMEType() {
		throw new UnsupportedOperationException(FontIcon.class.getSimpleName()
			+ " should not be used where a MIME type is needed.");
	}

	@Override
	public String getFontFamily() {
		return PamapamIcons.fontFamily;
	}

	@Override
	public int getCodepoint() {
		return this.codepoint;
	}

	@Override
	public String getHtml() {
		return "<span class=\"v-icon v-icon-" + this.name().toLowerCase()
			+ "\" style=\"font-family: " + PamapamIcons.fontFamily + ";\">&#x"
			+ Integer.toHexString(this.codepoint) + ";</span>";
	}
}
