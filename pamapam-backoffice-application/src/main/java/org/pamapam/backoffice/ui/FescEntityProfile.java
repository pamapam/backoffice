package org.pamapam.backoffice.ui;

import org.jamgo.services.message.LocalizedMessageService;
import org.pamapam.backoffice.PamapamSession;
import org.pamapam.backoffice.layout.FescEntityProfileLayout;
import org.pamapam.backoffice.layout.FescSeeSelectionLayout;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.repository.search.SocialEconomyEntitySearch;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;

@SpringUI(path = "/fesc/profile")
@Theme("fescTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class FescEntityProfile extends UI {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private LocalizedMessageService messageSource;
	@Autowired
	private PamapamSession session;
	@Autowired
	private SocialEconomyEntityRepository socialEconomyEntityRepository;
	@Autowired
	private FescEntityProfileLayout fescEntityProfileLayout;
	@Autowired
	private ApplicationContext applicationContext;

	@Override
	protected void init(VaadinRequest request) {
		this.setNavigator(null);

		final long countSee = this.countUserInitiatives();
		if (countSee <= 0) {
			MessageBox.createError()
				.withMessage(this.messageSource.getMessage("user.fesc.noEntity"))
				.withOkButton(() -> this.reloadPage(), ButtonOption.caption(this.messageSource.getMessage("dialog.ok")))
				.asModal(true)
				.open();
		} else if (countSee > 1) {
			this.showSeeSelectionLayout(this.session.getCurrentUser());
		} else {
			final SocialEconomyEntity see = this.getConnectedSocialEconomyEntity();
			this.showProfileLayout(see);
		}

		this.setSizeFull();
	}
	
	private void showProfileLayout(SocialEconomyEntity see) {
		this.fescEntityProfileLayout.initialie(see);
		this.setContent(this.fescEntityProfileLayout);
	}
	
	private void reloadPage() {
		this.getSession().close();
		SecurityContextHolder.clearContext();
		this.getUI().getPage().reload();
	}
	
	private Long countUserInitiatives() {
		final SocialEconomyEntitySpecification spec = this.getSeeSpecification();
		return this.socialEconomyEntityRepository.count(spec);
	}

	private SocialEconomyEntity getConnectedSocialEconomyEntity() {
		final SocialEconomyEntitySpecification spec = this.getSeeSpecification();
		return this.socialEconomyEntityRepository.findOne(spec);
	}

	private SocialEconomyEntitySpecification getSeeSpecification() {
		final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
		searchObject.setInitiativeUser(this.session.getCurrentUser());
		searchObject.setFescRegistered(true);
		final SocialEconomyEntitySpecification spec = new SocialEconomyEntitySpecification();
		spec.setSearchObject(searchObject);
		return spec;
	}

	private void showSeeSelectionLayout(final PamapamUser user) {
		final FescSeeSelectionLayout layout = this.applicationContext.getBean(FescSeeSelectionLayout.class);
		layout.initialize(true);
		layout.setOkHandler(this::resumeProfile);
		this.setContent(layout);
	}
	
	private void resumeProfile(final FescSeeSelectionLayout layout) {
		final SocialEconomyEntity see = layout.getSelectedSee();
		this.showProfileLayout(see);
	}
	
}
