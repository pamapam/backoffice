package org.pamapam.backoffice.ui;

import com.google.common.collect.Lists;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.themes.ValoTheme;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import org.apache.commons.lang3.StringUtils;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.LoginUI;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.pamapam.backoffice.PamapamAuthenticationSuccessHandler;
import org.pamapam.backoffice.PamapamSession;
import org.pamapam.backoffice.layout.PrivacyConsentLayout;
import org.pamapam.exception.GdprConsentException;
import org.pamapam.exception.NotAllowedException;
import org.pamapam.model.PamapamUser;
import org.pamapam.services.NewsletterRegistrationService;
import org.pamapam.services.PamapamUserService;
import org.pamapam.services.dto.NewsletterRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.vaadin.spring.security.shared.VaadinAuthenticationSuccessHandler;

import javax.servlet.http.Cookie;
import java.util.Optional;

@SpringUI(path = "/login")
@Theme("demoTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class PamapamLoginUI extends LoginUI {

	private static final long serialVersionUID = 1L;
	private static final String FRONT_COOKIE = "wordpress_origin";

	@Autowired
	private PamapamUserService userService;
	@Autowired
	private NewsletterRegistrationService newsletterRegistrationService;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	private PrivacyConsentLayout privacyConsentLayout;
	@Autowired
	private PamapamSession session;
	@Autowired
	private JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	private VaadinAuthenticationSuccessHandler vaadinAuthenticationSuccessHandler;

	private Authentication token;

	@Override
	protected void init(final VaadinRequest request) {
		// ...	We don't need a Navigator in this UI. If Navigator is not null, default initialization
		//		fires an exception.
		this.setNavigator(null);

		final VerticalLayout mainLayout = this.componentFactory.newVerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(false);
		mainLayout.setSpacing(false);
		final Component layout = this.buildLayout();
		mainLayout.addComponents(layout);

		mainLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
		this.setContent(mainLayout);
		this.username.focus();
		this.setSizeFull();
	}

	private Component buildLayout() {
		final HorizontalLayout layout = this.componentFactory.newHorizontalLayout();
		layout.setMargin(false);
		layout.setSpacing(false);
		layout.setStyleName("login-layout");
		layout.setSizeFull();
		final Component loginForm = this.buildLoginForm();
		layout.addComponents(loginForm);
		return layout;
	}

	private Component buildLoginForm() {

		final VerticalLayout layout = this.componentFactory.newVerticalLayout();
		layout.setSizeFull();
		layout.setMargin(false);
		layout.setSpacing(false);
		final HorizontalLayout topBar = this.createLoginHeaderLayout();
		final Panel loginFormPanel = this.createLoginFormLayout();

		final Label description = this.componentFactory.newLabel();
		description.setContentMode(ContentMode.HTML);
		description.setValue(this.messageSource.getMessage("login.description"));
		description.setWidth(50, Unit.PERCENTAGE);

		final Label descriptionFesc = this.componentFactory.newLabel();
		descriptionFesc.setContentMode(ContentMode.HTML);
		descriptionFesc.setValue(this.messageSource.getMessage("login.description.fesc"));
		descriptionFesc.setWidth(50, Unit.PERCENTAGE);

		final VerticalLayout bottomBar = this.componentFactory.newVerticalLayout();
		bottomBar.setWidth(100, Unit.PERCENTAGE);
		bottomBar.setStyleName("login-footer");
		bottomBar.addComponents(description, descriptionFesc);
		bottomBar.setComponentAlignment(description, Alignment.BOTTOM_CENTER);
		bottomBar.setComponentAlignment(descriptionFesc, Alignment.BOTTOM_CENTER);

		layout.addComponents(topBar, loginFormPanel, bottomBar);
		layout.setComponentAlignment(loginFormPanel, Alignment.MIDDLE_CENTER);

		return layout;
	}

	@Override
	protected HorizontalLayout createLoginHeaderLayout() {
		final HorizontalLayout topBar = new HorizontalLayout();
		topBar.setWidth(100, Unit.PERCENTAGE);
		topBar.setHeight(100, Unit.PERCENTAGE);
		topBar.setStyleName("login-header");

		final ThemeResource logoResource = new ThemeResource("images/logo.png");
		final Image logo = new Image(null, logoResource);
		logo.setStyleName("login-header-logo");
		logo.setWidth(400, Unit.PIXELS);

		final ThemeResource logoFescResource = new ThemeResource("images/logo-fesc-2021.png");
		final Image logoFesc = new Image(null, logoFescResource);
		logoFesc.setWidth(200, Unit.PIXELS);
		logoFesc.setStyleName("login-header-logo-fesc");
		final Label applicationTitle = new Label(this.messageSource.getMessage("application.name"));
		applicationTitle.addStyleName("login-header-title");
		topBar.addComponents(logo, applicationTitle, logoFesc);
		topBar.setExpandRatio(applicationTitle, 1.0f);
		topBar.setComponentAlignment(logo, Alignment.TOP_LEFT);
		topBar.setComponentAlignment(applicationTitle, Alignment.BOTTOM_CENTER);
		topBar.setComponentAlignment(logoFesc, Alignment.TOP_RIGHT);

		return topBar;
	}

	@Override
	protected void navigateToView() {
		final Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
		final boolean redirectToWordpress = Lists.newArrayList(cookies).stream().anyMatch(cookie -> cookie.getName().equals(PamapamLoginUI.FRONT_COOKIE));
		if (redirectToWordpress) {
			this.getPage().setLocation("/?connect=jamgo");
		} else {
			final String path = this.getPage().getLocation().getPath();
			String savedRedirectUrl = ((PamapamAuthenticationSuccessHandler) this.vaadinAuthenticationSuccessHandler).getSavedRedirectUrl();
			if (StringUtils.isEmpty(savedRedirectUrl)) {
				savedRedirectUrl = Optional.ofNullable(this.session.getSavedRedirectUrl()).orElse("");
			} else {
				// Back button hacking -> reuses previous saved redirect URL collected by spring security interceptors
				this.session.setSavedRedirectUrl(savedRedirectUrl);
			}
			this.getPage().setLocation(StringUtils.substringBefore(path, "/ui") + "/ui" + StringUtils.substringAfter(savedRedirectUrl, "/ui"));
//			try {
//				// TODO Improve authentication process implementing SessionAuthenticationStrategy for this project
//				this.vaadinAuthenticationSuccessHandler.onAuthenticationSuccess(this.token);
//			} catch (final Exception e) {
//				e.printStackTrace();
//			}
		}
	}

	@Override
	protected Panel createLoginFormLayout() {
		final FormLayout loginFormLayout = this.componentFactory.newFormLayout();
		loginFormLayout.setStyleName("login-form");
		loginFormLayout.setWidth(30, Unit.PERCENTAGE);
		loginFormLayout.setMargin(true);
		loginFormLayout.setSpacing(true);
		final VerticalLayout loginLayout = this.componentFactory.newVerticalLayout();
		loginLayout.addComponent(loginFormLayout);
		loginLayout.setSizeFull();
		loginLayout.setMargin(false);
		loginLayout.setComponentAlignment(loginFormLayout, Alignment.MIDDLE_CENTER);
		final Panel loginFormPanel = this.componentFactory.newPanel();
		loginFormPanel.setContent(loginLayout);
		loginFormPanel.setSizeFull();

		this.username = this.componentFactory.newTextField("login.user", "Usuari");
		this.password = this.componentFactory.newPasswordField("login.password", "Contrasenya");

		final Button accedir = this.componentBuilderFactory.createButtonBuilder().build();
		accedir.setCaption("Accedir");
		accedir.addClickListener(this::clickLogin);
		accedir.setClickShortcut(KeyCode.ENTER);

		final Button passwordRecovery = this.componentBuilderFactory.createButtonBuilder().build();
		passwordRecovery.setCaption("Restablir contrasenya");
		passwordRecovery.setStyleName(ValoTheme.BUTTON_LINK);
		passwordRecovery.addClickListener(event -> this.getResetMessageBox().open());

		final HorizontalLayout buttonsLayout = this.componentFactory.newHorizontalLayout();
		buttonsLayout.addComponent(accedir);
		buttonsLayout.setSpacing(true);

		loginFormLayout.addComponents(this.username, this.password/*, rememberMe*/, buttonsLayout, passwordRecovery);

		return loginFormPanel;
	}

	protected MessageBox getResetMessageBox() {
		if (this.username.isEmpty()) {
			return this.getResetInstructionsMessageBox();
		} else {
			return this.getResetConfirmationMessageBox();
		}
	}

	protected MessageBox getResetInstructionsMessageBox() {
		return MessageBox.createQuestion()
			.withCaption(this.messageSource.getMessage("dialog.reset.password.caption"))
			.withMessage(this.messageSource.getMessage("dialog.reset.password.instructions"))
			.withOkButton(ButtonOption.caption(this.messageSource.getMessage("dialog.ok")));
	}

	protected MessageBox getResetConfirmationMessageBox() {
		return MessageBox.createQuestion()
			.withCaption(this.messageSource.getMessage("dialog.reset.password.caption"))
			.withMessage(this.messageSource.getMessage("dialog.reset.password.message"))
			.withYesButton(() -> PamapamLoginUI.this.doPasswordReset(), ButtonOption.caption(this.messageSource.getMessage("dialog.yes")))
			.withNoButton(ButtonOption.caption(this.messageSource.getMessage("dialog.no")));
	}

	protected void doPasswordReset() {
		PamapamUser user = this.userService.findByUsername(this.username.getValue());
		if (user == null) {
			user = this.userService.findByEmail(this.username.getValue());
		}
		if (user == null) {
			Notification.show("No hi ha cap usuària amb aquest nom o correu electrònic.", Type.ERROR_MESSAGE);
		} else {
			try {
				this.userService.resetPassword(user);
				final Notification sendNotification = new Notification(this.messageSource.getMessage("dialog.reset.password.confirmation"), Type.HUMANIZED_MESSAGE);
				sendNotification.setStyleName("emphasized");
				sendNotification.setDelayMsec(3000);
				sendNotification.show(Page.getCurrent());
			} catch (final CrudException e) {
				Notification.show("Error restablint contrasenya", Type.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void clickLogin(final ClickEvent event) {
		try {
			final String user = this.username.getValue();
			final String pass = this.password.getValue();
			// TODO boolean check = rememberMe.getValue();
			this.login(user, pass/*, check*/);
		} catch (final AuthenticationException e) {
			Notification.show("Usuari o password incorrectes", Type.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (final NotAllowedException e) {
			final Notification notification = new Notification(this.messageSource.getMessage("login.notAllowed"), Type.WARNING_MESSAGE);
			notification.setHtmlContentAllowed(true);
			notification.setDelayMsec(Notification.DELAY_FOREVER);
			notification.show(Page.getCurrent());
			e.printStackTrace();
		} catch (final GdprConsentException e) {
			this.askForPrivacyConsent();
		} catch (final Exception e) {
			e.printStackTrace();
		}
		// TODO Register Remember me Token

		/*
		 * Redirect is handled by the VaadinRedirectStrategy. User is
		 * redirected to either always the default or the URL the user
		 * request before authentication.
		 *
		 * Strategy is configured within SeTokencurityConfiguration Defaults
		 * to User request URL.
		 */
	}

	@Override
	protected void login(final String username, final String password) throws Exception {
		this.token = this.authenticationManager
			.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		if (!this.userService.isLoginAllowed(username)) {
			throw new NotAllowedException();
		}
		if (!this.userService.hasPrivacyConsent(username)) {
			throw new GdprConsentException();
		}
		this.finishAuthentication();
		this.session.init();
	}

	private void finishAuthentication() {
		// Reinitialize the session to protect against session fixation attacks. This does not work
		// with websocket communication.
		VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
		SecurityContextHolder.getContext().setAuthentication(this.token);
		this.navigateToView();
	}

	private void askForPrivacyConsent() {
		final Window window = this.componentFactory.newWindow();
		window.setCaption(this.messageSource.getMessage("user.privacyConsent.title"));
		window.setModal(true);
		window.setWidth(60, Unit.EX);
		window.setHeight(30, Unit.EX);
		window.center();

		window.setContent(this.privacyConsentLayout);
		window.addCloseListener(new Window.CloseListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void windowClose(final CloseEvent ce) {
				PamapamLoginUI.this.privacyConsentResponse();
			}
		});
		UI.getCurrent().addWindow(window);
	}

	private void privacyConsentResponse() {
		if (this.privacyConsentLayout.isPrivaryConsentAccepted()) {
			this.userService.setPrivacyConsent(this.username.getValue(), true);

			this.finishAuthentication();
			// FIX User not stored in pamapam session
			this.session.init();
		}

		if (this.privacyConsentLayout.isNewsletterConsentAccepted()) {
			final PamapamUser user = this.userService.findByUsername(this.username.getValue());
			final NewsletterRegistrationDto newsletterRegistrationDto = new NewsletterRegistrationDto(this.username.getValue(), user.getEmail());
			try {
				this.newsletterRegistrationService.addNewsletterRegistration(newsletterRegistrationDto);
			} catch (final CrudException e) {
				e.printStackTrace();
			}
		}
	}
}
