package org.pamapam.backoffice.ui;

import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.ContentLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

@SpringUI(path = "/entitiesManagement")
@Theme("demoTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class SocialEconomyEntitiesManagementUI extends UI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected CrudServices crudServices;

	private ContentLayout contentLayout;

	@Override
	protected void init(VaadinRequest request) {
		this.setNavigator(null);

		this.contentLayout = this.applicationContext.getBean(ContentLayout.class);

		this.setContent(this.contentLayout);
		this.setSizeFull();
	}

}
