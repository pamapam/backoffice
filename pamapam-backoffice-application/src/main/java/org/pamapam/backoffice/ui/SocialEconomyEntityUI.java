package org.pamapam.backoffice.ui;

import java.util.Optional;

import org.jamgo.services.exception.CrudException;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.pamapam.backoffice.layout.details.see.SocialEconomyEntityDetailsLayout;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.services.SocialEconomyEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

@SpringUI(path = "/socialEconomyEntity")
@Theme("demoTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class SocialEconomyEntityUI extends UI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected SocialEconomyEntityService socialEconomyEntityService;
	@Autowired

	private SocialEconomyEntityDetailsLayout detailsLayout;

	@Override
	protected void init(VaadinRequest request) {
		this.setNavigator(null);

		this.detailsLayout = this.applicationContext.getBean(SocialEconomyEntityDetailsLayout.class);
		this.detailsLayout.initialize();
		this.detailsLayout.setOkHandler(obj -> this.saveDetails(obj));
		this.detailsLayout.setCancelHandler(obj -> this.cancelDetails(obj));
		this.detailsLayout.setOkDraftHandler(obj -> this.saveDetails(obj));
		this.detailsLayout.setOkPendingHandler(obj -> this.savePendingDetails(obj));

		SocialEconomyEntity socialEconomyEntity = new SocialEconomyEntity();
		this.detailsLayout.updateFields(socialEconomyEntity);
		this.setContent(this.detailsLayout);
		this.setSizeFull();
	}

	private void saveDetails(CrudDetailsLayout<SocialEconomyEntity> detailsLayout) {
		SocialEconomyEntity savedEntity = null;
		try {
			savedEntity = this.socialEconomyEntityService.save(
				detailsLayout.getTargetObject(), ((SocialEconomyEntityDetailsLayout) detailsLayout).getPreviousStatus());
		} catch (CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		detailsLayout.updateFields(savedEntity);
	}

	private void savePendingDetails(CrudDetailsLayout<SocialEconomyEntity> detailsLayout) {
		SocialEconomyEntity savedEntity = null;
		try {
			savedEntity = this.socialEconomyEntityService.savePendingSocialEconomyEntity(detailsLayout.getTargetObject());
		} catch (CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		detailsLayout.updateFields(savedEntity);
	}

	private void cancelDetails(CrudDetailsLayout<SocialEconomyEntity> detailsLayout) {
		// ...	TODO
	}

//	@Override
//	protected void init(VaadinRequest request) {
//		this.setNavigator(null);
//		SocialEconomyEntity demoEntity = new SocialEconomyEntity();
//		this.updateForm(demoEntity);
//		this.setContent(this.modelTabSheet);
//		this.setSizeFull();
//	}

//	private void updateForm(SocialEconomyEntity model) {
//		this.modelTabSheet.setSizeFull();
//		this.modelTabSheet.init(null, null);
//		this.modelTabSheet.getSearchButton().setVisible(false);
//		this.modelTabSheet.getCancelButton().setVisible(false);
//		if (VaadinService.getCurrentRequest().isUserInRole(ROLE_XINXETA)) {
//			this.modelTabSheet.getSaveButton().setCaption(this.messageSource.getMessage("action.save1"));
//			this.modelTabSheet.getSaveButton().setVisible(true);
//			this.modelTabSheet.getSaveButton().addStyleName("background-yellow");
//			// ...	FIXME: Volver a implementar correctamente el doble save.
////			this.modelTabSheet.getAltSaveButton().setVisible(true);
////			this.modelTabSheet.getAltSaveButton().addStyleName("background-green");
//		} else {
//			this.modelTabSheet.getSaveButton().setCaption(this.messageSource.getMessage("action.save"));
//			this.modelTabSheet.getSaveButton().setVisible(true);
//			// ...	FIXME: Volver a implementar correctamente el doble save.
////			this.modelTabSheet.getAltSaveButton().setVisible(false);
//		}
//
//		SocialEconomyEntityDetailsLayout demoEntityFormLayout = this.applicationContext.getBean(SocialEconomyEntityDetailsLayout.class);
//		demoEntityFormLayout.setCaption(this.messageSource.getMessage("form.basic.info"));
//		demoEntityFormLayout.setModelTabSheet(this.modelTabSheet);
//		demoEntityFormLayout.init(model, true);
//
//		this.modelTabSheet.addFormTab(demoEntityFormLayout).setCaption(demoEntityFormLayout.getCaption());
//
//		List<Layout> relatedForms = demoEntityFormLayout.getRelatedForms();
//		if (relatedForms != null) {
//			for (Layout relatedForm : relatedForms) {
//				String tabLabel = "";
//				if (ModelFormLayout.class.isAssignableFrom(relatedForm.getClass())) {
//					tabLabel = ((ModelFormLayout<?>) relatedForm).getName();
//				} else {
//					tabLabel = relatedForm.getCaption();
//				}
//				this.modelTabSheet.addFormTab(relatedForm).setCaption(tabLabel);
//			}
//		}
//
////		this.addComponent(this.modelTabSheet);
////		this.setComponentAlignment(this.modelTabSheet, Alignment.TOP_LEFT);
////		this.setExpandRatio(this.modelTabSheet, 0.7f);
//	}

}
