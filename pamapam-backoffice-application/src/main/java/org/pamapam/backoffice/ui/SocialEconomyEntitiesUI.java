package org.pamapam.backoffice.ui;

import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.pamapam.backoffice.layout.details.see.SocialEconomyEntityTableLayout;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

@SpringUI(path = "/socialEconomyEntities")
@Theme("demoTheme")
@Widgetset("org.pamapam.backoffice.Widgetset")
public class SocialEconomyEntitiesUI extends UI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private BackofficeApplicationDef backofficeApplicationDef;

	private SocialEconomyEntityTableLayout tableLayout;

	@SuppressWarnings("unchecked")
	@Override
	protected void init(VaadinRequest request) {
		this.setNavigator(null);

		this.tableLayout = this.applicationContext.getBean(SocialEconomyEntityTableLayout.class);
		this.tableLayout.initialize((CrudLayoutDef<SocialEconomyEntity>) this.backofficeApplicationDef.getLayoutDef(SocialEconomyEntity.class));

		this.setContent(this.tableLayout);
		this.setSizeFull();
	}

}
