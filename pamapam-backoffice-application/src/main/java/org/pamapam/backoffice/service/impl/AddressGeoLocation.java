package org.pamapam.backoffice.service.impl;

public class AddressGeoLocation {

	private double latitude;
	private double longitude;
	private String street;
	private String country;
	private String macroregion;
	private String region;
	private String localadmin;
	private String label;

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMacroregion() {
		return this.macroregion;
	}

	public void setMacroregion(String macroregion) {
		this.macroregion = macroregion;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLocaladmin() {
		return this.localadmin;
	}

	public void setLocaladmin(String localadmin) {
		this.localadmin = localadmin;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
