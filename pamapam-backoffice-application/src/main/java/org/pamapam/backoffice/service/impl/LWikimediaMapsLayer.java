package org.pamapam.backoffice.service.impl;

import org.vaadin.addon.leaflet.LTileLayer;

public class LWikimediaMapsLayer extends LTileLayer {

	private static final long serialVersionUID = 1L;

	public LWikimediaMapsLayer() {
		super("https://a.tile.openstreetmap.org/{z}/{x}/{y}.png");
		this.setAttributionString("Map data © &lt;a href=\\\"https://www.openstreetmap.org/copyright\\\"&gt;OpenStreetMap&lt;/a&gt; contributors");
	}

}