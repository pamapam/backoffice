package org.pamapam.backoffice.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;

import net.sf.junidecode.Junidecode;

@Service
public class MapzenService {

	private static final String API_KEY = "mapzen-6V1oZmE";
	private static final String BASE_URL = "http://search.mapzen.com/v1/autocomplete";

	// ...	Catalunya boundaries.

	private static final String MIN_LON = "0.16";
	private static final String MIN_LAT = "40.52";
	private static final String MAX_LON = "3.34";
	private static final String MAX_LAT = "42.87";

	// ...	Country boundary.

	private static final String COUNTRY = "ESP";

	public List<AddressGeoLocation> searchLocations(String address) throws EncoderException, JsonParseException, JsonMappingException, IOException {
		String transliteratedAddress = Junidecode.unidecode(address);
		StringUtils.replaceEach(transliteratedAddress, new String[] { ",", "." }, new String[] { " ", " " });
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder
			.append(BASE_URL).append("?")
			.append("api_key=").append(API_KEY).append("&")
			.append("text=").append(transliteratedAddress).append("&")
			.append("boundary.country=").append(COUNTRY);
		String url = urlBuilder.toString();

		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(url, String.class);

		List<AddressGeoLocation> addressGeoLocations = new ArrayList<>();

		MappingJsonFactory jsonFactory = new MappingJsonFactory();
		JsonParser jsonParser = jsonFactory.createParser(result);
		JsonNode rootNode = jsonParser.readValueAsTree();
		JsonNode featuresNode = rootNode.get("features");
		Iterator<JsonNode> featuresNodeIterator = featuresNode.elements();
		while (featuresNodeIterator.hasNext()) {
			JsonNode featureNode = featuresNodeIterator.next();
			addressGeoLocations.add(this.createAddressGeoLocation(featureNode));
		}

		return addressGeoLocations;
	}

	private AddressGeoLocation createAddressGeoLocation(JsonNode featureNode) {
		AddressGeoLocation addressGeoLocation = new AddressGeoLocation();
		if (featureNode.has("geometry")) {
			JsonNode geometryNode = featureNode.get("geometry");
			JsonNode coordinatesNode = geometryNode.get("coordinates");
			addressGeoLocation.setLongitude(coordinatesNode.get(0).asDouble());
			addressGeoLocation.setLatitude(coordinatesNode.get(1).asDouble());
		}
		if (featureNode.has("properties")) {
			JsonNode propertiesNode = featureNode.get("properties");
			if (propertiesNode.has("country")) {
				addressGeoLocation.setCountry(propertiesNode.get("country").asText());
			}
			if (propertiesNode.has("street")) {
				addressGeoLocation.setStreet(propertiesNode.get("street").asText());
			}
			if (propertiesNode.has("macroregion")) {
				addressGeoLocation.setMacroregion(propertiesNode.get("macroregion").asText());
			}
			if (propertiesNode.has("region")) {
				addressGeoLocation.setRegion(propertiesNode.get("region").asText());
			}
			if (propertiesNode.has("localadmin")) {
				addressGeoLocation.setLocaladmin(propertiesNode.get("localadmin").asText());
			}
			if (propertiesNode.has("label")) {
				addressGeoLocation.setLabel(propertiesNode.get("label").asText());
			}
		}
		return addressGeoLocation;
	}
}
