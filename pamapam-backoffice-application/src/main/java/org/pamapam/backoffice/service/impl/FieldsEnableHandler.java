package org.pamapam.backoffice.service.impl;

import com.vaadin.ui.Component;
import org.pamapam.backoffice.layout.details.see.SocialEconomyEntityDetailsLayout;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 *
 */
@Service
public class FieldsEnableHandler {
    /**
     * Disables all buttons that do not have to be accessible when the point being edited is the host of a matrix point (has mainOffice).
     *
     * @param socialEconomyEntity
     * @param socialEconomyEntityDetailsLayout
     */
    public void disableSocialEconomyPanelFields(SocialEconomyEntity socialEconomyEntity, SocialEconomyEntityDetailsLayout socialEconomyEntityDetailsLayout) {
        if (socialEconomyEntity.getMainOffice() != null && !socialEconomyEntity.isMainOffice()) {
            socialEconomyEntityDetailsLayout.getSectorCcaeField().setEnabled(false);
            socialEconomyEntityDetailsLayout.getLegalFormField().setEnabled(false);
            socialEconomyEntityDetailsLayout.getInvoicingField().setEnabled(false);
            socialEconomyEntityDetailsLayout.getSocialEconomyNetworksField().setEnabled(false);
            socialEconomyEntityDetailsLayout.getSocialEconomyNetworkTagsField().setEnabled(false);
            socialEconomyEntityDetailsLayout.getIdeologicalIdentificationField().setEnabled(false);
            socialEconomyEntityDetailsLayout.getXesBalanceField().setEnabled(false);
	    if (socialEconomyEntityDetailsLayout.getXesBalanceUrlField() != null) {
	            socialEconomyEntityDetailsLayout.getXesBalanceUrlField().setEnabled(false);
	    }
	    this.disableAllPersonRoleRows(socialEconomyEntityDetailsLayout.getPersonRoleRowFields());
            socialEconomyEntityDetailsLayout.getIntercoopAddLinkButton().setEnabled(false);
        }
    }

    private void disableAllPersonRoleRows(ArrayList<Component> personRoleRowFields) {
        personRoleRowFields.forEach(field -> field.setEnabled(false));
    }

    public void disableEntityEvaluationPanelsFields(SocialEconomyEntity socialEconomyEntity, SocialEconomyEntityDetailsLayout socialEconomyEntityDetailsLayout) {
        if (socialEconomyEntity.getMainOffice() != null) {
            socialEconomyEntityDetailsLayout.getEntityEvaluationPanels().forEach((evaluationId, panel) -> panel.setEnabled(false));
        }
    }
}
