package org.pamapam.backoffice;

import org.jamgo.BackofficeApplication;
import org.jamgo.model.filter.IgnoreDuringScan;
import org.jamgo.services.session.SessionContext;
import org.jamgo.services.session.SessionContextImpl;
import org.jamgo.ui.layout.ContentLayout;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.pamapam.backoffice.layout.PamapamContentLayout;
import org.pamapam.backoffice.layout.utils.PamapamComponentFactory;
import org.pamapam.services.config.PamapamBackofficeServicesConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.vaadin.spring.annotation.UIScope;

@Configuration
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@Import(value = { PamapamBackofficeServicesConfig.class })
@ComponentScan(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	org.jamgo.layout.PackageMarker.class,
	org.pamapam.services.PackageMarker.class,
	org.pamapam.repository.PackageMarker.class,
	org.pamapam.web.services.PackageMarker.class,
	org.pamapam.backoffice.PamapamBackofficeApplication.class
}, excludeFilters = @Filter(IgnoreDuringScan.class))
@EntityScan(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	org.pamapam.model.PackageMarker.class,
	org.pamapam.backoffice.PamapamBackofficeApplication.class
})
@EnableJpaRepositories(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	org.pamapam.repository.PackageMarker.class
})
public class PamapamBackofficeApplication extends BackofficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PamapamBackofficeApplication.class, args);
	}

	@Bean
	@Override
	public JamgoComponentFactory jamgoComponentFactory() {
		return new PamapamComponentFactory();
	}

	@Bean
	@UIScope
	@Override
	public ContentLayout contentLayout() {
		return new PamapamContentLayout();
	}
	
	@Bean
	public SessionContext sessionContext() {
		return new SessionContextImpl();
	}

//	@Bean
//	public FreeMarkerConfigurer freeMarkerConfigurer() {
//		FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
//		freemarker.template.Configuration freeMarkerConfiguration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_28);
//		freeMarkerConfiguration.setTemplateLoader(new ClassTemplateLoader(FreeMarkerConfigurer.class, "/templates"));
//		configurer.setConfiguration(freeMarkerConfiguration);
//		return configurer;
//	}
}
