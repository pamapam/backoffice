package org.pamapam.backoffice;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;
import org.vaadin.spring.http.HttpService;
import org.vaadin.spring.security.shared.VaadinUrlAuthenticationSuccessHandler;
import org.vaadin.spring.security.web.VaadinRedirectStrategy;

public class PamapamAuthenticationSuccessHandler extends VaadinUrlAuthenticationSuccessHandler {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private RequestCache requestCache = new HttpSessionRequestCache();

	public PamapamAuthenticationSuccessHandler(HttpService http, VaadinRedirectStrategy redirectStrategy, String defaultTargetUrl) {
		super(http, redirectStrategy, defaultTargetUrl);
	}

	@Override
	public void onAuthenticationSuccess(Authentication authentication) throws Exception {

		HttpServletRequest request = this.http.getCurrentRequest();
		HttpServletResponse response = this.http.getCurrentResponse();

		SavedRequest savedRequest = this.requestCache.getRequest(request, response);

		if (savedRequest == null) {
			super.onAuthenticationSuccess(authentication);

			return;
		}
		String targetUrlParameter = this.getTargetUrlParameter();
		if (this.isAlwaysUseDefaultTargetUrl() || (targetUrlParameter != null && StringUtils.hasText(request.getParameter(targetUrlParameter)))) {
			this.requestCache.removeRequest(request, response);
			super.onAuthenticationSuccess(authentication);

			return;
		}

		this.clearAuthenticationAttributes();

		// Use the DefaultSavedRequest URL
		String targetUrl = savedRequest.getRedirectUrl();
		this.logger.debug("Redirecting to saved request redirect url: " + targetUrl);
		this.redirectStrategy.sendRedirect(targetUrl);
	}

	public void setRequestCache(RequestCache requestCache) {
		this.requestCache = requestCache;
	}

	public String getSavedRedirectUrl() {
		HttpServletRequest request = this.http.getCurrentRequest();
		HttpServletResponse response = this.http.getCurrentResponse();

		SavedRequest savedRequest = this.requestCache.getRequest(request, response);
		return Optional.ofNullable(savedRequest).map(o -> o.getRedirectUrl()).orElse("");
	}
}
