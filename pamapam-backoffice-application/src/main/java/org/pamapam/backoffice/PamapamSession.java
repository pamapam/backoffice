package org.pamapam.backoffice;

import javax.annotation.PostConstruct;

import org.pamapam.model.PamapamUser;
import org.pamapam.services.PamapamUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class PamapamSession {

	@Autowired
	private PamapamUserService userService;

	private PamapamUser currentUser;

	private String savedRedirectUrl;

	@PostConstruct
	public void init() {
		this.currentUser = this.userService.getCurrentUser();
	}

	public PamapamUser getCurrentUser() {
		return this.currentUser;
	}

	public String getSavedRedirectUrl() {
		return this.savedRedirectUrl;
	}

	public void setSavedRedirectUrl(final String savedRedirectUrl) {
		this.savedRedirectUrl = savedRedirectUrl;
	}

}
