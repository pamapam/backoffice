package org.pamapam.backoffice.layout.details;

import org.jamgo.model.entity.RoleImpl;
import org.jamgo.model.repository.RoleRepository;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextArea;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.MailTemplate;
import org.pamapam.model.NotificationConfig;
import org.pamapam.model.enums.EntityAction;
import org.pamapam.model.enums.NotificationType;
import org.pamapam.model.enums.UserEntityLinkType;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.repository.MailTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.CheckBoxGroup;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NotificationConfigDetailsLayout extends CrudDetailsLayout<NotificationConfig> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private MailTemplateRepository mailTemplateRepository;

	private CrudDetailsPanel panel;

	private TextField nameField;
	private ComboBox<NotificationType> notificationTypeField;
	private ListSelect<RoleImpl> rolesField;
	private ComboBox<UserEntityLinkType> userEntityLinkTypeField;
	private CheckBoxGroup<EntityAction> entityActionField;
	private ListSelect<EntityStatus> entityStatusesField;
	private JmgLocalizedTextField mailSubjectField;
//	private JmgLocalizedTextField mailTitleField;
	private JmgLocalizedTextArea mailTextField;
	private ComboBox<MailTemplate> templateField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		this.panel = this.componentFactory.newCrudDetailsPanel();
		this.panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = this.componentFactory.newTextField("notificationConfig.name");
		this.nameField.setWidth(50, Unit.PERCENTAGE);
		this.panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, NotificationConfig::getName, NotificationConfig::setName);

		this.notificationTypeField = this.componentFactory.newComboBox("notificationConfig.notificationType");
		this.notificationTypeField.setDataProvider(DataProvider.ofCollection(Lists.newArrayList(NotificationType.values())));
		this.notificationTypeField.setItemCaptionGenerator(item -> this.messageSource.getMessage("notificationConfig.notificationType." + item.name().toLowerCase()));
		this.notificationTypeField.setWidth(50, Unit.PERCENTAGE);
		this.notificationTypeField.addValueChangeListener(event -> {
			NotificationType notificationType = event.getValue();
			if (NotificationType.ENTITY_ACTION.equals(notificationType)) {
				this.panel.setVisible(this.userEntityLinkTypeField, true);
				this.panel.setVisible(this.entityActionField, true);
				this.panel.setVisible(this.entityStatusesField, true);
			} else {
				this.panel.setVisible(this.userEntityLinkTypeField, false);
				this.panel.setVisible(this.entityActionField, false);
				this.panel.setVisible(this.entityStatusesField, false);
			}
		});

		this.panel.addComponent(this.notificationTypeField);
		this.binder.bind(this.notificationTypeField, NotificationConfig::getNotificationType, NotificationConfig::setNotificationType);

//		ComboBox<Role> roleField = this.componentFactory.newComboBox("notificationConfig.role");
//		roleField.setDataProvider(DataProvider.ofCollection(this.roleRepository.findAll()));
//		roleField.setWidth(50, Unit.PERCENTAGE);
//		panel.addComponent(roleField);
//		this.binder.bind(roleField, NotificationConfig::getRole, NotificationConfig::setRole);

		this.rolesField = this.componentFactory.newListSelect("notificationConfig.roles");
		this.rolesField.setDataProvider(DataProvider.ofCollection(this.roleRepository.findAll()));
		this.rolesField.setRows(6);
		this.rolesField.setItemCaptionGenerator(item -> item.getRolename());
		this.rolesField.setWidth(50, Unit.PERCENTAGE);
		this.panel.addComponent(this.rolesField);
		this.binder.bind(this.rolesField, NotificationConfig::getRoles, NotificationConfig::setRoles);

		this.userEntityLinkTypeField = this.componentFactory.newComboBox("notificationConfig.userEntityLinkType");
		this.userEntityLinkTypeField.setDataProvider(DataProvider.ofCollection(Lists.newArrayList(UserEntityLinkType.values())));
		this.userEntityLinkTypeField.setItemCaptionGenerator(item -> this.messageSource.getMessage("notificationConfig.userEntityLinkType." + item.name().toLowerCase()));
		this.userEntityLinkTypeField.setWidth(50, Unit.PERCENTAGE);
		this.panel.addComponent(this.userEntityLinkTypeField);
		this.binder.bind(this.userEntityLinkTypeField, NotificationConfig::getUserEntityLinkType, NotificationConfig::setUserEntityLinkType);

		this.entityActionField = this.componentFactory.newCheckBoxGroup("notificationConfig.entityAction");
		this.entityActionField.setDataProvider(DataProvider.ofCollection(Lists.newArrayList(EntityAction.values())));
		this.entityActionField.setItemCaptionGenerator(item -> this.messageSource.getMessage("notificationConfig.entityAction." + item.name().toLowerCase()));
		this.entityActionField.setWidth(50, Unit.PERCENTAGE);
		this.panel.addComponent(this.entityActionField);
		this.binder.bind(this.entityActionField, NotificationConfig::getEntityActions, NotificationConfig::setEntityActions);

		this.entityStatusesField = this.componentFactory.newListSelect("notificationConfig.entityStatuses");
		this.entityStatusesField.setDataProvider(DataProvider.ofCollection(this.entityStatusRepository.findAll()));
		this.entityStatusesField.setRows(6);
		this.entityStatusesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.entityStatusesField.setWidth(50, Unit.PERCENTAGE);
		this.panel.addComponent(this.entityStatusesField);
		this.binder.bind(this.entityStatusesField, NotificationConfig::getEntityStatuses, NotificationConfig::setEntityStatuses);

		this.mailSubjectField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("notificationConfig.mailSubject").setWidth(50, Unit.PERCENTAGE).build();
		this.panel.addComponent(this.mailSubjectField);
		this.binder.bind(this.mailSubjectField, NotificationConfig::getMailSubject, NotificationConfig::setMailSubject);

//		this.mailTitleField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("notificationConfig.mailTitle").setWidth(50, Unit.PERCENTAGE).build();
//		this.panel.addComponent(this.mailTitleField);
//		this.binder.bind(this.mailTitleField, NotificationConfig::getMailTitle, NotificationConfig::setMailTitle);

		this.mailTextField = (JmgLocalizedTextArea)this.componentBuilderFactory.createLocalizedTextAreaBuilder().setCaption("notificationConfig.mailText").setWidth(100, Unit.PERCENTAGE).build();
		this.mailTextField.setMaxLength(2048);
		this.panel.addComponent(this.mailTextField);
		this.binder.bind(this.mailTextField, NotificationConfig::getMailText, NotificationConfig::setMailText);

		this.templateField = this.componentFactory.newComboBox("notificationConfig.mailTemplate");
		this.templateField.setDataProvider(DataProvider.ofCollection(this.mailTemplateRepository.findAll()));
		this.templateField.setWidth(50, Unit.PERCENTAGE);
		this.templateField.setItemCaptionGenerator(item -> item.getName());
		this.panel.addComponent(this.templateField);
		this.binder.bind(this.templateField, NotificationConfig::getMailTemplate, NotificationConfig::setMailTemplate);

		return this.panel;
	}

	@Override
	public void updateFields(Object object) {
		NotificationConfig notificationConfig = (NotificationConfig) object;
		super.updateFields(notificationConfig);
		if ((notificationConfig == null) || NotificationType.ENTITY_ACTION.equals(notificationConfig.getNotificationType())) {
			this.panel.setVisible(this.userEntityLinkTypeField, true);
			this.panel.setVisible(this.entityActionField, true);
			this.panel.setVisible(this.entityStatusesField, true);
		} else {
			this.panel.setVisible(this.userEntityLinkTypeField, false);
			this.panel.setVisible(this.entityActionField, false);
			this.panel.setVisible(this.entityStatusesField, false);
		}
	}

	@Override
	protected Class<NotificationConfig> getTargetObjectClass() {
		return NotificationConfig.class;
	}

}
