package org.pamapam.backoffice.layout.details;

import com.google.common.io.ByteStreams;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.server.Setter;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.apache.tika.Tika;
import org.jamgo.layout.details.UserDetailsLayout;
import org.jamgo.model.entity.*;
import org.jamgo.model.repository.UserRepository;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextArea;
import org.jamgo.vaadin.ui.JmgNotification;
import org.pamapam.backoffice.layout.utils.PamapamComponentFactory;
import org.pamapam.model.Community;
import org.pamapam.model.PamapamUser;
import org.pamapam.repository.CommunityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import server.droporchoose.UploadComponent;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PamapamUserDetailsLayout extends UserDetailsLayout<PamapamUser> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private UserRepository<PamapamUser> userRepository;

	private JmgLocalizedTextArea descriptionField;
	private UploadComponent pictureUploadField;
	private Image pictureField;
	private ProgressBar pictureProgressBar;
	private ComboBox<Province> provinceField;
	private ComboBox<Region> regionField;
	private ComboBox<Town> townField;
	private ComboBox<District> districtField;
	private ComboBox<Neighborhood> neighborhoodField;
	private TwinColSelect<Community> communitiesField;
	private TextField twitterField;
	private TextField facebookField;
	private TextField instagramField;
	private TextField gitlabField;
	private TextField fediverseField;
	//	private CheckBox privacyConsentField;
	private DateField privacyConsentDateField;
	private DateField newsletterConsentDateField;

	private Path uploadedPicturePath;

	@Autowired
	private CommunityRepository communityRepository;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = super.createMainPanel();

		if (!VaadinService.getCurrentRequest().isUserInRole(RoleImpl.ROLE_ADMIN) && !VaadinService.getCurrentRequest().isUserInRole("ROLE_coordination")) {
			this.usernameField.setEnabled(false);
			this.enabledField.setEnabled(false);
			this.rolesGroup.setEnabled(false);
		}

		// ...	Remove roles component to add it later to the end.
		panel.removeComponent(this.rolesGroup);

		this.usernameField.addBlurListener(listener -> {
			final User user = this.userRepository.findByUsername(this.usernameField.getValue());
			if ((user != null) && !user.equals(this.targetObject)) {
				JmgNotification.show(this.messageSource.getMessage("validation.user.username.exists", new String[] { this.usernameField.getValue() }), Type.HUMANIZED_MESSAGE);
				this.usernameField.clear();
			}
		});

		this.emailField.addBlurListener(listener -> {
			final User user = this.userRepository.findByEmail(this.emailField.getValue());
			if ((user != null) && !user.equals(this.targetObject)) {
				JmgNotification.show(this.messageSource.getMessage("validation.user.email.exists", new String[] { this.emailField.getValue() }), Type.HUMANIZED_MESSAGE);
				this.emailField.clear();
			}
		});

		this.addPictureLayout(panel);
		this.addDescriptionField(panel);
		this.addTerritoryFields(panel);
		this.addCommunityField(panel);
		this.addTwitterField(panel);
		this.addFacebookField(panel);
		this.addInstagramField(panel);
		this.addGitlabField(panel);
		this.addFediverseField(panel);
		this.addPrivacyConsentField(panel);
		this.addNewsletterConsentField(panel);

		// ...	Adds roles component to the end.
		this.rolesGroup.setCaption(this.messageSource.getMessage("user.roles"));
		panel.addComponent(this.rolesGroup);

		return panel;
	}

	private void addPictureLayout(final CrudDetailsPanel panel) {
		final HorizontalLayout pictureLayout = this.componentFactory.newHorizontalLayout();
		pictureLayout.setCaption(this.messageSource.getMessage("user.picture"));

		this.pictureField = this.componentFactory.newImage();
		this.pictureField.setHeight(200, Unit.PIXELS);
		pictureLayout.addComponent(this.pictureField);

		final VerticalLayout pictureUploadLayout = this.componentFactory.newVerticalLayout();

		this.pictureUploadField = new UploadComponent(this::pictureUploadReceived);
		this.pictureUploadField.setStartedCallback(this::uploadStarted);
		this.pictureUploadField.setProgressCallback(this::uploadProgress);
		this.pictureUploadField.setFailedCallback(this::uploadFailed);
		this.pictureUploadField.getDropTextLabel().setValue(this.messageSource.getMessage("user.picture.dropFile"));
		this.pictureUploadField.getChoose().setButtonCaption(this.messageSource.getMessage("user.picture.chooseFile"));
		pictureUploadLayout.addComponent(this.pictureUploadField);
		this.pictureProgressBar = new ProgressBar();
		this.pictureProgressBar.setVisible(false);
		pictureUploadLayout.addComponent(this.pictureProgressBar);
		pictureLayout.addComponent(pictureUploadLayout);

		panel.addComponent(pictureLayout);
	}

	private void pictureUploadReceived(final String fileName, final Path path) {
		this.uploadedPicturePath = path;
		this.pictureProgressBar.setVisible(false);
		this.showUploadedImage(fileName);
	}

	private void uploadStarted(final String fileName) {
		this.pictureProgressBar.setVisible(true);
	}

	private void uploadProgress(final String fileName, final long readBytes, final long contentLength) {
		if (readBytes > 0) {
			this.pictureProgressBar.setValue((float) contentLength / readBytes);
		}
	}

	private void uploadFailed(final String fileName, final Path file) {
		this.pictureProgressBar.setVisible(false);
	}

	private void addDescriptionField(final CrudDetailsPanel panel) {
		this.descriptionField = (JmgLocalizedTextArea) this.componentBuilderFactory.createLocalizedTextAreaBuilder().setCaption("user.description").setWidth(100, Unit.PERCENTAGE).build();
		this.descriptionField.setMaxLength(2048);
		this.binder.bind(this.descriptionField, PamapamUser::getDescription, PamapamUser::setDescription);
		panel.addComponent(this.descriptionField);
	}

	private void addTwitterField(final CrudDetailsPanel panel) {
		this.twitterField = this.createTextField("user.twitter", this.binder, PamapamUser::getTwitter, PamapamUser::setTwitter, false);
		panel.addComponent(this.twitterField);
	}

	private void addFacebookField(final CrudDetailsPanel panel) {
		this.facebookField = this.createTextField("user.facebook", this.binder, PamapamUser::getFacebook, PamapamUser::setFacebook, false);
		panel.addComponent(this.facebookField);
	}

	private void addInstagramField(final CrudDetailsPanel panel) {
		this.instagramField = this.createTextField("user.instagram", this.binder, PamapamUser::getInstagram, PamapamUser::setInstagram, false);
		panel.addComponent(this.instagramField);
	}

	private void addGitlabField(final CrudDetailsPanel panel) {
		this.gitlabField = this.createTextField("user.gitlab", this.binder, PamapamUser::getGitlab, PamapamUser::setGitlab, false);
		panel.addComponent(this.gitlabField);
	}

	private void addFediverseField(final CrudDetailsPanel panel) {
		this.fediverseField = this.createTextField("user.fediverse", this.binder, PamapamUser::getFediverse, PamapamUser::setFediverse, false);
		panel.addComponent(this.fediverseField);
	}

	private void addPrivacyConsentField(final CrudDetailsPanel panel) {
		this.privacyConsentDateField = this.componentFactory.newDateField("user.privacyConsent.date");
		this.privacyConsentDateField.setDateFormat("dd/MM/yyyy");
		this.binder.forField(this.privacyConsentDateField)
			.withConverter(new LocalDateToDateConverter())
			.bind(PamapamUser::getPrivacyConsentDate, PamapamUser::setPrivacyConsentDate);
		panel.addComponent(this.privacyConsentDateField);
	}

	private void addNewsletterConsentField(final CrudDetailsPanel panel) {
		this.newsletterConsentDateField = this.componentFactory.newDateField("user.newsletterConsent.date");
		this.newsletterConsentDateField.setDateFormat("dd/MM/yyyy");
		this.binder.forField(this.newsletterConsentDateField)
			.withConverter(new LocalDateToDateConverter())
			.bind(PamapamUser::getNewsletterConsentDate, PamapamUser::setNewsletterConsentDate);
		panel.addComponent(this.newsletterConsentDateField);
	}

	private void addTerritoryFields(final CrudDetailsPanel panel) {
		this.createNeighborhoodField();
		this.createDistrictField();
		this.createTownField();
		this.createRegionField();
		this.createProvinceField();

		panel.addComponent(this.provinceField);
		panel.addComponent(this.regionField);
		panel.addComponent(this.townField);
		panel.addComponent(this.districtField);
		panel.addComponent(this.neighborhoodField);
	}

	private void createNeighborhoodField() {
		this.neighborhoodField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Neighborhood.class,
			null,
			"user.neighborhood",
			(ValueProvider<Neighborhood, Long>) item -> item.getId(),
			null);
		this.binder.bind(this.neighborhoodField, PamapamUser::getNeighborhood, PamapamUser::setNeighborhood);
	}

	private void createDistrictField() {
		this.districtField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			District.class,
			this.neighborhoodField,
			"user.district",
			(ValueProvider<District, Long>) item -> item.getId(),
			(ValueProvider<Neighborhood, District>) neighborhood -> neighborhood.getDistrict());
		this.binder.bind(this.districtField, PamapamUser::getDistrict, PamapamUser::setDistrict);
	}

	private void createTownField() {
		this.townField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Town.class,
			this.districtField,
			"user.town",
			(ValueProvider<Town, Long>) item -> item.getId(),
			(ValueProvider<District, Town>) district -> district.getTown());
		this.binder.bind(this.townField, PamapamUser::getTown, PamapamUser::setTown);
	}

	private void createRegionField() {
		this.regionField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Region.class,
			this.townField,
			"user.region",
			(ValueProvider<Region, Long>) item -> item.getId(),
			(ValueProvider<Town, Region>) town -> town.getRegion());
		this.binder.bind(this.regionField, PamapamUser::getRegion, PamapamUser::setRegion);
	}

	private void createProvinceField() {
		this.provinceField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Province.class,
			this.regionField,
			"user.province",
			null,
			(ValueProvider<Region, Province>) region -> region.getProvince());
		this.binder.bind(this.provinceField, PamapamUser::getProvince, PamapamUser::setProvince);
	}

	private void addCommunityField(final CrudDetailsPanel panel) {
		final List<Community> allCommunities = this.communityRepository.findAll();
		Collections.sort(allCommunities, Community.NAME_ORDER);

		this.communitiesField = this.componentFactory.newTwinColSelect("user.communities");
		this.communitiesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.communitiesField.setWidth(100, Unit.PERCENTAGE);
		this.communitiesField.setItems(allCommunities);

		this.binder.bind(this.communitiesField, PamapamUser::getCommunities, PamapamUser::setCommunities);
		panel.addComponent(this.communitiesField);
	}

	@Override
	public boolean hasChanges() {
		if (super.hasChanges()) {
			return true;
		}

		if (this.targetObject == null) {
			return false;
		}

		if (this.uploadedPicturePath != null) {
			return true;
		}

		return false;
	}

	@Override
	public void updateFields(final Object object) {
		final PamapamUser pamapamUser = (PamapamUser) object;
		super.updateFields(pamapamUser);

		this.uploadedPicturePath = null;

		if (pamapamUser.getCommunities() != null) {
			this.communitiesField.setValue(pamapamUser.getCommunities());
		}

		if (pamapamUser.getPicture() != null) {
			final byte[] pictureBytes = pamapamUser.getPicture().getContents();
			final InputStream pictureInputStream = new ByteArrayInputStream(pictureBytes);
			this.showImage(pictureInputStream, pamapamUser.getPicture().getFileName());
		} else {
			this.pictureField.setSource(new ThemeResource("images/placeholder.png"));
		}
	}

	@Override
	public void updateTargetObject() throws ValidationException {
		super.updateTargetObject();

		if (this.uploadedPicturePath != null) {
			byte[] pictureContents = null;
			String mimeType = "";
			try {
				final FileInputStream pictureInputStream = new FileInputStream(this.uploadedPicturePath.toFile());
				pictureContents = ByteStreams.toByteArray(pictureInputStream);
				mimeType = (new Tika()).detect(pictureInputStream);
				pictureInputStream.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (pictureContents != null) {
				final BinaryResource picture = new BinaryResource(pictureContents);
				picture.setMimeType(mimeType);
				picture.setFileLength(picture.getContents().length);
				picture.setFileName(this.uploadedPicturePath.getFileName().toString());
				this.targetObject.setPicture(picture);
			}
		}
		this.uploadedPicturePath = null;
	}

	private void showUploadedImage(final String fileName) {
		if (this.uploadedPicturePath != null) {
			try {
				final FileInputStream inputStream = new FileInputStream(this.uploadedPicturePath.toFile());
				this.showImage(inputStream, fileName);
			} catch (final Exception e) {
				// TODO: handle exception?
			}
		}
	}

	private void showImage(final InputStream imageInputStream, final String fileName) {
		final StreamResource resource = new StreamResource(() -> imageInputStream, fileName);
		this.pictureField.setSource(resource);
	}

	@Override
	protected Class<PamapamUser> getTargetObjectClass() {
		return PamapamUser.class;
	}

	// ...	TODO: move method to componentFactory
	private TextField createTextField(
		final String captionId,
		final Binder<PamapamUser> binder,
		final ValueProvider<PamapamUser, String> getter,
		final Setter<PamapamUser, String> setter,
		final boolean required) {
		final TextField field = this.componentFactory.newTextField(captionId);
		field.setSizeFull();
		field.setWidth(50, Unit.PERCENTAGE);
		if (binder != null) {
			binder.bind(field, getter, setter);
		}
		return field;
	}

}
