package org.pamapam.backoffice.layout.details;

import javax.annotation.PostConstruct;

import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.jamgo.vaadin.ui.JmgLocalizedTextArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CriterionAnswerDetailsLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected JamgoComponentFactory componentFactory;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;

	private JmgLocalizedTextArea answerField;
	private Label errorLabel;

	@Autowired
	protected LocalizedMessageService messageSource;

	@PostConstruct
	public void init() {
		this.setSpacing(true);
		this.setMargin(true);
		this.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

		this.answerField = (JmgLocalizedTextArea)this.componentBuilderFactory.createLocalizedTextAreaBuilder().setCaption("criterion.description").setWidth(50, Unit.PERCENTAGE).build();
		this.answerField.setWidth(50, Unit.PERCENTAGE);

		this.errorLabel = this.componentBuilderFactory.createLabelBuilder().build();
		this.errorLabel.setVisible(false);
		this.errorLabel.setStyleName("error-label");
		Button saveButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.save").build();
		saveButton.addClickListener(event -> {
			if ((this.getParent() != null) && (this.getParent() instanceof Window)) {
				((Window) this.getParent()).close();
			}
		});

		FormLayout passwordFieldsLayout = this.componentFactory.newFormLayout();
		passwordFieldsLayout.setSpacing(true);
		passwordFieldsLayout.setMargin(false);
		passwordFieldsLayout.setWidthUndefined();
		passwordFieldsLayout.addComponents(this.answerField);
		this.addComponents(passwordFieldsLayout, this.errorLabel, saveButton);

	}

	@Override
	public void attach() {
		super.attach();

//		this.oldPasswordField.clear();
//		this.newPasswordField.clear();
//		this.newPasswordRepeatedField.clear();
//		this.errorLabel.setVisible(false);
//		this.oldPasswordField.setVisible(!this.passwordField.isEmpty());
	}

	public void setPasswordField(PasswordField passwordField) {
//		this.passwordField = passwordField;
	}
}
