package org.pamapam.backoffice.layout.search;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.jamgo.model.entity.District;
import org.jamgo.model.entity.Neighborhood;
import org.jamgo.model.entity.Province;
import org.jamgo.model.entity.Region;
import org.jamgo.model.entity.RoleImpl;
import org.jamgo.model.entity.Town;
import org.jamgo.model.repository.RoleRepository;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudDetailsPanel.Format;
import org.jamgo.ui.layout.crud.CrudFormLayout;
import org.jamgo.ui.layout.crud.CrudSearchLayout;
import org.jamgo.ui.layout.utils.AdvancedFileDownloader;
import org.pamapam.backoffice.layout.utils.PamapamComponentFactory;
import org.pamapam.model.Community;
import org.pamapam.model.PamapamUser;
import org.pamapam.repository.CommunityRepository;
import org.pamapam.repository.search.PamapamUserSearch;
import org.pamapam.repository.search.PamapamUserSpecification;
import org.pamapam.services.PamapamUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PamapamUserSearchLayout extends CrudSearchLayout<PamapamUser, PamapamUserSearch> {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(PamapamUserSearchLayout.class);

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private CommunityRepository communityRepository;
	@Autowired
	private PamapamUserService pamapamUserService;

	private Button exportButton;

	@PostConstruct
	private void init() {
		this.searchSpecification = this.applicationContext.getBean(PamapamUserSpecification.class);
	}

	@Override
	protected List<Button> createActionButtons() {
		List<Button> buttons = super.createActionButtons();
		this.exportButton = this.componentBuilderFactory.createButtonBuilder().build();
		this.exportButton.setCaption(this.messageSource.getMessage("action.export"));
		AdvancedFileDownloader downloader = new AdvancedFileDownloader();
		downloader.addAdvancedDownloaderListener(downloadEvent -> {
			try {
				String filePath = this.doExport();
				downloader.setFilePath(filePath);
			} catch (Exception e) {
				logger.error("", e);
				Notification.show(this.messageSource.getMessage("message.generic.error"), Notification.Type.ERROR_MESSAGE);
			}
		});
		downloader.extend(this.exportButton);
		buttons.add(this.exportButton);
		return buttons;
	}

	private String doExport() {
		String exportFilePath = null;
		try {
			exportFilePath = this.pamapamUserService.exportToCsv(this.searchSpecification);
		} catch (IOException e) {
			logger.error("", e);
		}
		return exportFilePath;
	}

	@Override
	protected CrudDetailsPanel createPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel(Format.Vertical);
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		TextField usernameField = this.componentFactory.newTextField("user.username");
		usernameField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(usernameField);
		this.binder.bind(usernameField, PamapamUserSearch::getUsername, PamapamUserSearch::setUsername);
		
		TextField nameField = this.componentFactory.newTextField("user.name");
		nameField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(nameField);
		this.binder.bind(nameField, PamapamUserSearch::getName, PamapamUserSearch::setName);
		
		TextField emailField = this.componentFactory.newTextField("user.email");
		emailField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(emailField);
		this.binder.bind(emailField, PamapamUserSearch::getEmail, PamapamUserSearch::setEmail);

		ListSelect<RoleImpl> rolesField = this.componentFactory.newListSelect("user.role");
		rolesField.setDataProvider(DataProvider.ofCollection(this.roleRepository.findAll()));
		rolesField.setRows(10);
		rolesField.setItemCaptionGenerator(item -> item.getRolename());
		rolesField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(rolesField);
		this.binder.bind(rolesField, PamapamUserSearch::getRoles, PamapamUserSearch::setRoles);

		CheckBox enabledField = this.componentFactory.newCheckBox("user.active");
		panel.addComponent(enabledField);
		this.binder.bind(enabledField, PamapamUserSearch::getEnabled, PamapamUserSearch::setEnabled);

		// ...	Neighborhood.
		ComboBox<Neighborhood> neighborhoodField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Neighborhood.class,
			null,
			null,
			(ValueProvider<Neighborhood, Long>) item -> item.getId(),
			null);
		neighborhoodField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(neighborhoodField, PamapamUserSearch::getNeighborhood, PamapamUserSearch::setNeighborhood);

		// ...	District.
		ComboBox<District> districtField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			District.class,
			neighborhoodField,
			null,
			(ValueProvider<District, Long>) item -> item.getId(),
			(ValueProvider<Neighborhood, District>) neighborhood -> neighborhood.getDistrict());
		districtField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(districtField, PamapamUserSearch::getDistrict, PamapamUserSearch::setDistrict);

		// ...	Town.
		ComboBox<Town> townField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Town.class,
			districtField,
			null,
			(ValueProvider<Town, Long>) item -> item.getId(),
			(ValueProvider<District, Town>) district -> district.getTown());
		townField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(townField, PamapamUserSearch::getTown, PamapamUserSearch::setTown);

		// ...	Region.
		ComboBox<Region> regionField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Region.class,
			townField,
			null,
			(ValueProvider<Region, Long>) item -> item.getId(),
			(ValueProvider<Town, Region>) town -> town.getRegion());
		regionField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(regionField, PamapamUserSearch::getRegion, PamapamUserSearch::setRegion);

		// ... Province.
		ComboBox<Province> provinceField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Province.class,
			regionField,
			null,
			null,
			(ValueProvider<Region, Province>) region -> region.getProvince());
		provinceField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(provinceField, PamapamUserSearch::getProvince, PamapamUserSearch::setProvince);

		VerticalLayout territoryLayout = this.componentFactory.newVerticalLayout();
		territoryLayout.setCaption(this.messageSource.getMessage("user.territory"));
		territoryLayout.setSizeUndefined();
		territoryLayout.setWidth(100, Unit.PERCENTAGE);
		territoryLayout.setMargin(false);
		territoryLayout.addComponent(provinceField);
		territoryLayout.addComponent(regionField);
		territoryLayout.addComponent(townField);
		territoryLayout.addComponent(districtField);
		territoryLayout.addComponent(neighborhoodField);
		panel.addComponent(territoryLayout);

		ListSelect<Community> communitiesField = this.componentFactory.newListSelect("user.communities");
		List<Community> communities = this.communityRepository.findAll();
		Collections.sort(communities, Community.NAME_ORDER);
		communitiesField.setDataProvider(DataProvider.ofCollection(communities));
		communitiesField.setRows(10);
		communitiesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		communitiesField.setWidth(100, Unit.PERCENTAGE);

		panel.addComponent(communitiesField);
		this.binder.bind(communitiesField, PamapamUserSearch::getCommunities, PamapamUserSearch::setCommunities);

		return panel;
	}

	@Override
	protected Class<PamapamUserSearch> getTargetObjectClass() {
		return PamapamUserSearch.class;
	}

//	public Consumer<CrudSearchLayout<PamapamUser, PamapamUserSearch>> getExportHandler() {
//		return this.exportHandler;
//	}

//	public void setExportHandler(Consumer<CrudSearchLayout<SocialEconomyEntity, SocialEconomyEntitySearch>> exportHandler) {
//		this.exportHandler = exportHandler;
//	}

}
