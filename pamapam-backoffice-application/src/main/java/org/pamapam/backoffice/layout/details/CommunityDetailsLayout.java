package org.pamapam.backoffice.layout.details;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.Community;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CommunityDetailsLayout extends CrudDetailsLayout<Community> {

	private static final long serialVersionUID = 1L;

	private JmgLocalizedTextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("community.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, Community::getName, Community::setName);

		return panel;
	}

	@Override
	protected Class<Community> getTargetObjectClass() {
		return Community.class;
	}

}
