package org.pamapam.backoffice.layout.component;

import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.pamapam.model.ModelAttributeDef;
import org.pamapam.model.enums.ModelAttributeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.vaadin.data.Binder;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ModelAttributeDefListGridField extends ListGridField<ModelAttributeDef> {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected JamgoComponentFactory componentFactory;

	public ModelAttributeDefListGridField(String caption) {
		super(caption);
	}

	@Override
	public ModelAttributeDef createEmptyItem() {
		return new ModelAttributeDef();
	}

	@Override
	protected void configureGridColumns() {
		Binder<ModelAttributeDef> attributeDefBinder = this.fieldsGrid.getEditor().getBinder();
		this.fieldsGrid.addColumn(ModelAttributeDef::getName)
			.setCaption(this.messageSource.getMessage("modelAttributeDef.name"))
			.setEditorBinding(attributeDefBinder
				.forField(new TextField())
				.bind(ModelAttributeDef::getName, ModelAttributeDef::setName))
			.setEditable(true);
		this.fieldsGrid.addColumn(ModelAttributeDef::getLabel)
			.setCaption(this.messageSource.getMessage("modelAttributeDef.label"))
			.setEditorBinding(attributeDefBinder
				.forField(new TextField())
				.bind(ModelAttributeDef::getLabel, ModelAttributeDef::setLabel))
			.setEditable(true);

		ComboBox<ModelAttributeType> typesCombo = this.componentFactory.newComboBox();
		typesCombo.setDataProvider(DataProvider.ofCollection(Lists.newArrayList(ModelAttributeType.values())));
		this.fieldsGrid.addColumn(ModelAttributeDef::getType)
			.setCaption(this.messageSource.getMessage("modelAttributeDef.type"))
			.setEditorBinding(attributeDefBinder
				.forField(typesCombo)
				.bind(ModelAttributeDef::getType, ModelAttributeDef::setType))
			.setEditable(true);
	}

	@Override
	protected void configureCaptions() {
		this.helpLabel.setCaption(this.messageSource.getMessage("modelDef.attributesDef.help"));
		this.newElementButton.setCaption(this.messageSource.getMessage("modelDef.attributesDef.new"));
		this.removeElementsButton.setCaption(this.messageSource.getMessage("modelDef.attributesDef.delete"));
		this.fieldsGrid.getEditor().setSaveCaption(this.messageSource.getMessage("action.save"));
		this.fieldsGrid.getEditor().setCancelCaption(this.messageSource.getMessage("action.cancel"));
	}
}