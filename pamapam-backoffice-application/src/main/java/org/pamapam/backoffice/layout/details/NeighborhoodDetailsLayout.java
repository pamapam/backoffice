package org.pamapam.backoffice.layout.details;

import org.jamgo.model.entity.District;
import org.jamgo.model.entity.Neighborhood;
import org.jamgo.model.repository.DistrictRepository;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.ComboBox;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NeighborhoodDetailsLayout extends CrudDetailsLayout<Neighborhood> {

	private static final long serialVersionUID = 1L;

	@Autowired
	DistrictRepository districtRepository;

	private JmgLocalizedTextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("location.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, Neighborhood::getName, Neighborhood::setName);

		ComboBox<District> districtField = this.componentFactory.newComboBox("neighborhood.district");
		districtField.setWidth(50, Unit.PERCENTAGE);
		districtField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		districtField.setItems(this.districtRepository.findAll());
		panel.addComponent(districtField);
		this.binder.bind(districtField, Neighborhood::getDistrict, Neighborhood::setDistrict);

		return panel;
	}

	@Override
	protected Class<Neighborhood> getTargetObjectClass() {
		return Neighborhood.class;
	}

}
