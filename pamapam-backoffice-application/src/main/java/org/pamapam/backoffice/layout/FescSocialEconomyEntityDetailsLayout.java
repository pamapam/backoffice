package org.pamapam.backoffice.layout;

import java.util.List;

import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.backoffice.layout.details.see.SocialEconomyEntityDetailsLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.vaadin.sliderpanel.SliderPanel;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;

@SpringComponent(value = "FescEntityDetailsLayout")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FescSocialEconomyEntityDetailsLayout extends SocialEconomyEntityDetailsLayout {

	private static final long serialVersionUID = 1L;

	@Override
	protected List<CrudDetailsPanel> createPanels() {
		final List<CrudDetailsPanel> panels = super.createPanels();
		panels.remove(this.customFieldsPanel);
		//panels.remove(this.entityEvaluationPanel);
		panels.add(this.fescPanel);
		return panels;
	}
	
	@Override
	protected List<Button> createActionButtons() {
		List<Button> buttons = super.createActionButtons();
		buttons.clear();
		buttons.add(this.okButton);
		return buttons;
	}

	@Override
	protected SliderPanel createNotesSlider() {
		return null;
	}
	
	@Override
	public void updateFields(final Object object) {
		super.updateFields(object);
		
		this.okButton.setCaption(this.messageSource.getMessage("action.save"));
		this.okButton.removeStyleNames(this.okButton.getStyleName().split(" "));
		this.okButton.setVisible(true);
	}
	
}
