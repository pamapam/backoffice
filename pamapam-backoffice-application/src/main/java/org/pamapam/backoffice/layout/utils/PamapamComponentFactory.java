package org.pamapam.backoffice.layout.utils;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.jamgo.model.entity.Territory;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.filter.IgnoreDuringScan;
import org.jamgo.services.DatasourceServices;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

@Component
@IgnoreDuringScan
public class PamapamComponentFactory extends JamgoComponentFactory {

	@Autowired
	protected DatasourceServices datasourceServices;

	public <S, T extends Territory> ComboBox<T> createTerritoryComboBox(
		final Class<T> modelClass,
		final ComboBox<S> relatedComboBox,
		final String captionKey,
		final ValueProvider<T, Long> initialValueProvider,
		final ValueProvider<S, T> relatedValueProvider) {
		List<T> allTerritories = null;
		try {
			allTerritories = this.datasourceServices.getAll(modelClass);
		} catch (final RepositoryForClassNotDefinedException e) {
			Notification.show(RepositoryForClassNotDefinedException.class.getName(), Type.ERROR_MESSAGE);
			allTerritories = new ArrayList<>();
		}
		Collections.sort(allTerritories, Territory.NAME_ORDER);

		final ComboBox<T> comboBox = this.newComboBox(captionKey);
		comboBox.setWidth(50, Unit.PERCENTAGE);
		comboBox.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		comboBox.setItems(allTerritories);

		if (relatedComboBox != null) {
			comboBox.addValueChangeListener(event -> {
				final T territory = event.getValue();
				@SuppressWarnings("unchecked")
				final ListDataProvider<S> dataProvider = (ListDataProvider<S>) relatedComboBox.getDataProvider();
				if (territory != null) {
					dataProvider.setFilterByValue(relatedValueProvider, territory);
				} else {
					dataProvider.setFilterByValue(item -> ((Territory) item).getId(), null);
				}
			});
		}
		@SuppressWarnings("unchecked")
		final ListDataProvider<T> dataProvider = (ListDataProvider<T>) comboBox.getDataProvider();
		if (initialValueProvider != null) {
			dataProvider.setFilterByValue(initialValueProvider, null);
		}
		return comboBox;
	}

	public Grid<SocialEconomyEntity> createEntitiesGrid() {
		return this.createEntitiesGrid(false, true);
	}

	public Grid<SocialEconomyEntity> createEntitiesGrid(final boolean showStatus) {
		return this.createEntitiesGrid(showStatus, false);
	}

	public Grid<SocialEconomyEntity> createEntitiesGrid(final boolean showStatus, final boolean showEntityScopes) {
		final Grid<SocialEconomyEntity> boxGrid = this.newGrid();
		boxGrid.setSelectionMode(SelectionMode.NONE);
		boxGrid.setSizeFull();
		boxGrid.setHeaderVisible(false);
		final Column<SocialEconomyEntity, String> dateColumn = boxGrid.addColumn(obj -> {
			final Date date = obj.getCurrentStatusDate();
			if (date != null) {
				return DateTimeFormatter.ISO_LOCAL_DATE.format(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			} else {
				return "";
			}
		});
		dateColumn.setMinimumWidthFromContent(true);
		dateColumn.setExpandRatio(0);
		if (showStatus) {
			final Column<SocialEconomyEntity, String> statusColumn = boxGrid.addColumn(obj -> obj.getEntityStatus().getName().getDefaultText());
			statusColumn.setMinimumWidth(105);
		}
		final Column<SocialEconomyEntity, String> nameColumn = boxGrid.addColumn(obj -> obj.getName());
		nameColumn.setMinimumWidthFromContent(false);
		nameColumn.setExpandRatio(3);
		final Column<SocialEconomyEntity, String> userColumn = boxGrid.addColumn(obj -> Optional.ofNullable(obj.getRegistryUser())
			.map(user -> user.getFullName())
			.orElse(""));
		userColumn.setExpandRatio(1);
		if (showEntityScopes) {
			final Column<SocialEconomyEntity, String> entityScopesColumn = boxGrid.addColumn(obj -> obj.getEntityScopesString());
			entityScopesColumn.setMinimumWidthFromContent(false);
			entityScopesColumn.setExpandRatio(1);
		}
		boxGrid.setStyleGenerator(item -> "background-" + item.getColor());

		return boxGrid;
	}

}
