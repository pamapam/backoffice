package org.pamapam.backoffice.layout.component;

import org.jamgo.vaadin.ui.JmgLocalizedField.DisplayMode;
import org.jamgo.vaadin.ui.builder.JamgoLocalizedFieldBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// FIXME: Implement in framework. See PamapamLocalivedTextArea.

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class PamapamLocalizedTextAreaBuilder extends JamgoLocalizedFieldBuilder<PamapamLocalizedTextArea> {

	public PamapamLocalizedTextAreaBuilder() {
		super();
		this.instance = new PamapamLocalizedTextArea();
		this.instance.setCaptionAsHtml(true);
	}

	@Override
	public PamapamLocalizedTextAreaBuilder setDisplayMode(final DisplayMode displayMode) {
		this.instance.setDisplayMode(displayMode);
		return this;
	}

}
