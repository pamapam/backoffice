package org.pamapam.backoffice.layout.details;

import java.io.InputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.ContentLayout;
import org.jamgo.ui.layout.CustomLayout;
import org.jamgo.vaadin.util.OnDemandFileDownloader;
import org.jamgo.vaadin.util.OnDemandFileDownloader.OnDemandStreamResource;
import org.pamapam.model.SearchInfo;
import org.pamapam.services.SearchInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SearchInfoDetailsLayout extends CustomLayout {
	
	private static final Logger logger = LoggerFactory.getLogger(SearchInfoDetailsLayout.class);

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private LocalizedMessageService messageSource;
	@Autowired
	private SearchInfoService searchInfoService;
	@Autowired
	@Lazy
	private ContentLayout contentLayout;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy,  HH:mm");
	
	private GridLayout filterLayout;
	private DateField beginDateField;
	private DateField endDateField;
	private Button exportButton;
	
	private List<SearchInfo> searchInfoList = new ArrayList<>();
	private ListDataProvider<SearchInfo> searchInfoDataProvider = new ListDataProvider<>(this.searchInfoList);
	private Grid<SearchInfo> grid = new Grid<>(this.searchInfoDataProvider);

	@Override
	public void initialize() {
		this.setSizeFull();
		
		this.createFilterLayout();
		
		this.grid = new Grid<>();
		this.grid.setSizeFull();
		this.grid.setDataProvider(this.searchInfoDataProvider);
		this.grid.addColumn(obj -> obj.getText()).setCaption("Text").setExpandRatio(1);
		this.grid.addColumn(obj -> this.sdf.format(obj.getDate())).setCaption("Date");
		this.addComponent(this.grid);
		this.setExpandRatio(this.grid, 1);
		
		this.contentLayout.addSelectedTabChangeListener(listener -> {
			listener.getComponent();
			if (listener.getTabSheet().getSelectedTab().equals(SearchInfoDetailsLayout.this)) {
				this.searchInfoList.clear();
				this.searchInfoList.addAll(this.searchInfoService.getAll());
				this.searchInfoDataProvider.refreshAll();
			}
		});
	}
	
	private void createFilterLayout() {
		this.filterLayout = new GridLayout(3,1);
		this.filterLayout.setWidth(100, Unit.PERCENTAGE);
		this.filterLayout.setSpacing(true);
		this.filterLayout.setMargin(true);
		
		this.addComponent(this.filterLayout);
		
		this.createDateFilter();
		this.createExportButton();
	}
	
	private void createExportButton() {
		this.exportButton = new Button(this.messageSource.getMessage("action.export"));
		
		FileDownloader fdExcel = new OnDemandFileDownloader(new OnDemandStreamResource() {
			private static final long serialVersionUID = 1L;

			@Override
			public InputStream getStream() {
				try {
					List<SearchInfo> items = SearchInfoDetailsLayout.this.searchInfoDataProvider.fetch(new Query<>()).collect(Collectors.toList());
					return SearchInfoDetailsLayout.this.searchInfoService.exportToCsv(items);
				} catch (Exception e) {
					SearchInfoDetailsLayout.logger.error(e.getMessage(), e);
					Notification.show(SearchInfoDetailsLayout.this.messageSource.getMessage("message.generic.error"), Notification.Type.ERROR_MESSAGE);
				}
				return null;
			}
			
			@Override
			public String getFilename() {
				return "cerques - " + System.currentTimeMillis() + ".xls";
			}
		});
		((StreamResource)fdExcel.getFileDownloadResource()).setCacheTime(-1);
		fdExcel.extend(this.exportButton);	
			
		this.filterLayout.addComponent(this.exportButton,2,0);
		this.filterLayout.setComponentAlignment(this.exportButton, Alignment.TOP_RIGHT);
		this.filterLayout.setColumnExpandRatio(2, 1);
		
	}
	
	private void createDateFilter() {
		this.beginDateField = new DateField(this.messageSource.getMessage("filter.date.from"));
		this.beginDateField.addValueChangeListener(listener -> {
			this.searchInfoDataProvider.addFilter(searchInfo -> searchInfo.getDate().after(Date.valueOf(this.beginDateField.getValue())));
		});
		
		this.endDateField = new DateField(this.messageSource.getMessage("filter.date.to"));
		this.endDateField.addValueChangeListener(listener ->  {
			this.searchInfoDataProvider.addFilter(searchInfo -> searchInfo.getDate().before(Date.valueOf(this.endDateField.getValue().plus(1, ChronoUnit.DAYS))));
		});
		
		this.filterLayout.addComponent(this.beginDateField, 0,0);
		this.filterLayout.addComponent(this.endDateField, 1,0);
		
	}
}
