package org.pamapam.backoffice.layout;

import java.util.Optional;

import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.CrudTableLayout;
import org.jamgo.vaadin.ui.JmgNotification;
import org.pamapam.backoffice.PamapamBackofficeApplicationDef;
import org.pamapam.backoffice.layout.details.PamapamUserDetailsLayout;
import org.pamapam.backoffice.layout.details.see.SocialEconomyEntityDetailsLayout;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FescEntityProfileLayout extends FescLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("FescEntityDetailsLayout")
	private SocialEconomyEntityDetailsLayout seeDetailsLayout;
	@Autowired
	private PamapamBackofficeApplicationDef backofficeApplicationDef;
	@Autowired
	private CrudServices crudServices;
	@Autowired
	private LocalizedMessageService messageServices;

	public void initialie(final SocialEconomyEntity see) {
		this.setStyleName("fesc-registration");
		this.addHeader();
		this.addEntityDetails(see);
	}

	private void addEntityDetails(final SocialEconomyEntity see) {
		final CrudLayoutDef<SocialEconomyEntity> seeLayoutDef = (CrudLayoutDef<SocialEconomyEntity>) this.backofficeApplicationDef.getLayoutDef(SocialEconomyEntity.class);
		this.seeDetailsLayout.initialize(seeLayoutDef);
		this.seeDetailsLayout.updateFields(see);
		this.seeDetailsLayout.setOkHandler(obj -> this.save(obj));
		this.seeDetailsLayout.getCancelButton().setVisible(false);
		this.addComponent(this.seeDetailsLayout);
	}

	/**
	 * This method is an adaptation of saveDetails() in {@link CrudTableLayout} since {@link PamapamUserDetailsLayout}
	 * is used here to show user's profile.
	 *
	 * @param detailsLayout
	 */
	protected void save(final CrudDetailsLayout<SocialEconomyEntity> detailsLayout) {
		SocialEconomyEntity savedItem = null;
		try {
			savedItem = this.crudServices.save(detailsLayout.getTargetObject());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		JmgNotification.show(this.messageServices.getMessage("feedback.saved"), Type.HUMANIZED_MESSAGE);
		detailsLayout.updateFields(savedItem);
	}
}
