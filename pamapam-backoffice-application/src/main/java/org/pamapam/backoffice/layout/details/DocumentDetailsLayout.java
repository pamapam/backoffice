package org.pamapam.backoffice.layout.details;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.BinaryResourceTransferField;
import org.pamapam.model.Document;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DocumentDetailsLayout extends CrudDetailsLayout<Document> {

	private static final long serialVersionUID = 1L;

	private BinaryResourceTransferField resourceField;
	private TextField descriptionField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));
		
		this.resourceField = this.componentBuilderFactory.createBinaryResourceTransferFieldBuilder().build();
		this.resourceField.setDescriptionEnabled(false);
		this.resourceField.setPreviewEnabled(false);
		this.resourceField.setDateEnabled(false);
		this.resourceField.setDeleteEnabled(false);
		this.resourceField.setSucceededListener(listener -> {
			BinaryResource binaryResource = this.targetObject.getBinaryResource();
			if (binaryResource == null) {
				binaryResource = new BinaryResource();
				this.targetObject.setBinaryResource(binaryResource);
			}
			binaryResource.setContents(listener.getBinaryObject().getContents());
			binaryResource.setFileName(listener.getBinaryObject().getFileName());
			binaryResource.setMimeType(listener.getBinaryObject().getMimeType());
			binaryResource.setTimeStamp(listener.getBinaryObject().getTimeStamp());
		});
		panel.addComponent(this.resourceField);

		
		this.descriptionField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("generic.entity.description").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.descriptionField);
		this.binder.bind(this.descriptionField, document -> {
			if (document.getBinaryResource() == null) {
				return null;
			}
			return document.getBinaryResource().getDescription();
		}, (document,value) -> {
			if (document.getBinaryResource() != null) {
				document.getBinaryResource().setDescription(value);
			}
		});
		
		return panel;
	}

	@Override
	protected Class<Document> getTargetObjectClass() {
		return Document.class;
	}

	@Override
	public void updateFields(Object savedItem) {
		super.updateFields(savedItem);
		
		this.resourceField.setValue(((Document)savedItem).getBinaryResource());
	}
	
	

}
