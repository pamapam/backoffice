package org.pamapam.backoffice.layout.details;

import javax.annotation.PostConstruct;

import org.jamgo.services.message.LocalizedMessageService;
import org.pamapam.backoffice.service.impl.AddressGeoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GeoLocationSelectionLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	private Grid<AddressGeoLocation> addressGeoLocationsGrid;

	@Autowired
	protected LocalizedMessageService messageSource;

	@PostConstruct
	public void init() {
		this.setSpacing(true);
		this.setMargin(true);
		this.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
	}

	@Override
	public void attach() {
		super.attach();

		this.addComponent(this.addressGeoLocationsGrid);
	}

	public Grid<AddressGeoLocation> getAddressGeoLocationsGrid() {
		return this.addressGeoLocationsGrid;
	}

	public void setAddressGeoLocationsGrid(Grid<AddressGeoLocation> addressGeoLocationsGrid) {
		this.removeAllComponents();
		this.addressGeoLocationsGrid = addressGeoLocationsGrid;
	}

}
