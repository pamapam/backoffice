package org.pamapam.backoffice.layout.component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.SerializableComparator;
import com.vaadin.shared.Registration;
import com.vaadin.shared.ui.dnd.DropEffect;
import com.vaadin.shared.ui.dnd.EffectAllowed;
import com.vaadin.shared.ui.grid.DropMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.GridDragSource;
import com.vaadin.ui.components.grid.GridDropTarget;
import com.vaadin.ui.components.grid.NoSelectionModel;
import com.vaadin.ui.renderers.TextRenderer;

public class PamapamTwinColGrid<T> extends CustomComponent implements HasValue<Set<T>> {

	private static final long serialVersionUID = 1L;

	protected final Grid<T> leftGrid = new Grid<>();

	protected final Grid<T> rightGrid = new Grid<>();

	protected ListDataProvider<T> leftGridDataProvider;

	protected ListDataProvider<T> rightGridDataProvider;

	private final Button addAllButton = new Button();

	private final Button addButton = new Button();

	private final Button removeButton = new Button();

	private final Button removeAllButton = new Button();

	private final VerticalLayout buttonContainer;

	private Grid<T> draggedGrid;

	/**
	 * Constructs a new TwinColGrid with an empty {@link ListDataProvider}.
	 */
	public PamapamTwinColGrid() {
		this(DataProvider.ofCollection(new LinkedHashSet<>()));
	}

	/**
	 * Constructs a new TwinColGrid with data provider for options.
	 *
	 * @param dataProvider the data provider, not {@code null}
	 */
	public PamapamTwinColGrid(final ListDataProvider<T> dataProvider) {
		this.setDataProvider(dataProvider);

		this.rightGridDataProvider = DataProvider.ofCollection(new LinkedHashSet<>());
		this.rightGrid.setDataProvider(this.rightGridDataProvider);

		this.leftGrid.setSelectionMode(SelectionMode.SINGLE);
		this.rightGrid.setSelectionMode(SelectionMode.SINGLE);

		this.addButton.setIcon(VaadinIcons.ANGLE_RIGHT, ">");
		this.addButton.setWidth(3f, Unit.EM);
		this.addAllButton.setIcon(VaadinIcons.ANGLE_DOUBLE_RIGHT, ">>");
		this.addAllButton.setWidth(3f, Unit.EM);
		this.removeButton.setIcon(VaadinIcons.ANGLE_LEFT, "<");
		this.removeButton.setWidth(3f, Unit.EM);
		this.removeAllButton.setIcon(VaadinIcons.ANGLE_DOUBLE_LEFT, "<<");
		this.removeAllButton.setWidth(3f, Unit.EM);

		this.buttonContainer = new VerticalLayout(this.addButton, this.removeButton);
		this.buttonContainer.setSpacing(false);
		this.buttonContainer.setSizeUndefined();

		final HorizontalLayout container = new HorizontalLayout(this.leftGrid, this.buttonContainer, this.rightGrid);
		container.setSizeFull();
		this.leftGrid.setSizeFull();
		this.rightGrid.setSizeFull();
		container.setExpandRatio(this.leftGrid, 1f);
		container.setExpandRatio(this.rightGrid, 1f);

		this.addAllButton.addClickListener(e -> {
			this.leftGridDataProvider.getItems().stream().forEach(this.leftGrid.getSelectionModel()::select);
			this.updateSelection(new LinkedHashSet<>(this.leftGrid.getSelectedItems()), new HashSet<>());
		});

		this.addButton.addClickListener(e -> {
			this.updateSelection(new LinkedHashSet<>(this.leftGrid.getSelectedItems()), new HashSet<>());
		});

		this.removeButton.addClickListener(e -> {
			this.updateSelection(new HashSet<>(), this.rightGrid.getSelectedItems());
		});

		this.removeAllButton.addClickListener(e -> {
			this.rightGridDataProvider.getItems().stream().forEach(this.rightGrid.getSelectionModel()::select);
			this.updateSelection(new HashSet<>(), this.rightGrid.getSelectedItems());
		});

		this.setCompositionRoot(container);
		this.setSizeUndefined();
	}

	public void setItems(final Collection<T> items) {
		this.setDataProvider(DataProvider.ofCollection(items));
	}

	public void setItems(final Stream<T> items) {
		this.setDataProvider(DataProvider.fromStream(items));
	}

	private void setDataProvider(final ListDataProvider<T> dataProvider) {
		this.leftGridDataProvider = dataProvider;
		this.leftGrid.setDataProvider(dataProvider);
		if (this.rightGridDataProvider != null) {
			this.rightGridDataProvider.getItems().clear();
			this.rightGridDataProvider.refreshAll();
		}
	}

	/**
	 * Constructs a new TwinColGrid with the given options.
	 *
	 * @param options the options, cannot be {@code null}
	 */
	public PamapamTwinColGrid(final Collection<T> options) {
		this(DataProvider.ofCollection(new LinkedHashSet<>(options)));
	}

	/**
	 * Constructs a new TwinColGrid with caption and data provider for options.
	 *
	 * @param caption      the caption to set, can be {@code null}
	 * @param dataProvider the data provider, not {@code null}
	 */
	public PamapamTwinColGrid(final String caption, final ListDataProvider<T> dataProvider) {
		this(dataProvider);
		this.setCaption(caption);
	}

	/**
	 * Constructs a new TwinColGrid with caption and the given options.
	 *
	 * @param caption the caption to set, can be {@code null}
	 * @param options the options, cannot be {@code null}
	 */
	public PamapamTwinColGrid(final String caption, final Collection<T> options) {
		this(caption, DataProvider.ofCollection(new LinkedHashSet<>(options)));
	}

	/**
	 * Returns the number of rows in the selects.
	 *
	 * @return the number of rows visible
	 */
	public int getRows() {
		return (int) this.leftGrid.getHeightByRows();
	}

	/**
	 * Sets the number of rows in the selects. If the number of rows is set to 0 or
	 * less, the actual number of displayed rows is determined implicitly by the
	 * selects.
	 * <p>
	 * If a height if set (using {@link #setHeight(String)} or
	 * {@link #setHeight(float, Unit)}) it overrides the number of rows. Leave the
	 * height undefined to use this method.
	 *
	 * @param rows the number of rows to set.
	 */
	public PamapamTwinColGrid<T> withRows(int rows) {
		if (rows < 0) {
			rows = 0;
		}
		this.leftGrid.setHeightByRows(rows);
		this.rightGrid.setHeightByRows(rows);
		this.markAsDirty();
		return this;
	}

	/**
	 * Sets the text shown above the right column. {@code null} clears the caption.
	 *
	 * @param rightColumnCaption The text to show, {@code null} to clear
	 */
	public PamapamTwinColGrid<T> withRightColumnCaption(final String rightColumnCaption) {
		this.rightGrid.setCaption(rightColumnCaption);
		this.markAsDirty();
		return this;
	}

	/**
	 * Adds a new text column to this {@link Grid} with a value provider. The column
	 * will use a {@link TextRenderer}. The value is converted to a String using
	 * {@link Object#toString()}. In-memory sorting will use the natural ordering of
	 * elements if they are mutually comparable and otherwise fall back to comparing
	 * the string representations of the values.
	 *
	 * @param valueProvider the value provider
	 *
	 * @return the new column
	 */
	public <V> PamapamTwinColGrid<T> addColumn(final ValueProvider<T, V> valueProvider, final String caption) {
		this.leftGrid.addColumn(valueProvider, new TextRenderer()).setCaption(caption);
		this.rightGrid.addColumn(valueProvider, new TextRenderer()).setCaption(caption);
		return this;
	}

	public <V> PamapamTwinColGrid<T> addSortableColumn(final ValueProvider<T, V> valueProvider, final SerializableComparator<T> comparator, final String header) {
		this.leftGrid.addColumn(valueProvider, new TextRenderer()).setCaption(header).setComparator(comparator).setSortable(true);
		this.rightGrid.addColumn(valueProvider, new TextRenderer()).setCaption(header).setComparator(comparator).setSortable(true);
		return this;
	}

	public PamapamTwinColGrid<T> showAddAllButton() {
		this.buttonContainer.addComponent(this.addAllButton, 0);
		return this;
	}

	public PamapamTwinColGrid<T> showRemoveAllButton() {
		this.buttonContainer.addComponent(this.removeAllButton, this.buttonContainer.getComponentCount());
		return this;
	}

	public PamapamTwinColGrid<T> withSizeFull() {
		super.setSizeFull();
		return this;
	}

	/**
	 * Adds drag n drop support between grids.
	 *
	 * @return
	 */
	public PamapamTwinColGrid<T> withDragAndDropSupport() {
		this.configDragAndDrop(this.leftGrid, this.rightGrid);
		this.configDragAndDrop(this.rightGrid, this.leftGrid);
		return this;
	}

	/**
	 * Returns the text shown above the right column.
	 *
	 * @return The text shown or {@code null} if not set.
	 */
	public String getRightColumnCaption() {
		return this.rightGrid.getCaption();
	}

	/**
	 * Sets the text shown above the left column. {@code null} clears the caption.
	 *
	 * @param leftColumnCaption The text to show, {@code null} to clear
	 */
	public PamapamTwinColGrid<T> withLeftColumnCaption(final String leftColumnCaption) {
		this.leftGrid.setCaption(leftColumnCaption);
		this.markAsDirty();
		return this;
	}

	/**
	 * Returns the text shown above the left column.
	 *
	 * @return The text shown or {@code null} if not set.
	 */
	public String getLeftColumnCaption() {
		return this.leftGrid.getCaption();
	}

	@Override
	public void setValue(final Set<T> value) {
		if (value != null) {
			final Collection<T> rightItems = ((ListDataProvider<T>) this.rightGrid.getDataProvider()).getItems();
			final Set<T> toRemoveItems = rightItems.stream()
				.map(Objects::requireNonNull)
				.filter(each -> !value.contains(each))
				.collect(Collectors.toCollection(LinkedHashSet::new));
			final Set<T> toAddItems = value.stream()
				.map(Objects::requireNonNull)
				.filter(each -> !rightItems.contains(each))
				.collect(Collectors.toCollection(LinkedHashSet::new));
			this.updateSelection(toAddItems, new LinkedHashSet<>(toRemoveItems));
		}
	}

	/**
	 * Returns the current value of this object which is an immutable set of the
	 * currently selected items.
	 *
	 * @return the current selection
	 */
	@Override
	public Set<T> getValue() {
		return Collections.unmodifiableSet(new LinkedHashSet<>(this.rightGridDataProvider.getItems()));
	}

	@Override
	public Registration addValueChangeListener(final ValueChangeListener<Set<T>> listener) {
		return this.rightGridDataProvider.addDataProviderListener(e -> {
			listener.valueChange(new ValueChangeEvent<>(PamapamTwinColGrid.this,
				new LinkedHashSet<>(this.rightGridDataProvider.getItems()), true));
		});
	}

	@Override
	public boolean isReadOnly() {
		return super.isReadOnly();
	}

	@Override
	public boolean isRequiredIndicatorVisible() {
		return super.isRequiredIndicatorVisible();
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		this.leftGrid.setSelectionMode(readOnly ? SelectionMode.NONE : SelectionMode.SINGLE);
		this.rightGrid.setSelectionMode(readOnly ? SelectionMode.NONE : SelectionMode.SINGLE);
		this.addButton.setEnabled(!readOnly);
		this.removeButton.setEnabled(!readOnly);
		this.addAllButton.setEnabled(!readOnly);
		this.removeAllButton.setEnabled(!readOnly);
	}

	@Override
	public void setRequiredIndicatorVisible(final boolean visible) {
		super.setRequiredIndicatorVisible(visible);
	}

	private void updateSelection(final Set<T> addedItems, final Set<T> removedItems) {
		this.leftGridDataProvider.getItems().addAll(removedItems);
		this.leftGridDataProvider.getItems().removeAll(addedItems);
		this.leftGridDataProvider.refreshAll();

		this.rightGridDataProvider.getItems().addAll(addedItems);
		this.rightGridDataProvider.getItems().removeAll(removedItems);
		this.rightGridDataProvider.refreshAll();

		this.leftGrid.getSelectionModel().deselectAll();
		this.rightGrid.getSelectionModel().deselectAll();
	}

	@SuppressWarnings("unchecked")
	private void configDragAndDrop(final Grid<T> sourceGrid, final Grid<T> targetGrid) {
		final GridDragSource<T> dragSource = new GridDragSource<>(sourceGrid);
		dragSource.setEffectAllowed(EffectAllowed.MOVE);
		dragSource.setDragImage(VaadinIcons.COPY);

		final Set<T> draggedItems = new LinkedHashSet<>();

		dragSource.addGridDragStartListener(event -> {
			this.draggedGrid = sourceGrid;
			if (!(this.draggedGrid.getSelectionModel() instanceof NoSelectionModel)) {
				if (event.getComponent().getSelectedItems().isEmpty()) {
					draggedItems.addAll(event.getDraggedItems());
				} else {
					draggedItems.addAll(event.getComponent().getSelectedItems());
				}
			}
		});

		dragSource.addGridDragEndListener(event -> {
			if (event.getDropEffect() == DropEffect.MOVE) {
				if (this.draggedGrid == null) {
					draggedItems.clear();
					return;
				}
				final ListDataProvider<T> dragGridSourceDataProvider = (ListDataProvider<T>) this.draggedGrid
					.getDataProvider();
				dragGridSourceDataProvider.getItems().removeAll(draggedItems);
				dragGridSourceDataProvider.refreshAll();

				draggedItems.clear();

				this.draggedGrid.deselectAll();
				this.draggedGrid = null;
			}
		});

		final GridDropTarget<T> dropTarget = new GridDropTarget<>(targetGrid, DropMode.ON_TOP);
		dropTarget.setDropEffect(DropEffect.MOVE);
		dropTarget.addGridDropListener(event -> {
			event.getDragSourceExtension().ifPresent(source -> {
				if (source instanceof GridDragSource && this.draggedGrid != event.getComponent()) {
					final ListDataProvider<T> dragGridTargetDataProvider = (ListDataProvider<T>) event.getComponent()
						.getDataProvider();
					dragGridTargetDataProvider.getItems().addAll(draggedItems);
					dragGridTargetDataProvider.refreshAll();
				} else {
					this.draggedGrid = null;
				}
			});
		});
	}

	public Registration addLeftGridSelectionListener(final SelectionListener<T> listener) {
		return this.leftGrid.addSelectionListener(listener);
	}

	public Registration addRightGridSelectionListener(final SelectionListener<T> listener) {
		return this.rightGrid.addSelectionListener(listener);
	}

	public void setHeaderVisible(final boolean headerVisible) {
		this.leftGrid.setHeaderVisible(headerVisible);
		this.rightGrid.setHeaderVisible(headerVisible);
	}

	public void setSelectionMode(final SelectionMode selectionMode) {
		this.leftGrid.setSelectionMode(selectionMode);
		this.rightGrid.setSelectionMode(selectionMode);
	}

	public <V> PamapamTwinColGrid<T> addSortableColumn(final ValueProvider<T, V> valueProvider, final SerializableComparator<T> comparator, final String header, final double width) {
		final Column<T, V> leftColumn = this.leftGrid.addColumn(valueProvider, new TextRenderer())
			.setCaption(header)
			.setComparator(comparator)
			.setSortable(true)
			.setWidth(width);
		this.leftGrid.sort(leftColumn);
		final Column<T, V> rightColumn = this.rightGrid.addColumn(valueProvider, new TextRenderer())
			.setCaption(header)
			.setComparator(comparator)
			.setSortable(true)
			.setWidth(width);
		this.rightGrid.sort(rightColumn);
		return this;
	}

}
