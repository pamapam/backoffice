package org.pamapam.backoffice.layout.details.see;

import java.util.ArrayList;
import java.util.List;

public class EntityTokens {
	private List<ProductTagTokenizable> productTokens = new ArrayList<>();
	private List<SocialEconomyNetworkTagTokenizable> socialEconomyNetworkTokens = new ArrayList<>();
	private List<KeywordTagTokenizable> keywordTokens = new ArrayList<>();

	public List<ProductTagTokenizable> getProductTokens() {
		return this.productTokens;
	}

	public void setProductTokens(final List<ProductTagTokenizable> productTokens) {
		this.productTokens = productTokens;
	}

	public List<SocialEconomyNetworkTagTokenizable> getSocialEconomyNetworkTokens() {
		return this.socialEconomyNetworkTokens;
	}

	public void setSocialEconomyNetworkTokens(final List<SocialEconomyNetworkTagTokenizable> socialEconomyNetworkTokens) {
		this.socialEconomyNetworkTokens = socialEconomyNetworkTokens;
	}

	public List<KeywordTagTokenizable> getKeywordTokens() {
		return this.keywordTokens;
	}

	public void setKeywordTokens(final List<KeywordTagTokenizable> keywordTokens) {
		this.keywordTokens = keywordTokens;
	}

}
