package org.pamapam.backoffice.layout.details;

import java.util.Collections;
import java.util.List;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.model.Criterion;
import org.pamapam.model.CriterionAnswer;
import org.pamapam.model.EntityEvaluation;
import org.pamapam.repository.CriterionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EntityEvaluationDetailsLayout extends CrudDetailsLayout<EntityEvaluation> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private CriterionRepository criterionRepository;

	public class CriterionLevel {

		private String label;
		private Integer value;

		public CriterionLevel(String label, Integer value) {
			this.label = label;
			this.value = value;
		}

		public String getLabel() {
			return this.label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public Integer getValue() {
			return this.value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}

	}

	private List<CriterionLevel> criterionLevels;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.criterionLevels = Lists.newArrayList();
		this.criterionLevels.add(new CriterionLevel("No aplica", null));
		this.criterionLevels.add(new CriterionLevel("0", 0));
		this.criterionLevels.add(new CriterionLevel("1", 1));
		this.criterionLevels.add(new CriterionLevel("2", 2));
		this.criterionLevels.add(new CriterionLevel("3", 3));
		this.criterionLevels.add(new CriterionLevel("4", 4));
		this.criterionLevels.add(new CriterionLevel("5", 5));

		List<Criterion> allCriteria = this.criterionRepository.findByOldVersionFalse(new Sort("viewOrder"));
		Collections.sort(allCriteria, Criterion.ID_ORDER);
		for (Criterion eachCriterion : allCriteria) {
			panel.addComponent(this.createCriterionGroup(eachCriterion));
		}

		return panel;
	}

	private VerticalLayout createCriterionGroup(Criterion criterion) {
		VerticalLayout criterionGroupLayout = this.componentFactory.newVerticalLayout();
		criterionGroupLayout.setWidth(100, Unit.PERCENTAGE);
		criterionGroupLayout.setCaption(String.valueOf(criterion.getId()));

		Label nameLabel = new Label(criterion.getName().getDefaultText());
		criterionGroupLayout.addComponent(nameLabel);

		Label questionLabel = this.componentFactory.newLabel();
		questionLabel.setCaption("<b>" + criterion.getQuestion().getDefaultText() + "</b>");
		questionLabel.setContentMode(ContentMode.HTML);
		questionLabel.setDescription(criterion.getDescription().getDefaultText());
		criterionGroupLayout.addComponent(questionLabel);

		RadioButtonGroup<CriterionLevel> levelsGroup = this.componentFactory.newRadioButtonGroup();
		levelsGroup.setCaption("");
		levelsGroup.setItems(this.criterionLevels);
		levelsGroup.setItemCaptionGenerator(l -> l.getLabel());
		levelsGroup.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
		criterionGroupLayout.addComponent(levelsGroup);

		for (CriterionAnswer eachAnswer : criterion.getCriterionAnswers()) {
			String answerText = String.valueOf(eachAnswer.getAnswerOrder()) + ". " + eachAnswer.getAnswer().getDefaultText();
			Label answerLabel = this.componentFactory.newLabel();
			answerLabel.setCaption(answerText);
			criterionGroupLayout.addComponent(answerLabel);
		}
		criterionGroupLayout.addComponent(this.componentFactory.newLabel());
		criterionGroupLayout.addComponent(this.createTextArea("entityEvaluationCriterion.note"));
		criterionGroupLayout.addComponent(this.componentFactory.newLabel());
		return criterionGroupLayout;
	}

	private TextArea createTextArea(String captionId) {
		TextArea field = this.componentFactory.newTextArea(captionId);
		field.setSizeFull();
		field.setWordWrap(true);
		field.setWidth(100, Unit.PERCENTAGE);
		return field;
	}

	@Override
	protected Class<EntityEvaluation> getTargetObjectClass() {
		return EntityEvaluation.class;
	}

}
