package org.pamapam.backoffice.layout.details;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.tika.Tika;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextArea;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.Sector;
import org.pamapam.repository.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.easyuploads.UploadField;
import org.vaadin.easyuploads.UploadField.FieldType;

import com.vaadin.data.ValidationException;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SectorDetailsLayout extends CrudDetailsLayout<Sector> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private SectorRepository sectorRepository;

	private JmgLocalizedTextField nameField;
	private JmgLocalizedTextArea descriptionField;

	private UploadField iconUploadField;
	private Image iconField;
	private UploadField mapIconUploadField;
	private Image mapIconField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		ComboBox<Sector> parent = this.componentFactory.newComboBox("sector.parent");
		parent.setWidth(50, Unit.PERCENTAGE);
		parent.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		parent.setItems(this.sectorRepository.findAll());
		panel.addComponent(parent);
		this.binder.bind(parent, Sector::getParent, Sector::setParent);

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("location.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, Sector::getName, Sector::setName);

		this.descriptionField = (JmgLocalizedTextArea)this.componentBuilderFactory.createLocalizedTextAreaBuilder().setCaption("location.description").setWidth(100, Unit.PERCENTAGE).build();
		panel.addComponent(this.descriptionField);
		this.binder.bind(this.descriptionField, Sector::getDescription, Sector::setDescription);

		this.addIconLayout(panel);
		this.addMapIconLayout(panel);

		return panel;
	}

	private void addIconLayout(CrudDetailsPanel panel) {
		HorizontalLayout pictureLayout = this.componentFactory.newHorizontalLayout();
		pictureLayout.setCaption(this.messageSource.getMessage("sector.icon"));

		this.iconUploadField = this.componentFactory.newUploadField();
		this.iconUploadField.setAcceptFilter("image/*");
		this.iconUploadField.setFieldType(FieldType.BYTE_ARRAY);
		this.iconUploadField.setButtonCaption(this.messageSource.getMessage("sector.icon.chooseFile"));
		this.iconUploadField.addListener((Listener) event -> SectorDetailsLayout.this.showUploadedIcon());
		pictureLayout.addComponent(this.iconUploadField);

		this.iconField = this.componentFactory.newImage();
		this.iconField.setHeight(34, Unit.PIXELS);
		pictureLayout.addComponent(this.iconField);

		panel.addComponent(pictureLayout);
	}

	private void addMapIconLayout(CrudDetailsPanel panel) {
		HorizontalLayout pictureLayout = this.componentFactory.newHorizontalLayout();
		pictureLayout.setCaption(this.messageSource.getMessage("sector.mapIcon"));

		this.mapIconUploadField = this.componentFactory.newUploadField();
		this.mapIconUploadField.setAcceptFilter("image/*");
		this.mapIconUploadField.setFieldType(FieldType.BYTE_ARRAY);
		this.mapIconUploadField.setButtonCaption(this.messageSource.getMessage("sector.icon.chooseFile"));
		this.mapIconUploadField.addListener((Listener) event -> SectorDetailsLayout.this.showUploadedMapIcon());
		pictureLayout.addComponent(this.mapIconUploadField);

		this.mapIconField = this.componentFactory.newImage();
		this.mapIconField.setHeight(34, Unit.PIXELS);
		pictureLayout.addComponent(this.mapIconField);

		panel.addComponent(pictureLayout);
	}

	private void showUploadedIcon() {
		this.showUploadedImage(this.iconUploadField, this.iconField);
	}

	private void showUploadedMapIcon() {
		this.showUploadedImage(this.mapIconUploadField, this.mapIconField);
	}

	private void showUploadedImage(UploadField uploadField, Image image) {
		Object value = uploadField.getValue();
		if (value != null) {
			final byte[] data = (byte[]) value;

			String fileName = uploadField.getLastFileName();
			if (fileName == null) {
				fileName = "tmp";
			}
			fileName = fileName + String.valueOf((new Date()).getTime());
			StreamResource resource = new StreamResource(() -> new ByteArrayInputStream(data), fileName);

			image.setSource(resource);
		} else {
			image.setSource(null);
		}
	}

	@Override
	public void updateFields(Object object) {
		Sector sector = (Sector) object;
		super.updateFields(sector);

		if (this.targetObject != null) {
			if (this.targetObject.getIcon() != null) {
				byte[] pictureBytes = this.targetObject.getIcon().getContents();
				this.iconUploadField.setValue(pictureBytes);
			} else {
				this.iconUploadField.clear();
			}
			this.showUploadedIcon();
			if (this.targetObject.getMapIcon() != null) {
				byte[] pictureBytes = this.targetObject.getMapIcon().getContents();
				this.mapIconUploadField.setValue(pictureBytes);
			} else {
				this.mapIconUploadField.clear();
			}
			this.showUploadedMapIcon();
		}
	}

	@Override
	public void updateTargetObject() throws ValidationException {
		super.updateTargetObject();

		if (this.iconUploadField.getValue() != null) {
			this.targetObject.setIcon(this.createBinaryResource(this.iconUploadField));
		}

		if (this.mapIconUploadField.getValue() != null) {
			this.targetObject.setMapIcon(this.createBinaryResource(this.mapIconUploadField));
		}

	}

	private BinaryResource createBinaryResource(UploadField uploadField) {
		BinaryResource icon = new BinaryResource((byte[]) uploadField.getValue());
		InputStream pictureStream = new ByteArrayInputStream(icon.getContents());
		String mimeType = "";
		try {
			mimeType = (new Tika()).detect(pictureStream);
			pictureStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		icon.setMimeType(mimeType);
		icon.setFileLength(icon.getContents().length);
		icon.setFileName(uploadField.getLastFileName());
		return icon;
	}

	@Override
	protected Class<Sector> getTargetObjectClass() {
		return Sector.class;
	}

}
