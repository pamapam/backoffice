package org.pamapam.backoffice.layout.details;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.model.EntityScope;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.stereotype.Component;

import com.vaadin.ui.TextField;

@Component
@org.springframework.context.annotation.Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EntityScopeDetailsLayout extends CrudDetailsLayout<EntityScope> {

	private static final long serialVersionUID = 1L;

	private TextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("community.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, EntityScope::getName, EntityScope::setName);

		return panel;
	}

	@Override
	protected Class<EntityScope> getTargetObjectClass() {
		return EntityScope.class;
	}

}
