package org.pamapam.backoffice.layout.details;

import java.util.Set;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.EntityStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ListSelect;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EntityStatusDetailsLayout extends CrudDetailsLayout<EntityStatus> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private EntityStatusRepository entityStatusRepository;

	private JmgLocalizedTextField nameField;

	private ComboBox<EntityStatusType> entityStatusTypeField;

	private ListSelect<EntityStatus> nextStatusesField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField) this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("entityStatus.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, EntityStatus::getName, EntityStatus::setName);

		this.entityStatusTypeField = this.componentFactory.newComboBox("entityStatus.type");
		this.entityStatusTypeField.setWidth(50, Unit.PERCENTAGE);
		this.entityStatusTypeField.setDataProvider(DataProvider.ofCollection(Lists.newArrayList(EntityStatusType.values())));
		panel.addComponent(this.entityStatusTypeField);
		this.binder.bind(this.entityStatusTypeField, EntityStatus::getEntityStatusType, EntityStatus::setEntityStatusType);

		this.nextStatusesField = this.componentFactory.newListSelect("entityStatus.nextStatuses");
		this.nextStatusesField.setDataProvider(DataProvider.ofCollection(this.entityStatusRepository.findAll()));
		this.nextStatusesField.setRows(6);
		this.nextStatusesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.nextStatusesField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.nextStatusesField);
		this.binder.bind(this.nextStatusesField, EntityStatus::getNextStatuses, EntityStatus::setNextStatuses);

		return panel;
	}

	@Override
	protected Class<EntityStatus> getTargetObjectClass() {
		return EntityStatus.class;
	}

	@Override
	public void updateFields(final Object savedItem) {
		if (savedItem instanceof EntityStatus) {
			final EntityStatus entityStatus = (EntityStatus) savedItem;
			Set<EntityStatusType> sameOriginTypes;

			if (entityStatus.getEntityStatusType() == null) { // new item
				if (this.crudLayoutDef.getId() != null && this.crudLayoutDef.getId().startsWith("fesc")) {
					sameOriginTypes = EntityStatusType.getFescTypes();
				} else {
					sameOriginTypes = EntityStatusType.getPamapamTypes();
				}
			} else {
				sameOriginTypes = EntityStatusType.sameOriginValues(entityStatus.getEntityStatusType());
			}

			// set entity status data provider based on status type's origin of savedItem
			this.entityStatusTypeField.setDataProvider(DataProvider.ofCollection(sameOriginTypes));

			// set next statuses data provider based on status type's origin of savedItem
			this.nextStatusesField.setDataProvider(DataProvider.ofCollection(this.entityStatusRepository.findByEntityStatusTypeIn(sameOriginTypes)));
		}

		super.updateFields(savedItem);

	}

}
