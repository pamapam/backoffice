package org.pamapam.backoffice.layout.details.see;

import java.util.Map;

public class CriterionLevel {

	private String label;
	private Integer value;
	private Map<Long, Integer> answerValues;

	public CriterionLevel() {
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getValue() {
		return this.value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Map<Long, Integer> getAnswerValues() {
		return this.answerValues;
	}

	public void setAnswerValues(Map<Long, Integer> answerValues) {
		this.answerValues = answerValues;
	}

}
