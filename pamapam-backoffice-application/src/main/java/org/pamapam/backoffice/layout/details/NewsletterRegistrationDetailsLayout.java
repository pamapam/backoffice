package org.pamapam.backoffice.layout.details;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.model.NewsletterRegistration;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NewsletterRegistrationDetailsLayout extends CrudDetailsLayout<NewsletterRegistration> {

	private static final long serialVersionUID = 1L;

	protected TextField nameField;
	protected TextField emailField;
	protected DateField registrationDateField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = this.componentFactory.newTextField("user.name");
		this.nameField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.nameField);
		this.binder.forField(this.nameField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(NewsletterRegistration::getName, NewsletterRegistration::setName);

		this.emailField = this.componentFactory.newTextField("user.email");
		this.emailField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.emailField);
		this.binder.forField(this.emailField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(NewsletterRegistration::getEmail, NewsletterRegistration::setEmail);

		this.registrationDateField = this.componentFactory.newDateField("user.registrationDate");
		this.registrationDateField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.registrationDateField);
		this.binder.forField(this.registrationDateField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.withConverter(new LocalDateToDateConverter())
			.bind(NewsletterRegistration::getRegistrationDate, NewsletterRegistration::setRegistrationDate);
		
		return panel;
	}

	@Override
	protected Class<NewsletterRegistration> getTargetObjectClass() {
		return NewsletterRegistration.class;
	}

}
