package org.pamapam.backoffice.layout;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PrivacyConsentLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected JamgoComponentFactory componentFactory;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;

	private Label privacyTextField;
	private CheckBox privacyAcceptField;
	private CheckBox newsletterAcceptField;
	private Button okButton;
	private Button cancelButton;

	@PostConstruct
	public void init() {
		this.setSizeFull();
		this.setMargin(true);
		this.setSpacing(true);
		this.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

		this.privacyTextField = this.componentFactory.newLabel();
		this.privacyTextField.setSizeFull();
		this.privacyTextField.setContentMode(ContentMode.HTML);
		this.privacyTextField.setValue(this.messageSource.getMessage("user.privacyConsent.text"));
		this.addComponent(this.privacyTextField);
		this.setExpandRatio(this.privacyTextField, 1);

		this.privacyAcceptField = this.componentFactory.newCheckBox();
		this.privacyAcceptField.setCaption("Acepto");
		this.privacyAcceptField.addValueChangeListener(event -> {
			this.okButton.setEnabled(event.getValue());
		});
		this.addComponent(this.privacyAcceptField);

		this.newsletterAcceptField = this.componentFactory.newCheckBox();
		this.newsletterAcceptField.setCaption("Accepto rebre butlletins.");
		this.addComponent(this.newsletterAcceptField);

		this.okButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.ok").build();
		this.okButton.setEnabled(false);
		this.okButton.addClickListener(event -> {
//			this.passwordField.setValue(encodedNewPassword);
			if ((this.getParent() != null) && (this.getParent() instanceof Window)) {
				((Window) this.getParent()).close();
			}
		});
		this.cancelButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.cancel").build();
		this.cancelButton.addClickListener(event -> {
//			this.passwordField.setValue(encodedNewPassword);
			this.privacyAcceptField.setValue(false);
			this.newsletterAcceptField.setValue(false);
			if ((this.getParent() != null) && (this.getParent() instanceof Window)) {
				((Window) this.getParent()).close();
			}
		});
		final HorizontalLayout buttonsLayout = this.componentFactory.newHorizontalLayout();
		buttonsLayout.addComponents(this.okButton, this.cancelButton);
		buttonsLayout.setSpacing(true);
		buttonsLayout.setMargin(true);

		this.addComponent(buttonsLayout);
	}

	@Override
	public void attach() {
		super.attach();
		this.privacyAcceptField.setValue(false);
		this.newsletterAcceptField.setValue(false);
		this.okButton.setEnabled(false);
	}

	public boolean isPrivaryConsentAccepted() {
		return this.privacyAcceptField.getValue();
	}

	public boolean isNewsletterConsentAccepted() {
		return this.newsletterAcceptField.getValue();
	}
}
