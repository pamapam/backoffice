package org.pamapam.backoffice.layout.ie;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jamgo.services.ie.Importer;
import org.jamgo.ui.layout.crud.ie.CrudImportLayout;
import org.pamapam.model.Sector;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.services.ie.GreenCommerceImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SocialEconomyEntityImportLayout extends CrudImportLayout<SocialEconomyEntity>{

	private static final long serialVersionUID = 1L;

	@Autowired
	private GreenCommerceImporter entityImporter;
	
	@Override
	public Importer<SocialEconomyEntity> getImporter() {
		return this.entityImporter;
	}

	@Override
    public void attach() {
		this.importButton.setCaption("Importar entitats de Comerç Verd");
		this.generateSampleButton.setVisible(false);
		super.attach();  
            this.deleteAllCheck.setVisible(false);
    }

	private Sector getSubSector(SocialEconomyEntity entity) {
		if (entity == null) {
			return null;
		}
		
		if (CollectionUtils.isEmpty(entity.getSectors())) {
			return null;
		}
		
		try {
			return entity.getSectors().stream().findFirst().orElse(null); 
		} catch (Exception e) {
			return null;
		}
    }
            										
    @Override
    protected void createPreviewGrid(List<SocialEconomyEntity> data) {
            this.previewGrid = new Grid<SocialEconomyEntity>(SocialEconomyEntity.class);
            this.previewGrid.setSizeFull();
            this.previewGrid.setHeightByRows(15);
            this.previewGrid.setItems(data);        
            this.previewGrid.removeAllColumns();
            this.previewGrid.addColumn(entity -> entity.getId() != null ? "X" : "").setCaption(this.messageSource.getMessage("sociaelEconomyEntity.alreadyExists"));
            this.previewGrid.addColumn(entity -> entity.getName()).setCaption(this.messageSource.getMessage("socialEconomyEntity.name"));
            this.previewGrid.addColumn(entity -> entity.getNif()).setCaption(this.messageSource.getMessage("socialEconomyEntity.nif"));
            this.previewGrid.addColumn(entity -> entity.getEmail()).setCaption(this.messageSource.getMessage("socialEconomyEntity.email"));
            this.previewGrid.addColumn(entity -> entity.getPhone()).setCaption(this.messageSource.getMessage("socialEconomyEntity.phone"));
            this.previewGrid.addColumn(entity -> entity.getAddress()).setCaption(this.messageSource.getMessage("socialEconomyEntity.address"));
            this.previewGrid.addColumn(entity -> entity.getLatitude()).setCaption(this.messageSource.getMessage("socialEconomyEntity.latitude"));
            this.previewGrid.addColumn(entity -> entity.getLongitude()).setCaption(this.messageSource.getMessage("socialEconomyEntity.longitude")); 
            this.previewGrid.addColumn(entity -> entity.getTown() != null ? entity.getTown().getName() : null).setCaption(this.messageSource.getMessage("socialEconomyEntity.town"));
            this.previewGrid.addColumn(entity -> entity.getNeighborhood() != null ? entity.getNeighborhood().getName() : null).setCaption(this.messageSource.getMessage("socialEconomyEntity.neighborhood"));
            this.previewGrid.addColumn(entity -> entity.getMainSector() != null ? entity.getMainSector().getName() : null).setCaption(this.messageSource.getMessage("socialEconomyEntity.sector"));
            this.previewGrid.addColumn(entity ->  getSubSector(entity) != null ? getSubSector(entity).getName() : null).setCaption(this.messageSource.getMessage("socialEconomyEntity.mainSubsector"));
            this.previewGrid.addColumn(entity -> entity.getTwitter()).setCaption(this.messageSource.getMessage("socialEconomyEntity.twitter"));
            this.previewGrid.addColumn(entity -> entity.getFacebook()).setCaption(this.messageSource.getMessage("socialEconomyEntity.facebook"));
            this.previewGrid.addColumn(entity -> entity.getInstagram()).setCaption(this.messageSource.getMessage("socialEconomyEntity.instagram"));
            
    }
    
    @Override
    public void executeImport(ClickEvent event) {
    	super.executeImport(event);
        Notification.show("Importació finalitzada. Recorda revisar la geolocalització dels punts que has importat.", Notification.Type.WARNING_MESSAGE);
    }
}
