package org.pamapam.backoffice.layout.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.gargoylesoftware.htmlunit.javascript.host.External;
import org.apache.commons.collections.CollectionUtils;
import org.jamgo.model.entity.District;
import org.jamgo.model.entity.Neighborhood;
import org.jamgo.model.entity.Province;
import org.jamgo.model.entity.Region;
import org.jamgo.model.entity.Town;
import org.jamgo.services.DatasourceServices;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudDetailsPanel.Format;
import org.jamgo.ui.layout.crud.CrudSearchLayout;
import org.jamgo.ui.layout.utils.AdvancedFileDownloader;
import org.pamapam.backoffice.layout.utils.PamapamComponentFactory;
import org.pamapam.model.*;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.*;
import org.pamapam.repository.search.SocialEconomyEntitySearch;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.pamapam.services.EntityScopeService;
import org.pamapam.services.SocialEconomyEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.data.ValidationException;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SocialEconomyEntitySearchLayout extends CrudSearchLayout<SocialEconomyEntity, SocialEconomyEntitySearch> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private SocialEconomyEntityService socialEconomyEntityService;
	@Autowired
	private SectorRepository sectorRepository;
	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private LegalFormRepository legalFormRepository;
	@Autowired
	private PamapamUserRepository pamapamUserRepository;
	@Autowired
	protected DatasourceServices datasourceServices;
	@Autowired
	private ExternalFilterTagRepository externalFilterTagRepository;
	@Autowired
	private EntityScopeRepository entityScopeRepository;
	@Autowired
	private QuestionaireRepository questionaireRepository;
	@Autowired
	private EntityScopeService entityScopeService;

	private Button exportButton;
//	private Consumer<CrudSearchLayout<SocialEconomyEntity, SocialEconomyEntitySearch>> exportHandler;

	@PostConstruct
	private void init() {
		this.searchSpecification = this.applicationContext.getBean(SocialEconomyEntitySpecification.class);
	}

	@Override
	protected List<Button> createActionButtons() {
		final List<Button> buttons = super.createActionButtons();
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			this.exportButton = this.componentBuilderFactory.createButtonBuilder().build();
			this.exportButton.setCaption(this.messageSource.getMessage("action.export"));
			final AdvancedFileDownloader downloader = new AdvancedFileDownloader();
			((StreamResource) downloader.getFileDownloadResource()).setCacheTime(-1);
			downloader.addAdvancedDownloaderListener(downloadEvent -> {
				try {
					final String filePath = this.doExport();
					downloader.setFilePath(filePath);
				} catch (final Exception e) {
					e.printStackTrace();
					Notification.show(this.messageSource.getMessage("message.generic.error"), Notification.Type.ERROR_MESSAGE);
				}
			});

			downloader.extend(this.exportButton);
			buttons.add(this.exportButton);

		}
		return buttons;
	}

	private String doExport() {
		String exportFilePath = null;
		try {
			exportFilePath = this.socialEconomyEntityService.exportToCsv(this.searchSpecification);
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return exportFilePath;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected CrudDetailsPanel createPanel() {
		final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel(Format.Vertical);
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		final TextField nameField = this.componentFactory.newTextField("socialEconomyEntity.name");
		nameField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(nameField);
		this.binder.bind(nameField, SocialEconomyEntitySearch::getName, SocialEconomyEntitySearch::setName);

		final DateField dateFromField = this.componentFactory.newDateField("socialEconomyEntity.search.dateFrom");
		panel.addComponent(dateFromField);
		this.binder.forField(dateFromField)
			.withConverter(new LocalDateToDateConverter())
			.bind(SocialEconomyEntitySearch::getDateFrom, SocialEconomyEntitySearch::setDateFrom);

		final DateField dateToField = this.componentFactory.newDateField("socialEconomyEntity.search.dateTo");
		panel.addComponent(dateToField);
		this.binder.forField(dateToField)
			.withConverter(new LocalDateToDateConverter())
			.bind(SocialEconomyEntitySearch::getDateTo, SocialEconomyEntitySearch::setDateTo);

		final Label focusedOnLabel = this.componentBuilderFactory.createLabelBuilder().build();
		focusedOnLabel.setValue(this.messageSource.getMessage("fesc.registration.basic.focusedOn"));
		panel.addComponent(focusedOnLabel);

		final CheckBox enterprises = this.componentBuilderFactory.createCheckBoxBuilder().setCaption("fesc.registration.basic.enterprises").build();
		panel.addComponent(enterprises);
		this.binder.bind(enterprises, SocialEconomyEntitySearch::isEnterprises, SocialEconomyEntitySearch::setEnterprises);

		final CheckBox privateNoobs = this.componentBuilderFactory.createCheckBoxBuilder().setCaption("fesc.registration.basic.privateNoobs").build();
		panel.addComponent(privateNoobs);
		this.binder.bind(privateNoobs, SocialEconomyEntitySearch::isPrivateNoobs, SocialEconomyEntitySearch::setPrivateNoobs);

		final CheckBox privateActivists = this.componentBuilderFactory.createCheckBoxBuilder().setCaption("fesc.registration.basic.privateActivists").build();
		panel.addComponent(privateActivists);
		this.binder.bind(privateActivists, SocialEconomyEntitySearch::isPrivateActivists, SocialEconomyEntitySearch::setPrivateActivists);

		final CheckBox publicAdministration = this.componentBuilderFactory.createCheckBoxBuilder().setCaption("fesc.registration.basic.publicAdministration.short").build();
		panel.addComponent(publicAdministration);
		this.binder.bind(publicAdministration, SocialEconomyEntitySearch::isPublicAdministration, SocialEconomyEntitySearch::setPublicAdministration);

		final ListSelect<EntityStatus> entityStatusesField = this.componentFactory.newListSelect("socialEconomyEntity.search.entityStatuses");
		entityStatusesField.setDataProvider(DataProvider.ofCollection(this.getEntityStatuses()));
		entityStatusesField.setRows(6);
		entityStatusesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		entityStatusesField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(entityStatusesField);
		this.binder.bind(entityStatusesField, SocialEconomyEntitySearch::getEntityStatuses, SocialEconomyEntitySearch::setEntityStatuses);

		final ListSelect<Sector> subSectorsField = this.componentFactory.newListSelect("socialEconomyEntity.search.subSectors");
		final List<Sector> subSectors = this.sectorRepository.findByParentIsNotNull();
		Collections.sort(subSectors, Sector.NAME_ORDER);
		subSectorsField.setDataProvider(DataProvider.ofCollection(subSectors));
		subSectorsField.setRows(10);
		subSectorsField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		subSectorsField.setWidth(100, Unit.PERCENTAGE);
		final ListDataProvider<Sector> subSectorsDataProvider = (ListDataProvider<Sector>) subSectorsField.getDataProvider();
		subSectorsDataProvider.setFilterByValue(item -> item.getId(), null);

		final List<Sector> sectors = this.sectorRepository.findByParentIsNull();
		Collections.sort(sectors, Sector.NAME_ORDER);
		final ComboBox<Sector> sectorField = this.componentFactory.newComboBox("socialEconomyEntity.search.sector");
		sectorField.setWidth(100, Unit.PERCENTAGE);
		sectorField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		sectorField.setItems(sectors);
		sectorField.addValueChangeListener(event -> {
			final Sector sector = event.getValue();
			final ListDataProvider<Sector> dataProvider = (ListDataProvider<Sector>) subSectorsField.getDataProvider();
			if (sector != null) {
				dataProvider.setFilterByValue(item -> item.getParent(), sector);
			} else {
				dataProvider.setFilterByValue(item -> item.getId(), null);
			}
		});
		panel.addComponent(sectorField);
		this.binder.bind(sectorField, SocialEconomyEntitySearch::getSector, SocialEconomyEntitySearch::setSector);

		panel.addComponent(subSectorsField);
		this.binder.bind(subSectorsField, SocialEconomyEntitySearch::getSubSectors, SocialEconomyEntitySearch::setSubSectors);

		final ComboBox<LegalForm> legalFormField = this.componentFactory.newComboBox("socialEconomyEntity.legalForm");
		legalFormField.setWidth(100, Unit.PERCENTAGE);
		legalFormField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		legalFormField.setItems(this.legalFormRepository.findAll());
		panel.addComponent(legalFormField);
		this.binder.bind(legalFormField, SocialEconomyEntitySearch::getLegalForm, SocialEconomyEntitySearch::setLegalForm);

		// ...	Neighborhood.
		final ComboBox<Neighborhood> neighborhoodField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Neighborhood.class,
			null,
			null,
			(ValueProvider<Neighborhood, Long>) item -> item.getId(),
			null);
		neighborhoodField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(neighborhoodField, SocialEconomyEntitySearch::getNeighborhood, SocialEconomyEntitySearch::setNeighborhood);

		// ...	District.
		final ComboBox<District> districtField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			District.class,
			neighborhoodField,
			null,
			(ValueProvider<District, Long>) item -> item.getId(),
			(ValueProvider<Neighborhood, District>) neighborhood -> neighborhood.getDistrict());
		districtField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(districtField, SocialEconomyEntitySearch::getDistrict, SocialEconomyEntitySearch::setDistrict);

		// ...	Town.
		final ComboBox<Town> townField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Town.class,
			districtField,
			null,
			(ValueProvider<Town, Long>) item -> item.getId(),
			(ValueProvider<District, Town>) district -> district.getTown());
		townField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(townField, SocialEconomyEntitySearch::getTown, SocialEconomyEntitySearch::setTown);

		// ...	Region.
		final ComboBox<Region> regionField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Region.class,
			townField,
			null,
			(ValueProvider<Region, Long>) item -> item.getId(),
			(ValueProvider<Town, Region>) town -> town.getRegion());
		regionField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(regionField, SocialEconomyEntitySearch::getRegion, SocialEconomyEntitySearch::setRegion);

		// ... Province.
		final ComboBox<Province> provinceField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Province.class,
			regionField,
			null,
			null,
			(ValueProvider<Region, Province>) region -> region.getProvince());
		provinceField.setWidth(100, Unit.PERCENTAGE);
		this.binder.bind(provinceField, SocialEconomyEntitySearch::getProvince, SocialEconomyEntitySearch::setProvince);

		final VerticalLayout territoryLayout = this.componentFactory.newVerticalLayout();
		territoryLayout.setCaption(this.messageSource.getMessage("socialEconomyEntity.territory"));
		territoryLayout.setSizeUndefined();
		territoryLayout.setWidth(100, Unit.PERCENTAGE);
		territoryLayout.setMargin(false);
		territoryLayout.addComponent(provinceField);
		territoryLayout.addComponent(regionField);
		territoryLayout.addComponent(townField);
		territoryLayout.addComponent(districtField);
		territoryLayout.addComponent(neighborhoodField);
		panel.addComponent(territoryLayout);

		final TextField fullTextField = this.componentFactory.newTextField("socialEconomyEntity.search.fullText");
		fullTextField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(fullTextField);
		this.binder.bind(fullTextField, SocialEconomyEntitySearch::getText, SocialEconomyEntitySearch::setText);

		final ComboBox<PamapamUser> userRegistryField = this.componentFactory.newComboBox("socialEconomyEntity.user.xinxeta");
		userRegistryField.setWidth(100, Unit.PERCENTAGE);
		userRegistryField.setItemCaptionGenerator(item -> item.getUsername());
		final List<PamapamUser> users = this.pamapamUserRepository.findByRolenames(java.util.stream.Stream.of(PamapamUser.ROLE_XINXETA, PamapamUser.ROLE_COORDINATION, PamapamUser.ROLE_EXTERNAL, PamapamUser.ROLE_REGISTERED, PamapamUser.ROLE_SUPERXINXETA)
			.collect(Collectors.toSet()));
		userRegistryField.setItems(users);
		panel.addComponent(userRegistryField);
		this.binder.bind(userRegistryField, SocialEconomyEntitySearch::getRegistryUser, SocialEconomyEntitySearch::setRegistryUser);

		final ComboBox<PamapamUser> userInitiativeField = this.componentFactory.newComboBox("socialEconomyEntity.user.initiative");
		userInitiativeField.setWidth(100, Unit.PERCENTAGE);
		userInitiativeField.setItemCaptionGenerator(item -> item.getUsername());
		userInitiativeField.setItems(this.pamapamUserRepository.findByRolename(PamapamUser.ROLE_INITIATIVE));
		panel.addComponent(userInitiativeField);
		this.binder.bind(userInitiativeField, SocialEconomyEntitySearch::getInitiativeUser, SocialEconomyEntitySearch::setInitiativeUser);

		final ListSelect<Questionaire> evaluationField = this.componentFactory.newListSelect("socialEconomyEntity.evaluation");
		final List<Questionaire> questionaires = this.questionaireRepository.findAll();
		evaluationField.setDataProvider(DataProvider.ofCollection(questionaires));
		evaluationField.setRows(10);
		evaluationField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		evaluationField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(evaluationField);
		this.binder.bind(evaluationField, SocialEconomyEntitySearch::getQuestionaires, SocialEconomyEntitySearch::setQuestionaires);

		final ListSelect<ExternalFilterTag> externalFilterTagsField = this.componentFactory.newListSelect("socialEconomyEntity.externalFilterTags");
		final List<ExternalFilterTag> externalFilterTags = this.externalFilterTagRepository.findAll();
		Collections.sort(externalFilterTags, ExternalFilterTag.TAG_TEXT_ORDER);
		externalFilterTagsField.setDataProvider(DataProvider.ofCollection(externalFilterTags));
		externalFilterTagsField.setRows(10);
		externalFilterTagsField.setItemCaptionGenerator(item -> item.getTagText());
		externalFilterTagsField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(externalFilterTagsField);
		this.binder.bind(externalFilterTagsField, SocialEconomyEntitySearch::getExternalFilterTags, SocialEconomyEntitySearch::setExternalFilterTags);

		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			final ListSelect<EntityScope> entityScopesField = this.componentFactory.newListSelect("socialEconomyEntity.entityScopes");
			final List<EntityScope> entityScopes = this.entityScopeRepository.findAll();
			Collections.sort(entityScopes, EntityScope.NAME_ORDER);
			entityScopesField.setDataProvider(DataProvider.ofCollection(entityScopes));
			entityScopesField.setRows(10);
			entityScopesField.setItemCaptionGenerator(item -> item.getName());
			entityScopesField.setWidth(100, Unit.PERCENTAGE);
			panel.addComponent(entityScopesField);
			this.binder.bind(entityScopesField, SocialEconomyEntitySearch::getEntityScopes, SocialEconomyEntitySearch::setEntityScopes);
		}

		this.addFescDataFilters(panel);

		final CheckBox includeMainOffices = this.componentBuilderFactory.createCheckBoxBuilder().setCaption("socialEconomyEntity.mainOffices").build();
		panel.addComponent(includeMainOffices);
		this.binder.bind(includeMainOffices, SocialEconomyEntitySearch::includeMainOffices, SocialEconomyEntitySearch::setIncludeMainOffices);

		final CheckBox includeOffices = this.componentBuilderFactory.createCheckBoxBuilder().setCaption("socialEconomyEntity.offices").build();
		panel.addComponent(includeOffices);
		this.binder.bind(includeOffices, SocialEconomyEntitySearch::includeOffices, SocialEconomyEntitySearch::setIncludeOffices);

		final ComboBox<SocialEconomyEntity> mainOfficeField = this.componentFactory.newComboBox("socialEconomyEntity.mainOffice");
		mainOfficeField.setWidth(100, Unit.PERCENTAGE);
		mainOfficeField.setItemCaptionGenerator(item -> item.getName());
		mainOfficeField.setItems(this.socialEconomyEntityService.findAllMainOffices());
		panel.addComponent(mainOfficeField);
		this.binder.bind(mainOfficeField, SocialEconomyEntitySearch::getMainOffice, SocialEconomyEntitySearch::setMainOffice);


		return panel;
	}

	private void addFescDataFilters(final CrudDetailsPanel panel) {
		final ListSelect<EntityStatus> entityFescStatusesField = this.componentFactory.newListSelect("socialEconomyEntity.search.fesc.entityStatuses");
		entityFescStatusesField.setDataProvider(DataProvider.ofCollection(this.getEntityFescStatuses()));
		entityFescStatusesField.setRows(7);
		entityFescStatusesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		entityFescStatusesField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(entityFescStatusesField);
		this.binder.bind(entityFescStatusesField, SocialEconomyEntitySearch::getEntityFescStatuses, SocialEconomyEntitySearch::setEntityFescStatuses);

		final TextField contactMailField = this.componentFactory.newTextField("socialEconomyEntity.search.fesc.contactMail");
		contactMailField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(contactMailField);
		this.binder.bind(contactMailField, SocialEconomyEntitySearch::getFescMail, SocialEconomyEntitySearch::setFescMail);

	}


	private List<EntityStatus> getEntityStatuses() {
		List<EntityStatus> entityStatuses = new ArrayList<>(this.entityStatusRepository.findByEntityStatusTypeIn(EntityStatusType.getPamapamTypes()));

		// ...	Admnis user can view all entity statuses.
		if (!VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			entityStatuses = entityStatuses.stream().filter(each -> !each.getEntityStatusType().equals(EntityStatusType.ARCHIVED)).collect(Collectors.toList());
		}

		Collections.sort(entityStatuses, EntityStatus.NAME_ORDER);
		return entityStatuses;
	}

	private List<EntityStatus> getEntityFescStatuses() {
		final List<EntityStatus> entityStatuses = new ArrayList<>(this.entityStatusRepository.findByEntityStatusTypeIn(EntityStatusType.getFescTypes()));

		Collections.sort(entityStatuses, EntityStatus.NAME_ORDER);
		return entityStatuses;
	}

	@Override
	protected Class<SocialEconomyEntitySearch> getTargetObjectClass() {
		return SocialEconomyEntitySearch.class;
	}

//	public Consumer<CrudSearchLayout<SocialEconomyEntity, SocialEconomyEntitySearch>> getExportHandler() {
//		return this.exportHandler;
//	}

//	public void setExportHandler(Consumer<CrudSearchLayout<SocialEconomyEntity, SocialEconomyEntitySearch>> exportHandler) {
//		this.exportHandler = exportHandler;
//	}

	@Override
	public void updateTargetObject() throws ValidationException {
		super.updateTargetObject();

		// ...	If no entity status was selected, use available statuses without by user role. This is needed because if the collection
		//		is empty, the search engine asumes all statuses.
		//
		//		Admnis user can view all entity statuses.
		if (!VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			if (CollectionUtils.isEmpty(this.targetObject.getEntityStatuses())) {
				List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();
				entityStatuses = entityStatuses.stream().filter(each -> !each.getEntityStatusType().equals(EntityStatusType.ARCHIVED)).collect(Collectors.toList());
				this.targetObject.setEntityStatuses(this.entityStatusRepository.findByEntityStatusTypeNot(EntityStatusType.ARCHIVED));
			}

			this.targetObject.setEntityScopes(this.entityScopeService.getCurrentEntityScopes());
		}
	}

}
