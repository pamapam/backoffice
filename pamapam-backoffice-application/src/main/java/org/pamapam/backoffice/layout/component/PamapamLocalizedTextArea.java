package org.pamapam.backoffice.layout.component;

import org.jamgo.model.entity.LocalizedString;
import org.jamgo.vaadin.ui.JmgLocalizedField;

import com.vaadin.ui.TextArea;

// FIXME: Implement in framework. Reimplemented here to override getEmptyValue().

public class PamapamLocalizedTextArea extends JmgLocalizedField<TextArea> {

	private static final long serialVersionUID = 1L;

	public static double TEXT_AREA_DEFAULT_HEIGHT = 110;
	public static double FIELD_DEFAULT_HEIGHT = 35;

	public PamapamLocalizedTextArea() {
		super();
	}

	@Override
	protected TextArea createBasicField() {
		final TextArea basicField = new TextArea();
		basicField.setSizeFull();
		basicField.setWordWrap(true);
		return basicField;
	}

	@Override
	public LocalizedString getEmptyValue() {
		final LocalizedString localizedString = new LocalizedString();
		this.getLanguages().stream().forEach(language -> {
			localizedString.set(language.getLanguageCode(), "");
		});
		return localizedString;
	}
}
