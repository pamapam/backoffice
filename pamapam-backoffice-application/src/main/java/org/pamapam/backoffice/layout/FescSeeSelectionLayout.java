package org.pamapam.backoffice.layout;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.pamapam.backoffice.PamapamSession;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.repository.search.SocialEconomyEntitySearch;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.Binder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FescSeeSelectionLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected JamgoComponentFactory componentFactory;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	private SocialEconomyEntityRepository socialEconomyEntityRepository;
	@Autowired
	private PamapamSession session;

	protected Binder<SocialEconomyEntity> binder;

	private ComboBox<SocialEconomyEntity> seeField;
	private Button okButton;
	private Consumer<FescSeeSelectionLayout> okHandler;

	private boolean registeredOnly;

	public void initialize(final boolean registeredOnly) {
		this.registeredOnly = registeredOnly;

		this.setStyleName("fesc-registration");
		this.binder = this.componentFactory.newBinder(SocialEconomyEntity.class);
		this.addFields();
		this.addButtons();
	}

	private void addFields() {
		this.seeField = this.componentFactory.newComboBox("fesc.registration.selection.see");
		this.seeField.setWidth(50, Unit.PERCENTAGE);
		this.seeField.setItemCaptionGenerator(item -> item.getName() + ", " + item.getAddress());
		this.seeField.setItems(this.getSeeList());
		this.addComponent(this.seeField);
	}

	private void addButtons() {
		this.okButton = this.componentBuilderFactory.createButtonBuilder().build();
		this.okButton.setCaption(this.messageSource.getMessage("fesc.registration.selection.button"));
		this.okButton.addClickListener(event -> this.doOk());

		final HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.addComponents(this.okButton);

		this.addComponent(buttonsLayout);
	}

	protected void doOk() {
		if (this.okHandler != null) {
			final SocialEconomyEntity selectedSee = this.seeField.getValue();
			if (selectedSee != null) {
				this.okHandler.accept(this);
			}
		}
	}

	public SocialEconomyEntity getSelectedSee() {
		return this.seeField.getValue();
	}

	public void setOkHandler(final Consumer<FescSeeSelectionLayout> okHandler) {
		this.okHandler = okHandler;
	}

	private Collection<SocialEconomyEntity> getSeeList() {
		final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();
		searchObject.setInitiativeUser(this.session.getCurrentUser());
		if (this.registeredOnly) {
			searchObject.setFescRegistered(true);
		}
		final SocialEconomyEntitySpecification spec = new SocialEconomyEntitySpecification();
		spec.setSearchObject(searchObject);
		// FIXME filtered should be done in search specification, but query for not registered entities must be property build and is not
		return this.socialEconomyEntityRepository.findAll(spec)
			.stream()
			.filter(see -> this.registeredOnly ? Optional.ofNullable(see.getFescData().getEntityStatus()).isPresent() : !Optional.ofNullable(see.getFescData().getEntityStatus()).isPresent())
			.collect(Collectors.toList());
	}
}
