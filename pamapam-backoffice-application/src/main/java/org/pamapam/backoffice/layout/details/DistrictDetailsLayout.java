package org.pamapam.backoffice.layout.details;

import org.jamgo.model.entity.District;
import org.jamgo.model.entity.Town;
import org.jamgo.model.repository.TownRepository;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.ComboBox;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DistrictDetailsLayout extends CrudDetailsLayout<District> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private TownRepository townRepository;

	private JmgLocalizedTextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("location.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, District::getName, District::setName);

		ComboBox<Town> townField = this.componentFactory.newComboBox("district.town");
		townField.setWidth(50, Unit.PERCENTAGE);
		townField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		townField.setItems(this.townRepository.findAll());
		panel.addComponent(townField);
		this.binder.bind(townField, District::getTown, District::setTown);

		return panel;
	}

	@Override
	protected Class<District> getTargetObjectClass() {
		return District.class;
	}

}
