package org.pamapam.backoffice.layout.details;

import java.util.List;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.backoffice.layout.component.ModelAttributeDefListGridField;
import org.pamapam.model.ModelDef;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ModelDefDetailsLayout extends CrudDetailsLayout<ModelDef> {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected ApplicationContext applicationContext;

	private TextField modelNameField;
	private ModelAttributeDefListGridField modelAttributesDefField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.modelNameField = this.componentFactory.newTextField("modelDef.modelName");
		this.modelNameField.setWidth(50, Unit.PERCENTAGE);
		this.modelNameField.setEnabled(false);
		this.binder.bind(this.modelNameField, ModelDef::getModelName, ModelDef::setModelName);
		panel.addComponent(this.modelNameField);

		this.modelAttributesDefField = this.applicationContext.getBean(ModelAttributeDefListGridField.class, this.messageSource.getMessage("modelDef.attributesDef"));
		panel.addComponent(this.modelAttributesDefField);

		return panel;
	}

	// ...	FIXME: Find classes by reflection or by annotations.
	private List<String> getAvailableModelNames() {
		return Lists.newArrayList(SocialEconomyEntity.class.getSimpleName());
	}

	@Override
	public void updateFields(Object object) {
		super.updateFields(object);

		if (this.targetObject != null) {
			this.modelAttributesDefField.setValue(this.targetObject.getAttributesDef());
		}
	}

	@Override
	public void updateTargetObject() throws ValidationException {
		super.updateTargetObject();
		this.modelAttributesDefField.getValue().stream().forEach(each -> each.setModelDef(this.targetObject));
		this.targetObject.setAttributesDef(this.modelAttributesDefField.getValue());
	}

	@Override
	protected Class<ModelDef> getTargetObjectClass() {
		return ModelDef.class;
	}

}
