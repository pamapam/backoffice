package org.pamapam.backoffice.layout.details;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TwinColSelect;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.FescRegistrationFee;
import org.pamapam.model.FescRegistrationModality;
import org.pamapam.services.FescRegistrationFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FescRegistrationModalityDetailsLayout extends CrudDetailsLayout<FescRegistrationModality> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private FescRegistrationFeeService feeService;

	private JmgLocalizedTextField nameField;
	private CheckBox shareTableAllowedField;
	private CheckBox isTimeSlotField;
	private TwinColSelect<FescRegistrationFee> feesField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField) this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("fesc.registration.invoice.modality.name").setWidth(50, Unit.PERCENTAGE).build();
		this.binder.bind(this.nameField, FescRegistrationModality::getName, FescRegistrationModality::setName);
		panel.addComponent(this.nameField);

		this.shareTableAllowedField = this.componentFactory.newCheckBox("fesc.registration.invoice.modality.shareTableAllowed");
		this.binder.bind(this.shareTableAllowedField, FescRegistrationModality::isShareTableAllowed, FescRegistrationModality::setShareTableAllowed);
		panel.addComponent(this.shareTableAllowedField);

		this.isTimeSlotField = this.componentFactory.newCheckBox("fesc.registration.invoice.modality.isTimeSlot");
		this.binder.bind(this.isTimeSlotField, FescRegistrationModality::isTimeSlot, FescRegistrationModality::setTimeSlot);
		panel.addComponent(this.isTimeSlotField);

		final List<FescRegistrationFee> allFees = this.feeService.findAll();

		this.feesField = this.componentFactory.newTwinColSelect("fesc.registration.invoice.modality.fees");
		this.feesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.feesField.setWidth(100, Unit.PERCENTAGE);
		this.feesField.setItems(allFees);
		this.binder.bind(this.feesField, FescRegistrationModality::getFees, FescRegistrationModality::setFees);
		panel.addComponent(this.feesField);

		return panel;
	}

	@Override
	protected Class<FescRegistrationModality> getTargetObjectClass() {
		return FescRegistrationModality.class;
	}

}
