package org.pamapam.backoffice.layout;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FescLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	final HorizontalLayout headerLayout = new HorizontalLayout();

	protected void addHeader() {
		this.headerLayout.setWidth(100.0f, Unit.PERCENTAGE);
		this.headerLayout.setStyleName("header");
		final ThemeResource headerResource = new ThemeResource("images/header-fesc-2022.jpg");
		final Image image = new Image(null, headerResource);
		image.setStyleName("image");
		image.setWidth(100, Unit.PERCENTAGE);
		final Label label = new Label("<h1>Expositores<h1>", ContentMode.HTML);
		label.setStyleName("fesc-exhibitor-title");
		this.headerLayout.addComponents(label, image);
		this.headerLayout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
		this.headerLayout.setExpandRatio(image, 1);
		this.addComponent(this.headerLayout);
	}
}
