package org.pamapam.backoffice.layout.details;

import org.jamgo.model.entity.Region;
import org.jamgo.model.entity.Town;
import org.jamgo.model.repository.RegionRepository;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.ComboBox;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TownDetailsLayout extends CrudDetailsLayout<Town> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private RegionRepository regionRepository;

	private JmgLocalizedTextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("location.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, Town::getName, Town::setName);

		ComboBox<Region> regionField = this.componentFactory.newComboBox("location.region");
		regionField.setWidth(50, Unit.PERCENTAGE);
		regionField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		regionField.setItems(this.regionRepository.findAll());
		panel.addComponent(regionField);
		this.binder.bind(regionField, Town::getRegion, Town::setRegion);

		return panel;
	}

	@Override
	protected Class<Town> getTargetObjectClass() {
		return Town.class;
	}

}
