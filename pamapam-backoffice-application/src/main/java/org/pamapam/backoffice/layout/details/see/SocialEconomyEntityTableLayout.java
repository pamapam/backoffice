package org.pamapam.backoffice.layout.details.see;

import com.google.common.collect.Lists;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.CrudTableLayout;
import org.jamgo.vaadin.ui.JmgNotification;
import org.pamapam.model.*;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.repository.CriterionRepository;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.repository.search.SocialEconomyEntitySearch;
import org.pamapam.repository.search.SocialEconomyEntitySpecification;
import org.pamapam.services.EntityScopeService;
import org.pamapam.services.PamapamUserService;
import org.pamapam.services.SocialBalanceService;
import org.pamapam.services.SocialEconomyEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;

import java.util.*;
import java.util.stream.Collectors;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SocialEconomyEntityTableLayout extends CrudTableLayout<SocialEconomyEntity> {

	private static final long serialVersionUID = 1L;
	@Autowired
	private LocalizedMessageService messageServices;

	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private CriterionRepository criterionRepository;
	@Autowired
	private SocialEconomyEntityService socialEconomyentityService;
	@Autowired
	private PamapamUserService userService;
	@Autowired
	private EntityScopeService entityScopeService;
	@Autowired
	private SocialBalanceService socialBalanceService;
	private Button updateBalanceButton;

	@Override
	public void initialize(final CrudLayoutDef<SocialEconomyEntity> crudLayoutDef) {
		super.initialize(crudLayoutDef);

		final SocialEconomyEntityDetailsLayout seeDetailsLayout = (SocialEconomyEntityDetailsLayout) this.detailsLayout;
		seeDetailsLayout.setOkHandler(obj -> this.attemptSave(obj, () -> SocialEconomyEntityTableLayout.this.saveDetails(seeDetailsLayout)));
		seeDetailsLayout.setOkDraftHandler(obj -> this.attemptSave(obj, () -> SocialEconomyEntityTableLayout.this.saveDraftDetails(seeDetailsLayout)));
		seeDetailsLayout.setOkPendingHandler(obj -> this.attemptSave(obj, () -> SocialEconomyEntityTableLayout.this.savePendingDetails(seeDetailsLayout)));
		seeDetailsLayout.setCreateDraftHandler(obj -> this.attemptSave(obj, () -> SocialEconomyEntityTableLayout.this.createDraft(seeDetailsLayout)));
		seeDetailsLayout.setCreateCopyHandler(obj -> this.attemptSave(obj, () -> SocialEconomyEntityTableLayout.this.createCopy(seeDetailsLayout)));
		seeDetailsLayout.setCreateOfficeHandler(obj -> this.attemptSave(obj, () -> SocialEconomyEntityTableLayout.this.createOffice(seeDetailsLayout)));
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			this.addUpdateXesSocialBalanceEntitiesButton();
			this.setImportButtonCaption();
		}
		this.table.setStyleGenerator(item -> "background-" + item.getColor());
	}

	private void setImportButtonCaption() {
		for (int i = 0; i < this.toolBar.getComponentCount(); i++) {
			Component toolbarComponent = this.toolBar.getComponent(i);
			if (toolbarComponent.getClass().equals(Button.class)) {
				Button eachButton = (Button) toolbarComponent;
				if (eachButton.getCaption().equals("Importar")) {
					eachButton.setCaption("Importar punts de Comerç Verd");
					this.toolBar.removeComponent(eachButton);
					this.toolBar.addComponent(eachButton);

					return;
				}
			}
		}
	}
	
	protected void attemptSave(final CrudDetailsLayout<SocialEconomyEntity> detailsLayout, final Runnable action) {
		if ((detailsLayout != null) && detailsLayout.getBinder().validate().hasErrors()) {
			MessageBox.createError()
				.withCaption(this.messageSource.getMessage("dialog.ok"))
				.withMessage(this.messageSource.getMessage("dialog.hasErrors"))
				.withOkButton(ButtonOption.caption(this.messageSource.getMessage("dialog.ok")))
				.open();
		} else {
			action.run();
		}
	}

	@Override
	protected void saveDetails(final CrudDetailsLayout<?> detailsLayout) {
//		this.updateEntityStatusChange((SocialEconomyEntityDetailsLayout) detailsLayout);
		SocialEconomyEntity savedEntity = null;
		try {
			savedEntity = this.socialEconomyentityService.save((SocialEconomyEntity) detailsLayout.getTargetObject(), ((SocialEconomyEntityDetailsLayout) detailsLayout).getPreviousStatus());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		final Notification savedNotification = new Notification(this.messageSource.getMessage("feedback.saved"), Type.TRAY_NOTIFICATION);
		savedNotification.setStyleName("emphasized");
		savedNotification.setDelayMsec(3000);
		savedNotification.show(Page.getCurrent());
		detailsLayout.updateFields(savedEntity);
		((SocialEconomyEntityDetailsLayout) detailsLayout).deemphasizeSaveButtons();
		this.refreshAll();
	}

	@Override
	protected void doRemove() {
		final Collection<SocialEconomyEntity> selectedItems = this.table.getSelectedItems();
		selectedItems.forEach(each -> {
			if (each.getMainOffice() != null) {
				each.setLegalForm(null);
				each.setMainSector(null);
				each.setSectors(null);
				each.setSectorCcae(null);
				each.setOtherSocialEconomyNetworks(null);
				each.setCollaborationEntities(null);
				each.setExternalFilterTags(null);
				each.setXesBalance(null);
				each.setEntityPersonRoles(null);
				each.setInvoicing(null);
				each.setCustomFields(null);
				each.setEntityEvaluation(null);
				each.setFescData(null);
			}
		});
		super.doRemove();
	}

	private void saveDraftDetails(final SocialEconomyEntityDetailsLayout detailsLayout) {
//		this.updateEntityStatusChange(detailsLayout);
		SocialEconomyEntity savedEntity = null;
		try {
			savedEntity = this.socialEconomyentityService.saveDraftSocialEconomyEntity(detailsLayout.getTargetObject());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		final Notification savedNotification = new Notification(this.messageSource.getMessage("feedback.saved"), Type.TRAY_NOTIFICATION);
		savedNotification.setStyleName("emphasized");
		savedNotification.setDelayMsec(3000);
		savedNotification.show(Page.getCurrent());
		detailsLayout.updateFields(savedEntity);
		this.refreshAll();
	}

	private void savePendingDetails(final SocialEconomyEntityDetailsLayout detailsLayout) {
//		this.updateEntityStatusChange(detailsLayout);
		SocialEconomyEntity savedEntity = null;
		try {
			savedEntity = this.socialEconomyentityService.savePendingSocialEconomyEntity(detailsLayout.getTargetObject());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		final Notification savedNotification = new Notification(this.messageSource.getMessage("feedback.saved"), Type.TRAY_NOTIFICATION);
		savedNotification.setStyleName("emphasized");
		savedNotification.setDelayMsec(3000);
		savedNotification.show(Page.getCurrent());
		detailsLayout.updateFields(savedEntity);
		this.refreshAll();
	}

	private void createDraft(final SocialEconomyEntityDetailsLayout detailsLayout) {
		final SocialEconomyEntity source = detailsLayout.getTargetObject();
		if (this.socialEconomyentityService.hasDraftSibling(source)) {
			Notification.show(this.messageSource.getMessage("error.socialEconomyEntity.draftExists"), Type.ERROR_MESSAGE);
		} else {
			final SocialEconomyEntity copy = this.copyEntity(source);
//			final SocialEconomyEntity copy = new SocialEconomyEntity(detailsLayout.getTargetObject());
//			copy.setRegistryUser(this.userService.getCurrentUser());
//			copy.setEntityStatus(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.DRAFT));
//			copy.getEntityStatusChanges().clear();
//			if (copy.getPicture() != null) {
//				copy.getPicture().setId(null);
//				copy.getPicture().setVersion(null);
//			}
//			if (detailsLayout.getTargetObject().isOldVersion()) {
//				copy.setOldVersion(false);
//				final EntityEvaluation entityEvaluation = new EntityEvaluation();
//				final List<EntityEvaluationCriterion> entityEvaluationCriterions = Lists.newArrayList();
//				for (final Criterion eachCriterion : this.criterionRepository.findByOldVersionFalse(new Sort("viewOrder"))) {
//					entityEvaluationCriterions.add(new EntityEvaluationCriterion(entityEvaluation, eachCriterion));
//				}
//				entityEvaluation.setEntityEvaluationCriterions(entityEvaluationCriterions);
//				copy.setEntityEvaluation(entityEvaluation);
//			}
			SocialEconomyEntity savedEntity = null;
			try {
				savedEntity = this.socialEconomyentityService.save(copy, detailsLayout.getTargetObject().getEntityStatus());
			} catch (final CrudException e) {
				Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
			}
			final Notification createdNotification = new Notification(this.messageSource.getMessage("feedback.draftCreated"), Type.TRAY_NOTIFICATION);
			createdNotification.setStyleName("emphasized");
			createdNotification.setDelayMsec(3000);
			createdNotification.show(Page.getCurrent());
			this.refreshAll();
		}
	}

	private void createCopy(final SocialEconomyEntityDetailsLayout detailsLayout) {
		final SocialEconomyEntity copy = this.copyEntity(detailsLayout.getTargetObject());
		copy.setGlobalId(null);
		copy.setNormalizedName(null);
		if (copy.getName() != null) {
			copy.setName(copy.getName() + " (" + this.messageSource.getMessage("socialEconomyEntity.copy") + ")");
		}
		try {
			this.socialEconomyentityService.save(copy, detailsLayout.getTargetObject().getEntityStatus());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		final Notification createdNotification = new Notification(this.messageSource.getMessage("feedback.copyCreated"), Type.TRAY_NOTIFICATION);
		createdNotification.setStyleName("emphasized");
		createdNotification.setDelayMsec(3000);
		createdNotification.show(Page.getCurrent());
		this.refreshAll();
	}

	private void createOffice(final SocialEconomyEntityDetailsLayout detailsLayout) {
		final SocialEconomyEntity source = detailsLayout.getTargetObject();
		final SocialEconomyEntity copy = this.copyEntity(source);
		copy.setNormalizedName(null);
		copy.setGlobalId(null);
		copy.setLegalForm(null);
		copy.setMainSector(null);
		copy.setSectors(null);
		copy.setSectorCcae(null);
		copy.setOtherSocialEconomyNetworks(null);
		copy.setCollaborationEntities(null);
		copy.setExternalFilterTags(null);
		copy.setXesBalance(null);
		copy.setEntityPersonRoles(null);
		copy.setInvoicing(null);
		copy.setCustomFields(null);
		copy.setEntityEvaluation(null);
		copy.setFescData(null);
		copy.setMainOffice(source);
		try {
			this.socialEconomyentityService.save(copy, detailsLayout.getTargetObject().getEntityStatus());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		final Notification createdNotification = new Notification(this.messageSource.getMessage("feedback.officeCreated"), Type.TRAY_NOTIFICATION);
		createdNotification.setStyleName("emphasized");
		createdNotification.setDelayMsec(3000);
		createdNotification.show(Page.getCurrent());
		this.refreshAll();
	}

	private SocialEconomyEntity copyEntity(final SocialEconomyEntity source) {
		final SocialEconomyEntity copy = new SocialEconomyEntity(source);
		copy.setRegistryUser(this.userService.getCurrentUser());
		copy.setEntityStatus(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.DRAFT));
		copy.getEntityStatusChanges().clear();
		if (copy.getPicture() != null) {
			copy.getPicture().setId(null);
			copy.getPicture().setVersion(null);
		}
		if (source.isOldVersion()) {
			copy.setOldVersion(false);
			final EntityEvaluation entityEvaluation = new EntityEvaluation();
			final List<EntityEvaluationCriterion> entityEvaluationCriterions = Lists.newArrayList();
			for (final Criterion eachCriterion : this.criterionRepository.findByOldVersionFalse(new Sort("viewOrder"))) {
				entityEvaluationCriterions.add(new EntityEvaluationCriterion(entityEvaluation, eachCriterion));
			}
			entityEvaluation.setEntityEvaluationCriterions(entityEvaluationCriterions);
			copy.setEntityEvaluation(entityEvaluation);
		}
		copy.setQuestionaire(source.getQuestionaire());
		return copy;
	}

	public void addUpdateXesSocialBalanceEntitiesButton() {
		this.updateBalanceButton = this.componentFactory.newButton("action.update.socialBalance");
		this.updateBalanceButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> {
			final String message = this.socialBalanceService.updateOrInsertAllPamAPamSocialBalanceEntities();
			JmgNotification.show(this.messageServices.getMessage(message), Type.HUMANIZED_MESSAGE).setDelayMsec(3000);
		}));
		this.toolBar.addComponent(this.updateBalanceButton);
	}

//	private void updateEntityStatusChange(SocialEconomyEntityDetailsLayout detailsLayout) {
//		if (detailsLayout.getTargetObject().getId() == null) {
//			this.updateEntityStatusChange(detailsLayout.getTargetObject(), null, detailsLayout.getTargetObject().getEntityStatus());
//		} else if (detailsLayout.isStatusChanged()) {
//			this.updateEntityStatusChange(detailsLayout.getTargetObject(), detailsLayout.getPreviousStatus(), detailsLayout.getTargetObject().getEntityStatus());
//		}
//	}

//	private void updateEntityStatusChange(SocialEconomyEntity entity, EntityStatus previousStatus, EntityStatus nextStatus) {
//		EntityStatusChange entityStatusChange = new EntityStatusChange();
//		entityStatusChange.setEntityStatusFrom(previousStatus);
//		entityStatusChange.setEntityStatusTo(nextStatus);
//		entityStatusChange.setChangeTimestamp(Timestamp.from(Instant.now()));
//		entityStatusChange.setUser(this.userService.getCurrentUser());
//		entity.addEntityStatusChange(entityStatusChange);
//	}

	@Override
	protected boolean isRemoveEnabled() {
		return VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN);
	}

	@Override
	protected boolean isSearchEnabled() {
		return !VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_INITIATIVE);
	}

	@Override
	protected DataProvider<SocialEconomyEntity, ?> getDefaultDataProvider() {
		DataProvider<SocialEconomyEntity, ?> dataProvider = null;
		try {
			dataProvider = this.crudManager.getDataProvider(this.crudLayoutDef.getEntityClass(), this.createSearchSpecification());
		} catch (final CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		return dataProvider;
	}

	private SocialEconomyEntitySpecification createSearchSpecification() {
		final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();

		// ...	Only entity scopes related to user role.
		searchObject.setEntityScopes(this.entityScopeService.getCurrentEntityScopes());

		// ...	Only owned entities for initiatives.
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_INITIATIVE)) {
			searchObject.setInitiativeUser(this.userService.getCurrentUser());
		}

		// ...	Admnis user can view all entity statuses.
		Set<EntityStatus> entityStatuses = null;
		if (!VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			entityStatuses = new HashSet<>(this.entityStatusRepository.findAll());
			entityStatuses = entityStatuses.stream().filter(each -> !each.getEntityStatusType().equals(EntityStatusType.ARCHIVED)).collect(Collectors.toSet());
		}
		searchObject.setEntityStatuses(entityStatuses);

		final SocialEconomyEntitySpecification searchSpecification = new SocialEconomyEntitySpecification();
		searchSpecification.setSearchObject(searchObject);
		return searchSpecification;
	}
}
