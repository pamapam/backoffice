package org.pamapam.backoffice.layout.details;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.model.MailTemplate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MailTemplateDetailsLayout extends CrudDetailsLayout<MailTemplate> {

	private static final long serialVersionUID = 1L;

	private TextField nameField;
	private TextArea contentField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = this.componentFactory.newTextField("mailTemplate.name");
		this.nameField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, MailTemplate::getName, MailTemplate::setName);

		this.contentField = this.componentFactory.newTextArea("mailTemplate.content");
		this.contentField.setWidth(100, Unit.PERCENTAGE);
		this.contentField.setHeight(20, Unit.EM);
		panel.addComponent(this.contentField);
		this.binder.bind(this.contentField, MailTemplate::getContent, MailTemplate::setContent);

		return panel;
	}

	@Override
	protected Class<MailTemplate> getTargetObjectClass() {
		return MailTemplate.class;
	}

}
