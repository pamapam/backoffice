package org.pamapam.backoffice.layout.details;

import org.jamgo.model.entity.Province;
import org.jamgo.model.entity.Region;
import org.jamgo.model.repository.ProvinceRepository;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.ComboBox;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RegionDetailsLayout extends CrudDetailsLayout<Region> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ProvinceRepository provinceRepository;

	private JmgLocalizedTextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("location.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, Region::getName, Region::setName);

		ComboBox<Province> provinceField = this.componentFactory.newComboBox("region.province");
		provinceField.setWidth(50, Unit.PERCENTAGE);
		provinceField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		provinceField.setItems(this.provinceRepository.findAll());
		panel.addComponent(provinceField);
		this.binder.bind(provinceField, Region::getProvince, Region::setProvince);

		return panel;
	}

	@Override
	protected Class<Region> getTargetObjectClass() {
		return Region.class;
	}

}
