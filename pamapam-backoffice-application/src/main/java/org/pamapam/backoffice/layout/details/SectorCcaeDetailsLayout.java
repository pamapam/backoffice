package org.pamapam.backoffice.layout.details;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.SectorCcae;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SectorCcaeDetailsLayout extends CrudDetailsLayout<SectorCcae> {

	private static final long serialVersionUID = 1L;

	private JmgLocalizedTextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		TextField codeField = this.componentFactory.newTextField("sectorCcae.code");
		codeField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(codeField);
		this.binder.bind(codeField, SectorCcae::getCode, SectorCcae::setCode);

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("sectorCcae.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(this.nameField);
		this.binder.bind(this.nameField, SectorCcae::getName, SectorCcae::setName);

		return panel;
	}

	@Override
	protected Class<SectorCcae> getTargetObjectClass() {
		return SectorCcae.class;
	}

}
