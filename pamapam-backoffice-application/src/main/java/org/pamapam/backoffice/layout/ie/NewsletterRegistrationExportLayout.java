package org.pamapam.backoffice.layout.ie;

import org.jamgo.ui.layout.crud.ie.CrudExportLayout;
import org.pamapam.model.NewsletterRegistration;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.function.Supplier;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NewsletterRegistrationExportLayout extends CrudExportLayout<NewsletterRegistration> {

	private static final long serialVersionUID = 1L;

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public void applyFieldSelection() {
		// TODO Auto-generated method stub
	}

	@Override
	public void setExportableFields() {
		this.addExportableColumn(new ExportableColumn(() -> this.messageSource.getMessage("user.name"), NewsletterRegistration::getName));
		this.addExportableColumn(new ExportableColumn(() -> this.messageSource.getMessage("user.email"), NewsletterRegistration::getEmail));
		this.addExportableColumn(new ExportableColumn(() -> this.messageSource.getMessage("user.registrationDate"), nr -> nr.getRegistrationDate() != null ? NewsletterRegistrationExportLayout.sdf.format(nr.getRegistrationDate()) : "-"));
	}

	@Override
	public void setTClass() {
		this.tClazz = NewsletterRegistration.class;
	}

	@Override
	public Supplier<String> getEntityNameSupplier() {
		return () -> "";
	}

	@Override
	public void setEntityManagerBeanName() {
		// TODO Auto-generated method stub

	}

}
