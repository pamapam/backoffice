package org.pamapam.backoffice.layout.details.see;

import com.explicatis.ext_token_field.ExtTokenField;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.ValidationException;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.*;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.*;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.themes.ValoTheme;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.jamgo.model.entity.*;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.services.DatasourceServices;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudDetailsPanel.Format;
import org.jamgo.vaadin.ui.BinaryResourceTransferField;
import org.jamgo.vaadin.ui.JmgFormLayout;
import org.jamgo.vaadin.ui.JmgLocalizedTextArea;
import org.jamgo.vaadin.ui.JmgNotification;
import org.pamapam.backoffice.PamapamSession;
import org.pamapam.backoffice.layout.component.PamapamTwinColGrid;
import org.pamapam.backoffice.layout.utils.PamapamComponentFactory;
import org.pamapam.backoffice.service.impl.FieldsEnableHandler;
import org.pamapam.backoffice.service.impl.LWikimediaMapsLayer;
import org.pamapam.backoffice.service.impl.MapzenService;
import org.pamapam.exception.EmptyEmailException;
import org.pamapam.model.*;
import org.pamapam.model.enums.EntityStatusType;
import org.pamapam.model.enums.FescTimeSlot;
import org.pamapam.repository.*;
import org.pamapam.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LMarker;
import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.addon.leaflet.shared.Point;
import org.vaadin.sliderpanel.SliderPanel;
import org.vaadin.sliderpanel.SliderPanelBuilder;
import org.vaadin.sliderpanel.client.SliderMode;
import org.vaadin.sliderpanel.client.SliderTabPosition;
import server.droporchoose.UploadComponent;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@Primary
public class SocialEconomyEntityDetailsLayout extends CrudDetailsLayout<SocialEconomyEntity> {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(SocialEconomyEntityDetailsLayout.class);

    @Autowired
    protected ApplicationContext applicationContext;
    @Autowired
    private ProductTagService productTagService;
    @Autowired
    private KeywordTagService keywordTagService;
    @Autowired
    private MailService mailService;
    @Autowired
    private QuestionaireService questionaireService;
    @Autowired
    private EntityStatusService entityStatusService;
    @Autowired
    private SocialEconomyNetworkTagRepository socialEconomyNetworkTagRepository;
    @Autowired
    private ExternalFilterTagRepository externalFilterTagRepository;
    @Autowired
    private EntityStatusRepository entityStatusRepository;
    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private SectorRepository sectorRepository;
    @Autowired
    private IdeologicalIdentificationRepository ideologicalIdentificationRepository;
    @Autowired
    private EntityScopeRepository entityScopeRepository;
    @Autowired
    private SectorCcaeRepository sectorCcaeRepository;
    @Autowired
    private LegalFormRepository legalFormRepository;
    @Autowired
    private SocialEconomyNetworkRepository socialEconomyNetworkRepository;
    @Autowired
    private MapzenService mapzenService;
    @Autowired
    protected DatasourceServices datasourceServices;
    @Autowired
    private PamapamUserService userService;
    @Autowired
    private UserActionTokenService userActionTokenService;
    @Autowired
    private SocialEconomyEntityService socialEconomyEntityService;
    @Autowired
    private CriterionRepository criterionRepository;
    @Autowired
    private PamapamUserRepository pamapamUserRepository;
    @Autowired
    private PamapamSession pamapamSession;
    @Autowired
    private ModelDefService modelDefService;
    @Autowired
    private FescRegistrationModalityService modalityService;
    @Autowired
    private QuestionaireRepository questionaireRepository;
    @Autowired
    private FieldsEnableHandler fieldsEnableHandler;

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

    // ...	Panels

    protected CrudDetailsPanel contactPanel;
    protected CrudDetailsPanel socialEconomyPanel;
    Map<Long, CrudDetailsPanel> entityEvaluationPanels;
    protected CrudDetailsPanel customFieldsPanel;
    protected CrudDetailsPanel historyPanel;
    protected CrudDetailsPanel fescPanel;
    protected CrudDetailsPanel officesPanel;

    // ...	Basic Tab fields.

    private TextField nameField;
    private TextField nifField;
    private TextField foundationYearField;
    private JmgLocalizedTextArea descriptionField;
    private UploadComponent pictureUploadField;
    private Image pictureField;
    private ProgressBar pictureProgressBar;
    private TextField addressField;
    private TextField postalCodeField;
    private LMap map = new LMap();
    private final LTileLayer mediawikiMapsTiles = new LWikimediaMapsLayer();
    private org.springframework.data.geo.Point point;
    private LMarker currentMarker;
    private ComboBox<Province> provinceField;
    private ComboBox<Region> regionField;
    private ComboBox<Town> townField;
    private ComboBox<District> districtField;
    private ComboBox<Neighborhood> neighborhoodField;
    private TextField openingHours;
    private ComboBox<Language> languageField;
    private DateField registryDateField;
    private ComboBox<EntityStatus> entityStatusField;
    private ComboBox<PamapamUser> registryUserField;
    private ComboBox<PamapamUser> initiativeUserField;
    private ComboBox<SocialEconomyEntity> mainOfficeField;
    private CheckBox userConditionsAcceptedField;
    private DateField userConditionsDateField;
    private CheckBox newslettersAcceptedField;
    private DateField newslettersDateField;

    // ...	Communication Tab fields.

    private TextField contactPersonField;
    private TextField emailField;
    private TextField phoneField;
    private TextField webField;
    private TextField onlineShoppingWebField;
    private TextField laZonaWebField;
    private TextField twitterField;
    private TextField facebookField;
    private TextField instagramField;
    private TextField pinterestField;
    private TextField quitterField;
    private TextField videoUrlField;

    private final List<EntityDiscount> discountList = new ArrayList<>();
    private final ListDataProvider<EntityDiscount> discountListDataProvider = new ListDataProvider<>(this.discountList);

    private final List<EntityAudiovisual> audiovisualList = new ArrayList<>();
    private final ListDataProvider<EntityAudiovisual> audiovisualListDataProvider = new ListDataProvider<>(this.audiovisualList);

    private final List<TextUrl> intercooperacioLinksList = new ArrayList<>();
    private final ListDataProvider<TextUrl> intercooperacioLinksListDataProvider = new ListDataProvider<>(this.intercooperacioLinksList);

    // ...	Social Economy Tab fields.

    private ComboBox<Sector> mainSectorField;
    private PamapamTwinColGrid<Sector> sectorsField;
    private ComboBox<SectorCcae> sectorCcaeField;
    private ExtTokenField tagsField;
    private ExtTokenField keywordsField;
    private ComboBox<LegalForm> legalFormField;
    private PamapamTwinColGrid<SocialEconomyNetwork> socialEconomyNetworksField;
    private ExtTokenField socialEconomyNetworkTagsField;
    private ComboBox<IdeologicalIdentification> ideologicalIdentificationField;
    private Map<Long, PersonRoleRow> personRoleRows;
    private Map<Long, Binder<PersonRoleRow>> personRoleRowBinders;
    private RadioButtonGroup<String> xesBalanceField;
    private TextArea intercooperacioField; //field for associated initiatives inside or outside PaP
    private CheckBox focusedOnEnterprisesField;
    private CheckBox focusedOnPrivateNoobsField;
    private CheckBox focusedOnPrivateActivistsField;
    private CheckBox focusedOnPublicAdministrationField;

    //	private RadioButtonGroup<String> publicAdministrationField;
    private TextField invoicingField;
    private List<BinaryResourceTransferField> pictureFields;

    private Map<BinaryResourceTransferField, BinaryResource> pictureFieldMap;
    private Map<Long, Map<Long, CriterionGroupLayout>> criterionFields;
    private Map<Long, DateField> evaluationDateFields;
    private DateField evaluationDateField;

    private TextField xesBalanceUrlField;

    // ...	Custom fields tab.
    private Map<ModelAttributeDef, AbstractComponent> customFieldsMap;
    private Binder<CustomFields> customFieldsBinder;
    private CustomFields customFields;

    // ...	Entity history tab.
    private Grid<EntityStatusChange> entityStatusChangesField;
    private Grid<SocialEconomyEntity> siblingEntitiesField;

    // ...	Fesc tab.
    private ComboBox<EntityStatus> fescPreviousEntityStatusField;
    private ComboBox<EntityStatus> fescEntityStatusField;
    private TextField fescEmailField;
    private TextField fescKeywordTagsField;
    private TextField fescinvoiceNameField;
    private TextField fescinvoiceEmailField;
    private TextField fescinvoiceAddressField;
    private TextField fescinvoicePostalCodeField;
    private ComboBox<FescRegistrationModality> fescInvoiceModalityField;
    private TextField fescInvoiceFellowNameField;
    private ComboBox<FescTimeSlot> timeSlotField;
    private TextField fescPreviousFeeField;
    private TextField fescFeeField;
    private TextField fescinvoiceAccountField;
    private CheckBox fescRegistrationPolicyField;
    private DateField fescRegistrationPolicyDateField;
    private CheckBox fescParticipationPolicyField;
    private DateField fescParticipationPolicyDateField;

    private ComboBox<Sector> mainFescSectorField;

    // ...	Offices tab.
    private Grid<SocialEconomyEntity> officeEntitiesField;

    // ...	Notes slider
    private TextArea currentNoteField;
    private Panel notesHistoryPanel;
    private VerticalLayout notesHistoryField;

    private LinkedHashMap<String, Boolean> xesBalanceOptions;
    private LinkedHashMap<String, Boolean> publicAdministrationOptions;
    private EntityTokens entityTokens;
    private Binder<EntityTokens> entityTokensBinder;
    private List<ProductTagTokenizable> productTagTokenizables;
    private List<KeywordTagTokenizable> keywordTagTokenizables;
    private List<SocialEconomyNetworkTagTokenizable> socialEconomyNetworkTagTokenizables;
    private TwinColSelect<ExternalFilterTag> externalFilerTagsField;
    private TwinColSelect<EntityScope> entityScopesField;
    private Map<Long, CriterionLevel> criterionLevels;

    protected Button okDraftButton;
    protected Button okPendingButton;
    protected Button createDraftButton;
    protected Button createCopyButton;
    protected Button createOfficeButton;
    protected Button previousTabButton;
    protected Button nextTabButton;
    protected Button socialEconomyEvaluationButton;
    protected Button agroecologicalEvaluationButton;
    protected Button updateSocialEconomyEvaluationButton;
    protected Button updateAgroecologicalEvaluationButton;
    private Consumer<SocialEconomyEntityDetailsLayout> okDraftHandler;
    private Consumer<SocialEconomyEntityDetailsLayout> okPendingHandler;
    private Consumer<SocialEconomyEntityDetailsLayout> createDraftHandler;
    private Consumer<SocialEconomyEntityDetailsLayout> createCopyHandler;
    private Consumer<SocialEconomyEntityDetailsLayout> createOfficeHandler;

    private boolean changed;
    private EntityStatus previousStatus;

    private DateTimeFormatter fullDateTimeFormater;
    private DateTimeFormatter currentYearDateTimeFormater;
    private DateTimeFormatter todayDateTimeFormater;

    private Path uploadedPicturePath;

    private String value;
    private ArrayList<Component> personRoleRowFields;
    private Button intercoopAddLinkButton;

    //...socialEconomyPanelFields getters

    public ComboBox<SectorCcae> getSectorCcaeField() {
        return sectorCcaeField;
    }

    public ExtTokenField getTagsField() {
        return tagsField;
    }

    public ExtTokenField getKeywordsField() {
        return keywordsField;
    }

    public ComboBox<LegalForm> getLegalFormField() {
        return legalFormField;
    }

    public PamapamTwinColGrid<SocialEconomyNetwork> getSocialEconomyNetworksField() {
        return socialEconomyNetworksField;
    }

    public ExtTokenField getSocialEconomyNetworkTagsField() {
        return socialEconomyNetworkTagsField;
    }

    public RadioButtonGroup<String> getXesBalanceField() {
        return xesBalanceField;
    }

    public TextArea getIntercooperacioField() {
        return intercooperacioField;
    }

    public TextField getXesBalanceUrlField() {
        return xesBalanceUrlField;
    }

    public TextField getInvoicingField() {
        return invoicingField;
    }

    public ComboBox<IdeologicalIdentification> getIdeologicalIdentificationField() {
        return ideologicalIdentificationField;
    }

    public Map<Long, PersonRoleRow> getPersonRoleRows() {
        return personRoleRows;
    }

    public ArrayList<Component> getPersonRoleRowFields() {
        return personRoleRowFields;
    }

    public Button getIntercoopAddLinkButton() {
        return intercoopAddLinkButton;
    }

    // Entity Evaluations Panels getters

    public Map<Long, CrudDetailsPanel> getEntityEvaluationPanels() {
        return entityEvaluationPanels;
    }

    @PostConstruct
    protected void init() {
        this.productTagTokenizables = new ArrayList<>();
        this.keywordTagTokenizables = new ArrayList<>();
        this.pictureFields = new ArrayList<>();
        this.pictureFieldMap = new HashMap<>();
        this.criterionFields = Maps.newHashMap();
        this.evaluationDateFields = Maps.newHashMap();

        this.socialEconomyNetworkTagTokenizables = new ArrayList<>();
        this.initializeEntityTokens();
        this.initializeCriterionLevels();
        this.initializeXesBalanceOptions();
        this.initializePublicAdministrationOptions();
        this.initializeProductTagTokenizables();
        this.initializeKeywordTagTokenizables();
        this.initializeSocialEconomyNetworkTagTokenizables();
        this.fullDateTimeFormater = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.currentYearDateTimeFormater = DateTimeFormatter.ofPattern("MM-dd HH:mm");
        this.todayDateTimeFormater = DateTimeFormatter.ofPattern("HH:mm");
        this.initializeCustomFields();
    }

    @Override
    protected void addTabs() {
        super.addTabs();
        this.tabSheet.addSelectedTabChangeListener(event -> this.updateNavigationTabButtons());
    }

    private void updateNavigationTabButtons() {
        final int currentTabIndex = this.tabSheet.getTabPosition(this.tabSheet.getTab(this.tabSheet.getSelectedTab()));
        if (currentTabIndex >= (this.tabSheet.getComponentCount() - 1)) {
            this.nextTabButton.setVisible(false);
        } else {
            this.nextTabButton.setVisible(true);
        }
        if (currentTabIndex == 0) {
            this.previousTabButton.setVisible(false);
        } else {
            this.previousTabButton.setVisible(true);
        }
    }

    @Override
    protected Component createTabSheet() {
        final VerticalLayout tabSheetWrapper = this.componentFactory.newVerticalLayout();
        tabSheetWrapper.setSizeFull();
        tabSheetWrapper.setMargin(false);
        tabSheetWrapper.setSpacing(false);
        tabSheetWrapper.setId("wrapper");

        this.tabSheet = this.componentFactory.newTabSheet();
        this.tabSheet.setSizeFull();
        tabSheetWrapper.addComponent(this.tabSheet);
        tabSheetWrapper.setExpandRatio(this.tabSheet, 1);

        if (this.createNotesSlider() != null) {
            tabSheetWrapper.addComponent(this.createNotesSlider());
        }

        this.addComponent(tabSheetWrapper);
        this.setExpandRatio(tabSheetWrapper, 1);
        return tabSheetWrapper;
    }

    protected SliderPanel createNotesSlider() {
        final HorizontalLayout notesLayout = this.componentFactory.newHorizontalLayout();
        notesLayout.setMargin(true);
        notesLayout.setWidth(100, Unit.PERCENTAGE);
        notesLayout.setHeight(300, Unit.PIXELS);
        this.currentNoteField = SocialEconomyEntityDetailsLayout.createTextArea(null);
        this.currentNoteField.setPlaceholder(this.messageSource.getMessage("socialEconomyEntity.notes.placeholder"));
        this.currentNoteField.setSizeFull();
        this.currentNoteField.setRows(20);
        notesLayout.addComponent(this.currentNoteField);
        this.notesHistoryPanel = this.componentFactory.newPanel();
        this.notesHistoryPanel.setSizeFull();
        this.notesHistoryPanel.addStyleName("j-notes-history");
        this.notesHistoryField = this.componentFactory.newVerticalLayout();
        this.notesHistoryPanel.setContent(this.notesHistoryField);
        notesLayout.addComponent(this.notesHistoryPanel);

        return new SliderPanelBuilder(notesLayout, this.messageSource.getMessage("socialEconomyEntity.notes")).mode(SliderMode.BOTTOM)
                .tabPosition(SliderTabPosition.END)
                .flowInContent(true)
                .autoCollapseSlider(true)
                .animationDuration(100)
                .style("j-notes-slider")
                .zIndex(9980)
                .build();
    }

    private Component createEntityNoteRow(final SocialEconomyEntityNote entityNote) {
        final VerticalLayout noteBox = this.componentFactory.newVerticalLayout();
        noteBox.setMargin(new MarginInfo(false, false, false, false));
        noteBox.setSpacing(false);
        final HorizontalLayout noteHeader = this.componentFactory.newHorizontalLayout();
        noteHeader.setSizeFull();
        noteHeader.setMargin(false);
        final LocalDateTime noteLocalDateTime = entityNote.getTimestamp().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        final Label noteTimestampLabel = this.componentFactory.newLabel();
        noteTimestampLabel.setValue("(" + this.formatDateTime(noteLocalDateTime) + ")");

        final Label noteUserLabel = this.componentFactory.newLabel();
        noteUserLabel.addStyleName("j-note-box-header");
        final StringBuilder noteUserLabelBuilder = new StringBuilder();
        noteUserLabelBuilder
                .append("(").append(this.formatDateTime(noteLocalDateTime)).append(") ")
                .append(entityNote.getUser() != null ? entityNote.getUser().getFullName() : "unknown").append(":");
        noteUserLabel.setValue(noteUserLabelBuilder.toString());

        noteBox.addComponent(noteUserLabel);
        final Panel noteTextPanel = new Panel();
        noteTextPanel.setWidth(90, Unit.PERCENTAGE);
        noteTextPanel.addStyleName("j-note-text");
        final Label noteTextLabel = this.componentFactory.newLabel();
        noteTextLabel.setSizeFull();
        noteTextLabel.setValue(entityNote.getText());
        noteTextLabel.addStyleName("j-note-text");
        noteTextPanel.setContent(noteTextLabel);
        noteBox.addComponent(noteTextPanel);

        if (this.pamapamSession.getCurrentUser().equals(entityNote.getUser())) {
            noteBox.setComponentAlignment(noteUserLabel, Alignment.MIDDLE_LEFT);
            noteBox.setComponentAlignment(noteTextPanel, Alignment.MIDDLE_LEFT);
        } else {
            noteBox.setComponentAlignment(noteUserLabel, Alignment.MIDDLE_RIGHT);
            noteBox.setComponentAlignment(noteTextPanel, Alignment.MIDDLE_RIGHT);
        }

        return noteBox;
    }

    private String formatDateTime(final LocalDateTime dateTime) {
        final LocalDateTime now = LocalDateTime.now();
        if (dateTime.toLocalDate().equals(now.toLocalDate())) {
            return this.todayDateTimeFormater.format(dateTime);
        } else if (dateTime.getYear() == now.getYear()) {
            return this.currentYearDateTimeFormater.format(dateTime);
        } else {
            return this.fullDateTimeFormater.format(dateTime);
        }
    }

    @Override
    protected List<Button> createActionButtons() {
        final List<Button> buttons = super.createActionButtons();

        // ... FIXME: Button captions must be seteable by caller.
        this.okButton.setCaption(this.messageSource.getMessage("action.save"));
        // ... FIXME: Button listeners must be overwritables.
        final Collection<?> clickListeners = this.okButton.getListeners(ClickEvent.class);
        clickListeners.forEach(each -> this.okButton.removeClickListener((Button.ClickListener) each));
        this.okButton.addClickListener(event -> this.attemptToSave(() -> this.doOk()));

        this.okDraftButton = this.componentBuilderFactory.createButtonBuilder().build();
        this.okDraftButton.setCaption(this.messageSource.getMessage("action.saveDraft"));
        this.okDraftButton.addStyleName("background-yellow");
        this.okDraftButton.addClickListener(event -> this.attemptToSave(() -> this.doOkDraft()));

        this.okPendingButton = this.componentBuilderFactory.createButtonBuilder().build();
        this.okPendingButton.setCaption(this.messageSource.getMessage("action.savePending"));
        this.okPendingButton.addStyleName("background-green");
        this.okPendingButton.addClickListener(event -> this.attemptToSave(() -> this.doOkPending()));

        this.createDraftButton = this.componentBuilderFactory.createButtonBuilder().build();
        this.createDraftButton.setCaption(this.messageSource.getMessage("action.createDraft"));
        this.createDraftButton.addStyleName("background-yellow");
        this.createDraftButton.addClickListener(event -> this.doCreateDraft());

        this.createCopyButton = this.componentBuilderFactory.createButtonBuilder().build();
        this.createCopyButton.setCaption(this.messageSource.getMessage("action.createCopy"));
        this.createCopyButton.addClickListener(event -> this.doCreateCopy());

        this.createOfficeButton = this.componentBuilderFactory.createButtonBuilder().build();
        this.createOfficeButton.setCaption(this.messageSource.getMessage("action.createOffice"));
        this.createOfficeButton.addClickListener(event -> this.doCreateOffice());

        this.nextTabButton = this.componentBuilderFactory.createButtonBuilder().build();
        this.nextTabButton.setCaption(this.messageSource.getMessage("action.nextTab"));
        this.nextTabButton.addClickListener(event -> this.doNextTab());

        this.previousTabButton = this.componentBuilderFactory.createButtonBuilder().build();
        this.previousTabButton.setCaption(this.messageSource.getMessage("action.previousTab"));
        this.previousTabButton.addClickListener(event -> this.doPreviousTab());
        this.previousTabButton.setVisible(false);

        buttons.add(buttons.indexOf(this.cancelButton), this.okDraftButton);
        buttons.add(buttons.indexOf(this.cancelButton), this.okPendingButton);
        buttons.add(buttons.indexOf(this.cancelButton), this.createDraftButton);
        buttons.add(buttons.indexOf(this.cancelButton), this.createCopyButton);
        buttons.add(buttons.indexOf(this.cancelButton), this.createOfficeButton);
        buttons.add(buttons.indexOf(this.cancelButton), this.previousTabButton);
        buttons.add(buttons.indexOf(this.cancelButton), this.nextTabButton);
        return buttons;
    }

    public void createEntityEvaluationButtons(final SocialEconomyEntity socialEconomyEntity) {
        this.socialEconomyEvaluationButton = this.componentBuilderFactory.createButtonBuilder()
                .setCaption("buttons.socialeconomy.evaluation.ess")
                .build();
        final EntityStatus draftStatus = this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.DRAFT);

        this.socialEconomyEvaluationButton.addClickListener(listener -> {
            final Questionaire socialEconomy = this.questionaireRepository.findOne(1L);
            socialEconomyEntity.setQuestionaire(socialEconomy);
            socialEconomyEntity.setEntityEvaluation(this.createEntityEvaluation(socialEconomyEntity));

            this.entityStatusField.setValue(draftStatus);
            this.loadEvaluationValues(socialEconomyEntity, socialEconomyEntity.getEntityEvaluation());
            this.updateOkButtonColor(draftStatus);
            this.setVisibleAllEntityEvaluationPanels(false);
            this.setVisible(this.entityEvaluationPanels.get(socialEconomyEntity.getQuestionaire().getId()), true);
        });

        this.agroecologicalEvaluationButton = this.componentBuilderFactory.createButtonBuilder()
                .setCaption("buttons.socialeconomy.evaluation.agroecological")
                .build();

        this.agroecologicalEvaluationButton.addClickListener(listener -> {
            final Questionaire agroecological = this.questionaireRepository.findOne(2L);
            socialEconomyEntity.setQuestionaire(agroecological);
            socialEconomyEntity.setEntityEvaluation(this.createEntityEvaluation(socialEconomyEntity));

            this.entityStatusField.setValue(draftStatus);
            this.loadEvaluationValues(socialEconomyEntity, socialEconomyEntity.getEntityEvaluation());
            this.updateOkButtonColor(draftStatus);
            this.setVisibleAllEntityEvaluationPanels(false);
            this.setVisible(this.entityEvaluationPanels.get(socialEconomyEntity.getQuestionaire().getId()), true);
        });
    }

    public void createUpdateEntityEvaluationButtons() {
        this.updateSocialEconomyEvaluationButton = this.componentBuilderFactory.createButtonBuilder()
                .setCaption("buttons.socialeconomy.evaluation.ess")
                .build();
        final EntityStatus draftStatus = this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.DRAFT);

        final boolean hasQuestionaireAlready = this.targetObject != null && this.targetObject.getQuestionaire() != null;
        final EntityEvaluation oldEntityEvaluation = this.targetObject.getEntityEvaluation();

        this.updateSocialEconomyEvaluationButton.addClickListener(listener -> {
            final Questionaire socialEconomy = this.questionaireRepository.findOne(1L);
            final EntityEvaluation newEntityEvaluation = this.createEntityEvaluation(this.targetObject);

            this.targetObject.setQuestionaire(socialEconomy);

            if (this.targetObject != null) {
                this.targetObject.setQuestionaire(socialEconomy);
            }

            if (hasQuestionaireAlready) {
                this.entityEvaluationcopyAllCommonCriterionAndAnswers(oldEntityEvaluation, newEntityEvaluation, socialEconomy);
            }

            this.targetObject.setEntityEvaluation(newEntityEvaluation);

            this.entityStatusField.setValue(draftStatus);
            this.loadEvaluationValues(this.targetObject, this.targetObject.getEntityEvaluation());
            this.updateOkButtonColor(draftStatus);
            this.setVisibleAllEntityEvaluationPanels(false);
            this.setVisible(this.entityEvaluationPanels.get(this.targetObject.getQuestionaire().getId()), true);
        });

        this.updateAgroecologicalEvaluationButton = this.componentBuilderFactory.createButtonBuilder()
                .setCaption("buttons.socialeconomy.evaluation.agroecological")
                .build();

        this.updateAgroecologicalEvaluationButton.addClickListener(listener -> {
            final Questionaire agroecological = this.questionaireRepository.findOne(2L);
            final EntityEvaluation newEntityEvaluation = this.createEntityEvaluation(this.targetObject);

            this.targetObject.setQuestionaire(agroecological);

            if (this.targetObject != null) {
                this.targetObject.setQuestionaire(agroecological);
            }

            if (hasQuestionaireAlready) {
                this.entityEvaluationcopyAllCommonCriterionAndAnswers(oldEntityEvaluation, newEntityEvaluation, agroecological);
            }

            this.targetObject.setEntityEvaluation(newEntityEvaluation);

            this.entityStatusField.setValue(draftStatus);
            this.loadEvaluationValues(this.targetObject, this.targetObject.getEntityEvaluation());
            this.updateOkButtonColor(draftStatus);
            this.setVisibleAllEntityEvaluationPanels(false);
            this.setVisible(this.entityEvaluationPanels.get(this.targetObject.getQuestionaire().getId()), true);
        });
    }

    private void attemptToSave(final Runnable action) {
        if (this.targetObject.getMainOffice() == null && this.mainOfficeField.getValue() != null) {
            MessageBox.createQuestion()
                    .withCaption(this.messageSource.getMessage("dialog.mainOfficeSetted"))
                    .withMessage(this.messageSource.getMessage("dialog.areyousure.mainOfficeSetted"))
                    .withYesButton(action, ButtonOption.caption(this.messageSource.getMessage("dialog.yes")))
                    .withNoButton(ButtonOption.caption(this.messageSource.getMessage("dialog.no")))
                    .open();
        } else {
            action.run();
        }
    }

    @Override
    protected void doOk() {
        if (this.okHandler != null) {
            this.setChanged(this.binder.hasChanges());
            this.previousStatus = this.targetObject.getEntityStatus();
            try {

                this.updateTargetObject();
                final EntityStatusType selectedEntityStatusType = Optional.ofNullable(this.entityStatusField.getValue())
                        .map(EntityStatus::getEntityStatusType).orElse(null);

                if (selectedEntityStatusType.equals(EntityStatusType.PREPUBLISHED)) {
                    final Timestamp validTo = Timestamp.valueOf("9999-12-31 23:59:59.000");
                    final UserActionToken userActionToken = this.userActionTokenService.generateEntityToken(this.targetObject, "conditionsAcceptance", validTo);
                    final UserActionToken updatedToken = this.userActionTokenService.save(userActionToken);
                    if (updatedToken != null) {
                        this.logger.info("New generated token " + userActionToken.toString());
                    }

                    this.mailService.sendConditionsAcceptanceMail(userActionToken);
                }
            } catch (final EmptyEmailException | ValidationException e) {
                SocialEconomyEntityDetailsLayout.logger.error(e.getMessage(), e);
                JmgNotification.show(e.getMessage(), Type.ERROR_MESSAGE);
                return;
            }
            this.okHandler.accept(this);
        }
    }

    protected void doOkDraft() {
        if (this.okDraftHandler != null) {
            this.setChanged(this.binder.hasChanges());
            this.previousStatus = this.targetObject.getEntityStatus();
            try {
                this.updateTargetObject();
            } catch (final ValidationException e) {
                SocialEconomyEntityDetailsLayout.logger.error(e.getMessage(), e);
                JmgNotification.show(this.messageSource.getMessage("dialog.hasErrors"), Type.ERROR_MESSAGE);
            }
            this.okDraftHandler.accept(this);
        }
    }

    protected void doOkPending() {
        if (this.okPendingHandler != null) {
            this.setChanged(this.binder.hasChanges());
            this.previousStatus = this.targetObject.getEntityStatus();
            try {
                this.updateTargetObject();
            } catch (final ValidationException e) {
                SocialEconomyEntityDetailsLayout.logger.error(e.getMessage(), e);
                JmgNotification.show(this.messageSource.getMessage("dialog.hasErrors"), Type.ERROR_MESSAGE);
            }
            this.okPendingHandler.accept(this);
        }
    }

    protected void doCreateDraft() {
        if (this.createDraftHandler != null) {
            this.createDraftHandler.accept(this);
        }
    }

    protected void doCreateCopy() {
        if (this.createCopyHandler != null) {
            this.createCopyHandler.accept(this);
        }
    }

    protected void doCreateOffice() {
        if (this.createOfficeHandler != null) {
            this.createOfficeHandler.accept(this);
        }
    }

    private void doNextTab() {
        final int currentTabIndex = this.tabSheet.getTabPosition(this.tabSheet.getTab(this.tabSheet.getSelectedTab()));
        if (currentTabIndex < (this.tabSheet.getComponentCount() - 1)) {
            this.tabSheet.setSelectedTab(currentTabIndex + 1);
        }
    }

    private void doPreviousTab() {
        final int currentTabIndex = this.tabSheet.getTabPosition(this.tabSheet.getTab(this.tabSheet.getSelectedTab()));
        if (currentTabIndex > 0) {
            this.tabSheet.setSelectedTab(currentTabIndex - 1);
        }
    }

    protected void emphasizeSaveButtons() {
        this.okButton.addStyleName("emphasized");
        this.okDraftButton.addStyleName("emphasized");
    }

    protected void deemphasizeSaveButtons() {
        this.okButton.removeStyleName("emphasized");
        this.okDraftButton.removeStyleName("emphasized");
    }

    private void initializeProductTagTokenizables() {
        final List<ProductTag> productTags = this.productTagService.findAll();
        Collections.sort(productTags, TextTag.TEXT_ORDER);
        for (final ProductTag eachProductTag : productTags) {
            final ProductTagTokenizable productTagTokenizable = new ProductTagTokenizable(eachProductTag);
            if (!this.productTagTokenizables.contains(productTagTokenizable)) {
                this.productTagTokenizables.add(productTagTokenizable);
            }
        }
    }

    private void initializeKeywordTagTokenizables() {
        final List<KeywordTag> keywordTags = this.keywordTagService.findAll();
        Collections.sort(keywordTags, TextTag.TEXT_ORDER);
        for (final KeywordTag eachKeywordTag : keywordTags) {
            final KeywordTagTokenizable keywordTagTokenizable = new KeywordTagTokenizable(eachKeywordTag);
            if (!this.keywordTagTokenizables.contains(keywordTagTokenizable)) {
                this.keywordTagTokenizables.add(keywordTagTokenizable);
            }
        }
    }

    private void initializeSocialEconomyNetworkTagTokenizables() {
        final List<SocialEconomyNetworkTag> socialEconomyNetworkTags = this.socialEconomyNetworkTagRepository.findAll();
        Collections.sort(socialEconomyNetworkTags, TextTag.TEXT_ORDER);
        for (final SocialEconomyNetworkTag eachSocialEconomyNetworkTag : socialEconomyNetworkTags) {
            final SocialEconomyNetworkTagTokenizable socialEconomyNetworkTagTokenizable = new SocialEconomyNetworkTagTokenizable(eachSocialEconomyNetworkTag);
            if (!this.socialEconomyNetworkTagTokenizables.contains(socialEconomyNetworkTagTokenizable)) {
                this.socialEconomyNetworkTagTokenizables.add(socialEconomyNetworkTagTokenizable);
            }
        }
    }

    @Override
    public void updateFields(final Object object) {
        final SocialEconomyEntity socialEconomyEntity = (SocialEconomyEntity) object;
        this.uploadedPicturePath = null;

        if (socialEconomyEntity != null) {
            this.updateMainFields(socialEconomyEntity);
            this.updateCommunicationFields(socialEconomyEntity);

            this.updateSocialEconomyFields(socialEconomyEntity);
            this.updateCustomFields(socialEconomyEntity);
            this.updateEntityEvaluationFields(socialEconomyEntity);
            this.updateFescFields(socialEconomyEntity);
            this.updateHistoryFields(socialEconomyEntity);
            this.updateOfficesFields(socialEconomyEntity);

            this.entityTokensBinder.readBean(this.entityTokens);
            this.decorateComponents(socialEconomyEntity);
        }

        super.updateFields(socialEconomyEntity);
        this.deemphasizeSaveButtons();
        this.binder.addValueChangeListener(event -> this.emphasizeSaveButtons());
    }

    private void updateMainFields(final SocialEconomyEntity socialEconomyEntity) {
        if (socialEconomyEntity.getEntityStatus() != null) {
            final EntityStatus entityStatus = socialEconomyEntity.getEntityStatus();
            @SuppressWarnings("unchecked") final ListDataProvider<EntityStatus> entityStatusProvider = (ListDataProvider<EntityStatus>) this.entityStatusField.getDataProvider();
            final Set<EntityStatus> availableStatuses = new HashSet<>();
            final VaadinRequest currentRequest = VaadinService.getCurrentRequest();
            if (currentRequest.isUserInRole(PamapamUser.ROLE_ADMIN)) {
                availableStatuses.addAll(this.entityStatusRepository.findAll());
            } else if (currentRequest.isUserInRole(PamapamUser.ROLE_COORDINATION) ||
                    currentRequest.isUserInRole(PamapamUser.ROLE_SUPERXINXETA)) {
                availableStatuses.addAll(entityStatus.getNextStatuses());
            } else if (currentRequest.isUserInRole(PamapamUser.ROLE_XINXETA) && currentRequest.isUserInRole(PamapamUser.ROLE_EXTERNAL)) {
                availableStatuses.add(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.DRAFT));
                availableStatuses.add(this.entityStatusRepository.findFirstByEntityStatusType(EntityStatusType.EXTERNAL));
                availableStatuses.addAll(entityStatus.getNextStatuses());
            } else {
                final List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();
                availableStatuses.add(entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.REVIEW_PENDING).findFirst().get());
                if (currentRequest.isUserInRole(PamapamUser.ROLE_EXTERNAL)) {
                    availableStatuses.add(entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.EXTERNAL).findFirst().get());
                }
            }
            entityStatusProvider.setFilter(item -> {
                return item.equals(entityStatus) || availableStatuses.contains(item);
            });
        } else {
            final List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();
            final EntityStatus initialStatus;
            if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_INITIATIVE)) {
                initialStatus = entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.INITIATIVE).findFirst().get();
            } else {
                initialStatus = entityStatuses.stream().filter(each -> each.getEntityStatusType() == EntityStatusType.DRAFT).findFirst().get();
            }
            socialEconomyEntity.setEntityStatus(initialStatus);
            @SuppressWarnings("unchecked") final ListDataProvider<EntityStatus> entityStatusProvider = (ListDataProvider<EntityStatus>) this.entityStatusField.getDataProvider();
            entityStatusProvider.setFilter(item -> item.equals(initialStatus));
            this.entityStatusField.setSelectedItem(initialStatus);
        }

        if (socialEconomyEntity.getEntityStatus().isDraft()) {
            this.addStyleName("draft-entity");
        } else {
            this.removeStyleName("draft-entity");
        }

        byte[] pictureBytes = null;
        if (socialEconomyEntity.getPicture() != null) {
            pictureBytes = socialEconomyEntity.getPicture().getContents();
        }

        if (pictureBytes != null) {
            final InputStream pictureInputStream = new ByteArrayInputStream(pictureBytes);
            this.showImage(pictureInputStream, socialEconomyEntity.getPicture().getFileName());
        } else {
            this.pictureField.setSource(new ThemeResource("images/placeholder.png"));
        }

        if (socialEconomyEntity.getRegistryDate() == null) {
            socialEconomyEntity.setRegistryDate(new Date());
        }

        final Region region = socialEconomyEntity.getRegion();
        if (region != null) {
            this.provinceField.setValue(region.getProvince());
            this.regionField.setValue(region);
        } else {
            this.provinceField.setValue(null);
            this.regionField.setValue(null);
        }

        this.updateMap(socialEconomyEntity.getLatitude(), socialEconomyEntity.getLongitude());

        if (this.currentNoteField != null) {
            this.currentNoteField.setValue("");
        }

        if (this.notesHistoryField != null) {
            this.notesHistoryField.removeAllComponents();
        }

        if (!CollectionUtils.isEmpty(socialEconomyEntity.getNotes())) {
            for (final SocialEconomyEntityNote eachEntityNote : socialEconomyEntity.getNotes()) {
                this.notesHistoryField.addComponent(this.createEntityNoteRow(eachEntityNote));
            }
            this.notesHistoryPanel.setScrollTop(Integer.MAX_VALUE);
        }
    }

    private void updateCommunicationFields(final SocialEconomyEntity socialEconomyEntity) {
        // ...	Product Tags.

        if (CollectionUtils.isEmpty(socialEconomyEntity.getProductTag())) {
            socialEconomyEntity.setProductTag(Sets.newHashSet());
        }

        final List<ProductTagTokenizable> tagsTokens = new ArrayList<>();
        for (final ProductTag eachProductTag : socialEconomyEntity.getProductTag()) {
            tagsTokens.add(new ProductTagTokenizable(eachProductTag));
        }
        this.entityTokens.setProductTokens(tagsTokens);

        if (socialEconomyEntity.getKeywordTags() != null) {
            final List<KeywordTagTokenizable> keywordTagsTokens = new ArrayList<>();
            for (final KeywordTag eachKeywordTag : socialEconomyEntity.getKeywordTags()) {
                keywordTagsTokens.add(new KeywordTagTokenizable(eachKeywordTag));
            }
            this.entityTokens.setKeywordTokens(keywordTagsTokens);
        }

        this.pictureFieldMap.clear();
        this.pictureFields.stream().forEach(each -> each.setValue(null));
        if (!CollectionUtils.isEmpty(socialEconomyEntity.getProductPictures())) {
            socialEconomyEntity.getProductPictures().stream().forEach(picture -> {
                final BinaryResourceTransferField brTransferField = this.pictureFields.get(socialEconomyEntity.getProductPictures().indexOf(picture));
                brTransferField.setValue(picture);
                this.pictureFieldMap.put(brTransferField, picture);
            });
        }

        this.discountList.clear();
        if (!CollectionUtils.isEmpty(socialEconomyEntity.getDiscounts())) {
            this.discountList.addAll(socialEconomyEntity.getDiscounts());
        }
        this.discountListDataProvider.refreshAll();

        this.audiovisualList.clear();
        if (!CollectionUtils.isEmpty(socialEconomyEntity.getAudiovisualDocuments())) {
            this.audiovisualList.addAll(socialEconomyEntity.getAudiovisualDocuments());
        }
        this.audiovisualListDataProvider.refreshAll();
    }

    private void updateSocialEconomyFields(final SocialEconomyEntity socialEconomyEntity) {

        String xesBalanceValue = null;
        for (final Map.Entry<String, Boolean> eachEntry : this.xesBalanceOptions.entrySet()) {
            if (Objects.equals(eachEntry.getValue(), socialEconomyEntity.getXesBalance())) {
                xesBalanceValue = eachEntry.getKey();
                break;
            }
        }
        this.xesBalanceField.setSelectedItem(xesBalanceValue);

        if (CollectionUtils.isEmpty(socialEconomyEntity.getEntityPersonRoles())) {
            socialEconomyEntity.setEntityPersonRoles(Lists.newArrayList(this.createEntityPersonRoles(socialEconomyEntity)));
        }

        for (final EntityPersonRole eachEntityPersonRole : socialEconomyEntity.getEntityPersonRoles()) {
            final PersonRoleRow personRoleRow = this.personRoleRows.get(eachEntityPersonRole.getPersonRole().getId());
            personRoleRow.setFemaleCount(Optional.ofNullable(eachEntityPersonRole.getFemaleCount()).orElse(0));
            personRoleRow.setMaleCount(Optional.ofNullable(eachEntityPersonRole.getMaleCount()).orElse(0));
            personRoleRow.setOtherCount(Optional.ofNullable(eachEntityPersonRole.getOtherCount()).orElse(0));
            final Binder<PersonRoleRow> personRoleRowBinder = this.personRoleRowBinders.get(eachEntityPersonRole.getPersonRole().getId());
            personRoleRowBinder.readBean(personRoleRow);
        }
        this.intercooperacioLinksList.clear();
        if (!CollectionUtils.isEmpty(socialEconomyEntity.getIntercooperacioLinks())) {
            this.intercooperacioLinksList.addAll(socialEconomyEntity.getIntercooperacioLinks());
        }
        this.intercooperacioLinksListDataProvider.refreshAll();

        // ... Other social economy network tags.

        if (CollectionUtils.isEmpty(socialEconomyEntity.getOtherSocialEconomyNetworks())) {
            socialEconomyEntity.setOtherSocialEconomyNetworks(Sets.newHashSet());
        }

        final List<SocialEconomyNetworkTagTokenizable> networkTagsTokens = new ArrayList<>();
        for (final SocialEconomyNetworkTag eachNetworkTag : socialEconomyEntity.getOtherSocialEconomyNetworks()) {
            networkTagsTokens.add(new SocialEconomyNetworkTagTokenizable(eachNetworkTag));
        }
        this.entityTokens.setSocialEconomyNetworkTokens(networkTagsTokens);

        // ... External Filter tags.
        if ((socialEconomyEntity.getExternalFilterTags() != null) &&
                (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN) ||
                        VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_EXTERNAL))) {
            this.externalFilerTagsField.setValue(socialEconomyEntity.getExternalFilterTags());
        }

    }

    private void updateCustomFields(final SocialEconomyEntity socialEconomyEntity) {
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            final String customFieldsString = Optional.ofNullable(socialEconomyEntity.getCustomFields()).orElse("{}");
            final JsonNode rootNode;
            rootNode = objectMapper.readTree(customFieldsString);
            this.customFieldsMap.forEach((attributeDef, field) -> {
                try {
                    final String stringValue = Optional.ofNullable(rootNode.get(attributeDef.getName())).map(o -> o.asText()).orElse(null);
                    if (stringValue != null) {
                        switch (attributeDef.getType()) {
                            case STRING:
                                SocialEconomyEntityDetailsLayout.this.customFields.setStringValue(attributeDef, stringValue);
                                break;
                            case INTEGER:
                                final Integer integerValue = objectMapper.readValue(stringValue, Integer.class);
                                SocialEconomyEntityDetailsLayout.this.customFields.setIntegerValue(attributeDef, integerValue);
                                break;
                            case LONG:
                                final Long longValue = objectMapper.readValue(stringValue, Long.class);
                                SocialEconomyEntityDetailsLayout.this.customFields.setLongValue(attributeDef, longValue);
                                break;
                            case DECIMAL:
                                final BigDecimal bigDecimalValue = objectMapper.readValue(stringValue, BigDecimal.class);
                                SocialEconomyEntityDetailsLayout.this.customFields.setBigDecimalValue(attributeDef, bigDecimalValue);
                                break;
                            case BOOLEAN:
                                final Boolean booleanValue = objectMapper.readValue(stringValue, Boolean.class);
                                SocialEconomyEntityDetailsLayout.this.customFields.setBooleanValue(attributeDef, booleanValue);
                                break;
                            case DATETIME:
                                final Date dateValue = objectMapper.readValue(stringValue, Date.class);
                                SocialEconomyEntityDetailsLayout.this.customFields.setDateValue(attributeDef, dateValue);
                                break;
                            default:
                                break;
                        }
                    } else {
                        SocialEconomyEntityDetailsLayout.this.customFields.setValue(attributeDef, null);
                    }
                } catch (final IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            });
        } catch (final IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        this.customFieldsBinder.setBean(this.customFields);

    }

    private void setVisibleAllEntityEvaluationPanels(final boolean visible) {
        for (final CrudDetailsPanel entityEvaluationPanel : this.entityEvaluationPanels.values()) {
            this.setVisible(entityEvaluationPanel, visible);
        }
    }

    private void updateEntityEvaluationFields(final SocialEconomyEntity socialEconomyEntity) {
        if (socialEconomyEntity.getEntityEvaluation() == null) {
            socialEconomyEntity.setEntityEvaluation(this.createEntityEvaluation(socialEconomyEntity));
        }

        this.setVisibleAllEntityEvaluationPanels(false);

        if (socialEconomyEntity.getQuestionaire() != null) {
            this.setVisible(this.entityEvaluationPanels.get(socialEconomyEntity.getQuestionaire().getId()), true);
        }

        final EntityEvaluation entityEvaluation = socialEconomyEntity.getEntityEvaluation();

        if (entityEvaluation == null) {
            JmgNotification.show(this.messageSource.getMessage("error.entity-must-have-evaluation"), Type.ERROR_MESSAGE);
            SocialEconomyEntityDetailsLayout.logger.error("All entities must have evaluation and entity with id {} and name {} doesn't.", socialEconomyEntity.getId(), socialEconomyEntity.getName());
        }

        final LocalDate evaluationDate = Optional.ofNullable(this.targetObject)
                .map(SocialEconomyEntity::getEntityEvaluation)
                .map(EntityEvaluation::getDate)
                .map(o -> o.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .orElse(LocalDate.now());

        if (entityEvaluation.getDate() != null && socialEconomyEntity.getQuestionaire() != null) {
            this.evaluationDateFields.get(socialEconomyEntity.getQuestionaire().getId())
                    .setValue(evaluationDate);
        }

        this.loadEvaluationValues(socialEconomyEntity, entityEvaluation);
    }

    /**
     * Copy all answers' values from old evaluation to new evaluation when changing questionaire.
     * <p>
     * It copies values based on answers' order, it will copy possible answer with order 1 value on questionaire 1
     * to possible answer with order 1 value on questionaire 2 and viceversa.
     * <p>
     * Since we are doing this process basing on answers' order, it won't work correctly if we have two different sizes
     * of possible answers for a Criterion. It will store logs but not crash on that case.
     *
     * @param oldEntityEvaluation
     * @param newEntityEvaluation
     * @param questionaire
     */
    private void entityEvaluationcopyAllCommonCriterionAndAnswers(final EntityEvaluation oldEntityEvaluation, final EntityEvaluation newEntityEvaluation, final Questionaire questionaire) {

        final List<Criterion> newEvaluationCriterions = newEntityEvaluation.getEntityEvaluationCriterions().stream()
                .map(EntityEvaluationCriterion::getCriterion)
                .collect(Collectors.toList());

        int i = 0;
        for (final EntityEvaluationCriterion oldEntityEvaluationCriterion : oldEntityEvaluation.getEntityEvaluationCriterions()) {

            if (newEvaluationCriterions.contains(oldEntityEvaluationCriterion.getCriterion())) {
                final Questionaire otherQuestionaire = this.questionaireService.findTheOtherQuestionaire(questionaire);

                //Since now we have a List only with the answers with value, we build another one where the not answered
                //ones have 0 as value and sort it by the answer order setted on db
                final List<EntityEvaluationCriterionAnswer> oldAnswers = SocialEconomyEntityDetailsLayout.buildOldAnswersList(oldEntityEvaluationCriterion, otherQuestionaire);
                oldAnswers.stream().
                        sorted(EntityEvaluationCriterionAnswer.ANSWER_ORDER)
                        .collect(Collectors.toList());

                //Build a list with new criterionAnswers with all values setted to 0 and sort it by the answer order setted on db
                final List<EntityEvaluationCriterionAnswer> newAnswers = SocialEconomyEntityDetailsLayout.buildNewZeroValueAnswersList(newEntityEvaluation, questionaire, i);
                newAnswers.stream()
                        .sorted(EntityEvaluationCriterionAnswer.ANSWER_ORDER)
                        .collect(Collectors.toList());

                //Set new answers values based on answers order
                //We check both sizes because one could be bigger than the other and crash the app
                final int newAnswersSize = newAnswers.size();
                final int oldAnswersSize = oldAnswers.size();

                if (newAnswersSize != oldAnswersSize) {
                    logger.error("EntityEvaluationCriterionAnswer List for " + oldEntityEvaluationCriterion.getCriterion().getName() + " have different sizes " +
                            "for Agroecological and ESS evaluations. Evaluation data might not be correct.");
                }

                for (int index = 0; index < newAnswersSize && index < oldAnswersSize; index++) {
                    final int oldAnswerValue = oldAnswers.get(index).getValue();
                    newAnswers.get(index).setValue(oldAnswerValue);
                }

                //Set notes
                newEntityEvaluation.getEntityEvaluationCriterions().get(i)
                        .setNotes(oldEntityEvaluationCriterion.getNotes());

                //Set Level (idk what is this)
                newEntityEvaluation.getEntityEvaluationCriterions().get(i)
                        .setLevel(oldEntityEvaluationCriterion.getLevel());

                //Set each answer puntuation
                newEntityEvaluation.getEntityEvaluationCriterions().get(i)
                        .setEntityEvaluationCriterionAnswers(newAnswers);
            }

            i++;
        }

    }

    /**
     * Builds an EntityEvaluationCriterionAnswer List from CriterionAnswers related to a certain Questionaire and
     * sets all their values to 0.
     *
     * @param newEntityEvaluation
     * @param questionaire
     * @param index
     * @return
     */
    public static List<EntityEvaluationCriterionAnswer> buildNewZeroValueAnswersList(final EntityEvaluation newEntityEvaluation, final Questionaire questionaire, final int index) {
        final List<EntityEvaluationCriterionAnswer> newEmptyValueEntityEvaluationCriterionAnswers = new ArrayList<>();

        final List<CriterionAnswer> criterionAnswers = newEntityEvaluation.getEntityEvaluationCriterions()
                .get(index).getCriterion()
                .getCriterionAnswers(questionaire);

        for (final CriterionAnswer criterionAnswer : criterionAnswers) {
            final EntityEvaluationCriterion entityEvaluationCriterion = newEntityEvaluation.getEntityEvaluationCriterions().get(index);
            final EntityEvaluationCriterionAnswer newEmptyValueAnswer = new EntityEvaluationCriterionAnswer(entityEvaluationCriterion, criterionAnswer, 0);
            newEmptyValueEntityEvaluationCriterionAnswers.add(newEmptyValueAnswer);
        }

        return newEmptyValueEntityEvaluationCriterionAnswers;
    }

    /**
     * Since, when calling to database, we have an EntityEvaluationCriterionAnswer List only with actually answered questions
     * this method can build another list from that one filling not answered questions with 0 value, and answered ones with their value,
     * so we can compare with other kinds of questionaries.
     * <p>
     * Sets value to zero if it has none, and to current value if it has something.
     *
     * @param oldEntityEvaluationCriterion
     * @param questionaire
     * @return
     */
    public static List<EntityEvaluationCriterionAnswer> buildOldAnswersList(final EntityEvaluationCriterion oldEntityEvaluationCriterion, final Questionaire questionaire) {
        final List<EntityEvaluationCriterionAnswer> newEmptyValueEntityEvaluationCriterionAnswers = new ArrayList<>();

        final List<CriterionAnswer> criterionAnswers = oldEntityEvaluationCriterion.getCriterion()
                .getCriterionAnswers(questionaire);

        for (final CriterionAnswer criterionAnswer : criterionAnswers) {

            final EntityEvaluationCriterionAnswer answer = oldEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers()
                    .stream()
                    .filter(o -> o.getCriterionAnswer().equals(criterionAnswer))
                    .findFirst()
                    .orElse(null);

            final int value = Optional.ofNullable(answer)
                    .map(EntityEvaluationCriterionAnswer::getValue)
                    .orElse(0);

            final EntityEvaluationCriterionAnswer newValueAnswer = new EntityEvaluationCriterionAnswer(oldEntityEvaluationCriterion, criterionAnswer, value);
            newEmptyValueEntityEvaluationCriterionAnswers.add(newValueAnswer);
        }

        return newEmptyValueEntityEvaluationCriterionAnswers;
    }

    /**
     * ...	There are four evaluation versions:
     * <p>
     * 1) Original evaluations migrated from Drupal (with old questionaire criterion).
     * 2) New questionaire criteria with total value but without detailed answers values.
     * 3) New questionaire criteria with detailes answer values.
     * 4) Types of questionaire on table Questionaire
     * <p>
     * The evaluation fields are updated only for 2) and 3).
     * <p>
     * For the 2) case the total value is shown, but detailed answer values are empty. If the user selects an answer value,
     * the criterion group becomes 3).
     */
    private void loadEvaluationValues(final SocialEconomyEntity socialEconomyEntity, final EntityEvaluation entityEvaluation) {
        if (socialEconomyEntity.isOldVersion()) {
            return;
        }

        final List<EntityEvaluationCriterion> entityEvaluationCriteria = entityEvaluation.getEntityEvaluationCriterions();

        for (final EntityEvaluationCriterion eachEntityEvaluationCriterion : entityEvaluationCriteria) {
            final CriterionGroupLayout criterionGroupLayout = this.criterionFields.get(socialEconomyEntity.getQuestionaire().getId())
                    .get(eachEntityEvaluationCriterion.getCriterion().getId());
            criterionGroupLayout.setQuestionaire(socialEconomyEntity.getQuestionaire());
            criterionGroupLayout.resetAnswerValues();

            if ((eachEntityEvaluationCriterion.getLevel() != null) && (eachEntityEvaluationCriterion.getLevel() == -1)) {
                criterionGroupLayout.getNotAplicableField().setValue(true);
            } else if (eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers() != null) {
                criterionGroupLayout.getNotAplicableField().setValue(false);
                if (eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers().isEmpty() && (eachEntityEvaluationCriterion.getLevel() != null)) {
                    criterionGroupLayout.setLevel(eachEntityEvaluationCriterion.getLevel());
                } else {
                    for (final EntityEvaluationCriterionAnswer eachEntityEvaluationCriterionAnswer : eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers()) {
                        criterionGroupLayout.setAnswerValue(eachEntityEvaluationCriterionAnswer.getCriterionAnswer(), eachEntityEvaluationCriterionAnswer.getValue());
                    }
                }
                criterionGroupLayout.updateLevelLabels();
            }
            criterionGroupLayout.getNotesField().setValue(Optional.ofNullable(eachEntityEvaluationCriterion.getNotes()).orElse(""));
        }
    }

    private void updateFescFields(final SocialEconomyEntity socialEconomyEntity) {
        // Treballeu amb ...?
        if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
            this.focusedOnEnterprisesField.setValue(Optional.ofNullable(socialEconomyEntity.getEnterprises()).orElse(false));
            this.focusedOnPrivateActivistsField.setValue(Optional.ofNullable(socialEconomyEntity.getPrivateActivists()).orElse(false));
            this.focusedOnPrivateNoobsField.setValue(Optional.ofNullable(socialEconomyEntity.getPrivateNoobs()).orElse(false));
        }
        this.focusedOnPublicAdministrationField.setValue(Optional.ofNullable(socialEconomyEntity.getPublicAdministration()).orElse(false));

        if (socialEconomyEntity.getFescData() != null) {
            this.fescEntityStatusField.setValue(Optional.ofNullable(socialEconomyEntity.getFescEntityStatus()).orElse(null));
            this.fescEmailField.setValue(Optional.ofNullable(socialEconomyEntity.getFescEmail()).orElse(""));
            this.fescinvoiceNameField.setValue(Optional.ofNullable(socialEconomyEntity.getFescInvoiceName()).orElse(""));
            this.fescinvoiceEmailField.setValue(Optional.ofNullable(socialEconomyEntity.getFescInvoiceEmail()).orElse(""));
            this.fescinvoiceAddressField.setValue(Optional.ofNullable(socialEconomyEntity.getFescInvoiceAddress()).orElse(""));
            this.fescinvoicePostalCodeField.setValue(Optional.ofNullable(socialEconomyEntity.getFescInvoicePostalCode()).orElse(""));
            this.fescFeeField.setValue(Optional.ofNullable(socialEconomyEntity.getFescFee()).map(o -> o.toString()).orElse(""));
            this.fescinvoiceAccountField.setValue(Optional.ofNullable(socialEconomyEntity.getFescInvoiceAccount()).orElse(""));
            this.fescParticipationPolicyField.setValue(Optional.ofNullable(socialEconomyEntity.getFescParticipationPrivacyPolicy()).orElse(false));
            this.fescParticipationPolicyDateField.setValue(Optional.ofNullable(socialEconomyEntity.getFescParticipationPrivacyPolicyDate())
                    .map(p -> p.toLocalDateTime().toLocalDate()).orElse(null));
            this.fescRegistrationPolicyField.setValue(Optional.ofNullable(socialEconomyEntity.getFescRegistrationPrivacyPolicy()).orElse(false));
            this.fescRegistrationPolicyDateField.setValue(Optional.ofNullable(socialEconomyEntity.getFescRegistrationPrivacyPolicyDate())
                    .map(p -> p.toLocalDateTime().toLocalDate()).orElse(null));
        }
    }

    private void updateHistoryFields(final SocialEconomyEntity socialEconomyEntity) {
        if (socialEconomyEntity.getId() != null) {
            final List<EntityStatusChange> entityStatusChanges = socialEconomyEntity.getEntityStatusChanges();
            this.entityStatusChangesField.setHeightByRows(Math.max(1, entityStatusChanges.size()));
            this.entityStatusChangesField.setItems(entityStatusChanges);

            final List<SocialEconomyEntity> siblingEntities = this.socialEconomyEntityService.findSiblingEntities(socialEconomyEntity);
            this.siblingEntitiesField.setHeightByRows(Math.max(1, siblingEntities.size()));
            this.siblingEntitiesField.setItems(siblingEntities);
        } else {
            this.entityStatusChangesField.setHeightByRows(1);
            this.siblingEntitiesField.setHeightByRows(1);
        }

    }

    private void updateOfficesFields(final SocialEconomyEntity socialEconomyEntity) {
        if (socialEconomyEntity.getId() != null) {
            final List<SocialEconomyEntity> officeEntities = this.socialEconomyEntityService.findOfficeEntities(socialEconomyEntity);
            this.officeEntitiesField.setHeightByRows(Math.max(1, officeEntities.size()));
            this.officeEntitiesField.setItems(officeEntities);
        } else {
            this.officeEntitiesField.setHeightByRows(1);
        }

    }

    private void launchEntityEvaluationSelectionModal(final SocialEconomyEntity socialEconomyEntity) {
        if (socialEconomyEntity.getEntityStatus().isDraft() && !socialEconomyEntity.isOldVersion() && socialEconomyEntity.getQuestionaire() == null) {
            this.createEntityEvaluationButtons(socialEconomyEntity);

            MessageBox.createQuestion()
                    .withCaption(this.messageSource.getMessage("socialeconomyentity.select.evaluation"))
                    .withMessage(this.messageSource.getMessage("socialeconomyentity.select.evaluation.description"))
                    .asModal(true)
                    .withButton(this.socialEconomyEvaluationButton)
                    .withButton(this.agroecologicalEvaluationButton)
                    .asModal(true)
                    .open();
        }

    }

    private void decorateComponents(final SocialEconomyEntity socialEconomyEntity) {
        final EntityStatus entityStatus = socialEconomyEntity.getEntityStatus();
        final VaadinRequest currentRequest = VaadinService.getCurrentRequest();

        this.getPanels().forEach(each -> each.setEnabled(true));
        this.launchEntityEvaluationSelectionModal(socialEconomyEntity);
        final Tab officesTab = this.tabSheet.getTab(this.officesPanel);
        if (socialEconomyEntity.getMainOffice() != null && !socialEconomyEntity.isMainOffice()) {
            this.fieldsEnableHandler.disableSocialEconomyPanelFields(socialEconomyEntity, this);
            this.fieldsEnableHandler.disableEntityEvaluationPanelsFields(socialEconomyEntity, this);
            this.customFieldsPanel.setEnabled(false);
            this.fescPanel.setEnabled(false);
            //Optional.ofNullable(this.entityScopesField).ifPresent(o -> o.setEnabled(false));
            this.createOfficeButton.setVisible(false);
            officesTab.setVisible(false);
        } else {
            this.createOfficeButton.setVisible(true);
            final long officesCount = this.socialEconomyEntityService.countOfficeEntities(socialEconomyEntity);
            if (officesCount > 0) {
                officesTab.setVisible(true);
            } else {
                officesTab.setVisible(false);
            }
        }

        final EntityStatusType entityStatusType = entityStatus.getEntityStatusType();
        if ((entityStatusType == EntityStatusType.PUBLISHED) && !currentRequest.isUserInRole(PamapamUser.ROLE_INITIATIVE)) {
            this.createDraftButton.setVisible(true);
        } else {
            this.createDraftButton.setVisible(false);
        }

        this.okDraftButton.setVisible(false);
        this.okPendingButton.setVisible(false);
        this.updateOkButtonColor(entityStatus);

        if (currentRequest.isUserInRole(PamapamUser.ROLE_ADMIN) || currentRequest.isUserInRole(PamapamUser.ROLE_COORDINATION) || currentRequest.isUserInRole(PamapamUser.ROLE_POL_COOPERATIU)) {
            return;
        }

        // ...	Disable components not allowed by superxinxetas.

        this.createCopyButton.setVisible(false);
        this.createOfficeButton.setVisible(false);
        this.mainOfficeField.setEnabled(false);

        if (currentRequest.isUserInRole(PamapamUser.ROLE_SUPERXINXETA)) {
            return;
        }

        // ...	Disable components not allowed by xinxetas.

        this.registryDateField.setEnabled(false);
        this.entityStatusField.setEnabled(false);

        if (currentRequest.isUserInRole(PamapamUser.ROLE_INITIATIVE)) {
            return;
        }

        if (currentRequest.isUserInRole(PamapamUser.ROLE_EXTERNAL)) {
            this.okDraftButton.setVisible(false);
            this.okPendingButton.setVisible(false);
            this.okButton.setVisible(true);
            this.entityStatusField.setEnabled(true);
            return;
        }

        if ((entityStatusType == EntityStatusType.PUBLISHED) ||
                (entityStatusType == EntityStatusType.UNPUBLISHED) ||
                (entityStatusType == EntityStatusType.UPDATE_PENDING) ||
                (entityStatusType == EntityStatusType.EXTERNAL)) {
            this.getPanels().forEach(each -> each.setEnabled(false));
            this.map.setReadOnly(true);
            this.okDraftButton.setVisible(false);
            this.okPendingButton.setVisible(false);
            this.okButton.setVisible(false);
        } else if (entityStatusType == EntityStatusType.INCOMPLETE) {
            this.okDraftButton.setVisible(false);
            this.okPendingButton.setVisible(false);
            this.okButton.setVisible(true);
            this.entityStatusField.setEnabled(true);
        } else if (entityStatusType == EntityStatusType.REVIEW_PENDING) {
            this.okDraftButton.setVisible(false);
            this.okPendingButton.setVisible(true);
            this.okButton.setVisible(false);
        } else if (entityStatusType == EntityStatusType.DRAFT) {
            this.okDraftButton.setVisible(true);
            this.okPendingButton.setVisible(true);
            this.okButton.setVisible(false);
        } else {
            this.okDraftButton.setVisible(true);
            this.okPendingButton.setVisible(false);
            this.okButton.setVisible(false);
        }

        if (currentRequest.isUserInRole(PamapamUser.ROLE_XINXETA) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_XINXETA_EINATECA) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_EXTERNAL)) {
            //currentRequest.isUserInRole(PamapamUser.ROLE_POL_COOPERATIU)) {
            return;
        }

        // ...	Disable components not allowed by initiatives.

        this.okDraftButton.setVisible(false);
        this.okPendingButton.setVisible(false);
        this.okButton.setVisible(true);
    }

    private void updateOkButtonColor(final EntityStatus entityStatus) {
        final List<String> styleNames = Lists.newArrayList(StringUtils.split(this.okButton.getStyleName()));
        this.okButton.removeStyleNames(styleNames.stream().filter(each -> each.startsWith("background-")).toArray(String[]::new));
        this.okButton.addStyleName("background-" + entityStatus.getColor());
        this.okButton.setCaption(this.messageSource.getMessage("action.save") + " " + entityStatus.getName().getDefaultText());
    }

    private void pictureUploadReceived(final String fileName, final Path path) {
        this.uploadedPicturePath = path;
        this.pictureProgressBar.setVisible(false);
        this.showUploadedImage(fileName);
    }

    private void uploadStarted(final String fileName) {
        this.pictureProgressBar.setVisible(true);
    }

    private void uploadProgress(final String fileName, final long readBytes, final long contentLength) {
        if (readBytes > 0) {
            this.pictureProgressBar.setValue((float) contentLength / readBytes);
        }
    }

    private void uploadFailed(final String fileName, final Path file) {
        this.pictureProgressBar.setVisible(false);
    }

    private void showUploadedImage(final String fileName) {
        if (this.uploadedPicturePath != null) {
            try {
                final FileInputStream inputStream = new FileInputStream(this.uploadedPicturePath.toFile());
                this.showImage(inputStream, fileName);
            } catch (final Exception e) {
                // TODO: handle exception?
            }
        }
    }

    private void showImage(final InputStream imageInputStream, final String fileName) {
        final StreamResource resource = new StreamResource(() -> imageInputStream, Optional.ofNullable(fileName).orElse(imageInputStream.toString()));
        this.pictureField.setSource(resource);
    }

    private HorizontalLayout createPersonRoleGroup(final PersonRole personRole) {
        final HorizontalLayout personRoleGroupLayout = new HorizontalLayout();
        personRoleGroupLayout.setWidth(60, Unit.PERCENTAGE);
        personRoleGroupLayout.setCaption(String.valueOf(personRole.getName().getDefaultText()));
        final List<Component> personRoleComponents = Lists.newArrayList();

        final Binder<PersonRoleRow> entityPersonRoleRowBinder = new Binder<>(PersonRoleRow.class);
        final PersonRoleRow personRoleRow = new PersonRoleRow();
        entityPersonRoleRowBinder.setBean(personRoleRow);

        final TextField totalCountField = this.componentFactory.newTextField("socialEconomyEntity.personRoles.total");
        totalCountField.setWidth(90, Unit.PERCENTAGE);
        totalCountField.setEnabled(false);
        entityPersonRoleRowBinder.forField(totalCountField)
                .withConverter(new StringToIntegerConverter("Must enter a number"))
                .bind(PersonRoleRow::getTotalCount, null);

        final TextField femaleCountField = this.componentFactory.newTextField("socialEconomyEntity.personRoles.female");
        femaleCountField.setWidth(90, Unit.PERCENTAGE);
        entityPersonRoleRowBinder.forField(femaleCountField)
                .withConverter(new StringToIntegerConverter("Must enter a number"))
                .bind(PersonRoleRow::getFemaleCount, PersonRoleRow::setFemaleCount);
        femaleCountField.addValueChangeListener(event -> totalCountField.setValue(String.valueOf(personRoleRow.getTotalCount())));
        personRoleGroupLayout.addComponent(femaleCountField);
        personRoleComponents.add(femaleCountField);
        this.personRoleRowFields.add(femaleCountField);

        final TextField maleCountField = this.componentFactory.newTextField("socialEconomyEntity.personRoles.male");
        maleCountField.setWidth(90, Unit.PERCENTAGE);
        entityPersonRoleRowBinder.forField(maleCountField)
                .withConverter(new StringToIntegerConverter("Must enter a number"))
                .bind(PersonRoleRow::getMaleCount, PersonRoleRow::setMaleCount);
        maleCountField.addValueChangeListener(event -> totalCountField.setValue(String.valueOf(personRoleRow.getTotalCount())));
        personRoleGroupLayout.addComponent(maleCountField);
        personRoleComponents.add(maleCountField);
        this.personRoleRowFields.add(maleCountField);

        final TextField otherCountField = this.componentFactory.newTextField("socialEconomyEntity.personRoles.other");
        otherCountField.setWidth(90, Unit.PERCENTAGE);
        entityPersonRoleRowBinder.forField(otherCountField)
                .withConverter(new StringToIntegerConverter("Must enter a number"))
                .bind(PersonRoleRow::getOtherCount, PersonRoleRow::setOtherCount);
        otherCountField.addValueChangeListener(event -> totalCountField.setValue(String.valueOf(personRoleRow.getTotalCount())));
        personRoleGroupLayout.addComponent(otherCountField);
        personRoleComponents.add(otherCountField);
        this.personRoleRowFields.add(otherCountField);

        personRoleGroupLayout.addComponent(totalCountField);
        personRoleComponents.add(totalCountField);

        this.personRoleRows.put(personRole.getId(), personRoleRow);
        this.personRoleRowBinders.put(personRole.getId(), entityPersonRoleRowBinder);

        return personRoleGroupLayout;
    }

    private EntityEvaluation createEntityEvaluation(final SocialEconomyEntity entity) {
        final EntityEvaluation entityEvaluation = new EntityEvaluation();
        final List<EntityEvaluationCriterion> entityEvaluationCriterions = Lists.newArrayList();
        //No hay questionaire
        final Questionaire questionaire = entity.getQuestionaire();
        if (questionaire != null) {
            for (final Criterion eachCriterion : this.getAllCriteria()) {
                entityEvaluationCriterions.add(new EntityEvaluationCriterion(entityEvaluation, eachCriterion));
            }
        }
        entityEvaluation.setEntityEvaluationCriterions(entityEvaluationCriterions);
        return entityEvaluation;
    }

    private List<EntityPersonRole> createEntityPersonRoles(final SocialEconomyEntity entity) {
        final List<EntityPersonRole> entityPersonRoles = Lists.newArrayList();
        for (final PersonRole eachPersonRole : this.getAllPersonRoles()) {
            entityPersonRoles.add(new EntityPersonRole(entity, eachPersonRole));
        }
        return entityPersonRoles;
    }

    private List<Criterion> getAllCriteria() {
        return this.criterionRepository.findByOldVersionFalse(new Sort("viewOrder"));
    }

    private List<PersonRole> getAllPersonRoles() {
        List<PersonRole> allPersenRoles = Lists.newArrayList();
        try {
            allPersenRoles = this.datasourceServices.getAll(PersonRole.class);
            Collections.sort(allPersenRoles, PersonRole.ID_ORDER);
        } catch (final RepositoryForClassNotDefinedException e) {
            // ...	Do nothing. Leaves allCriteria list empty.
        }
        return allPersenRoles;
    }

    private static TextArea createTextArea(final String caption) {
        final TextArea field = new TextArea(caption);
        field.setSizeFull();
        field.setWordWrap(true);
        field.setWidth(100, Unit.PERCENTAGE);
        return field;
    }

    @Override
    public boolean hasChanges() {
        if (super.hasChanges()) {
            return true;
        }

        if (this.targetObject == null) {
            return false;
        }

        if (!Objects.equals(this.targetObject.getXesBalance(), this.xesBalanceOptions.get(this.xesBalanceField.getValue()))) {
            return true;
        }

        if (!Objects.equals(this.targetObject.getDescription(), this.descriptionField.getValue())) {
            return true;
        }

        if (this.uploadedPicturePath != null) {
            return true;
        }

        for (final EntityPersonRole eachEntityPersonRole : this.targetObject.getEntityPersonRoles()) {
            final PersonRoleRow personRoleRow = this.personRoleRows.get(eachEntityPersonRole.getPersonRole().getId());
            if ((Optional.ofNullable(eachEntityPersonRole.getFemaleCount()).orElse(0) != personRoleRow.getFemaleCount()) ||
                    (Optional.ofNullable(eachEntityPersonRole.getMaleCount()).orElse(0) != personRoleRow.getMaleCount()) ||
                    (Optional.ofNullable(eachEntityPersonRole.getOtherCount()).orElse(0) != personRoleRow.getOtherCount())) {
                return true;
            }
        }

        if (!this.targetObject.isOldVersion()) {
            final EntityEvaluation entityEvaluation = this.targetObject.getEntityEvaluation();
            for (final EntityEvaluationCriterion eachEntityEvaluationCriterion : entityEvaluation.getEntityEvaluationCriterions()) {
                final CriterionGroupLayout criterionGroupLayout = this.criterionFields.get(this.targetObject.getQuestionaire().getId()).get(eachEntityEvaluationCriterion.getCriterion().getId());
                criterionGroupLayout.setQuestionaire(this.targetObject.getQuestionaire());
                Map<CriterionAnswer, EntityEvaluationCriterionAnswer> currentAnswers = new HashMap<>();
                if (eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers() != null) {
                    currentAnswers = eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers()
                            .stream().collect(Collectors.toMap(o -> o.getCriterionAnswer(), o -> o));
                }
                for (final Map.Entry<CriterionAnswer, Label> eachEntry : criterionGroupLayout.getAnswerValueLabels().entrySet()) {
                    final CriterionAnswer eachCriterionAnswer = eachEntry.getKey();
                    final Integer eachCriterionAnswerValue = (Integer) eachEntry.getValue().getData();
                    final EntityEvaluationCriterionAnswer currentAnswer = currentAnswers.get(eachCriterionAnswer);
                    if ((currentAnswer == null) && (eachCriterionAnswerValue > 0)) {
                        return true;
                    } else if ((currentAnswer != null) && !currentAnswer.getValue().equals(eachCriterionAnswerValue)) {
                        return true;
                    }
                }
                final TextArea notesField = criterionGroupLayout.getNotesField();
                if (!Objects.equals(Optional.ofNullable(eachEntityEvaluationCriterion.getNotes()).orElse(""), notesField.getValue())) {
                    return true;
                }
            }
        }

        for (final ProductTagTokenizable eachTagToken : this.entityTokens.getProductTokens()) {
            if (!this.targetObject.getProductTag().contains(eachTagToken.getProductTag())) {
                return true;
            }

        }

        for (final SocialEconomyNetworkTagTokenizable eachTagToken : this.entityTokens.getSocialEconomyNetworkTokens()) {
            if (!this.targetObject.getOtherSocialEconomyNetworks().contains(eachTagToken.getSocialEconomyNetworkTag())) {
                return true;
            }
        }

        if (this.point != null) {
            if ((this.targetObject.getLatitude() != this.point.getY()) || (this.targetObject.getLongitude() != this.point.getX())) {
                return true;
            }
        }

        if (!StringUtils.isBlank(this.currentNoteField.getValue())) {
            return true;
        }

        return false;
    }

    @Override
    public void updateTargetObject() throws ValidationException {

        // ...	If changing back from office to main office, keep previous main office to copy inherited attributes.

        SocialEconomyEntity previousMainOffice = null;
        if (this.targetObject.getMainOffice() != null && this.mainOfficeField.getValue() == null) {
            previousMainOffice = this.targetObject.getMainOffice();
        }

        super.updateTargetObject();

        if (this.targetObject.getMainOffice() != null) {
            this.targetObject.setLegalForm(null);
            this.targetObject.setSectorCcae(null);
            this.targetObject.setOtherSocialEconomyNetworks(null);
            this.targetObject.setCollaborationEntities(null);
            this.targetObject.setXesBalance(null);
            this.targetObject.setEntityPersonRoles(null);
            this.targetObject.setInvoicing(null);
            this.targetObject.setCustomFields(null);
            this.targetObject.setEntityEvaluation(null);
            this.targetObject.setFescData(null);
        } else if (previousMainOffice != null) {
            this.targetObject.updateFromMainOffice(previousMainOffice);
        } else {
            this.targetObject.setXesBalance(this.xesBalanceOptions.get(this.xesBalanceField.getValue()));

            if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
                this.targetObject.setEnterprises(this.focusedOnEnterprisesField.getValue());
                this.targetObject.setPrivateActivists(this.focusedOnPrivateActivistsField.getValue());
                this.targetObject.setPrivateNoobs(this.focusedOnPrivateNoobsField.getValue());
            }
            this.targetObject.setPublicAdministration(Optional.ofNullable(this.focusedOnPublicAdministrationField.getValue()).orElse(false));

            for (final EntityPersonRole eachEntityPersonRole : this.targetObject.getEntityPersonRoles()) {
                final PersonRoleRow personRoleRow = this.personRoleRows.get(eachEntityPersonRole.getPersonRole().getId());
                eachEntityPersonRole.setFemaleCount(personRoleRow.getFemaleCount());
                eachEntityPersonRole.setMaleCount(personRoleRow.getMaleCount());
                eachEntityPersonRole.setOtherCount(personRoleRow.getOtherCount());
            }

            if (!this.targetObject.isOldVersion()) {
                final EntityEvaluation entityEvaluation = this.targetObject.getEntityEvaluation();
                if (this.targetObject.getQuestionaire() != null) {
                    entityEvaluation.setDate(Date.from(this.evaluationDateFields.get(this.targetObject.getQuestionaire().getId()).getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } else {
                    logger.warn("SocialEconomyEntity with id " + this.targetObject.getId() + " doesn't have an associated Questionaire");
                }

                for (final EntityEvaluationCriterion eachEntityEvaluationCriterion : entityEvaluation.getEntityEvaluationCriterions()) {
                    final CriterionGroupLayout criterionGroupLayout = this.criterionFields.get(this.targetObject.getQuestionaire().getId()).get(eachEntityEvaluationCriterion.getCriterion().getId());
                    criterionGroupLayout.setQuestionaire(this.targetObject.getQuestionaire());
                    if (criterionGroupLayout.getNotAplicableField().getValue()) {
                        eachEntityEvaluationCriterion.setLevel(-1);
                        eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers().clear();
                        continue;
                    }
                    Map<CriterionAnswer, EntityEvaluationCriterionAnswer> currentAnswers = new HashMap<>();
                    if (eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers() != null) {
                        currentAnswers = eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers()
                                .stream().collect(Collectors.toMap(o -> o.getCriterionAnswer(), o -> o));
                    }
                    for (final Map.Entry<CriterionAnswer, Label> eachEntry : criterionGroupLayout.getAnswerValueLabels().entrySet()) {
                        final CriterionAnswer eachCriterionAnswer = eachEntry.getKey();
                        final Integer eachCriterionAnswerValue = (Integer) eachEntry.getValue().getData();
                        EntityEvaluationCriterionAnswer currentAnswer = currentAnswers.get(eachCriterionAnswer);
                        if (currentAnswer != null) {
                            if (eachCriterionAnswerValue > 0) {
                                currentAnswer.setValue(eachCriterionAnswerValue);
                            } else {
                                eachEntityEvaluationCriterion.getEntityEvaluationCriterionAnswers().remove(currentAnswer);
                            }
                        } else {
                            if (eachCriterionAnswerValue > 0) {
                                eachEntityEvaluationCriterion.setLevel(null);
                                currentAnswer = new EntityEvaluationCriterionAnswer(eachEntityEvaluationCriterion, eachCriterionAnswer, eachCriterionAnswerValue);
                                eachEntityEvaluationCriterion.addEntityEvaluationCriterionAnswer(currentAnswer);
                            }
                        }
                    }
                    eachEntityEvaluationCriterion.setNotes(Optional.of(criterionGroupLayout.getNotesField().getValue()).filter(v -> !v.trim().isEmpty()).orElse(null));
                }
            }

            final Set<SocialEconomyNetworkTag> socialEconomyNetworkTags = new HashSet<>();
            for (final SocialEconomyNetworkTagTokenizable eachTagToken : this.entityTokens.getSocialEconomyNetworkTokens()) {
                SocialEconomyNetworkTag networkTag = eachTagToken.getSocialEconomyNetworkTag();
                if (networkTag.getId() == null) {
                    networkTag = this.socialEconomyNetworkTagRepository.save(networkTag);
                }
                socialEconomyNetworkTags.add(networkTag);
            }
            this.targetObject.setOtherSocialEconomyNetworks(socialEconomyNetworkTags);

            final ObjectMapper objectMapper = new ObjectMapper();
            try {
                this.targetObject.setCustomFields(objectMapper.writer().writeValueAsString(this.customFields.getValuesMap()));
            } catch (final JsonProcessingException e) {
                // TODO
            }
        }

        if (this.uploadedPicturePath != null) {
            byte[] pictureContents = null;
            String mimeType = "";
            try {
                final FileInputStream pictureInputStream = new FileInputStream(this.uploadedPicturePath.toFile());
                pictureContents = ByteStreams.toByteArray(pictureInputStream);
                mimeType = (new Tika()).detect(pictureInputStream);
                pictureInputStream.close();
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (pictureContents != null) {
                final BinaryResource picture = new BinaryResource(pictureContents);
                picture.setMimeType(mimeType);
                picture.setFileLength(picture.getContents().length);
                picture.setFileName(this.uploadedPicturePath.getFileName().toString());
                this.targetObject.setPicture(picture);
            }
        }
        this.uploadedPicturePath = null;

        if (this.targetObject.getRegistryUser() == null) {
            this.targetObject.setRegistryUser(this.userService.getCurrentUser());
        }

        final Set<ProductTag> productTags = new HashSet<>();
        for (final ProductTagTokenizable eachTagToken : this.entityTokens.getProductTokens()) {
            ProductTag productTag = eachTagToken.getProductTag();
            if (productTag.getId() == null) {
                productTag = this.productTagService.findOrCreate(productTag.getTag());
            }
            productTags.add(productTag);
        }
        this.targetObject.setProductTag(productTags);

        final Set<KeywordTag> keywordTags = new HashSet<>();
        for (final KeywordTagTokenizable eachTagToken : this.entityTokens.getKeywordTokens()) {
            KeywordTag keywordTag = eachTagToken.getKeywordTag();
            if (keywordTag.getId() == null) {
                keywordTag = this.keywordTagService.findOrCreate(keywordTag.getTag());
            }
            keywordTags.add(keywordTag);
        }
        this.targetObject.setKeywordTags(keywordTags);

        if (this.point != null) {
            this.targetObject.setLatitude(this.point.getY());
            this.targetObject.setLongitude(this.point.getX());
        }

        if (Optional.ofNullable(this.currentNoteField).isPresent() && !StringUtils.isBlank(this.currentNoteField.getValue())) {
            final SocialEconomyEntityNote newNote = new SocialEconomyEntityNote();
            newNote.setUser(this.pamapamSession.getCurrentUser());
            newNote.setEntity(this.targetObject);
            newNote.setTimestamp(Timestamp.from(Instant.now()));
            newNote.setText(this.currentNoteField.getValue());
            this.targetObject.getNotes().add(newNote);
        }

        this.pictureFieldMap.entrySet().stream().forEach(entry -> {
            final BinaryResourceTransferField pictureField = entry.getKey();
            final BinaryResource picture = entry.getValue();
            if (pictureField.getValue() != null) {
                picture.setDescription(pictureField.getValue().getDescription());
            }
        });

        this.targetObject.setDiscounts(this.discountList);
        this.targetObject.setAudiovisualDocuments(this.audiovisualList);
        this.targetObject.setIntercooperacioLinks(this.intercooperacioLinksList);
    }

    private TextField createTextField(
            final String captionId,
            final Binder<SocialEconomyEntity> binder,
            final ValueProvider<SocialEconomyEntity, String> getter,
            final Setter<SocialEconomyEntity, String> setter,
            final boolean required) {
        final TextField field = this.componentFactory.newTextField(captionId);
        field.setSizeFull();
        field.setWidth(50, Unit.PERCENTAGE);
        if (binder != null) {
            binder.bind(field, getter, setter);
        }
        return field;
    }

    private void initializeEntityTokens() {
        this.entityTokens = new EntityTokens();
        this.entityTokensBinder = new Binder<>(EntityTokens.class);
        this.entityTokensBinder.setBean(this.entityTokens);
    }

    private void initializeCriterionLevels() {
        this.criterionLevels = Maps.newHashMap();
    }

    private void initializeXesBalanceOptions() {
        this.xesBalanceOptions = Maps.newLinkedHashMap();
        this.xesBalanceOptions.put(this.messageSource.getMessage("socialEconomyEntity.xesBalance.na"), null);
        this.xesBalanceOptions.put(this.messageSource.getMessage("socialEconomyEntity.xesBalance.no"), false);
        this.xesBalanceOptions.put(this.messageSource.getMessage("socialEconomyEntity.xesBalance.yes"), true);
    }

    private void initializePublicAdministrationOptions() {
        this.publicAdministrationOptions = Maps.newLinkedHashMap();
        this.publicAdministrationOptions.put(this.messageSource.getMessage("fesc.registration.basic.publicAdministration.no"), false);
        this.publicAdministrationOptions.put(this.messageSource.getMessage("fesc.registration.basic.publicAdministration.yes"), true);
    }

    private void initializeCustomFields() {
        this.customFieldsMap = new HashMap<>();
        this.customFields = new CustomFields();
        this.customFieldsBinder = new Binder<>(CustomFields.class);
        this.customFieldsBinder.setBean(this.customFields);
    }

    private void doCleanMap() {
        this.map.removeAllComponents();
        this.map.addBaseLayer(this.mediawikiMapsTiles, "Mediawiki Maps");
        this.point = null;
    }

    private void centerEmptyMap() {
        this.map.setCenter(41.3903, 2.1941);
        this.map.setZoomLevel(12);
    }

    private void updateMap(final Double latitude, final Double longitude) {
        if (this.currentMarker != null) {
            this.map.removeComponent(this.currentMarker);
        }
        if ((latitude != null) && (longitude != null)) {
            this.point = new org.springframework.data.geo.Point(longitude, latitude);
            this.currentMarker = new LMarker(latitude, longitude);
            this.map.addComponent(this.currentMarker);
            this.map.flyTo(new Point(latitude, longitude), 15D);
        } else {
            this.point = null;
            this.centerEmptyMap();
        }
    }

    @Override
    protected List<CrudDetailsPanel> createPanels() {
        final VaadinRequest currentRequest = VaadinService.getCurrentRequest();
        final List<CrudDetailsPanel> panels = super.createPanels();

        this.contactPanel = this.createCommunicationPanel();
        this.socialEconomyPanel = this.createSocialEconomyPanel();
        this.customFieldsPanel = this.createCustomFieldsPanel();
        this.entityEvaluationPanels = this.createEntityEvaluationPanels();
        this.historyPanel = this.createHistoryPanel();
        this.fescPanel = this.createFescPanel();
        this.officesPanel = this.createOfficesPanel();

        panels.add(this.contactPanel);
        panels.add(this.socialEconomyPanel);
        panels.add(this.customFieldsPanel);
        if (currentRequest.isUserInRole(PamapamUser.ROLE_ADMIN) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_COORDINATION) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_SUPERXINXETA) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_XINXETA) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_XINXETA_EINATECA) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_EXTERNAL) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_POL_COOPERATIU)) {
            panels.addAll(this.entityEvaluationPanels.values());
        }

        if (currentRequest.isUserInRole(PamapamUser.ROLE_ADMIN) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_COORDINATION) ||
                currentRequest.isUserInRole(PamapamUser.ROLE_SUPERXINXETA)) {
            panels.add(this.fescPanel);
            panels.add(this.historyPanel);
        }
        panels.add(this.officesPanel);
        return panels;
    }

    @Override
    protected CrudDetailsPanel createMainPanel() {
        final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
        ((JmgFormLayout) panel.getFormLayout()).setRows(40);
        panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.basic"));

        this.addNameField(panel);
        this.addNifField(panel);
        this.addFoundationYear(panel);
        this.addDescriptionField(panel);
        this.addPictureLayout(panel);
        this.addAddressField(panel);
        this.addPostalCodeField(panel);
        this.addMapField(panel);
        this.addTerritoryFields(panel);
        this.addOpeningHoursField(panel);
        this.addLanguageField(panel);
        this.addRegistryDateField(panel);
        this.addEntityStatusField(panel);
        if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
            this.addRegistryUserField(panel);
            this.addInitiativeUserField(panel);
            this.addEntityScopesField(panel);
        }
        this.addMainOfficeField(panel);
        if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN) ||
                VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_INITIATIVE)) {
            this.addAcceptanceField(panel);
        }

        return panel;
    }

    protected CrudDetailsPanel createCommunicationPanel() {
        final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
        panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.contact"));

        this.addContactPersonField(panel);
        this.addEmailField(panel);
        this.addPhoneField(panel);
        this.addWebField(panel);
        this.addOnlineShoppingWebField(panel);
        this.addLaZonaWebField(panel);
        this.addTwitterField(panel);
        this.addFacebookField(panel);
        this.addInstagramField(panel);
        this.addPinterestField(panel);
        this.addQuitterField(panel);

        // > URL Video de presentació
        this.addVideoUrlField(panel);

        // > Què hi pots trobar (Ara està a mercat social!)
        this.addTagsField(panel);
        // > Altres paraules clau
        this.addKeywordField(panel);
        // > Fotos i text de productes
        this.addProductPictures(panel);
        // > Ofertes i descomptes
        this.addOffersAndDiscounts(panel);
        // #10943 Documents audiovisuals: Títol, Breu descripció del vídeo, Durada, URL
        this.addAudiovisualDocuments(panel);

        return panel;
    }

    /**
     * Creates Mercat Social Panel.
     *
     * @return
     */
    protected CrudDetailsPanel createSocialEconomyPanel() {
        final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
        panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.socialEconomy"));

        this.addMainSectorField(panel);
        this.addSectorsField(panel);
        this.addSectorsCcaeField(panel);
        this.addLegalFormField(panel);
        this.addInvoicingField(panel);
        this.addSocialEconomyNetworksField(panel);
        this.addSocialEconomyNetworkTagsField(panel);
        this.addIdeologicalIdentificationField(panel);
        this.addEntityPersonRolesFields(panel);
        this.addintercooperacioField(panel);
        this.addXesBalanceField(panel);
        if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
            this.addXesBalanceUrlField(panel);
        }
        if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN) ||
                VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_EXTERNAL)) {
            this.addExternalFilterTagsField(panel);
        }
        this.addIdentificationFormLink(panel);

        return panel;
    }

    protected CrudDetailsPanel createCustomFieldsPanel() {
        final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
        panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.customFields"));

        final List<ModelAttributeDef> modelAttributeDefs = this.modelDefService.getModelAttributeDefs(SocialEconomyEntity.class.getSimpleName());
        for (final ModelAttributeDef eachAttributeDef : modelAttributeDefs) {
            AbstractComponent attributeField = null;
            switch (eachAttributeDef.getType()) {
                case STRING:
                    attributeField = this.componentFactory.newTextField(null, eachAttributeDef.getLabel());
                    this.customFields.setStringValue(eachAttributeDef, null);
                    this.customFieldsBinder.forField((TextField) attributeField)
                            .bind(o -> o.getStringValue(eachAttributeDef), (o, v) -> o.setStringValue(eachAttributeDef, v));
                    break;
                case INTEGER:
                    attributeField = this.componentFactory.newTextField(null, eachAttributeDef.getLabel());
                    this.customFields.setIntegerValue(eachAttributeDef, null);
                    this.customFieldsBinder.forField((TextField) attributeField)
                            .withNullRepresentation("")
                            .withConverter(new StringToIntegerConverter(this.messageSource.getMessage("converters.mustBeInteger")))
                            .bind(o -> o.getIntegerValue(eachAttributeDef), (o, v) -> o.setIntegerValue(eachAttributeDef, v));
                    break;
                case LONG:
                    attributeField = this.componentFactory.newTextField(null, eachAttributeDef.getLabel());
                    this.customFields.setLongValue(eachAttributeDef, null);
                    this.customFieldsBinder.forField((TextField) attributeField)
                            .withNullRepresentation("")
                            .withConverter(new StringToLongConverter(this.messageSource.getMessage("converters.mustBeInteger")))
                            .bind(o -> o.getLongValue(eachAttributeDef), (o, v) -> o.setLongValue(eachAttributeDef, v));
                    break;
                case DECIMAL:
                    attributeField = this.componentFactory.newTextField(null, eachAttributeDef.getLabel());
                    this.customFields.setBigDecimalValue(eachAttributeDef, null);
                    this.customFieldsBinder.forField((TextField) attributeField)
                            .withNullRepresentation("")
                            .withConverter(new StringToBigDecimalConverter(this.messageSource.getMessage("converters.mustBeNumber")))
                            .bind(o -> o.getBigDecimalValue(eachAttributeDef), (o, v) -> o.setBigDecimalValue(eachAttributeDef, v));
                    break;
                case BOOLEAN:
                    attributeField = new CheckBox(eachAttributeDef.getLabel());
                    this.customFields.setBooleanValue(eachAttributeDef, null);
                    this.customFieldsBinder.forField((CheckBox) attributeField)
                            .bind(o -> o.getBooleanValue(eachAttributeDef), (o, v) -> o.setBooleanValue(eachAttributeDef, v));
                    break;
                case DATETIME:
                    attributeField = this.componentFactory.newDateField(null, eachAttributeDef.getLabel());
                    this.customFields.setDateValue(eachAttributeDef, null);
                    // ...	FIXME: Controlar que el formato por locale en cada navegador sale bien y no setearlo hardcoded.
                    ((DateField) attributeField).setDateFormat("dd/MM/yyyy");
                    this.customFieldsBinder.forField((DateField) attributeField)
                            .withConverter(new LocalDateToDateConverter())
                            .bind(o -> o.getDateValue(eachAttributeDef), (o, v) -> o.setDateValue(eachAttributeDef, v));
                    break;
                default:
                    break;
            }
            this.customFieldsMap.put(eachAttributeDef, attributeField);
            panel.addComponent(attributeField);
        }

        return panel;
    }

    protected Map<Long, CrudDetailsPanel> createEntityEvaluationPanels() {
        final List<Questionaire> questionaires = this.questionaireRepository.findAll();
        final Map<Long, CrudDetailsPanel> panels = new HashMap<>();

        for (final Questionaire questionaire : questionaires) {
            final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
            this.addUpdateEntityEvaluationTypeButton(panel);
            panel.setVisible(true);
            panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.evaluation") + " " + questionaire.getName());
            this.addEntityEvaluationFields(panel, questionaire);

            panels.put(questionaire.getId(), panel);
        }

        return panels;
    }

    private void addUpdateEntityEvaluationTypeButton(final CrudDetailsPanel panel) {
        final VaadinRequest currentRequest = VaadinService.getCurrentRequest();
        final Button evaluationButton = this.componentFactory.newButton("entityEvaluation.update");
        evaluationButton.addClickListener(event -> {
            if (!currentRequest.isUserInRole(PamapamUser.ROLE_ADMIN)) {
                JmgNotification.show(this.messageSource.getMessage("entityEvaluation.update.notAllowedMessage")).setDelayMsec(5000);
            } else {
                JmgNotification.show(this.messageSource.getMessage("entityEvaluation.update.adminReminder")).setDelayMsec(5000);
                this.createUpdateEntityEvaluationButtons();

                MessageBox.createQuestion()
                        .withCaption(this.messageSource.getMessage("socialEconomyEntity.select.evaluation"))
                        .withMessage(this.messageSource.getMessage("socialEconomyEntity.select.evaluation.update.description"))
                        .asModal(true)
                        .withButton(this.updateSocialEconomyEvaluationButton)
                        .withButton(this.updateAgroecologicalEvaluationButton)
                        .asModal(true)
                        .open();
            }
        });

        panel.addComponent(evaluationButton);
    }

    protected CrudDetailsPanel createHistoryPanel() {
        final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel(Format.Vertical);
        panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.history"));

        this.addEntityStatusChangesSection(panel);
        this.addSiblingEntitiesSection(panel);

        return panel;
    }

    protected CrudDetailsPanel createFescPanel() {
        final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
        panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.fesc"));
        ((JmgFormLayout) panel.getFormLayout()).setRows(30);

        this.addFescPreviousEntityStatusField(panel);
        this.addFescEntityStatusField(panel);
        this.addFescEmailField(panel);
        this.addFescKeywordTagsField(panel);
        this.addPublicAdministrationField(panel);
        this.addFescinvoiceNameField(panel);
        this.addFescinvoiceEmailField(panel);
        this.addFescinvoiceAddressField(panel);
        this.addFescinvoicePostalCodeField(panel);
        this.addFescinvoiceModalityFields(panel);
        this.addFescPreviousFeeField(panel);
        this.addFescFeeField(panel);
        this.addFescinvoiceAccountField(panel);
        this.addFescParticipationFields(panel);
        this.addFescRegistrationPolicyFields(panel);

        return panel;
    }

    protected CrudDetailsPanel createOfficesPanel() {
        final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel(Format.Vertical);
        panel.setName(this.messageSource.getMessage("socialEconomyEntity.tab.offices"));

        this.addOfficeEntitiesSection(panel);

        return panel;
    }

    // ...	Fesc tab fields.

    private void addFescPreviousEntityStatusField(final CrudDetailsPanel tab) {
        this.fescPreviousEntityStatusField = this.componentFactory.newComboBox("fesc.registration.invoice.previousEntityStatus");
        this.fescPreviousEntityStatusField.setWidth(50, Unit.PERCENTAGE);
        this.fescPreviousEntityStatusField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
        this.fescPreviousEntityStatusField.setItems(this.entityStatusRepository.findByEntityStatusTypeIn(EntityStatusType.getFescTypes()));
        this.fescPreviousEntityStatusField.setEnabled(false);
        this.binder.bind(this.fescPreviousEntityStatusField, SocialEconomyEntity::getFescPreviousEntityStatus, SocialEconomyEntity::setFescPreviousEntityStatus);
        tab.addComponent(this.fescPreviousEntityStatusField);
    }

    private void addFescEntityStatusField(final CrudDetailsPanel tab) {
        this.fescEntityStatusField = this.componentFactory.newComboBox("fesc.registration.invoice.entityStatus");
        this.fescEntityStatusField.setWidth(50, Unit.PERCENTAGE);
        this.fescEntityStatusField.setItemCaptionGenerator(item -> item.getName().getDefaultText());

        final Set<EntityStatus> fescTypesWithNumbers = this.entityStatusService.fescTypesWithNumbers();
        this.fescEntityStatusField.setItems(fescTypesWithNumbers);
        this.fescEntityStatusField.setEmptySelectionAllowed(false);
        this.binder.bind(this.fescEntityStatusField, SocialEconomyEntity::getFescEntityStatus, SocialEconomyEntity::setFescEntityStatus);
        tab.addComponent(this.fescEntityStatusField);
    }

    private void addFescEmailField(final CrudDetailsPanel tab) {
        this.fescEmailField = this.componentFactory.newTextField("fesc.registration.contact.email");
        this.fescEmailField.setWidth(50, Unit.PERCENTAGE);
        this.binder.bind(this.fescEmailField, SocialEconomyEntity::getFescEmail, SocialEconomyEntity::setFescEmail);
        tab.addComponent(this.fescEmailField);
    }

    private void addFescKeywordTagsField(final CrudDetailsPanel tab) {
        this.fescKeywordTagsField = this.componentFactory.newTextField("fesc.registration.communication.tags");
        this.fescKeywordTagsField.setWidth(50, Unit.PERCENTAGE);
//		tab.addComponent(this.fescKeywordTagsField);
    }

    private void addFescinvoiceNameField(final CrudDetailsPanel tab) {
        this.fescinvoiceNameField = this.componentFactory.newTextField("fesc.registration.invoice.name");
        this.fescinvoiceNameField.setWidth(50, Unit.PERCENTAGE);
        this.binder.bind(this.fescinvoiceNameField, SocialEconomyEntity::getFescInvoiceName, SocialEconomyEntity::setFescInvoiceName);
        tab.addComponent(this.fescinvoiceNameField);
    }

    private void addFescinvoiceEmailField(final CrudDetailsPanel tab) {
        this.fescinvoiceEmailField = this.componentFactory.newTextField("fesc.registration.invoice.email");
        this.fescinvoiceEmailField.setWidth(50, Unit.PERCENTAGE);
        this.binder.bind(this.fescinvoiceEmailField, SocialEconomyEntity::getFescEmail, SocialEconomyEntity::setFescEmail);
        tab.addComponent(this.fescinvoiceEmailField);
    }

    private void addFescinvoiceAddressField(final CrudDetailsPanel tab) {
        this.fescinvoiceAddressField = this.componentFactory.newTextField("fesc.registration.invoice.address");
        this.fescinvoiceAddressField.setWidth(50, Unit.PERCENTAGE);
        this.binder.bind(this.fescinvoiceAddressField, SocialEconomyEntity::getFescInvoiceAddress, SocialEconomyEntity::setFescInvoiceAddress);
        tab.addComponent(this.fescinvoiceAddressField);
    }

    private void addFescinvoicePostalCodeField(final CrudDetailsPanel tab) {
        this.fescinvoicePostalCodeField = this.componentFactory.newTextField("fesc.registration.invoice.postalCode");
        this.fescinvoicePostalCodeField.setWidth(50, Unit.PERCENTAGE);
        this.binder.bind(this.fescinvoicePostalCodeField, SocialEconomyEntity::getFescInvoicePostalCode, SocialEconomyEntity::setFescInvoicePostalCode);
        tab.addComponent(this.fescinvoicePostalCodeField);
    }

    private void addFescinvoiceModalityFields(final CrudDetailsPanel tab) {
        this.fescInvoiceModalityField = this.componentFactory.newComboBox("fesc.registration.invoice.modality");
        this.fescInvoiceModalityField.setWidth(50, Unit.PERCENTAGE);
        this.fescInvoiceModalityField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
        this.fescInvoiceModalityField.setItems(this.modalityService.findAll());
        this.fescInvoiceModalityField.setEmptySelectionAllowed(false);
        this.binder.bind(this.fescInvoiceModalityField, SocialEconomyEntity::getModality, SocialEconomyEntity::setModality);
        this.fescInvoiceModalityField.addValueChangeListener(this::modalitySelected);

        this.fescInvoiceFellowNameField = this.componentFactory.newTextField("fesc.registration.invoice.fellowName");
        this.fescInvoiceFellowNameField.setWidth(50, Unit.PERCENTAGE);
        this.fescInvoiceFellowNameField.setEnabled(false);
        this.binder.bind(this.fescInvoiceFellowNameField, SocialEconomyEntity::getFellowName, SocialEconomyEntity::setFellowName);

        this.timeSlotField = this.componentFactory.newComboBox("fesc.registration.invoice.timeSlot");
        this.timeSlotField.setWidth(50, Unit.PERCENTAGE);
        this.timeSlotField.setItems(Arrays.asList(FescTimeSlot.values()));
        this.timeSlotField.setItemCaptionGenerator(item -> this.messageSource.getMessage("fesc.registration.invoice.timeSlot." + item.toString()));
        this.timeSlotField.setEnabled(false);
        this.binder.forField(this.timeSlotField)
                .bind(SocialEconomyEntity::getFescTimeSlot, SocialEconomyEntity::setFescTimeSLot);

        List<Sector> allSectors = this.sectorRepository.findAll();
        Collections.sort(allSectors, Sector.NAME_HIERARCHICAL_ORDER);
        allSectors = allSectors.stream()
                .filter(sector -> sector.getParent() != null)
                .collect(Collectors.toList());

        this.mainFescSectorField = this.componentFactory.newComboBox("fesc.registration.basic.backoffice.fescSector");
        this.mainFescSectorField.setWidth(50, Unit.PERCENTAGE);
        this.mainFescSectorField.setItemCaptionGenerator(item -> item.getFullName());
        this.mainFescSectorField.setItems(allSectors);
        this.binder.forField(this.mainFescSectorField)
                .bind(SocialEconomyEntity::getFescMainSector, SocialEconomyEntity::setFescMainSector);

        tab.addComponent(this.fescInvoiceModalityField);
        tab.addComponent(this.fescInvoiceFellowNameField);
        tab.addComponent(this.timeSlotField);
        tab.addComponent(this.mainFescSectorField);
    }

    private void modalitySelected(final ValueChangeEvent<FescRegistrationModality> event) {
        final FescRegistrationModality selectedOption = event.getValue();
        if (selectedOption != null) {
            this.fescInvoiceFellowNameField.setEnabled(selectedOption.isShareTableAllowed());
            this.timeSlotField.setEnabled(selectedOption.isTimeSlot());
        } else {
            this.fescInvoiceFellowNameField.setEnabled(false);
            this.timeSlotField.setEnabled(false);
        }
    }

    private void addFescPreviousFeeField(final CrudDetailsPanel tab) {
        this.fescPreviousFeeField = this.componentFactory.newTextField("fesc.registration.invoice.previousFee");
        this.fescPreviousFeeField.setWidth(50, Unit.PERCENTAGE);
        this.fescPreviousFeeField.setId("fesc_previous_fee");
        this.fescPreviousFeeField.setEnabled(false);
        this.binder.forField(this.fescPreviousFeeField)
                .withConverter(new StringToIntegerConverter(""))
                .withNullRepresentation(0)
                .bind(SocialEconomyEntity::getFescPreviousFee, SocialEconomyEntity::setFescPreviousFee);
        tab.addComponent(this.fescPreviousFeeField);
    }

    private void addFescFeeField(final CrudDetailsPanel tab) {
        this.fescFeeField = this.componentFactory.newTextField("fesc.registration.invoice.currentFee");
        this.fescFeeField.setWidth(50, Unit.PERCENTAGE);
        this.fescFeeField.setId("fesc_fee");
        this.binder.forField(this.fescFeeField)
                .withConverter(new StringToIntegerConverter(""))
                .withNullRepresentation(0)
                .bind(SocialEconomyEntity::getFescFee, SocialEconomyEntity::setFescFee);
        tab.addComponent(this.fescFeeField);
    }

    private void addFescinvoiceAccountField(final CrudDetailsPanel tab) {
        this.fescinvoiceAccountField = this.componentFactory.newTextField("fesc.registration.invoice.account");
        this.fescinvoiceAccountField.setWidth(50, Unit.PERCENTAGE);
        this.binder.bind(this.fescinvoiceAccountField, SocialEconomyEntity::getFescInvoiceAccount, SocialEconomyEntity::setFescInvoiceAccount);
        tab.addComponent(this.fescinvoiceAccountField);
    }

    private void addFescParticipationFields(final CrudDetailsPanel tab) {
        final HorizontalLayout fescParticipationPolicyLayout = new HorizontalLayout();
        fescParticipationPolicyLayout.setWidth(60, Unit.PERCENTAGE);
        fescParticipationPolicyLayout.setCaption(this.messageSource.getMessage("fesc.registration.backoffice.participationPrivacyPolicy"));

        this.addFescParticipationPolicyField(fescParticipationPolicyLayout);
        this.addFescParticipationPolicyDateField(fescParticipationPolicyLayout);

        tab.addComponent(fescParticipationPolicyLayout);
    }

    private void addFescRegistrationPolicyFields(final CrudDetailsPanel tab) {
        final HorizontalLayout fescRegistrationPolicyLayout = new HorizontalLayout();
        fescRegistrationPolicyLayout.setWidth(60, Unit.PERCENTAGE);
        fescRegistrationPolicyLayout.setCaption(this.messageSource.getMessage("fesc.registration.backoffice.privacyPolicy"));

        this.addFescRegistrationPolicyField(fescRegistrationPolicyLayout);
        this.addFescRegistrationPolicyDateField(fescRegistrationPolicyLayout);

        tab.addComponent(fescRegistrationPolicyLayout);
    }

    private void addFescRegistrationPolicyField(final HorizontalLayout layout) {
        this.fescRegistrationPolicyField = this.componentFactory.newCheckBox("fesc.registration.backoffice.privacyPolicy");
        this.fescRegistrationPolicyField.setWidth(100, Unit.PERCENTAGE);
        this.binder.bind(this.fescRegistrationPolicyField, SocialEconomyEntity::getFescRegistrationPrivacyPolicy, SocialEconomyEntity::setFescRegistrationPrivacyPolicy);
        layout.addComponent(this.fescRegistrationPolicyField);
    }

    private void addFescRegistrationPolicyDateField(final HorizontalLayout layout) {
        this.fescRegistrationPolicyDateField = this.componentFactory.newDateField();
        this.fescRegistrationPolicyDateField.setDateFormat("dd/MM/yyyy");
        this.binder.forField(this.fescRegistrationPolicyDateField)
                .bind(
                        o -> Optional.ofNullable(o.getFescRegistrationPrivacyPolicyDate())
                                .map(p -> p.toLocalDateTime().toLocalDate()).orElse(null),
                        (o, v) -> o.setFescRegistrationPrivacyPolicyDate(Optional.ofNullable(v)
                                .map(p -> p.atStartOfDay())
                                .map(p -> Timestamp.valueOf(p))
                                .orElse(null)));

        layout.addComponent(this.fescRegistrationPolicyDateField);
    }

    private void addFescParticipationPolicyField(final HorizontalLayout layout) {
        this.fescParticipationPolicyField = this.componentFactory.newCheckBox("fesc.registration.backoffice.participationPrivacyPolicy");
        this.fescParticipationPolicyField.setWidth(100, Unit.PERCENTAGE);
        this.binder.bind(this.fescParticipationPolicyField, SocialEconomyEntity::getFescParticipationPrivacyPolicy, SocialEconomyEntity::setFescParticipationPrivacyPolicy);
        layout.addComponent(this.fescParticipationPolicyField);
    }

    private void addFescParticipationPolicyDateField(final HorizontalLayout layout) {
        this.fescParticipationPolicyDateField = this.componentFactory.newDateField();
        this.fescParticipationPolicyDateField.setDateFormat("dd/MM/yyyy");
        this.binder.forField(this.fescParticipationPolicyDateField)
                .bind(
                        o -> Optional.ofNullable(o.getFescParticipationPrivacyPolicyDate())
                                .map(p -> p.toLocalDateTime().toLocalDate()).orElse(null),
                        (o, v) -> o.setFescParticipationPrivacyPolicyDate(Optional.ofNullable(v)
                                .map(p -> p.atStartOfDay())
                                .map(p -> Timestamp.valueOf(p))
                                .orElse(null)));

        layout.addComponent(this.fescParticipationPolicyDateField);
    }

    // ... Main tab fields.

    private void addNameField(final CrudDetailsPanel tab) {
        this.nameField = this.createTextField("socialEconomyEntity.name", this.binder, SocialEconomyEntity::getName, SocialEconomyEntity::setName, true);
        tab.addComponent(this.nameField);
    }

    private void addNifField(final CrudDetailsPanel tab) {
        this.nifField = this.createTextField("socialEconomyEntity.nif", this.binder, SocialEconomyEntity::getNif, SocialEconomyEntity::setNif, false);
        tab.addComponent(this.nifField);
    }

    private void addDescriptionField(final CrudDetailsPanel tab) {
        this.descriptionField = (JmgLocalizedTextArea) this.componentBuilderFactory.createLocalizedTextAreaBuilder().setCaption("socialEconomyEntity.description").setWidth(100, Unit.PERCENTAGE).build();
        this.descriptionField.setMaxLength(5000);
        this.binder.bind(this.descriptionField, SocialEconomyEntity::getDescription, SocialEconomyEntity::setDescription);
        tab.addComponent(this.descriptionField);
    }

    private void addPictureLayout(final CrudDetailsPanel tab) {
        final HorizontalLayout pictureLayout = this.componentFactory.newHorizontalLayout("socialEconomyEntity.picture");

        this.pictureField = this.componentFactory.newImage();
        this.pictureField.setHeight(200, Unit.PIXELS);
        pictureLayout.addComponent(this.pictureField);

        final VerticalLayout pictureUploadLayout = this.componentFactory.newVerticalLayout();

        this.pictureUploadField = new UploadComponent(this::pictureUploadReceived);
        this.pictureUploadField.setStartedCallback(this::uploadStarted);
        this.pictureUploadField.setProgressCallback(this::uploadProgress);
        this.pictureUploadField.setFailedCallback(this::uploadFailed);
        this.pictureUploadField.getDropTextLabel().setValue(this.messageSource.getMessage("user.picture.dropFile"));
        this.pictureUploadField.getChoose().setButtonCaption(this.messageSource.getMessage("user.picture.chooseFile"));
        pictureUploadLayout.addComponent(this.pictureUploadField);
        this.pictureProgressBar = new ProgressBar();
        this.pictureProgressBar.setVisible(false);
        pictureUploadLayout.addComponent(this.pictureProgressBar);
        pictureLayout.addComponent(pictureUploadLayout);

        tab.addComponent(pictureLayout);
    }

    private void addAddressField(final CrudDetailsPanel tab) {
        this.addressField = this.createTextField("socialEconomyEntity.address", this.binder, SocialEconomyEntity::getAddress, SocialEconomyEntity::setAddress, false);
        this.addressField.setValueChangeMode(ValueChangeMode.BLUR);
        tab.addComponent(this.addressField);
    }

    private void addPostalCodeField(final CrudDetailsPanel tab) {
        this.postalCodeField = this.createTextField("socialEconomyEntity.postalCode", this.binder, SocialEconomyEntity::getPostalCode, SocialEconomyEntity::setPostalCode, false);
        tab.addComponent(this.postalCodeField);
    }

    private void addMapField(final CrudDetailsPanel tab) {
        final Label helpLabel = this.componentFactory.newLabel();
        helpLabel.setValue(this.messageSource.getMessage("socialEconomyEntity.geoLocation.description"));
        tab.addComponent(helpLabel);

        this.map = new LMap();
        this.map.setCaption(this.messageSource.getMessage("socialEconomyEntity.geoLocation"));
        this.map.setHeight("440px");
        this.mediawikiMapsTiles.setAttributionString("© Mediawiki Maps Contributors");
        this.map.addBaseLayer(this.mediawikiMapsTiles, "Mediawiki Maps");
        this.map.setScrollWheelZoomEnabled(false);
        this.map.addClickListener(event -> {
            // save the point to be used by context menu listener
            if (event.getMouseEvent().isCtrlKey()) {
                final Point newPoint = event.getPoint();
                this.doCleanMap();
                this.updateMap(newPoint.getLat(), newPoint.getLon());
            }
        });
        this.centerEmptyMap();
        tab.addComponent(this.map);
    }

    private void addTerritoryFields(final CrudDetailsPanel tab) {
        this.createNeighborhoodField();
        this.createDistrictField();
        this.createTownField();
        this.createRegionField();
        this.createProvinceField();

        tab.addComponent(this.provinceField);
        tab.addComponent(this.regionField);
        tab.addComponent(this.townField);
        tab.addComponent(this.districtField);
        tab.addComponent(this.neighborhoodField);
    }

    private void createNeighborhoodField() {
        this.neighborhoodField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
                Neighborhood.class,
                null,
                "socialEconomyEntity.neighborhood",
                (ValueProvider<Neighborhood, Long>) item -> item.getId(),
                null);
        this.binder.bind(this.neighborhoodField, SocialEconomyEntity::getNeighborhood, SocialEconomyEntity::setNeighborhood);
    }

    private void createDistrictField() {
        this.districtField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
                District.class,
                this.neighborhoodField,
                "socialEconomyEntity.district",
                (ValueProvider<District, Long>) item -> item.getId(),
                (ValueProvider<Neighborhood, District>) neighborhood -> neighborhood.getDistrict());
        this.binder.bind(this.districtField, SocialEconomyEntity::getDistrict, SocialEconomyEntity::setDistrict);
    }

    private void createTownField() {
        this.townField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
                Town.class,
                this.districtField,
                "socialEconomyEntity.town",
                (ValueProvider<Town, Long>) item -> item.getId(),
                (ValueProvider<District, Town>) district -> district.getTown());
        this.binder.bind(this.townField, SocialEconomyEntity::getTown, SocialEconomyEntity::setTown);
    }

    private void createRegionField() {
        this.regionField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
                Region.class,
                this.townField,
                "socialEconomyEntity.region",
                (ValueProvider<Region, Long>) item -> item.getId(),
                (ValueProvider<Town, Region>) town -> town.getRegion());
        this.binder.bind(this.regionField, SocialEconomyEntity::getRegion, SocialEconomyEntity::setRegion);
    }

    private void createProvinceField() {
        this.provinceField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
                Province.class,
                this.regionField,
                "socialEconomyEntity.province",
                null,
                (ValueProvider<Region, Province>) region -> region.getProvince());
        this.binder.bind(this.provinceField, SocialEconomyEntity::getProvince, SocialEconomyEntity::setProvince);
    }

    private void addOpeningHoursField(final CrudDetailsPanel tab) {
        this.openingHours = this.createTextField("socialEconomyEntity.openingHours", this.binder, SocialEconomyEntity::getOpeningHours, SocialEconomyEntity::setOpeningHours, false);
        tab.addComponent(this.openingHours);
    }

    private void addLanguageField(final CrudDetailsPanel tab) {
        this.languageField = this.componentFactory.newComboBox("socialEconomyEntity.language");
        this.languageField.setWidth(50, Unit.PERCENTAGE);
        this.languageField.setItemCaptionGenerator(item -> item.getName());
        this.languageField.setItems(this.languageRepository.findAll());
        this.binder.bind(this.languageField, SocialEconomyEntity::getLanguage, SocialEconomyEntity::setLanguage);
        tab.addComponent(this.languageField);
    }

    private void addRegistryDateField(final CrudDetailsPanel tab) {
        this.registryDateField = this.componentFactory.newDateField("socialEconomyEntity.registryDate");
        // ...	FIXME: Controlar que el formato por locale en cada navegador sale bien y no setearlo hardcoded.
        this.registryDateField.setDateFormat("dd/MM/yyyy");
        this.binder.forField(this.registryDateField)
                .withConverter(new LocalDateToDateConverter())
                .bind(SocialEconomyEntity::getRegistryDate, SocialEconomyEntity::setRegistryDate);
        tab.addComponent(this.registryDateField);
    }

    private void addEntityStatusField(final CrudDetailsPanel tab) {
        this.entityStatusField = this.componentFactory.newComboBox("socialEconomyEntity.entityStatus");
        this.entityStatusField.setWidth(50, Unit.PERCENTAGE);
        this.entityStatusField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
        this.entityStatusField.setItems(this.entityStatusRepository.findByEntityStatusTypeIn(EntityStatusType.getPamapamTypes()));
        this.entityStatusField.setEmptySelectionAllowed(false);

        this.entityStatusField.addValueChangeListener((event) ->
        {
            final EntityStatus oldEntityStatus = this.targetObject != null ? this.targetObject.getEntityStatus() : null;

            if (this.entityStatusIsNotAccordingUserConditions(event.getValue())) {
                JmgNotification.show(this.messageSource.getMessage("error.published.terms&conditions"), Type.ERROR_MESSAGE).setDelayMsec(5000);
                this.entityStatusField.setValue(oldEntityStatus);
                return;
            }

            this.updateOkButtonColor(event.getValue());
        });

        this.binder.bind(this.entityStatusField, SocialEconomyEntity::getEntityStatus, SocialEconomyEntity::setEntityStatus);
        tab.addComponent(this.entityStatusField);
    }

    private boolean entityStatusIsNotAccordingUserConditions(final EntityStatus selectedEntityStatus) {
        final boolean entityConditionsAccepted = Optional.ofNullable(this.targetObject)
                .map(SocialEconomyEntity::getUserConditionsAccepted)
                .orElse(false);

        return selectedEntityStatus.isPublished() && !entityConditionsAccepted;
    }

    private void addRegistryUserField(final CrudDetailsPanel tab) {
        this.registryUserField = this.componentFactory.newComboBox("socialEconomyEntity.user.xinxeta");
        this.registryUserField.setWidth(50, Unit.PERCENTAGE);
        this.registryUserField.setItemCaptionGenerator(item -> item.getUsername());
        this.registryUserField.setItems(this.pamapamUserRepository.findAll());
        this.binder.bind(this.registryUserField, SocialEconomyEntity::getRegistryUser, SocialEconomyEntity::setRegistryUser);
        tab.addComponent(this.registryUserField);
    }

    private void addInitiativeUserField(final CrudDetailsPanel tab) {
        this.initiativeUserField = this.componentFactory.newComboBox("socialEconomyEntity.user.initiative");
        this.initiativeUserField.setWidth(50, Unit.PERCENTAGE);
        this.initiativeUserField.setItemCaptionGenerator(item -> item.getUsername());
        this.initiativeUserField.setItems(this.pamapamUserRepository.findByRolename(PamapamUser.ROLE_INITIATIVE));
        this.binder.bind(this.initiativeUserField, SocialEconomyEntity::getInitiativeUser, SocialEconomyEntity::setInitiativeUser);
        tab.addComponent(this.initiativeUserField);
    }

    private void addMainOfficeField(final CrudDetailsPanel tab) {
        this.mainOfficeField = this.componentFactory.newComboBox("socialEconomyEntity.mainOffice");
        this.mainOfficeField.setWidth(50, Unit.PERCENTAGE);
        this.mainOfficeField.setItemCaptionGenerator(item -> item.getName());
        this.mainOfficeField.setItems(this.socialEconomyEntityService.findAllMainOffices());
        this.binder.bind(this.mainOfficeField, SocialEconomyEntity::getMainOffice, SocialEconomyEntity::setMainOffice);
        tab.addComponent(this.mainOfficeField);
    }

    private void addAcceptanceField(final CrudDetailsPanel tab) {
        final HorizontalLayout userConditionsLayout = new HorizontalLayout();
        userConditionsLayout.setWidth(60, Unit.PERCENTAGE);
        userConditionsLayout.setCaption(this.messageSource.getMessage("socialEconomyEntity.userConditionsAccepted"));

        this.userConditionsAcceptedField = this.componentFactory.newCheckBox();
        this.userConditionsAcceptedField.addValueChangeListener(event -> {
            if (event.getValue().equals(Optional.ofNullable(this.targetObject.getUserConditionsAccepted()).orElse(false))) {
                return;
            }
            if (event.getValue()) {
                this.userConditionsDateField.setValue(LocalDate.now());
            } else {
                this.userConditionsDateField.setValue(null);
            }
        });
        userConditionsLayout.addComponent(this.userConditionsAcceptedField);
        this.binder.bind(this.userConditionsAcceptedField, SocialEconomyEntity::getUserConditionsAccepted, SocialEconomyEntity::setUserConditionsAccepted);

        this.userConditionsDateField = this.componentFactory.newDateField();
        this.userConditionsDateField.setDateFormat("dd/MM/yyyy");
        this.binder.forField(this.userConditionsDateField)
                .bind(
                        o -> Optional.ofNullable(o.getUserConditionsDate()).map(p -> p.toLocalDateTime().toLocalDate()).orElse(null),
                        (o, v) -> o.setUserConditionsDate(Optional.ofNullable(v)
                                .map(p -> p.atStartOfDay())
                                .map(p -> Timestamp.valueOf(p))
                                .orElse(null)));
        userConditionsLayout.addComponent(this.userConditionsDateField);

        tab.addComponent(userConditionsLayout);

        final HorizontalLayout newslettersLayout = new HorizontalLayout();
        newslettersLayout.setWidth(60, Unit.PERCENTAGE);
        newslettersLayout.setCaption(this.messageSource.getMessage("socialEconomyEntity.newslettersAccepted"));

        this.newslettersAcceptedField = this.componentFactory.newCheckBox();
        this.newslettersAcceptedField.addValueChangeListener(event -> {
            if (event.getValue().equals(Optional.ofNullable(this.targetObject.getNewslettersAccepted()).orElse(false))) {
                return;
            }
            if (event.getValue()) {
                this.newslettersDateField.setValue(LocalDate.now());
            } else {
                this.newslettersDateField.setValue(null);
            }
        });
        newslettersLayout.addComponent(this.newslettersAcceptedField);
        this.binder.bind(this.newslettersAcceptedField, SocialEconomyEntity::getNewslettersAccepted, SocialEconomyEntity::setNewslettersAccepted);

        this.newslettersDateField = this.componentFactory.newDateField();
        this.newslettersDateField.setDateFormat("dd/MM/yyyy");
        this.binder.forField(this.newslettersDateField)
                .bind(
                        o -> Optional.ofNullable(o.getNewslettersDate()).map(p -> p.toLocalDateTime().toLocalDate()).orElse(null),
                        (o, v) -> o.setNewslettersDate(Optional.ofNullable(v)
                                .map(p -> p.atStartOfDay())
                                .map(p -> Timestamp.valueOf(p))
                                .orElse(null)));
        newslettersLayout.addComponent(this.newslettersDateField);

        tab.addComponent(newslettersLayout);
    }

    private void addContactPersonField(final CrudDetailsPanel tab) {
        this.contactPersonField = this.createTextField("socialEconomyEntity.contactPerson", this.binder, SocialEconomyEntity::getContactPerson, SocialEconomyEntity::setContactPerson, false);
        tab.addComponent(this.contactPersonField);
    }

    private void addEmailField(final CrudDetailsPanel tab) {
        this.emailField = this.createTextField("socialEconomyEntity.email", this.binder, SocialEconomyEntity::getEmail, SocialEconomyEntity::setEmail, false);
        tab.addComponent(this.emailField);
    }

    private void addPhoneField(final CrudDetailsPanel tab) {
        this.phoneField = this.createTextField("socialEconomyEntity.phone", this.binder, SocialEconomyEntity::getPhone, SocialEconomyEntity::setPhone, false);
        tab.addComponent(this.phoneField);
    }

    private void addWebField(final CrudDetailsPanel tab) {
        this.webField = this.createTextField("socialEconomyEntity.web", this.binder, SocialEconomyEntity::getWeb, SocialEconomyEntity::setWeb, false);
        tab.addComponent(this.webField);
    }

    private void addOnlineShoppingWebField(final CrudDetailsPanel tab) {
        this.onlineShoppingWebField = this.createTextField("socialEconomyEntity.onlineShoppingWeb", this.binder, SocialEconomyEntity::getOnlineShoppingWeb, SocialEconomyEntity::setOnlineShoppingWeb, false);
        tab.addComponent(this.onlineShoppingWebField);
    }

    private void addLaZonaWebField(final CrudDetailsPanel tab) {
        this.laZonaWebField = this.createTextField("socialEconomyEntity.laZonaWeb", this.binder, SocialEconomyEntity::getLaZonaWeb, SocialEconomyEntity::setLaZonaWeb, false);
        tab.addComponent(this.laZonaWebField);
    }

    private void addTwitterField(final CrudDetailsPanel tab) {
        this.twitterField = this.createTextField("socialEconomyEntity.twitter", this.binder, SocialEconomyEntity::getTwitter, SocialEconomyEntity::setTwitter, false);
        tab.addComponent(this.twitterField);
    }

    private void addFacebookField(final CrudDetailsPanel tab) {
        this.facebookField = this.createTextField("socialEconomyEntity.facebook", this.binder, SocialEconomyEntity::getFacebook, SocialEconomyEntity::setFacebook, false);
        tab.addComponent(this.facebookField);
    }

    private void addInstagramField(final CrudDetailsPanel tab) {
        this.instagramField = this.createTextField("socialEconomyEntity.instagram", this.binder, SocialEconomyEntity::getInstagram, SocialEconomyEntity::setInstagram, false);
        tab.addComponent(this.instagramField);
    }

    private void addPinterestField(final CrudDetailsPanel tab) {
        this.pinterestField = this.createTextField("socialEconomyEntity.pinterest", this.binder, SocialEconomyEntity::getPinterest, SocialEconomyEntity::setPinterest, false);
        tab.addComponent(this.pinterestField);
    }

    private void addQuitterField(final CrudDetailsPanel tab) {
        this.quitterField = this.createTextField("socialEconomyEntity.quitter", this.binder, SocialEconomyEntity::getQuitter, SocialEconomyEntity::setQuitter, false);
        tab.addComponent(this.quitterField);
    }

    private void addVideoUrlField(final CrudDetailsPanel tab) {
        this.videoUrlField = this.createTextField("socialEconomyEntity.urlVideo", this.binder, SocialEconomyEntity::getFescVideo, SocialEconomyEntity::setFescVideo, true);
        tab.addComponent(this.videoUrlField);
    }

    private void addIdentificationFormLink(final CrudDetailsPanel tab) {
        final Label description = this.componentFactory.newLabel();
        description.setContentMode(ContentMode.HTML);
        description.setValue(this.messageSource.getMessage("socialEconomyEntity.identificationForm"));
        description.setWidth(100, Unit.PERCENTAGE);
        final GridLayout formLayout = (GridLayout) tab.getFormLayout();
        //formLayout.addComponent(description, 0, formLayout.getRows() - 1, 1, formLayout.getRows() - 1);
        formLayout.addComponent(description);
    }

    private void addXesBalanceUrlField(final CrudDetailsPanel tab) {
        this.xesBalanceUrlField = this.createTextField("socialEconomyEntity.XESBalanceUrl",
                this.binder, SocialEconomyEntity::getUrlBalancSocial,
                SocialEconomyEntity::setUrlBalancSocial, true);

        this.xesBalanceUrlField.setResponsive(true);
        this.xesBalanceUrlField.setHeightUndefined();
        tab.addComponent(this.xesBalanceUrlField);
    }

    private void addMainSectorField(final CrudDetailsPanel tab) {
        List<Sector> allSectors = this.sectorRepository.findAll();
        Collections.sort(allSectors, Sector.NAME_HIERARCHICAL_ORDER);
        allSectors = allSectors.stream()
                .filter(sector -> sector.getParent() != null)
                .collect(Collectors.toList());

        this.mainSectorField = this.componentFactory.newComboBox("socialEconomyEntity.mainSector");
        this.mainSectorField.setWidth(50, Unit.PERCENTAGE);
        this.mainSectorField.setItemCaptionGenerator(item -> item.getFullName());
        this.mainSectorField.setItems(allSectors);
        this.binder.bind(this.mainSectorField, SocialEconomyEntity::getMainSector, SocialEconomyEntity::setMainSector);
        tab.addComponent(this.mainSectorField);
    }

    private void addSectorsField(final CrudDetailsPanel tab) {
        List<Sector> allSectors = this.sectorRepository.findAll();
        Collections.sort(allSectors, Sector.NAME_HIERARCHICAL_ORDER);
        allSectors = allSectors.stream()
                .filter(sector -> sector.getParent() != null)
                .collect(Collectors.toList());

        this.sectorsField = new PamapamTwinColGrid<Sector>()
                .addSortableColumn(item -> item.getFullName(), Sector.NAME_HIERARCHICAL_ORDER::compare, "", 512)
                .withLeftColumnCaption(this.messageSource.getMessage("socialEconomyEntity.availableSectors"))
                .withRightColumnCaption(this.messageSource.getMessage("socialEconomyEntity.selectedSectors"))
                .withRows(8);
        this.sectorsField.setHeaderVisible(false);
        this.sectorsField.setCaption(this.messageSource.getMessage("socialEconomyEntity.sectors"));
        this.sectorsField.setWidth(100, Unit.PERCENTAGE);
        this.sectorsField.setItems(allSectors);

        this.binder.bind(this.sectorsField, SocialEconomyEntity::getSectors, SocialEconomyEntity::setSectors);
        tab.addComponent(this.sectorsField);
    }

    private void addSectorsCcaeField(final CrudDetailsPanel tab) {
        final List<SectorCcae> allSectorsCcae = this.sectorCcaeRepository.findAll();
        Collections.sort(allSectorsCcae, SectorCcae.NAME_ORDER);

        this.sectorCcaeField = this.componentFactory.newComboBox("socialEconomyEntity.sectorCcae");
        this.sectorCcaeField.setWidth(50, Unit.PERCENTAGE);
        this.sectorCcaeField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
        this.sectorCcaeField.setItems(allSectorsCcae);
        this.binder.bind(this.sectorCcaeField, SocialEconomyEntity::getSectorCcae, SocialEconomyEntity::setSectorCcae);
        tab.addComponent(this.sectorCcaeField);
    }

    private void addTagsField(final CrudDetailsPanel tab) {
        this.tagsField = new ExtTokenField();
        final ComboBox<ProductTagTokenizable> tagsComboField = this.componentFactory.newComboBox();
        tagsComboField.setCaption("");
        tagsComboField.setDataProvider(DataProvider.ofCollection(this.productTagTokenizables));
        tagsComboField.setTextInputAllowed(true);

        this.tagsField.setCaption(this.messageSource.getMessage("socialEconomyEntity.tags"));
        this.tagsField.setInputField(tagsComboField);
        this.tagsField.setEnableDefaultDeleteTokenAction(true);

        tagsComboField.setItemCaptionGenerator(ProductTagTokenizable::getStringValue);
        tagsComboField.setPlaceholder("Type here to add");
        tagsComboField.addValueChangeListener(event -> {
            final ProductTagTokenizable value = event.getValue();
            if (value != null) {
                this.tagsField.addTokenizable(value);
                event.getSource().setValue(null);
            }
        });
        tagsComboField.setNewItemHandler(value -> {
            final ProductTagTokenizable newTagTokenizable = new ProductTagTokenizable(new ProductTag(null, value));
            this.tagsField.addTokenizable(newTagTokenizable);
            tagsComboField.markAsDirty();
            tagsComboField.setValue(null);
        });

        this.entityTokensBinder.forField(this.tagsField).bind("productTokens");

        tab.addComponent(this.tagsField);
    }

    private void addKeywordField(final CrudDetailsPanel tab) {
        this.keywordsField = new ExtTokenField();
        final ComboBox<KeywordTagTokenizable> tagsComboField = this.componentFactory.newComboBox();
        tagsComboField.setCaption("");
        tagsComboField.setDataProvider(DataProvider.ofCollection(this.keywordTagTokenizables));
        tagsComboField.setTextInputAllowed(true);

        this.keywordsField.setCaption(this.messageSource.getMessage("socialEconomyEntity.keywords"));
        this.keywordsField.setInputField(tagsComboField);
        this.keywordsField.setEnableDefaultDeleteTokenAction(true);

        tagsComboField.setItemCaptionGenerator(KeywordTagTokenizable::getStringValue);
        tagsComboField.setPlaceholder("Type here to add");
        tagsComboField.addValueChangeListener(event -> {
            final KeywordTagTokenizable value = event.getValue();
            if (value != null) {
                this.keywordsField.addTokenizable(value);
                event.getSource().setValue(null);
            }
        });
        tagsComboField.setNewItemHandler(value -> {
            final KeywordTagTokenizable newTagTokenizable = new KeywordTagTokenizable(new KeywordTag(null, value));
            this.keywordsField.addTokenizable(newTagTokenizable);
            tagsComboField.markAsDirty();
            tagsComboField.setValue(null);
        });

        this.entityTokensBinder.forField(this.keywordsField).bind("keywordTokens");

        tab.addComponent(this.keywordsField);
    }

    private void addIdeologicalIdentificationField(final CrudDetailsPanel tab) {
        final List<IdeologicalIdentification> allIdeologicalIdentifications = this.ideologicalIdentificationRepository.findAll();
        Collections.sort(allIdeologicalIdentifications, IdeologicalIdentification.NAME_ORDER);

        this.ideologicalIdentificationField = this.componentFactory.newComboBox("socialEconomyEntity.ideologicalIdentification");
        this.ideologicalIdentificationField.setWidth(50, Unit.PERCENTAGE);
        this.ideologicalIdentificationField.setItemCaptionGenerator(item -> item.getName());
        this.ideologicalIdentificationField.setItems(allIdeologicalIdentifications);
        this.binder.bind(this.ideologicalIdentificationField, SocialEconomyEntity::getIdeologicalIdentification, SocialEconomyEntity::setIdeologicalIdentification);
        tab.addComponent(this.ideologicalIdentificationField);
    }

    private void addProductPictures(final CrudDetailsPanel tab) {
        final Label warningFescLabel = this.componentBuilderFactory.createLabelBuilder().setCaption("fesc.registration.basic.warning").build();
        tab.addComponent(warningFescLabel);

        final GridLayout picturesLayout = new GridLayout(3, 2);
        picturesLayout.setSpacing(true);
        picturesLayout.setCaption(this.messageSource.getMessage("socialEconomyEntity.productPictures"));
        IntStream.range(0, 5).forEach(i -> {
            // TODO this function overrides must be moved to jamgo-framework
            final BinaryResourceTransferField pictureField = new BinaryResourceTransferField() {
                private static final long serialVersionUID = 1L;

                @Override
                public void setValue(final BinaryObject binaryObject) {
                    super.setValue(binaryObject);
                    if (binaryObject == null) {
                        this.descriptionField.setValue("");
                        this.fileName.setValue("");
                    }
                }

                @Override
                public BinaryResource getValue() {
                    if (this.value != null) {
                        this.value.setDescription(this.descriptionField.getValue());
                    }
                    return Optional.ofNullable(this.value).map(o -> new BinaryResource(o)).orElse(null);
                }

                @Override
                public void initializeIconsLayout() {
                    this.descriptionField.setWidth(250, Unit.PIXELS);
                    this.descriptionField.setPlaceholder(SocialEconomyEntityDetailsLayout.this.messageSource.getMessage("socialEconomyEntity.productPictures.placeholder"));
                    this.descriptionField.setMaxLength(100);
                    this.descriptionField.setRows(3);
                    this.imagePreview.setWidth(320, Unit.PIXELS);
                    this.deleteButton.setStyleName(ValoTheme.BUTTON_LINK);

                    this.iconsLayout.removeAllComponents();
                    this.iconsLayout.addComponents(this.descriptionField, this.upload, this.deleteButton);
                }

            };
            pictureField.setPreviewEnabled(true);
            pictureField.setFileNameEnabled(false);
            pictureField.setDescriptionEnabled(true);
            pictureField.setDateEnabled(false);
            pictureField.setHeight(250, Unit.PIXELS);
            pictureField.setWidth(100, Unit.PERCENTAGE);
            pictureField.setSucceededListener(listener -> {
                // TODO limitar el tamaño del fichero que se sube
                final BinaryObject binaryObject = listener.getBinaryObject();
//				System.out.println("****** " + binaryObject.getFileLength());
                if (!this.pictureFieldMap.containsKey(pictureField)) {
                    final BinaryResource binaryResource = new BinaryResource(binaryObject);
                    this.targetObject.getProductPictures().add(binaryResource);
                    this.pictureFieldMap.put(pictureField, binaryResource);
                } else {
                    final BinaryResource binaryResource = this.pictureFieldMap.get(pictureField);
                    binaryResource.setContents(binaryObject.getContents());
                    binaryResource.setDescription(binaryObject.getDescription());
                    binaryResource.setFileName(binaryObject.getFileName());
                }
            });
            pictureField.addDeleteClickListener(listener -> {
                final BinaryObject binaryObject = this.pictureFieldMap.get(pictureField);
                this.targetObject.getProductPictures().remove(binaryObject);
                pictureField.setValue(null);
                this.pictureFieldMap.remove(pictureField);
            });
            this.pictureFields.add(pictureField);
            picturesLayout.addComponent(pictureField);
        });
        tab.addComponent(picturesLayout);
    }

    private void addOffersAndDiscounts(final CrudDetailsPanel panel) {
        final Window window = new Window(".");
        window.setModal(true);
        window.setWidth(50, Unit.PERCENTAGE);
        final Binder<EntityDiscount> binder = new Binder<>(EntityDiscount.class);
        final FormLayout discountFormLayout = new FormLayout();
        discountFormLayout.setMargin(true);
        final TextField nameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("socialEconomyEntity.discounts.name").setWidth(100, Unit.PERCENTAGE).build();
        discountFormLayout.addComponent(nameField);
        binder.bind(nameField, EntityDiscount::getName, EntityDiscount::setName);

        final TextArea descriptionField = this.componentBuilderFactory.createTextAreaBuilder().setCaption("socialEconomyEntity.discounts.description").setWidth(100, Unit.PERCENTAGE).build();
        discountFormLayout.addComponent(descriptionField);
        binder.bind(descriptionField, EntityDiscount::getDescription, EntityDiscount::setDescription);

        final TextField urlField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("socialEconomyEntity.discounts.url").setWidth(100, Unit.PERCENTAGE).build();
        discountFormLayout.addComponent(urlField);
        binder.bind(urlField, EntityDiscount::getUrl, EntityDiscount::setUrl);

        final DateTimeField beginDateField = this.componentBuilderFactory.createDateTimeFieldBuilder().setCaption("socialEconomyEntity.discounts.beginDate").build();
        discountFormLayout.addComponent(beginDateField);
        binder.forField(beginDateField)
                .withConverter(new LocalDateTimeToDateConverter(ZoneId.systemDefault()))
                .bind(EntityDiscount::getBeginDate, EntityDiscount::setBeginDate);
        final DateTimeField endDateField = this.componentBuilderFactory.createDateTimeFieldBuilder().setCaption("socialEconomyEntity.discounts.endDate").build();
        discountFormLayout.addComponent(endDateField);
        binder.forField(endDateField)
                .withConverter(new LocalDateTimeToDateConverter(ZoneId.systemDefault()))
                .bind(EntityDiscount::getEndDate, EntityDiscount::setEndDate);
        final Button okButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.ok").build();
        okButton.addClickListener(okListener -> {
            final EntityDiscount discount = binder.getBean();
            if (discount.getEntity() == null) {
                discount.setEntity(this.targetObject);
                this.discountList.add(discount);
            }
            try {
                binder.writeBean(discount);
                window.close();
            } catch (final ValidationException e) {
                SocialEconomyEntityDetailsLayout.logger.error(e.getMessage(), e);
            }
            this.discountListDataProvider.refreshAll();
        });
        discountFormLayout.addComponent(okButton);
        window.setContent(discountFormLayout);

        final Grid<EntityDiscount> discountGrid = new Grid<>();
        discountGrid.setWidth(80, Unit.PERCENTAGE);
        discountGrid.setCaption(this.messageSource.getMessage("socialEconomyEntity.discounts"));
        discountGrid.setDataProvider(this.discountListDataProvider);
        discountGrid.addColumn(discount -> discount.getName()).setCaption(this.messageSource.getMessage("socialEconomyEntity.discounts.name")).setExpandRatio(1);
        discountGrid.addColumn(discount -> discount.getDescription()).setCaption(this.messageSource.getMessage("socialEconomyEntity.discounts.description")).setExpandRatio(5);
        discountGrid.addComponentColumn(discount -> {
            if (Optional.ofNullable(discount.getUrl()).isPresent()) {
                final Button urlButton = new Button(discount.getUrl());
                urlButton.setStyleName(ValoTheme.BUTTON_LINK);
                return urlButton;
            }
            return null;
        }).setCaption(this.messageSource.getMessage("socialEconomyEntity.discounts.url")).setExpandRatio(1);
        discountGrid.addColumn(discount -> Optional.ofNullable(discount.getBeginDate()).map(d -> this.sdf.format(d)).orElse("")).setCaption(this.messageSource.getMessage("socialEconomyEntity.discounts.beginDate")).setExpandRatio(1);
        discountGrid.addColumn(discount -> Optional.ofNullable(discount.getEndDate()).map(d -> this.sdf.format(d)).orElse("")).setCaption(this.messageSource.getMessage("socialEconomyEntity.discounts.endDate")).setExpandRatio(1);
        discountGrid.addComponentColumn(discount -> {
            final Button editButton = this.componentBuilderFactory.createButtonBuilder().build();
            editButton.setCaption(VaadinIcons.EDIT.getHtml());
            editButton.setCaptionAsHtml(true);
            editButton.setStyleName(ValoTheme.BUTTON_LINK);
            editButton.addClickListener(listener -> {
                binder.setBean(discount);
                UI.getCurrent().addWindow(window);
            });

            final Button deleteButton = this.componentBuilderFactory.createButtonBuilder().build();
            deleteButton.setCaption(VaadinIcons.TRASH.getHtml());
            deleteButton.setCaptionAsHtml(true);
            deleteButton.setStyleName(ValoTheme.BUTTON_LINK);
            deleteButton.addClickListener(listener -> {
                this.discountList.remove(discount);
                this.discountListDataProvider.refreshAll();
            });

            final HorizontalLayout actionsLayout = new HorizontalLayout();
            actionsLayout.addComponents(editButton, deleteButton);
            return actionsLayout;
        }).setWidth(100);
        this.discountListDataProvider.addDataProviderListener(listener -> {
            final long count = listener.getSource().fetch(new Query<>()).count();
            discountGrid.setHeightByRows(count > 0 ? count : 1);
        });

        final Button addDiscountButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.add").build();
        addDiscountButton.addStyleName(ValoTheme.BUTTON_LINK);
        addDiscountButton.addClickListener(listener -> {
            binder.setBean(new EntityDiscount());
            UI.getCurrent().addWindow(window);
        });

        final VerticalLayout discountsLayout = new VerticalLayout(discountGrid, addDiscountButton);
        discountsLayout.setMargin(true);
        panel.addComponent(discountsLayout);
    }

    private void addAudiovisualDocuments(final CrudDetailsPanel panel) {
        final Binder<EntityAudiovisual> binder = new Binder<>(EntityAudiovisual.class);
        final Window window = this.createAudiovisualFormWindow(binder);

        final Grid<EntityAudiovisual> audiovisualGrid = new Grid<>();
        audiovisualGrid.setWidth(80, Unit.PERCENTAGE);
        audiovisualGrid.setCaption(this.messageSource.getMessage("socialEconomyEntity.audiovisualDocuments"));
        audiovisualGrid.setDataProvider(this.audiovisualListDataProvider);
        audiovisualGrid.addColumn(audiovisual -> audiovisual.getName()).setCaption(this.messageSource.getMessage("socialEconomyEntity.audiovisualDocuments.name")).setExpandRatio(1);
        audiovisualGrid.addColumn(audiovisual -> audiovisual.getDescription()).setCaption(this.messageSource.getMessage("socialEconomyEntity.audiovisualDocuments.description")).setExpandRatio(5);
        audiovisualGrid.addColumn(audiovisual -> audiovisual.getDuration()).setCaption(this.messageSource.getMessage("socialEconomyEntity.audiovisualDocuments.duration")).setExpandRatio(1);
        audiovisualGrid.addComponentColumn(audiovisual -> {
            if (Optional.ofNullable(audiovisual.getUrl()).isPresent()) {
                final Button urlButton = new Button(audiovisual.getUrl());
                urlButton.setStyleName(ValoTheme.BUTTON_LINK);
                return urlButton;
            }
            return null;
        }).setCaption(this.messageSource.getMessage("socialEconomyEntity.audiovisualDocuments.url")).setExpandRatio(1);
        audiovisualGrid.addComponentColumn(audiovisual -> {
            final Button editButton = this.componentBuilderFactory.createButtonBuilder().build();
            editButton.setCaption(VaadinIcons.EDIT.getHtml());
            editButton.setCaptionAsHtml(true);
            editButton.setStyleName(ValoTheme.BUTTON_LINK);
            editButton.addClickListener(listener -> {
                binder.setBean(audiovisual);
                UI.getCurrent().addWindow(window);
            });

            final Button deleteButton = this.componentBuilderFactory.createButtonBuilder().build();
            deleteButton.setCaption(VaadinIcons.TRASH.getHtml());
            deleteButton.setCaptionAsHtml(true);
            deleteButton.setStyleName(ValoTheme.BUTTON_LINK);
            deleteButton.addClickListener(listener -> {
                this.audiovisualList.remove(audiovisual);
                this.audiovisualListDataProvider.refreshAll();
            });

            final HorizontalLayout actionsLayout = new HorizontalLayout();
            actionsLayout.addComponents(editButton, deleteButton);
            return actionsLayout;
        }).setWidth(100);
        this.audiovisualListDataProvider.addDataProviderListener(listener -> {
            final long count = listener.getSource().fetch(new Query<>()).count();
            audiovisualGrid.setHeightByRows(count > 0 ? count : 1);
        });

        final Button addAudiovisualButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.add").build();
        addAudiovisualButton.addStyleName(ValoTheme.BUTTON_LINK);
        addAudiovisualButton.addClickListener(listener -> {
            binder.setBean(new EntityAudiovisual());
            UI.getCurrent().addWindow(window);
        });

        final VerticalLayout audiovisualLayout = new VerticalLayout(audiovisualGrid, addAudiovisualButton);
        audiovisualLayout.setMargin(true);
        panel.addComponent(audiovisualLayout);
    }

    private Window createAudiovisualFormWindow(final Binder<EntityAudiovisual> binder) {
        final Window window = new Window(".");
        window.setModal(true);
        window.setWidth(50, Unit.PERCENTAGE);
        final FormLayout audiovisualFormLayout = new FormLayout();
        audiovisualFormLayout.setMargin(true);
        final TextField nameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("socialEconomyEntity.audiovisualDocuments.name").setWidth(100, Unit.PERCENTAGE).build();
        audiovisualFormLayout.addComponent(nameField);
        binder.bind(nameField, EntityAudiovisual::getName, EntityAudiovisual::setName);

        final TextArea descriptionField = this.componentBuilderFactory.createTextAreaBuilder().setCaption("socialEconomyEntity.audiovisualDocuments.description").setWidth(100, Unit.PERCENTAGE).build();
        audiovisualFormLayout.addComponent(descriptionField);
        binder.bind(descriptionField, EntityAudiovisual::getDescription, EntityAudiovisual::setDescription);

        final TextArea durationField = this.componentBuilderFactory.createTextAreaBuilder().setCaption("socialEconomyEntity.audiovisualDocuments.duration").setWidth(100, Unit.PERCENTAGE).build();
        audiovisualFormLayout.addComponent(durationField);
        binder.bind(durationField, EntityAudiovisual::getDuration, EntityAudiovisual::setDuration);

        final TextField urlField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("socialEconomyEntity.audiovisualDocuments.url").setWidth(100, Unit.PERCENTAGE).build();
        audiovisualFormLayout.addComponent(urlField);
        binder.bind(urlField, EntityAudiovisual::getUrl, EntityAudiovisual::setUrl);

        final Button okButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.ok").build();
        okButton.addClickListener(okListener -> {
            final EntityAudiovisual audiovisual = binder.getBean();
            if (audiovisual.getEntity() == null) {
                audiovisual.setEntity(this.targetObject);
                this.audiovisualList.add(audiovisual);
            }
            try {
                binder.writeBean(audiovisual);
                window.close();
            } catch (final ValidationException e) {
                SocialEconomyEntityDetailsLayout.logger.error(e.getMessage(), e);
            }
            this.audiovisualListDataProvider.refreshAll();
        });
        audiovisualFormLayout.addComponent(okButton);
        window.setContent(audiovisualFormLayout);
        return window;
    }

    private void addLegalFormField(final CrudDetailsPanel tab) {
        this.legalFormField = this.componentFactory.newComboBox("socialEconomyEntity.legalForm");
        this.legalFormField.setWidth(50, Unit.PERCENTAGE);
        this.legalFormField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
        this.legalFormField.setItems(this.legalFormRepository.findAll());
        this.binder.bind(this.legalFormField, SocialEconomyEntity::getLegalForm, SocialEconomyEntity::setLegalForm);
        tab.addComponent(this.legalFormField);
    }

    private void addInvoicingField(final CrudDetailsPanel tab) {
        this.invoicingField = this.componentFactory.newTextField("socialEconomyEntity.invoicing");
        this.binder.forField(this.invoicingField)
                .withNullRepresentation("")
                .withConverter(new StringToBigDecimalConverter("Must enter a number"))
                .bind(SocialEconomyEntity::getInvoicing, SocialEconomyEntity::setInvoicing);
        tab.addComponent(this.invoicingField);
    }

    private void addFoundationYear(final CrudDetailsPanel tab) {
        this.foundationYearField = this.componentFactory.newTextField("socialEconomyEntity.foundationYear");
        this.binder.forField(this.foundationYearField)
                .withNullRepresentation("")
                .withConverter(new StringToIntegerConverter("Must enter a number"))
                .bind(SocialEconomyEntity::getFoundationYear, SocialEconomyEntity::setFoundationYear);
        tab.addComponent(this.foundationYearField);
    }

    private void addSocialEconomyNetworkTagsField(final CrudDetailsPanel tab) {
        this.socialEconomyNetworkTagsField = new ExtTokenField();
        final ComboBox<SocialEconomyNetworkTagTokenizable> socialEconomyNetworkTagsComboField = new ComboBox<>("");
        socialEconomyNetworkTagsComboField.setDataProvider(DataProvider.ofCollection(this.socialEconomyNetworkTagTokenizables));
        socialEconomyNetworkTagsComboField.setTextInputAllowed(true);

        this.socialEconomyNetworkTagsField.setCaption(this.messageSource.getMessage("socialEconomyEntity.otherSocialEconomyNetworks"));
        this.socialEconomyNetworkTagsField.setInputField(socialEconomyNetworkTagsComboField);
        this.socialEconomyNetworkTagsField.setEnableDefaultDeleteTokenAction(true);

        socialEconomyNetworkTagsComboField.setItemCaptionGenerator(SocialEconomyNetworkTagTokenizable::getStringValue);
        socialEconomyNetworkTagsComboField.setPlaceholder("Type here to add");
        socialEconomyNetworkTagsComboField.setWidthUndefined();
        socialEconomyNetworkTagsComboField.addValueChangeListener(event -> {
            final SocialEconomyNetworkTagTokenizable value = event.getValue();
            if (value != null) {
                this.socialEconomyNetworkTagsField.addTokenizable(value);
                event.getSource().setValue(null);
            }
        });
        socialEconomyNetworkTagsComboField.setNewItemHandler(value -> {
            final SocialEconomyNetworkTagTokenizable newTagTokenizable = new SocialEconomyNetworkTagTokenizable(new SocialEconomyNetworkTag(null, value));
            this.socialEconomyNetworkTagsField.addTokenizable(newTagTokenizable);
        });

        this.entityTokensBinder.forField(this.socialEconomyNetworkTagsField).bind("socialEconomyNetworkTokens");
        tab.addComponent(this.socialEconomyNetworkTagsField);
    }

    private void addExternalFilterTagsField(final CrudDetailsPanel tab) {
        final List<ExternalFilterTag> allExternalFilterTags = this.externalFilterTagRepository.findAll();
        Collections.sort(allExternalFilterTags, ExternalFilterTag.TAG_TEXT_ORDER);
        this.externalFilerTagsField = this.componentFactory.newTwinColSelect("socialEconomyEntity.externalFilterTags");
        this.externalFilerTagsField.setItemCaptionGenerator(item -> item.getTagText());
        this.externalFilerTagsField.setWidth(100, Unit.PERCENTAGE);
        this.externalFilerTagsField.setItems(allExternalFilterTags);

        this.binder.bind(this.externalFilerTagsField, SocialEconomyEntity::getExternalFilterTags, SocialEconomyEntity::setExternalFilterTags);
        tab.addComponent(this.externalFilerTagsField);
    }

    private void addEntityScopesField(final CrudDetailsPanel tab) {
        final List<EntityScope> allEntityScopes = this.entityScopeRepository.findAll();
        Collections.sort(allEntityScopes, EntityScope.NAME_ORDER);
        this.entityScopesField = this.componentFactory.newTwinColSelect("socialEconomyEntity.entityScopes");
        this.entityScopesField.setItemCaptionGenerator(item -> item.getName());
        this.entityScopesField.setWidth(100, Unit.PERCENTAGE);
        this.entityScopesField.setItems(allEntityScopes);

        this.binder.bind(this.entityScopesField, SocialEconomyEntity::getEntityScopes, SocialEconomyEntity::setEntityScopes);
        tab.addComponent(this.entityScopesField);
    }

    private void addSocialEconomyNetworksField(final CrudDetailsPanel tab) {
        final List<SocialEconomyNetwork> allSocialEconomyNetworks = this.socialEconomyNetworkRepository.findAll();
        Collections.sort(allSocialEconomyNetworks, SocialEconomyNetwork.DEFAULT_ORDER);
        this.socialEconomyNetworksField = new PamapamTwinColGrid<SocialEconomyNetwork>()
                .addSortableColumn(item -> item.getName().getDefaultText(), SocialEconomyNetwork.DEFAULT_ORDER::compare, "", 512)
                .withLeftColumnCaption(this.messageSource.getMessage("socialEconomyEntity.availableSocialEconomyNetworks"))
                .withRightColumnCaption(this.messageSource.getMessage("socialEconomyEntity.selectedSocialEconomyNetworks"))
                .withRows(8);
        this.socialEconomyNetworksField.setHeaderVisible(false);
        this.socialEconomyNetworksField.setCaption(this.messageSource.getMessage("socialEconomyEntity.socialEconomyNetworks"));
        this.socialEconomyNetworksField.setWidth(100, Unit.PERCENTAGE);
        this.socialEconomyNetworksField.setItems(allSocialEconomyNetworks);

        this.binder.bind(this.socialEconomyNetworksField, SocialEconomyEntity::getSocialEconomyNetworks, SocialEconomyEntity::setSocialEconomyNetworks);
        tab.addComponent(this.socialEconomyNetworksField);
    }

    private void addXesBalanceField(final CrudDetailsPanel tab) {
        this.xesBalanceField = this.componentFactory.newRadioButtonGroup("socialEconomyEntity.xesBalance");
        this.xesBalanceField.setItems(this.xesBalanceOptions.keySet());
        tab.addComponent(this.xesBalanceField);
    }

    private void addintercooperacioField(final CrudDetailsPanel tab) {
        final Window window = new Window(".");
        window.setModal(true);
        window.setWidth(50, Unit.PERCENTAGE);
        final Binder<TextUrl> binder = new Binder<>(TextUrl.class);
        final FormLayout intercooperacioLinksFormLayout = new FormLayout();
        intercooperacioLinksFormLayout.setMargin(true);
        final TextField nameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("socialEconomyEntity.intercooperacio.name").setWidth(100, Unit.PERCENTAGE).build();
        intercooperacioLinksFormLayout.addComponent(nameField);
        binder.bind(nameField, TextUrl::getName, TextUrl::setName);

        final TextField urlField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("socialEconomyEntity.intercooperacio.url").setWidth(100, Unit.PERCENTAGE).build();
        intercooperacioLinksFormLayout.addComponent(urlField);
        binder.bind(urlField, TextUrl::getUrl, TextUrl::setUrl);

        final Button okButton = this.componentBuilderFactory.createButtonBuilder().setCaption("action.ok").build();
        okButton.addClickListener(okListener -> {
            final TextUrl textUrl = binder.getBean();
            final SocialEconomyEntity textUrlEntity = textUrl.getEntity();

            if (textUrlEntity == null) {
                textUrl.setEntity(this.targetObject);

                this.intercooperacioLinksList.add(textUrl);
            }
            try {
                binder.writeBean(textUrl);
                window.close();
            } catch (final ValidationException e) {
                SocialEconomyEntityDetailsLayout.logger.error(e.getMessage(), e);
            }
            this.intercooperacioLinksListDataProvider.refreshAll();
        });

        intercooperacioLinksFormLayout.addComponent(okButton);
        window.setContent(intercooperacioLinksFormLayout);

        final Grid<TextUrl> intercooperacioLinksGrid = new Grid<>();
        intercooperacioLinksGrid.setWidth(80, Unit.PERCENTAGE);
        intercooperacioLinksGrid.setCaption(this.messageSource.getMessage("socialEconomyEntity.intercooperacio"));
        intercooperacioLinksGrid.setDataProvider(this.intercooperacioLinksListDataProvider);

        intercooperacioLinksGrid.addColumn(TextUrl::getName)
                .setCaption(this.messageSource.getMessage("socialEconomyEntity.intercooperacio.name"))
                .setExpandRatio(1);

        intercooperacioLinksGrid.addComponentColumn(link -> {

                    if (Optional.ofNullable(link.getUrl()).isPresent()) {
                        final Button urlButton = new Button(link.getUrl());
                        urlButton.setStyleName(ValoTheme.BUTTON_LINK);
                        return urlButton;
                    }

                    return null;

                }).setCaption(this.messageSource.getMessage("socialEconomyEntity.intercooperacio.url"))
                .setExpandRatio(1);

        intercooperacioLinksGrid.addComponentColumn(link -> {
            final Button editButton = this.componentBuilderFactory.createButtonBuilder().build();
            editButton.setCaption(VaadinIcons.EDIT.getHtml());
            editButton.setCaptionAsHtml(true);
            editButton.setStyleName(ValoTheme.BUTTON_LINK);
            editButton.addClickListener(listener -> {
                binder.setBean(link);
                UI.getCurrent().addWindow(window);
            });

            final Button deleteButton = this.componentBuilderFactory.createButtonBuilder().build();
            deleteButton.setCaption(VaadinIcons.TRASH.getHtml());
            deleteButton.setCaptionAsHtml(true);
            deleteButton.setStyleName(ValoTheme.BUTTON_LINK);
            deleteButton.addClickListener(listener -> {
                this.intercooperacioLinksList.remove(link);
                this.intercooperacioLinksListDataProvider.refreshAll();
            });

            final HorizontalLayout actionsLayout = new HorizontalLayout();
            actionsLayout.addComponents(editButton, deleteButton);
            return actionsLayout;
        }).setWidth(100);

        this.intercooperacioLinksListDataProvider.addDataProviderListener(listener -> {
            final long count = listener.getSource().fetch(new Query<>()).count();
            intercooperacioLinksGrid.setHeightByRows(count > 0 ? count : 1);
        });

        this.intercoopAddLinkButton = this.componentBuilderFactory.createButtonBuilder()
                .setCaption("action.add")
                .build();

        this.intercoopAddLinkButton.addStyleName(ValoTheme.BUTTON_LINK);
        this.intercoopAddLinkButton.addClickListener(listener -> {
            binder.setBean(new TextUrl());
            UI.getCurrent().addWindow(window);
        });

        final VerticalLayout intercooperacioLayout = new VerticalLayout(intercooperacioLinksGrid, this.intercoopAddLinkButton);
        intercooperacioLayout.setMargin(true);
        tab.addComponent(intercooperacioLayout);
    }

    private void addEntityPersonRolesFields(final CrudDetailsPanel tab) {
        this.personRoleRows = Maps.newHashMap();
        this.personRoleRowBinders = Maps.newHashMap();
        this.personRoleRowFields = new ArrayList<>();

        tab.addComponent(new Label(""));
        final Label personRolesLabel = this.componentFactory.newLabel();
        personRolesLabel.setCaption(this.messageSource.getMessage("socialEconomyEntity.personRoles"));
        personRolesLabel.setCaption("");
        tab.addComponent(personRolesLabel);

        for (final PersonRole eachPersonRole : this.getAllPersonRoles()) {
            tab.addComponent(this.createPersonRoleGroup(eachPersonRole));
        }
        tab.addComponent(new Label(""));
    }

    private void addEntityEvaluationFields(final CrudDetailsPanel tab, final Questionaire questionaire) {
        this.criterionFields.put(questionaire.getId(), new HashMap<>());

        final DateField evaluationDateField = this.componentFactory.newDateField("socialEconomyEntity.evaluationDate");

        final LocalDate evaluationDate = Optional.ofNullable(this.targetObject)
                .map(SocialEconomyEntity::getEntityEvaluation)
                .map(EntityEvaluation::getDate)
                .map(o -> o.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .orElse(LocalDate.now());

        evaluationDateField.setValue(evaluationDate);
        // ...	FIXME: Controlar que el formato por locale en cada navegador sale bien y no setearlo hardcoded.
        evaluationDateField.setDateFormat("dd/MM/yyyy");
        this.evaluationDateFields.put(questionaire.getId(), evaluationDateField);
        tab.addComponent(evaluationDateField);

        for (final Criterion eachCriterion : this.getAllCriteria()) {
            final CriterionGroupLayout criterionGroupLayout = this.applicationContext.getBean(CriterionGroupLayout.class);
            criterionGroupLayout.setQuestionaire(questionaire);
            criterionGroupLayout.initialize(eachCriterion);
            this.criterionFields.get(questionaire.getId()).put(eachCriterion.getId(), criterionGroupLayout);
            tab.addComponent(criterionGroupLayout);
        }
    }

    private void addEntityStatusChangesSection(final CrudDetailsPanel tab) {
        final Label boxTitle = this.componentFactory.newLabel();
        boxTitle.setValue(this.messageSource.getMessage("socialEconomyEntity.statusChanges"));
        tab.addComponent(boxTitle);
        this.entityStatusChangesField = this.componentFactory.newGrid();
        this.entityStatusChangesField.setSelectionMode(SelectionMode.NONE);
        this.entityStatusChangesField.setWidth(100, Unit.PERCENTAGE);
        this.entityStatusChangesField.setHeaderVisible(false);
        final Column<EntityStatusChange, String> dateColumn = this.entityStatusChangesField.addColumn(obj -> {
            final Date date = obj.getChangeTimestamp();
            if (date != null) {
                return this.fullDateTimeFormater.format(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            } else {
                return "";
            }
        });
        dateColumn.setWidth(160);
        this.entityStatusChangesField.addColumn(obj -> {
            if (obj.getEntityStatusFrom() == null) {
                return obj.getEntityStatusTo().getName().getDefaultText();
            } else {
                return obj.getEntityStatusFrom().getName().getDefaultText() + " -> " + obj.getEntityStatusTo().getName().getDefaultText();
            }
        });
        this.entityStatusChangesField.addColumn(obj -> obj.getUser().getFullName());
        tab.addComponent(this.entityStatusChangesField);
    }

    private void addSiblingEntitiesSection(final CrudDetailsPanel tab) {
        final Label boxTitle = this.componentFactory.newLabel();
        boxTitle.setValue(this.messageSource.getMessage("socialEconomyEntity.siblings"));
        tab.addComponent(boxTitle);
        this.siblingEntitiesField = ((PamapamComponentFactory) this.componentFactory).createEntitiesGrid(true);
        this.siblingEntitiesField.setWidth(100, Unit.PERCENTAGE);
        tab.addComponent(this.siblingEntitiesField);
    }

    private void addOfficeEntitiesSection(final CrudDetailsPanel tab) {
        final Label boxTitle = this.componentFactory.newLabel();
        boxTitle.setValue(this.messageSource.getMessage("socialEconomyEntity.offices"));
        tab.addComponent(boxTitle);
        this.officeEntitiesField = ((PamapamComponentFactory) this.componentFactory).createEntitiesGrid(true);
        this.officeEntitiesField.setWidth(100, Unit.PERCENTAGE);
        tab.addComponent(this.officeEntitiesField);
    }

    private void addPublicAdministrationField(final CrudDetailsPanel tab) {
        final Label focusedOnLabel = this.componentBuilderFactory.createLabelBuilder().setCaption("fesc.registration.basic.focusedOn").build();
        tab.addComponent(focusedOnLabel);

        if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
            this.focusedOnEnterprisesField = this.componentFactory.newCheckBox("fesc.registration.basic.enterprises");
            tab.addComponent(this.focusedOnEnterprisesField);

            this.focusedOnPrivateNoobsField = this.componentFactory.newCheckBox("fesc.registration.basic.privateNoobs");
            tab.addComponent(this.focusedOnPrivateNoobsField);

            this.focusedOnPrivateActivistsField = this.componentFactory.newCheckBox("fesc.registration.basic.privateActivists");
            tab.addComponent(this.focusedOnPrivateActivistsField);
        }

        this.focusedOnPublicAdministrationField = this.componentFactory.newCheckBox("fesc.registration.basic.publicAdministration.short");
        tab.addComponent(this.focusedOnPublicAdministrationField);

    }

    @Override
    protected Class<SocialEconomyEntity> getTargetObjectClass() {
        return SocialEconomyEntity.class;
    }

    public Button getOkPendingButton() {
        return this.okPendingButton;
    }

    public Button getOkDraftButton() {
        return this.okDraftButton;
    }

    public Button getCreateDraftButton() {
        return this.createDraftButton;
    }

    public Button getCreateCopyButton() {
        return this.createCopyButton;
    }

    public Button getCreateOfficeButton() {
        return this.createOfficeButton;
    }

    public Consumer<SocialEconomyEntityDetailsLayout> getOkDraftHandler() {
        return this.okDraftHandler;
    }

    public void setOkDraftHandler(final Consumer<SocialEconomyEntityDetailsLayout> okDraftHandler) {
        this.okDraftHandler = okDraftHandler;
    }

    public Consumer<SocialEconomyEntityDetailsLayout> getOkPendingHandler() {
        return this.okPendingHandler;
    }

    public void setOkPendingHandler(final Consumer<SocialEconomyEntityDetailsLayout> okPendingHandler) {
        this.okPendingHandler = okPendingHandler;
    }

    public Consumer<SocialEconomyEntityDetailsLayout> getCreateDraftHandler() {
        return this.createDraftHandler;
    }

    public void setCreateDraftHandler(final Consumer<SocialEconomyEntityDetailsLayout> createDraftHandler) {
        this.createDraftHandler = createDraftHandler;
    }

    public Consumer<SocialEconomyEntityDetailsLayout> getCreateCopyHandler() {
        return this.createCopyHandler;
    }

    public void setCreateCopyHandler(final Consumer<SocialEconomyEntityDetailsLayout> createCopyHandler) {
        this.createCopyHandler = createCopyHandler;
    }

    public Consumer<SocialEconomyEntityDetailsLayout> getCreateOfficeHandler() {
        return this.createOfficeHandler;
    }

    public void setCreateOfficeHandler(final Consumer<SocialEconomyEntityDetailsLayout> createOfficeHandler) {
        this.createOfficeHandler = createOfficeHandler;
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void setChanged(final boolean changed) {
        this.changed = changed;
    }

    public boolean isStatusChanged() {
        return !Objects.equals(this.targetObject.getEntityStatus(), this.previousStatus);
    }

    public EntityStatus getPreviousStatus() {
        return this.previousStatus;
    }

}
