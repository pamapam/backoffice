package org.pamapam.backoffice.layout.details;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.model.ExternalFilterTag;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ExternalFilterTagDetailsLayout extends CrudDetailsLayout<ExternalFilterTag> {

	private static final long serialVersionUID = 1L;

	private TextField tagTextField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.tagTextField = this.componentFactory.newTextField("externalFilterTag.tagText");
		this.tagTextField.setSizeFull();
		this.tagTextField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.tagTextField);
		this.binder.bind(this.tagTextField, ExternalFilterTag::getTagText, ExternalFilterTag::setTagText);

		return panel;
	}

	@Override
	protected Class<ExternalFilterTag> getTargetObjectClass() {
		return ExternalFilterTag.class;
	}

}
