package org.pamapam.backoffice.layout.details;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.FescRegistrationFee;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FescRegistrationFeeDetailsLayout extends CrudDetailsLayout<FescRegistrationFee> {

	private static final long serialVersionUID = 1L;

	private JmgLocalizedTextField nameField;
	private TextField feeField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = (JmgLocalizedTextField) this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("fesc.registration.invoice.fee.name").setWidth(50, Unit.PERCENTAGE).build();
		this.binder.bind(this.nameField, FescRegistrationFee::getName, FescRegistrationFee::setName);
		panel.addComponent(this.nameField);

		this.feeField = this.componentBuilderFactory.createTextFieldBuilder()
			.setCaption("fesc.registration.invoice.fee.fee")
			.build();
		this.feeField.setWidth(90, Unit.PERCENTAGE);
		this.binder.forField(this.feeField)
			.withConverter(new StringToIntegerConverter("Must enter a number"))
			.withNullRepresentation(0)
			.bind(FescRegistrationFee::getFee, FescRegistrationFee::setFee);
		panel.addComponent(this.feeField);

		return panel;
	}

	@Override
	protected Class<FescRegistrationFee> getTargetObjectClass() {
		return FescRegistrationFee.class;
	}

}
