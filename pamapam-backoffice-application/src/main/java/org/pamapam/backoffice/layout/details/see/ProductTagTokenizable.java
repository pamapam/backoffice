package org.pamapam.backoffice.layout.details.see;

import java.util.Objects;

import org.pamapam.model.ProductTag;

import com.explicatis.ext_token_field.Tokenizable;

public class ProductTagTokenizable implements Tokenizable {

	private ProductTag productTag;

	public ProductTagTokenizable(ProductTag productTag) {
		this.productTag = productTag;
	}

	public ProductTag getProductTag() {
		return this.productTag;
	}

	public void setProductTag(ProductTag productTag) {
		this.productTag = productTag;
	}

	@Override
	public String getStringValue() {
		return this.productTag.getTag();
	}

	@Override
	public long getIdentifier() {
		if (this.productTag.getId() == null) {
			return this.hashCode();
		} else {
			return this.productTag.getId();
		}
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == this) {
			return true;
		}
		if (!(obj instanceof ProductTagTokenizable)) {
			return false;
		}
		ProductTagTokenizable other = (ProductTagTokenizable) obj;
		return Objects.equals(this.productTag, other.productTag);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.productTag);
	}

}
