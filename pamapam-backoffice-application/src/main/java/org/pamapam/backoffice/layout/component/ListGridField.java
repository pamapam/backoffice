package org.pamapam.backoffice.layout.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.vaadin.shared.Registration;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public abstract class ListGridField<T extends Object> extends CustomField<List<T>> {

	private static final long serialVersionUID = 1L;

	protected Label helpLabel = new Label();
	protected Grid<T> fieldsGrid = new Grid<T>();
	protected Button newElementButton = new Button();
	protected Button removeElementsButton = new Button();

	private List<T> value;

	public ListGridField(String caption) {
		this.setCaption(caption);
	}

	public ListGridField(String caption, String addButtonCaption, String deleteButtonCaption) {
		this.setCaption(caption);
		this.newElementButton.setCaption(addButtonCaption);
		this.removeElementsButton.setCaption(deleteButtonCaption);
	}

	public abstract T createEmptyItem();

	protected abstract void configureGridColumns();

	protected abstract void configureCaptions();

	@Override
	protected Component initContent() {
		VerticalLayout layout = new VerticalLayout();
		layout.setMargin(false);
		layout.setSpacing(true);
		layout.setWidth("100%");
		layout.setHeight("100%");

		this.fieldsGrid.getEditor().setEnabled(true);
		this.fieldsGrid.setSelectionMode(SelectionMode.MULTI);
		this.fieldsGrid.setColumnReorderingAllowed(false);
		this.fieldsGrid.setWidth("100%");
		this.fieldsGrid.getEditor().setEnabled(true);

		this.configureGridColumns();
		this.configureCaptions();

		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setMargin(false);
		buttonsLayout.setSpacing(true);
		buttonsLayout.setWidth(100, Unit.PERCENTAGE);

		this.removeElementsButton.setWidth(100, Unit.PERCENTAGE);
		this.removeElementsButton.addClickListener(e -> this.removeSelectedItems());
		this.newElementButton.setWidth(100, Unit.PERCENTAGE);
		this.newElementButton.addClickListener(e -> this.addNewItem());
		buttonsLayout.addComponents(this.newElementButton, this.removeElementsButton);
		buttonsLayout.setExpandRatio(this.newElementButton, 1.0f);
		buttonsLayout.setExpandRatio(this.removeElementsButton, 1.0f);

		layout.addComponents(this.helpLabel, this.fieldsGrid, buttonsLayout);
		layout.setExpandRatio(this.fieldsGrid, 1);
		return layout;
	}

	public Registration addNewElementClickListener(ClickListener listener) {
		return this.newElementButton.addClickListener(listener);
	}

	private void addNewItem() {
		if (this.getValue() == null) {
			this.value = new ArrayList<>();
		}
		this.value.add(this.createEmptyItem());
		this.fieldsGrid.setItems(this.value);
	}

	private void removeSelectedItems() {
		Set<T> selectedItems = this.fieldsGrid.getSelectedItems();
		this.removeItems(selectedItems);
	}

	private void removeItems(Set<T> selectedItems) {
		this.value.removeAll(selectedItems);
		this.fieldsGrid.setItems(this.value);
	}

	@Override
	public List<T> getValue() {
		return this.value;
	}

	@Override
	protected void doSetValue(List<T> value) {
		if (value == null) {
			value = new ArrayList<>();
		}
		this.fieldsGrid.setItems(value);
		this.value = value;
	}
}
