package org.pamapam.backoffice.layout.details;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.server.SerializablePredicate;
import com.vaadin.ui.*;
import org.apache.tika.Tika;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.ui.JmgLocalizedTextArea;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.pamapam.model.Criterion;
import org.pamapam.model.CriterionAnswer;
import org.pamapam.model.Questionaire;
import org.pamapam.repository.QuestionaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.easyuploads.UploadField;
import org.vaadin.easyuploads.UploadField.FieldType;

import com.vaadin.data.ValidationException;
import com.vaadin.server.StreamResource;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CriterionDetailsLayout extends CrudDetailsLayout<Criterion> {

	private static final long serialVersionUID = 1L;

	@Autowired
	QuestionaireRepository questionaireRepository;

	private CheckBoxGroup<Questionaire> questionaireField;

	private JmgLocalizedTextField nameField;
	private JmgLocalizedTextField questionField;
	private JmgLocalizedTextArea descriptionField;

	private GridLayout criterionAnswers;

	private UploadField iconUploadField;
	private Image iconField;

	private Map<CriterionAnswer, JmgLocalizedTextArea> criterionAnswerFields;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

//TODO: Cambiar esto por un radio button con los questionaries que refresque los criteriaAnswers onClick

//		this.questionaireField = (CheckBoxGroup<Questionaire>) this.componentBuilderFactory.<Questionaire> createCheckBoxGroupBuilder()
//			.setCaption("questionaires")
//			.setWidth(100, Unit.PERCENTAGE)
//			.build();
//		List<Questionaire> questionaires = this.questionaireRepository.findAll();
//		DataProvider<Questionaire, SerializablePredicate<Questionaire>> dp = DataProvider.ofCollection(questionaires);
//		this.questionaireField.setDataProvider(dp);
//		this.questionaireField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
//
//		panel.addComponent(this.questionaireField);
//		this.binder.bind(this.questionaireField, Criterion::getQuestionaires, Criterion::setQuestionaires);

		this.nameField = (JmgLocalizedTextField)this.componentBuilderFactory
				.createLocalizedTextFieldBuilder()
				.setCaption("criterion.name")
				.setWidth(50, Unit.PERCENTAGE)
				.build();

		panel.addComponent(this.nameField);

		this.questionField = (JmgLocalizedTextField)this.componentBuilderFactory.createLocalizedTextFieldBuilder().setCaption("criterion.question").setWidth(50, Unit.PERCENTAGE).build();
		this.questionField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.questionField);

		this.descriptionField = (JmgLocalizedTextArea)this.componentBuilderFactory.createLocalizedTextAreaBuilder().setCaption("criterion.description").setWidth(50, Unit.PERCENTAGE).build();
		this.descriptionField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.descriptionField);

		this.addIconLayout(panel);

		this.criterionAnswers = this.componentFactory.newGridLayout();
		this.criterionAnswers.setCaption(this.messageSource.getMessage("criterion.answers"));
		this.criterionAnswers.setWidth(100, Unit.PERCENTAGE);
		this.criterionAnswers.setColumns(2);
		this.criterionAnswers.setSpacing(true);
		panel.addComponent(this.criterionAnswers);

		this.criterionAnswerFields = new HashMap<>();

		return panel;
	}

	private void addIconLayout(CrudDetailsPanel panel) {
		HorizontalLayout pictureLayout = this.componentFactory.newHorizontalLayout();
		pictureLayout.setCaption(this.messageSource.getMessage("criterion.icon"));

		this.iconUploadField = this.componentFactory.newUploadField();
		this.iconUploadField.setAcceptFilter("image/*");
		this.iconUploadField.setFieldType(FieldType.BYTE_ARRAY);
		this.iconUploadField.setButtonCaption(this.messageSource.getMessage("criterion.icon.chooseFile"));
		this.iconUploadField.addListener((Listener) event -> CriterionDetailsLayout.this.showUploadedImage());
		pictureLayout.addComponent(this.iconUploadField);

		this.iconField = this.componentFactory.newImage();
		this.iconField.setHeight(34, Unit.PIXELS);
		pictureLayout.addComponent(this.iconField);

		panel.addComponent(pictureLayout);
	}

	private void showUploadedImage() {
		Object value = this.iconUploadField.getValue();
		if (value != null) {
			final byte[] data = (byte[]) value;

			String fileName = this.iconUploadField.getLastFileName();
			if (fileName == null) {
				fileName = "tmp";
			}
			fileName = fileName + String.valueOf((new Date()).getTime());
			StreamResource resource = new StreamResource(() -> new ByteArrayInputStream(data), fileName);

			this.iconField.setSource(resource);
		} else {
			this.iconField.setSource(null);
		}
	}

	@Override
	public void updateFields(Object object) {
		Criterion criterion = (Criterion) object;
		super.updateFields(criterion);

		if (this.targetObject != null) {
			this.criterionAnswers.removeAllComponents();
			this.criterionAnswerFields.clear();

			for (CriterionAnswer eachAnswer : this.targetObject.getCriterionAnswers()) {
				Label orderField = this.componentFactory.newLabel();
				orderField.setValue(eachAnswer.getAnswerOrder().toString());
				this.criterionAnswers.addComponent(orderField);
				JmgLocalizedTextArea answerTextField =  (JmgLocalizedTextArea)this.componentBuilderFactory.createLocalizedTextAreaBuilder().setWidth(100, Unit.PERCENTAGE).build();
				answerTextField.setLocalizedText(eachAnswer.getAnswer());
				this.criterionAnswers.addComponent(answerTextField);
				this.criterionAnswers.setColumnExpandRatio(1, 1);

				this.criterionAnswerFields.put(eachAnswer, answerTextField);
			}

			this.nameField.setLocalizedText(this.targetObject.getName());
			this.questionField.setLocalizedText(this.targetObject.getQuestion());
			this.descriptionField.setLocalizedText(this.targetObject.getDescription());

			if (this.targetObject.getIcon() != null) {
				byte[] pictureBytes = this.targetObject.getIcon().getContents();
				this.iconUploadField.setValue(pictureBytes);
			} else {
				this.iconUploadField.clear();
			}
			this.showUploadedImage();
		}
	}

	@Override
	public void updateTargetObject() throws ValidationException {
		super.updateTargetObject();

		//this.targetObject.setQuestionaires(this.questionaireField.getValue());
		this.targetObject.setName(this.nameField.getValue());
		this.targetObject.setQuestion(this.questionField.getValue());
		this.targetObject.setDescription(this.descriptionField.getValue());

		if (this.iconUploadField.getValue() != null) {
			BinaryResource icon = new BinaryResource((byte[]) this.iconUploadField.getValue());
			InputStream pictureStream = new ByteArrayInputStream(icon.getContents());
			String mimeType = "";
			try {
				mimeType = (new Tika()).detect(pictureStream);
				pictureStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			icon.setMimeType(mimeType);
			icon.setFileLength(icon.getContents().length);
			icon.setFileName(this.iconUploadField.getLastFileName());
			this.targetObject.setIcon(icon);
		}

		this.criterionAnswerFields.entrySet().stream().forEach(entry -> entry.getKey().setAnswer(entry.getValue().getValue()));
	}

	@Override
	protected Class<Criterion> getTargetObjectClass() {
		return Criterion.class;
	}

}
