package org.pamapam.backoffice.layout.details;

import java.util.List;
import java.util.Optional;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.pamapam.model.EmbeddedMapConfig;
import org.pamapam.model.EntityScope;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.ExternalFilterTag;
import org.pamapam.repository.EntityScopeRepository;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.repository.ExternalFilterTagRepository;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToBigDecimalConverter;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EmbeddedMapConfigDetailsLayout extends CrudDetailsLayout<EmbeddedMapConfig> {

	private static final long serialVersionUID = 1L;
	private static final String LATITUDE_REGEX = "^\\-?[0-8]?[0-9]\\.\\d+$|^-?90\\.[0]+$";
	private static final String LONGITUDE_REGEX = "^\\-?[1]?[0-7]?[0-9]\\.\\d+$|^\\-?[0-9]?[0-9]\\.\\d+$|^-?180\\.[0]+$";

	@Autowired
	private ExternalFilterTagRepository externalFilterTagRepository;
	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private EntityScopeRepository entityScopeRepository;

	private TextField domainField;
	private TextField apiKeyField;
	private Button generateKeyButton;
	private ListSelect<ExternalFilterTag> externalFilterTagField;
	private ListSelect<EntityStatus> entityStatusesField;
	private ListSelect<EntityScope> entityScopesField;
	private TextArea sampleIframeLabel;
	private TextField latitudeField;
	private TextField longitudeField;
	private TextField zoomField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.domainField = this.componentFactory.newTextField("embeddedMapConfig.domain");
		this.domainField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.domainField);
		this.binder.forField(this.domainField)
			.asRequired()
			.bind(EmbeddedMapConfig::getDomain, EmbeddedMapConfig::setDomain);

		this.addApiKey(panel);

		this.externalFilterTagField = this.componentFactory.newListSelect("embeddedMapConfig.tags");
		this.externalFilterTagField.setDataProvider(DataProvider.ofCollection(this.externalFilterTagRepository.findAll()));
		this.externalFilterTagField.setRows(6);
		this.externalFilterTagField.setItemCaptionGenerator(item -> item.getTagText());
		this.externalFilterTagField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.externalFilterTagField);
		this.binder.bind(this.externalFilterTagField, EmbeddedMapConfig::getExternalFilterTags, EmbeddedMapConfig::setExternalFilterTags);

		this.entityStatusesField = this.componentFactory.newListSelect("embeddedMapConfig.statuses");
		this.entityStatusesField.setDataProvider(DataProvider.ofCollection(this.entityStatusRepository.findAll()));
		this.entityStatusesField.setRows(6);
		this.entityStatusesField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.entityStatusesField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.entityStatusesField);
		this.binder.bind(this.entityStatusesField, EmbeddedMapConfig::getStatuses, EmbeddedMapConfig::setStatuses);

		this.entityScopesField = this.componentFactory.newListSelect("embeddedMapConfig.entityScopes");
		this.entityScopesField.setDataProvider(DataProvider.ofCollection(this.entityScopeRepository.findAll()));
		this.entityScopesField.setRows(6);
		this.entityScopesField.setItemCaptionGenerator(item -> item.getName());
		this.entityScopesField.setWidth(50, Unit.PERCENTAGE);
		panel.addComponent(this.entityScopesField);
		this.binder.bind(this.entityScopesField, EmbeddedMapConfig::getEntityScopes, EmbeddedMapConfig::setEntityScopes);

		final HorizontalLayout centerLayout = this.componentFactory.newHorizontalLayout("embeddedMapConfig.center");
		this.latitudeField = this.componentFactory.newTextField();
		this.latitudeField.addValueChangeListener(event -> {
			this.sampleIframeLabel.setValue(this.getSampleIframe());
		});
		centerLayout.addComponent(this.latitudeField);
		this.binder.forField(this.latitudeField)
			.asRequired()
			.withNullRepresentation("")
			.withValidator(new RegexpValidator("Must enter a valid latitude", EmbeddedMapConfigDetailsLayout.LATITUDE_REGEX))
//			.withConverter(new StringToBigDecimalConverter("Must enter a number"))
			.bind(EmbeddedMapConfig::getLatitude, EmbeddedMapConfig::setLatitude);
		this.longitudeField = this.componentFactory.newTextField();
		this.longitudeField.addValueChangeListener(event -> {
			this.sampleIframeLabel.setValue(this.getSampleIframe());
		});
		centerLayout.addComponent(this.longitudeField);
		this.binder.forField(this.longitudeField)
			.asRequired()
			.withNullRepresentation("")
			.withValidator(new RegexpValidator("Must enter a valid longitude", EmbeddedMapConfigDetailsLayout.LONGITUDE_REGEX))
//			.withConverter(new StringToBigDecimalConverter("Must enter a number"))
			.bind(EmbeddedMapConfig::getLongitude, EmbeddedMapConfig::setLongitude);
		panel.addComponent(centerLayout);

		this.zoomField = this.componentFactory.newTextField("embeddedMapConfig.zoom");
		this.zoomField.addValueChangeListener(event -> {
			this.sampleIframeLabel.setValue(this.getSampleIframe());
		});
		panel.addComponent(this.zoomField);
		this.binder.forField(this.zoomField)
			.asRequired()
			.withNullRepresentation("")
			.withConverter(new StringToBigDecimalConverter("Must enter a number"))
			.bind(EmbeddedMapConfig::getZoom, EmbeddedMapConfig::setZoom);

		this.sampleIframeLabel = this.componentFactory.newTextArea("embeddedMapConfig.sampleIframe");
		this.sampleIframeLabel.setRows(8);
		this.sampleIframeLabel.setWidth(100, Unit.PERCENTAGE);
		this.sampleIframeLabel.setReadOnly(true);
		panel.addComponent(this.sampleIframeLabel);

		return panel;
	}

	@Override
	protected Class<EmbeddedMapConfig> getTargetObjectClass() {
		return EmbeddedMapConfig.class;
	}

	private void addApiKey(final CrudDetailsPanel panel) {
		final HorizontalLayout apiKeyLayout = this.componentFactory.newHorizontalLayout("embeddedMapConfig.apiKey");
		apiKeyLayout.setWidth(50, Unit.PERCENTAGE);

		this.apiKeyField = this.componentFactory.newTextField();
		this.apiKeyField.setWidth(100, Unit.PERCENTAGE);
		apiKeyLayout.addComponent(this.apiKeyField);
		this.binder.forField(this.apiKeyField)
			.asRequired()
			.bind(EmbeddedMapConfig::getApiKey, EmbeddedMapConfig::setApiKey);
		apiKeyLayout.setExpandRatio(this.apiKeyField, 1);

		this.generateKeyButton = this.componentBuilderFactory.createButtonBuilder().build();
		this.generateKeyButton.setIcon(VaadinIcons.KEY_O);
		this.generateKeyButton.setDescription(this.messageSource.getMessage("embeddedMapConfig.generateApiKey"));
		this.generateKeyButton.addClickListener(event -> {
			final String generatedApiKey = this.generateApiKey();
			this.apiKeyField.setValue(generatedApiKey);
			this.sampleIframeLabel.setValue(this.getSampleIframe());
		});
		apiKeyLayout.addComponent(this.generateKeyButton);

		panel.addComponent(apiKeyLayout);
	}

	private String generateApiKey() {
		final List<CharacterRule> rules = Lists.newArrayList(
			new CharacterRule(EnglishCharacterData.UpperCase, 1),
			new CharacterRule(EnglishCharacterData.LowerCase, 1),
			new CharacterRule(EnglishCharacterData.Digit, 1));
		final PasswordGenerator generator = new PasswordGenerator();
		return generator.generatePassword(32, rules);
	}

	private String getSampleIframe() {
		final String apiKey = Optional.ofNullable(this.apiKeyField.getValue()).map(v -> v.toString()).orElse("");
		final String latitude = Optional.ofNullable(this.latitudeField.getValue()).map(v -> v.toString()).orElse("");
		final String longitude = Optional.ofNullable(this.longitudeField.getValue()).map(v -> v.toString()).orElse("");
		final String zoom = Optional.ofNullable(this.zoomField.getValue()).map(v -> v.toString()).orElse("");
		return "<iframe\n" +
			"	id=\"map-iframe\"\n" +
			"	width=\"100%\"\n" +
			"	height=\"600\"\n" +
			"	frameborder=\"0\" style=\"border:0\" scrolling=\"no\"\n" +
			"	src=\"https://pamapam.cat/map/embed?center=[" + latitude + "," + longitude + "]&zoom=" + zoom + "&apiKey=" + apiKey + "\"\n" +
			"	allowfullscreen>\n" +
			"</iframe>";
	}

	@Override
	public void updateFields(final Object object) {
		final EmbeddedMapConfig embeddedMapConfig = (EmbeddedMapConfig) object;
		super.updateFields(embeddedMapConfig);

		if (embeddedMapConfig != null) {
			this.sampleIframeLabel.setValue(this.getSampleIframe());
		}
	}

//	@Override
//	public void updateTargetObject() {
//		try {
//			this.validateForm();
//		} catch (ValidationException e) {
//			// ...	TODO: Ver dónde conviene atrapar la exception.
//			e.printStackTrace();
//		}
//		super.updateTargetObject();
//	}

	@Override
	protected void doOk() {
		try {
			this.validateForm();
			super.doOk();
		} catch (final ValidationException e) {
			Notification.show(this.messageSource.getMessage("dialog.hasErrors"), Type.ERROR_MESSAGE);
		} catch (final IllegalArgumentException e) {
			Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
		}
	}

}
