package org.pamapam.backoffice.layout.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.ui.*;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/** Field that adds two textfields to the list (key:value) every time the user clicks on the + button.
 *
 * Right now, it returns a Map with two strings as value but it would be cool to transform them to json using the 2 methods
 * included on this page.
 *
 */
public class DynamicMapField extends CustomField<Map<String, String>> {
    private final Button button = new Button("+");
    @Autowired
    protected JamgoComponentFactory componentFactory;
    private Map<TextField, TextField> linksList;
    @Override
    protected Component initContent() {
        this.linksList = new LinkedHashMap<>();
        VerticalLayout layout = new VerticalLayout();
        button.addClickListener(event -> {
            TextField link = new TextField();
            TextField entity = new TextField();

            HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.addComponent(link);
            horizontalLayout.addComponent(entity);
            this.linksList.put(entity, link);

            layout.addComponent(horizontalLayout);
        });


        layout.addComponent(new Label("Click the button"));
        layout.addComponent(button);
        return layout;
    }

    @Override
    public Map<String, String> getValue() {
        Map<String, String> values = new HashMap<>();

        for (Map.Entry<TextField,TextField> entry : this.linksList.entrySet()) {
            String entity = Optional.ofNullable(entry.getKey()).map(TextField::getValue).orElse("");
            String link = Optional.ofNullable(entry.getValue()).map(TextField::getValue).orElse("");

            values.put(entity, link);
        }

        return values;
    }

    @Override
    protected void doSetValue(Map<String, String> linksList) {
        for (Map.Entry<String,String> entry : linksList.entrySet()) {
            String entity = Optional.ofNullable(entry.getKey()).orElse("");
            String link = Optional.ofNullable(entry.getValue()).orElse("");

            TextField entityField = new TextField();
            entityField.setValue(entity);

            TextField linkField = new TextField();
            linkField.setValue(link);

            this.linksList.put(entityField, linkField);
        }
    }

    public String convertMapToJson(Map<String, String> elements) {

        ObjectMapper objectMapper = new ObjectMapper();
        String json = "";

        try {
            json = objectMapper.writeValueAsString(elements);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return json;
    }

    public Map<String, String> convertJsonToMap(String json) {
        ObjectMapper objectMapper = new ObjectMapper();

        HashMap<String, String> map = new HashMap<>();

        try {
            map = objectMapper.readValue(json, HashMap.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }
}
