package org.pamapam.backoffice.layout;

import com.google.common.collect.Sets;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.ContentLayout;
import org.jamgo.ui.layout.CustomLayout;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.pamapam.backoffice.layout.utils.PamapamComponentFactory;
import org.pamapam.model.*;
import org.pamapam.repository.EntityStatusRepository;
import org.pamapam.repository.PamapamRoleRepository;
import org.pamapam.repository.search.SocialEconomyEntitySearch;
import org.pamapam.services.PamapamUserService;
import org.pamapam.services.SocialEconomyEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.*;
import java.util.stream.Collectors;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DashboardLayout extends CustomLayout {
	private static final long serialVersionUID = 1L;

	@Autowired
	private JamgoComponentFactory componentFactory;
	@Autowired
	private JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	private EntityStatusRepository entityStatusRepository;
	@Autowired
	private SocialEconomyEntityService socialEconomyEntityService;
	@Autowired
	@Lazy
	private ContentLayout contentLayout;
	@Autowired
	private BackofficeApplicationDef backofficeApplicationDef;
	@Autowired
	private PamapamUserService userService;
	@Autowired
	private PamapamRoleRepository pamapamRoleRepository;

	private GridLayout mainLayout;

	private enum BoxStatus {
		//		INITIATIVE, PROPOSED, UNFINISHED, PENDING, PUBLISHED, UNPUBLISHED, EXTERNAL, EINATECA
		INITIATIVE, PROPOSED, UNFINISHED, PENDING, PUBLISHED, UNPUBLISHED, EXTERNAL
	}

	@Override
	public void initialize() {
		this.setSizeFull();
		this.setMargin(true);
		this.setSpacing(true);

		this.mainLayout = this.componentFactory.newGridLayout();
		this.mainLayout.setSizeFull();
		this.mainLayout.setMargin(true);
		this.mainLayout.setSpacing(true);

		this.addComponent(this.mainLayout);

		this.addEntityBoxes();
	}

	@Override
	public void attach() {
		super.attach();

		this.contentLayout.addSelectedTabChangeListener(listener -> {
			listener.getComponent();
			if (DashboardLayout.this.equals(listener.getTabSheet().getSelectedTab())) {
				this.mainLayout.removeAllComponents();
				this.addEntityBoxes();
			}
		});
	}

	private void addEntityBoxes() {
		final List<EntityStatus> entityStatuses = this.entityStatusRepository.findAll();
		final Map<BoxStatus, List<EntityStatus>> boxStatusesMap = new HashMap<>();
		entityStatuses.forEach(each -> {
			if (each.getEntityStatusType() != null) {
				switch (each.getEntityStatusType()) {
					case INITIATIVE:
						boxStatusesMap.computeIfAbsent(BoxStatus.INITIATIVE, emptyList -> new ArrayList<>()).add(each);
						break;
					case PROPOSED:
						boxStatusesMap.computeIfAbsent(BoxStatus.PROPOSED, emptyList -> new ArrayList<>()).add(each);
						break;
					case DRAFT:
					case INCOMPLETE:
						boxStatusesMap.computeIfAbsent(BoxStatus.UNFINISHED, emptyList -> new ArrayList<>()).add(each);
						break;
					case REVIEW_PENDING:
					case UPDATE_PENDING:
						boxStatusesMap.computeIfAbsent(BoxStatus.PENDING, emptyList -> new ArrayList<>()).add(each);
						break;
					case PUBLISHED:
						boxStatusesMap.computeIfAbsent(BoxStatus.PUBLISHED, emptyList -> new ArrayList<>()).add(each);
						break;
					case UNPUBLISHED:
						boxStatusesMap.computeIfAbsent(BoxStatus.UNPUBLISHED, emptyList -> new ArrayList<>()).add(each);
						break;
					case EXTERNAL:
						boxStatusesMap.computeIfAbsent(BoxStatus.EXTERNAL, emptyList -> new ArrayList<>()).add(each);
						break;
					default:
						break;
				}
			}
		});

		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN) || VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_COORDINATION)) {
			this.createAdminLayout(boxStatusesMap);
		} else if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_INITIATIVE)) {
			this.createInitiativeLayout(boxStatusesMap, this.userService.getCurrentUser());
		} else if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_EXTERNAL)) {
			this.createExternalLayout(boxStatusesMap, this.userService.getCurrentUser());
		} else if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_POL_COOPERATIU)) {
			this.createIntercoopLayout(boxStatusesMap, this.userService.getCurrentUser());
		} else {
			this.createUserLayout(boxStatusesMap, this.userService.getCurrentUser());
		}
	}

	private void createAdminLayout(final Map<BoxStatus, List<EntityStatus>> boxStatusesMap) {
		this.mainLayout.setColumns(2);
		this.mainLayout.setRows(4);

		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNFINISHED), null, "dashboard.unfinished"), 0, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PENDING), null, "dashboard.pending"), 1, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PUBLISHED), null, "dashboard.published"), 0, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNPUBLISHED), null, "dashboard.unpublished"), 1, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.INITIATIVE), null, "dashboard.initiative"), 0, 2);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PROPOSED), null, "dashboard.proposed"), 1, 2);
	}

	private void createInitiativeLayout(final Map<BoxStatus, List<EntityStatus>> boxStatusesMap, final PamapamUser user) {
		this.mainLayout.setColumns(1);
		this.mainLayout.setRows(1);

		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PUBLISHED), user, "dashboard.published", false), 0, 0);
	}

	private void createExternalLayout(final Map<BoxStatus, List<EntityStatus>> boxStatusesMap, final PamapamUser user) {
		this.mainLayout.setColumns(2);
		this.mainLayout.setRows(3);

		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.INITIATIVE), null, "dashboard.initiative"), 0, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.EXTERNAL), user, "dashboard.external"), 1, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNFINISHED), user, "dashboard.unfinished"), 0, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PENDING), user, "dashboard.pending"), 1, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PUBLISHED), user, "dashboard.published"), 0, 2);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNPUBLISHED), user, "dashboard.unpublished"), 1, 2);
	}

	private void createIntercoopLayout(final Map<BoxStatus, List<EntityStatus>> boxStatusesMap, final PamapamUser user) {
		this.mainLayout.setColumns(2);
		this.mainLayout.setRows(3);

		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.INITIATIVE), null, "dashboard.initiative"), 0, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PROPOSED), null, "dashboard.proposed"), 1, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNFINISHED), null, "dashboard.unfinished"), 0, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PENDING), null, "dashboard.pending"), 1, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PUBLISHED), null, "dashboard.published"), 0, 2);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNPUBLISHED), null, "dashboard.unpublished"), 1, 2);
	}

	private void createUserLayout(final Map<BoxStatus, List<EntityStatus>> boxStatusesMap, final PamapamUser user) {
		this.mainLayout.setColumns(2);
		this.mainLayout.setRows(3);

		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.INITIATIVE), null, "dashboard.initiative"), 0, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PROPOSED), null, "dashboard.proposed"), 1, 0);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNFINISHED), user, "dashboard.unfinished"), 0, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PENDING), user, "dashboard.pending"), 1, 1);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.PUBLISHED), user, "dashboard.published"), 0, 2);
		this.mainLayout.addComponent(this.createEntitiesBox(boxStatusesMap.get(BoxStatus.UNPUBLISHED), user, "dashboard.unpublished"), 1, 2);
	}

	private Component createEntitiesBox(final List<EntityStatus> statuses, final PamapamUser user, final String messageKey) {
		return this.createEntitiesBox(statuses, user, messageKey, true);
	}

	private Component createEntitiesBox(final List<EntityStatus> statuses, final PamapamUser user, final String messageKey, final boolean linksEnabled) {
		Page<SocialEconomyEntity> page = null;
		final SocialEconomyEntitySearch searchObject = new SocialEconomyEntitySearch();

		final PamapamUser currentUser = this.userService.getCurrentUser();
		final List<PamapamRole> pamapamRoles = this.pamapamRoleRepository.findByRoleIn(currentUser.getRoles());
		final Set<EntityScope> entityScopes = pamapamRoles.stream()
			.map(each -> each.getEntityScopes())
			.flatMap(each -> each.stream())
			.collect(Collectors.toSet());
		searchObject.setEntityScopes(entityScopes);

		if (statuses != null) {
			searchObject.setEntityStatuses(Sets.newHashSet(statuses));
			searchObject.setRegistryUser(user);
			page = this.socialEconomyEntityService.findTopEntities(statuses, user, entityScopes, 10);
		} else {
			page = new PageImpl<>(new ArrayList<>());
		}

		final VerticalLayout boxLayout = this.componentFactory.newVerticalLayout();
		boxLayout.setSizeFull();
		boxLayout.setMargin(new MarginInfo(true, false, false, false));
		boxLayout.setSpacing(true);
		final Button boxTitle = this.componentBuilderFactory.createButtonBuilder().setCaption(messageKey).build();
		boxTitle.setCaption(boxTitle.getCaption() + " (" + String.valueOf(page.getTotalElements()) + ")");
		boxTitle.addStyleName("link");
		boxTitle.addStyleName("dashboard-box-title");
		if (linksEnabled && (statuses != null)) {
			boxTitle.addClickListener(event -> this.doNavigateToEntities(event));
			boxTitle.setData(searchObject);
		} else {
			boxTitle.setEnabled(false);
		}
		boxLayout.addComponent(boxTitle);
		final Grid<SocialEconomyEntity> boxGrid = ((PamapamComponentFactory) this.componentFactory).createEntitiesGrid();
		boxGrid.setItems(page.getContent());
		boxLayout.addComponent(boxGrid);
		boxLayout.setExpandRatio(boxGrid, 1f);

		return boxLayout;
	}

	private void doNavigateToEntities(final ClickEvent event) {
		@SuppressWarnings("unchecked") final CrudLayoutDef<SocialEconomyEntity> layoutDef = (CrudLayoutDef<SocialEconomyEntity>) this.backofficeApplicationDef.getLayoutDef(SocialEconomyEntity.class);
		final Button boxButton = event.getButton();
		final SocialEconomyEntitySearch searchObject = (SocialEconomyEntitySearch) boxButton.getData();
		this.contentLayout.activateCrudTab(layoutDef, searchObject);
	}

}
