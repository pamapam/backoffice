package org.pamapam.backoffice.layout.search;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudDetailsPanel.Format;
import org.jamgo.ui.layout.crud.CrudSearchLayout;
import org.pamapam.model.Document;
import org.pamapam.repository.search.DocumentSearchSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DocumentSearchLayout extends CrudSearchLayout<Document, Document> {

	private static final long serialVersionUID = 1L;

	private TextField nameField;
	private ComboBox<String> mimeTypeCombo;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@PostConstruct
	private void init() {
		this.searchSpecification = this.applicationContext.getBean(DocumentSearchSpecification.class);
	}

	@Override
	protected CrudDetailsPanel createPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel(Format.Vertical);
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.nameField = this.componentFactory.newTextField("generic.entity.name");
		this.nameField.setWidth(100, Unit.PERCENTAGE);
		panel.addComponent(this.nameField);
//		this.binder.bind(this.nameField, BinaryResourceSearch::getFileName, BinaryResourceSearch::setFileName);

		this.mimeTypeCombo = this.componentFactory.newComboBox("binaryResource.mimeType");
		this.mimeTypeCombo.setWidth(100, Unit.PERCENTAGE);
//		mimeTypeCombo.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.mimeTypeCombo.setItems(Arrays.asList("application/pdf","application/word"));
		panel.addComponent(this.mimeTypeCombo);
//		this.binder.bind(this.mimeTypeCombo, BinaryResourceSearch::getMimeType, BinaryResourceSearch::setMimeType);

		return panel;
	}

	@Override
	protected Class<Document> getTargetObjectClass() {
		return Document.class;
	}

}
