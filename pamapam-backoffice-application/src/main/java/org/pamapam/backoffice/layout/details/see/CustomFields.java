package org.pamapam.backoffice.layout.details.see;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.pamapam.model.ModelAttributeDef;

public class CustomFields {

	private Map<ModelAttributeDef, Object> customFieldValues = new HashMap<>();

	public Map<ModelAttributeDef, Object> getCustomFieldValues() {
		return this.customFieldValues;
	}

	public void setCustomFieldValues(Map<ModelAttributeDef, Object> customFieldValues) {
		this.customFieldValues = customFieldValues;
	}

	public Object getValue(ModelAttributeDef attributeName) {
		return this.customFieldValues.get(attributeName);
	}

	public void setValue(ModelAttributeDef attributeName, Object value) {
		this.customFieldValues.put(attributeName, value);
	}

	public String getStringValue(ModelAttributeDef attributeName) {
		return (String) this.customFieldValues.get(attributeName);
	}

	public void setStringValue(ModelAttributeDef attributeName, String value) {
		this.customFieldValues.put(attributeName, value);
	}

	public Integer getIntegerValue(ModelAttributeDef attributeName) {
		return (Integer) this.customFieldValues.get(attributeName);
	}

	public void setIntegerValue(ModelAttributeDef attributeName, Integer value) {
		this.customFieldValues.put(attributeName, value);
	}

	public Long getLongValue(ModelAttributeDef attributeName) {
		return (Long) this.customFieldValues.get(attributeName);
	}

	public void setLongValue(ModelAttributeDef attributeName, Long value) {
		this.customFieldValues.put(attributeName, value);
	}

	public BigDecimal getBigDecimalValue(ModelAttributeDef attributeName) {
		return (BigDecimal) this.customFieldValues.get(attributeName);
	}

	public void setBigDecimalValue(ModelAttributeDef attributeName, BigDecimal value) {
		this.customFieldValues.put(attributeName, value);
	}

	public Boolean getBooleanValue(ModelAttributeDef attributeName) {
		return (Boolean) this.customFieldValues.get(attributeName);
	}

	public void setBooleanValue(ModelAttributeDef attributeName, Boolean value) {
		this.customFieldValues.put(attributeName, value);
	}

	public Date getDateValue(ModelAttributeDef attributeName) {
		return (Date) this.customFieldValues.get(attributeName);
	}

	public void setDateValue(ModelAttributeDef attributeName, Date value) {
		this.customFieldValues.put(attributeName, value);
	}

	public Map<String, Object> getValuesMap() {
		Map<String, Object> valuesMap = new HashMap<>();
		this.customFieldValues.forEach((key, value) -> valuesMap.put(key.getName(), value));
		return valuesMap;
	}
}