package org.pamapam.backoffice.layout.details.see;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.pamapam.backoffice.ui.PamapamIcons;
import org.pamapam.model.Criterion;
import org.pamapam.model.CriterionAnswer;
import org.pamapam.model.Questionaire;
import org.pamapam.repository.QuestionaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CriterionGroupLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected JamgoComponentFactory componentFactory;
	@Autowired
	protected LocalizedMessageService messageSource;

	private Label nameLabel;
	private Label questionLabel;
	private HorizontalLayout levelLabelsLayout;
	private CheckBox notAplicableField;
	private List<Layout> answerLayouts;
	private List<Layout> answerValueLayouts;
	private Map<CriterionAnswer, Label> answerValueLabels;
	private TextArea notesField;

	private Questionaire questionaire;

	@Autowired
	private QuestionaireRepository questionaireRepository;
	private Integer level;

	public void initialize(Criterion criterion) {
		this.setWidth(100, Unit.PERCENTAGE);
		this.setMargin(false);
		this.setSpacing(false);
		this.setCaption(String.valueOf(criterion.getId()));

		HorizontalLayout nameLayout = this.componentFactory.newHorizontalLayout();
		nameLayout.setSpacing(true);
		this.nameLabel = this.componentFactory.newLabel();
		this.nameLabel.setValue("<b><span style=\"text-decoration: underline;\">" + criterion.getName().getDefaultText() + "</span></b>");
		this.nameLabel.setContentMode(ContentMode.HTML);
		this.levelLabelsLayout = this.componentFactory.newHorizontalLayout();
		this.levelLabelsLayout.setSizeUndefined();
		this.levelLabelsLayout.setMargin(false);
		this.levelLabelsLayout.setSpacing(false);
		this.level = 0;
		nameLayout.addComponents(this.nameLabel, this.levelLabelsLayout);
		this.addComponent(nameLayout);

		this.questionLabel = this.componentFactory.newLabel();
		this.questionLabel.setValue("<b>" + criterion.getQuestion().getDefaultText() + "</b>");
		this.questionLabel.setWidth(100, Unit.PERCENTAGE);
		this.questionLabel.setContentMode(ContentMode.HTML);
		this.questionLabel.setDescription(criterion.getDescription().getDefaultText());
		this.addComponent(this.questionLabel);

		this.addComponent(this.componentFactory.newLabel());
		this.notAplicableField = this.componentFactory.newCheckBox("entityEvaluationCriterion.notApplicable");
		this.notAplicableField.addValueChangeListener(event -> this.doNotApplicableChanged(event));
		this.addComponent(this.notAplicableField);

		this.answerLayouts = new ArrayList<>();
		this.answerValueLayouts = new ArrayList<>();
		this.answerValueLabels = new HashMap<>();

		Questionaire economiaSocialSolidariaQuestionaire = this.questionaireRepository.findOne(1L); //ESS by default because it was there first
		List<CriterionAnswer> criteriaAnswer =(this.questionaire == null) ? criterion.getCriterionAnswers(economiaSocialSolidariaQuestionaire) : criterion.getCriterionAnswers(this.questionaire);
		for (CriterionAnswer eachAnswer : criteriaAnswer) {
			HorizontalLayout answerLayout = this.componentFactory.newHorizontalLayout();
			answerLayout.setWidth(100, Unit.PERCENTAGE);
			answerLayout.setMargin(false);
			answerLayout.setSpacing(false);
			Label answerValueLabel = this.componentFactory.newLabel();
			answerValueLabel.setIcon(PamapamIcons.EMPTY_POINT);
			answerValueLabel.setData(0);
			HorizontalLayout answerValueLayout = new HorizontalLayout();
			answerValueLayout.setMargin(false);
			answerValueLayout.setSpacing(false);
			answerValueLayout.addStyleName("j-answer-value");
			answerValueLayout.addComponent(answerValueLabel);
			answerValueLayout.addLayoutClickListener(event -> this.doAnswerValueSwitch(event));
			this.answerValueLabels.put(eachAnswer, answerValueLabel);
			String answerText = String.valueOf(eachAnswer.getAnswerOrder()) + ". " + eachAnswer.getAnswer().getDefaultText();
			Label answerLabel = this.componentFactory.newLabel();
			answerLabel.setValue(answerText);
			answerLabel.setWidth(100, Unit.PERCENTAGE);
			answerLayout.addComponents(answerValueLayout, answerLabel);
			answerLayout.setExpandRatio(answerLabel, 1);
			this.answerValueLayouts.add(answerValueLayout);
			this.answerLayouts.add(answerLayout);
			this.addComponent(answerLayout);
		}
		this.addComponent(this.componentFactory.newLabel());
		this.notesField = this.createTextArea("entityEvaluationCriterion.note");
		this.addComponent(this.notesField);
		this.addComponent(this.componentFactory.newLabel());
	}

	public void setQuestionaire(Questionaire questionaire) {
		this.questionaire = questionaire;
	}

	private void doNotApplicableChanged(ValueChangeEvent<Boolean> event) {
		if (event.getValue()) {
			this.resetAnswerValues();
			this.answerLayouts.forEach(each -> each.setEnabled(false));
			this.answerValueLayouts.forEach(each -> {
				each.removeStyleName("j-answer-value");
				each.addStyleName("j-answer-value-disabled");
			});
		} else {
			this.answerLayouts.forEach(each -> each.setEnabled(true));
			this.answerValueLayouts.forEach(each -> {
				each.removeStyleName("j-answer-value-disabled");
				each.addStyleName("j-answer-value");
			});
		}
	}

	private void doAnswerValueSwitch(LayoutClickEvent event) {
		Label answerValueLabel = (Label) ((HorizontalLayout) event.getComponent()).getComponent(0);
		Integer currentValue = (Integer) answerValueLabel.getData();
		switch (currentValue) {
			case 0:
				answerValueLabel.setData(100);
				answerValueLabel.setIcon(PamapamIcons.FULL_POINT);
				break;
			case 50:
				answerValueLabel.setData(0);
				answerValueLabel.setIcon(PamapamIcons.EMPTY_POINT);
				break;
			case 100:
				answerValueLabel.setData(50);
				answerValueLabel.setIcon(PamapamIcons.HALF_POINT);
				break;
			default:
				break;
		}
		this.level = this.getCalculatedLevel();
		this.updateLevelLabels();
	}

	private Integer getCalculatedLevel() {
		return Math.min(this.answerValueLabels.values().stream().collect(Collectors.summingInt(each -> (int) each.getData())), Criterion.MAX_VALUE);
	}

	public void setAnswerValue(CriterionAnswer criterionAnswer, Integer answerValue) {
		Label answerValueLabel = this.answerValueLabels.get(criterionAnswer);

		if(answerValueLabel == null) {
			return;
		}

		switch (answerValue) {
			case 0:
				answerValueLabel.setData(0);
				answerValueLabel.setIcon(PamapamIcons.EMPTY_POINT);
				break;
			case 50:
				answerValueLabel.setData(50);
				answerValueLabel.setIcon(PamapamIcons.HALF_POINT);
				if (this.level < Criterion.MAX_VALUE) {
					this.level += 50;
				}
				break;
			case 100:
				answerValueLabel.setData(100);
				answerValueLabel.setIcon(PamapamIcons.FULL_POINT);
				if (this.level < Criterion.MAX_VALUE) {
					this.level += 100;
				}
				break;
			default:
				break;
		}
	}

	public void resetAnswerValues() {
		this.level = 0;
		this.answerValueLabels.values().stream().forEach(each -> {
			each.setData(0);
			each.setIcon(PamapamIcons.EMPTY_POINT);
		});
		this.levelLabelsLayout.removeAllComponents();
	}

	public void updateLevelLabels() {
		this.levelLabelsLayout.removeAllComponents();

		if ((this.level > 0) && (this.getCalculatedLevel() == 0)) {
			this.levelLabelsLayout.removeStyleName("j-level-labels");
			this.levelLabelsLayout.addStyleName("j-level-labels-disabled");
		} else {
			this.levelLabelsLayout.removeStyleName("j-level-labels-disabled");
			this.levelLabelsLayout.addStyleName("j-level-labels");
		}

		int fullPoints = this.level / 100;
		int halfPoints = this.level % 100;
		IntStream.range(0, fullPoints).forEach(i -> {
			Label fullPointLabel = new Label();
			fullPointLabel.setIcon(PamapamIcons.FULL_POINT);
			this.levelLabelsLayout.addComponent(fullPointLabel);
		});
		if (halfPoints > 0) {
			Label halfPointLabel = new Label();
			halfPointLabel.setIcon(PamapamIcons.HALF_POINT);
			this.levelLabelsLayout.addComponent(halfPointLabel);
		}
	}

	private TextArea createTextArea(String captionId) {
		TextArea field = this.componentFactory.newTextArea(captionId);
		field.setSizeFull();
		field.setWordWrap(true);
		field.setWidth(100, Unit.PERCENTAGE);
		return field;
	}

	public Map<CriterionAnswer, Label> getAnswerValueLabels() {
		return this.answerValueLabels;
	}

	public TextArea getNotesField() {
		return this.notesField;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public CheckBox getNotAplicableField() {
		return this.notAplicableField;
	}

	public void setNotAplicableField(CheckBox notAplicableField) {
		this.notAplicableField = notAplicableField;
	}

}
