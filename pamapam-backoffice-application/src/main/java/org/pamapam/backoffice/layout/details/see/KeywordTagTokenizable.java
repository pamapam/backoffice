package org.pamapam.backoffice.layout.details.see;

import java.util.Objects;

import org.pamapam.model.KeywordTag;

import com.explicatis.ext_token_field.Tokenizable;

public class KeywordTagTokenizable implements Tokenizable {

	private KeywordTag keywordTag;

	public KeywordTagTokenizable(final KeywordTag keywordTag) {
		this.keywordTag = keywordTag;
	}

	public KeywordTag getKeywordTag() {
		return this.keywordTag;
	}

	public void setKeywordTag(final KeywordTag keywordTag) {
		this.keywordTag = keywordTag;
	}

	@Override
	public String getStringValue() {
		return this.keywordTag.getTag();
	}

	@Override
	public long getIdentifier() {
		if (this.keywordTag.getId() == null) {
			return this.hashCode();
		} else {
			return this.keywordTag.getId();
		}
	}

	@Override
	public boolean equals(final Object obj) {

		if (obj == this) {
			return true;
		}
		if (!(obj instanceof KeywordTagTokenizable)) {
			return false;
		}
		final KeywordTagTokenizable other = (KeywordTagTokenizable) obj;
		return Objects.equals(this.keywordTag, other.keywordTag);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.keywordTag);
	}

}
