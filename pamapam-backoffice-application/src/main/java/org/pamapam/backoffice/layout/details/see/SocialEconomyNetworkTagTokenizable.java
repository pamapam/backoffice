package org.pamapam.backoffice.layout.details.see;

import java.util.Objects;

import org.pamapam.model.SocialEconomyNetworkTag;

import com.explicatis.ext_token_field.Tokenizable;

public class SocialEconomyNetworkTagTokenizable implements Tokenizable {

	private SocialEconomyNetworkTag socialEconomyNetworkTag;

	public SocialEconomyNetworkTagTokenizable(SocialEconomyNetworkTag socialEconomyNetworkTag) {
		this.socialEconomyNetworkTag = socialEconomyNetworkTag;
	}

	public SocialEconomyNetworkTag getSocialEconomyNetworkTag() {
		return this.socialEconomyNetworkTag;
	}

	public void setSocialEconomyNetworkTag(SocialEconomyNetworkTag socialEconomyNetworkTag) {
		this.socialEconomyNetworkTag = socialEconomyNetworkTag;
	}

	@Override
	public String getStringValue() {
		return this.socialEconomyNetworkTag.getTag();
	}

	@Override
	public long getIdentifier() {
		if (this.socialEconomyNetworkTag.getId() == null) {
			return this.hashCode();
		} else {
			return this.socialEconomyNetworkTag.getId();
		}
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == this) {
			return true;
		}
		if (!(obj instanceof SocialEconomyNetworkTagTokenizable)) {
			return false;
		}
		SocialEconomyNetworkTagTokenizable other = (SocialEconomyNetworkTagTokenizable) obj;
		return Objects.equals(this.socialEconomyNetworkTag, other.socialEconomyNetworkTag);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.socialEconomyNetworkTag);
	}

}
