package org.pamapam.backoffice.layout;

import java.util.List;

import org.jamgo.model.entity.District;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.model.entity.Neighborhood;
import org.jamgo.model.entity.Province;
import org.jamgo.model.entity.Region;
import org.jamgo.model.entity.RoleImpl;
import org.jamgo.model.entity.Town;
import org.jamgo.ui.layout.menu.MenuGroup;
import org.jamgo.ui.layout.menu.MenuItem;
import org.jamgo.ui.layout.menu.MenuLayout;
import org.pamapam.model.Community;
import org.pamapam.model.Criterion;
import org.pamapam.model.Document;
import org.pamapam.model.EmbeddedMapConfig;
import org.pamapam.model.EntityScope;
import org.pamapam.model.EntityStatus;
import org.pamapam.model.ExternalFilterTag;
import org.pamapam.model.FescEntityStatus;
import org.pamapam.model.FescRegistrationFee;
import org.pamapam.model.FescRegistrationModality;
import org.pamapam.model.IdeologicalIdentification;
import org.pamapam.model.LegalForm;
import org.pamapam.model.MailTemplate;
import org.pamapam.model.ModelDef;
import org.pamapam.model.NewsletterRegistration;
import org.pamapam.model.NotificationConfig;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.PersonRole;
import org.pamapam.model.SearchInfo;
import org.pamapam.model.Sector;
import org.pamapam.model.SectorCcae;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.model.SocialEconomyNetwork;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.server.VaadinService;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PamapamMenuLayout extends MenuLayout {

	private static final long serialVersionUID = 1L;

	@Override
	public MenuGroup getRootMenuGroup() {
		final MenuGroup menuGroup = new MenuGroup();

		menuGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(MyProfileLayout.class)));
		menuGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(DashboardLayout.class)));
		menuGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(SocialEconomyEntity.class)));
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_XINXETA)
			|| VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_XINXETA_EINATECA)) {
			menuGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Document.class)));
		}
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN) || VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_COORDINATION)) {
			menuGroup.addMenuItem(this.getConfigMenuGroup());
		}
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			menuGroup.addMenuItem(this.getTerritoryMenuGroup());
			menuGroup.addMenuItem(this.getAuxiliarTablesMenuGroup());
		}
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			menuGroup.addMenuItem(this.getFescMenuGroup());
		}

		return menuGroup;
	}

	private MenuGroup getConfigMenuGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setCaption(this.messageSource.getMessage("title.config.tables"));
		final List<MenuItem> crudMenuItems = menuGroup.getMenuItems();
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN) || VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_COORDINATION)) {
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(PamapamUser.class)));
		}
		if (VaadinService.getCurrentRequest().isUserInRole(PamapamUser.ROLE_ADMIN)) {
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(RoleImpl.class)));
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Language.class)));
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(NotificationConfig.class)));
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(MailTemplate.class)));
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(LocalizedMessage.class)));
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(EmbeddedMapConfig.class)));
			crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(ModelDef.class)));
		}
		return menuGroup;
	}

	private MenuGroup getTerritoryMenuGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setCaption(this.messageSource.getMessage("title.territory.tables"));
		final List<MenuItem> crudMenuItems = menuGroup.getMenuItems();
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Province.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Region.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Town.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(District.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Neighborhood.class)));
		return menuGroup;
	}

	private MenuGroup getAuxiliarTablesMenuGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setCaption(this.messageSource.getMessage("title.auxiliar.tables"));
		final List<MenuItem> crudMenuItems = menuGroup.getMenuItems();
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Sector.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(SectorCcae.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(LegalForm.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Community.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(EntityScope.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(PersonRole.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(SocialEconomyNetwork.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(IdeologicalIdentification.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Criterion.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(EntityStatus.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(NewsletterRegistration.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(ExternalFilterTag.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(SearchInfo.class)));
		return menuGroup;
	}

	private MenuItem getFescMenuGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setCaption("FESC");
		final List<MenuItem> crudMenuItems = menuGroup.getMenuItems();
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(FescEntityStatus.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(FescRegistrationModality.class)));
		crudMenuItems.add(new MenuItem(this.backofficeApplicationDef.getLayoutDef(FescRegistrationFee.class)));
		return menuGroup;
	}

}
