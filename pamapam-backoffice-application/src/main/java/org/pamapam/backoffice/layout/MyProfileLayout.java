package org.pamapam.backoffice.layout;

import java.util.Optional;

import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.CustomLayout;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.CrudTableLayout;
import org.jamgo.vaadin.ui.JmgNotification;
import org.pamapam.backoffice.PamapamBackofficeApplicationDef;
import org.pamapam.backoffice.PamapamSession;
import org.pamapam.backoffice.layout.details.PamapamUserDetailsLayout;
import org.pamapam.model.PamapamUser;
import org.pamapam.model.SocialEconomyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MyProfileLayout extends CustomLayout{

	private static final long serialVersionUID = 1L;

	@Autowired
	private PamapamUserDetailsLayout userDetailsLayout;
	@Autowired
	private PamapamSession session;
	@Autowired
	private CrudServices crudServices;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected AuthenticationManager authenticationManager;
	@Autowired
	protected PamapamContentLayout pamapamContentLayout;
	@Autowired
	protected PamapamBackofficeApplicationDef backofficeApplicationDef;
	
	private String previousUsername;
	
	@Override
	public void initialize() {
		this.setSizeFull();
		
		this.previousUsername = this.session.getCurrentUser().getUsername();
		
		CrudLayoutDef<SocialEconomyEntity> userLayoutDef = (CrudLayoutDef<SocialEconomyEntity>) this.backofficeApplicationDef.getLayoutDef(PamapamUser.class);
		this.userDetailsLayout.initialize(userLayoutDef);
		this.userDetailsLayout.updateFields(this.session.getCurrentUser());
		this.userDetailsLayout.setOkHandler(obj -> this.save(obj));
		this.userDetailsLayout.getCancelButton().setVisible(false);
		this.addComponent(this.userDetailsLayout);
	}
	
	/**
	 * This method is an adaptation of saveDetails() in {@link CrudTableLayout} since {@link PamapamUserDetailsLayout}
	 * is used here to show user's profile.
	 * 
	 * @param detailsLayout
	 */
	protected void save(CrudDetailsLayout<PamapamUser> detailsLayout) {
		PamapamUser savedItem = null;
		try {
			savedItem = this.crudServices.save(detailsLayout.getTargetObject());
		} catch (CrudException e) {
			Notification.show(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName(), Type.ERROR_MESSAGE);
		}
		detailsLayout.updateFields(savedItem);
		
		if (!this.previousUsername.equals(savedItem.getUsername())) {
			JmgNotification.show(this.messageSource.getMessage("user.warning.logout"), Type.WARNING_MESSAGE);
		}
	}
	
	/**
	 * This method is an adaptation of attemptLostFocusDetails() in {@link CrudTableLayout} since {@link PamapamUserDetailsLayout}
	 * is used here to show user's profile.
	 * 
	 * @param detailsLayout
	 * @param action
	 */
	protected void attemptLostFocusDetails(CrudDetailsLayout<PamapamUser> detailsLayout, Runnable action) {
		if ((detailsLayout != null) && detailsLayout.hasChanges()) {
			MessageBox.createQuestion()
				.withCaption(this.messageSource.getMessage("dialog.close"))
				.withMessage(this.messageSource.getMessage("dialog.areyousure.close"))
				.withYesButton(action, ButtonOption.caption(this.messageSource.getMessage("dialog.yes")))
				.withNoButton(ButtonOption.caption(this.messageSource.getMessage("dialog.no")))
				.open();
		} else {
			action.run();
		}
	}
}
