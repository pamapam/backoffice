package org.pamapam.backoffice.layout;

import com.explicatis.ext_token_field.ExtTokenField;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;
import com.vaadin.data.Binder;
import com.vaadin.data.Binder.BindingBuilder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.ValidationException;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.server.Setter;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.apache.commons.collections.CollectionUtils;
import org.apache.tika.Tika;
import org.jamgo.model.entity.*;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.jamgo.vaadin.ui.JmgLocalizedField.DisplayMode;
import org.jamgo.vaadin.ui.JmgNotification;
import org.pamapam.backoffice.layout.component.PamapamLocalizedTextArea;
import org.pamapam.backoffice.layout.component.PamapamLocalizedTextAreaBuilder;
import org.pamapam.backoffice.layout.details.see.EntityTokens;
import org.pamapam.backoffice.layout.details.see.KeywordTagTokenizable;
import org.pamapam.backoffice.layout.details.see.ProductTagTokenizable;
import org.pamapam.backoffice.layout.utils.PamapamComponentFactory;
import org.pamapam.backoffice.service.impl.LWikimediaMapsLayer;
import org.pamapam.model.*;
import org.pamapam.model.enums.FescTimeSlot;
import org.pamapam.repository.LegalFormRepository;
import org.pamapam.repository.SectorRepository;
import org.pamapam.repository.SocialEconomyNetworkRepository;
import org.pamapam.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LMarker;
import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.addon.leaflet.shared.Point;
import server.droporchoose.UploadComponent;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FescRegistrationLayout extends FescLayout {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(FescRegistrationLayout.class);

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected JamgoComponentFactory componentFactory;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected ValidationService validationService;
	@Autowired
	private SectorRepository sectorRepository;
	@Autowired
	private LegalFormRepository legalFormRepository;
	@Autowired
	private SocialEconomyNetworkRepository socialEconomyNetworkRepository;
	@Autowired
	private PamapamUserService pamapamUserService;
	@Autowired
	private ProductTagService productTagService;
	@Autowired
	private KeywordTagService keywordTagService;
	@Autowired
	private FescRegistrationModalityService modalityService;
	@Autowired
	private FescRegistrationFeeService feeService;
	@Autowired
	private EntityScopeService entityScopeService;

	protected Binder<SocialEconomyEntity> binder;
	private SocialEconomyEntity targetObject;
	private boolean publicRegistration = false;

	private EntityTokens entityTokens;
	private Binder<EntityTokens> entityTokensBinder;
	private List<ProductTagTokenizable> productTagTokenizables;
	private List<KeywordTagTokenizable> keywordTagTokenizables;

	private LinkedHashMap<String, Boolean> xesBalanceOptions;
	private LinkedHashMap<String, Boolean> publicAdministrationOptions;
	private List<FescRegistrationModality> modalityOptions;
	private List<FescTimeSlot> fescTimeSlots;
	private LinkedHashMap<String, Integer> invoiceFeeOptions;

	private Button okButton;
	private Consumer<FescRegistrationLayout> okHandler;

	private TextField contactPersonField;
	private TextField emailFescField;
	private TextField nameField;
	private PamapamLocalizedTextArea descriptionField;
	private ComboBox<Sector> mainSectorField;
	private ComboBox<Sector> mainSubSectorField;
	private ComboBox<Sector> fescSectorField;
	private TextField addressField;
	private TextField postalCodeField;
	private ComboBox<Province> provinceField;
	private ComboBox<Region> regionField;
	private ComboBox<Town> townField;
	private ComboBox<District> districtField;
	private ComboBox<Neighborhood> neighborhoodField;
	private LMap map = new LMap();
	private final LTileLayer mediawikiMapsTiles = new LWikimediaMapsLayer();
	private org.springframework.data.geo.Point point;
	private LMarker currentMarker;
	private TwinColSelect<SocialEconomyNetwork> socialEconomyNetworksField;
	private RadioButtonGroup<String> xesBalanceField;
	private RadioButtonGroup<String> publicAdministrationField;

	private Image pictureField;
	private UploadComponent pictureUploadField;
	private Path uploadedPicturePath;
	private ProgressBar pictureProgressBar;
	private TextField emailField;
	private TextField phoneField;
	private TextField webField;
	private TextField twitterField;
	private TextField facebookField;
	private TextField instagramField;
	private TextField pinterestField;
	private TextField quitterField;
	private ExtTokenField productsField;
	private ExtTokenField keywordsField;

	private TextField invoiceNameField;
	private ComboBox<LegalForm> legalFormField;
	private TextField nifField;
	private TextField invoiceEmailField;
	private TextField invoiceAddressField;
	private TextField invoicePostalCodeField;
	private ComboBox<FescRegistrationModality> invoiceModalityField;
	private TextField invoiceFellowNameField;
	private RadioButtonGroup<String> invoiceFeeOptionsField;
	private TextField invoiceFeeField;
	private TextField invoiceAccountField;
	private ComboBox<FescTimeSlot> timeSlotField;

	@Value("${fesc.registration.legalAdviceUrl}")
	private String legalAdviceUrl;

	@Value("${fesc.registration.participationPolicyUrl}")
	private String participationPolicyUrl;

	@Value("${fesc.registration.pamapamNewsletterUrl}")
	private String pamapamNewsletterUrl;

	public void initialize() {
		this.setStyleName("fesc-registration");
		this.binder = this.componentFactory.newBinder(SocialEconomyEntity.class);
		this.productTagTokenizables = new ArrayList<>();
		this.keywordTagTokenizables = new ArrayList<>();

		this.addHeader();
		this.addForm();
	}

	private void addForm() {
		this.initializeEntityTokens();
		this.initializeProductTagTokenizables();
		this.initializeKeywordTagTokenizables();
		this.initializeXesBalanceOptions();
		this.initializePublicAdministrationOptions();
		this.initializeModalityOptions();
		this.initializeFescTimeSlots();
		this.addFields();
		this.addPrivacyPolicyFields();
		this.addButtons();
	}

	private void initializeEntityTokens() {
		this.entityTokens = new EntityTokens();
		this.entityTokensBinder = new Binder<>(EntityTokens.class);
		this.entityTokensBinder.setBean(this.entityTokens);
	}

	private void initializeProductTagTokenizables() {
		final List<ProductTag> productTags = this.productTagService.findAll();
		Collections.sort(productTags, TextTag.TEXT_ORDER);
		for (final ProductTag eachProductTag : productTags) {
			final ProductTagTokenizable productTagTokenizable = new ProductTagTokenizable(eachProductTag);
			if (!this.productTagTokenizables.contains(productTagTokenizable)) {
				this.productTagTokenizables.add(productTagTokenizable);
			}
		}
	}

	private void initializeKeywordTagTokenizables() {
		final List<KeywordTag> keywordTags = this.keywordTagService.findAll();
		Collections.sort(keywordTags, TextTag.TEXT_ORDER);
		for (final KeywordTag eachKeywordTag : keywordTags) {
			final KeywordTagTokenizable keywordTagTokenizable = new KeywordTagTokenizable(eachKeywordTag);
			if (!this.keywordTagTokenizables.contains(keywordTagTokenizable)) {
				this.keywordTagTokenizables.add(keywordTagTokenizable);
			}
		}
	}

	private void initializeXesBalanceOptions() {
		this.xesBalanceOptions = Maps.newLinkedHashMap();
		this.xesBalanceOptions.put(this.messageSource.getMessage("socialEconomyEntity.xesBalance.na"), null);
		this.xesBalanceOptions.put(this.messageSource.getMessage("socialEconomyEntity.xesBalance.no"), false);
		this.xesBalanceOptions.put(this.messageSource.getMessage("socialEconomyEntity.xesBalance.yes"), true);
	}

	private void initializePublicAdministrationOptions() {
		this.publicAdministrationOptions = Maps.newLinkedHashMap();
		this.publicAdministrationOptions.put(this.messageSource.getMessage("fesc.registration.basic.publicAdministration.no"), false);
		this.publicAdministrationOptions.put(this.messageSource.getMessage("fesc.registration.basic.publicAdministration.yes"), true);
	}

	private void initializeModalityOptions() {
		this.modalityOptions = this.modalityService.findAll();
		this.invoiceFeeOptions = Maps.newLinkedHashMap();
	}

	private void initializeFescTimeSlots() {
		this.fescTimeSlots = Arrays.asList(FescTimeSlot.values());
	}

	private void addFields() {
		this.addContactDataFields();
		this.addEntityBasicDataFields();
		this.addCommunicationDataFields();
		this.addInvoiceDataFields();
	}

	private void addContactDataFields() {
		final Label title = this.createTitle("fesc.registration.contact");
		this.addComponent(title);
		this.addContactPersonField();
		this.addEmailFescField();
	}

	private void addContactPersonField() {
		this.contactPersonField = this.createTextField("fesc.registration.contact.contactPerson", this.binder, SocialEconomyEntity::getContactPerson, SocialEconomyEntity::setContactPerson, true);
		this.addComponent(this.contactPersonField);
	}

	private void addEmailFescField() {
		this.emailFescField = this.createEmailField("fesc.registration.contact.email", this.binder, SocialEconomyEntity::getFescEmail, SocialEconomyEntity::setFescEmail, true);
		this.addComponent(this.emailFescField);
	}

	private void addEntityBasicDataFields() {
		final Label title = this.createTitle("fesc.registration.basic");
		this.addComponent(title);
		this.addNameField();
		this.addDescriptionField();
		this.addMainSectorField();
		this.addTerritoryFields();
		this.addAddressField();
		this.addPostalCodeField();
		this.addMapField();
		this.addSocialEconomyNetworksField();
		this.addXesBalanceField();
		this.addPublicAdministrationField();
	}

	private void addNameField() {
		this.nameField = this.createTextField("fesc.registration.basic.name", this.binder, SocialEconomyEntity::getName, SocialEconomyEntity::setName, true);
		this.addComponent(this.nameField);
	}

	private void addDescriptionField() {
		this.descriptionField = (PamapamLocalizedTextArea) this.applicationContext.getBean(PamapamLocalizedTextAreaBuilder.class)
			.setCaption("fesc.registration.basic.description")
			.setWidth(100, Unit.PERCENTAGE)
			.setDisplayMode(DisplayMode.ONE_LANGUAGE)
			.build();
		this.descriptionField.setMaxLength(2048);
		this.binder.forField(this.descriptionField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(SocialEconomyEntity::getDescription, SocialEconomyEntity::setDescription);
		this.addComponent(this.descriptionField);
	}

	private void addMainSectorField() {
		List<Sector> allSectors = this.sectorRepository.findAll();
		Collections.sort(allSectors, Sector.NAME_HIERARCHICAL_ORDER);
		allSectors = allSectors.stream()
			.filter(sector -> sector.getParent() != null)
			.collect(Collectors.toList());

		final ListDataProvider<Sector> subSectorDataProvider = new ListDataProvider<>(allSectors);
		subSectorDataProvider.addFilter(sector -> !this.mainSectorField.isEmpty() && this.mainSectorField.getValue().equals(sector.getParent()));

		this.mainSectorField = this.componentFactory.newComboBox("fesc.registration.basic.mainSector");
		this.mainSectorField.setWidth(50, Unit.PERCENTAGE);
		this.mainSectorField.setItemCaptionGenerator(item -> item.getFullName());
		this.mainSectorField.setItems(allSectors.stream().filter(sector -> !Optional.ofNullable(sector.getParent()).isPresent()));
		this.mainSectorField.addValueChangeListener(listener -> subSectorDataProvider.refreshAll());
		this.binder.forField(this.mainSectorField)
			.asRequired(this.messageSource.getMessage("validation.emptySelection"))
			.bind((entity) -> {
				if ((entity == null) || (entity.getMainSector() == null)) {
					return null;
				}
				if (entity.getMainSector().getParent() == null) {
					return entity.getMainSector();
				} else {
					return entity.getMainSector().getParent();
				}
			}, (entity, sector) -> {
				if (this.mainSubSectorField.isEmpty()) {
					entity.setMainSector(sector);
				} else {
					entity.setMainSector(this.mainSubSectorField.getValue());
				}
			});
		this.addComponent(this.mainSectorField);

		this.mainSubSectorField = this.componentFactory.newComboBox("fesc.registration.basic.mainSubSector");
		this.mainSubSectorField.setWidth(50, Unit.PERCENTAGE);
		this.mainSubSectorField.setItemCaptionGenerator(item -> item.getFullName());
		this.mainSubSectorField.setDataProvider(subSectorDataProvider);
		this.binder.forField(this.mainSubSectorField)
			.bind((entity) -> {
				if ((entity == null) || (entity.getMainSector() == null)) {
					return null;
				}
				if (entity.getMainSector().getParent() != null) {
					return entity.getMainSector();
				}
				return null;
			}, (entity, sector) -> {
				if (sector != null) {
					entity.setMainSector(sector);
				}
			});
		this.addComponent(this.mainSubSectorField);
	}

	private void addFescSectorField() {
		List<Sector> allSectors = this.sectorRepository.findAll();
		Collections.sort(allSectors, Sector.NAME_HIERARCHICAL_ORDER);
		allSectors = allSectors.stream()
			.filter(sector -> sector.getParent() != null)
			.collect(Collectors.toList());

		this.fescSectorField = this.componentFactory.newComboBox("fesc.registration.basic.fescSector");
		this.fescSectorField.setWidth(50, Unit.PERCENTAGE);
		this.fescSectorField.setItemCaptionGenerator(item -> item.getFullName());
		this.fescSectorField.setItems(allSectors.stream().filter(sector -> !Optional.ofNullable(sector.getParent()).isPresent()));
		this.binder.forField(this.fescSectorField)
			.asRequired(this.messageSource.getMessage("validation.emptySelection"))
			.bind((entity) -> {
				if ((entity == null) || (entity.getFescMainSector() == null)) {
					return null;
				}
				if (entity.getFescMainSector().getParent() == null) {
					return entity.getFescMainSector();
				} else {
					return entity.getFescMainSector().getParent();
				}
			}, (entity, sector) -> {
				entity.setFescMainSector(sector);
			});
		this.addComponent(this.fescSectorField);
	}

	private void addTerritoryFields() {
		this.createNeighborhoodField();
		this.createDistrictField();
		this.createTownField();
		this.createRegionField();
		this.createProvinceField();

		this.addComponents(this.provinceField, this.regionField, this.townField, this.districtField, this.neighborhoodField);
	}

	private void createNeighborhoodField() {
		this.neighborhoodField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Neighborhood.class,
			null,
			"fesc.registration.basic.neighborhood",
			(ValueProvider<Neighborhood, Long>) item -> item.getId(),
			null);
		this.binder.bind(this.neighborhoodField, SocialEconomyEntity::getNeighborhood, SocialEconomyEntity::setNeighborhood);
	}

	private void createDistrictField() {
		this.districtField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			District.class,
			this.neighborhoodField,
			"fesc.registration.basic.district",
			(ValueProvider<District, Long>) item -> item.getId(),
			(ValueProvider<Neighborhood, District>) neighborhood -> neighborhood.getDistrict());
		this.binder.bind(this.districtField, SocialEconomyEntity::getDistrict, SocialEconomyEntity::setDistrict);
	}

	private void createTownField() {
		this.townField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Town.class,
			this.districtField,
			"fesc.registration.basic.town",
			(ValueProvider<Town, Long>) item -> item.getId(),
			(ValueProvider<District, Town>) district -> district.getTown());
		this.binder.forField(this.townField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(SocialEconomyEntity::getTown, SocialEconomyEntity::setTown);
	}

	private void createRegionField() {
		this.regionField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Region.class,
			this.townField,
			"fesc.registration.basic.region",
			(ValueProvider<Region, Long>) item -> item.getId(),
			(ValueProvider<Town, Region>) town -> town.getRegion());
		this.binder.forField(this.regionField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(SocialEconomyEntity::getRegion, SocialEconomyEntity::setRegion);
	}

	private void createProvinceField() {
		this.provinceField = ((PamapamComponentFactory) this.componentFactory).createTerritoryComboBox(
			Province.class,
			this.regionField,
			"fesc.registration.basic.province",
			null,
			(ValueProvider<Region, Province>) region -> region.getProvince());
		this.binder.forField(this.provinceField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(SocialEconomyEntity::getProvince, SocialEconomyEntity::setProvince);
	}

	private void addAddressField() {
		this.addressField = this.createTextField("fesc.registration.basic.address", this.binder, SocialEconomyEntity::getAddress, SocialEconomyEntity::setAddress, false);
		this.addressField.setValueChangeMode(ValueChangeMode.BLUR);
		this.addComponent(this.addressField);
	}

	private void addPostalCodeField() {
		this.postalCodeField = this.createTextField("fesc.registration.basic.postalCode", this.binder, SocialEconomyEntity::getPostalCode, SocialEconomyEntity::setPostalCode, false);
		this.addComponent(this.postalCodeField);
	}

	private void addMapField() {
		this.map = new LMap();
		this.map.setCaption(this.messageSource.getMessage("fesc.registration.basic.geoLocation") + " " + this.messageSource.getMessage("fesc.registration.basic.geoLocation.description"));
		this.map.setHeight("440px");
		this.mediawikiMapsTiles.setAttributionString("© Mediawiki Maps Contributors");
		this.map.addBaseLayer(this.mediawikiMapsTiles, "Mediawiki Maps");
		this.map.setScrollWheelZoomEnabled(false);
		this.map.addClickListener(event -> {
			// save the point to be used by context menu listener
			if (event.getMouseEvent().isCtrlKey()) {
				final Point newPoint = event.getPoint();
				this.doCleanMap();
				this.updateMap(newPoint.getLat(), newPoint.getLon());
			}
		});
		this.centerEmptyMap();
		this.addComponent(this.map);
	}

	private void doCleanMap() {
		this.map.removeAllComponents();
		this.map.addBaseLayer(this.mediawikiMapsTiles, "Mediawiki Maps");
		this.point = null;
	}

	private void centerEmptyMap() {
		this.map.setCenter(41.3903, 2.1941);
		this.map.setZoomLevel(12);
	}

	private void updateMap(final Double latitude, final Double longitude) {
		if (this.currentMarker != null) {
			this.map.removeComponent(this.currentMarker);
		}
		if ((latitude != null) && (longitude != null)) {
			this.point = new org.springframework.data.geo.Point(longitude, latitude);
			this.currentMarker = new LMarker(latitude, longitude);
			this.map.addComponent(this.currentMarker);
			this.map.flyTo(new Point(latitude, longitude), 15D);
		} else {
			this.point = null;
			this.centerEmptyMap();
		}
	}

	private void addSocialEconomyNetworksField() {
		final List<SocialEconomyNetwork> allSocialEconomyNetworks = this.socialEconomyNetworkRepository.findAll();
		Collections.sort(allSocialEconomyNetworks, SocialEconomyNetwork.DEFAULT_ORDER);
		this.socialEconomyNetworksField = this.componentFactory.newTwinColSelect("fesc.registration.basic.socialEconomyNetworks");
		this.socialEconomyNetworksField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.socialEconomyNetworksField.setWidth(100, Unit.PERCENTAGE);
		this.socialEconomyNetworksField.addStyleName("j-select-twincol");
		this.socialEconomyNetworksField.setItems(allSocialEconomyNetworks);

		this.binder.bind(this.socialEconomyNetworksField, SocialEconomyEntity::getSocialEconomyNetworks, SocialEconomyEntity::setSocialEconomyNetworks);
		this.addComponent(this.socialEconomyNetworksField);
	}

	private void addXesBalanceField() {
		this.xesBalanceField = this.componentFactory.newRadioButtonGroup("fesc.registration.basic.xesBalance");
		this.xesBalanceField.setItems(this.xesBalanceOptions.keySet());
		this.addComponent(this.xesBalanceField);
	}

	private void addPublicAdministrationField() {
		this.publicAdministrationField = this.componentFactory.newRadioButtonGroup("fesc.registration.basic.publicAdministration");
		this.publicAdministrationField.setItems(this.publicAdministrationOptions.keySet());
		this.addComponent(this.publicAdministrationField);
	}

	private void addCommunicationDataFields() {
		final Label title = this.createTitle("fesc.registration.communication");
		this.addComponent(title);

		this.addPictureLayout();
		this.addEmailField();
		this.addPhoneField();
		this.addWebField();
		this.addTwitterField();
		this.addFacebookField();
		this.addInstagramField();
		this.addPinterestField();
		this.addQuitterField();
		this.addProductsField();
		this.addKeywordsField();
		// TODO Add photos field (fotos i descripció productes)
	}

	private void addPictureLayout() {
		final HorizontalLayout pictureLayout = this.componentFactory.newHorizontalLayout("fesc.registration.communication.picture");

		this.pictureField = this.componentFactory.newImage();
		this.pictureField.setHeight(200, Unit.PIXELS);
		pictureLayout.addComponent(this.pictureField);

		final VerticalLayout pictureUploadLayout = this.componentFactory.newVerticalLayout();

		this.pictureUploadField = new UploadComponent(this::pictureUploadReceived);
		this.pictureUploadField.setStartedCallback(this::uploadStarted);
		this.pictureUploadField.setProgressCallback(this::uploadProgress);
		this.pictureUploadField.setFailedCallback(this::uploadFailed);
		this.pictureUploadField.getDropTextLabel().setValue(this.messageSource.getMessage("user.picture.dropFile"));
		this.pictureUploadField.getChoose().setButtonCaption(this.messageSource.getMessage("user.picture.chooseFile"));
		pictureUploadLayout.addComponent(this.pictureUploadField);
		this.pictureProgressBar = new ProgressBar();
		this.pictureProgressBar.setVisible(false);
		pictureUploadLayout.addComponent(this.pictureProgressBar);
		pictureLayout.addComponent(pictureUploadLayout);

		this.addComponent(pictureLayout);
	}

	private void pictureUploadReceived(final String fileName, final Path path) {
		this.uploadedPicturePath = path;
		this.pictureProgressBar.setVisible(false);
		this.showUploadedImage(fileName);
	}

	private void showUploadedImage(final String fileName) {
		if (this.uploadedPicturePath != null) {
			try {
				final FileInputStream inputStream = new FileInputStream(this.uploadedPicturePath.toFile());
				this.showImage(inputStream, fileName);
			} catch (final Exception e) {
				// TODO: handle exception?
			}
		}
	}

	private void showImage(final InputStream imageInputStream, final String fileName) {
		final StreamResource resource = new StreamResource(() -> imageInputStream, Optional.ofNullable(fileName).orElse(imageInputStream.toString()));
		this.pictureField.setSource(resource);
	}

	private void uploadStarted(final String fileName) {
		this.pictureProgressBar.setVisible(true);
	}

	private void uploadProgress(final String fileName, final long readBytes, final long contentLength) {
		if (readBytes > 0) {
			this.pictureProgressBar.setValue((float) contentLength / readBytes);
		}
	}

	private void uploadFailed(final String fileName, final Path file) {
		this.pictureProgressBar.setVisible(false);
	}

	/** This field should be filled with backoffice data. If not, the entity is not valid.
	 *
	 */
	private void addEmailField() {

		this.emailField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("fesc.registration.communication.email").build();
		this.emailField.setSizeFull();
		this.emailField.setWidth(50, Unit.PERCENTAGE);
		this.emailField.setValueChangeMode(ValueChangeMode.BLUR);
		final BindingBuilder<SocialEconomyEntity, String> bb = this.binder.forField(this.emailField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.withValidator(email -> this.validationService.isValidEmail(email), this.messageSource.getMessage("validation.email"));
		if (this.isPublicRegistration()) {
			// Add validator: no user exists with the introduced email
			bb.withValidator(email -> this.pamapamUserService.findByUsername(email.trim()) == null, this.messageSource.getMessage("validation.email.exists"));
		}

		//If this is a public registration form OR the email is empty, this field should be enabled.
		final AtomicReference<Boolean> enableField = new AtomicReference<>(false);
		enableField.set(this.isPublicRegistration());

		bb.bind((socialEconomyEntity) -> {
			final String email = socialEconomyEntity.getEmail();

			final Boolean isEmpty = Optional.ofNullable(socialEconomyEntity.getEmail())
				.map(String::isEmpty)
				.orElse(false);

			enableField.set(isEmpty);
			return email;
		}, SocialEconomyEntity::setEmail);

		this.emailField.setEnabled(enableField.get());

		this.addComponent(this.emailField);
	}

	private void addPhoneField() {
		this.phoneField = this.createTextField("fesc.registration.communication.phone", this.binder, SocialEconomyEntity::getPhone, SocialEconomyEntity::setPhone, false);
		this.addComponent(this.phoneField);
	}

	private void addWebField() {
		this.webField = this.createTextField("fesc.registration.communication.web", this.binder, SocialEconomyEntity::getWeb, SocialEconomyEntity::setWeb, false);
		this.addComponent(this.webField);
	}

	private void addTwitterField() {
		this.twitterField = this.createTextField("fesc.registration.communication.twitter", this.binder, SocialEconomyEntity::getTwitter, SocialEconomyEntity::setTwitter, false);
		this.addComponent(this.twitterField);
	}

	private void addFacebookField() {
		this.facebookField = this.createTextField("fesc.registration.communication.facebook", this.binder, SocialEconomyEntity::getFacebook, SocialEconomyEntity::setFacebook, false);
		this.addComponent(this.facebookField);
	}

	private void addInstagramField() {
		this.instagramField = this.createTextField("fesc.registration.communication.instagram", this.binder, SocialEconomyEntity::getInstagram, SocialEconomyEntity::setInstagram, false);
		this.addComponent(this.instagramField);
	}

	private void addPinterestField() {
		this.pinterestField = this.createTextField("fesc.registration.communication.pinterest", this.binder, SocialEconomyEntity::getPinterest, SocialEconomyEntity::setPinterest, false);
		this.addComponent(this.pinterestField);
	}

	private void addQuitterField() {
		this.quitterField = this.createTextField("fesc.registration.communication.quitter", this.binder, SocialEconomyEntity::getQuitter, SocialEconomyEntity::setQuitter, false);
		this.addComponent(this.quitterField);
	}

	private void addProductsField() {
		this.productsField = new ExtTokenField();
		final ComboBox<ProductTagTokenizable> tagsComboField = this.componentFactory.newComboBox();
		tagsComboField.setCaption("");
		tagsComboField.setDataProvider(DataProvider.ofCollection(this.productTagTokenizables));
		tagsComboField.setTextInputAllowed(true);

		this.productsField.setCaption(this.messageSource.getMessage("fesc.registration.communication.products"));
		this.productsField.setInputField(tagsComboField);
		this.productsField.setEnableDefaultDeleteTokenAction(true);

		tagsComboField.setItemCaptionGenerator(ProductTagTokenizable::getStringValue);
		tagsComboField.setPlaceholder("Type here to add");
		tagsComboField.addValueChangeListener(event -> {
			final ProductTagTokenizable value = event.getValue();
			if (value != null) {
				this.productsField.addTokenizable(value);
				event.getSource().setValue(null);
			}
		});
		tagsComboField.setNewItemHandler(value -> {
			final ProductTagTokenizable newTagTokenizable = new ProductTagTokenizable(new ProductTag(null, value));
			this.productsField.addTokenizable(newTagTokenizable);
			tagsComboField.markAsDirty();
			tagsComboField.setValue(null);
		});

		this.entityTokensBinder.forField(this.productsField).bind("productTokens");

		this.addComponent(this.productsField);
	}

	private void addKeywordsField() {
		this.keywordsField = new ExtTokenField();
		final ComboBox<KeywordTagTokenizable> tagsComboField = this.componentFactory.newComboBox();
		tagsComboField.setCaption("");
		tagsComboField.setDataProvider(DataProvider.ofCollection(this.keywordTagTokenizables));
		tagsComboField.setTextInputAllowed(true);

		this.keywordsField.setCaption(this.messageSource.getMessage("fesc.registration.communication.tags"));
		this.keywordsField.setInputField(tagsComboField);
		this.keywordsField.setEnableDefaultDeleteTokenAction(true);

		tagsComboField.setItemCaptionGenerator(KeywordTagTokenizable::getStringValue);
		tagsComboField.setPlaceholder("Type here to add");
		tagsComboField.addValueChangeListener(event -> {
			final KeywordTagTokenizable value = event.getValue();
			if (value != null) {
				this.keywordsField.addTokenizable(value);
				event.getSource().setValue(null);
			}
		});
		tagsComboField.setNewItemHandler(value -> {
			final KeywordTagTokenizable newTagTokenizable = new KeywordTagTokenizable(new KeywordTag(null, value));
			this.keywordsField.addTokenizable(newTagTokenizable);
			tagsComboField.markAsDirty();
			tagsComboField.setValue(null);
		});

		this.entityTokensBinder.forField(this.keywordsField).bind("keywordTokens");

		this.addComponent(this.keywordsField);
	}

	private void addInvoiceDataFields() {
		final Label title = this.createTitle("fesc.registration.invoice");
		this.addComponent(title);
		this.addInvoiceNameField();
		this.addLegalFormField();
		this.addNifField();
		this.addInvoiceEmailField();
		this.addInvoiceAddressField();
		this.addInvoicePostalCodeField();
		this.addInvoiceFeeField();
		this.addFescSectorField();
		this.addInvoiceAccountField();
	}

	private void addInvoiceNameField() {
		this.invoiceNameField = this.createTextField("fesc.registration.invoice.name", this.binder, SocialEconomyEntity::getFescInvoiceName, SocialEconomyEntity::setFescInvoiceName, true);
		this.addComponent(this.invoiceNameField);
	}

	private void addLegalFormField() {
		this.legalFormField = this.componentFactory.newComboBox("fesc.registration.invoice.legalForm");
		this.legalFormField.setWidth(50, Unit.PERCENTAGE);
		this.legalFormField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.legalFormField.setItems(this.legalFormRepository.findAll());
		this.binder.bind(this.legalFormField, SocialEconomyEntity::getLegalForm, SocialEconomyEntity::setLegalForm);
		this.addComponent(this.legalFormField);
	}

	private void addNifField() {
		this.nifField = this.createTextField("fesc.registration.invoice.nif", this.binder, SocialEconomyEntity::getNif, SocialEconomyEntity::setNif, true);
		this.addComponent(this.nifField);
	}

	private void addInvoiceEmailField() {
		this.invoiceEmailField = this.createEmailField("fesc.registration.invoice.email", this.binder, SocialEconomyEntity::getFescInvoiceEmail, SocialEconomyEntity::setFescInvoiceEmail, true);
		this.addComponent(this.invoiceEmailField);
	}

	private void addInvoiceAddressField() {
		this.invoiceAddressField = this.createTextField("fesc.registration.invoice.address", this.binder, SocialEconomyEntity::getFescInvoiceAddress, SocialEconomyEntity::setFescInvoiceAddress, true);
		this.addComponent(this.invoiceAddressField);
	}

	private void addInvoicePostalCodeField() {
		this.invoicePostalCodeField = this.createTextField("fesc.registration.invoice.postalCode", this.binder, SocialEconomyEntity::getFescInvoicePostalCode, SocialEconomyEntity::setFescInvoicePostalCode, true);
		this.addComponent(this.invoicePostalCodeField);
	}

	private void addInvoiceFeeField() {
		// #10491 Fesc2021: show combobox to select modality, then show fee options depending on modality selection
		this.invoiceModalityField = this.componentFactory.newComboBox("fesc.registration.invoice.modality");
		this.invoiceModalityField.setWidth(50, Unit.PERCENTAGE);
		this.invoiceModalityField.setItemCaptionGenerator(item -> item.getName().getDefaultText());
		this.invoiceModalityField.setItems(this.modalityOptions);
		this.invoiceModalityField.addValueChangeListener(this::invoiceModalitySelected);
		this.binder.forField(this.invoiceModalityField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(SocialEconomyEntity::getModality, SocialEconomyEntity::setModality);
		this.addComponent(this.invoiceModalityField);

		this.invoiceFellowNameField = this.createTextField("fesc.registration.invoice.fellowName",
			this.binder, SocialEconomyEntity::getFellowName, SocialEconomyEntity::setFellowName,
			false);
		this.invoiceFellowNameField.setVisible(false);
		this.addComponent(this.invoiceFellowNameField);

		this.timeSlotField = this.componentFactory.newComboBox("fesc.registration.invoice.timeSlot");
		this.timeSlotField.setWidth(50, Unit.PERCENTAGE);
		this.timeSlotField.setItems(this.fescTimeSlots);
		this.timeSlotField.setItemCaptionGenerator(item -> this.messageSource.getMessage("fesc.registration.invoice.timeSlot." + item.toString()));
		this.binder.forField(this.timeSlotField)
			.bind(SocialEconomyEntity::getFescTimeSlot, SocialEconomyEntity::setFescTimeSLot);
		this.timeSlotField.setVisible(false);
		this.addComponent(this.timeSlotField);

		this.invoiceFeeOptionsField = this.componentFactory.newRadioButtonGroup("fesc.registration.invoice.fee");
		this.invoiceFeeOptionsField.addValueChangeListener(this::invoiceFeeOptionSelected);
		this.invoiceFeeOptionsField.setVisible(false);
		this.addComponent(this.invoiceFeeOptionsField);

		this.invoiceFeeField = this.createNumericTextField(null, this.binder, SocialEconomyEntity::getFescFee, SocialEconomyEntity::setFescFee);
		this.invoiceFeeField.setEnabled(false);
		this.invoiceFeeField.setVisible(false);
		this.addComponent(this.invoiceFeeField);
	}

	private void invoiceModalitySelected(final ValueChangeEvent<FescRegistrationModality> event) {
		final FescRegistrationModality selectedOption = event.getValue();
		this.invoiceFeeOptionsField.clear();

		if (selectedOption != null) {
			this.invoiceFellowNameField.setVisible(selectedOption.isShareTableAllowed());
			this.timeSlotField.setVisible(selectedOption.isTimeSlot());

			final List<FescRegistrationFee> fees = this.feeService.findByModality(selectedOption);
			this.invoiceFeeOptions.clear();
			fees.stream().forEach(fee -> this.invoiceFeeOptions.put(fee.getFee() == null ? fee.getName().getDefaultText() : String.format("%s: %s €", fee.getName().getDefaultText(), fee.getFee()), fee.getFee()));
			this.invoiceFeeOptionsField.setItems(this.invoiceFeeOptions.keySet());
			this.invoiceFeeOptionsField.setVisible(true);
			this.invoiceFeeField.setVisible(true);
		} else {
			this.invoiceFellowNameField.setVisible(false);
			this.invoiceFeeOptionsField.setVisible(false);
			this.timeSlotField.setVisible(false);
			this.invoiceFeeField.setVisible(false);
		}
	}

	private void invoiceFeeOptionSelected(final ValueChangeEvent<String> event) {
		final String selectedOption = event.getValue();
		final Integer value = this.invoiceFeeOptions.get(selectedOption);
		this.invoiceFeeField.setEnabled(value == null);
		this.invoiceFeeField.setValue(value == null ? "" : value.toString());
		if (value == null) {
			this.invoiceFeeField.focus();
		}
	}

	private void addInvoiceAccountField() {
		this.invoiceAccountField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("fesc.registration.invoice.account").build();
		this.invoiceAccountField.setSizeFull();
		this.invoiceAccountField.setWidth(50, Unit.PERCENTAGE);
		this.invoiceAccountField.setValueChangeMode(ValueChangeMode.BLUR);
		this.binder
			.forField(this.invoiceAccountField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.withValidator(iban -> this.validationService.isValidIBAN(iban), this.messageSource.getMessage("validation.iban"))
			.bind(SocialEconomyEntity::getFescInvoiceAccount, SocialEconomyEntity::setFescInvoiceAccount);

		this.addComponent(this.invoiceAccountField);
	}

	private void addPrivacyPolicyFields() {
		this.addFESCRegistrationPolicyField();
		this.addFESCParticipationPolicyField();
		this.addPamapamNewsletterField();
	}

	private void addFESCRegistrationPolicyField() {
		final CheckBox privacyPolicyCheckBox = new CheckBox();
		this.binder.forField(privacyPolicyCheckBox)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(SocialEconomyEntity::getFescRegistrationPrivacyPolicy,
				SocialEconomyEntity::setFescRegistrationPrivacyPolicy);

		final Label privacyPolicyLabel = new Label(this.messageSource.getMessage("fesc.registration.privacyPolicy", new String[] { this.legalAdviceUrl }), ContentMode.HTML);
		final HorizontalLayout layout = this.componentFactory.newHorizontalLayout();
		layout.addComponents(privacyPolicyCheckBox, privacyPolicyLabel);
		this.addComponent(layout);
	}

	private void addFESCParticipationPolicyField() {
		final CheckBox privacyPolicyCheckBox = new CheckBox();
		this.binder.forField(privacyPolicyCheckBox)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(SocialEconomyEntity::getFescParticipationPrivacyPolicy, SocialEconomyEntity::setFescParticipationPrivacyPolicy);

		final Label privacyPolicyLabel = new Label(this.messageSource.getMessage("fesc.registration.participationPrivacyPolicy", new String[] { this.participationPolicyUrl }), ContentMode.HTML);
		final HorizontalLayout layout = this.componentFactory.newHorizontalLayout();
		layout.addComponents(privacyPolicyCheckBox, privacyPolicyLabel);
		this.addComponent(layout);
	}

	private void addPamapamNewsletterField() {
		final CheckBox privacyPolicyCheckBox = new CheckBox();

		this.binder.forField(privacyPolicyCheckBox)
			.bind(SocialEconomyEntity::getNewslettersAccepted, SocialEconomyEntity::setNewslettersAccepted);

		final Label privacyPolicyLabel = new Label(this.messageSource.getMessage("fesc.registration.pamapamNewsletter", new String[] { this.pamapamNewsletterUrl }), ContentMode.HTML);
		final HorizontalLayout layout = this.componentFactory.newHorizontalLayout();
		layout.addComponents(privacyPolicyCheckBox, privacyPolicyLabel);
		this.addComponent(layout);
	}

	private void privacyPolicyChanged(final ValueChangeEvent<Boolean> event) {
		this.okButton.setEnabled(event.getValue());
	}

	private void addButtons() {
		this.okButton = this.componentBuilderFactory.createButtonBuilder().build();
		this.okButton.setCaption(this.messageSource.getMessage("fesc.registration.button"));
		this.okButton.setEnabled(true);
		this.okButton.addClickListener(event -> this.doOk());

		final HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.addComponents(this.okButton);

		this.addComponent(buttonsLayout);
	}

	protected void doOk() {
		if (this.okHandler != null) {
			try {
				this.updateTargetObject();
				this.okHandler.accept(this);
			} catch (final ValidationException e) {
				final String message = e.getFieldValidationErrors().stream().filter(er -> er.getMessage().isPresent()).findFirst().get().getMessage().get();
				FescRegistrationLayout.logger.error(e.getMessage(), e);
				JmgNotification.show(message, Type.ERROR_MESSAGE);
			}
		}
	}

	public SocialEconomyEntity getTargetObject() {
		return this.targetObject;
	}

	public void setTargetObject(final SocialEconomyEntity targetObject) {
		this.targetObject = targetObject;
		this.binder.readBean(this.targetObject);
		this.updateFields();
	}

	public boolean isPublicRegistration() {
		return this.publicRegistration;
	}

	public void setPublicRegistration(final boolean publicRegistration) {
		this.publicRegistration = publicRegistration;
	}

	public void updateTargetObject() throws ValidationException {
		this.validateForm();
		this.binder.writeBean(this.targetObject);

		this.targetObject.setXesBalance(this.xesBalanceOptions.get(this.xesBalanceField.getValue()));
		this.targetObject.setPublicAdministration(this.publicAdministrationOptions.get(this.publicAdministrationField.getValue()));

		if (this.uploadedPicturePath != null) {
			byte[] pictureContents = null;
			String mimeType = "";
			try {
				final FileInputStream pictureInputStream = new FileInputStream(this.uploadedPicturePath.toFile());
				pictureContents = ByteStreams.toByteArray(pictureInputStream);
				mimeType = (new Tika()).detect(pictureInputStream);
				pictureInputStream.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (pictureContents != null) {
				final BinaryResource picture = new BinaryResource(pictureContents);
				picture.setMimeType(mimeType);
				picture.setFileLength(picture.getContents().length);
				picture.setFileName(this.uploadedPicturePath.getFileName().toString());
				this.targetObject.setPicture(picture);
			}
		}
		this.uploadedPicturePath = null;

		for (final ProductTagTokenizable eachTagToken : this.entityTokens.getProductTokens()) {
			ProductTag productTag = eachTagToken.getProductTag();
			if (productTag.getId() == null) {
				productTag = this.productTagService.findOrCreate(productTag.getTag());
			}
			this.targetObject.getProductTag().add(productTag);
		}

		for (final KeywordTagTokenizable eachTagToken : this.entityTokens.getKeywordTokens()) {
			KeywordTag keywordTag = eachTagToken.getKeywordTag();
			if (keywordTag.getId() == null) {
				keywordTag = this.keywordTagService.findOrCreate(keywordTag.getTag());
			}
			this.targetObject.getKeywordTags().add(keywordTag);
		}

		if (this.point != null) {
			this.targetObject.setLatitude(this.point.getY());
			this.targetObject.setLongitude(this.point.getX());
		}

	}

	protected void validateForm() throws ValidationException {
		if (this.binder.validate().hasErrors()) {
			throw new ValidationException(this.binder.validate().getFieldValidationErrors(),
				this.binder.validate().getBeanValidationErrors());
		}
	}

	private TextField createTextField(
		final String captionId,
		final Binder<SocialEconomyEntity> binder,
		final ValueProvider<SocialEconomyEntity, String> getter,
		final Setter<SocialEconomyEntity, String> setter,
		final boolean required) {
		final TextField field = this.componentBuilderFactory.createTextFieldBuilder().setCaption(captionId).build();
		field.setSizeFull();
		field.setWidth(50, Unit.PERCENTAGE);
		if (binder != null) {
			final BindingBuilder<SocialEconomyEntity, String> bb = binder.forField(field);
			if (required) {
				bb.asRequired(this.messageSource.getMessage("validation.emptyField"));
			}
			bb.bind(getter, setter);
		}
		return field;
	}

	private TextField createEmailField(
		final String captionId,
		final Binder<SocialEconomyEntity> binder,
		final ValueProvider<SocialEconomyEntity, String> getter,
		final Setter<SocialEconomyEntity, String> setter,
		final boolean required) {
		final TextField field = this.componentBuilderFactory.createTextFieldBuilder().setCaption(captionId).build();
		field.setSizeFull();
		field.setWidth(50, Unit.PERCENTAGE);
		field.setValueChangeMode(ValueChangeMode.BLUR);
		if (binder != null) {
			final BindingBuilder<SocialEconomyEntity, String> bb = binder.forField(field);
			if (required) {
				bb.asRequired(this.messageSource.getMessage("validation.emptyField"));
			}
			bb.withValidator(email -> this.validationService.isValidEmail(email), this.messageSource.getMessage("validation.email"));
			bb.bind(getter, setter);
		}
		return field;
	}

	private TextField createUrlField(
		final String captionId,
		final Binder<SocialEconomyEntity> binder,
		final ValueProvider<SocialEconomyEntity, String> getter,
		final Setter<SocialEconomyEntity, String> setter,
		final boolean required) {
		final TextField field = this.componentBuilderFactory.createTextFieldBuilder().setCaption(captionId).build();
		field.setSizeFull();
		field.setWidth(50, Unit.PERCENTAGE);
		field.setValueChangeMode(ValueChangeMode.BLUR);
		if (binder != null) {
			final BindingBuilder<SocialEconomyEntity, String> bb = binder.forField(field);
			if (required) {
				bb.asRequired(this.messageSource.getMessage("validation.emptyField"));
			}
			bb.withValidator(url -> this.validationService.isValidUrl(url), this.messageSource.getMessage("validation.url"));
			bb.bind(getter, setter);
		}
		return field;
	}

	private TextField createNumericTextField(
		final String captionId,
		final Binder<SocialEconomyEntity> binder,
		final ValueProvider<SocialEconomyEntity, Integer> getter,
		final Setter<SocialEconomyEntity, Integer> setter) {
		final TextField field = this.componentBuilderFactory.createTextFieldBuilder().setCaption(captionId).build();
		field.setSizeFull();
		field.setWidth(25, Unit.PERCENTAGE);
		if (binder != null) {
			binder.forField(field)
				.withNullRepresentation("")
				.withConverter(new StringToIntegerConverter(0, this.messageSource.getMessage("validation.emptyField")))
				.withValidator(new IntegerRangeValidator(this.messageSource.getMessage("validations.number.greaterThanZero"), 0, null))
				.asRequired(this.messageSource.getMessage("validation.emptyField"))
				.bind(getter, setter);
		}
		return field;
	}

	private Label createTitle(final String value) {
		return this.createLabel(value, "title");
	}

	private Label createLabel(final String value, final String styleName) {
		final Label label = this.componentFactory.newLabel();
		label.setValue(this.messageSource.getMessage(value));
		if (styleName != null) {
			label.setStyleName(styleName);
		}
		return label;
	}

	private void updateFields() {
		this.uploadedPicturePath = null;
		final SocialEconomyEntity socialEconomyEntity = this.targetObject;
		if (socialEconomyEntity != null) {

			// main picture / logo
			byte[] pictureBytes = null;
			if (socialEconomyEntity.getPicture() != null) {
				pictureBytes = socialEconomyEntity.getPicture().getContents();
			}

			if (pictureBytes != null) {
				final InputStream pictureInputStream = new ByteArrayInputStream(pictureBytes);
				this.showImage(pictureInputStream, socialEconomyEntity.getPicture().getFileName());
			} else {
				this.pictureField.setSource(new ThemeResource("images/placeholder.png"));
			}

			// Has fet el balanç social?
			String xesBalanceValue = null;
			for (final Map.Entry<String, Boolean> eachEntry : this.xesBalanceOptions.entrySet()) {
				if (Objects.equals(eachEntry.getValue(), socialEconomyEntity.getXesBalance())) {
					xesBalanceValue = eachEntry.getKey();
					break;
				}
			}
			this.xesBalanceField.setSelectedItem(xesBalanceValue);

			// Treballeu per l'administració?
			String publicAdministrationValue = null;
			Boolean seeValue = socialEconomyEntity.getPublicAdministration();
			if (seeValue == null) {
				seeValue = Boolean.FALSE;
			}
			for (final Map.Entry<String, Boolean> eachEntry : this.publicAdministrationOptions.entrySet()) {
				if (Objects.equals(eachEntry.getValue(), seeValue)) {
					publicAdministrationValue = eachEntry.getKey();
					break;
				}
			}
			this.publicAdministrationField.setSelectedItem(publicAdministrationValue);

			// TODO Quota FESC

			// Territory fields
			final Region region = socialEconomyEntity.getRegion();
			if (region != null) {
				this.provinceField.setValue(region.getProvince());
				this.regionField.setValue(region);
			} else {
				this.provinceField.setValue(null);
				this.regionField.setValue(null);
			}

			// Map geoLocation
			this.updateMap(socialEconomyEntity.getLatitude(), socialEconomyEntity.getLongitude());

			// ...	Product Tags.

			if (CollectionUtils.isEmpty(socialEconomyEntity.getProductTag())) {
				socialEconomyEntity.setProductTag(Sets.newHashSet());
			}

			final List<ProductTagTokenizable> productTagsTokens = new ArrayList<>();
			for (final ProductTag eachProductTag : socialEconomyEntity.getProductTag()) {
				productTagsTokens.add(new ProductTagTokenizable(eachProductTag));
			}

			// ...	Keyword Tags.

			if (CollectionUtils.isEmpty(socialEconomyEntity.getKeywordTags())) {
				socialEconomyEntity.setKeywordTags(Sets.newHashSet());
			}

			final List<KeywordTagTokenizable> keywordTagsTokens = new ArrayList<>();
			for (final KeywordTag eachKeywordTag : socialEconomyEntity.getKeywordTags()) {
				keywordTagsTokens.add(new KeywordTagTokenizable(eachKeywordTag));
			}

			this.entityTokens.setProductTokens(productTagsTokens);
			this.entityTokens.setKeywordTokens(keywordTagsTokens);
			this.entityTokensBinder.readBean(this.entityTokens);
		}
	}

	public void setOkHandler(final Consumer<FescRegistrationLayout> okHandler) {
		this.okHandler = okHandler;
	}

}
