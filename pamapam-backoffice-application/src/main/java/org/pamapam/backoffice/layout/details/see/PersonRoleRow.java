package org.pamapam.backoffice.layout.details.see;

public class PersonRoleRow {

	private Integer femaleCount;
	private Integer maleCount;
	private Integer otherCount;

	public PersonRoleRow() {
		this.femaleCount = 0;
		this.maleCount = 0;
		this.otherCount = 0;
	}

	public Integer getFemaleCount() {
		return this.femaleCount;
	}

	public void setFemaleCount(Integer femaleCount) {
		this.femaleCount = femaleCount;
	}

	public Integer getMaleCount() {
		return this.maleCount;
	}

	public void setMaleCount(Integer maleCount) {
		this.maleCount = maleCount;
	}

	public Integer getOtherCount() {
		return this.otherCount;
	}

	public void setOtherCount(Integer otherCount) {
		this.otherCount = otherCount;
	}

	public Integer getTotalCount() {
		Integer totalCount = this.femaleCount == null ? 0 : this.femaleCount;
		totalCount += this.maleCount == null ? 0 : this.maleCount;
		totalCount += this.otherCount == null ? 0 : this.otherCount;
		return totalCount;
	}

}
