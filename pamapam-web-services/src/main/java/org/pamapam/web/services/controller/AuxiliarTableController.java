package org.pamapam.web.services.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.dto.PageDto;
import org.jamgo.services.dto.ResponseDto;
import org.jamgo.web.services.controller.ServiceController;
import org.modelmapper.TypeToken;
import org.pamapam.services.CommunityService;
import org.pamapam.services.CriterionService;
import org.pamapam.services.LegalFormService;
import org.pamapam.services.SocialEconomyNetworkService;
import org.pamapam.services.dto.CommunityDto;
import org.pamapam.services.dto.CriterionDto;
import org.pamapam.services.dto.LegalFormDto;
import org.pamapam.services.dto.SocialEconomyNetworkDto;
import org.pamapam.services.dto.views.DtoViews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Servlet Controller for SearchCategories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 *
 */
@Controller
@RequestMapping("/api")
public class AuxiliarTableController extends ServiceController {

	@Autowired
	private LegalFormService legalFormService;
	@Autowired
	private CommunityService communityService;
	@Autowired
	private SocialEconomyNetworkService socialEconomyNetworkService;
	@Autowired
	private CriterionService criterionService;

	@RequestMapping(value = "/legalForms", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(LegalFormDto.class)
	protected ResponseDto<PageDto<LegalFormDto>> getLegalForms(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.legalFormService.findAllDto(pageRequest, new TypeToken<List<LegalFormDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/communities", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(CommunityDto.class)
	protected ResponseDto<List<CommunityDto>> getCommunities(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.communityService.findAllDto(new Sort("name")));
	}

	@RequestMapping(value = "/socialEconomyNetworks", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(SocialEconomyNetworkDto.class)
	protected ResponseDto<PageDto<SocialEconomyNetworkDto>> getSocialEconomyNetworks(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.socialEconomyNetworkService.findAllDto(pageRequest, new TypeToken<List<SocialEconomyNetworkDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/criteria", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(DtoViews.DetailedList.class)
	protected ResponseDto<List<CriterionDto>> getCriteria(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.criterionService.findAllDto(new Sort("viewOrder")));
	}

	@RequestMapping(value = "/oldCriteria", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(DtoViews.DetailedList.class)
	protected ResponseDto<List<CriterionDto>> getOldCriteria(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.criterionService.findAllOldDto(new Sort("viewOrder")));
	}

}
