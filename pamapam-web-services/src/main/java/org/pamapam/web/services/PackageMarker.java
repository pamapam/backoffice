package org.pamapam.web.services;

/*
 * From @ComponentScan -> basePackageClasses documentation:
 *
 * Type-safe alternative to basePackages() for specifying the packages to scan
 * for annotated components. The package of each class specified will be scanned.
 *
 * Consider creating a special no-op marker class or interface in each package
 * that serves no purpose other than being referenced by this attribute.
 */

public interface PackageMarker {

}
