package org.pamapam.web.services.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.dto.PageDto;
import org.jamgo.services.dto.ResponseDto;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.web.services.controller.ServiceController;
import org.modelmapper.TypeToken;
import org.pamapam.services.PamapamUserService;
import org.pamapam.services.dto.PamapamUserDto;
import org.pamapam.services.dto.PamapamUserSearchDto;
import org.pamapam.services.dto.views.DtoViews;
import org.pamapam.services.exception.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Servlet Controller for SearchCategories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 *
 */
@Controller
@RequestMapping("/api")
public class PamapamUserController extends ServiceController {

	@Autowired
	private LocalizedMessageService messageService;
	
	@Autowired
	private PamapamUserService pamapamUserService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(PamapamUserDto.class)
	protected ResponseDto<PageDto<PamapamUserDto>> getUsers(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.pamapamUserService.findAllDto(pageRequest, new TypeToken<List<PamapamUserDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/activityUsers", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(PamapamUserDto.class)
	protected ResponseDto<PageDto<PamapamUserDto>> getActivityUsers(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.pamapamUserService.findSortedByActivityDto(pageRequest));
	}

	@RequestMapping(value = "/searchUsers", method = RequestMethod.POST)
	@ResponseBody
	@JsonView(DtoViews.DetailedList.class)
	protected ResponseDto<PageDto<PamapamUserDto>> searchUsers(
		HttpServletRequest request,
		HttpServletResponse response,
		@RequestBody PamapamUserSearchDto searchObjectDto) {

		PageRequest pageRequest = new PageRequest(searchObjectDto.getPage(), searchObjectDto.getSize(), new Sort("name"));
		return new ResponseDto<>(this.pamapamUserService.searchDto(searchObjectDto, pageRequest));
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(PamapamUserDto.class)
	protected ResponseDto<PamapamUserDto> getUser(
		HttpServletRequest request,
		HttpServletResponse response,
		@PathVariable Long id) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.pamapamUserService.findOneDto(id, PamapamUserDto.class));
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	@ResponseBody
	protected ResponseDto<Object> addUser(
		HttpServletRequest request,
		HttpServletResponse response,
		@RequestBody PamapamUserDto userDto) throws CrudException, UserAlreadyExistsException {

		ResponseDto<Object> responseDto = new ResponseDto<Object>();
		if (this.pamapamUserService.findByUsername(userDto.getUsername().trim()) != null) {
			responseDto.setStatus(HttpStatus.CONFLICT.value());
			responseDto.setMessage(this.messageService.getMessage("validation.user.username.exists", new String[] {userDto.getUsername()},request.getLocale()));
			return responseDto;
		}
		if (this.pamapamUserService.findByEmail(userDto.getEmail().trim()) != null) {
			responseDto.setStatus(HttpStatus.CONFLICT.value());
			responseDto.setMessage(this.messageService.getMessage("validation.user.email.exists", new String[] {userDto.getEmail()},request.getLocale()));
			return responseDto;
		} 
		this.pamapamUserService.addUser(userDto);
		return new ResponseDto<Object>(null);
	}

}
