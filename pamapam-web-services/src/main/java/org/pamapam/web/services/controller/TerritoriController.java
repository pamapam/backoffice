package org.pamapam.web.services.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.dto.PageDto;
import org.jamgo.services.dto.ResponseDto;
import org.jamgo.web.services.controller.ServiceController;
import org.modelmapper.TypeToken;
import org.pamapam.services.*;
import org.pamapam.services.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Servlet Controller for Territories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 *
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/api")
public class TerritoriController extends ServiceController {

	@Autowired
	private ProvinceService provinceService;
	@Autowired
	private RegionService regionService;
	@Autowired
	private TownService townService;
	@Autowired
	private DistrictService districtService;
	@Autowired
	private NeighborhoodService neighborhoodService;
	@Autowired
	private TerritoryService territoryService;

	@RequestMapping(value = "/provinces", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(ProvinceDto.class)
	protected ResponseDto<PageDto<ProvinceDto>> getProvinces(
		final HttpServletRequest request,
		final HttpServletResponse response,
		final Pageable pageable) throws RepositoryForClassNotDefinedException {

		final PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.provinceService.findAllDto(pageRequest, new TypeToken<List<ProvinceDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/provinces/{id}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(ProvinceDto.class)
	protected ResponseDto<ProvinceDto> getProvince(
		final HttpServletRequest request,
		final HttpServletResponse response,
		@PathVariable final Long id) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.provinceService.findOneDto(id, ProvinceDto.class));
	}

	@RequestMapping(value = "/regions", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(RegionDto.class)
	protected ResponseDto<PageDto<RegionDto>> getRegions(
		final HttpServletRequest request,
		final HttpServletResponse response,
		final Pageable pageable) throws RepositoryForClassNotDefinedException {

		final PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.regionService.findAllDto(pageRequest, new TypeToken<List<RegionDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/regions/{id}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(RegionDto.class)
	protected ResponseDto<RegionDto> getRegion(
		final HttpServletRequest request,
		final HttpServletResponse response,
		@PathVariable final Long id) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.regionService.findOneDto(id, RegionDto.class));
	}

	@RequestMapping(value = "/towns", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(TownDto.class)
	protected ResponseDto<PageDto<TownDto>> getTowns(
		final HttpServletRequest request,
		final HttpServletResponse response,
		final Pageable pageable) throws RepositoryForClassNotDefinedException {

		final PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name", "id"));
		return new ResponseDto<>(this.townService.findAllDto(pageRequest, new TypeToken<List<TownDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/towns/{id}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(TownDto.class)
	protected ResponseDto<TownDto> getTown(
		final HttpServletRequest request,
		final HttpServletResponse response,
		@PathVariable final Long id) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.townService.findOneDto(id, TownDto.class));
	}

	@RequestMapping(value = "/districts", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(DistrictDto.class)
	protected ResponseDto<PageDto<DistrictDto>> getDistricts(
		final HttpServletRequest request,
		final HttpServletResponse response,
		final Pageable pageable) throws RepositoryForClassNotDefinedException {

		final PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.districtService.findAllDto(pageRequest, new TypeToken<List<DistrictDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/districts/{id}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(DistrictDto.class)
	protected ResponseDto<DistrictDto> getDistrict(
		final HttpServletRequest request,
		final HttpServletResponse response,
		@PathVariable final Long id) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.districtService.findOneDto(id, DistrictDto.class));
	}

	@RequestMapping(value = "/neighborhoods", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(NeighborhoodDto.class)
	protected ResponseDto<PageDto<NeighborhoodDto>> getNeighborhoods(
		final HttpServletRequest request,
		final HttpServletResponse response,
		final Pageable pageable) throws RepositoryForClassNotDefinedException {

		final PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.neighborhoodService.findAllDto(pageRequest, new TypeToken<List<NeighborhoodDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/neighborhoods/{id}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(NeighborhoodDto.class)
	protected ResponseDto<NeighborhoodDto> getNeighborhood(
		final HttpServletRequest request,
		final HttpServletResponse response,
		@PathVariable final Long id) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.neighborhoodService.findOneDto(id, NeighborhoodDto.class));
	}

	@RequestMapping(value = "/territories", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(TerritoryDto.class)
	protected ResponseDto<List<TerritoryDto>> getTerritories(
		final HttpServletRequest request,
		final HttpServletResponse response) throws RepositoryForClassNotDefinedException {

		return new ResponseDto<>(this.territoryService.findAllDto());
	}

}
