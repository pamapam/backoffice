package org.pamapam.web.services.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.dto.ResponseDto;
import org.jamgo.web.services.controller.ServiceController;
import org.jamgo.web.services.exception.NotFoundException;
import org.pamapam.services.EmbeddedMapConfigService;
import org.pamapam.services.dto.EmbeddedMapConfigDto;
import org.pamapam.services.dto.views.DtoViews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Servlet Controller for SearchCategories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 *
 */
@Controller
@RequestMapping("/api")
public class EmbeddedMapConfigController extends ServiceController {

	@Autowired
	private EmbeddedMapConfigService embeddedMapConfigService;

	@RequestMapping(value = "/embeddedMapConfig/{domain}/{apiKey}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(DtoViews.Full.class)
	protected ResponseDto<EmbeddedMapConfigDto> getEmbeddedMapConfig(
		final HttpServletRequest request,
		final HttpServletResponse response,
		@PathVariable final String domain,
		@PathVariable final String apiKey)
		throws RepositoryForClassNotDefinedException,
		NotFoundException {

		final EmbeddedMapConfigDto embeddedMapConfigDto = this.embeddedMapConfigService.findByDomainAndApiKeyDto(domain.replace('_', '.'), apiKey);
		if (embeddedMapConfigDto == null) {
			throw new NotFoundException("Embedded map config not found");
		}
		return new ResponseDto<>(embeddedMapConfigDto);
	}

}
