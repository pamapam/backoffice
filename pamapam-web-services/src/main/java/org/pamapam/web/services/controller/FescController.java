package org.pamapam.web.services.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.services.dto.ResponseDto;
import org.pamapam.services.FescService;
import org.pamapam.services.dto.FescEntityDto;
import org.pamapam.services.dto.views.DtoViews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/api")
public class FescController {
	
	@Autowired
	private FescService fescService;

	@RequestMapping(value = "/fesc/entities", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(DtoViews.DetailedList.class)
	protected ResponseDto<List<FescEntityDto>> getFescEntities(
		HttpServletRequest request,
		HttpServletResponse response) {

		List<FescEntityDto> seeList = this.fescService.getFescEntities();
		return new ResponseDto<>(seeList);
	}
}
