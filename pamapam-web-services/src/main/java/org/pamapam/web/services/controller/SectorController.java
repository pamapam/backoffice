package org.pamapam.web.services.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.web.services.controller.ServiceController;
import org.modelmapper.AbstractConverter;
import org.pamapam.model.Sector;
import org.pamapam.repository.SectorRepository;
import org.pamapam.services.dto.SectorDto;
import org.pamapam.services.dto.views.DtoViews;
import org.pamapam.services.dto.converters.SectorDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Servlet Controller for SearchCategories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 */
@Controller
@RequestMapping("/api")
public class SectorController extends ServiceController {

    @Autowired
    private SectorRepository sectorRepository;
    @Autowired
    private SectorDtoConverter sectorDtoConverter;

    @RequestMapping(value = "/sectors", method = RequestMethod.GET)
    @ResponseBody
    @JsonView(DtoViews.DetailedList.class)
    protected List<SectorDto> getSectors(
            final HttpServletRequest request,
            final HttpServletResponse response)
            throws RepositoryForClassNotDefinedException {

        List<Sector> sectors = this.sectorRepository.findAll();
        Collections.sort(sectors, Sector.FULLNAME_ORDER);
        sectors = sectors.stream()
            .filter(sector -> sector.getParent() != null)
            .collect(Collectors.toList());

        List<SectorDto> sectorDtos = sectors.stream()
            .map(each -> this.modelMapper.map(each, SectorDto.class))
            .collect(Collectors.toList());

        sectorDtos.forEach(each -> each.setParent(null));
        return sectorDtos;
    }

    @RequestMapping(value = "/sectors/", method = RequestMethod.GET)
    @ResponseBody
    @JsonView(DtoViews.Internal.class)
    protected List<SectorDto> getSectors(final HttpServletRequest request, final HttpServletResponse response, @RequestParam Long[] values)
            throws RepositoryForClassNotDefinedException {

        List<Long> valuesList = Arrays.asList(values);
        List<Sector> sectors = this.sectorRepository.findAll();
        Collections.sort(sectors, Sector.FULLNAME_ORDER);
        sectors = sectors.stream()
                .filter(sector -> sector.getParent() != null)
                .filter(sector -> valuesList.contains(sector.getParent().getId()))
                .collect(Collectors.toList());

        return sectors.stream().map(each -> this.sectorDtoConverter.convert(each)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/mainSectors", method = RequestMethod.GET)
    @ResponseBody
    @JsonView(Object.class)
    protected List<SectorDto> getMainSectors(
            final HttpServletRequest request,
            final HttpServletResponse response)
            throws RepositoryForClassNotDefinedException {

        final List<Sector> sectors = this.sectorRepository.findByParentIsNull();
        Collections.sort(sectors, Sector.NAME_HIERARCHICAL_ORDER);

        return sectors.stream().map(each -> this.modelMapper.map(each, SectorDto.class)).collect(Collectors.toList());
    }

}
