package org.pamapam.web.services.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.jamgo.services.dto.PageDto;
import org.jamgo.services.dto.ResponseDto;
import org.jamgo.services.exception.CrudException;
import org.jamgo.web.services.controller.ServiceController;
import org.pamapam.model.EntityScope;
import org.pamapam.model.SocialEconomyEntity;
import org.pamapam.repository.SocialEconomyEntityRepository;
import org.pamapam.services.*;
import org.pamapam.services.dto.SocialEconomyEntityDto;
import org.pamapam.services.dto.SocialEconomyEntityInitiativeDto;
import org.pamapam.services.dto.SocialEconomyEntityProposedDto;
import org.pamapam.services.dto.SocialEconomyEntitySearchDto;
import org.pamapam.services.dto.views.DtoViews;
import org.pamapam.services.exception.UserAlreadyExistsException;
import org.pamapam.services.geojson.dto.GeoJsonDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Servlet Controller for SearchCategories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/api")
public class SocialEconomyEntityController extends ServiceController {

    private final static String PAMAPAM_SCOPE = "pamapam";
    private final static String COMMUNITY_ECONOMY_SCOPE = "economia-comunitaria";
    private static final Logger logger = LoggerFactory.getLogger(SocialEconomyEntityController.class);

    @Autowired
    private SocialEconomyEntityService socialEconomyEntityService;
    @Autowired
    private SearchInfoService searchInfoService;
    @Autowired
    private EntityScopeService entityScopeService;
    @Autowired
    private SocialBalanceService socialBalanceService;
    @Autowired
    private BalanceTownConverterService balanceTownConverterService;
    @Autowired
    private SocialEconomyEntityRepository socialEconomyEntityRepository;
    @Autowired
    private SectorService sectorService;

    @RequestMapping(value = "/entities", method = RequestMethod.GET)
    @ResponseBody
    @JsonView(DtoViews.DetailedList.class)
    protected ResponseDto<PageDto<SocialEconomyEntityDto>> getSocialEconomyEntities(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final Pageable pageable) {

        final PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
        return new ResponseDto<>(this.socialEconomyEntityService.findAllPublishedDto(pageRequest));
    }

    @RequestMapping(value = "/searchEntities", method = RequestMethod.POST)
    @ResponseBody
    @JsonView(DtoViews.Full.class)
    protected ResponseDto<PageDto<SocialEconomyEntityDto>> searchEntities(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestBody final SocialEconomyEntitySearchDto searchObjectDto) {

        if (StringUtils.isNotBlank(searchObjectDto.getText())) {
            this.searchInfoService.addSearchInfo(searchObjectDto.getText(), new Timestamp(System.currentTimeMillis()));
        }

        final PageRequest pageRequest = new PageRequest(
                Optional.ofNullable(searchObjectDto.getPage()).orElse(0),
                Optional.ofNullable(searchObjectDto.getSize()).orElse(20),
                new Sort("name"));

        if (searchObjectDto.getApiKey() == null) {
            return new ResponseDto<>(this.socialEconomyEntityService.searchAllPamapamPublishedAndSocialBalancePublishedAndExternal(searchObjectDto, pageRequest));
        } else {
            return new ResponseDto<>(this.socialEconomyEntityService.searchDto(searchObjectDto, pageRequest));
        }
    }

    @RequestMapping(value = "/entitiesGeojson/{entityId}", method = RequestMethod.GET)
    @ResponseBody
    protected ResponseDto<GeoJsonDto> getEntityGeojson(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @PathVariable final Long entityId) {

        return new ResponseDto<>(this.socialEconomyEntityService.findGeoJson(entityId));
    }

    @RequestMapping(value = "/entitiesGeojson", method = RequestMethod.GET)
    @ResponseBody
    protected ResponseDto<List<GeoJsonDto>> getEntitiesGeojson(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        return new ResponseDto<>(this.socialEconomyEntityService.findAllGeoJson());
    }

    @RequestMapping(value = "/searchEntitiesGeojson", method = RequestMethod.POST)
    @ResponseBody
    protected ResponseDto<List<GeoJsonDto>> searchEntitiesGeojson(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestBody final SocialEconomyEntitySearchDto searchObjectDto) {

        if (StringUtils.isNotBlank(searchObjectDto.getText())) {
            this.searchInfoService.addSearchInfo(searchObjectDto.getText(), new Timestamp(System.currentTimeMillis()));
        }

        if (searchObjectDto.getApiKey() == null) { //no api key means that it is not an embedded map that can be customized from the Backoffice
            return new ResponseDto<>(this.socialEconomyEntityService.findAllGeoJsonPamapamPublishedAndSocialBalancePublishedAndExternal(searchObjectDto));
        } else {
            return new ResponseDto<>(this.socialEconomyEntityService.searchGeoJson(searchObjectDto));
        }
    }

    @RequestMapping(value = "/entities/{entityId}", method = RequestMethod.GET)
    @ResponseBody
    @JsonView(DtoViews.Full.class)
    protected ResponseDto<SocialEconomyEntityDto> getSocialEconomyEntity(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @PathVariable final String entityId) {

        return new ResponseDto<>(this.socialEconomyEntityService.findByEntityId(entityId));
    }

    @RequestMapping(value = "/relatedEntitiesBySector/{id}/{count}", method = RequestMethod.GET)
    @ResponseBody
    @JsonView(DtoViews.DetailedList.class)
    protected ResponseDto<List<SocialEconomyEntityDto>> getRelatedSocialEconomyEntitiesBySector(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @PathVariable final Long id,
            @PathVariable final Integer count) {

        return new ResponseDto<>(this.socialEconomyEntityService.findRandomRelatedDtoBySector(id, count));
    }

    @RequestMapping(value = "/relatedEntitiesByProximity/{id}/{count}", method = RequestMethod.GET)
    @ResponseBody
    @JsonView(DtoViews.DetailedList.class)
    protected ResponseDto<List<SocialEconomyEntityDto>> getRelatedSocialEconomyEntitiesByProximity(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @PathVariable final Long id,
            @PathVariable final Integer count) {

        return new ResponseDto<>(this.socialEconomyEntityService.findRandomRelatedDtoByProximity(id, count));
    }

    @RequestMapping(value = "/addProposed", method = RequestMethod.POST)
    @ResponseBody
    protected ResponseDto<Object> addProposed(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestBody final SocialEconomyEntityProposedDto proposedDto)
            throws CrudException {

        if (SocialEconomyEntityController.COMMUNITY_ECONOMY_SCOPE.equals(proposedDto.getScope())) {
            final Set<EntityScope> entityScopes = Sets.newHashSet(
                    this.entityScopeService.getDefaultEntityScope(),
                    this.entityScopeService.getCommunityEconomyEntityScope());
            this.socialEconomyEntityService.addProposed(proposedDto, entityScopes);
        } else {
            this.socialEconomyEntityService.addPamapamProposed(proposedDto);
        }
        return new ResponseDto<>(null);
    }

    /**
     * Añade un punto nuevo y lo asocia al ámbito pol intercooperatiu.
     * Cuando se crea un punto de este ámbito se crea un usuario iniciativa.
     *
     * @param request
     * @param response
     * @param proposedDto
     * @return
     * @throws CrudException
     */
    @RequestMapping(value = "/addPolIntercoop", method = RequestMethod.POST)
    @ResponseBody
    protected ResponseDto<Object> addPolIntercoop(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestBody final SocialEconomyEntityInitiativeDto proposedDto)
            throws CrudException {

        try {
            this.socialEconomyEntityService.addPamaPamPolIntercoop(proposedDto);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return new ResponseDto<>(null);
    }

    @RequestMapping(value = "/addInitiative", method = RequestMethod.POST)
    @ResponseBody
    protected ResponseDto<Object> addInitiative(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestBody final SocialEconomyEntityInitiativeDto initiativeDto)
            throws CrudException,
            UserAlreadyExistsException {

        if (SocialEconomyEntityController.COMMUNITY_ECONOMY_SCOPE.equals(initiativeDto.getScope())) {
            final Set<EntityScope> entityScopes = Sets.newHashSet(
                    this.entityScopeService.getDefaultEntityScope(),
                    this.entityScopeService.getCommunityEconomyEntityScope());
            this.socialEconomyEntityService.addInitiative(initiativeDto, entityScopes);
        } else {
            this.socialEconomyEntityService.addPamapamInitiative(initiativeDto);
        }
        return new ResponseDto<>(null);
    }

    @RequestMapping(value = "/addEinatecaInitiative", method = RequestMethod.POST)
    @ResponseBody
    protected ResponseDto<Object> addEinatecaInitiative(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestBody final SocialEconomyEntityInitiativeDto initiativeDto)
            throws CrudException,
            UserAlreadyExistsException {

        this.socialEconomyEntityService.addEinatecaInitiative(initiativeDto);
        return new ResponseDto<>(null);
    }

    /**
     * @param request
     * @param response
     * @param which
     * @param userIdParam
     * @param token
     */
    @RequestMapping(value = "/conditionsAcceptance/{which}/{userIdParam}/{token}", method = RequestMethod.GET)
    protected void conditionsAcceptance(
            final HttpServletRequest request,
            final HttpServletResponse response,
            @PathVariable final Integer which,
            @PathVariable String userIdParam,
            @PathVariable final String token) {

        userIdParam = userIdParam.replace(",", "");
        final Long userId = Long.valueOf(userIdParam);

        if (this.socialEconomyEntityService.conditionsAcceptance(which, userId, token)) {

            SocialEconomyEntity entity = this.socialEconomyEntityRepository.findOne(userId);
            entity = SocialEconomyEntityService.setEntityStatusToFirstNext(entity);

            try {
                this.socialEconomyEntityService.acceptConditionsFromPrepublishedEmail(entity);
            } catch (final Exception e) {
                logger.error("Error when trying to accept conditions for entity {}", entity, e.getMessage());
                response.setHeader("Location", "/nok-condicions-usuari");
            }

            response.setHeader("Location", "/ok-condicions-usuari");
        } else {
            response.setHeader("Location", "/nok-condicions-usuari");
        }
        response.setStatus(302);
    }

}
