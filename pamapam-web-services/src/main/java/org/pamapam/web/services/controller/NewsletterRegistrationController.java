package org.pamapam.web.services.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.dto.PageDto;
import org.jamgo.services.dto.ResponseDto;
import org.jamgo.services.exception.CrudException;
import org.jamgo.web.services.controller.ServiceController;
import org.modelmapper.TypeToken;
import org.pamapam.services.NewsletterRegistrationService;
import org.pamapam.services.dto.NewsletterRegistrationDto;
import org.pamapam.services.dto.PamapamUserDto;
import org.pamapam.services.exception.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Servlet Controller for SearchCategories services. Results are include in the response
 * in JSON format. Each method documentation must define the output JSON format.
 *
 * @author martin.espinach
 *
 */
@Controller
@RequestMapping("/api")
public class NewsletterRegistrationController extends ServiceController {

	@Autowired
	private NewsletterRegistrationService newsletterRegistrationService;

	@RequestMapping(value = "/newsletterRegistrations", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(PamapamUserDto.class)
	protected ResponseDto<PageDto<NewsletterRegistrationDto>> getUsers(
		HttpServletRequest request,
		HttpServletResponse response,
		Pageable pageable) throws RepositoryForClassNotDefinedException {

		PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort("name"));
		return new ResponseDto<>(this.newsletterRegistrationService.findAllDto(pageRequest, new TypeToken<List<NewsletterRegistrationDto>>() {
		}.getType()));
	}

	@RequestMapping(value = "/addNewsletterRegistration", method = RequestMethod.POST)
	@ResponseBody
	protected ResponseDto<Object> addUser(
		HttpServletRequest request,
		HttpServletResponse response,
		@RequestBody NewsletterRegistrationDto newsletterRegistrationDto) throws CrudException, UserAlreadyExistsException {

		this.newsletterRegistrationService.addNewsletterRegistration(newsletterRegistrationDto);
		return new ResponseDto<Object>(null);
	}

}
