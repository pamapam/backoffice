package org.pamapam.web.services.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.repository.BinaryResourceRepository;
import org.jamgo.web.services.controller.ServiceController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api")
public class BinaryResourceController extends ServiceController {

	@Autowired
	private BinaryResourceRepository binanyResourceRepository;

	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
	@ResponseBody
	protected void getResource(
		HttpServletRequest request,
		HttpServletResponse response,
		@PathVariable Long id) throws SQLException, IOException {

		BinaryResource binaryResource = null;
		binaryResource = this.binanyResourceRepository.findOne(id);

		String mimeType = null;
		if ((binaryResource.getMimeType() != null) && !binaryResource.getMimeType().isEmpty()) {
			mimeType = binaryResource.getMimeType();
		}
		if (mimeType == null) {
			Tika tika = new Tika();
			if (binaryResource.getFileName() != null) {
				mimeType = tika.detect(binaryResource.getFileName());
			}
			if (mimeType == null) {
				mimeType = tika.detect(binaryResource.getContents());
			}
		}
		response.setContentType(mimeType);

		if (binaryResource.getFileLength() != null) {
			response.setContentLength(binaryResource.getFileLength());
		} else {
			response.setContentLength(binaryResource.getContents().length);
		}

		InputStream contentsStream = new ByteArrayInputStream(binaryResource.getContents());
		IOUtils.copy(contentsStream, response.getOutputStream());
		response.getOutputStream().close();
	}

}
