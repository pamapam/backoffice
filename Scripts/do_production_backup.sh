#!/bin/sh
# This script is used to make a backup of the production environment of the backoffice from Pam a Pam.
# WARNING: YOU MUST INITIALIZE DB_USER_NAME, DB_PASSWORD AND DB_NAME BEFORE EXECUTING THIS SCRIPT!!
DB_USER_NAME=
DB_PASSWORD=
DB_NAME=

# Delete everything in the server's backups folder.
rm -r /srv/backups/*

# Create a new directory with the date and time as its name.
mkdir /srv/backups/$(date +%Y%m%d%T)

# Copy the war being used that day, copy the resources zip, and do a mysqldump of the production database.
cp /srv/pamapam/pamapam-backoffice.war /srv/backups/$(date +%Y%m%d%T)
cp /srv/pamapam/resources.zip /srv/backups/$(date +%Y%m%d%T)
mysqldump --max_allowed_packet=512M -u ${DB_USER_NAME} -p${DB_PASSWORD} ${DB_NAME} > /srv/backups/$(date +%Y%m%d%T)/pamapam_backoffice-$(date +%Y%m%d%T).sql
