#!/bin/sh
REPOSITORY=/srv/borg-backup

if ! test -e $REPOSITORY ; then
    mkdir -p /srv/borg-backup
    borg init --encryption=none $REPOSITORY
fi

BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes borg create -v --stats \
    $REPOSITORY::'{hostname}-{now:%Y-%m-%d}' \
    /srv/backups \
    /srv/backups-qa

borg prune -v --list $REPOSITORY --prefix '{hostname}-' --keep-daily=7 --keep-weekly=4 --keep-monthly=6 --keep-yearly=10